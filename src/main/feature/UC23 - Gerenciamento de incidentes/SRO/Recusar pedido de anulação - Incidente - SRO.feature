#language: pt

@SRO @UC23 @WEB @SPRINT10 @RECUSARANULACAO
Funcionalidade: UC-23 - Recusar pedido de anulação - Incidente - SRO

  Cenário: CT01 - Recusar pedido de anulação - Incidente - SRO
    Dado que eu crie um novo incidente via API
    E que eu esteja logado com usuário CCO
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico em Solicitar Anulação do incidente
    E preencho os dados do modal de Solicitar Anulação do incidente
    Entao confirmo o modal de Solicitar Anulação do incidente
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E deve exibir o box com a mensagem "Aguardando retorno da SRO sobre o pedido de anulação."
    E que eu esteja logado com usuário SRO
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    Entao deve exibir o box com a mensagem "Pedido de anulação pelo usuário. Para ver mais detalhes, veja a aba histórico."
    E clico no side-menu HISTÓRICO de Incidentes
    Então valido que o histórico da solicitação de anulação está sendo exibido
    E clico no botão Recusar Anulação
    E preencho o modal de recusar pedido de anulação
    E clico em confirmar do modal de recusar o pedido de anulação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que o histórico da solicitação de anulação recusado está sendo exibido
    Então acesso a TAB Incidentes
    E acesso a TAB Investigação
    E ordeno o incidente por ordem decrescente
    Então confirmo que a investigação está na com status "Aberto" na linha 1

  Cenário: CT02 - Valida pedido de anulação recusado - Usuário solicitante
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E deve exibir o box com a mensagem "Pedido de anulação recusado."
    Entao clico no side-menu HISTÓRICO de Incidentes
    E valido que o histórico da solicitação de anulação recusado está sendo exibido