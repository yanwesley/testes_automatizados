#language: pt

@SRO @UC23 @WEB @SPRINT10 @REABRIRINCIDENTE.1
Funcionalidade: UC-23 - Reabrir Incidente

  Cenário: CT01 - Reabrir incidente anulado
    Dado que eu crie um novo incidente via API
    E que eu esteja logado com usuário SRO
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico em anular a investigação do incidente
    Entao confirmo a anulação da investigação
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao valido que a investigação foi anulada
    E confirmo que a investigação está na com status Anulado
    Quando clico na lupa o incidente na listagem
    E clico em reabrir incidente
    Entao preencho a justificativa de reabertura
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    E ordeno o incidente por ordem decrescente
    Então confirmo que a investigação está na com status "Reaberto" na linha 1