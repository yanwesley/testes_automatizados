#language: pt

@SRO @UC23 @WEB @SPRINT10 @APROVARINVESTIGACAO
Funcionalidade: UC-23 - Aprovar investigação SRO

  Cenário: CT01 - Aprovar investigação SRO
    Dado que eu crie um novo incidente via API
    E que eu esteja logado com o Criador do incidente
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico no side-menu análise de causa de incidentes
    Entao preencho a tela de Análise de causas de incidentes
    E clicar no botão Adicionar Análise de causas no incidente
    E adiciono análise de causas no Incidente
    E preencho a causa identificada no incidente
    Quando clico no botão salvar da análise de causa do incidente
    Entao deve exibir a snackBar com a mensagem "Análise de causa salva com sucesso"
    E valido os porquês da análise de causa do incidente
    Entao clicar no botão Salvar e Criar Plano de Ação do incidente
    E clicar na confirmação para Salvar e Criar Plano de Ação do incidente
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E acesso a TAB Central de Ações
    E deve exibir a pagina de listagem de ações
    Entao valido que o plano de ação foi criado a partir do incidente
    Dado que eu esteja logado com o Criador do incidente
    E acesso a TAB Central de Ações
    E clicar no botão Editar ação da central de ações
    E valido que o plano avulso foi criado pelo incidente
    Quando eu clico no botão Criar uma ação
    E ao preencher as informações iniciais
    E adicionar um responsável na ação
    Então clico no botão Salvar do Modal de Incluir Uma Ação
    E deve exibir a snackBar com a mensagem "Ação criada com sucesso."
    E que eu esteja logado com usuário CCO
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao clicar no botão Aceitar Ação
    Então deve exibir o modal de confirmação para o Aceite da Ação
    E ao clicar em Confirmar no modal de Aceite da Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    Quando clico no side-menu Plano de ação de incidentes
    Então clico no botão SALVAR E ENCERRAR incidente
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E que eu esteja logado com usuário SRO
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico em aprovar a investigação do incidente
    Entao confirmo a aprovação da investigação
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E acesso a TAB Incidentes
    E acesso a TAB Encerrados
    E ordeno o incidente por ordem decrescente
    Então confirmo que a investigação está na com status "Encerrado" na linha 1

  Cenário: CT02 - Reabrir incidente Aprovado
    E que eu esteja logado com usuário SRO
    E acesso a TAB Incidentes
    E acesso a TAB Encerrados
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Visualizar do incidente ordenado por ordem decrescente
    E clico em reabrir incidente
    Entao preencho a justificativa de reabertura
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Então confirmo que a investigação está na com status "Reaberto" na linha 1