#language: pt

@SRO @UC23 @WEB @SPRINT10 @ANULARINVESTIGACAO
Funcionalidade: UC-23 - Anular Investigação

  Cenário: CT01 - Anular Investigação SRO
    Dado que eu crie um novo incidente via API
    E que eu esteja logado com usuário SRO
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico em anular a investigação do incidente
    Entao confirmo a anulação da investigação
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao valido que a investigação foi anulada
    E confirmo que a investigação está na com status Anulado

