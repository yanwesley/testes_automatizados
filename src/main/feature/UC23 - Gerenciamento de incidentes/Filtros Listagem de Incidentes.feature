#language: pt

@SRO @UC23 @WEB @SPRINT10 @FILTROSLISTAGEMINCIDENTE
Funcionalidade: UC23 - Filtros Listagem de Incidentes

  Cenario: CT01 - Valida que o botão Novo Sinistro não é exibido
    Dado que eu crie um novo incidente via API
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E acesso a TAB Investigação
    Entao valido que o botão Novo Incidente deve ser exibido

  Cenario: CT02 - Valida exibição dos elementos permitidos para o perfil SRO
    Entao deve exibir os campos do header do Incidente para filtrar a lista e exportar

  Cenario: CT03 - Valida exibição dos campos da tabela de registros abertos conforme permissões
    Entao deve exibir os campos da tabela de de Incidentes

  Cenario: CT04 - Valida filtro por texto com resultado válidos
    Quando eu busco pelo texto "Rolamento Aquecido" no filtro de incidente
    Entao deve ser exibido os registros que contenham o texto "Rolamento Aquecido" na lista de incidente

  Cenario: CT05 - Valida filtro por texto sem resultados na tab de Investigação
    Quando eu busco pelo texto "Chimpanzé" no filtro de incidente
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado." na lista de incidente

  Cenario: CT06 - Valida filtro por data inicial
    Quando filtro a lista de Incidentes por data inicial "01/10/2019"
    Entao deve exibir apenas os incidentes abertos a partir de "01/08/2019"

  Cenario: CT07 - Valida filtro por data final
    Quando filtro por incidentes com data final "01/12/2019"
    Entao deve exibir apenas os incidentes abertos até "01/12/2019"

  Cenario: CT08 - Valida filtro por data final e inicial sem resultados
    Quando filtro por data inicial "02/07/2018" e data final "05/07/2018" na lista de incidentes
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado." na lista de incidente

  Cenario: CT09 - Valida filtro por texto com resultado válidos na TAB Incidentes criados
    E acesso a TAB Investigação
    E limpo os filtros de data de Incidentes
    Quando eu busco pelo texto "Rolamento Aquecido" no filtro de incidente
    Entao deve ser exibido os registros que contenham o texto "Rolamento Aquecido" na lista de incidente

  Cenario: CT10 - Valida filtro por texto sem resultado na TAB Incidentes criados
    E acesso a TAB Incidentes criados
    Quando eu busco pelo texto "Chimpanzé" no filtro de incidente
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado." na lista de incidente

  Cenario: CT11 - Valida filtro por texto com resultado válidos na TAB Encerrados
    E acesso a TAB Encerrados
    Quando eu busco pelo texto "Rolamento Aquecido" no filtro de incidente
    Entao deve ser exibido os registros que contenham o texto "Rolamento Aquecido" na lista de incidente

  Cenario: CT12 - Valida filtro por texto sem resultado na TAB Encerrados
    E acesso a TAB Encerrados
    Quando eu busco pelo texto "Chimpanzé" no filtro de incidente
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado." na lista de incidente