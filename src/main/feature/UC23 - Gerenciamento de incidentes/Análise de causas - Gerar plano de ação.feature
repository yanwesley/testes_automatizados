#language: pt

@SRO @UC23 @WEB @SPRINT10 @PLANOACAOINCIDENTE
Funcionalidade: UC-23 - Gerenciamento de incidentes - Análise de causas e gerar Plano

  Cenário: CT01 - Cadastrar nova Análise de causas e gerar plano
    Dado que eu crie um novo incidente via API
    E que eu esteja logado com o Criador do incidente
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico no side-menu análise de causa de incidentes
    Entao preencho a tela de Análise de causas de incidentes
    E clicar no botão Adicionar Análise de causas no incidente
    E adiciono análise de causas no Incidente
    E preencho a causa identificada no incidente
    Quando clico no botão salvar da análise de causa do incidente
    Entao deve exibir a snackBar com a mensagem "Análise de causa salva com sucesso"
    E valido os porquês da análise de causa do incidente
    Entao clicar no botão Salvar e Criar Plano de Ação do incidente
    E clicar na confirmação para Salvar e Criar Plano de Ação do incidente
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E acesso a TAB Central de Ações
    E deve exibir a pagina de listagem de ações
    Entao valido que o plano de ação foi criado a partir do incidente

  Cenário: CT02 - Validar os dados do plano de ação criado
    Dado que eu esteja logado com o Criador do incidente
    E acesso a TAB Central de Ações
    E clicar no botão Editar ação da central de ações
    E valido que o plano avulso foi criado pelo incidente

  Cenario: CT03 - Criar uma ação para o plano e validar no incidente
    Quando eu clico no botão Criar uma ação
    E ao preencher as informações iniciais
    E adicionar um responsável na ação
    Então clico no botão Salvar do Modal de Incluir Uma Ação
    E deve exibir a snackBar com a mensagem "Ação criada com sucesso."
    Entao que eu esteja logado com o Criador do incidente
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    Quando clico no side-menu Plano de ação de incidentes
    Entao valido a ação incluída na análise de causa

  Cenario: CT04 - Valida o detalhe da ação incluída
    Dado clico na lupa para visualizar detalhe da ação do incidente
    Entao valido os dados da ação inserida do incidente no dialog

  Cenário: CT05 - Editar a Análise de causas
    Dado que eu crie um novo incidente via API
    E que eu esteja logado com o Criador do incidente
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico no side-menu análise de causa de incidentes
    Entao preencho a tela de Análise de causas de incidentes
    E clicar no botão Adicionar Análise de causas no incidente
    E adiciono análise de causas no Incidente
    E preencho a causa identificada no incidente
    Quando clico no botão salvar da análise de causa do incidente
    Entao deve exibir a snackBar com a mensagem "Análise de causa salva com sucesso"
    E valido os porquês da análise de causa do incidente
    Quando clico no botão editar uma análise de causa
    Entao edito as informações da análise de causa
    Quando clico no botão salvar da análise de causa do incidente
    Entao deve exibir a snackBar com a mensagem "Análise de causa salva com sucesso"
    E clico no botão salvar da pagina de Análise de causas
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao valido os dados editados da Análise de causas
    Entao clicar no botão Salvar e Criar Plano de Ação do incidente
    E clicar na confirmação para Salvar e Criar Plano de Ação do incidente
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"