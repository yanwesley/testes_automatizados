#language: pt

@SRO @UC23 @WEB @SPRINT10 @SOLICITARANULACAO
Funcionalidade: UC-23 - Solicitar anulação - Incidente - outros

  Cenário: CT01 - Solicitar anulação - Incidente - outros
    Dado que eu crie um novo incidente via API
    E que eu esteja logado com usuário CCO
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico em Solicitar Anulação do incidente
    E preencho os dados do modal de Solicitar Anulação do incidente
    Entao confirmo o modal de Solicitar Anulação do incidente
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E deve exibir o box com a mensagem "Aguardando retorno da SRO sobre o pedido de anulação."
    Entao clico no side-menu HISTÓRICO de Incidentes
    E valido que o histórico da solicitação de anulação está sendo exibido
    Então acesso a TAB Incidentes
    E acesso a TAB Investigação
    E ordeno o incidente por ordem decrescente
    Então confirmo que a investigação está na com status "Solicitação anulação" na linha 1


