#language: pt

@SRO @UC23 @WEB @SPRINT10 @AREARESPONSAVEL
Funcionalidade: UC-23 - Área responsável - Incidente - outros

  Cenário: CT01 -Alterar a Área responsável do Incidente
    Dado que eu crie um novo incidente via API
    E que eu esteja logado com usuário CCO
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico no side-menu Área responsável de Incidentes
    E preencho os dados da área responsavel do incidente
    Então salvo os dados da área responsavel do incidente
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao valido que os dados da área responsavel do incidente persistiram

  Cenário: CT01 - Valido area alterada na listagem de Incidente
    E que eu esteja logado com usuário CCO
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Entao deve exibir a pagina de listagem de Incidentes
    E ordeno o incidente por ordem decrescente
    Então valido que a área responsavel é a que foi alterada


