#language: pt

@SRO @UC09 @WEB @SPRINT10 @ANEXOINCIDENTE
Funcionalidade: UC-23 - Gerenciamento de incidentes - Anexos

  Cenário: CT01 - Incluindo um Anexo
    Dado que eu crie um novo incidente via API
    E que eu esteja logado com usuário SRO
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E acesso a TAB Investigação
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico no side-menu ANEXO de Incidentes
    E estou na página de ANEXOS de Incidentes
    E clico no botão Inserir anexos de Incidentes
    Dado insere 3 arquivo na tela de anexos de incidentes com comentário
    Entao valida que o(s) 3 arquivo(s) foram adicionados em incidentes

  Cenário: CT02 - Editando um comentário do anexo
    Dado clico no botão Editar do anexo de Incidentes
    E edito o comentário do anexo do incidente
    E clico no botão Salvar da tela de anexos
    Entao deve exibir a snackBar com a mensagem "A informação foi registrada com sucesso!"
    E valida que o comentário do anexo de Incidente foi editado

  Cenário: CT03 - Excluindo Anexo
    Dado excluo 1 arquivos dos Anexos de incidentes
    Entao valida que o(s) 2 arquivo(s) foram adicionados em incidentes
