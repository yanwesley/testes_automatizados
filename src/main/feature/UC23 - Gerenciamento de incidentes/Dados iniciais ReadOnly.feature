#language: pt

@SRO @UC23 @WEB @SPRINT10 @NOVO_INCIDENTE_READONLY
Funcionalidade: UC-23 - Gerenciamento de incidentes - Novo Incidente ReadOnly

  Cenário: CT01 - Cadastrar dados iniciais de um novo Incidente
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Incluir novo incidente
    Então estou na página de Dados Iniciais de Incidentes
    E ao preencher os dados iniciais do incidente
    E clicar no Salvar e Abrir Incidente
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."

  Cenário: CT02 - Validação dos dados cadastrados do novo incidente como ReadOnly
    Dado que eu esteja logado com o Criador do incidente
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E acesso a TAB Investigação
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    Entao valida os dados preenchidos do Incidente ReadOnly

  Cenario: CT03 - Editando os dados depois ao após clicar em Editar
    Dado clico nos botões de editar da tele de incidente
    E edito os dados do incidente
    Então valido os dados editados do incidente
