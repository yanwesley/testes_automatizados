#language: pt

@SRO @UC23 @WEB @SPRINT10 @HISTORICOINCIDENTE
Funcionalidade: UC23 - Histórico Incidente

  Cenário: CT01 - Cadastrar nova Análise de causas e gerar plano
    Dado que eu crie um novo incidente via API
    E que eu esteja logado com o Criador do incidente
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico no side-menu análise de causa de incidentes
    Entao preencho a tela de Análise de causas de incidentes
    E clicar no botão Adicionar Análise de causas no incidente
    E adiciono análise de causas no Incidente
    E preencho a causa identificada no incidente
    Quando clico no botão salvar da análise de causa do incidente
    Entao deve exibir a snackBar com a mensagem "Análise de causa salva com sucesso"
    E valido os porquês da análise de causa do incidente
    Entao clicar no botão Salvar e Criar Plano de Ação do incidente
    E clicar na confirmação para Salvar e Criar Plano de Ação do incidente
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    #Preencher área responsável
    E clico no side-menu Área responsável de Incidentes
    E preencho os dados da área responsavel do incidente
    Então salvo os dados da área responsavel do incidente
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao valido que os dados da área responsavel do incidente persistiram
    #Inserir Anexo
    E clico no side-menu ANEXO de Incidentes
    E estou na página de ANEXOS de Incidentes
    E clico no botão Inserir anexos de Incidentes
    Quando insere 1 arquivo na tela de anexos de incidentes com comentário
    Entao valida que o(s) 1 arquivo(s) foram adicionados em incidentes
    #Anular Investigação
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E acesso a TAB Investigação
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    Mas clico no side-menu Dados Iniciais de Incidentes
    E clico em anular a investigação do incidente
    Entao confirmo a anulação da investigação
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    #Reabrir a investigação
    Entao clico em reabrir incidente
    E preencho a justificativa de reabertura
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenário: CT02 - Valida histórico do incidente
    E que eu esteja logado com usuário SRO
    E acesso a TAB Incidentes
    E acesso a TAB Investigação
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E clico no side-menu HISTÓRICO
    Então deve exibir a lista com as ações realizadas
      | Incluiu o incidente          |
      | Editou o incidente           |
      | Incluiu uma análise de causa |
      | Editou o incidente           |
      | Alterou o responsável        |
      | Anulou o incidente           |
