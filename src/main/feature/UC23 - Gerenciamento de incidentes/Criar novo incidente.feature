#language: pt

@SRO @UC23 @WEB @SPRINT10 @NOVO_INCIDENTE @REGRESSION.01
Funcionalidade: UC-23 - Gerenciamento de incidentes - Novo Incidente

  Cenário: CT01 - Cadastrar dados iniciais de um novo Incidente
    E que eu esteja logado com usuário SRO
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E clicar no botão Incluir novo incidente
    Então estou na página de Dados Iniciais de Incidentes
    E ao preencher os dados iniciais do incidente
    E clicar no Salvar e Abrir Incidente
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."

  Cenário: CT02 - Validação dos dados cadastrados do novo incidente
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E acesso a TAB Incidentes criados
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    Entao valida os dados preenchidos do Incidente

  Cenário: CT03 - Validação dos dados Editado do incidente
    E acesso a TAB Incidentes
    Entao deve exibir a pagina de listagem de Incidentes
    E acesso a TAB Incidentes criados
    E clicar no botão Editar da incidente ordenado por ordem decrescente
    E edito os dados do incidente
    Então valido os dados editados do incidente
