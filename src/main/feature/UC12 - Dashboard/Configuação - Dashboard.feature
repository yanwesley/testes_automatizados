#language: pt

@SRO @UC11 @WEB @SPRINT11 @CONFIDASHBOARD
Funcionalidade: UC12 - Dashboard - Configuração Dashboard

  Cenário: CT01 - Preencher configuração Dashboard
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Configurações
    E acesso a TAB Dashboard das configurações
    Então preencho a tabela de Tolerância para acidentes
    E preencho a tabela de Tolerância para gravidades
    E clico no botão para Salvar as configurações do dashboard
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    E acesso a TAB Parametrização
    E acesso a TAB Dashboard das configurações
    E valido as informações inseridas nas configurações do dashboard
