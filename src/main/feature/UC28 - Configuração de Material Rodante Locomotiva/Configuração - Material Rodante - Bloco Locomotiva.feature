#language: pt

@SRO @UC25 @WEB @SPRINT12 @CONFIGMATERIALRODANTE
Funcionalidade: UC28 - SRO Configurações - Material Rodante - Bloco Locomotiva

  Cenário: CT01 - Criar uma nova configuração
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Configurações
    E acesso a TAB Material Rodante das configurações
    E clico no separator para expandir o formulário de configuração de material rodante
    Entao clico no botão Adicionar bloco de configuração de Material Rodante
    E Insiro o nome do bloco da configuração de Material Rodante
    E Clica em confirmar no dialog para adicionar uma configuração de Material Rodante
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valida que um novo bloco que configuração de Material Rodante foi adicionado
    E valida que o status do bloco de configuração é "ativo"

  Cenário: CT02 - Editar uma nova configuração
    Entao clico no botão Editar bloco
    E edito os dados do bloco de configuração
    E Clica em confirmar no dialog para adicionar uma configuração de Material Rodante
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valida que um novo bloco que configuração de Material Rodante foi editado
    E valida que o status do bloco de configuração é "inativo"

  Cenário: CT03 - Deixar o bloco de configuração ativo
    Entao clico no botão Editar bloco
    E deixa o bloco de configuração de material rodante ativo
    E Clica em confirmar no dialog para adicionar uma configuração de Material Rodante
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valida que o status do bloco de configuração é "ativo"






