#language: pt

@SRO @UC28 @WEB @SPRINT12 @CONFIGLOCOMOTIVA
Funcionalidade: UC28 - SRO Configurações - Configuração locomotiva

  Cenário: CT01 - Criar uma nova configuração locomotiva e validar as informações
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Configurações
    E acesso a TAB Material Rodante das configurações
    Quando clico no separator para expandir o formulário de configuração de material rodante
    Entao clico no botão Adicionar bloco de configuração de Material Rodante
    E Insiro o nome do bloco da configuração de Material Rodante
    E Clica em confirmar no dialog para adicionar uma configuração de Material Rodante
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E clico no botão adicionar configuração na configuração de locomotiva
    Entao preencho a tela de criar configuração locomotiva inicial
    E clico em salvar a configuração da locomotiva
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Quando clico no separator para expandir o formulário de configuração de material rodante
    E clico no botão editar configuração na configuração de locomotiva
    E valido que as informações de configuração de locomotiva inicial persistiram
    E clico em salvar a configuração da locomotiva
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenário: CT02 - Adicionar configuração copiada
    Quando clico no separator para expandir o formulário de configuração de material rodante
    E clico no botão adicionar configuração na configuração de locomotiva
    E preencho o modal adicionando configuração copiada
    E preencho o modelo e configuração da tela de configuração
    Entao valido que os valores dos parâmetros foram copiados da configuração locomotiva
    E clico em salvar a configuração da locomotiva
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenário: CT03 - Editar a configuração locomotiva e validar
    Quando clico no separator para expandir o formulário de configuração de material rodante
    E clico no botão editar configuração na configuração de locomotiva
    Entao edito a tela de criar configuração locomotiva inicial
    E clico em salvar a configuração da locomotiva
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Quando clico no separator para expandir o formulário de configuração de material rodante
    E clico no botão editar configuração na configuração de locomotiva
    E valido que as informações de configuração de locomotiva editadas persistiram
    E clico em salvar a configuração da locomotiva
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenário: CT04 - Adiciono outra configuração locomotiva
    Quando clico no separator para expandir o formulário de configuração de material rodante
    E clico no botão adicionar configuração na configuração de locomotiva
    E preencho o modal adicionar bloco sem copiar parâmetro
    Entao preencho a tela de criar configuração locomotiva
    E clico em salvar a configuração da locomotiva
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Quando clico no separator para expandir o formulário de configuração de material rodante
    E clico no botão editar configuração adicional na configuração de locomotiva na linha 1
    E valido que as informações de configuração de locomotiva persistiram
    E clico em salvar a configuração da locomotiva
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenário: CT05 - Excluir configuração adicional
    Quando clico no separator para expandir o formulário de configuração de material rodante
    E clico no botão excluir configuração adicional de locomotiva na linha 1
    E confirmo a exclusão da configuração adicional locomotiva
    Entao deve exibir a snackBar com a mensagem "Informações excluídas com sucesso"







