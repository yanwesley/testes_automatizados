#language: pt

@SRO @UC08 @WEB @SPRINT01
Funcionalidade: UC08 - SRO Sinistros - Visão registros de sinistros Encerrados - perfil CCO

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    E seleciono a TAB de sinistros Encerrados

  Cenario: CT01 - Valida exibição dos campos da tabela de registros Encerrados conforme permissões
    Entao deve exibir os campos permitidos em registros Encerrados para o perfil CCO

  Cenario: CT02 - Valida filtro por texto com resultado válidos
    Quando eu busco pelo texto "ASD"
    Entao deve ser exibido os registros que contenham o texto "ASD"

  Cenario: CT03 - Valida filtro por texto sem resultados
    Quando eu busco pelo texto "Calafrios"
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado."