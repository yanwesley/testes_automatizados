#language: pt

@SRO @UC08 @WEB @SPRINT01
Funcionalidade: UC08 - SRO Sinistros - Visão registros de sinistros Encerrados - perfil SRO

  Contexto:
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    E seleciono a TAB de sinistros Encerrados

  Cenario: CT01 - Valida exibição dos campos da tabela de registros Encerrados conforme permissões
    Entao deve exibir os campos permitidos em registros encerrados para o perfil SRO

  Cenario: CT02 - Valida filtro por texto com resultado válidos
    Quando eu busco pelo texto "ASD"
    Entao deve ser exibido os registros que contenham o texto "ASD"

  Cenario: CT03 - Valida filtro por texto sem resultados
    Quando eu busco pelo texto "Chimpanzé"
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado."

  Cenario: CT04 - Valida exportação XLS
  Apenas valida se baixou o arquivo
    Quando clico no botão Exportar
    Entao deve efetuar o download do arquivo XLS "relatorio_sinistros"