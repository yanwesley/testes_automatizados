#language: pt

@SRO @UC08 @WEB @SPRINT01
Funcionalidade: UC08 - SRO Sinistros - Visão registros de sinistros Abertos - perfil CCO

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros

  Cenario: CT01 - Valida que o botão Novo Sinistro é exibido
    Entao valido que o botão Novo Sinistro é exibido

  Cenario: CT02 - Valida navegação do botão Incluir novo sinistro para a tela de Entrada do novo sinistro
    Quando clico no botão Incluir novo sinistro
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro

  Cenario: CT03 - Valida exibição dos elementos permitidos para o perfil CCO
    Entao deve exibir os campos permitidos para o perfil CCO

  Cenario: CT04 - Valida exibição dos campos da tabela de registros abertos conforme permissões
    Entao deve exibir os campos da tabela de registros abertos permitidos para o perfil CCO

  Cenario: CT05 - Valida filtro por texto com resultado válidos
    Quando eu busco pelo texto "85272010"
    Entao deve ser exibido os registros que contenham o texto "85272010"

  Cenario: CT06 - Valida filtro por texto sem resultados
    Quando eu busco pelo texto "Inflação"
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado."

  Cenario: CT07 - Valida filtro por data inicial
    Quando filtro por data inicial "01/08/2019"
    Entao deve exibir apenas os registros abertos a partir de "01/08/2019"

  Cenario: CT08 - Valida filtro por data final
    Quando filtro por data final "01/08/2019"
    Entao deve exibir apenas os registros abertos até "01/08/2019"

  Cenario: CT09 - Valida filtro por data final e inicial sem resultados
    Quando filtro por data final "02/07/2018" e data final "05/07/2018"
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado."

  Cenario: CT10 - Valida ação do botão de remover filtro
    Quando eu remover o filtro selecionado
    Entao deve exibir os resultados dos últimos três meses
    E o botão filtrar deve estar com a cor "filled"