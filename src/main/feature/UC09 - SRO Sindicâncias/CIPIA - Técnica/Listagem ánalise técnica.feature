#language: pt

@SRO @UC09 @WEB @SPRINT06 @LISTAGEMTÉCNICA @REGRESSION06.1
Funcionalidade: UC09 - SRO Sindicâncias - Listagem análise técnica

  Cenário: CT02 - Valido as informações de conclusão
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E ordeno a sindicancia por ordem decrescente
    Entao eu valido o status conclusão não iniciado

  Cenário: CT03 - Insiro conclusão CIPIA VIA
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CONCLUSÃO
    E estou na página de CONCLUSÃO
    Quando preencho o campo de Conclusão
    E clico no botão Salvar e Concluir
    E deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"

  Cenário: CT04 - Insiro conclusão CIPIA MATERIAL RODANTE
    Dado que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CONCLUSÃO
    E estou na página de CONCLUSÃO
    Quando preencho o campo de Conclusão
    E clico no botão Salvar e Concluir
    E deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"

  Cenário: CT05 - Insiro conclusão CIPIA OPERAÇÃO
    Dado que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CONCLUSÃO
    E estou na página de CONCLUSÃO
    Quando preencho o campo de Conclusão
    E clico no botão Salvar e Concluir
    E deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"

  Cenário: CT06 - Insiro conclusão SEGURANÇA PATRIMONIAL
    Dado que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu Registro do acidente
    E ao adicionar todas as imagens e comentários do Registro do Acidente
    E clico no botão Salvar e Concluir Segurança Patrimonial
    E deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"

  Cenário: CT07 - Valido o status conclusão Finalizado
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E ordeno a sindicancia por ordem decrescente
    Entao eu valido o status conclusão Finalizado
