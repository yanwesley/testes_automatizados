#language: pt

@SRO @UC09 @WEB @SPRINT06 @HistoricoCP
Funcionalidade: UC09 - SRO Sindicância - Histórico - CIPIA Técnica

  Cenário: CT01 - Validando exibição da tela de histórico
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Análise Técnica
    E clico no side-menu HISTÓRICO
    Entao deve exibir a página de histórico para a Análise Técnica

  Cenário: CT02 - Validando exibição da tela de histórico no modo ReadOnly
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Análise Técnica
    E clico no side-menu HISTÓRICO
    Entao deve exibir a página de histórico para a Análise Técnica