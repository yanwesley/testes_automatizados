#language: pt

@SRO @UC09 @WEB @SPRINT06 @CONCLUSAOPRESIDENTE
Funcionalidade: UC09 - SRO Sindicância - Conclusão - CIPIA Técnica

  Cenário: CT01 - Valida Exibição Menu Custos
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CONCLUSÃO
    E estou na página de CONCLUSÃO
    E valido a label "Insira abaixo a conclusão da equipe técnica:" da conclusão da Análise Técnica
    Quando preencho o campo de Conclusão
    E clico no botão Salvar da Conclusão
    E deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"

  Cenario: CT02 - Edito o relatório final com o perfil SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CONCLUSÃO
    E estou na página de CONCLUSÃO
    Quando preencho o campo de Conclusão
    E clico no botão Salvar da Conclusão
    E deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"

  Cenario: CT03 - Valido o modo ReadOnly
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Análise Técnica
    E clico no side-menu CONCLUSÃO
    Entao valido que a conclusão inserida está sendo apresentada no modo readonly