#language: pt

@SRO @UC09 @WEB @SPRINT06 @RELATORIOFINAL @REGRESSION06.3
Funcionalidade: UC09 - SRO Sindicâncias - Relatório Final

  Cenário: CT01 - Valido a exibição das conclusões das outras áreas
    Dado que eu crie um novo sinistro sem veiculos via API
    E concluo a áreas da sindicância via API
    E preencho a descrição da Segurança Patrimonial da sindicância via API
    Quando que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que estou na página relatório final
    Entao valido o preenchimento das conclusões de cada área

  Cenário: CT02 - Valido o preenchimento do Relatório Final
    Quando preencho o dono do acidente
    E preencho as definições de causa
    E salvo as informações
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao deve persistir as informações do Relatorio final

  Cenário: CT03 - Adiciono mais causas contributivas
    Quando adiciono mais causas contributivas
    E salvo as informações
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E deve persistir as informações com mais causas contributivas

  Cenário: CT04 - Removo as Causas contributivas
    Quando removo as causas contributivas
    E salvo as informações
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E deve persistir as informações com a remoção das causas contributivas

  Cenario: CT05 - Edito o relatório final com o perfil SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que estou na página relatório final
    Quando preencho o dono do acidente
    E preencho as definições de causa
    E salvo as informações
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao deve persistir as informações do Relatorio final

  Cenario: CT06 - Valido o modo ReadOnly
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Análise Técnica
    E valido que estou na página relatório final
    Entao valido o preenchimento das conclusões de cada área

