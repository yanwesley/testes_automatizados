#language: pt

@SRO @UC09 @WEB @SPRINT06 @ANALISEDECAUSA
Funcionalidade: UC09 - SRO Sindicâncias - Relatório Final

  Cenário: CT01 - Adiciono análises de causa
    Dado que eu crie um novo sinistro sem veiculos via API
    E concluo a áreas da sindicância via API
    E preencho a descrição da Segurança Patrimonial da sindicância via API
    E preencho o relatório final da CIPIA Técnica da sindicância via API
    Quando que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu análise de causa
    Entao valido o preenchimento da análise de causa
    E adiciono análise de causas
    Quando clico no botão salvar da análise de causa
    Entao deve exibir a snackBar com a mensagem "Análise de causa salva com sucesso"
    E valido os porquês da análise de causa

  Cenário: CT02 - Edito e valido a análise de causa
    Dado que edito a análise de causa
    Quando clico no botão salvar da análise de causa
    Entao deve exibir a snackBar com a mensagem "Análise de causa salva com sucesso"
    E valido que as informações editadas foram salvas na análise de causa

  Cenário: CT03 - Excluo a análise de causa
    Dado que excluo a análise de causa
    Entao deve exibir a snackBar com a mensagem "Registro deletado com sucesso"
    E valido que a análise de causa foi excluida

  Cenario: CT04 - Edito a análise de causa com o perfil SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu análise de causa
    Entao valido o preenchimento da análise de causa
    E adiciono análise de causas
    Quando clico no botão salvar da análise de causa
    Entao deve exibir a snackBar com a mensagem "Análise de causa salva com sucesso"
    Dado que excluo a análise de causa
    Entao deve exibir a snackBar com a mensagem "Registro deletado com sucesso"
    E valido que a análise de causa foi excluida

  Cenario: CT05 - Valido o modo ReadOnly
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Análise Técnica
    E clico no side-menu análise de causa
    Entao valido o preenchimento da análise de causa