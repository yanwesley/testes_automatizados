#language: pt

@SRO @UC09 @WEB @SPRINT06 @PLANOACAO @REGRESSION06.2
Funcionalidade: UC09 - SRO Sindicâncias - Plano de ação - CIPIA Técnica

  Cenário: CT01 - Valido o preenchimento do plano de ação
    Dado que eu crie um novo sinistro sem veiculos via API
    E concluo a áreas da sindicância via API
    E preencho a descrição da Segurança Patrimonial da sindicância via API
    E preencho o relatório final da CIPIA Técnica da sindicância via API
    Quando que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu Plano de ação
    Entao valido as informações preenchidas do plano de ação

  Cenario: CT02 - Valido a exibição em modo ReadOnly
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Análise Técnica
    E clico no side-menu Plano de ação
    Entao valido as informações preenchidas do plano de ação