#language: pt

@SRO @UC09 @WEB @SPRINT06 @CUSTOSCIPIATECNICA
Funcionalidade: UC09 - SRO Sindicância - Custos - CIPIA Técnica

  Cenário: CT01 - Valida Exibição Menu Custos
    Dado que eu crie um novo sinistro sem veiculos via API
    E adiciono uma LOCOMOTIVA via API
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos

  Cenario: CT02 - Incluir custo de Material CIPIA VIA
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo de Materiais
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de Materiais na linha 1

  Cenario: CT03 - Incluir custo MAT RODANTE
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu CUSTOS
    Entao valida que existe a tabela de custos de Locomotiva
    E clica no botão para adicionar um novo custo de Locomotiva
    E preenche o modal de Custo de Locomotiva
    E clicar no botão salvar do modal Custo de Locomotiva
    Entao valida que o custo foi adicionado na tabela de custos de Locomotiva
    E clico no botão Salvar Informações da tela de custos Material Rodante
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenario: CT04 - Incluir custo de OPERAÇÃO
    Dado que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de custos na linha 1

  Cenario: CT04 - Incluir custo de SEGURO PATRIMONIAL
    Dado que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de custos na linha 1

  Cenario: CT05 - Incluir custo de CONTROLE DE PERDAS
    Dado que eu esteja logado com usuário Controle de Perdas
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu Controle de Perdas
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de custos na linha 1

  Cenario: CT06 - Incluir custo de MEIO AMBIENTE
    Dado que eu esteja logado com usuário Meio Ambiente
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu Meio Ambiente
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de custos na linha 1

  Cenario: CT07 - Incluir custo de SST
    Dado que eu esteja logado com usuário SST
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu SST
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de custos na linha 1

  Cenario: CT08 - Incluir custo de TO
    Dado que eu esteja logado com usuário TO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu TO
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de custos na linha 1

  Cenario: CT09 - Validar o custo total no CIPIA Técnica
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CUSTOS
    Entao valido o valor dos custos

#  Cenario: CT10 - Validar o custo de cada área no modo ReadOnly
#    Dado que eu esteja logado com usuário CIPIA Técnica
#    E acesso a TAB Sindicâncias
#    Entao deve exibir a pagina de listagem de Sindicâncias
#    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
#    E clico no side-menu CUSTOS
#    Entao valido o valor dos custos de cada área

  Cenario: CT11 - Valido o modo ReadOnly Custos
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Análise Técnica
    E clico no side-menu CUSTOS
    Entao valido o valor dos custos de cada área

