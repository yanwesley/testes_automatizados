#language: pt

@SRO @UC09 @WEB @SPRINT06 @REGISTROACIDENTE
Funcionalidade: UC09 - SRO Sindicâncias - Segurança Patrimonial - Registro do acidente

  Cenário: CT01 - Valida inserção de imagens e comentários do registro de acidentes
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao clico na TAB Segurança Patrimonial
    E clico no side-menu Registro do acidente
    Entao deve exibir a página de Registro do Acidente
    E ao adicionar todas as imagens e comentários do Registro do Acidente
    E clico no botão Salvar Informações do Registro do Acidente
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido se os dados do registro fotográfico foram persistido

  Cenário: CT02 - Valida edição dos comentários do registro de acidentes
    Dado que eu edite os comentários das imagens do registro de acidentes
    E clico no botão Salvar Informações do Registro do Acidente
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido se os comentários editado do registro fotográfico

  Cenário: CT03 - Valida exclusão dos anexos e comentários do registro de acidentes
    Então que eu exclua os comentários e as imagens do registro de acidentes

  Cenário: CT04 - Valida adição de nova imagem no registro de acidentes
    Dado que eu insira uma nova imagem no registro de acidentes
    Entao deve exibir a snackBar com a mensagem "Registro do acidente salvo com sucesso"
    E adiciono um comentário para a nova imagem
    E clico no botão Salvar Informações do Registro do Acidente
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido se o comentário e a imagem está sendo exibida do registro fotográfico

  Cenario: CT06 - Editar registro do acidente com perfil SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Segurança Patrimonial
    Entao clico no side-menu Registro do acidente
    E ao adicionar todas as imagens e comentários do Registro do Acidente via SRO
    E clico no botão Salvar Informações do Registro do Acidente
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenário: CT05 - Valido mudança de status ao concluir a SEGURANÇA PATRIMONIAL
    E que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao clico na TAB Segurança Patrimonial
    E clico no side-menu Registro do acidente
    Entao deve exibir a página de Registro do Acidente
    Dado clico no botão Salvar e Concluir Segurança Patrimonial
    Entao deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"
    Mas ordeno a sindicancia por ordem decrescente
    E eu valido se o status conclusão da area está Finalizado

  Cenário: CT07 - Validar dados registro do acidente readonly
    E que eu esteja logado com usuário Financeiro
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu Registro do acidente
    E valido que estou na página registro do acidente
    E valido que as informações registro do acidente foram exibidas no modo readonly






