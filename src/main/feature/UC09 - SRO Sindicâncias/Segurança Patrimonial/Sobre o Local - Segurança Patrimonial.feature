#language: pt

@SRO @UC09 @WEB @SPRINT06 @SOBREOLOCAL @REGRESSION06.7
Funcionalidade: UC09 - SRO Sindicância - Sobre o Local - Segurança Patrimonial

  Cenário: CT01 - Preencho e valido as informações sobre o local
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu sobre o local
    E valido que estou na página sobre o local
    Então preencho sobre o local
    E clico no botão Salvar do Sobre o Local
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que as informações foram salvas e persistem

  Cenário: CT02 - Edito as informações sobre o local
    Dado Edito as informações
    Quando clico no botão Salvar do Sobre o Local
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que as informações editadas foram salvas e persistem

  Cenário: CT03 - Edito as informações sobre o local com perfil SRO
    E que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu sobre o local
    E valido que estou na página sobre o local
    Então preencho sobre o local
    E clico no botão Salvar do Sobre o Local
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que as informações foram salvas e persistem

  Cenário: CT04 - Validar dados sobre o local readonly
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu sobre o local
    E valido que estou na página sobre o local
    E valido que as informações foram exibidas no modo readonly

  Cenário: CT05 - Edito as informações sobre o local com perfil CIPIA Via
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu sobre o local
    E valido que estou na página sobre o local
    Então Edito as informações
    E clico no botão Salvar do Sobre o Local
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que as informações editadas foram salvas e persistem


