#language: pt

@SRO @UC09 @WEB @SPRINT07 @DESCRICAOSEGPATRIMONIAL
Funcionalidade: UC09 - SRO Sindicância - Descrição - Segurança Patrimonial

  Cenário: CT01 - Preenche e valida o campo descrição
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu descrição
    E valido que estou na página de descrição
    Entao preencho o campo de Descrição
    E clico no botão Salvar da tela de Descrição
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que a descrição persistiu

  Cenário: CT02 - Valida o campo descrição no modo readonly
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu descrição
    E valido que estou na página de descrição
    E valido que a descrição é exibida no modo readonly

  Cenário: CT03 - Preenche e valida o campo descrição como SRO
    E que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu descrição
    E valido que estou na página de descrição
    Entao preencho o campo de Descrição
    E clico no botão Salvar da tela de Descrição
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que a descrição persistiu