#language: pt

@SRO @UC09 @WEB @SPRINT06 @VITIMAS
Funcionalidade: UC09 - SRO Sindicância - Vitimas - Sobre os envolvidos - Segurança Patrimonial

  Cenário: CT01 - Valida exibição da Pagina 'Sobre os Envolvidos' Vítimas.
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu Sobre os envolvidos
    E valido que estou na página Sobre os envolvidos

  Cenario: CT02 - Inclusão de uma Vitima - Sobre os envolvidos
    Quando clico no botão Adicionar vítima
    Entao deve exibir o modal de Adicionar vítima
    Quando preencher todos os campos do modal de Adicionar vítima
    E clico no botão Salvar do modal de Adicionar Vítima
    Então deve exibir a snackBar com a mensagem 'Informações salvas com sucesso'
    E a vítima deve ser inserida na seção de Vítimas

  Cenario: CT03 - Edição de uma Vitima - Sobre os envolvidos
    Dado que existe uma vítima
    Quando clicar no botão Editar da vítima 1
    Entao deve exibir o modal de Adicionar vítima
    Quando edito todos os campos do modal de Adicionar vítima
    E clico no botão Salvar do modal de Adicionar Vítima
    Então deve exibir a snackBar com a mensagem 'Informações salvas com sucesso'
    E a vítima editada deve ser exibida na seção de Vítimas

  Cenario: CT04 - Exclusão de uma Vitima - Sobre os envolvidos
    Dado que existe uma vítima
    Quando clicar no botão Excluir da vítima 1
    Entao deve exibir o modal para excluir com a mensagem 'Deseja excluir a vítima?'
    Quando confirmo a exclusão da Vítima
    Entao deve exibir a snackBar com a mensagem 'Vítima excluída com sucesso'
    E a Vítima deve ser excluída

  Cenario: CT05 - Adicionar observação - Sobre os envolvidos
    Dado que eu adiciono uma observação da seção da vítima
    Quando clico no botão Salvar Informações da página Sobre os envolvidos
    Entao deve exibir a snackBar com a mensagem 'Informações salvas com sucesso'
    E deve persistir o observação da seção da vítima

  Cenario: CT06 - Editar observação - Sobre os envolvidos
    Dado que eu edite a observação da seção da vítima
    Quando clico no botão Salvar Informações da página Sobre os envolvidos
    Entao deve exibir a snackBar com a mensagem 'Informações salvas com sucesso'
    E deve persistir o observação editado da seção da vítima

  Cenario: CT07 - Inclusão de uma Vítima - Sobre os envolvidos com perfil SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu Sobre os envolvidos
    E valido que estou na página Sobre os envolvidos
    Quando clico no botão Adicionar vítima
    Entao deve exibir o modal de Adicionar vítima
    Quando preencher todos os campos do modal de Adicionar vítima
    E clico no botão Salvar do modal de Adicionar Vítima
    Então deve exibir a snackBar com a mensagem 'Informações salvas com sucesso'
    E a vítima deve ser inserida na seção de Vítimas

  Cenario: CT08 - Editar observação - Sobre os envolvidos com perfil SRO
    Dado que eu edite a observação da seção da vítima
    Quando clico no botão Salvar Informações da página Sobre os envolvidos
    Entao deve exibir a snackBar com a mensagem 'Informações salvas com sucesso'
    E deve persistir o observação editado da seção da vítima

  Cenario: CT09 - Valida exibição da Vítima no modo readonly
    Dado que eu esteja logado com usuário Financeiro
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu Sobre os envolvidos
    E valido que estou na página Sobre os envolvidos
    E a vítima deve ser exibida na seção de Vítimas no modo readonly
