#language: pt

@SRO @UC09 @WEB @SPRINT06 @VEICULOSENVOLVIDOS
Funcionalidade: UC09 - SRO Sindicância - Veículos Envolvidos - Sobre os envolvidos - Segurança Patrimonial

  Cenário: CT01 - Valida exibição da Pagina 'Sobre os Envolvidos' Veículos Envolvidos.
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu Sobre os envolvidos
    E valido que estou na página Sobre os envolvidos

  Cenario: CT02 - Inclusão de um Veículo Envolvido - Sobre os envolvidos
    Quando clico no botão Adicionar veículo
    Entao deve exibir o modal de Adicionar veículo
    Quando preencher todos os campos do modal de Adicionar veículo
    E clico no botão Salvar do modal de Adicionar Veículo
    Então deve exibir a snackBar com a mensagem 'Informações salvas com sucesso'
    E o veículo deve ser inserida na seção de Veículos

  Cenario: CT03 - Edição de um Veículo - Sobre os envolvidos
    Dado que existe uma veículo
    Quando clicar no botão Editar da veículo 1
    Entao deve exibir o modal de Adicionar veículo
    Quando edito todos os campos do modal de Adicionar veículo
    E clico no botão Salvar do modal de Adicionar Veículo
    Então deve exibir a snackBar com a mensagem 'Informações salvas com sucesso'
    E o veículo editado deve ser exibido na seção de Veículos

  Cenario: CT04 - Exclusão de um Veículo - Sobre os envolvidos
    Dado que existe uma veículo
    Quando clicar no botão Excluir da veículo 1
    Entao deve exibir o modal para excluir com a mensagem 'Deseja excluir o veículo?'
    Quando confirmo a exclusão do Veículo
    Entao deve exibir a snackBar com a mensagem 'Veículo excluído com sucesso'
    E o Veículo deve ser excluído

  Cenario: CT05 - Inclusão de um Veículo - Sobre os envolvidos com perfil SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu Sobre os envolvidos
    E valido que estou na página Sobre os envolvidos
    Quando clico no botão Adicionar veículo
    Entao deve exibir o modal de Adicionar veículo
    Quando preencher todos os campos do modal de Adicionar veículo
    E clico no botão Salvar do modal de Adicionar Veículo
    Então deve exibir a snackBar com a mensagem 'Informações salvas com sucesso'
    E o veículo deve ser inserida na seção de Veículos

  Cenario: CT06 - Valida exibição do Veículo no modo readonly
    Dado que eu esteja logado com usuário Financeiro
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Segurança Patrimonial
    Entao clico no side-menu Sobre os envolvidos
    E valido que estou na página Sobre os envolvidos
    E o veículo deve ser exibido na seção de Veículos no modo readonly