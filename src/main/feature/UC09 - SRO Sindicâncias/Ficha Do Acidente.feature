#language: pt

@SRO @UC09 @WEB @SPRINT04 @FICHA
Funcionalidade: UC09 - SRO Sindicâncias - Ficha do Acidente

  Cenário: CT01 - Valida campos do sinistro, veiculos e liberação como readOnly na ficha do Acidente
    Dado que eu esteja logado com usuário CCO
    Quando cadastro um novo Sinistro manual com Veículo na linha 1 e datas de liberação
    E que eu esteja logado com usuário CCO VIA
    E preencho os dados do Sinistro com o CCO VIA
    Entao que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Liberação
    Quando que preencho as a Liberação final
    E clico no botão Salvar e Encerrar
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "O sinistro foi encerrado com sucesso."
    Dado que eu esteja logado com usuário SRO
    Entao acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Aceite do Sinistro
    Entao preenche a Escolha do Aceite com Não
    E preenche o motivo da recusa
    E insere 1 arquivo na recusa do sinistro
    Entao valida que os 1 arquivos da recusa foram adicionados
    Quando clico em Confirmar Recusa
    Entao deve exibir a snackBar com a mensagem "Recusa salva com sucesso."
    Quando clico no botão Reabrir Sinistro
    E insiro um comentário para reabertura do sinistro
    E  insere 1 arquivo na reabertura do sinistro
    E clico em confirmar reabertura
    Entao deve exibir a snackBar com a mensagem "Reabertura salva com sucesso."
    Entao preenche a Escolha do Aceite com Sim
    E preenche Classificação como Acidente
    E preenche a Bandeira e a Causa Provável com a opção 1
    Quando clico em Confirmar Aceite
    Entao deve exibir a snackBar com a mensagem "Sinistro aceito com sucesso."
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no botão Ficha do Acidente
    Então deve abrir uma nova guia com a ficha do acidente
    E clico para expandir os dados do sinistro
    E clico na tab Dados do CCO Circulação
    Entao valido que os dados do sinistro foram carregados no formulário como readonly na visao do SRO

  Cenario: CT02 - validando os dados preenchidos pelo CCO VIA
    E clico na tab Dados CCO VIA
    Entao o responsável deve ser inserido na seção de Responsáveis adicionados com os dados ReadOnly
    E exibir a máquina na tabela de máquinas com os dados ReadOnly
    E a turma deve ser inserida na seção de Acionamento de Turmas com os dados ReadOnly

  Cenario: CT03 - Valida histórico do sinistro com eventos de Recusa e Reabertura e Aceite
    E clico na tab Aceite do Sinistro
    Entao deve exibir o histórico de aceite com 1 recusa e 1 reabertura

  Cenário: CT04 - Valida histórico do sinistro
    E clico na tab Histórico
    Então deve exibir a a lista com as ações realizadas
      | Incluiu o sinistro                 |
      | Incluiu um veículo de terceiro     |
      | Alterou informações de liberação   |
      | Incluiu um responsável             |
      | Incluiu um recurso material        |
      | Incluiu uma turma                  |
      | CCO Via concluiu seu preenchimento |
      | Alterou o sinistro                 |
      | Alterou informações de liberação   |
      | Recusou o sinistro                 |
      | Reabriu o sinistro                 |
      | Aceitou o sinistro                 |
      | Alterou o sinistro                 |
