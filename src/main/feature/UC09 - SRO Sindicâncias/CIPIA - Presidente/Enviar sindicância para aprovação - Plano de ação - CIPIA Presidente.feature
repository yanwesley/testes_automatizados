#language: pt

@SRO @CHANGE @UC09 @WEB @SPRINT10 @ENVIARAPROVACAO
Funcionalidade: UC09 - SRO Sindicâncias - Enviar sindicância para aprovação - Plano de ação - CIPIA Presidente

  Cenário: CT01 - Enviar sindicância para aprovação via SRO
    Quando que eu esteja logado com usuário SRO
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    E acesso a TAB Sindicâncias
    E ordeno a sindicancia por ordem decrescente
    Mas valido que o status da sindicância é "Aberta"
    E clicar no botão Editar da sindicancia
    E clico no side-menu Plano de ação
    Quando eu clicar no botão ENVIAR PARA APROVAÇÃO
    Entao deve exibir o modal de confirmação com a mensagem "Deseja enviar a sindicância para aprovação do SRO?"
    E clico em confirmar no modal de Enviar para aprovação
    Então deve exibir a snackBar com a mensagem "Sindicância finalizada com sucesso."
    E ordeno a sindicancia por ordem decrescente
    Mas valido que o status da sindicância é "Aguardando Aprovação"

  Cenário: CT01 - Enviar sindicância para aprovação via CIPIA Presidente
    Quando que eu esteja logado com usuário CIPIA Presidente
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    E acesso a TAB Sindicâncias
    E ordeno a sindicancia por ordem decrescente
    Mas valido que o status da sindicância é "Aberta"
    E clicar no botão Editar da sindicancia
    E clico no side-menu Plano de ação
    Quando eu clicar no botão ENVIAR PARA APROVAÇÃO
    Entao deve exibir o modal de confirmação com a mensagem "Deseja enviar a sindicância para aprovação do SRO?"
    E clico em confirmar no modal de Enviar para aprovação
    Então deve exibir a snackBar com a mensagem "Sindicância finalizada com sucesso."
    E ordeno a sindicancia por ordem decrescente
    Mas valido que o status da sindicância é "Aguardando Aprovação"