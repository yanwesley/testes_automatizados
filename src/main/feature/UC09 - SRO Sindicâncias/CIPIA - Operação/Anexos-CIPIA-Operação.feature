#language: pt

@SRO @UC09 @WEB @SPRINT04 @RUN @READONLYOPERACAO
Funcionalidade: UC09 - SRO Sindicância - Anexos - CIPIA-Operações

  Cenário: CT01 - Incluindo um Anexo
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu ANEXO
    E estou na página de ANEXOS
    Dado insere 3 arquivo na tela de anexos com comentário
    Entao valida que o(s) 3 arquivo(s) foram adicionados

  Cenário: CT02 - Excluindo Anexo
    E que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Operação
    Quando clico no side-menu ANEXO
    E estou na página de ANEXOS
    E valida que o(s) 3 arquivo(s) foram adicionados
    Dado excluo 2 arquivos dos Anexos
    Entao valida que o(s) 1 arquivo(s) foram adicionados

  Cenário: CT03 - Editando um comentário do anexo
    E que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu ANEXO
    E estou na página de ANEXOS
    Dado clico no botão Editar do anexo
    E edito o comentário do anexo
    E clico no botão Salvar da tela de anexos
    Entao deve exibir a snackBar com a mensagem "A informação foi registrada com sucesso!"
    E valida que o comentário do anexo foi editado

  Cenário: CT04 - adiciono uma anexo com o perfil SRO
    E que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu ANEXO
    Dado insere 1 arquivo na tela de anexos com comentário
    Entao valida que o(s) 2 arquivo(s) foram adicionados

  Cenário: CT05 - valido a exibição no modo readonly
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Operação
    E clico no side-menu ANEXO
    Entao valida que o(s) 2 arquivo(s) foram adicionados

