#language: pt

@SRO @UC09 @WEB @SPRINT04 @CUSTOOPERACAO @READONLYOPERACAO
Funcionalidade: UC09 - SRO Sindicâncias - Custos - CIPIA-Operação

  Cenário: CT01 - Valida Exibição Menu Custos
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos

  Cenario: CT02 - Incluir custo
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de custos na linha 1

  Cenario: CT03 - Editar custo
    Entao o custo deve ser exibido na tabela de custos na linha 1
    E ao editar o custo da linha 1
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo editado deve ser exibido na tabela de custos na linha 1

  Cenario: CT04 - Excluir custo
    Quando clicar para excluir o custo da linha 1
    Então deve exibir o modal de confirmação de exclusão
    E confirmar a exclusao do custo
    Então deve exibir a snackBar com a mensagem 'Custo excluído com sucesso!'
    E não deve exibir a tabela de custo

  Cenario: CT05 - Incluir custo com perfil SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de custos na linha 1

  Cenario: CT06 - Valido a exibição em modo Read Only
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    Quando clico na TAB Operação
    E clico no side-menu CUSTOS
    E o custo deve ser exibido na tabela de custos na linha 1

