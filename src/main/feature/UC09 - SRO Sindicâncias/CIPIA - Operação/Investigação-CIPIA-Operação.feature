#language: pt

@SRO @UC09 @WEB @SPRINT05 @INVESTIGACAO @READONLYOPERACAO
Funcionalidade: UC09 - SRO Sindicância - Investigação - CIPIA-Operação

  Cenário: CT01 - Investigação - Operação
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu Investigação
    Então preencho todo o formulário de Investigação
    E clico no botão Salvar Rascunho da Investigação
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que os dados da Investigação estão conforme o preenchido

  Cenário: CT02 - Editar Investigação - Operação
    Dado que eu edite todo o formulário de Investigação
    E clico no botão Salvar Rascunho da Investigação
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que os dados da Investigação estão conforme o preenchido na edição

  Cenario: CT03 - Valido a edição como SRO
    E que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu Investigação
    Então preencho todo o formulário de Investigação
    E clico no botão Salvar Rascunho da Investigação
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que os dados da Investigação estão conforme o preenchido

  Cenário: CT04 - Editar Investigação com o Perfil CIPIA Via
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu Investigação
    Então preencho todo o formulário de Investigação
    E clico no botão Salvar Rascunho da Investigação
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que os dados da Investigação estão conforme o preenchido

  Cenario: CT05 - Valido a exibição em modo Read Only
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Operação
    E clico no side-menu Investigação
    E valido que os dados da Investigação estão conforme o preenchido na edição ReadOnly


