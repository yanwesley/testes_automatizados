#language: pt

@SRO @UC09 @WEB @SPRINT04 @CONCLUSAOOP @READONLYOPERACAO
Funcionalidade: UC09 - SRO Sindicância - Conclusão - CIPIA-Operação

  Cenário: CT01 - Inserindo uma conclusão
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu CONCLUSÃO
    E estou na página de CONCLUSÃO
    Quando preencho o campo de Conclusão
    E clico no botão Salvar da Conclusão
    E deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"

  Cenario: CT02 - Edito a conclusão com o perfil SRO
    E que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu CONCLUSÃO
    E estou na página de CONCLUSÃO
    Quando preencho o campo de Conclusão
    E clico no botão Salvar da Conclusão
    E deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"

  Cenario: CT03 - Concluir a conclusão
    E que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu CONCLUSÃO
    Entao valido que a conclusão inserida está sendo apresentada
    Quando clico no botão Salvar e Concluir
    Entao deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"
    E deve exibir a pagina de listagem de Sindicâncias
    Quando lista de sindicancia ordenado por ordem decrescente
    Entao valida o ícone atualizado do status da sindicancia

  Cenario: CT04 - Valido a exibição em modo Read Only
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    Quando clico na TAB Operação
    E clico no side-menu CONCLUSÃO
    Entao valido que a conclusão inserida está sendo apresentada no modo readonly

