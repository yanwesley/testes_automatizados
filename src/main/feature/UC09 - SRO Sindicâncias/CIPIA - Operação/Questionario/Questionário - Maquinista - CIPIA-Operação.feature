#language: pt

@SRO @UC09 @WEB @SPRINT05 @QUESTIONARIO_MAQUINISTA @READONLYOPERACAO
Funcionalidade: UC09 - SRO Sindicância - Questionário Maquinista - CIPIA-Operação

  Cenário: CT01 - Preencher informações do Questionário Maquinista - Operação
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu Questionário
    E clico no side-sub-menu Maquinista
    Entao deve exibir a página de Informações do Maquinista
    E ao preencher os dados de Informações do Maquinista
    E ao preencher os dados do QUESTIONÁRIO MAQUINISTA
    E ao preencher os dados do QUESTIONÁRIO CHECK-LIST ACIDENTE
    E clico no botão Salvar Rascunho do Questionário do Maquinista
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que os dados do Questionario do Maquinista estão conforme o preenchido

  Cenário: CT02 - Editar informações do Questionário Maquinista - Operação
    Quando eu editar os dados de Informações do Maquinista
    E ao editar os dados do QUESTIONÁRIO MAQUINISTA
    E ao editar os dados do QUESTIONÁRIO CHECK-LIST ACIDENTE
    E clico no botão Salvar do Questionário do Maquinista
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que os dados editar do Questionario do Maquinista estão conforme o preenchido

  Cenario: CT03 - Valido a edição como SRO
    E que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu Questionário
    E clico no side-sub-menu Maquinista
    Entao deve exibir a página de Informações do Maquinista
    E ao preencher os dados do QUESTIONÁRIO MAQUINISTA
    E ao preencher os dados do QUESTIONÁRIO CHECK-LIST ACIDENTE
    E clico no botão Salvar do Questionário do Maquinista
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que os dados do Questionario do Maquinista estão conforme o preenchido

  Cenario: CT04 - Valido a exibição em modo Read Only
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu Questionário
    E clico no side-sub-menu Maquinista
    E valido que os dados do Questionario do Maquinista estão conforme o preenchido no modo readonly