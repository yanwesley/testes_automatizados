#language: pt

@SRO @UC09 @WEB @SPRINT05 @ANALISEVIAGEM @READONLYOPERACAO
Funcionalidade: UC09 - SRO Sindicância - Análise de Viagem - CIPIA-Operações

  Cenário: CT01 - Justificativa
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Operação
    E clico no side-menu ANÁLISE DE VIAGEM
    E estou na página de ANÁLISE DE VIAGEM
    Dado clico no link para preencher a JUSTIFICATIVA
    E preencho os dados do modal de JUSTIFICATIVA
    E clico em salvar modal de JUSTIFICATIVA
    E valida que a justificativa esta preenchida
    Quando clico no botão Salvar Informações do Análise da viagem
    Entao deve exibir a snackBar com a mensagem "Justificativa salva com sucesso"
    E clico no botão Excluir Justificativa
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "Justificativa excluida com sucesso"
    E o botão Salvar Informações Análise da viagem deve estar disabled

  Cenário: CT02 - Incluindo uma nova Análise de Viagem
    Dado que incluo uma nova Análise de viagem
    Quando clico no botão Salvar Informações do Análise da viagem
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso!"

  Cenário: CT03 - Validando Análise de Viagem preenchida
    Dado validando Análise de Viagem preenchida

  Cenario: CT04 - Valido a exibição em modo Read Only
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Operação
    E valido a exibição da análise de viagem em modo Read Only


