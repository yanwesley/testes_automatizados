#language: pt

@SRO @UC09 @WEB @SPRINT03 @SI
Funcionalidade: UC09 - SRO Sindicâncias - Visão registros de sindicâncias Coleta de Dados - CIPIA Via

  Contexto:
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias

  Cenário: CT01 - Valida quantidade de dias aberto
    Dado que tenha 1 registro na tabela de registros
    Então eu valido que os Dias em aberto da linha 1 está calculado certo

  Cenario: CT02 - Valida filtro por texto com resultado válidos
    Quando eu busco pelo texto "ASD"
    Entao deve ser exibido os registros que contenham o texto "ASD"

  Cenario: CT03 - Valida filtro por texto sem resultados
    Quando eu busco pelo texto "Inflação"
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado."

  Cenario: CT04 - Valida filtro por data inicial
    Quando filtro por data inicial "01/08/2019"
    Entao deve exibir apenas os registros abertos a partir de "01/08/2019"

  Cenario: CT05 - Valida filtro por data final
    Quando filtro por data final "01/08/2019"
    Entao deve exibir apenas os registros abertos até "01/08/2019"

  Cenario: CT06 - Valida filtro por data final e inicial sem resultados
    Quando filtro por data final "01/07/2018" e data final "05/07/2018"
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado."

  Cenario: CT07 - Valida ação do botão de remover filtro
    Quando eu remover o filtro selecionado
    Entao deve exibir os resultados dos últimos três meses
    E o botão filtrar deve estar com a cor "filled"