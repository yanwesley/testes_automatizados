#language: pt

@SRO @WEB @UC09 @SPRINT12 @FINANCEIROMECANICA
Funcionalidade: UC09 - Sindicância - Financeiro Mecânica

  Cenário: CT01 - Preenche e valida preenchimento Financeiro Mecânica - Financeiro
    Dado que eu esteja logado com usuário Financeiro
    E que eu crie um novo sinistro sem veiculos via API
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Então valido que o menu Financeiro está sendo exibido
    E clico no side-menu MECÂNICA
    Então preencho o numero da ordem de serviço
    E adicionar RC
    E adicionar PEDIDO
    E clico no botão salvar informações do Financeiro
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido se os dados do financeiro persistiram

  Cenário: CT02 - Editar e valida preenchimento Financeiro Mecânica - Financeiro
    E clico no side-menu MECÂNICA
    Então edito o numero da ordem de serviço
    E edito a RC
    E edito o PEDIDO
    E clico no botão salvar informações do Financeiro
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido se os dados editado do financeiro persistiram

  Cenario: CT03 - Excluir RC e Pedido Financeiro Mecânica - Financeiro
    E clico no side-menu MECÂNICA
    E excluo a RC
    E excluo o PEDIDO
    E clico no botão salvar informações do Financeiro
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido se os dados do financeiro foram excluidos
