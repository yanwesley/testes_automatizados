#language: pt

@SRO @WEB @UC09 @SPRINT12 @FINANCEIRO
Funcionalidade: UC09 - Sindicância - Financeiro

  Cenário: CT01 - Valida Exibição Menu Financeiro - SRO
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro está sendo exibido

  Cenário: CT02 - Valida Exibição Menu Financeiro - Financeiro
    E que eu esteja logado com usuário Financeiro
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro está sendo exibido

  Cenário: CT03 - Valida Exibição Menu Financeiro - CIPIA MATERIAL RODANTE
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro não está sendo exibido

  Cenário: CT04 - Valida Exibição Menu Financeiro - CIPIA OPERAÇÃO
    E que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro não está sendo exibido

  Cenário: CT05 - Valida Exibição Menu Financeiro - CIPIA PRESIDENTE
    E que eu esteja logado com usuário CIPIA Presidente
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro não está sendo exibido

  Cenário: CT06 - Valida Exibição Menu Financeiro - CIPIA TÉCNICA
    E que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro não está sendo exibido

  Cenário: CT07 - Valida Exibição Menu Financeiro - CIPIA VIA
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro não está sendo exibido

  Cenário: CT08 - Valida Exibição Menu Financeiro - MEIO AMBIENTE
    E que eu esteja logado com usuário Meio Ambiente
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro não está sendo exibido

  Cenário: CT09 - Valida Exibição Menu Financeiro - SEGURANÇA PATRIMONIAL
    E que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro não está sendo exibido

  Cenário: CT10 - Valida Exibição Menu Financeiro - SST
    E que eu esteja logado com usuário SST
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro não está sendo exibido

  Cenário: CT11 - Valida Exibição Menu Financeiro - TO
    E que eu esteja logado com usuário TO
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro não está sendo exibido