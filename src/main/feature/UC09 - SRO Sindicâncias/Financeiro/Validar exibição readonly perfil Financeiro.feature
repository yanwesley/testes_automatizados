#language: pt

@SRO @WEB @UC09 @SPRINT12 @FINANCEIRO @FINANCEIRO_READONLY
Funcionalidade: UC09 - SRO - Sindicância - Validar exibição readonly perfil Financeiro

  Cenário: CT01 - Valida visualização readonly para perfil Financeiro
    Dado que eu crie um novo sinistro sem veiculos via API
    E adiciono uma LOCOMOTIVA via API
    E adiciono um VAGÃO via API
    E que eu esteja logado com usuário Financeiro
    E acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E valido que o menu Financeiro está sendo exibido
    Então clico na TAB Via
    E valido que a Dados Via está no modo readonly
    Então clico na TAB Material Rodante
    E clico no side-menu VAGÕES
    Então valido que Vagoes está no modo readonly
    Então clico na TAB Operação
    E valido que a Analise de Viagem está no modo readonly
    Então clico na TAB Segurança Patrimonial
    E clico no side-menu sobre o local
    E valido que Sobre o Local está no modo readonly
    Então clico na TAB Outros
    E valido que o Custo Outros está no modo readonly