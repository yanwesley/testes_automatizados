#language: pt

@SRO @UC09 @WEB @SPRINT06 @ATUALIZARSTATUS
Funcionalidade: UC09 - SRO Sindicâncias - Listagem análise técnica - Atualizar status área ao salvar

  Cenário: CT01 - Valido as informações de conclusão
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E ordeno a sindicancia por ordem decrescente
    Entao eu valido o status conclusão não iniciado

  Cenário: CT02 - Insiro Custos CIPIA VIA
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo de Materiais
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de Materiais na linha 1

  Cenário: CT03 - Insiro depoimento do CIPIA MATERIAL RODANTE
    Dado que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu DEPOIMENTO
    E estou na página de DEPOIMENTO
    Dado insere 3 arquivo na tela de depoimento
    E insiro uma observação na tela de depoimento
    E clico em Salvar Informações da tela de depoimento
    Entao deve exibir a snackBar com a mensagem "Depoimento salvo com sucesso"
    E valida que os 3 arquivos na tela de depoimento foram adicionados
    E valida a observação inserida na tela de depoimento

  Cenário: CT04 - Insiro custo da CIPIA OPERAÇÃO
    Dado que eu esteja logado com usuário CIPIA Operação
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de custos na linha 1

  Cenário: CT05 - Insiro custo da SEGURANÇA PATRIMONIAL
    Dado que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de custos na linha 1

  Cenário: CT06 - Valido o status iniciado
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E ordeno a sindicancia por ordem decrescente
    Entao eu valido o status iniciado







