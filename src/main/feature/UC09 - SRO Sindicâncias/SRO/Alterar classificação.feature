#language: pt

@SRO @UC09 @WEB @SPRINT06 @SRO @ALTERARCLASSIFICACAO @REGRESSION06
Funcionalidade: UC09 - SRO Sindicâncias - SRO - Alterar Classificação

  Contexto:
    Dado que eu crie um novo sinistro sem veiculos via API

  Delineacao do Cenario:  <CT> - Alterar a classificação para <classificação>
    Dado que eu esteja logado com usuário SRO
    Quando acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E altero a classificação da sindicância para "<classificação>"
    Entao deve exibir a snackBar com a mensagem "A classificação foi alterada com sucesso."
    E deve exibir a classificação "<classificação>" no header da sindicância
    Entao acesso a TAB Sindicâncias
    E lista de sindicancia ordenado por ordem decrescente
    Entao valido que a classificação é "<classificação>" na listagem sindicância 1

    Exemplos:
      | CT   | classificação |
      | CT01 | OCORRÊNCIA    |
      | CT02 | ACIDENTE      |