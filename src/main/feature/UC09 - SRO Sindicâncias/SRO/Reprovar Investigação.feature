#language: pt

@SRO @UC25 @WEB @SPRINT07 @REPROVARSINDICÂNCIA
Funcionalidade: UC25 - SRO Sindicância - Reprovar Sindicância

  Contexto:
    Quando que eu esteja logado com usuário SRO
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    E acesso a TAB Sindicâncias
    E ordeno a sindicancia por ordem decrescente
    Mas valido que o status da sindicância é "Aberta"
    E clicar no botão Editar da sindicancia
    E valido que estou na página relatório final
    E clico no side-menu Plano de ação
    Quando eu clicar no botão ENVIAR PARA APROVAÇÃO
    Entao deve exibir o modal de confirmação com a mensagem "Deseja enviar a sindicância para aprovação do SRO?"
    E clico em confirmar no modal de Enviar para aprovação
    Então deve exibir a snackBar com a mensagem "Sindicância finalizada com sucesso."
    E ordeno a sindicancia por ordem decrescente
    Mas valido que o status da sindicância é "Aguardando Aprovação"
    E clicar no botão Editar da sindicancia
    E valido que estou na página relatório final

  Cenário: CT01 - Realizo reprovação da investigação
    Dado que clique no botão reprovar sindicância
    E seleciono reprovar investigação
    Entao escolho uma ou mais áreas a reprovar
    E preencho a justificativa de reprovação da investigação
    E insiro um arquivo para reprovação
    E Confirmo a reprovação
    Entao deve exibir a snackBar com a mensagem "Sindicância reprovada com sucesso."
    E ordeno a sindicancia por ordem decrescente
    Entao valido que o status da sindicância é "Reprovada"

  Cenário: CT02 - Realizo a reprovação do plano de ação completo
    Dado que clique no botão reprovar sindicância
    E seleciono reprovar plano de ação completo
    E preencho a justificativa de reprovação completa
    E insiro um arquivo para reprovação
    E Confirmo a reprovação
    Entao deve exibir a snackBar com a mensagem "Sindicância reprovada com sucesso."
    E ordeno a sindicancia por ordem decrescente
    Entao valido que o status da sindicância é "Reprovada"

  Cenário: CT03 - Realizo a reprovação do plano de ação parcialmente
    Dado que clique no botão reprovar sindicância
    E seleciono reprovar plano de ação parcialmente
    Entao seleciono a ação a reprovar
    E preencho a justificativa de reprovação parcial
    E insiro um arquivo para reprovação
    E Confirmo a reprovação
    Entao deve exibir a snackBar com a mensagem "Sindicância reprovada com sucesso."
    E ordeno a sindicancia por ordem decrescente
    Entao valido que o status da sindicância é "Reprovada"