#language: pt

@SRO @UC25 @WEB @SPRINT07 @APROVARSINDICANCIASRO
Funcionalidade: UC25 - SRO Sindicância - Aprovar Sindicância

  Contexto:
    Quando que eu esteja logado com usuário SRO
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    E acesso a TAB Sindicâncias
    E ordeno a sindicancia por ordem decrescente
    Mas valido que o status da sindicância é "Aberta"
    E clicar no botão Editar da sindicancia
    E clico no side-menu Plano de ação
    Quando eu clicar no botão ENVIAR PARA APROVAÇÃO
    Entao deve exibir o modal de confirmação com a mensagem "Deseja enviar a sindicância para aprovação do SRO?"
    E clico em confirmar no modal de Enviar para aprovação
    Então deve exibir a snackBar com a mensagem "Sindicância finalizada com sucesso."
    E ordeno a sindicancia por ordem decrescente
    Mas valido que o status da sindicância é "Aguardando Aprovação"
    E clicar no botão Editar da sindicancia
    E valido que estou na página relatório final

  Cenário: CT01 - Realizo a aprovação da sindicância via SRO
    Dado que clique no botão aprovar sindicância
    E aprovo a investigação e o plano de ação
    E escolho que a ação não será pública
    E não desdobro a ação
    Entao valido as informações de confirmação
      | Aprovação da investigação e do plano de ação | Nenhuma ação pública | Nenhuma ação desdobrada |
    E confirmo a aprovação da sindicância
    Entao deve exibir a snackBar com a mensagem "Sindicância aprovada com sucesso"
    E vou para aba finalizados
    E ordeno a sindicancia por ordem decrescente
    Então valido que a sindicancia está finalizada

  Cenário: CT02 - Realizo a aprovação da sindicância com ação pública e ação desdobrada
    Dado que clique no botão aprovar sindicância
    E aprovo a investigação e o plano de ação
    E escolho que a ação será pública
    E desdobro a ação
    E valido que o responsável foi atribuido
    Entao valido as informações de confirmação
      | Aprovação da investigação e do plano de ação | 1 ação pública | 1 ação desdobrada |
    E confirmo a aprovação da sindicância
    Entao deve exibir a snackBar com a mensagem "Sindicância aprovada com sucesso"
    E vou para aba finalizados
    E ordeno a sindicancia por ordem decrescente
    Então valido que a sindicancia está finalizada