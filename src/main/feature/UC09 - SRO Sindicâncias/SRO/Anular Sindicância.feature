#language: pt

@SRO @UC25 @WEB @SPRINT07 @ANULARSINDICANCIA
Funcionalidade: UC25 - SRO Sindicância - Anular Sindicância

  Cenario: CT01- Realizo a anulação da sindicância
    Dado que eu crie um novo sinistro sem veiculos via API
    Quando que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    E ordeno a sindicancia por ordem decrescente
    Mas valido que o status da sindicância é "Aberta"
    E clicar no botão Editar da sindicancia
    E valido que estou na página relatório final
    Dado que clique no botão anular sindicância
    E preencho a justificativa da anulação
    Entao clico no botão confirmar anulação
    Entao deve exibir a snackBar com a mensagem "Sindicância anulada com sucesso!"
    E vou para aba finalizados
    Entao ordeno a sindicancia por ordem decrescente
    E vou em detalhes da sindicância anulada
    E valido que a sindicancia está anulada




