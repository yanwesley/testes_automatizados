#language: pt

@SRO @UC09 @WEB @SPRINT06 @SRO @ALTERARCLASSE
Funcionalidade: UC09 - SRO Sindicâncias - SRO - Alterar classe via

  Contexto:
    Dado que eu crie um novo sinistro sem veiculos via API

  Delineacao do Cenario: <CT> - Alterar a classe para <classe>
    Dado que eu esteja logado com usuário SRO
    Quando acesso a TAB Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E altero a classe para "<classe>"
    Entao deve exibir a snackBar com a mensagem "A classe de Via foi alterada com sucesso."
    E deve exibir a classe "<classe>" no header da sindicância

    Exemplos:
      | CT   | classe |
      | CT01 | A      |
      | CT02 | B      |
      | CT03 | C      |
      | CT04 | D      |
