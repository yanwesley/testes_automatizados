#language: pt

@SRO @UC09 @WEB @SPRINT04 @DEPOCIPIAVIA
Funcionalidade: UC09 - SRO Sindicância - CIPIA-Via Depoimento

  Cenário: CT01 - Novo depoimento
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu DEPOIMENTO
    E estou na página de DEPOIMENTO
    Dado insere 3 arquivo na tela de depoimento
    E insiro uma observação na tela de depoimento
    E clico em Salvar Informações da tela de depoimento
    Entao deve exibir a snackBar com a mensagem "Depoimento salvo com sucesso"
    E valida que os 3 arquivos na tela de depoimento foram adicionados
    E valida a observação inserida na tela de depoimento
    Quando excluo 2 arquivos da tela de depoimento
    Entao valida que os 1 arquivos na tela de depoimento foram adicionados
    E clico no botão Salvar e Avançar da tela de Depoimento
    Entao estou na página de CONCLUSÃO

  Cenario: CT02 - Valido a exibição em modo Read Only
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na tab VIA
    E clico no side-menu DEPOIMENTO
    E estou na página de DEPOIMENTO
    Entao valida que os 1 arquivos na tela de depoimento foram adicionados


