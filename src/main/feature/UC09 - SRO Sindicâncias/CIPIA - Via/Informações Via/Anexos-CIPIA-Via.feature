#language: pt

@SRO @UC09 @WEB @SPRINT04 @ANXCIPIAVIA
Funcionalidade: UC09 - SRO Sindicância - CIPIA-Via Anexos

  Cenário: CT01 - Incluindo um Anexo
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu ANEXO
    E estou na página de ANEXOS
    Dado insere 3 arquivo na tela de anexos com comentário
    Entao valida que o(s) 3 arquivo(s) foram adicionados

  Cenário: CT02 - Excluindo Anexo
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu ANEXO
    E estou na página de ANEXOS
    E valida que o(s) 3 arquivo(s) foram adicionados
    Dado excluo 2 arquivos dos Anexos
    Entao valida que o(s) 1 arquivo(s) foram adicionados

  Cenário: CT03 - Editando um comentário do anexo
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu ANEXO
    E estou na página de ANEXOS
    Dado clico no botão Editar do anexo
    E edito o comentário do anexo
    E clico no botão Salvar da tela de anexos
    Entao deve exibir a snackBar com a mensagem "A informação foi registrada com sucesso!"
    E valida que o comentário do anexo foi editado

  Cenario: CT04 - Adiciono um anexo com o perfil SRO
    E que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na tab VIA
    E clico no side-menu ANEXO
    E estou na página de ANEXOS
    Dado insere 1 arquivo na tela de anexos com comentário

  Cenario: CT05 - Valido o modo ReadOnly
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    Quando clico na tab VIA
    E clico no side-menu ANEXO
    Entao valida que o(s) 2 arquivo(s) foram adicionados

