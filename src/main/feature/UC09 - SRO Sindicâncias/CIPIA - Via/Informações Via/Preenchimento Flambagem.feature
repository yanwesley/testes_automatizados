#language: pt

@SRO @UC09 @WEB @SPRINT04 @FLAMBAGEM
Funcionalidade: UC09 - SRO Sindicância - Preenchimento da Aba de Flambagem

  Cenário: CT01 - Validação dos campos da Aba de Flambagem da Sindicância
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu FLAMBAGEM
    Entao deve exibir a página de preenchimento da Flambagem
    E valida a regra da seção LOCAL DA FLAMBAGEM
    E valida a regra da seção HISTÓRICO DA FLAMBAGEM
    E valida a regra da seção HISTÓRICO DO ACIDENTE

  Cenário: CT02 - Preenchimento da Aba de Flambagem da Sindicância
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu FLAMBAGEM
    Entao deve exibir a página de preenchimento da Flambagem
    Dado que preencho a Aba Flambagem
    E clico no botão Salvar Informações Flambagem
    E deve exibir a snackBar com a mensagem "Informação salva com sucesso."
    Entao valido os dados preenchido na tela de Flambagem

  Cenário: CT03 - Valido o modo ReadOnly da aba Flambagem
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na tab VIA
    E clico no side-menu FLAMBAGEM
    Entao deve exibir a página de preenchimento da Flambagem
    Entao valido os dados preenchido na tela de Flambagem

  Cenário: CT04 - Edito os dados da via com o perfil Segurança Patrimonial
    Dado que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico na tab VIA
    E clico no side-menu FLAMBAGEM
    Entao deve exibir a página de preenchimento da Flambagem
    Dado que preencho a Aba Flambagem
    E clico no botão Salvar Informações Flambagem
    E deve exibir a snackBar com a mensagem "Informação salva com sucesso."
    Entao valido os dados preenchido na tela de Flambagem

  Cenário: CT04 - Edito os dados da via com o perfil SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico na tab VIA
    E clico no side-menu FLAMBAGEM
    Entao deve exibir a página de preenchimento da Flambagem
    Dado que preencho a Aba Flambagem
    E clico no botão Salvar Informações Flambagem
    E deve exibir a snackBar com a mensagem "Informação salva com sucesso."
    Entao valido os dados preenchido na tela de Flambagem