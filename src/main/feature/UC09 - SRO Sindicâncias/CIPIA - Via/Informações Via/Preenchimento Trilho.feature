#language: pt

@SRO @UC09 @WEB @SPRINT04 @TRILHO
Funcionalidade: UC09 - SRO Sindicâncias - Preenchimento Trilho - CIPIA Via

  Cenário: CT01 - Preenche Dados Trilho da Sindicância
    Dado que eu crie um novo sinistro sem veiculos via API
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu TRILHO
    E preenche os dados do Trilho
    Quando clico no botão Salvar Informações Trilho
    Entao deve exibir a snackBar com a mensagem "Informações do trilho salvo com sucesso."
    E valida que os valores preenchidos sobre o trilho persistiram

  Cenário: CT02 - Editar Dados Trilho da Sindicância
    E edito os dados do Trilho
    Quando clico no botão Salvar Informações Trilho
    Entao deve exibir a snackBar com a mensagem "Informações do trilho salvo com sucesso."
    E valida que os valores editado sobre o trilho persistiram

  Cenário: CT03 - Editar Dados com o perfil SRO
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao clico na tab VIA
    E clico no side-menu TRILHO
    E preenche os dados do Trilho
    Quando clico no botão Salvar Informações Trilho
    Entao deve exibir a snackBar com a mensagem "Informações do trilho salvo com sucesso."
    E valida que os valores preenchidos sobre o trilho persistiram

  Cenário: CT04 - Valido o modo ReadOnly dados trilho
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    Entao clico na tab VIA
    E clico no side-menu TRILHO
    E valida que os valores preenchidos sobre o trilho persistiram



