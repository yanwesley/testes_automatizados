#language: pt

@SRO @UC09 @WEB @SPRINT04 @CUSTOUTROSVIA @CUSTO
Funcionalidade: UC09 - SRO Sindicâncias - Preenchimento Custos Outros - CIPIA Via

  Cenário: CT01 - Valida Exibição Menu Custos
    Dado que eu crie um novo sinistro sem veiculos via API
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico na tab VIA
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos

  Cenario: CT02 - Incluir custo do tipo Outros
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo de Outros
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de Outros na linha 1

  Cenario: CT03 - Editar custo tipo Outros
    Entao o custo deve ser exibido na tabela de Outros na linha 1
    E ao editar o custo de Outros da linha 1
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo editado deve ser exibido na tabela de Outros na linha 1

  Cenario: CT04 - Excluir custo tipo Outros
    Quando clicar para excluir o custo Outros da linha 1
    Então deve exibir o modal de confirmação de exclusão
    E confirmar a exclusao do custo
    Então deve exibir a snackBar com a mensagem 'Custo excluído com sucesso!'
    E não deve exibir a tabela de custo Outros

  Cenario: CT05 - Incluir custo tipo Outros - SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na tab VIA
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos
    E ao optar por adicionar Custos
    Entao deve exibir o botão para Incluir custo
    E ao adicionar um novo custo de Outros
    E clicar no botão Salvar Modal de custos
    Então deve exibir a snackBar com a mensagem 'Custo salvo com sucesso!'
    E o custo deve ser exibido na tabela de Outros na linha 1

  Cenario: CT06 - valido ReadOnly custo de tipo Outros
    Dado que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na tab VIA
    E clico no side-menu CUSTOS
    Entao deve exibir a opção de selecionar se houve custos
    E o custo deve ser exibido na tabela de Outros na linha 1