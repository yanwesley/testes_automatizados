#language: pt

@SRO @UC09 @WEB @SPRINT04 @REGFOT

Funcionalidade: UC09 - SRO Sindicância - CIPIA-Via Registro fotográfico

  Cenário: CT01 - Registro fotográfico
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu REGISTRO FOTOGRÁFICO
    E estou na página de REGISTRO FOTOGRÁFICO
    Entao valida os itens da tela de Registro fotográfico

  Cenario: CT02 - Adicionando imagens na tela de Registro fotográfico
    Dado insiro as imagens na tela de registro fotográfico
    Entao valido que as imagens foram inseridas na tela de registro fotográfico

  Cenario: CT03 - Excluo imagens na tela de Registro fotográfico
    Dado excluo as imagens na tela de registro fotográfico
    Entao valida os itens da tela de Registro fotográfico
    E clica no botão Avançar
    E estou na página de DEPOIMENTO

  Cenario: CT04 - Registro fotográfico SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na tab VIA
    E clico no side-menu REGISTRO FOTOGRÁFICO
    E estou na página de REGISTRO FOTOGRÁFICO
    Dado insiro as imagens na tela de registro fotográfico
    Entao valido que as imagens foram inseridas na tela de registro fotográfico

  Cenario: CT05 - Valido o modo ReadOnly registro fotográfico
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na tab VIA
    E clico no side-menu REGISTRO FOTOGRÁFICO
    E estou na página de REGISTRO FOTOGRÁFICO
    Entao valido que as imagens foram inseridas na tela de registro fotográfico
