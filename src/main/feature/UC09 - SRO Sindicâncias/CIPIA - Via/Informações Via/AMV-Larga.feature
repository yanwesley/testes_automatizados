#language: pt

@SRO @UC09 @WEB @SPRINT05 @AMVTEST @AMVLARGA
Funcionalidade: UC09 - SRO Sindicância - CIPIA-Via AMV-Larga

  Cenário: CT01 - Preenchendo AMV-Larga
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu AMV
    E estou na página de AMV
    Entao preencho a página de AMV-Larga
    Quando clico no botão Salvar Informações AMV
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    E valida o preenchimento da página de AMV-Larga

  Cenario: CT02 - Valido o modo ReadOnly AMV-larga
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    Entao clico na tab VIA
    E clico no side-menu AMV
    E estou na página de AMV
    E valida o preenchimento da página de AMV-Larga

  Cenario: CT03 - Edito AMV-larga com o perfil SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao clico na tab VIA
    E clico no side-menu AMV
    E estou na página de AMV
    Entao preencho a página de AMV-Larga
    Quando clico no botão Salvar Informações AMV
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    E valida o preenchimento da página de AMV-Larga

  Cenário: CT04 - Validando campos obrigatórios AMV-Larga
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu AMV
    E estou na página de AMV
    Entao preencho a página de AMV-Larga parcialmente para validar os campos obrigatórios
    Quando clico no botão Salvar Informações AMV
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."