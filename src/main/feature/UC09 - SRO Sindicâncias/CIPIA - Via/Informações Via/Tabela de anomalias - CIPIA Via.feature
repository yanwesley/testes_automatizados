#language: pt

@SRO @UC09 @WEB @SPRINT03 @TA
Funcionalidade: UC09 - SRO Sindicâncias - Tabela de anomalias - Cipia Via

  Cenário: CT01 -Preencher Tabela de anomalias - Cipia Via
    Dado que eu crie um novo sinistro sem veiculos via API
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E preenche a aba Dados Via da sindicância
    Quando clico no botão Salvar Informações Dados Via
    Entao deve exibir a snackBar com a mensagem "Informação salva com sucesso."
    E clico no botão Preencher em tela cheia
    E clico no botão Todas as colunas
    Então deve exibir o modal de preenchimento de tabela de anomalias
    E ao preencher todo o modal de anomalias
    E salvar as tabela de anomalias
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    E deve preencher automaticamente toda a tabela de anomalias
