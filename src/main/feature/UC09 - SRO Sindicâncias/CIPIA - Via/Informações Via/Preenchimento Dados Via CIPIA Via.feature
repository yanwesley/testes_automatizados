#language: pt

@SRO @UC09 @WEB @SPRINT03 @DADOSVIA
Funcionalidade: UC09 - SRO Sindicâncias - Preenchimento Dados Via CIPIA Via

  Cenário: CT01 - Preenche Dados Via da Sindicância
    Dado que eu crie um novo sinistro sem veiculos via API
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E preenche a aba Dados Via da sindicância
    Quando clico no botão Salvar Informações Dados Via
    Entao deve exibir a snackBar com a mensagem "Informação salva com sucesso."
    E valida que os valores preenchidos na opção Dados Via persistiram

  Cenário: CT02 - Valido o modo ReadOnly do dados Via
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na tab VIA
    E valida que os valores preenchidos na opção Dados Via persistiram

  Cenário: CT03 - Edito os dados da via com o perfil SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico na tab VIA
    E preenche a aba Dados Via da sindicância
    Quando clico no botão Salvar Informações Dados Via
    Entao deve exibir a snackBar com a mensagem "Informação salva com sucesso."
    E valida que os valores preenchidos na opção Dados Via persistiram

  Cenário: CT04 - Valida preenchimento impossivel medir
    Dado que eu crie um novo sinistro sem veiculos via API
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no link para preencher a JUSTIFICATIVA
    E preencho os dados do modal de JUSTIFICATIVA
    E clico em salvar modal de JUSTIFICATIVA
    Quando clico no botão Salvar Informações Dados Via
    Entao deve exibir a snackBar com a mensagem "Informação salva com sucesso."
    E valida que a justificativa esta preenchida

  Cenário: CT05 - Excluir justificativa impossivel medir
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no botão Excluir Justificativa
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "Justificativa excluida com sucesso"
    E o botão Salvar Informações Dados Via deve estar disabled

