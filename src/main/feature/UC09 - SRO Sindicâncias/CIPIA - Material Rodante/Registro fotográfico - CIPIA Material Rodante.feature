#language: pt

@SRO @UC09 @WEB @SPRINT05 @RFMR
Funcionalidade: UC09 - SRO Sindicância - Registro fotográfico - CIPIA-Material Rodante

  Cenário: CT01 - Registro fotográfico
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Entao deve exibir a página de inclusão de informações do acidente
    E clico no side-menu REGISTRO FOTOGRÁFICO
    Entao valida os itens da tela de Registro fotográfico do perfil CIPIA Material Rodante

  Cenario: CT02 - Adicionando imagens na tela de Registro fotográfico
    Dado insiro as imagens na tela de registro fotográfico para CIPIA Material Rodante
    Entao valido que as imagens foram inseridas na tela de registro fotográfico para CIPIA Material Rodante

  Cenario: CT03 - Excluo imagens na tela de Registro fotográfico
    Dado excluo as imagens na tela de registro fotográfico do perfil CIPIA Material Rodante
    Entao valida os itens da tela de Registro fotográfico do perfil CIPIA Material Rodante foram excluídos

  Cenario: CT04 - Adicionar imagem adicional tela de Registro fotográfico
    Dado que insiro 1 imagem(s) adicional na tela de registro fotográfico para CIPIA Material Rodante
    Entao valida que a 1 imagem(s) adicional do perfil CIPIA Material Rodante foi adicionadas

  Cenario: CT05 - Registro fotográfico SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico na TAB Material Rodante
    E clico no side-menu REGISTRO FOTOGRÁFICO
    E estou na página de REGISTRO FOTOGRÁFICO
    Dado insiro as imagens na tela de registro fotográfico para CIPIA Material Rodante
    Entao valido que as imagens foram inseridas na tela de registro fotográfico para CIPIA Material Rodante

  Cenario: CT06 - Valido o modo ReadOnly registro fotográfico
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    E clico na TAB Material Rodante
    E clico no side-menu REGISTRO FOTOGRÁFICO
    E estou na página de REGISTRO FOTOGRÁFICO
    Entao valido que as imagens foram inseridas na tela de registro fotográfico para CIPIA Material Rodante

