#language: pt

@SRO @UC09 @WEB @SPRINT05 @JUSTIVAGAO @VAGAO
Funcionalidade: UC09 - SRO Sindicância - Vagão - CIPIA Material Rodante

  Cenário: CT01 - Preencher tela de vagões
    Dado que eu crie um novo sinistro sem veiculos via API
    E adiciono um VAGÃO via API
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu VAGÕES
    E estou na página de VAGÕES
    Então preencho o campos da tela do vagão descarrilado
    E clico no botão Salvar da página de Vagões
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso!"
    E valido que os dados do vagão descarrilado foram persistido corretamente

  Cenário: CT02 - Editar tela de vagões
    Então edito os campos da tela do vagão descarrilado
    E clico no botão Salvar da página de Vagões
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso!"
    E valido que os dados editado do vagão descarrilado foram persistido corretamente

  Cenário: CT03 - Preencher tela cheia
    Dado que eu clique em preencher em tela cheia
    E preencha as medidas das rodas
    E salvo as medidas
    Entao valido que o valor preenchido das medidas das rodas persistiram

  Cenário: CT04 - Justificativa - Vagões
    Dado que eu crie um novo sinistro sem veiculos via API
    E adiciono um VAGÃO via API
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu VAGÕES
    E estou na página de VAGÕES
    Dado clico no link para preencher a JUSTIFICATIVA
    E preencho os dados do modal de JUSTIFICATIVA
    E clico em salvar modal de JUSTIFICATIVA
    Entao deve exibir a snackBar com a mensagem "Justificativa salva com sucesso"
    E valida que a justificativa esta preenchida
    E clico no botão Excluir Justificativa
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "Justificativa excluida com sucesso"
    E o botão Salvar Informações dos Vagões deve estar disabled


