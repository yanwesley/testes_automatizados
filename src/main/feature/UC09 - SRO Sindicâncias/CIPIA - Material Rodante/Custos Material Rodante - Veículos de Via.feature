#language: pt

@SRO @UC09 @WEB @SPRINT09 @CUSTO @CMR @CUSTOMATERIALRODANTE.2
Funcionalidade: UC09 - Custos Material Rodante - Veículos de Via

  Cenario: CT01 - Incluir custo
    Dado que eu crie um novo sinistro sem veiculos via API
    E adiciono um VEÍCULO DE VIA via API
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu CUSTOS
    Entao valida que existe a tabela de custos do Veículo de Via
    E clica no botão para adicionar um novo custo do Veículo de Via
    E preenche o modal de Custo do Veículo de via
    E clicar no botão salvar do modal Custo do Veículo de via
    Entao valida que o custo foi adicionado na tabela de custos do Veículo de Via
    E clico no botão Salvar Informações da tela de custos Material Rodante
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenario: CT02 - Editar custo do Veículo de Via
    Entao clico no botão editar o custo do Veículo de Via
    E edito os dados do modal de custo do Veículo de Via
    E clicar no botão salvar do modal Custo do Veículo de via
    Entao valida que o custo foi adicionado na tabela de custos do Veículo de Via Editado
    E clico no botão Salvar Informações da tela de custos Material Rodante
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenario: CT03 - Sem Custo do Veículo de Via
    E seleciono o check box Sem custos de Veículos de Via
    E clico no botão Ok do modal de Remover Custo
    Entao valida o custo zerado na tabela de custos do Veículo de Via