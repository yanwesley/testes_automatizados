#language: pt

@SRO @UC09 @WEB @SPRINT09 @CUSTO @CMR @CustosVagao
Funcionalidade: UC09 - Custos Material Rodante - Vagões

  Cenário: CT01 - Insiro um custo vagão
    Dado que eu crie um novo sinistro sem veiculos via API
    E adiciono um VAGÃO via API
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu CUSTOS
    E clico em adicionar um custo vagão
    Entao preencho os valores para o custo vagão
    Quando salvo o custo
    Entao valido os valores preenchidos para o custo vagão
    E clico em salvar o custo vagão
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenário: CT02 - Edito e excluo o custo Vagão
    Dado que clique em editar o custo vagão
    E editos os valores para o custo vagão
    Quando salvo o custo
    E valido os valores editados para o custo vagão
