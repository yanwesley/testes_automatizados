#language: pt

@SRO @UC09 @WEB @SPRINT05 @LocomotivaJu
Funcionalidade: UC09 - SRO Sindicância - Locomotiva - CIPIA-Material Rodante

  Cenário: CT01 - Justificativa - Locomotiva
    Dado que eu crie um novo sinistro sem veiculos via API
    E adiciono uma LOCOMOTIVA via API
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu LOCOMOTIVA
    E estou na página de LOCOMOTIVA
    Dado clico no link para preencher a JUSTIFICATIVA
    E preencho os dados do modal de JUSTIFICATIVA
    E clico em salvar modal de JUSTIFICATIVA
    Entao deve exibir a snackBar com a mensagem "Justificativa salva com sucesso"
    E valida que a justificativa esta preenchida
    E clico no botão Excluir Justificativa
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "Justificativa excluida com sucesso"
    E o botão Salvar Informações da Locomotiva deve estar disabled



