#language: pt

@SRO @UC09 @WEB @SPRINT05 @AMP
Funcionalidade: UC09 - SRO Sindicância - Anexos - CIPIA Material Rodante

  Cenário: CT01 - Incluindo um Anexo
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu ANEXO
    E estou na página de ANEXOS
    Dado insere 3 arquivo na tela de anexos com comentário
    Entao valida que o(s) 3 arquivo(s) foram adicionados

  Cenário: CT02 - Excluindo Anexo
    Dado excluo 2 arquivos dos Anexos
    Entao valida que o(s) 1 arquivo(s) foram adicionados

  Cenário: CT03 - Editando um comentário do anexo
    Dado clico no botão Editar do anexo
    E edito o comentário do anexo
    E clico no botão Salvar da tela de anexos
    Entao deve exibir a snackBar com a mensagem "A informação foi registrada com sucesso!"
    E valida que o comentário do anexo foi editado

  Cenário: CT03 - Editando um comentário do anexo com perfil CIPIA Via
    Dado que eu esteja logado com usuário CIPIA Via
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu ANEXO
    Entao clico no botão Editar do anexo
    E edito o comentário do anexo
    E clico no botão Salvar da tela de anexos
    Entao deve exibir a snackBar com a mensagem "A informação foi registrada com sucesso!"
    E valida que o comentário do anexo foi editado

  Cenario: CT04 - Adiciono um anexo com o perfil SRO
    E que eu esteja logado com usuário SRO
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu ANEXO
    E estou na página de ANEXOS
    Dado insere 1 arquivo na tela de anexos com comentário

  Cenario: CT05 - Valido o modo ReadOnly
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    Quando clico na TAB Material Rodante
    E clico no side-menu ANEXO
    Entao valida que o(s) 2 arquivo(s) foram adicionados