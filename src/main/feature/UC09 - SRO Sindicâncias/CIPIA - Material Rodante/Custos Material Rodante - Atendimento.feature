#language: pt

@SRO @UC09 @WEB @SPRINT09 @CUSTO @CMR @CustosAtendimento
Funcionalidade: UC09 - Custos Material Rodante - Custo atendimento

  Cenário: CT01 - Valido Custos de atendimento
    Dado que eu crie um novo sinistro sem veiculos via API
    E adiciono um VEÍCULO DE VIA via API
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu CUSTOS
    E seleciono custos de atendimento
    E clico no botão incluir custo
    Entao ao adicionar um novo custo de atendimento
    E valido que os valores do custo de atendimento persistiram

  Cenário: CT02 - Edito e removo o custo atendimento
    Entao edito este novo custo
    E valido que os valores do custo de atendimento editado persistiram
    Quando removo um custo
    E clico no botão Ok do modal de Remover Custo
    E valido que o custo editado foi removido
