#language: pt

@SRO @UC09 @WEB @SPRINT09 @CUSTO @CMR @CUSTOMATERIALRODANTE.2
Funcionalidade: UC09 - Custos Material Rodante - Locomotiva

  Cenario: CT01 - Incluir custo de Locomotiva
    Dado que eu crie um novo sinistro sem veiculos via API
    E adiciono uma LOCOMOTIVA via API
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu CUSTOS
    Entao valida que existe a tabela de custos de Locomotiva
    E clica no botão para adicionar um novo custo de Locomotiva
    E preenche o modal de Custo de Locomotiva
    E clicar no botão salvar do modal Custo de Locomotiva
    Entao valida que o custo foi adicionado na tabela de custos de Locomotiva
    E clico no botão Salvar Informações da tela de custos Material Rodante
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenario: CT02 - Editar custo da Locomotiva
    Entao clico no botão editar o custo de Locomotiva
    E edito os dados do modal de custo de Locomotiva
    E clicar no botão salvar do modal Custo de Locomotiva
    Entao valida que o custo foi adicionado na tabela de custos de Locomotiva Editado
    E clico no botão Salvar Informações da tela de custos Material Rodante
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenario: CT03 - Sem Custo da Locomotiva
    Entao seleciono o check box Sem custos de Locomotiva
    E clico no botão Ok do modal de Remover Custo
    Entao valida o custo zerado na tabela de custos do Locomotiva