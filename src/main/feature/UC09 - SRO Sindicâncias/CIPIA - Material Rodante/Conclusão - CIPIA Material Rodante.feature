#language: pt

@SRO @UC09 @WEB @SPRINT05 @CONCLUSAOMP
Funcionalidade: UC09 - SRO Sindicância - Conclusão - CIPIA Material Rodante

  Cenário: CT01 - Inserindo uma conclusão
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário Cipia Material Rodante
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    Quando clico na TAB Material Rodante
    E clico no side-menu CONCLUSÃO
    E estou na página de CONCLUSÃO
    Quando preencho o campo de Conclusão
    E clico no botão Salvar da Conclusão
    E deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"
    Entao estou na página de CONCLUSÃO
    Quando clico no side-menu ANEXO
    Entao estou na página de ANEXOS
    E clico no side-menu CONCLUSÃO
    E estou na página de CONCLUSÃO
    Entao valido que a conclusão inserida está sendo apresentada
    Quando clico no botão Salvar e Concluir
    Entao deve exibir a snackBar com a mensagem "A coleta de dados foi registrada com sucesso!"
    E deve exibir a pagina de listagem de Sindicâncias
    Quando lista de sindicancia ordenado por ordem decrescente
    Entao valida o ícone atualizado do status da sindicancia

  Cenario: CT02 - Valido a exibição em modo Read Only
    Dado que eu esteja logado com usuário CIPIA Técnica
    E acesso a TAB Sindicâncias
    Entao deve exibir a pagina de listagem de Sindicâncias
    E clicar no botão Editar da sindicancia ordenado por ordem decrescente
    E clico no botão Ficha do Acidente
    Entao deve abrir uma nova guia com a ficha do acidente
    E clico no expansion da sindicância
    Quando clico na TAB Material Rodante
    E clico no side-menu CONCLUSÃO
    Entao valido que a conclusão inserida está sendo apresentada no modo readonly