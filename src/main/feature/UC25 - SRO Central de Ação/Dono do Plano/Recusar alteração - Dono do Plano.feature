#language: pt

@SRO @UC25 @WEB @SPRINT08 @RECUSARACAOSRO
Funcionalidade: UC25 - SRO Central De Ações - Recusar pedido de alteração da ação - Dono do Plano

  Cenário: CT01 - Recusar pedido de alteração da ação - Dono do Plano
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao clicar no botão Recusar Ação
    Então seleciono a opção "Discorda da descrição" no modal de recusar ação
    E preencho os dados do modal de recusar ação
    E ao clicar em Enviar Solicitação no modal de Recusar Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E deve exibir o box com a mensagem "Aguardando retorno do solicitante."
    Entao que eu esteja logado com o Dono do Plano
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E ordeno a acao por ordem decrescente
    Então valido que o status da 1 ação é "Recusada"
    E clico no botão Editar Acao na listagem na linha 1
    Então deve exibir a página de Visualizar Ação
    E deve exibir o box com a mensagem "Solicitação de alteração por discordar da descrição."
    Então clico na tab HISTÓRICO da ação
    E valido que o pedido de solicitação está registrado
    E clico no botão Recusar Alteração
    E preencho a justificativa do modal de Reprovar alteração
    E confirmo a Recusa da ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido o historico de alteração recusada
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E ordeno a acao por ordem decrescente
    Então valido que o status da 1 ação é "Aguardando aceite"
    E clico no botão Editar Acao na listagem na linha 1
    Então deve exibir a página de Visualizar Ação
    E deve exibir o box com a mensagem "Alteração da descrição da ação negada."


