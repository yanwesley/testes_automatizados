#language: pt

@SRO @UC25 @WEB @SPRINT07 @ELABORARPAO @REGRESSION_07
Funcionalidade: UC25 - SRO Central Ação - Elaborar plano de ação - Dono do Plano

  Cenário: CT01 - Valido os detalhes do plano de ação
    Dado que eu crie um novo plano de ação via API
    Entao que eu esteja logado com o Dono do Plano
    Quando acesso a TAB Central de Ações
    E clicar no botão Editar da central de ações ordenado por ordem decrescente
    Entao deve exibir os detalhes do plano de ação na elaboração plano de ação

  Cenário: CT02 - Criar uma ação
    Dado que eu esteja na tela de Elaborar Plano de Ação
    Quando eu clico no botão Criar uma ação
    E deve exibir as informações do plano de ação no modal de "INCLUIR UMA AÇÃO"
    E ao preencher as informações iniciais
    E adicionar um responsável na ação
    Então clico no botão Salvar do Modal de Incluir Uma Ação
    E deve exibir a snackBar com a mensagem "Ação criada com sucesso."
    E a ação deve ser exibida na listagem de Elaboração de ações

  Cenário: CT03 - Editar uma ação
    Dado que eu esteja na tela de Elaborar Plano de Ação
    E que eu tenha 1 ação criada
    Quando eu clico no botão editar da ação da linha 1
    Entao deve exibir as informações do plano de ação no modal de "EDITAR AÇÃO"
    E ao editar as informações iniciais
    E remover o responsável da ação
    E clico no botão de buscar responsável
    E adicionar um novo responsável na ação
    Então clico no botão Salvar do Modal de Incluir Uma Ação
    E deve exibir a snackBar com a mensagem "Ação editada com sucesso."
    E a ação editada deve ser exibida na listagem de Elaboração de ações

  Cenário: CT04 - Excluir uma ação
    Dado que eu esteja na tela de Elaborar Plano de Ação
    E que eu tenha 1 ação criada
    Quando eu clico no botão excluir da ação da linha 1
    Entao deve exibir o modal para confirmar a exclusão
    E ao confirmar a exclusão
    Entao deve exibir a snackBar com a mensagem "Ação excluída com sucesso."
    E a ação não deve ser exibida na listagem de Elaboração de ações