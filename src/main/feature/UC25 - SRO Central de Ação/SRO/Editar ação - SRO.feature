#language: pt

@SRO @UC25 @WEB @SPRINT07 @EDITARACAOSRO @REGRESSION_07
Funcionalidade: UC25 - SRO Central De Ações - Editar ação - SRO

  Cenário: CT01 - Editar ação para aceite - SRO
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação
    E clico no botão Editar Acao
    Então deve exibir o modal de Editar ação
    E seleciono a opção Editar Ação no modal
    Então preencho a justificativa do modal de editar ação
    E edito as informações iniciais da ação
    E clico no botão confirmar do modal de editar acao
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que os dados da ação foram editado

  Cenário: CT02 - Alterar responsável da ação para aceite - SRO
    E clico no botão Editar Acao
    Então deve exibir o modal de Editar ação
    E seleciono a opção Alterar o responsável por esta acao Ação no modal
    Então preencho a justificativa do modal de editar ação
    E edito as informações iniciais da ação
    E edito as informações do responsável
    E clico no botão confirmar do modal de editar acao
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que os dados da ação foram editado
    E o responsável da ação foi editado

  Cenário: CT03 - Anular ação para aceite - SRO
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação
    Quando clico no botão Editar Acao
    Então deve exibir o modal de Editar ação
    E seleciono a opção Anular Ação no modal
    Então preencho a justificativa do modal de editar ação
    E clico no botão confirmar do modal de editar acao
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que a ação foi anulada
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação