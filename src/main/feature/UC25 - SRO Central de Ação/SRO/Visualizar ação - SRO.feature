#language: pt

@SRO @UC25 @WEB @SPRINT07 @VISUALIZARACAOSRO
Funcionalidade: UC25 - SRO Central De Ações - Visualizar ação - SRO

  Cenário: CT01 - Visualizar acao para aceite - SRO
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação