#language: pt

@SRO @UC25 @WEB @SPRINT07 @PLANODEACAOSRO
Funcionalidade: UC25 - SRO Central De Ações - Listagem planos de ação - SRO

  Cenário: CT01 - Valido que o plano de ação está na listagem
    Dado que eu crie um novo plano de ação via API
    E que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    Entao valido que o plano de ação está sendo exibido na listagem