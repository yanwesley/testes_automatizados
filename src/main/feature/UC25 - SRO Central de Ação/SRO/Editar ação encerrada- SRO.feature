#language: pt

@SRO @UC25 @WEB @SPRINT07 @EDITARACAOENCERRADASRO
Funcionalidade: UC25 - SRO Central De Ações - Editar ação encerrada - SRO

  Cenário: CT01 - Editar acao Encerrada - SRO
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Em execução
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao preencher os dados de execução da ação
    E clicar no botão Salvar e Finalizar Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação
    E clico no botão Editar Acao
    E seleciono a opção Editar Ação no modal
    Então preencho a justificativa do modal de editar ação
    E edito se a ação é pública nas informações iniciais da ação
    E clico no botão confirmar do modal de editar acao
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que os dados da ação pública foi editada

  Cenário: CT02 - Anular acao Encerrada - SRO
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação
    Quando clico no botão Editar Acao
    E seleciono a opção Anular Ação no modal
    Então preencho a justificativa do modal de editar ação
    E clico no botão confirmar do modal de editar acao
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que a ação foi anulada
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação