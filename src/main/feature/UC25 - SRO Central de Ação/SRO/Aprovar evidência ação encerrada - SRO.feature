#language: pt

@SRO @UC25 @WEB @SPRINT07 @APROVAREVIDENCIASACOES
Funcionalidade: UC25 - SRO Central De Ações - Aprovar evidências da ação encerrada - SRO

  Cenário: CT01 - Aprovar evidências da ação encerrada sem a verificação da eficácia da ação - SRO
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Em execução
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao preencher os dados de execução da ação
    E clicar no botão Salvar e Finalizar Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação
    E clico no botão Aprovar Evidência
    E deve exibir o modal de Aprovar Evidência
    E ao selecionar a opção que não houve verificação da eficácia da ação
    Então preencho os dados do modal de Aprovar Evidência
    E clico no botão confirmar do modal de Aprovar Evidência
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E ordeno a acao por ordem decrescente
    Então valido que o status da 1 ação é "Encerrada"
    E clico no botão Editar Acao na listagem na linha 1
    Então deve exibir a página de Visualizar Ação
    E valido que os botões Aprovar e Reprovar evidências não são exibidos

  Cenário: CT02 - Aprovar evidências da ação encerrada com a verificação da eficácia da ação - SRO
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Em execução
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao preencher os dados de execução da ação
    E clicar no botão Salvar e Finalizar Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação
    E clico no botão Aprovar Evidência
    E deve exibir o modal de Aprovar Evidência
    E ao selecionar a opção que houve verificação da eficácia da ação
    Então preencho os dados do modal de Aprovar Evidência
    E clico no botão confirmar do modal de Aprovar Evidência
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E ordeno a acao por ordem decrescente
    Então valido que o status da 1 ação é "Auditada"
    E clico no botão Editar Acao na listagem na linha 1
    Então deve exibir a página de Visualizar Ação
    E valido que os botões Aprovar e Reprovar evidências não são exibidos
