#language: pt

@SRO @UC25 @WEB @SPRINT07 @VISUALIZARPLANODETALHADO
Funcionalidade: UC25 - SRO Central Ação - Visualizar plano de ação - SRO

  Cenário: CT01 - Valido os detalhes do plano de ação
    Dado que eu crie um novo plano de ação via API
    E que eu esteja logado com usuário SRO
    Quando acesso a TAB Central de Ações
    E clicar no botão Editar da central de ações ordenado por ordem decrescente
    Entao deve exibir os detalhes do plano de ação na elaboração plano de ação

  Cenário: CT02 - Visualizar plano de ação com ação aberta
    Dado que eu crie um novo plano de ação via API
    E que eu crie uma nova ação via API
    E que eu esteja logado com usuário SRO
    Quando acesso a TAB Central de Ações
    E clicar no botão Editar da central de ações ordenado por ordem decrescente
    Entao deve exibir os detalhes do plano de ação na elaboração plano de ação
    E exibir os detalhes da ação com status "Aguardando aceite" no plano de ação

  Cenário: CT03 - Visualizar plano de ação com ação executada
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Em execução
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao preencher os dados de execução da ação
    E clicar no botão Salvar e Finalizar Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E clicar no botão Editar da central de ações ordenado por ordem decrescente
    Entao deve exibir os detalhes do plano de ação na elaboração plano de ação
    E exibir os detalhes da ação com status "Encerrada" no plano de ação
    Entao valido o plano de ação detalhado

