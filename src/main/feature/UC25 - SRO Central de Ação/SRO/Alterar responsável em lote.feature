#language: pt

@SRO @UC25 @WEB @SPRINT08 @ALTERARRESPONSAVELLOTE
Funcionalidade: UC25 - SRO Central De Ações - Alterar responsável em lote - SRO

  Cenário: CT01 - Alterar responsável em lote e valida alteração
    Entao que eu esteja logado com usuário SRO
    Dado que eu crie 10 nova(s) ação(es) via API
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E ordeno a acao por ordem decrescente
    Então seleciono 10 açõe(es)
    E clico no botão alterar responsavel em lote
    E edito as informações do responsável em lote
    Entao deve exibir o modal de confirmar alteração em lote
    E ao clicar no botão Ok no modal de confirmar alterações da ação em lote
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao valido que as 10 ação(es) está(ão) sendo exibida(s) na listagem em aberto