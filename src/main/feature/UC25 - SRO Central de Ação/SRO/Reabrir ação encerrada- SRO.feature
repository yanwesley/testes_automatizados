#language: pt

@SRO @UC25 @WEB @SPRINT07 @REABRIRACAO
Funcionalidade: UC25 - SRO Central De Ações - Reabrir ação encerrada - SRO

  Cenário: CT01 - Reabrir ação encerrada - SRO
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Em execução
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao preencher os dados de execução da ação
    E clicar no botão Salvar e Finalizar Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação
    E clico no botão Reabrir Ação
    Então preencho a justificativa do modal de Reabrir Ação
    E edito as informações iniciais da ação no modal de Reabrir Ação
    E clico no botão confirmar do modal de Reabrir Ação
    Entao deve exibir a snackBar com a mensagem "Ação reaberta com sucesso"
    E valido que os dados da ação foram editado
    E acesso a TAB Central de Ações
    E que eu clique na tab Em execução
    E ordeno a acao por ordem decrescente
    Então valido que o status da 1 ação é "Reaberta"
    E clico no botão Editar Acao na listagem na linha 1
    Então deve exibir a página de Visualizar Ação
    E deve exibir os dados da execução preenchidos