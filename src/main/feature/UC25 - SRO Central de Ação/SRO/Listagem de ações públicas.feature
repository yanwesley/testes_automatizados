#language: pt

@SRO @UC25 @WEB @SPRINT08 @LISTAGEMPUBLICA
Funcionalidade: UC25 - SRO Central De Ações - Listagem de ação públicas

  Cenário: CT01 - Valido que a ação está na listagem com o perfil SRO
    Dado que eu crie uma nova ação via API
    E que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E clico na tab pública
    E ordeno a acao por ordem decrescente
    Entao valido que o plano de ação está sendo exibido na listagem pública

  Cenário: CT02 - Valido que a ação está na listagem com o perfil segurança patrimonial
    Dado que eu crie uma nova ação via API
    E que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E clico na tab pública
    E ordeno a acao por ordem decrescente
    Entao valido que o plano de ação está sendo exibido na listagem pública com perfil diferente de SRO

  Cenário: CT03 - Visualizar ação pública
    Dado que eu crie uma nova ação via API
    E que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E clico na tab pública
    Entao ordeno a acao por ordem decrescente
    E valido os detalhes da ação pública

