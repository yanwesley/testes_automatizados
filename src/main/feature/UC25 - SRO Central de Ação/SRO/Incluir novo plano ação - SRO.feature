#language: pt

@SRO @UC25 @WEB @SPRINT07 @INCLUIRPLANOACAOSRO @REGRESSION_07.1
Funcionalidade: UC25 - SRO Central De Ações - Incluir novo plano de ação - SRO

  Cenário: CT01 - Incluir novo plano de ação - SRO
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E clico no botão incluir novo plano
    Entao preencho o nome do plano
    E adicionar um novo responsável no plano de ação avulso
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao valido que o plano avulso foi criado

  Cenário: CT02 - Editar plano de ação
    Dado que clico em editar plano
    Entao preencho o nome do plano editado
    E adicionar um novo responsável no plano de ação avulso editado
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao valido que o plano avulso foi editado

  Cenário: CT03 - Incluir ação
    Dado que clico em incluir uma ação
    Entao ao preencher as informações iniciais
    E adicionar um responsável na ação
    Então clico no botão Salvar do Modal de Incluir Uma Ação
    E deve exibir a snackBar com a mensagem "Ação criada com sucesso."
    Entao valido a ação do plano avulso

  Cenário: CT04 - Excluir ação
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao clicar no botão Aceitar Ação
    Então deve exibir o modal de confirmação para o Aceite da Ação
    E ao clicar em Confirmar no modal de Aceite da Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Dado que eu esteja logado com o Dono do Plano
    E acesso a TAB Central de Ações
    E clicar no botão Editar ação da central de ações
    E clico na ação avulsa
    Entao excluo a ação avulsa
    E clico no boto confirmar exclusão
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"





