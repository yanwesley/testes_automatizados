#language: pt

@SRO @UC25 @WEB @SPRINT07 @DESDOBRARACAO
Funcionalidade: UC25 - SRO Central De Ações - Desdobrar ação - SRO

  Cenário: CT01 - Desdobrar ação para aceite - SRO
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação
    E clico no botão Desdobrar Ação
    E edito as informações iniciais da ação no modal de Desdobrar Ação
    E clico no botão para buscar o responsável para a ação desdobrada
    E adicionar um novo responsável na ação
    E clico no botão confirmar do modal de Desdobrar Ação
    Entao deve exibir a snackBar com a mensagem "Ação desdobrada com sucesso."
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E ordeno a acao por ordem decrescente
    Entao deve exibir a ação da linha 1 com a origem da ação
    E valido que o status da 1 ação é "Aguardando aceite"
    E clico no botão Editar Acao na listagem na linha 1
    Então deve exibir a página de Visualizar Ação com os detalhes da ação desdobrada
