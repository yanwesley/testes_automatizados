#language: pt

@SRO @UC25 @WEB @SPRINT08 @ACEITARACAOLOTE
Funcionalidade: UC25 - SRO Central De Ações - Aceitar ação em lote - Usuário

  Cenário: CT01 - Aceitar ação em lotes e validar mudança para o status ABERTO
    Dado que eu crie 10 nova(s) ação(es) via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E ordeno a acao por ordem decrescente
    Então seleciono 10 açõe(es)
    E clicar no botão Aceitar selecionadas
    Então deve exibir o modal de aceitar ação em lote
    E ao clicar no botão Ok no modal de aceitar ação em lote
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E que eu clique na tab Em execução
    Quando ordeno a acao por ordem decrescente
    Entao valido que as 10 ação(es) está(ão) sendo exibida(s) na listagem de em execução