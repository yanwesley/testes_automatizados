#language: pt

@SRO @UC25 @WEB @SPRINT07 @FILTROLISTAGEM
Funcionalidade: UC25 - SRO Central De Ações - Filtro Listagem plano de ação

  Contexto:
    Dado que eu crie uma nova ação via API
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    Entao deve exibir a pagina de listagem de ações

  Cenario: CT01 - Valida filtro por texto com resultado válidos
    Quando eu busco pelo texto "André"
    Entao deve ser exibido os registros que contenham o texto "André" na listagem de ações

  Cenario: CT02 - Valida filtro por texto sem resultados
    Quando eu busco pelo texto "Inflação"
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado."

  Cenario: CT03 - Valida filtro por gerente na TAB Para aceite
    Dado que eu clique na tab Para Aceite
    E escolho o gerente no filtro "Fabiano Burghi Xavier"
    Entao valido a listagem com o gerente "Fabiano Burghi Xavier"

  Cenario: CT04 - Valida filtro por gerente na TAB Pública
    Dado aceito a ação via API
    Dado que eu clique na tab Para Aceite
    E escolho a área no filtro "RUMO - Manut. Mecânica Turno"
    Entao valido a listagem com a área "RUMO - Manut. Mecânica Turno"

  Cenario: CT05 - Valida filtro por responsável na TAB Para aceite
    Dado que eu clique na tab Para Aceite
    E escolho o responsável no filtro "Lenaldo Monteiro"
    Entao valido a listagem com o responsável "Lenaldo Monteiro"

  Cenario: CT06 - Valida filtro por gerente na TAB Para Em execução
    Dado aceito a ação via API
    Quando que eu clique na tab Em execução
    E escolho o gerente no filtro "Fabiano Burghi Xavier"
    Entao valido a listagem com o gerente "Fabiano Burghi Xavier"

  Cenario: CT07 - Valida filtro por gerente na TAB Pública
    Dado aceito a ação via API
    Quando que eu clique na tab pública
    E escolho o gerente no filtro "Fabiano Burghi Xavier"
    Entao valido a listagem com o gerente "Fabiano Burghi Xavier"

  Cenario: CT08 - Valida filtro por texto sem resultados na aba pública
    Quando eu busco pelo texto "Inflação"
    Entao deve exibir a mensagem informando "Nenhum resultado encontrado."

  Cenario: CT09 - Valida filtro por gerente na TAB Pública
    Dado aceito a ação via API
    Quando que eu clique na tab pública
    E escolho a área no filtro "RUMO - Manut. Mecânica Turno"
    Entao valido a listagem com a área "RUMO - Manut. Mecânica Turno"




