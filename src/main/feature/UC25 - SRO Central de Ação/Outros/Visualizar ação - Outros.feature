#language: pt

@SRO @UC25 @WEB @SPRINT07 @VISUALIZARACAOOUTROS
Funcionalidade: UC25 - SRO Central De Ações - Visualizar ação - Usuário

  Cenário: CT01 - Visualizar ação para aceite - Dono do acidente
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação