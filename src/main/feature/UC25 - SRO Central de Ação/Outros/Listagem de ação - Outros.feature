#language: pt

@SRO @UC25 @WEB @SPRINT07 @LISTAGEMDEACAODONO
Funcionalidade: UC25 - SRO Central De Ações - Listagem de ação - Usuário

  Cenário: CT01 - Valido que ação está na listagem de ação PARA ACEITE
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    Quando ordeno a acao por ordem decrescente
    Entao valido que a ação está sendo exibido na listagem

  Cenário: CT02 - Valido que ação está na listagem de ação EM EXECUÇÃO
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Em execução
    Quando ordeno a acao por ordem decrescente
    Entao valido que a ação está sendo exibido na listagem

  Cenário: CT03 - Valido que ação anulada está na listagem de ação ENCERRADAS
    Dado que eu crie uma nova ação via API
    E anulo a ação via API
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    Quando ordeno a acao por ordem decrescente
    Entao valido que a ação anulada está sendo exibido na listagem de encerradas