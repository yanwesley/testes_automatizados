#language: pt

@SRO @UC25 @WEB @SPRINT07 @JUSTIFICARPRAZO
Funcionalidade: UC25 - SRO Central De Ações - Justificar prazo da ação - Usuário

  Cenário: CT01 - Justificar prazo da ação.
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Em execução
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao clicar no botão Justificar Atraso
    Então deve exibir o modal de Justificar Atraso
    E preencho os dados do modal de Justificar Atraso
    E clico no botão Confirmar do modal de Justificar Atraso
    E confirmo a ação de Justificar Atraso
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    E deve exibir a data preenchida no campo de Previsão de Conclusão