#language: pt

@SRO @UC25 @WEB @SPRINT07 @LISTAGEMPLANODEACAODONO
Funcionalidade: UC25 - SRO Central De Ações - Listagem planos de ação - Usuário

  Cenário: CT01 - Valido que o plano de ação está na listagem de plano de ação
    Dado que eu crie um novo plano de ação via API
    E que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    Entao valido que o plano de ação está sendo exibido na listagem