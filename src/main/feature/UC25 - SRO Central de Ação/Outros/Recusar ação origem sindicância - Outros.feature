#language: pt

@SRO @UC25 @WEB @SPRINT07 @RECUSAACAO
Funcionalidade: UC25 - SRO Central De Ações - Recusar ação origem sindicância - Outros - Usuário

  Cenário: CT01 - Recusar ação origem sindicância - discordar da Descrição
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao clicar no botão Recusar Ação
    Então seleciono a opção "Discorda da descrição" no modal de recusar ação
    E preencho os dados do modal de recusar ação
    E ao clicar em Enviar Solicitação no modal de Recusar Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E deve exibir o box com a mensagem "Aguardando retorno do solicitante."
    Entao que eu esteja logado com o Dono do Plano
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E ordeno a acao por ordem decrescente
    Então valido que o status da 1 ação é "Recusada"
    E clico no botão Editar Acao na listagem na linha 1
    Então deve exibir a página de Visualizar Ação
    E deve exibir o box com a mensagem "Solicitação de alteração por discordar da descrição."
    Então clico na tab HISTÓRICO da ação
    E valido que o pedido de solicitação está registrado

  Cenário: CT02 - Recusar ação origem sindicância - discordar do prazo
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao clicar no botão Recusar Ação
    Então seleciono a opção "Discorda do prazo" no modal de recusar ação
    E preencho os dados do modal de recusar ação
    E ao clicar em Enviar Solicitação no modal de Recusar Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E deve exibir o box com a mensagem "Aguardando retorno do solicitante."
    Entao que eu esteja logado com o Dono do Plano
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E ordeno a acao por ordem decrescente
    Então valido que o status da 1 ação é "Recusada"
    E clico no botão Editar Acao na listagem na linha 1
    Então deve exibir a página de Visualizar Ação
    E deve exibir o box com a mensagem "Solicitação de alteração por discordar do prazo."
    Então clico na tab HISTÓRICO da ação
    E valido que o pedido de solicitação está registrado

  Cenário: CT03 - Recusar ação origem sindicância - discordar do responsável
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao clicar no botão Recusar Ação
    Então seleciono a opção "Discorda do responsável" no modal de recusar ação
    E preencho os dados do modal de recusar ação
    E ao clicar em Enviar Solicitação no modal de Recusar Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E deve exibir o box com a mensagem "Aguardando retorno do solicitante."
    Entao que eu esteja logado com o Dono do Plano
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E ordeno a acao por ordem decrescente
    Então valido que o status da 1 ação é "Recusada"
    E clico no botão Editar Acao na listagem na linha 1
    Então deve exibir a página de Visualizar Ação
    E deve exibir o box com a mensagem "Solicitação de alteração por discordar do responsável."
    Então clico na tab HISTÓRICO da ação
    E valido que o pedido de solicitação está registrado