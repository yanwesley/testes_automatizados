#language: pt

@SRO @UC25 @WEB @SPRINT07 @RECUSARACAODESDOBRADA
Funcionalidade: UC25 - SRO Central De Ações -Recusar ação origem desdobrada

  Cenário: CT01 - Recusar ação origem desdobrada - SRO
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação com os detalhes da ação
    E clico no botão Desdobrar Ação
    E edito as informações iniciais da ação no modal de Desdobrar Ação
    E clico no botão para buscar o responsável para a ação desdobrada
    E adicionar um novo responsável na ação
    E clico no botão confirmar do modal de Desdobrar Ação
    Entao deve exibir a snackBar com a mensagem "Ação desdobrada com sucesso."
    Dado que eu esteja logado com usuário CCO VIA
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    E ao clicar no botão Recusar Ação
    E seleciono o motivo para recusar a ação desdobrada
    E preencho a justificativa da recusa desdobrar ação
    Entao confirmo a recusa da ação desdobrada
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E ordeno a acao por ordem decrescente
    Então valido que o status da 1 ação é "Anulada"
    E clico no botão Editar Acao na listagem na linha 1
    Então deve exibir a página de Visualizar Ação
    Então clico na tab HISTÓRICO da ação
    E valido que o pedido de solicitação da ação desdobrada está registrado

