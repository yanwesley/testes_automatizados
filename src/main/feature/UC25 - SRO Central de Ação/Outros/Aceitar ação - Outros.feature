#language: pt

@SRO @UC25 @WEB @SPRINT07 @ACEITARACAO
Funcionalidade: UC25 - SRO Central De Ações - Aceitar ação - Usuário

  Cenário: CT01 - Aceitar ação e validar mudança para o status ABERTO
    Dado que eu crie uma nova ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Para Aceite
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao clicar no botão Aceitar Ação
    Então deve exibir o modal de confirmação para o Aceite da Ação
    E ao clicar em Confirmar no modal de Aceite da Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E que eu clique na tab Em execução
    Quando ordeno a acao por ordem decrescente
    Entao valido que a ação está sendo exibido na listagem