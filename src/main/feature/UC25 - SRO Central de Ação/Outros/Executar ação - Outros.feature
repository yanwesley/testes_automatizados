#language: pt

@SRO @UC25 @WEB @SPRINT07 @EXECUTARACAO
Funcionalidade: UC25 - SRO Central De Ações - Executar ação - Usuário

  Cenário: CT01 - Executar prazo da ação.
    Dado que eu crie uma nova ação via API
    E aceito a ação via API
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Em execução
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Então deve exibir a página de Visualizar Ação
    E ao preencher os dados de execução da ação
    E clicar no botão Salvar e Finalizar Ação
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E deve exibir os dados da execução preenchidos

  Cenário: CT02 - Valido que ação executada está na listagem de ação ENCERRADAS
    Entao que eu esteja logado com usuário Segurança Patrimonial
    E acesso a TAB Central de Ações
    E que eu clique na tab Encerradas
    E clicar no botão Editar ação da central de ações ordenado por ordem decrescente
    Entao deve exibir os dados da execução preenchidos