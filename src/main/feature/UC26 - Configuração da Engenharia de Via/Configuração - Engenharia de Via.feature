#language: pt

@SRO @UC26 @WEB @SPRINT12 @CONFIGVIA
Funcionalidade: UC26 - SRO Configurações - Engenharia de Via

  Cenário: CT01 - Criar uma nova configuração da Via
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Configurações
    E acesso a TAB Via das configurações
    Então clico em Adicionar Configuração de Via
    E preencho os dados da configuração da Via
    E clico em salvar no modal de adicionar configuração da Via
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    E valido os dados da configuração da via adicionada

  Cenário: CT02 - Editar uma nova configuração da Via
    Entao clico no botão Editar configuração Via
    E edito os dados da configuração da Via
    E clico em salvar no modal de adicionar configuração da Via
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    E valido os dados da configuração da via editada

  Cenário: CT03 - Adicionar faixa em uma configuração da Via
    Quando clico no botão Adicionar Faixa
    E preencho os dados da faixa no modal
    E clico no botão Salvar no modal de Faixa
    E clico em Salvar Informações na tela de Configurações Via
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    Então valido a faixa adicionada na linha 1

  Cenário: CT04 - Editar faixa em uma configuração da Via
    Quando clico no botão Editar Faixa na linha 1
    E edito os dados da faixa no modal
    E clico no botão Salvar no modal de Faixa
    E clico em Salvar Informações na tela de Configurações Via
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    Então valido a faixa editada na linha 1

  Cenário: CT05 - Excluir faixa em uma configuração da Via
    Quando clico no botão Excluir Faixa na linha 1
    E confirmo a exclusão da faixa
    E clico em Salvar Informações na tela de Configurações Via
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    Então valido a faixa excluida na linha 1