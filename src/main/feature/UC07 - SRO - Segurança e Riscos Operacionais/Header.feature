#language: pt

@SRO @UC07 @WEB @SPRINT01 @ABAINCIDENTES
Funcionalidade: UC07 - SRO - Segurança e Riscos Operacionais - Header menu

  Cenario: CT01 - Valida menu para usuário SRO
    Dado que eu esteja logado com usuário SRO
    Então deve exibir o menu com permissões do SRO

  Cenario: CT02 - Valida menu para usuário CCO
    Dado que eu esteja logado com usuário CCO
    Então deve exibir o menu com permissões do CCO

  Cenario: CT03 - Valida menu para usuário CCO VIA
    Dado que eu esteja logado com usuário CCO VIA
    Então deve exibir o menu com permissões do CCO VIA

  Cenario: CT04 - Valida navegação de cada opção do menu
    Dado que eu esteja logado com usuário SRO
    Entao valido se ao clicar no menu a página é atualizada
      | dashboard   |
      | sinistro    |
      | sindicancia |
      | incidente   |
      | acoes       |
      | info        |

  Cenario: CT05 - Valida menu para usuário CIPIA VIA
    Dado que eu esteja logado com usuário CIPIA Via
    Então deve exibir o menu com permissões do CIPIA VIA






