#language: pt

@SRO @UC07 @WEB
Funcionalidade: UC07 - SRO - Segurança e Riscos Operacionais - Login

  Cenario: CT01 - Efetuar login com usuário e senha válido
    Dado que eu estou na página de login
    Quando preencho com usuário e senha
      | user | senha |
    E clico em efetuar o login
    Então deve exibir o módulo do SRO

  Cenario: CT02 - Efetuar login com usuário ou senha inválidos
    Dado que eu estou na página de login
    Quando preencho com usuário e senha
      | user | senha |
    E clico em efetuar o login
    Então deve exibir a mensagem de login inválido