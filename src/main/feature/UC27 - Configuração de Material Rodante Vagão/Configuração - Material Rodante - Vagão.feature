#language: pt

@SRO @UC27 @WEB @SPRINT12 @CONFIGMATERIALRODANTEVAGAO
Funcionalidade: UC27 - SRO Configurações - Material Rodante - Vagão

  Cenário: CT01 - Criar uma nova configuração para Vagão
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Configurações
    E acesso a TAB Material Rodante das configurações
    E clico no separator para expandir o formulário de configuração de vagão
    Entao clico no botão Adicionar bloco de configuração de Vagão
    E preenche a configuração do vagão da linha 1
    Dado clico no botão Salvar Informações da configuração de Vagão
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"

  Cenário: CT02 - Valida a configuração criada para Vagão
    E acesso a TAB Configurações
    E acesso a TAB Material Rodante das configurações
    E clico no separator para expandir o formulário de configuração de vagão
    Entao valida o bloco de configuração de vagão
    E clica no botão editar do bloco de configuração do vagão
    E valida os dados preenchidos da configuração do vagão

  Cenário: CT03 - Edita a configuração criada para Vagão
    Dado edito os dados da configuração do vagão da linha 1
    E clico no botão Salvar Informações da configuração de Vagão
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E acesso a TAB Configurações
    E acesso a TAB Material Rodante das configurações
    E clico no separator para expandir o formulário de configuração de vagão
    E valida o bloco de configuração de vagão editado
    E clica no botão editar do bloco de configuração do vagão
    Entao valida os dados preenchidos da configuração do vagão editado
    E valida a mensagem de preenchimento do Nome da manga duplicado na configuração de vagão na linha 2
    E valida a mensagem de preenchimento do nome do tipo de manutenção duplicado na configuração de vagão na linha 2
    E valida a mensagem de preenchimento do nome do limite da cunha de fricção duplicado na configuração de vagão na linha 2
    E valida a mensagem de preenchimento do nome de limite do prato pião duplicado na configuração de vagão na linha 2
    E valida a mensagem de preenchimento do nome do limite de amparo balanço duplicado na configuração de vagão na linha 2
    E valida a mensagem de preenchimento do nome dos limites das molas duplicado na configuração de vagão na linha 2
    E valida a mensagem de preenchimento do nome da mola cunha duplicado na configuração de vagão na linha 2







