#language: pt
@SRO @WEB @SPRINT02 @UC13
Funcionalidade: UC13 - Novo sinistro - Header Formulário

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando clico no botão Incluir novo sinistro
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro

  Cenario: CT01 - Valida que o botão Ficha do trem não é exibida quando for entrada manual
  Visível somente se o preenchimento for via macro ou "Trem ou OS"
    E clico no botão Dados Manuais
    Entao valido que o botão Ficha do Trem não é exibido

  Cenario: CT02 -Valida que o botão Endereço do Sinistro não é exibida quando for entrada manual
  Visível somente se o preenchimento for via macro ou "Trem ou OS"
    E clico no botão Dados Manuais
    Entao valido que o botão Endereço do Sinistro não é exibido

  Cenario: CT03 - Valida que o botão Enviar e-mail esteja desabilitado até que o sinistro não é salvo pelo menos uma vez
  Visível somente se o preenchimento for via macro ou "Trem ou OS"
    E clico no botão Dados Manuais
    Entao valido que o botão Enviar e-mail está desabilitado

  Cenario: CT04 - Valida que o botão Enviar e-mail esteja habilitado quando salvar o sinistro
  Visível somente se o preenchimento for via macro ou "Trem ou OS"
    E clico no botão Dados Manuais
    Entao preencho todos os campos do sinistro manualmente
    E clico em Salvar Sinistro
    Entao valido que o botão Enviar e-mail está habilitado

  @TESTEFAIL.02
  Cenario: CT05 - Enviar e-mail para a empresa
    E clico no botão Dados Manuais
    Entao preencho todos os campos do sinistro manualmente
    E clico em Salvar Sinistro
    Entao clico em Enviar e-mail
    E confirmo o envio do e-mail
    Entao deve exibir a snackBar com a mensagem "E-mail enviado com sucesso."

  Cenario: CT06 - Ao clicar no botão alterar deve retornar para a escolha do tipo de entrada do sinistro
    E clico no botão Dados Manuais
    Entao deve exibir a página de inserção de dados do sinistro manual
    E clico no botão Alterar
    Então deve exibir a página para selecionar o tipo de entrada do novo sinistro

  Cenario: CT07 - Ao salvar o sinistro não deve exibir o botão alterar
    E clico no botão Dados Manuais
    Entao preencho todos os campos do sinistro manualmente
    E clico em Salvar Sinistro
    Entao o botão Alterar não deve mais estar visivel