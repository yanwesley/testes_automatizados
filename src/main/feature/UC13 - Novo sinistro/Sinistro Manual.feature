#language: pt
@SRO @WEB @SPRINT02 @UC13 @SINISTROMANUAL
Funcionalidade: UC13 - Novo sinistro - Novo sinistro Manual

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando clico no botão Incluir novo sinistro
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro
    E clico no botão Dados Manuais

  Cenario: CT01 - Valida que os campos do formulário estão vazios
    Entao deve exibir o formulario de novo sinistro com os campos vazios

  Cenario: CT02 - Cadastrar um novo sinistro manualmente
    Entao preencho todos os campos do sinistro manualmente
    E clico em Salvar Sinistro
    Entao deve exibir a snackBar com a mensagem 'Sinistro salvo com sucesso.'
    E o sinistro deve ser exibido na Listagem de Sinistros abertos com o status de "Aberto"
    Entao clicar no botão Editar do sinistro da linha 1
    E valido que os dados do sinistro foram carregados no formulário
