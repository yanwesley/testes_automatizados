#language: pt
@SRO @WEB @SPRINT03 @UC13
Funcionalidade: UC13 - Recusa do Sinistro - SRO

  Cenario: CT01 - Recusa do Sinistro
    Dado que eu esteja logado com usuário CCO
    Quando cadastro um novo Sinistro manual com Veículo na linha 1
    E que eu esteja logado com usuário CCO VIA
    E preencho os dados do Sinistro com o CCO VIA
    Entao que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Liberação
    Quando que preencho as a Liberação final
    E clico no botão Salvar e Encerrar
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "O sinistro foi encerrado com sucesso."
    Dado que eu esteja logado com usuário SRO
    Entao acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Aceite do Sinistro
    Entao preenche a Escolha do Aceite com Não
    E preenche o motivo da recusa
    E insere 5 arquivo na recusa do sinistro
    Entao valida que os 5 arquivos da recusa foram adicionados
    Quando clico no botão excluir arquivo da linha 1 da Recusa
    E valida que os 4 arquivos da recusa foram adicionados
    Quando clico em Confirmar Recusa
    Entao deve exibir a snackBar com a mensagem "Recusa salva com sucesso."
    E volta para a lista de sinistros
    E seleciono a TAB de sinistros Encerrados
    Entao valida que o Sinistro foi encerrado e não aceito