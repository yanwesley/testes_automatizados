#language: pt
@SRO @WEB @SPRINT03 @UC13 @REABERTURA
Funcionalidade: UC13 - Reabertura e histórico de aceite de sinistro - SRO

  Cenario: CT01 - Reabertura do Sinistro
    Dado que eu esteja logado com usuário CCO
    Quando cadastro um novo Sinistro manual com Veículo na linha 1
    E que eu esteja logado com usuário CCO VIA
    E preencho os dados do Sinistro com o CCO VIA
    Entao que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Liberação
    Quando que preencho as a Liberação final
    E clico no botão Salvar e Encerrar
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "O sinistro foi encerrado com sucesso."
    Dado que eu esteja logado com usuário SRO
    Entao acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Aceite do Sinistro
    Entao preenche a Escolha do Aceite com Não
    E preenche o motivo da recusa
    E insere 5 arquivo na recusa do sinistro
    Entao valida que os 5 arquivos da recusa foram adicionados
    Quando clico no botão excluir arquivo da linha 1 da Recusa
    E valida que os 4 arquivos da recusa foram adicionados
    Quando clico em Confirmar Recusa
    Entao deve exibir a snackBar com a mensagem "Recusa salva com sucesso."
    Quando clico no botão Reabrir Sinistro
    E insiro um comentário para reabertura do sinistro
    E  insere 1 arquivo na reabertura do sinistro
    E clico em confirmar reabertura
    Entao deve exibir a snackBar com a mensagem "Reabertura salva com sucesso."
    E volta para a lista de sinistros
    E seleciono a TAB de sinistros Abertos
    Entao o sinistro deve ser exibido na Listagem de Sinistros abertos do SRO com o status de "Aguardando aceite"

  Cenario: CT02 - Valida histórico do sinistro com eventos de Recusa e Reabertura
    Dado que eu esteja logado com usuário SRO
    Entao acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Aceite do Sinistro
    Entao deve exibir o histórico de aceite com 1 recusa e 1 reabertura

  Cenário: CT03 - Valida histórico do sinistro
    Dado que eu esteja logado com usuário SRO
    Entao acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Histórico
    Então deve exibir a lista com as ações realizadas
      | Incluiu o sinistro                 |
      | Incluiu um veículo de terceiro     |
      | Incluiu um responsável             |
      | Incluiu um recurso material        |
      | Incluiu uma turma                  |
      | CCO Via concluiu seu preenchimento |
      | Alterou o sinistro                 |
      | Alterou informações de liberação   |
      | Alterou o sinistro                 |
      | Recusou o sinistro                 |
      | Reabriu o sinistro                 |
