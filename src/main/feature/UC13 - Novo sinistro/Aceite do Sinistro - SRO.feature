#language: pt
@SRO @WEB @SPRINT03 @UC13 @ACEITESINISTRO
Funcionalidade: UC13 - Aceite do Sinistro - SRO

  Cenario: CT-01 SRO Validando mensagem na tela de aceite
    Dado que eu crie um novo sinistro sem veiculos via API
    Entao que eu esteja logado com usuário SRO
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Aceite do Sinistro
    Entao valido a mensagem informativa para prosseguir com o Aceite "Atenção: para prosseguir com o Aceite do sinistro é necessário que o CCO Circulação e Via encerrem."

  Cenario: CT-02 Preencher CCO e CCO VIA e aceitar o sinistro
    Dado que eu esteja logado com usuário CCO
    Quando cadastro um novo Sinistro manual com Veículo na linha 1
    E que eu esteja logado com usuário CCO VIA
    E preencho os dados do Sinistro com o CCO VIA
    Entao que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Liberação
    Quando que preencho as a Liberação final
    E clico no botão Salvar e Encerrar
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "O sinistro foi encerrado com sucesso."
    Dado que eu esteja logado com usuário SRO
    Entao acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Aceite do Sinistro
    Entao preenche a Escolha do Aceite com Sim
    E preenche Classificação como Acidente
    E preenche a Bandeira e a Causa Provável com a opção 1
    Quando clico em Confirmar Aceite
    Entao deve exibir a snackBar com a mensagem "Sinistro aceito com sucesso."
    E volta para a lista de sinistros
    E seleciono a TAB de sinistros Encerrados
    Entao valida que o Sinistro foi encerrado e aceito
