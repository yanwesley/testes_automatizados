#language: pt

@SRO @WEB @SPRINT03 @UC13 @DS
Funcionalidade: UC13 - Dados do Sinistro - CCO-VIA

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E cadastro um novo Sinistro manual com Veículo na linha 1

  Cenario: CT-001 - Validação dos dados preenchidos no sinistro
    Dado que eu esteja logado com usuário CCO VIA
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Dados do sinistro
    Entao valido que os dados do sinistro foram carregados no formulário como readonly
