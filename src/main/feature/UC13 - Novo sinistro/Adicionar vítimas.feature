#language: pt

@SRO @WEB @SPRINT02 @UC13
Funcionalidade: UC13 - Novo sinistro - adicionar vítimas
  Não está fazendo validação com backend

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando clico no botão Incluir novo sinistro
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro
    E clico no botão Dados Manuais

  Cenario: CT01 - Usuário informa que existem vítimas
    Quando informo que existem vítimas
    Entao deve exibir os campos para preencher a quantidade de vítimas

  Cenario: CT02 - Usuário informa que não existem vítimas
    Quando informo que não existem vítimas
    Entao não deve exibir os campos preencher a quantidade de vítimas

  Cenario: CT03 - Preencher campos de vítimas
    Quando informo que existem vítimas
    Entao deve exibir os campos para preencher a quantidade de vítimas
    E ao preencher com campos vazios a quantidade de vítimas
    Entao deve exibir a snackBar com a mensagem "Soma de vítimas zerada, portanto não há vítimas envolvidas."
