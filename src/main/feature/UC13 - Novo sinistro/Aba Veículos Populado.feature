#language: pt

# todo 12/09/2019   Serviço de Composição do Trem não está funcionando tag @SPRINT03 removida até correção
@SRO @WEB @UC13 @VP
Funcionalidade: UC13 - Aba Veículos Populado

  Cenario: CT01 - Exibir composição do trem do legado
    Dado que eu esteja logado com usuário CCO
    E cadastro um novo Sinistro via Macro sem Veículo e Liberação
    Entao deve exibir a snackBar com a mensagem 'Sinistro salvo com sucesso.'
    E altero a data para "26/08/2019" OS para 1514472
    E clico em Salvar E Avançar
    Entao deve exibir a snackBar com a mensagem 'Sinistro salvo com sucesso.'
    Entao deve exibir a composição do trem carregada pela OS

  Cenario: CT02 - Filtrar veículos marcados.
    E ao selecionar 1 veículo
    E clicar em Filtrar veículos marcados
    Entao deve exibir 1 veículo

  Cenario: CT03 - Exibir todos os veículos
    E clicar em Exibir todos os veículos
    Entao deve exibir a composição do trem carregada pela OS

  Cenario: CT04 - Selecionar veículo da composição e salvar
    E ao selecionar 1 veículo
    E ao selecionar a situação "Semi-Tombado"
    E clico em Salvar Informações dos Veículos
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."

  Cenário:  CT05 - Cadastrar veículo manual e validar dados.
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E os dados da nova linha da tabela devem estar vazios
    Entao preencho todos os campos do veiculos 2
    E clico em Salvar Informações dos Veículos
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    E o veículo deve ser exibido na lista de veículos na linha 2