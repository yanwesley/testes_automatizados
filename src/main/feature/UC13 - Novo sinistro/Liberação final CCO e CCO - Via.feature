#language: pt
@SRO @WEB @SPRINT02 @UC13 @RUMO-535
Funcionalidade: UC13 - Liberaçao final CCO e CCO Via

  Contexto:
    Dado que eu esteja logado com usuário CCO
    Quando cadastro um novo Sinistro manual com Veículo na linha 1
    E o sinistro deve ser exibido na Listagem de Sinistros abertos com o status de "Aberto"

  Cenario: CT01 - CCO Finalizado e CCO Via Não Finalizado
    Quando clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Liberação
    E que preencho a Aba liberação parcialmente
    Entao não é possível preencher a seção de Liberação final
    E o botão Salvar e Encerrar não está sendo apresentado

  Cenario: CT02 - CCO Não Finalizado e CCO Via Finalizado
    Dado que eu esteja logado com usuário CCO VIA
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando seleciono a TAB de sinistros Abertos
    Entao o sinistro deve ser exibido na Listagem de Sinistros abertos com o status de "Aberto"
    Quando clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico no botão Salvar e Encerrar
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "O sinistro foi encerrado com sucesso."
    E o sinistro deve ser exibido na Listagem de Sinistros abertos com o status de "Aguardando finalização"

  Cenario: CT03 - CCO Finalizado e CCO Via Finalizado
    Dado que eu esteja logado com usuário CCO VIA
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando seleciono a TAB de sinistros Abertos
    Entao o sinistro deve ser exibido na Listagem de Sinistros abertos com o status de "Aberto"
    Quando clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico no botão Salvar e Encerrar
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "O sinistro foi encerrado com sucesso."
    E o sinistro deve ser exibido na Listagem de Sinistros abertos com o status de "Aguardando finalização"
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando seleciono a TAB de sinistros Abertos
    Quando clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Liberação
    E preencho todos os campos da tab Liberação
    E na seção Liberação Final está sendo exibida a mensagem "Atenção: ao inserir informações da liberação final e clicar no botão Encerrar você não poderá mais inserir informações."
    Entao clico no botão Salvar e Encerrar
    E clico em confirmar
    Entao deve exibir a snackBar com a mensagem "O sinistro foi encerrado com sucesso."
    Entao o sinistro deve ser exibido na Listagem de Sinistros abertos com o status de "Aguardando aceite"





