#language: pt
@SRO @WEB @SPRINT02 @UC13
Funcionalidade: UC13 - Novo sinistro - Endereço do Sinistro

  Contexto:
    Dado cadastro um novo Sinistro via Macro sem Veículo e Liberação
    E que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros

  @TESTEFAIL.01
  Cenario: CT01 - Copiar endereço do sinistro para a área de transferência
    Quando eu clico para editar o sinistro aberto via macro da linha 1
    Entao deve exibir o formulario de novo sinistro com os campos carregados
    E ao clicar no botão Endereço do sinistro
    Entao deve exibir a snackBar com a mensagem 'Link copiado com sucesso.'
    E deve copiar a mensagem "https://maps.google.com/?q=40,60" para a area de transferência

