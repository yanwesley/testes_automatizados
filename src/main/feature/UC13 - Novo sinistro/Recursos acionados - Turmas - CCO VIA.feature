#language: pt
@SRO @WEB @SPRINT02 @UC13
Funcionalidade: UC13 - Adicionar Informações Sinistro - Recursos acionados - Turmas - CCO VIA

  Contexto:
    Dado que eu esteja logado com usuário CCO VIA
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando seleciono a TAB de sinistros Abertos
    Entao valido que a lista de sinistros não está vazia
    E clico no botão Editar do sinistro da linha 4
    Entao deve exibir a seção de Recursos acionados - Turmas
    Quando seleciono a opção Sim para a pergunta Turmas foram acionadas?

  Cenario: CT01 - Inclusão de um novo Recursos acionados - Turmas
    Quando clico no botão Adicionar turma
    Entao deve exibir o modal de Acionamento de turmas
    Quando preencher todos os campos do modal de Acionamento de turmas
    E clico no botão Salvar
    Então deve exibir a snackBar com a mensagem 'Turma adicionada com sucesso'
    E a turma deve ser inserida na seção de Acionamento de Turmas

  Cenario: CT02 - Edição do Recursos acionados - Máquinas adicionado
    Dado que existe uma Turma
    Quando clicar no botão Editar turma 1
    Entao deve exibir o modal de Acionamento de turmas
    E ao alterar os dados da Turma
    E clico no botão Salvar
    Então deve exibir a snackBar com a mensagem 'Turma editada com sucesso'
    E exibir a turma na tabela de turmas

  Cenario: CT03 - Exclusão do Recursos acionados - Turmas adicionado
    Dado que existe uma Turma
    Quando clicar no botão Excluir Turma 1
    Entao deve exibir o modal para excluir com a mensagem 'Deseja excluir a Turma?'
    Quando confirmo a exclusão da Turma
    Entao deve exibir a snackBar com a mensagem 'Turma excluída com sucesso'
    E a Turma deve ser excluída

  Cenario: CT04 - Cancelar a adição de um novo Recursos acionados - Turma
    Quando clico no botão Adicionar turma
    Entao deve exibir o modal de Acionamento de turmas
    Quando clico no botão cancelar
    Entao deve exibir a seção de Recursos acionados - Turmas

