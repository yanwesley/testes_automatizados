#language: pt
@SRO @WEB @SPRINT02 @UC13
Funcionalidade: UC13 - Adicionar Informações Sinistro - Recursos acionados - Máquinas - CCO VIA

  Contexto:
    Dado que eu esteja logado com usuário CCO VIA
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando seleciono a TAB de sinistros Abertos
    Entao valido que a lista de sinistros não está vazia
    E clico no botão Editar do sinistro da linha 4
    Entao deve exibir a seção de Recursos acionados - Máquinas
    Quando seleciono a opção Sim para a pergunta Máquinas foram acionadas?

  Cenario: CT01 - Inclusão de um novo Recursos acionados - Máquinas
    Quando clico no botão Adicionar máquina
    Entao deve exibir o modal de Acionamento de máquina
    Quando preencher todos os campos do modal de Acionamento de máquina
    E clico no botão Salvar
    Então deve exibir a snackBar com a mensagem 'Máquina adicionada com sucesso'
    E a máquina deve ser inserido na seção de Acionamento de máquina

  Cenario: CT02 - Edição do Recursos acionados - Máquinas adicionado
    Dado que existe uma Máquina
    Quando clicar no botão Editar máquina 1
    Entao deve exibir o modal de Acionamento de máquina
    E ao alterar os dados da Máquina
    E clico no botão Salvar
    Então deve exibir a snackBar com a mensagem 'Máquina editada com sucesso'
    E exibir a máquina na tabela de máquinas

  Cenario: CT03 - Exclusão do Recursos acionados - Máquinas adicionado
    Dado que existe uma Máquina
    Quando clicar no botão Excluir Máquina 1
    Entao deve exibir o modal para excluir com a mensagem 'Deseja excluir a Máquina?'
    Quando confirmo a exclusão da Máquina
    Entao deve exibir a snackBar com a mensagem 'Máquina excluída com sucesso'
    E a Máquina deve ser excluída

  Cenario: CT04 - Cancelar a adição de um novo Recursos acionados - Máquinas
    Quando clico no botão Adicionar máquina
    Entao deve exibir o modal de Acionamento de máquina
    Quando clico no botão cancelar
    Entao deve exibir a seção de Recursos acionados - Máquinas

