#language: pt

@SRO @WEB @SPRINT03 @UC13 @EMAILANTT
Funcionalidade: UC13 - Enviar Email para a ANTT

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros

  Cenario: CT-001 - Validar campos da tela
    Dado que eu crie um novo sinistro sem veiculos via API
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Liberação
    E clico no botão de Enviar email para a ANTT
    Entao deve ser exibido a modal de Informações para e-mail da ANTT
    E todos os campos da modal para envio da informação da ANTT devem ser apresentados

  Cenario:  CT-002 - Motivo deve estar desabilitado se não for preenchido a Natureza
    Quando clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Liberação
    Entao deve exibir o aviso para que o email para a ANTT seja enviado
    E clico no botão de Enviar email para a ANTT
    Entao deve ser exibido a modal de Informações para e-mail da ANTT
    E o campo Motivo deve estar desabilitado
    E seleciono o campo Natureza
    Entao o campo Motivo deve estar habilitado

  Cenario:  CT-003 - Valida alteração do Relato após alterar a Natureza
    Quando clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Liberação
    E clico no botão de Enviar email para a ANTT
    E preencho todos os campos da modal de envio de email para a ANTT
    Quando alterar a Natureza
    Entao valida que o campo Relato foi alterado
    E o campo Motivo não deve estar preenchido

  Cenario:  CT-004 - Envio do email para a ANTT
    Quando clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Liberação
    E clico no botão de Enviar email para a ANTT
    E preencho todos os campos da modal de envio de email para a ANTT
    Quando clico em Confirmar o envio
    Entao deve exibir a snackBar com a mensagem "E-mail enviado com sucesso"
    E deve exibir a modal com o E-mail enviado para a ANTT
    Quando clico no botão Fechar da modal de email enviado
    Entao deve ser exibida mensagem de envio para a ANTT
    Quando clico no botão Visualizar email enviado
    Entao deve exibir a modal com o E-mail enviado para a ANTT
