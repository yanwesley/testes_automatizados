#language: pt

@SRO @WEB @UC13 @SPRINT08 @EDITVEICULOS
Funcionalidade: SRO - Editar Veículos Envolvidos - perfil SRO

  Cenario: CT01 - Adicionar campo vazio.
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário SRO
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E os dados da nova linha da tabela devem estar vazios

  Cenario: CT02 - Ao alterar o tipo do veículo deve alterar o ícone
    Quando acesso a TAB Dashboard
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E os dados da nova linha da tabela devem estar vazios
    Então valido se ao alterar o tipo do veículo o ícone é alterado

  Cenario: CT03 - Ao selecionar o tipo Locomotiva deve desabilitar os campos mercadoria e perda
    Quando acesso a TAB Dashboard
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E ao selecionar o tipo de veículo "Locomotiva"
    Entao deve manter os campos Mercadoria e Perda readOnly

  Esquema do Cenario: <CT> - Valida preenchimento dos rodeiros pelo tipo <tipo>
    Quando acesso a TAB Dashboard
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E ao selecionar o tipo de veículo "<tipo>"
    Então valido se ao alterar o situação do veículo a quantidade de rodeiros é preenchida corretamente "<tipo>"

    Exemplos:
      | CT   | tipo           |
      | CT04 | Locomotiva     |
      | CT05 | Vagão          |
      | CT06 | Veículo de Via |

  Cenario: CT07 - Editar quantidade de rodeiros descarrilados
    Quando acesso a TAB Dashboard
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E ao selecionar o tipo de veículo "Vagão"
    E ao selecionar a situação "Semi-Tombado"
    Então deve permitir a edição dos rodeiros na linha 1
    E ao clicar para editar os rodeiros do veículo 1
    E ao marcar apenas 2 rodeiros
    E salvar a alteração de rodeiros
    Então deve atualizar a quantidade de rodeiros do veículo da linha 1 para 2

  Cenário:  CT08 - Cadastrar veículo e validar dados.
    Quando acesso a TAB Dashboard
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E os dados da nova linha da tabela devem estar vazios
    Entao preencho todos os campos do veiculos 1
    E clico em Salvar Informações na Edicao do SRO
    E o veículo deve ser exibido na lista de veículos na linha 1

  Cenario: CT09 - CCO Via - Sinistro - Aba Dados do Sinistro (Deve ser ReadOnly)
    Dado que eu esteja logado com usuário CCO VIA
    E acesso a TAB Sinistros
    Quando clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Dados do sinistro
    E valido veículo no formulário como readonly

  Cenario: CT010 - Qualquer perfil - Ficha Acidente (Deve ser ReadOnly, acessada por botão do header da Sindicância)
    Dado que eu esteja logado com usuário Cipia Material Rodante
    E navego até a ficha do trem
    Entao valido que os dados do veículo como readonly na ficha do trem





