#language: pt
@SRO @WEB @SPRINT03 @UC13 @EDITVEICULOSCCO @VEICULOS
Funcionalidade: UC13 - Aba Veículos

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando seleciono a TAB de sinistros Abertos
    Entao valido que a lista de sinistros não está vazia

  Cenario: CT01 - Adicionar campo vazio.
    Dado que eu crie um novo sinistro sem veiculos via API
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Veículos na página de novo sinistro
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E os dados da nova linha da tabela devem estar vazios

  Cenario: CT02 - Ao alterar o tipo do veículo deve alterar o ícone
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Veículos na página de novo sinistro
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E os dados da nova linha da tabela devem estar vazios
    Então valido se ao alterar o tipo do veículo o ícone é alterado

  Cenario: CT03 - Ao selecionar o tipo Locomotiva deve desabilitar os campos mercadoria e perda
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Veículos na página de novo sinistro
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E ao selecionar o tipo de veículo "Locomotiva"
    Entao deve manter os campos Mercadoria e Perda readOnly

  Esquema do Cenario: <CT> - Valida preenchimento dos rodeiros pelo tipo <tipo>
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Veículos na página de novo sinistro
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E ao selecionar o tipo de veículo "<tipo>"
    Então valido se ao alterar o situação do veículo a quantidade de rodeiros é preenchida corretamente "<tipo>"

    Exemplos:
      | CT   | tipo           |
      | CT04 | Locomotiva     |
      | CT05 | Vagão          |
      | CT06 | Veículo de Via |

  Cenario: CT07 - Editar quantidade de rodeiros descarrilados
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Veículos na página de novo sinistro
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E ao selecionar o tipo de veículo "Vagão"
    E ao selecionar a situação "Semi-Tombado"
    Então deve permitir a edição dos rodeiros na linha 1
    E ao clicar para editar os rodeiros do veículo 1
    E ao marcar apenas 2 rodeiros
    E salvar a alteração de rodeiros
    Então deve atualizar a quantidade de rodeiros do veículo da linha 1 para 2

  Cenário:  CT08 - Cadastrar veículo e validar dados.
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Veículos na página de novo sinistro
    Quando clico no botão Adicionar campo vazio
    Entao deve adicionar mais uma linha na tabela de veículos
    E os dados da nova linha da tabela devem estar vazios
    Entao preencho todos os campos do veiculos 1
    E clico em Salvar Informações dos Veículos
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    E o veículo deve ser exibido na lista de veículos na linha 1