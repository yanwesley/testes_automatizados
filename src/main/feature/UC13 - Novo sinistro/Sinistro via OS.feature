#language: pt
@SRO @WEB @SPRINT02 @UC13 @VIAOS
Funcionalidade: UC13 - Novo sinistro via OS

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir os campos permitidos para o perfil CCO
    Quando clico no botão Incluir novo sinistro
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro
    E clico no botão Número da OS ou prefixo

  Cenario: CT01 - Clicar no botão Alterar a entrada de dados
    Dado que estou na tela de listagem de OS ou prefixo
    E que clico no botão Alterar a entrada de dados
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro

  Cenario: CT02 - Validação do botão Selecionar
    Entao o botão Selecionar deve estar desabilitado
    Quando eu preencher os campos de data e hora da busca de OS
      | 13/10/2019 | 12:00 |
    E selecionar a linha 1 da OS e setar os dados da linha selecionada
    Entao o botão Selecionar deve estar habilitado
    E clicar no botão Selecionar
    Entao deve exibir a página de inserção de dados do sinistro via OS

  Cenario: CT03 - Clicar no botão Cancelar
    Dado que clico no botão Cancelar
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro

  Cenario: CT04 - Deve ser possível selecionar apenas um checkBox
    Quando eu preencher os campos de data e hora da busca de OS
      | 13/10/2019 | 12:00 |
    Quando selecionar a linha 1 de macro e OS
    E selecionar a linha 2 de macro e OS
    Entao validar que a OS da linha 1 não está selecionada