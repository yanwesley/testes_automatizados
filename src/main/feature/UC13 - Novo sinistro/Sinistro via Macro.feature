#language: pt
@SRO @WEB @SPRINT02 @UC13 @MACRO
Funcionalidade: UC13 - Novo sinistro via Macro

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir os campos permitidos para o perfil CCO
    Quando clico no botão Incluir novo sinistro
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro
    E clico no botão Número da Macro

  Cenario: CT01 - Clicar no botão Alterar a entrada de dados
    Dado que estou na tela de listagem de Macros
    E que clico no botão Alterar a entrada de dados
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro

  Cenario: CT02 - Validação do botão Selecionar
    Entao o botão Selecionar deve estar desabilitado
    Quando selecionar a linha 1 de macro e OS
    Entao o botão Selecionar deve estar habilitado
    E clicar no botão Selecionar
    Entao deve exibir a página de inserção de dados do sinistro via macro

  Cenario: CT03 - Clicar no botão Cancelar
    Dado que clico no botão Cancelar
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro

  Cenario: CT04 - Selecionar duas macros de prefixos diferentes
    Quando selecionar a linha 1 de macro e OS
    E selecionar a linha 2 de macro e OS
    Entao deve exibir a snackBar com a mensagem "Para selecionar mais de um, o prefixo deve ser o mesmo."

    #TODO: AGUARDANDO UMA MASSA PARA EXECUÇÃO DO TESTE - 12/09/2019
#  Cenario: CT05 - Selecionar mais de uma macro com o mesmo prefixo
#    Quando selecionar a linha 1 de macro e OS
#    E selecionar a linha 2 de macro e OS
#    Entao validar que as duas linhas selecionadas são do mesmo prefixo e estão selecionadas