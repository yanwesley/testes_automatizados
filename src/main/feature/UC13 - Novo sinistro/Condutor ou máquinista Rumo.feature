#language: pt

@SRO @WEB @SPRINT02 @UC13
Funcionalidade: UC13 - Novo sinistro - Condutor ou maquinista rumo

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando clico no botão Incluir novo sinistro
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro
    E clico no botão Dados Manuais

  Cenario: CT01 - Usuário informa que é condutor ou maquinista da RUMO
    Quando informo que é condutor ou maquinista da RUMO
    Entao deve exibir os campos para adicionar novos condutores ou maquinistas

  Cenario: CT02 - Usuário informa que não é condutor ou maquinista da RUMO
    Quando informo que não é condutor ou maquinista da RUMO
    Entao não deve exibir os campos para adicionar novos condutores ou maquinistas

  Cenario: CT03 - Adiciona novo condutor ou maquinista da RUMO
    Quando informo que é condutor ou maquinista da RUMO
    Entao deve exibir os campos para adicionar novos condutores ou maquinistas
    E ao editar o condutor ou maquinista
    E clicar em Salvar adição de Condutor
    Entao deve exibir a snackBar com a mensagem "O colaborador foi alterado com sucesso."
    E deve exibir o colaborador selecionado

  Cenario:  CT04 - Cancelar ação de adicionar/editar novo condutor ou maquinista da RUMO
    Quando informo que é condutor ou maquinista da RUMO
    Entao deve exibir os campos para adicionar novos condutores ou maquinistas
    E ao editar o condutor ou maquinista
    E clicar em Cancelar adição de Condutor
    Entao deve exibir a snackBar com a mensagem "Ação cancelada, o colaborador não foi alterado."