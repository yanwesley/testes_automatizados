#language: pt
@SRO @WEB @SPRINT01 @UC13
Funcionalidade: UC13 - Novo sinistro - escolha de entrada

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    E clico no botão Incluir novo sinistro

  Cenario: CT01 - Valida exibição dos campos de entrada Macro, Os e Manual
    Então deve exibir a página para selecionar o tipo de entrada do novo sinistro

  Cenario: CT02 - Selecionar entrada Manual
    Então deve exibir a página para selecionar o tipo de entrada do novo sinistro
    Quando clico no botão Dados Manuais
    Entao deve exibir a página de inserção de dados do sinistro manual
