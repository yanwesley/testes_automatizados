#language: pt

@SRO @WEB @SPRINT03 @UC13 @CCO-SRO
Funcionalidade: UC13 - Dados CCO CIRCULAÇÃO - SRO

  Cenário: CT01 - Valida campos do sinistro, veiculos e liberação como readOnly
    Dado que eu esteja logado com usuário CCO
    E cadastro um novo Sinistro manual com Veículo na linha 1 e datas de liberação
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Dados do CCO Circulação
    Entao valido que os dados do sinistro foram carregados no formulário

  Cenario: CT02 - Edita campos do sinistro liberação como readOnly
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Dados do CCO Circulação
    Entao edito os dados do sinistro na visão do SRO
    E clico em Salvar Informações na Edicao do SRO
    Então deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    E valido que os dados do sinistro foram carregados no formulário como readonly na visao do SRO com os dados editado



