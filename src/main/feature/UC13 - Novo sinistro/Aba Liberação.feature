#language: pt

@SRO @WEB @SPRINT02 @UC13
Funcionalidade: UC13 - Aba Liberação

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando seleciono a TAB de sinistros Abertos

  Cenario: CT01 - Exclusão de uma previsão de liberação
    Dado cadastro um novo Sinistro manual sem Veículo e Liberação
    Entao clico na tab Liberação
    Dado insiro uma data de previsão
    Entao consigo excluir a data de previsão inserida na linha 1
    E a data de previsão não deve mais aparecer na tela

  Cenario: CT02 - Validar que antes do preenchimento do campo data de liberação final o botão Salvar informações é apresentado
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Liberação
    Dado que a previsão final não está preenchida
    Entao o botão Salvar informações está sendo apresentado
    E o botão Salvar e Encerrar não está sendo apresentado

  Cenario: CT03 - Preenchimento parcial da Aba Liberação
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao clico na tab Liberação
    Dado que preencho a Aba liberação parcialmente
    E clico no botão Salvar Informações
    E deve exibir a snackBar com a mensagem "Informações salvas com sucesso."
    Entao clico na tab Sinistro na página de novo sinistro
    E clico na tab Liberação
    Entao valido que os dados parcialmente preenchidos estão na tela
