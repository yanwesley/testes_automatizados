#language: pt
@SRO @WEB @SPRINT02 @UC13
Funcionalidade: UC13 - Adicionar Informações Sinistro - Responsáveis

  Cenario: CT01 - Inclusão de um novo Responsável
    Dado que eu crie um novo sinistro sem veiculos via API
    E que eu esteja logado com usuário CCO VIA
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando seleciono a TAB de sinistros Abertos
    Entao valido que a lista de sinistros não está vazia
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    Entao deve exibir a seção de Responsáveis adicionais
    Quando seleciono a opção Sim para a pergunta Foram adicionados responsáveis?
    E clico no botão Adicionar responsável
    Entao deve exibir o modal de Acionamento de responsáveis
    Quando preencher todos os campos do modal de Acionamento de responsáveis
    E clico no botão Salvar
    Entao o responsável deve ser inserido na seção de Responsáveis adicionados

  Cenario: CT02 - Edição do Responsável adicionado
    Dado que existe um Responsável
    Quando clicar no botão Editar do responsável 1
    Entao deve ser alterado os dados do Responsável
    E clico no botão Salvar
    Então deve exibir a snackBar com a mensagem 'Responsável editado com sucesso'
    E os dados do responsável foram editado com sucesso

  Cenario: CT03 - Exclusão do Responsável adicionado
    Dado que existe um Responsável
    Quando clicar no botão Excluir
    Entao deve exibir o modal para excluir com a mensagem 'Deseja excluir o responsável?'
    Quando confirmo a exclusão do Responsável
    Entao deve exibir a snackBar com a mensagem 'Responsável excluído com sucesso'
    E o Responsável deve ser excluído

  Cenario: CT04 - Cancelar a adição de um novo responsável
    Quando seleciono a opção Sim para a pergunta Foram adicionados responsáveis?
    E clico no botão Adicionar responsável
    Entao deve exibir o modal de Acionamento de responsáveis
    Quando clico no botão cancelar
    Entao deve exibir a seção de Responsáveis adicionais

