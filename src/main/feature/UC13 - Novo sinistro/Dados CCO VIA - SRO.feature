#language: pt

@SRO @WEB @SPRINT03 @UC13 @TABSRO
Funcionalidade: UC13 - Dados CCO VIA - SRO

  Cenario: CT-001 SRO validando os dados preenchidos pelo CCO VIA
    Dado que eu crie um novo sinistro sem veiculos via API
    Entao que eu esteja logado com usuário CCO VIA
    E preencho os dados do Sinistro com o CCO VIA
    Dado que eu esteja logado com usuário SRO
    E acesso a TAB Sinistros
    E clicar no botão Editar do sinistro ordenado por ordem decrescente
    E clico na tab Dados CCO VIA
    Entao o responsável deve ser inserido na seção de Responsáveis adicionados com os dados ReadOnly
    E exibir a máquina na tabela de máquinas com os dados ReadOnly
    E a turma deve ser inserida na seção de Acionamento de Turmas com os dados ReadOnly
