#language: pt

@SRO @WEB @SPRINT01 @UC13
Funcionalidade: UC13 - Novo sinistro - adicionar veiculos de terceiros
  Não está fazendo validação com backend

  Contexto:
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Sinistros
    Entao deve exibir a pagina de listagem de sinistros
    Quando clico no botão Incluir novo sinistro
    Entao deve exibir a página para selecionar o tipo de entrada do novo sinistro
    E clico no botão Dados Manuais

  Cenario: CT01 - Usuário informa que existem veículos de terceiro
    Quando informo que existem veículos de terceiro
    Entao deve exibir os campos para adicionar, remove ou editar

  Cenario: CT02 - Usuário informa que não existem veículos de terceiro
    Quando informo que não existem veículos de terceiro
    Entao não deve exibir os campos para adicionar, remove ou editar

  Cenario: CT03 - Incluir veículo de terceiro
    Quando informo que existem veículos de terceiro
    E clico no botão adicionar mais um veículo
    Entao deve exibir a modal para adicionar ou editar veiculo
    E preencho os dados para adicionar um novo veículo
    E clico em salvar modal de adicionar adicionar ou editar veículo
    Entao deve exibir a snackBar com a mensagem "Veículo adicionado com sucesso."
    E exibir o veiculo na tabela de veiculos de terceiro

  Cenario: CT04 - Editar veículo de terceiro
    Quando informo que existem veículos de terceiro
    E clico no botão adicionar mais um veículo
    Entao deve exibir a modal para adicionar ou editar veiculo
    E preencho os dados para adicionar um novo veículo
    E clico em salvar modal de adicionar adicionar ou editar veículo
    Entao deve exibir a snackBar com a mensagem "Veículo adicionado com sucesso."
    E exibir o veiculo na tabela de veiculos de terceiro
    Entao clico para editar o veiculo adicionado
    E altero os dados do veiculos
    E clico em salvar modal de adicionar adicionar ou editar veículo
    Entao deve exibir a snackBar com a mensagem "Veículo alterado com sucesso."
    E exibir o veiculo na tabela de veiculos de terceiro editado

  Cenario: CT05 - Excluir veículo de terceiro
    Quando informo que existem veículos de terceiro
    E clico no botão adicionar mais um veículo
    Entao deve exibir a modal para adicionar ou editar veiculo
    E preencho os dados para adicionar um novo veículo
    E clico em salvar modal de adicionar adicionar ou editar veículo
    Entao deve exibir a snackBar com a mensagem "Veículo adicionado com sucesso."
    E exibir o veiculo na tabela de veiculos de terceiro
    Entao clico para excluir o veículo adicionado
    E deve exibir o modal para excluir com a mensagem "Deseja realmente excluir o veículo?"
    Quando clico em confirmar modal de excluir adicionar ou editar veículo
    Entao deve exibir a snackBar com a mensagem "Veículo removido com sucesso."
    E remover o veiculo na tabela de veículos de terceiro
