#language: pt

@SRO @UC11 @WEB @SPRINT11 @DOCUMENTACAO
Funcionalidade: UC11 - SRO Informações - Documentação

  Cenário: CT01 - Preencher os dados sobre o setor SRO no Administração
    Dado que eu navegue até o Administração
    E acesso a TAB SRO
    E acesso a TAB Documentação no Administração
    Então preencho a Documentação oficial do projeto
    E clico no botão Salvar Informações do Sobre a SRO
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que as informações de Documentação oficial do projeto no Administração persistiram

  Cenário: CT02 - Valida os dados sobre o setor SRO no SRO
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Informações
    E acesso a TAB DOCUMENTAÇÃO no SRO
    Então valido as informações de Documentação oficial do projeto  no SRO
