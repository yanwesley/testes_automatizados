#language: pt

@SRO @UC11 @WEB @SPRINT11 @SOBRESETOR
Funcionalidade: UC11 - SRO Informações - Sobre o setor SRO no Administração

  Cenário: CT01 - Preencher os dados sobre o setor SRO no Administração
    Dado que eu navegue até o Administração
    E acesso a TAB SRO
    E acesso a TAB SOBRE no Administração
    Então preencho os dados sobre o setor no Administração
    E clico no botão Salvar Informações do Sobre a SRO
    Entao deve exibir a snackBar com a mensagem "Informações salvas com sucesso"
    E valido que as informações de sobre o setor no Administração persistiram

  Cenário: CT02 - Valida os dados sobre o setor SRO no SRO
    Dado que eu esteja logado com usuário CCO
    E acesso a TAB Informações
    E acesso a TAB SOBRE no SRO
    Então valido as informações sobre o setor no SRO
