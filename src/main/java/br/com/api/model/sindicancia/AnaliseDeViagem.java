package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class AnaliseDeViagem {
    private Faker faker = new Faker();
    private String numeroViagem;
    private String comentarioAnaliseDeViagem;
    private String comentarioAnaliseDeViagemComparativaUm;
    private String comentarioAnaliseDeViagemComparativaDois;
    private String comentarioLogDaLocomotiva;

    public AnaliseDeViagem() {
        numeroViagem = String.valueOf(getRandom(null));
        comentarioAnaliseDeViagem = getRandomCharacters(2000);
        comentarioAnaliseDeViagemComparativaUm = getRandomCharacters(2000);
        comentarioAnaliseDeViagemComparativaDois = getRandomCharacters(2000);
        comentarioLogDaLocomotiva = getRandomCharacters(2000);
    }


    public String getComentarioAnaliseDeViagem() {
        return comentarioAnaliseDeViagem;
    }

    public void setComentarioAnaliseDeViagem(String comentarioAnaliseDeViagem) {
        this.comentarioAnaliseDeViagem = comentarioAnaliseDeViagem;
    }

    public String getNumeroViagem() {
        return numeroViagem;
    }

    public void setNumeroViagem(String numeroViagem) {
        this.numeroViagem = numeroViagem;
    }

    public String getComentarioAnaliseDeViagemComparativaUm() {
        return comentarioAnaliseDeViagemComparativaUm;
    }

    public void setComentarioAnaliseDeViagemComparativaUm(String comentarioAnaliseDeViagemComparativaUm) {
        this.comentarioAnaliseDeViagemComparativaUm = comentarioAnaliseDeViagemComparativaUm;
    }

    public String getComentarioAnaliseDeViagemComparativaDois() {
        return comentarioAnaliseDeViagemComparativaDois;
    }

    public void setComentarioAnaliseDeViagemComparativaDois(String comentarioAnaliseDeViagemComparativaDois) {
        this.comentarioAnaliseDeViagemComparativaDois = comentarioAnaliseDeViagemComparativaDois;
    }

    public String getComentarioLogDaLocomotiva() {
        return comentarioLogDaLocomotiva;
    }

    public void setComentarioLogDaLocomotiva(String comentarioLogDaLocomotiva) {
        this.comentarioLogDaLocomotiva = comentarioLogDaLocomotiva;
    }


}
