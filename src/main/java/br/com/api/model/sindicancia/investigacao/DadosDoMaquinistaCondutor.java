package br.com.api.model.sindicancia.investigacao;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.*;

public class DadosDoMaquinistaCondutor {

    Faker faker;
    private boolean oMaquinistaEDaRumo;
    private String nomeDoMaquinista;
    private String documento;
    private String CS;
    private String TESimuladoAssociadaACausa;
    private String testesSofridosMesAtual;
    private String testesSofridosMesM1;
    private String testesSofridosMesM2;
    private String testesSofridosRelacionadoComOAcidenteMesAtual;
    private String testesSofridosRelacionadoComOAcidenteMesM1;
    private String testesSofridosRelacionadoComOAcidenteMesM2;
    private String outrosTestesInsatisfatoriosMesAtual;
    private String outrosTestesInsatisfatoriosMesM1;
    private String outrosTestesInsatisfatoriosMesM2;
    private String notaProvaMesAtual;
    private String notaProvaMesM1;
    private String notaProvaMesM2;
    private String dataUltimaReciclagemOuTreinamentoRO;
    private String notaUltimaReciclagemRO;
    private String dataUltimaReciclagemOuTreinamentoPORelacionadoAoAcidente;
    private String notaUltimaReciclagemPORelacionadoAoAcidente;
    private String questoesRespondidasIncorretamente;
    private String horasDeServicosNoMomentoDoAcidente;
    private String diasAposOUltimoDescansoSemanal;
    private String mesesAposAsUltimasFerias;

    public DadosDoMaquinistaCondutor() {
        faker = new Faker(new Locale("pt-BR"));
        oMaquinistaEDaRumo = faker.bool().bool();
        nomeDoMaquinista = "Raul Herculano Fagundes";
        documento = getRandomCharacters(30);

        TESimuladoAssociadaACausa = getRandomCharacters(30);
        testesSofridosMesAtual = String.valueOf(getRandom(null));
        testesSofridosMesM1 = String.valueOf(getRandom(null));
        testesSofridosMesM2 = String.valueOf(getRandom(null));

        testesSofridosRelacionadoComOAcidenteMesAtual = String.valueOf(getRandom(null));
        testesSofridosRelacionadoComOAcidenteMesM1 = String.valueOf(getRandom(null));
        testesSofridosRelacionadoComOAcidenteMesM2 = String.valueOf(getRandom(null));

        outrosTestesInsatisfatoriosMesAtual = String.valueOf(getRandom(null));
        outrosTestesInsatisfatoriosMesM1 = String.valueOf(getRandom(null));
        outrosTestesInsatisfatoriosMesM2 = String.valueOf(getRandom(null));

        notaProvaMesAtual = String.valueOf(getRandom(null));
        notaProvaMesM1 = String.valueOf(getRandom(null));
        notaProvaMesM2 = String.valueOf(getRandom(null));

        dataUltimaReciclagemOuTreinamentoRO = getDay();
        notaUltimaReciclagemRO = String.valueOf(getRandom(null));

        dataUltimaReciclagemOuTreinamentoPORelacionadoAoAcidente = getDay();
        notaUltimaReciclagemPORelacionadoAoAcidente = String.valueOf(getRandom(null));

        questoesRespondidasIncorretamente = getRandomCharacters(2000);

        horasDeServicosNoMomentoDoAcidente = String.valueOf(getRandom(null));
        diasAposOUltimoDescansoSemanal = String.valueOf(getRandom(null));
        mesesAposAsUltimasFerias = String.valueOf(getRandom(null));
    }

    public boolean isoMaquinistaEDaRumo() {
        return oMaquinistaEDaRumo;
    }

    public String getNomeDoMaquinista() {
        return nomeDoMaquinista;
    }

    public void setNomeDoMaquinista(String nomeDoMaquinista) {
        this.nomeDoMaquinista = nomeDoMaquinista;
    }

    public String getDocumento() {
        return documento;
    }

    public String getCS() {
        return CS;
    }

    public void setCS(String CS) {
        this.CS = CS;
    }

    public String getTESimuladoAssociadaACausa() {
        return TESimuladoAssociadaACausa;
    }

    public String getTestesSofridosMesAtual() {
        return testesSofridosMesAtual;
    }

    public String getTestesSofridosMesM1() {
        return testesSofridosMesM1;
    }

    public String getTestesSofridosMesM2() {
        return testesSofridosMesM2;
    }

    public String getTestesSofridosRelacionadoComOAcidenteMesAtual() {
        return testesSofridosRelacionadoComOAcidenteMesAtual;
    }

    public String getTestesSofridosRelacionadoComOAcidenteMesM1() {
        return testesSofridosRelacionadoComOAcidenteMesM1;
    }

    public String getTestesSofridosRelacionadoComOAcidenteMesM2() {
        return testesSofridosRelacionadoComOAcidenteMesM2;
    }

    public String getOutrosTestesInsatisfatoriosMesAtual() {
        return outrosTestesInsatisfatoriosMesAtual;
    }

    public String getOutrosTestesInsatisfatoriosMesM1() {
        return outrosTestesInsatisfatoriosMesM1;
    }

    public String getOutrosTestesInsatisfatoriosMesM2() {
        return outrosTestesInsatisfatoriosMesM2;
    }

    public String getNotaProvaMesAtual() {
        return notaProvaMesAtual;
    }

    public String getNotaProvaMesM1() {
        return notaProvaMesM1;
    }

    public String getNotaProvaMesM2() {
        return notaProvaMesM2;
    }

    public String getDataUltimaReciclagemOuTreinamentoRO() {
        return dataUltimaReciclagemOuTreinamentoRO;
    }

    public String getNotaUltimaReciclagemRO() {
        return notaUltimaReciclagemRO;
    }

    public String getDataUltimaReciclagemOuTreinamentoPORelacionadoAoAcidente() {
        return dataUltimaReciclagemOuTreinamentoPORelacionadoAoAcidente;
    }

    public String getNotaUltimaReciclagemPORelacionadoAoAcidente() {
        return notaUltimaReciclagemPORelacionadoAoAcidente;
    }

    public String getQuestoesRespondidasIncorretamente() {
        return questoesRespondidasIncorretamente;
    }

    public String getHorasDeServicosNoMomentoDoAcidente() {
        return horasDeServicosNoMomentoDoAcidente;
    }

    public String getDiasAposOUltimoDescansoSemanal() {
        return diasAposOUltimoDescansoSemanal;
    }

    public String getMesesAposAsUltimasFerias() {
        return mesesAposAsUltimasFerias;
    }

}
