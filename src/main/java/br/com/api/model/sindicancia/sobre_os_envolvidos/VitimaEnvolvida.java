package br.com.api.model.sindicancia.sobre_os_envolvidos;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class VitimaEnvolvida {
    private Faker faker;
    private String nomeCompleto;
    private String RG;
    private String CPF;
    private boolean fatal;
    private String localDoObitoIndex;
    private String localDoObitoValue;
    private boolean desmembramento;
    private String observacao;

    public VitimaEnvolvida() {
        faker = new Faker(new Locale("pt-BR"));
        nomeCompleto = getRandomCharacters(512);
        observacao = getRandomCharacters(2000);
        RG = String.valueOf(getRandom(null));
        CPF = "456.752.000-96";
        fatal = faker.bool().bool();
        localDoObitoIndex = faker.options().option("NO_LOCAL", "A_CAMINHO_DO_HOSPITAL", "POSTERIORMENTE");
        desmembramento = faker.bool().bool();
    }

    public String getLocalDoObitoValue() {
        return localDoObitoValue;
    }

    public void setLocalDoObitoValue(String localDoObitoValue) {
        this.localDoObitoValue = localDoObitoValue;
    }

    public String getObservacao() {
        return observacao;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public String getRG() {
        return RG;
    }

    public String getCPF() {
        return CPF;
    }

    public boolean isFatal() {
        return fatal;
    }

    public String getLocalDoObitoIndex() {
        return localDoObitoIndex;
    }

    public boolean isDesmembramento() {
        return desmembramento;
    }

}
