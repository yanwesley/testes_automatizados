package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class ReprovarSindicancia {
    Faker faker;
    private boolean cipiaVia;
    private boolean cipiaOperacao;
    private boolean cipiaMatRodante;
    private boolean cipiaTecnica;
    private boolean segPatrimonial;
    private String justificativa;
    private String justificativaAnulacao;

    public ReprovarSindicancia() {
        faker = new Faker(new Locale("pt-BR"));
        cipiaVia = faker.bool().bool();
        cipiaOperacao = faker.bool().bool();
        cipiaMatRodante = !cipiaOperacao;
        cipiaTecnica = faker.bool().bool();
        segPatrimonial = faker.bool().bool();
        justificativa = getRandomCharacters(2000);
        justificativaAnulacao = getRandomCharacters(2000);
    }

    public boolean isCipiaVia() {
        return cipiaVia;
    }

    public void setCipiaVia(boolean cipiaVia) {
        this.cipiaVia = cipiaVia;
    }

    public String getJustificativaAnulacao() {
        return justificativaAnulacao;
    }

    public boolean isCipiaOperacao() {
        return cipiaOperacao;
    }

    public void setCipiaOperacao(boolean cipiaOperacao) {
        this.cipiaOperacao = cipiaOperacao;
    }

    public boolean isCipiaMatRodante() {
        return cipiaMatRodante;
    }

    public void setCipiaMatRodante(boolean cipiaMatRodante) {
        this.cipiaMatRodante = cipiaMatRodante;
    }

    public boolean isCipiaTecnica() {
        return cipiaTecnica;
    }

    public void setCipiaTecnica(boolean cipiaTecnica) {
        this.cipiaTecnica = cipiaTecnica;
    }

    public boolean isSegPatrimonial() {
        return segPatrimonial;
    }

    public void setSegPatrimonial(boolean segPatrimonial) {
        this.segPatrimonial = segPatrimonial;
    }

    public String getJustificativa() {
        return justificativa;
    }
}


