package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.*;

public class Trilho {

    private Faker faker;
    private int perfilTrilhoIndex;
    private String perfilTrilhoValue;
    private int pernaIndex;
    private String pernaValue;
    private int filaIndex;
    private String filaValue;
    private int fabricanteIndex;
    private String fabricanteValue;
    private String dataFabricacaoTrilho;
    private String comprimentoDaBarra;
    private int condicaoSuperficialDoTrilhoIndex;
    private String condicaoSuperficialDoTrilhoValue;
    private int curtaLongaIndex;
    private String curtaLongaValue;
    private int estadoDoPatimIndex;
    private String estadoDoPatimValue;
    private String desgasteVertical;
    private String desgasteHorizontalInterno;
    private String desgasteHorizontalExterno;
    private String wModuloDeResistenciaTR;
    private boolean existeCaminhamentoDeTrilho;
    private boolean aLinhaEstavaLubrificada;
    private boolean houveFraturaDoTrilho;
    private boolean ocorreuDuranteAPassagemDoTrem;
    private int qualTipoDoDefeitoIndex;
    private String qualTipoDoDefeitoValue;
    private boolean fraturaEmSolda;
    private boolean qualEraACondicaoDaSolda;
    private int qualTipoDaSoldaIndex;
    private String qualTipoDaSoldaValue;
    private String comentarioSolda;
    private boolean fraturaEmJunta;
    private int qualEraACondicaoDaJunta;
    private String distancia1Furo;
    private String distanciaEntreFuro;
    private String comentarioJunta;
    private boolean fraturaEmReducao;
    private int qualTipoDaReducaoIndex;
    private String qualTipoDaReducaoValue;
    private boolean haviaDTQNoLocal;
    private boolean estavaOperacionalDTQ;
    private boolean atuouDTQ;
    private String comentarioDTQ;
    private String temperaturaDaDeteccao;
    private String temperaturaMinima;
    private String temperaturaMaxima;

    public Trilho() {
        faker = new Faker(new Locale("pt-BR"));
        perfilTrilhoIndex = getRandom(10);
        pernaIndex = getRandom(5);
        filaIndex = faker.options().option(0, 1, 2);
        fabricanteIndex = getRandom(10);
        dataFabricacaoTrilho = getDay();
        comprimentoDaBarra = String.valueOf(getRandom(null));
        condicaoSuperficialDoTrilhoIndex = getRandom(6);
        curtaLongaIndex = getRandom(1);
        estadoDoPatimIndex = getRandom(1);
        desgasteVertical = String.valueOf(getRandom(null));
        desgasteHorizontalInterno = String.valueOf(getRandom(null));
        desgasteHorizontalExterno = String.valueOf(getRandom(null));
        wModuloDeResistenciaTR = getRandomCharacters(255);
        existeCaminhamentoDeTrilho = faker.bool().bool();
        aLinhaEstavaLubrificada = faker.bool().bool();
        houveFraturaDoTrilho = faker.bool().bool();
        ocorreuDuranteAPassagemDoTrem = faker.bool().bool();
        qualTipoDoDefeitoIndex = getRandom(10);
        fraturaEmSolda = faker.bool().bool();
        qualEraACondicaoDaSolda = faker.bool().bool();
        qualTipoDaSoldaIndex = faker.options().option(0, 1, 2);
        comentarioSolda = getRandomCharacters(4000);
        fraturaEmJunta = faker.bool().bool();
        qualEraACondicaoDaJunta = faker.options().option(0, 1, 2);
        distancia1Furo = String.valueOf(getRandom(null));
        distanciaEntreFuro = String.valueOf(getRandom(null));
        comentarioJunta = comentarioSolda = getRandomCharacters(4000);
        fraturaEmReducao = faker.bool().bool();
        qualTipoDaReducaoIndex = getRandom(10);
        haviaDTQNoLocal = faker.bool().bool();
        estavaOperacionalDTQ = faker.bool().bool();
        atuouDTQ = faker.bool().bool();
        comentarioDTQ = comentarioSolda = getRandomCharacters(4000);
        temperaturaDaDeteccao = String.valueOf(getRandom(null));
        temperaturaMinima = String.valueOf(getRandom(null));
        temperaturaMaxima = String.valueOf(getRandom(null));
    }

    public int getPerfilTrilhoIndex() {
        return perfilTrilhoIndex;
    }

    public String getPerfilTrilhoValue() {
        return perfilTrilhoValue;
    }

    public void setPerfilTrilhoValue(String perfilTrilhoValue) {
        this.perfilTrilhoValue = perfilTrilhoValue;
    }

    public int getPernaIndex() {
        return pernaIndex;
    }

    public String getPernaValue() {
        return pernaValue;
    }

    public void setPernaValue(String pernaValue) {
        this.pernaValue = pernaValue;
    }

    public int getFilaIndex() {
        return filaIndex;
    }

    public String getFilaValue() {
        return filaValue;
    }

    public void setFilaValue(String filaValue) {
        this.filaValue = filaValue;
    }

    public int getFabricanteIndex() {
        return fabricanteIndex;
    }

    public String getFabricanteValue() {
        return fabricanteValue;
    }

    public void setFabricanteValue(String fabricanteValue) {
        this.fabricanteValue = fabricanteValue;
    }

    public String getDataFabricacaoTrilho() {
        return dataFabricacaoTrilho;
    }

    public String getComprimentoDaBarra() {
        return comprimentoDaBarra;
    }

    public int getCondicaoSuperficialDoTrilhoIndex() {
        return condicaoSuperficialDoTrilhoIndex;
    }

    public String getCondicaoSuperficialDoTrilhoValue() {
        return condicaoSuperficialDoTrilhoValue;
    }

    public void setCondicaoSuperficialDoTrilhoValue(String condicaoSuperficialDoTrilhoValue) {
        this.condicaoSuperficialDoTrilhoValue = condicaoSuperficialDoTrilhoValue;
    }

    public int getCurtaLongaIndex() {
        return curtaLongaIndex;
    }

    public String getCurtaLongaValue() {
        return curtaLongaValue;
    }

    public void setCurtaLongaValue(String curtaLongaValue) {
        this.curtaLongaValue = curtaLongaValue;
    }

    public int getEstadoDoPatimIndex() {
        return estadoDoPatimIndex;
    }

    public String getEstadoDoPatimValue() {
        return estadoDoPatimValue;
    }

    public void setEstadoDoPatimValue(String estadoDoPatimValue) {
        this.estadoDoPatimValue = estadoDoPatimValue;
    }

    public String getDesgasteVertical() {
        return desgasteVertical;
    }

    public String getDesgasteHorizontalInterno() {
        return desgasteHorizontalInterno;
    }

    public String getDesgasteHorizontalExterno() {
        return desgasteHorizontalExterno;
    }

    public String getwModuloDeResistenciaTR() {
        return wModuloDeResistenciaTR;
    }

    public boolean isExisteCaminhamentoDeTrilho() {
        return existeCaminhamentoDeTrilho;
    }

    public boolean isaLinhaEstavaLubrificada() {
        return aLinhaEstavaLubrificada;
    }

    public boolean isHouveFraturaDoTrilho() {
        return houveFraturaDoTrilho;
    }

    public boolean isOcorreuDuranteAPassagemDoTrem() {
        return ocorreuDuranteAPassagemDoTrem;
    }

    public int getQualTipoDoDefeitoIndex() {
        return qualTipoDoDefeitoIndex;
    }

    public String getQualTipoDoDefeitoValue() {
        return qualTipoDoDefeitoValue;
    }

    public void setQualTipoDoDefeitoValue(String qualTipoDoDefeitoValue) {
        this.qualTipoDoDefeitoValue = qualTipoDoDefeitoValue;
    }

    public boolean isFraturaEmSolda() {
        return fraturaEmSolda;
    }

    public boolean isQualEraACondicaoDaSolda() {
        return qualEraACondicaoDaSolda;
    }

    public int getQualTipoDaSoldaIndex() {
        return qualTipoDaSoldaIndex;
    }

    public String getQualTipoDaSoldaValue() {
        return qualTipoDaSoldaValue;
    }

    public void setQualTipoDaSoldaValue(String qualTipoDaSoldaValue) {
        this.qualTipoDaSoldaValue = qualTipoDaSoldaValue;
    }

    public String getComentarioSolda() {
        return comentarioSolda;
    }

    public boolean isFraturaEmJunta() {
        return fraturaEmJunta;
    }

    public int getQualEraACondicaoDaJunta() {
        return qualEraACondicaoDaJunta;
    }

    public String getDistancia1Furo() {
        return distancia1Furo;
    }

    public String getDistanciaEntreFuro() {
        return distanciaEntreFuro;
    }

    public String getComentarioJunta() {
        return comentarioJunta;
    }

    public boolean isFraturaEmReducao() {
        return fraturaEmReducao;
    }

    public int getQualTipoDaReducaoIndex() {
        return qualTipoDaReducaoIndex;
    }

    public String getQualTipoDaReducaoValue() {
        return qualTipoDaReducaoValue;
    }

    public void setQualTipoDaReducaoValue(String qualTipoDaReducaoValue) {
        this.qualTipoDaReducaoValue = qualTipoDaReducaoValue;
    }

    public boolean isHaviaDTQNoLocal() {
        return haviaDTQNoLocal;
    }

    public boolean isEstavaOperacionalDTQ() {
        return estavaOperacionalDTQ;
    }

    public boolean isAtuouDTQ() {
        return atuouDTQ;
    }

    public String getComentarioDTQ() {
        return comentarioDTQ;
    }

    public String getTemperaturaDaDeteccao() {
        return temperaturaDaDeteccao;
    }

    public String getTemperaturaMinima() {
        return temperaturaMinima;
    }

    public String getTemperaturaMaxima() {
        return temperaturaMaxima;
    }
}
