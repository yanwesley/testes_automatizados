package br.com.api.model.sindicancia.questionario;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class QuestionarioCheckListAcidente {
    private Faker faker;
    private boolean houveFalhaHumana;
    private String descrevaBrevementeOAcidente;
    private int voceAcreditaQueOAcidenteFoiCausadoPorIndex;
    private String voceAcreditaQueOAcidenteFoiCausadoPorValue;
    private boolean voceConsideraQueEstavaAtentoDuranteAExecucaoDaTarefa;
    private boolean voceEstavaCansadoNoMomentoDoAcidente;
    private boolean voceDescansouCorretamenteNoDiaAnterior;
    private boolean haviaUmProcedimentoParaAquiloQueVoceEstavaExecutando;
    private boolean voceConheciaEsteProcedimento;
    private String descrevaOProcedimentoAbaixo;
    private boolean voceSeConsideraAptoARealizarEssaTarefa;
    private boolean voceEstavaComDuvidaDoQueEstavaFazendo;
    private int voceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaIndex;
    private String voceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaValue;
    private int quemTeDeuOTreinamentoIndex;
    private String quemTeDeuOTreinamentoValue;
    private boolean alguemPediuParaVoceDescumprirAlgumProcedimentoParaExecutarEssaTarefa;
    private int voceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezIndex;
    private String voceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezValue;
    private boolean distracaoDesatencao;
    private boolean algumFatorExterno;
    private boolean faltaDeConhecimentoDoProcedimento;
    private boolean execucaoErradaDoProcedimento;
    private boolean faltaDeProcedimento;
    private boolean maQualidadeNoTreinamento;
    private boolean naoTeveTreinamento;
    private boolean faltaDeEquipamentosParaExecutarATarefaExecucaoErradaDoProcedimento;
    private boolean voceDeveriaTerExecutadoATarefaSemEquipamentos;
    private boolean pressaoParaExecutarATarefa;
    private String qualASuaSugestaoParaQueAcidentesComoEsseNaoOcorressemMaisNaNossaCompanhia;

    public QuestionarioCheckListAcidente() {
        faker = new Faker(new Locale("pt-BR"));

        houveFalhaHumana = true;
        descrevaBrevementeOAcidente = getRandomCharacters(2000);
        voceAcreditaQueOAcidenteFoiCausadoPorIndex = faker.options().option(0, 1);
        voceConsideraQueEstavaAtentoDuranteAExecucaoDaTarefa = faker.bool().bool();
        voceEstavaCansadoNoMomentoDoAcidente = faker.bool().bool();
        voceDescansouCorretamenteNoDiaAnterior = faker.bool().bool();
        haviaUmProcedimentoParaAquiloQueVoceEstavaExecutando = faker.bool().bool();
        voceConheciaEsteProcedimento = faker.bool().bool();
        descrevaOProcedimentoAbaixo = getRandomCharacters(2000);
        voceSeConsideraAptoARealizarEssaTarefa = faker.bool().bool();
        voceEstavaComDuvidaDoQueEstavaFazendo = faker.bool().bool();
        voceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaIndex = faker.options().option(0, 1, 2);
        quemTeDeuOTreinamentoIndex = faker.options().option(0, 1);
        alguemPediuParaVoceDescumprirAlgumProcedimentoParaExecutarEssaTarefa = faker.bool().bool();
        voceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezIndex = faker.options().option(0, 1, 2);
        distracaoDesatencao = faker.bool().bool();
        algumFatorExterno = faker.bool().bool();
        faltaDeConhecimentoDoProcedimento = faker.bool().bool();
        execucaoErradaDoProcedimento = faker.bool().bool();
        faltaDeProcedimento = faker.bool().bool();
        maQualidadeNoTreinamento = faker.bool().bool();
        naoTeveTreinamento = faker.bool().bool();
        faltaDeEquipamentosParaExecutarATarefaExecucaoErradaDoProcedimento = faker.bool().bool();
        voceDeveriaTerExecutadoATarefaSemEquipamentos = faker.bool().bool();
        pressaoParaExecutarATarefa = faker.bool().bool();
        qualASuaSugestaoParaQueAcidentesComoEsseNaoOcorressemMaisNaNossaCompanhia = getRandomCharacters(2000);

    }

    public String getQuemTeDeuOTreinamentoValue() {
        return quemTeDeuOTreinamentoValue;
    }

    public void setQuemTeDeuOTreinamentoValue(String quemTeDeuOTreinamentoValue) {
        this.quemTeDeuOTreinamentoValue = quemTeDeuOTreinamentoValue;
    }

    public String getVoceAcreditaQueOAcidenteFoiCausadoPorValue() {
        return voceAcreditaQueOAcidenteFoiCausadoPorValue;
    }

    public void setVoceAcreditaQueOAcidenteFoiCausadoPorValue(String voceAcreditaQueOAcidenteFoiCausadoPorValue) {
        this.voceAcreditaQueOAcidenteFoiCausadoPorValue = voceAcreditaQueOAcidenteFoiCausadoPorValue;
    }

    public int getVoceAcreditaQueOAcidenteFoiCausadoPorIndex() {
        return voceAcreditaQueOAcidenteFoiCausadoPorIndex;
    }

    public int getQuemTeDeuOTreinamentoIndex() {
        return quemTeDeuOTreinamentoIndex;
    }

    public boolean isHouveFalhaHumana() {
        return houveFalhaHumana;
    }

    public String getDescrevaBrevementeOAcidente() {
        return descrevaBrevementeOAcidente;
    }


    public boolean isVoceConsideraQueEstavaAtentoDuranteAExecucaoDaTarefa() {
        return voceConsideraQueEstavaAtentoDuranteAExecucaoDaTarefa;
    }

    public boolean isVoceEstavaCansadoNoMomentoDoAcidente() {
        return voceEstavaCansadoNoMomentoDoAcidente;
    }

    public boolean isVoceDescansouCorretamenteNoDiaAnterior() {
        return voceDescansouCorretamenteNoDiaAnterior;
    }

    public boolean isHaviaUmProcedimentoParaAquiloQueVoceEstavaExecutando() {
        return haviaUmProcedimentoParaAquiloQueVoceEstavaExecutando;
    }

    public boolean isVoceConheciaEsteProcedimento() {
        return voceConheciaEsteProcedimento;
    }

    public String getDescrevaOProcedimentoAbaixo() {
        return descrevaOProcedimentoAbaixo;
    }

    public boolean isVoceSeConsideraAptoARealizarEssaTarefa() {
        return voceSeConsideraAptoARealizarEssaTarefa;
    }

    public boolean isVoceEstavaComDuvidaDoQueEstavaFazendo() {
        return voceEstavaComDuvidaDoQueEstavaFazendo;
    }

    public int getVoceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaIndex() {
        return voceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaIndex;
    }

    public String getVoceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaValue() {
        return voceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaValue;
    }

    public void setVoceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaValue(String voceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaValue) {
        this.voceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaValue = voceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaValue;
    }

    public boolean isAlguemPediuParaVoceDescumprirAlgumProcedimentoParaExecutarEssaTarefa() {
        return alguemPediuParaVoceDescumprirAlgumProcedimentoParaExecutarEssaTarefa;
    }

    public int getVoceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezIndex() {
        return voceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezIndex;
    }

    public String getVoceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezValue() {
        return voceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezValue;
    }

    public void setVoceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezValue(String voceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezValue) {
        this.voceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezValue = voceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezValue;
    }

    public boolean isDistracaoDesatencao() {
        return distracaoDesatencao;
    }

    public boolean isAlgumFatorExterno() {
        return algumFatorExterno;
    }

    public boolean isFaltaDeConhecimentoDoProcedimento() {
        return faltaDeConhecimentoDoProcedimento;
    }

    public boolean isExecucaoErradaDoProcedimento() {
        return execucaoErradaDoProcedimento;
    }

    public boolean isFaltaDeProcedimento() {
        return faltaDeProcedimento;
    }

    public boolean isMaQualidadeNoTreinamento() {
        return maQualidadeNoTreinamento;
    }

    public boolean isNaoTeveTreinamento() {
        return naoTeveTreinamento;
    }

    public boolean isFaltaDeEquipamentosParaExecutarATarefaExecucaoErradaDoProcedimento() {
        return faltaDeEquipamentosParaExecutarATarefaExecucaoErradaDoProcedimento;
    }

    public boolean isVoceDeveriaTerExecutadoATarefaSemEquipamentos() {
        return voceDeveriaTerExecutadoATarefaSemEquipamentos;
    }

    public boolean isPressaoParaExecutarATarefa() {
        return pressaoParaExecutarATarefa;
    }

    public String getQualASuaSugestaoParaQueAcidentesComoEsseNaoOcorressemMaisNaNossaCompanhia() {
        return qualASuaSugestaoParaQueAcidentesComoEsseNaoOcorressemMaisNaNossaCompanhia;
    }
}
