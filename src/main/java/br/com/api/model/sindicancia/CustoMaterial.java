package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getDay;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class CustoMaterial {
    private Faker faker;
    private String material;
    private String data;
    private String unidade;
    private long quantidade;
    private long valorUnitario;

    public CustoMaterial() {
        faker = new Faker(new Locale("pt-BR"));
        material = getRandomCharacters(255);
        data = getDay();
        unidade = faker.options().option("Horas", "Unidades");
        quantidade = faker.random().nextInt(0, 999999).longValue();
        valorUnitario = faker.random().nextInt(0, 999999).longValue();
    }

    public String getData() {
        return data;
    }

    public String getUnidade() {
        return unidade;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public long getValorUnitario() {
        return valorUnitario;
    }

    public String getMaterial() {
        return material;
    }
}
