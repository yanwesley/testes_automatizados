package br.com.api.model.sindicancia.investigacao;

import com.github.javafaker.Faker;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class CriticaDeViagem {
    private Faker faker = new Faker();
    private String comentarioConducaoDoTrem;
    private String comentarioCriticaDaViagem;
    private String comentarioConsideracoesFinais;

    public CriticaDeViagem() {
        comentarioConducaoDoTrem = getRandomCharacters(2000);
        comentarioCriticaDaViagem = getRandomCharacters(2000);
        comentarioConsideracoesFinais = getRandomCharacters(2000);
    }

    public String getComentarioConducaoDoTrem() {
        return comentarioConducaoDoTrem;
    }

    public void setComentarioConducaoDoTrem(String comentarioConducaoDoTrem) {
        this.comentarioConducaoDoTrem = comentarioConducaoDoTrem;
    }

    public String getComentarioCriticaDaViagem() {
        return comentarioCriticaDaViagem;
    }

    public void setComentarioCriticaDaViagem(String comentarioCriticaDaViagem) {
        this.comentarioCriticaDaViagem = comentarioCriticaDaViagem;
    }

    public String getComentarioConsideracoesFinais() {
        return comentarioConsideracoesFinais;
    }

    public void setComentarioConsideracoesFinais(String comentarioConsideracoesFinais) {
        this.comentarioConsideracoesFinais = comentarioConsideracoesFinais;
    }

}
