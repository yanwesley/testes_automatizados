package br.com.api.model.sindicancia;

import java.util.ArrayList;
import java.util.List;

public class TabelaAnomalias {
    private int superelevacaoMinima;
    private int superelevacaoMaxima;
    private int velocidadeDeCirculacao;
    private String maiorValorSuperelevacao;
    private List<Integer> superel = new ArrayList<>();
    private List<Integer> efeitoDinamico = new ArrayList<>();
    private List<Integer> bitola = new ArrayList<>();
    private List<Integer> flechaCorda = new ArrayList<>();

    public TabelaAnomalias() {
        superelevacaoMinima = 0;
        superelevacaoMaxima = 51;
        velocidadeDeCirculacao = 16;
        setSuperel();
        setEfeitoDinamico();
        setBitola();
        setFlechaCorda();
    }

    private void setSuperel() {
        for (int i = 0; i <= 50; i++) {
            this.superel.add(i);
        }
    }

    private void setEfeitoDinamico() {
        for (int i = 0; i <= 50; i++) {
            this.efeitoDinamico.add(i);
        }
    }

    private void setBitola() {
        for (int i = 0; i <= 50; i++) {
            this.bitola.add(i);
        }
    }

    private void setFlechaCorda() {
        for (int i = 0; i <= 50; i++) {
            this.flechaCorda.add(i);
        }
    }

    public int getSuperelevacaoMinima() {
        return superelevacaoMinima;
    }

    public int getSuperelevacaoMaxima() {
        return superelevacaoMaxima;
    }

    public int getVelocidadeDeCirculacao() {
        return velocidadeDeCirculacao;
    }

    public String getMaiorValorSuperelevacao() {
        return maiorValorSuperelevacao;
    }

    public List<Integer> getSuperel() {
        return superel;
    }

    public List<Integer> getEfeitoDinamico() {
        return efeitoDinamico;
    }

    public List<Integer> getBitola() {
        return bitola;
    }

    public List<Integer> getFlechaCorda() {
        return flechaCorda;
    }
}
