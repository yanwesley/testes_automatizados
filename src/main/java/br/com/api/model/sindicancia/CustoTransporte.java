package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getDay;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class CustoTransporte {
    private Faker faker;

    private String subTipo;
    private String colaborador;
    private String area;
    private String cidade;
    private String data;
    private long quantidade;
    private long valorUnitario;

    public CustoTransporte() {
        colaborador = "João";
        faker = new Faker(new Locale("pt-BR"));
        area = getRandomCharacters(255);
        cidade = getRandomCharacters(255);
        subTipo = faker.options().option("Hotel", "Pedágio", "Alimentação");
        data = getDay();
        quantidade = faker.random().nextInt(0, 999999).longValue();
        valorUnitario = faker.random().nextInt(0, 999999).longValue();
    }

    public String getCidade() {
        return cidade;
    }

    public String getColaborador() {
        return colaborador;
    }

    public void setColaborador(String colaborador) {
        this.colaborador = colaborador;
    }

    public String getSubTipo() {
        return subTipo;
    }

    public String getData() {
        return data;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public long getValorUnitario() {
        return valorUnitario;
    }

    public String getArea() {
        return area;
    }
}
