package br.com.api.model.sindicancia.investigacao;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;

public class Estacionamento {

    Faker faker;

    private boolean houveDeslocamentoDeAtivos;
    private String quantosVagoesCorreram;
    private String quantosVagoesEstavamComFreioManualAplicado;
    private String vagoesCarregados;
    private String vagoesVazios;
    private boolean aLocomotivaEstavaComFreioManualAplicado;
    private boolean aLocomotivaEstavaCalcada;
    private boolean comoEraOCalco;
    private String quantosVeiculosEstavamDescalcos;
    private boolean haviaFreioManualInoperante;
    private String quantosFreiosManualInoperante;

    public Estacionamento() {
        faker = new Faker(new Locale("pt-BR"));
        houveDeslocamentoDeAtivos = faker.bool().bool();
        quantosVagoesCorreram = String.valueOf(getRandom(null));
        quantosVagoesEstavamComFreioManualAplicado = String.valueOf(getRandom(null));
        vagoesCarregados = String.valueOf(getRandom(null));
        vagoesVazios = String.valueOf(getRandom(null));
        aLocomotivaEstavaComFreioManualAplicado = faker.bool().bool();
        aLocomotivaEstavaCalcada = faker.bool().bool();
        comoEraOCalco = faker.bool().bool();
        quantosVeiculosEstavamDescalcos = String.valueOf(getRandom(null));
        haviaFreioManualInoperante = faker.bool().bool();
        quantosFreiosManualInoperante = String.valueOf(getRandom(null));
    }

    public boolean isHouveDeslocamentoDeAtivos() {
        return houveDeslocamentoDeAtivos;
    }

    public String getQuantosVagoesCorreram() {
        return quantosVagoesCorreram;
    }

    public String getQuantosVagoesEstavamComFreioManualAplicado() {
        return quantosVagoesEstavamComFreioManualAplicado;
    }

    public String getVagoesCarregados() {
        return vagoesCarregados;
    }

    public String getVagoesVazios() {
        return vagoesVazios;
    }

    public boolean isaLocomotivaEstavaComFreioManualAplicado() {
        return aLocomotivaEstavaComFreioManualAplicado;
    }

    public boolean isaLocomotivaEstavaCalcada() {
        return aLocomotivaEstavaCalcada;
    }

    public boolean isComoEraOCalco() {
        return comoEraOCalco;
    }

    public String getQuantosVeiculosEstavamDescalcos() {
        return quantosVeiculosEstavamDescalcos;
    }

    public boolean isHaviaFreioManualInoperante() {
        return haviaFreioManualInoperante;
    }

    public String getQuantosFreiosManualInoperante() {
        return quantosFreiosManualInoperante;
    }
}
