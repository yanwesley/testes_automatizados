package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class CustoColaborador {

    private Faker faker;
    private String colaborador;
    private String turma;
    private String cargo;
    private String horaInicio;
    private String horaFim;
    private long valorTotal;

    public CustoColaborador() {
        faker = new Faker(new Locale("pt-BR"));
        colaborador = "João";
        turma = getRandomCharacters(255);
        horaInicio = "01:00";
        horaFim = "23:00";
        valorTotal = faker.random().nextInt(0, 999999).longValue();
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public String getHoraFim() {
        return horaFim;
    }

    public String getColaborador() {
        return colaborador;
    }

    public void setColaborador(String colaborador) {
        this.colaborador = colaborador;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getTurma() {
        return turma;
    }


    public long getValorTotal() {
        return valorTotal;
    }

}
