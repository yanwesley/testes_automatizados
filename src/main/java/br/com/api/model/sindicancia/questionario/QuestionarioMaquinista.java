package br.com.api.model.sindicancia.questionario;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class QuestionarioMaquinista {
    private Faker faker;
    private String nomeDoMaquinista;
    private String documento;
    private String CS;
    private boolean maquinistaPraticante;
    private boolean acidenteDuranteAObra;
    private String informeOKMDaParadaDaLocomotiva;
    private int haviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeIndex;
    private String haviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeValue;
    private String qualAVelocidadePermitidaNoLocalObservada;
    private int haviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesIndex;
    private String haviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesValue;
    private String qualAVelocidadePermitidaNoLocalNaoObservada;
    private int haviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteIndex;
    private String haviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteValue;
    private String qualCondicaoVistaOuSentida;
    private int haviaAlgumaCondicaoIrregularNoEquipamentoIndex;
    private String qualCondicaoHaviaAlgumaCondicaoIrregularNoEquipamento;
    private String haviaAlgumaCondicaoIrregularNoEquipamentoValue;
    private int foiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremIndex;
    private String foiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremValue;
    private int istoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex;
    private String ondeIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTrem;
    private String istoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue;
    private int houveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArIndex;
    private String houveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArValue;
    private String qualHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeAr;
    private int houveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoIndex;
    private String houveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoValue;
    private String expliqueHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamento;
    private int oTremEstavaIndex;
    private String oTremEstavaValue;
    private int naHoraDoAcidenteAsLocomotivasEstavamEmIndex;
    private String naHoraDoAcidenteAsLocomotivasEstavamEmValue;
    private String qualAAmperagem;

    private int peloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteIndex;
    private String peloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteValue;
    private int haviaAlgumaLocomotivaDeslizandoOuPerdendoForcaIndex;
    private String haviaAlgumaLocomotivaDeslizandoOuPerdendoForcaValue;
    private int issoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex;
    private String issoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue;
    private String ondeIssoTeveAlgumEfeitoNaAcaoDasFolgasDoTrem;
    private int houveVazamentoDoEncanamentoGeralIndex;
    private String houveVazamentoDoEncanamentoGeralValue;
    private String quantasLibrasPorMinuto;
    private int oFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoIndex;
    private String oFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoValue;
    private String emQualKM;
    private String eQualFoiAAplicacao;
    private String peloConhecimentoDoMaquinistaQuantasLocomotivasEstavamComOFreioDinamicoFuncionando;
    private String qualAAmperagemAntesDoDescarrilamento;
    private int oFreioDinamicoEstavaOscilandoIndex;
    private String oFreioDinamicoEstavaOscilandoValue;
    private String quantoOFreioDinamicoEstavaOscilando;
    private int ouveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoIndex;
    private String ouveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoValue;

    private boolean houveDeslizamentoDeRodaAntesOuNoMomentoDoDescarrilamento;

    public QuestionarioMaquinista() {
        faker = new Faker(new Locale("pt-BR"));
        nomeDoMaquinista = "Raul Herculano Fagundes";
        documento = getRandomCharacters(30);
        maquinistaPraticante = faker.bool().bool();
        acidenteDuranteAObra = faker.bool().bool();
        informeOKMDaParadaDaLocomotiva = String.valueOf(getRandom(null));
        haviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeIndex = faker.options().option(0, 1, 2);
        qualAVelocidadePermitidaNoLocalObservada = String.valueOf(getRandom(null));
        haviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesIndex = faker.options().option(0, 1, 2);
        qualAVelocidadePermitidaNoLocalNaoObservada = String.valueOf(getRandom(null));
        haviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteIndex = faker.options().option(0, 1, 2);
        qualCondicaoVistaOuSentida = getRandomCharacters(2000);
        haviaAlgumaCondicaoIrregularNoEquipamentoIndex = faker.options().option(0, 1, 2);
        qualCondicaoHaviaAlgumaCondicaoIrregularNoEquipamento = getRandomCharacters(2000);
        foiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremIndex = faker.options().option(0, 1, 2);
        istoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex = faker.options().option(0, 1, 2);
        ondeIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTrem = getRandomCharacters(2000);
        houveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArIndex = faker.options().option(0, 1, 2);
        qualHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeAr = getRandomCharacters(2000);
        houveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoIndex = faker.options().option(0, 1, 2);
        expliqueHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamento = getRandomCharacters(2000);
        oTremEstavaIndex = faker.options().option(0, 1, 2);
        naHoraDoAcidenteAsLocomotivasEstavamEmIndex = faker.options().option(0, 1);
        qualAAmperagem = String.valueOf(getRandom(null));
        peloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteIndex = faker.options().option(0, 1, 2);
        haviaAlgumaLocomotivaDeslizandoOuPerdendoForcaIndex = faker.options().option(0, 1, 2);
        issoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex = faker.options().option(0, 1, 2);
        ondeIssoTeveAlgumEfeitoNaAcaoDasFolgasDoTrem = getRandomCharacters(2000);
        houveVazamentoDoEncanamentoGeralIndex = faker.options().option(0, 1, 2);
        quantasLibrasPorMinuto = String.valueOf(getRandom(null));
        oFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoIndex = faker.options().option(0, 1, 2);
        emQualKM = String.valueOf(getRandom(null));
        eQualFoiAAplicacao = String.valueOf(getRandom(null));
        peloConhecimentoDoMaquinistaQuantasLocomotivasEstavamComOFreioDinamicoFuncionando = String.valueOf(getRandom(null));
        qualAAmperagemAntesDoDescarrilamento = String.valueOf(getRandom(null));
        oFreioDinamicoEstavaOscilandoIndex = faker.options().option(0, 1, 2);
        quantoOFreioDinamicoEstavaOscilando = String.valueOf(getRandom(null));
        ouveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoIndex = faker.options().option(0, 1, 2);
        houveDeslizamentoDeRodaAntesOuNoMomentoDoDescarrilamento = faker.bool().bool();
    }

    public boolean isHouveDeslizamentoDeRodaAntesOuNoMomentoDoDescarrilamento() {
        return houveDeslizamentoDeRodaAntesOuNoMomentoDoDescarrilamento;
    }

    public int getPeloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteIndex() {
        return peloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteIndex;
    }

    public String getPeloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteValue() {
        return peloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteValue;
    }

    public void setPeloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteValue(String peloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteValue) {
        this.peloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteValue = peloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteValue;
    }

    public String getNomeDoMaquinista() {
        return nomeDoMaquinista;
    }

    public void setNomeDoMaquinista(String nomeDoMaquinista) {
        this.nomeDoMaquinista = nomeDoMaquinista;
    }

    public String getDocumento() {
        return documento;
    }

    public String getCS() {
        return CS;
    }

    public void setCS(String CS) {
        this.CS = CS;
    }

    public boolean isMaquinistaPraticante() {
        return maquinistaPraticante;
    }

    public boolean isAcidenteDuranteAObra() {
        return acidenteDuranteAObra;
    }

    public String getInformeOKMDaParadaDaLocomotiva() {
        return informeOKMDaParadaDaLocomotiva;
    }

    public int getHaviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeIndex() {
        return haviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeIndex;
    }

    public String getHaviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeValue() {
        return haviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeValue;
    }

    public void setHaviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeValue(String haviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeValue) {
        this.haviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeValue = haviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeValue;
    }

    public String getQualAVelocidadePermitidaNoLocalObservada() {
        return qualAVelocidadePermitidaNoLocalObservada;
    }

    public int getHaviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesIndex() {
        return haviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesIndex;
    }

    public String getHaviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesValue() {
        return haviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesValue;
    }

    public void setHaviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesValue(String haviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesValue) {
        this.haviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesValue = haviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesValue;
    }

    public String getQualAVelocidadePermitidaNoLocalNaoObservada() {
        return qualAVelocidadePermitidaNoLocalNaoObservada;
    }

    public int getHaviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteIndex() {
        return haviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteIndex;
    }

    public String getHaviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteValue() {
        return haviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteValue;
    }

    public void setHaviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteValue(String haviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteValue) {
        this.haviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteValue = haviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteValue;
    }

    public String getQualCondicaoVistaOuSentida() {
        return qualCondicaoVistaOuSentida;
    }

    public int getHaviaAlgumaCondicaoIrregularNoEquipamentoIndex() {
        return haviaAlgumaCondicaoIrregularNoEquipamentoIndex;
    }

    public String getQualCondicaoHaviaAlgumaCondicaoIrregularNoEquipamento() {
        return qualCondicaoHaviaAlgumaCondicaoIrregularNoEquipamento;
    }

    public String getHaviaAlgumaCondicaoIrregularNoEquipamentoValue() {
        return haviaAlgumaCondicaoIrregularNoEquipamentoValue;
    }

    public void setHaviaAlgumaCondicaoIrregularNoEquipamentoValue(String haviaAlgumaCondicaoIrregularNoEquipamentoValue) {
        this.haviaAlgumaCondicaoIrregularNoEquipamentoValue = haviaAlgumaCondicaoIrregularNoEquipamentoValue;
    }

    public int getFoiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremIndex() {
        return foiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremIndex;
    }

    public String getFoiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremValue() {
        return foiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremValue;
    }

    public void setFoiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremValue(String foiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremValue) {
        this.foiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremValue = foiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremValue;
    }

    public int getIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex() {
        return istoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex;
    }

    public String getOndeIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTrem() {
        return ondeIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTrem;
    }

    public String getIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue() {
        return istoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue;
    }

    public void setIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue(String istoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue) {
        this.istoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue = istoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue;
    }

    public int getHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArIndex() {
        return houveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArIndex;
    }

    public String getHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArValue() {
        return houveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArValue;
    }

    public void setHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArValue(String houveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArValue) {
        this.houveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArValue = houveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArValue;
    }

    public String getQualHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeAr() {
        return qualHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeAr;
    }

    public int getHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoIndex() {
        return houveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoIndex;
    }

    public String getHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoValue() {
        return houveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoValue;
    }

    public void setHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoValue(String houveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoValue) {
        this.houveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoValue = houveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoValue;
    }

    public String getExpliqueHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamento() {
        return expliqueHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamento;
    }

    public int getoTremEstavaIndex() {
        return oTremEstavaIndex;
    }

    public String getoTremEstavaValue() {
        return oTremEstavaValue;
    }

    public void setoTremEstavaValue(String oTremEstavaValue) {
        this.oTremEstavaValue = oTremEstavaValue;
    }

    public int getNaHoraDoAcidenteAsLocomotivasEstavamEmIndex() {
        return naHoraDoAcidenteAsLocomotivasEstavamEmIndex;
    }

    public String getNaHoraDoAcidenteAsLocomotivasEstavamEmValue() {
        return naHoraDoAcidenteAsLocomotivasEstavamEmValue;
    }

    public void setNaHoraDoAcidenteAsLocomotivasEstavamEmValue(String naHoraDoAcidenteAsLocomotivasEstavamEmValue) {
        this.naHoraDoAcidenteAsLocomotivasEstavamEmValue = naHoraDoAcidenteAsLocomotivasEstavamEmValue;
    }

    public String getQualAAmperagem() {
        return qualAAmperagem;
    }

    public int getHaviaAlgumaLocomotivaDeslizandoOuPerdendoForcaIndex() {
        return haviaAlgumaLocomotivaDeslizandoOuPerdendoForcaIndex;
    }

    public String getHaviaAlgumaLocomotivaDeslizandoOuPerdendoForcaValue() {
        return haviaAlgumaLocomotivaDeslizandoOuPerdendoForcaValue;
    }

    public void setHaviaAlgumaLocomotivaDeslizandoOuPerdendoForcaValue(String haviaAlgumaLocomotivaDeslizandoOuPerdendoForcaValue) {
        this.haviaAlgumaLocomotivaDeslizandoOuPerdendoForcaValue = haviaAlgumaLocomotivaDeslizandoOuPerdendoForcaValue;
    }

    public int getIssoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex() {
        return issoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex;
    }

    public String getIssoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue() {
        return issoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue;
    }

    public void setIssoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue(String issoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue) {
        this.issoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue = issoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue;
    }

    public String getOndeIssoTeveAlgumEfeitoNaAcaoDasFolgasDoTrem() {
        return ondeIssoTeveAlgumEfeitoNaAcaoDasFolgasDoTrem;
    }

    public int getHouveVazamentoDoEncanamentoGeralIndex() {
        return houveVazamentoDoEncanamentoGeralIndex;
    }

    public String getHouveVazamentoDoEncanamentoGeralValue() {
        return houveVazamentoDoEncanamentoGeralValue;
    }

    public void setHouveVazamentoDoEncanamentoGeralValue(String houveVazamentoDoEncanamentoGeralValue) {
        this.houveVazamentoDoEncanamentoGeralValue = houveVazamentoDoEncanamentoGeralValue;
    }

    public String getQuantasLibrasPorMinuto() {
        return quantasLibrasPorMinuto;
    }

    public int getOFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoIndex() {
        return oFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoIndex;
    }

    public String getoFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoValue() {
        return oFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoValue;
    }

    public void setoFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoValue(String oFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoValue) {
        this.oFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoValue = oFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoValue;
    }

    public String getEmQualKM() {
        return emQualKM;
    }

    public String geteQualFoiAAplicacao() {
        return eQualFoiAAplicacao;
    }

    public String getPeloConhecimentoDoMaquinistaQuantasLocomotivasEstavamComOFreioDinamicoFuncionando() {
        return peloConhecimentoDoMaquinistaQuantasLocomotivasEstavamComOFreioDinamicoFuncionando;
    }

    public String getQualAAmperagemAntesDoDescarrilamento() {
        return qualAAmperagemAntesDoDescarrilamento;
    }

    public int getoFreioDinamicoEstavaOscilandoIndex() {
        return oFreioDinamicoEstavaOscilandoIndex;
    }

    public String getoFreioDinamicoEstavaOscilandoValue() {
        return oFreioDinamicoEstavaOscilandoValue;
    }

    public void setoFreioDinamicoEstavaOscilandoValue(String oFreioDinamicoEstavaOscilandoValue) {
        this.oFreioDinamicoEstavaOscilandoValue = oFreioDinamicoEstavaOscilandoValue;
    }

    public String getQuantoOFreioDinamicoEstavaOscilando() {
        return quantoOFreioDinamicoEstavaOscilando;
    }

    public int getOuveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoIndex() {
        return ouveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoIndex;
    }

    public String getOuveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoValue() {
        return ouveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoValue;
    }

    public void setOuveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoValue(String ouveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoValue) {
        this.ouveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoValue = ouveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoValue;
    }

}
