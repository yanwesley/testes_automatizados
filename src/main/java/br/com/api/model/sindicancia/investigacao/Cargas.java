package br.com.api.model.sindicancia.investigacao;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class Cargas {
    Faker faker;
    private boolean condicaoDoVagao;
    private String descrevaQualOMetodoDeCarregamentoDoCliente;
    private String descrevaSeOMetodoContribuiuParaODesbalanceamento;
    private String desbalanceamentoLateral;
    private String desbalanceamentoLongitudinal;
    private boolean demaisVagoesCargaDeslocados;
    private String descrevaOsItensObservados;

    public Cargas() {
        faker = new Faker(new Locale("pt-BR"));
        condicaoDoVagao = faker.bool().bool();
        descrevaQualOMetodoDeCarregamentoDoCliente = getRandomCharacters(2000);
        descrevaSeOMetodoContribuiuParaODesbalanceamento = getRandomCharacters(2000);
        desbalanceamentoLateral = String.valueOf(getRandom(null));
        desbalanceamentoLongitudinal = String.valueOf(getRandom(null));
        demaisVagoesCargaDeslocados = faker.bool().bool();
        descrevaOsItensObservados = getRandomCharacters(2000);
    }

    public boolean isCondicaoDoVagao() {
        return condicaoDoVagao;
    }

    public String getDescrevaQualOMetodoDeCarregamentoDoCliente() {
        return descrevaQualOMetodoDeCarregamentoDoCliente;
    }

    public String getDescrevaSeOMetodoContribuiuParaODesbalanceamento() {
        return descrevaSeOMetodoContribuiuParaODesbalanceamento;
    }

    public String getDesbalanceamentoLateral() {
        return desbalanceamentoLateral;
    }

    public String getDesbalanceamentoLongitudinal() {
        return desbalanceamentoLongitudinal;
    }

    public boolean isDemaisVagoesCargaDeslocados() {
        return demaisVagoesCargaDeslocados;
    }

    public String getDescrevaOsItensObservados() {
        return descrevaOsItensObservados;
    }
}
