package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class RegistroDoAcidente {
    Faker faker;
    private String observacaoCroquiDoAcidente;
    private String observacaoFotoGeralDoAcidente;
    private String observacaoFotoSentidoDeCirculacaoDoMotorista;
    private String observacaoFotoSentidoContrarioDaCirculacaoDoMotorista;
    private String observacaoFotoSentidoDeCirculacaoDoMaquinista;
    private String observacaoFotoSentidoContrarioDeCirculacaoDoMaquinista;
    private String observacaoDepoimentoDoCondutorDoVeiculo;
    private String observacaoDepoimentoDasTestemunhas;
    private String observacaoOutros;
    private String tituloOutros;
    private String numeroBoletimDeOcorrencia;
    private String numeroLaudoDoIML;

    public RegistroDoAcidente() {
        faker = new Faker(new Locale("pt-BR"));
        observacaoCroquiDoAcidente = getRandomCharacters(2000);
        observacaoFotoGeralDoAcidente = getRandomCharacters(2000);
        observacaoFotoSentidoDeCirculacaoDoMotorista = getRandomCharacters(2000);
        observacaoFotoSentidoContrarioDaCirculacaoDoMotorista = getRandomCharacters(2000);
        observacaoFotoSentidoDeCirculacaoDoMaquinista = getRandomCharacters(2000);
        observacaoFotoSentidoContrarioDeCirculacaoDoMaquinista = getRandomCharacters(2000);
        observacaoDepoimentoDoCondutorDoVeiculo = getRandomCharacters(2000);
        observacaoDepoimentoDasTestemunhas = getRandomCharacters(2000);
        numeroBoletimDeOcorrencia = getRandomCharacters(2000);
        numeroLaudoDoIML = getRandomCharacters(2000);
        observacaoOutros = getRandomCharacters(2000);
        tituloOutros = getRandomCharacters(60);
    }

    public String getObservacaoOutros() {
        return observacaoOutros;
    }

    public String getTituloOutros() {
        return tituloOutros;
    }

    public String getObservacaoCroquiDoAcidente() {
        return observacaoCroquiDoAcidente;
    }

    public String getObservacaoFotoGeralDoAcidente() {
        return observacaoFotoGeralDoAcidente;
    }

    public String getObservacaoFotoSentidoDeCirculacaoDoMotorista() {
        return observacaoFotoSentidoDeCirculacaoDoMotorista;
    }

    public String getObservacaoFotoSentidoContrarioDaCirculacaoDoMotorista() {
        return observacaoFotoSentidoContrarioDaCirculacaoDoMotorista;
    }

    public String getObservacaoFotoSentidoDeCirculacaoDoMaquinista() {
        return observacaoFotoSentidoDeCirculacaoDoMaquinista;
    }

    public String getObservacaoFotoSentidoContrarioDeCirculacaoDoMaquinista() {
        return observacaoFotoSentidoContrarioDeCirculacaoDoMaquinista;
    }

    public String getObservacaoDepoimentoDoCondutorDoVeiculo() {
        return observacaoDepoimentoDoCondutorDoVeiculo;
    }

    public String getObservacaoDepoimentoDasTestemunhas() {
        return observacaoDepoimentoDasTestemunhas;
    }

    public String getNumeroBoletimDeOcorrencia() {
        return numeroBoletimDeOcorrencia;
    }

    public String getNumeroLaudoDoIML() {
        return numeroLaudoDoIML;
    }
}
