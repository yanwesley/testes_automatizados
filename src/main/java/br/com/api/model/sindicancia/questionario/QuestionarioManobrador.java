package br.com.api.model.sindicancia.questionario;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class QuestionarioManobrador {
    private Faker faker;
    private String nomeDoMaquinista;
    private String documento;
    private String CS;
    private boolean acidenteDuranteAObra;
    private int queTipoDeSinalizacaoEstavaSendoUtilizadaIndex;
    private String queTipoDeSinalizacaoEstavaSendoUtilizadaValue;
    private String quemEstavaTransmitindoOsSinais;
    private String qualFoiAUltimoSinalRecebidoPeloMaquinistaAntesDoDescarrilamento;
    private boolean haviaCoberturaDoTremPorParteDoManobrador;
    private String osSinaisDeDistanciaEstavamSendoInformadosComo;
    private boolean voceVisualizouAPosicaoDaChaveDoAMV;
    private boolean voceUltrapassouOAMVComTodaALocomotiva;
    private String oQuantoFoiUltrapassado;
    private String voceQuandoSaiDeLocomotivaDeManobraOQueFazParaImobilizarALocomotiva;
    private String quaisAsAnomaliasVoceObservouNaOperacaoEQualSuaParticipacaoParaQueOEventoAcidenteOcorrenciaEtcOcorresseOQuePoderiaSerFeitoNaSuaOpiniaoParaEvitarNovaOcorrencia;
    private String descrevaAOperacaoDoTremNoMomentoDoDescarrilamento;
    private String quantosVagoesEstavamEngatadosNaLocomotivaNoMomentoDoDescarrilamento;
    private boolean estavaSendoMovimentadoUmAMVPorUmMembroDaTripulacaoNoMomentoDoDescarrilamento;
    private int algumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoIndex;
    private String algumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoValue;
    private int oFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoIndex;
    private String oFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoValue;
    private String emQuantosVagoes;
    private int haviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoIndex;
    private String haviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoValue;
    private int haviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoIndex;
    private String haviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoValue;
    private String expliqueHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamento;
    private int oDescarrilamentoOcorreuDuranteOEngateIndex;
    private String oDescarrilamentoOcorreuDuranteOEngateValue;
    private int osEngatesEstavamAlinhadosIndex;
    private String osEngatesEstavamAlinhadosValue;
    private int oDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosIndex;
    private String oDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosValue;
    private String expliqueODescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2Veiculos;
    private int foiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoIndex;
    private String foiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoValue;
    private String expliqueFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamento;
    private String aondeEstavaOMaquinistaNoMomentoDoDescarrilamento;
    private String aondeEstavaOAjudanteDeMaquinistaNoMomentoDoDescarrilamento;
    private String aondeEstavaOManobradorAssistenteDePatioNoMomentoDoDescarrilamento;

    public QuestionarioManobrador() {
        faker = new Faker(new Locale("pt-BR"));
        nomeDoMaquinista = "Raul Herculano Fagundes";
        documento = getRandomCharacters(30);
        acidenteDuranteAObra = faker.bool().bool();
        acidenteDuranteAObra = faker.bool().bool();
        queTipoDeSinalizacaoEstavaSendoUtilizadaIndex = faker.options().option(0, 1);
        quemEstavaTransmitindoOsSinais = getRandomCharacters(512);
        qualFoiAUltimoSinalRecebidoPeloMaquinistaAntesDoDescarrilamento = getRandomCharacters(2000);
        haviaCoberturaDoTremPorParteDoManobrador = faker.bool().bool();
        osSinaisDeDistanciaEstavamSendoInformadosComo = getRandomCharacters(2000);
        voceVisualizouAPosicaoDaChaveDoAMV = faker.bool().bool();
        voceUltrapassouOAMVComTodaALocomotiva = faker.bool().bool();
        oQuantoFoiUltrapassado = String.valueOf(getRandom(null));
        voceQuandoSaiDeLocomotivaDeManobraOQueFazParaImobilizarALocomotiva = getRandomCharacters(2000);
        quaisAsAnomaliasVoceObservouNaOperacaoEQualSuaParticipacaoParaQueOEventoAcidenteOcorrenciaEtcOcorresseOQuePoderiaSerFeitoNaSuaOpiniaoParaEvitarNovaOcorrencia
                = getRandomCharacters(2000);
        descrevaAOperacaoDoTremNoMomentoDoDescarrilamento = getRandomCharacters(2000);
        quantosVagoesEstavamEngatadosNaLocomotivaNoMomentoDoDescarrilamento = String.valueOf(getRandom(null));
        estavaSendoMovimentadoUmAMVPorUmMembroDaTripulacaoNoMomentoDoDescarrilamento = faker.bool().bool();
        algumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoIndex = faker.options().option(0, 1, 2);
        oFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoIndex = faker.options().option(0, 1, 2);
        emQuantosVagoes = String.valueOf(getRandom(null));
        haviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoIndex = faker.options().option(0, 1, 2);
        haviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoIndex = faker.options().option(0, 1, 2);
        expliqueHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamento = getRandomCharacters(2000);
        oDescarrilamentoOcorreuDuranteOEngateIndex = faker.options().option(0, 1, 2);
        osEngatesEstavamAlinhadosIndex = faker.options().option(0, 1, 2);
        oDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosIndex = faker.options().option(0, 1, 2);
        expliqueODescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2Veiculos = getRandomCharacters(2000);
        foiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoIndex = faker.options().option(0, 1, 2);
        expliqueFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamento = getRandomCharacters(2000);
        aondeEstavaOMaquinistaNoMomentoDoDescarrilamento = getRandomCharacters(2000);
        aondeEstavaOAjudanteDeMaquinistaNoMomentoDoDescarrilamento = getRandomCharacters(2000);
        aondeEstavaOManobradorAssistenteDePatioNoMomentoDoDescarrilamento = getRandomCharacters(2000);
    }

    public String getNomeDoMaquinista() {
        return nomeDoMaquinista;
    }

    public void setNomeDoMaquinista(String nomeDoMaquinista) {
        this.nomeDoMaquinista = nomeDoMaquinista;
    }

    public String getDocumento() {
        return documento;
    }

    public String getCS() {
        return CS;
    }

    public void setCS(String CS) {
        this.CS = CS;
    }

    public boolean isAcidenteDuranteAManobra() {
        return acidenteDuranteAObra;
    }

    public int getQueTipoDeSinalizacaoEstavaSendoUtilizadaIndex() {
        return queTipoDeSinalizacaoEstavaSendoUtilizadaIndex;
    }

    public String getQueTipoDeSinalizacaoEstavaSendoUtilizadaValue() {
        return queTipoDeSinalizacaoEstavaSendoUtilizadaValue;
    }

    public void setQueTipoDeSinalizacaoEstavaSendoUtilizadaValue(String queTipoDeSinalizacaoEstavaSendoUtilizadaValue) {
        this.queTipoDeSinalizacaoEstavaSendoUtilizadaValue = queTipoDeSinalizacaoEstavaSendoUtilizadaValue;
    }

    public String getQuemEstavaTransmitindoOsSinais() {
        return quemEstavaTransmitindoOsSinais;
    }

    public String getQualFoiAUltimoSinalRecebidoPeloMaquinistaAntesDoDescarrilamento() {
        return qualFoiAUltimoSinalRecebidoPeloMaquinistaAntesDoDescarrilamento;
    }

    public boolean isHaviaCoberturaDoTremPorParteDoManobrador() {
        return haviaCoberturaDoTremPorParteDoManobrador;
    }

    public String getOsSinaisDeDistanciaEstavamSendoInformadosComo() {
        return osSinaisDeDistanciaEstavamSendoInformadosComo;
    }

    public boolean isVoceVisualizouAPosicaoDaChaveDoAMV() {
        return voceVisualizouAPosicaoDaChaveDoAMV;
    }

    public boolean isVoceUltrapassouOAMVComTodaALocomotiva() {
        return voceUltrapassouOAMVComTodaALocomotiva;
    }

    public String getoQuantoFoiUltrapassado() {
        return oQuantoFoiUltrapassado;
    }

    public String getVoceQuandoSaiDeLocomotivaDeManobraOQueFazParaImobilizarALocomotiva() {
        return voceQuandoSaiDeLocomotivaDeManobraOQueFazParaImobilizarALocomotiva;
    }

    public String getQuaisAsAnomaliasVoceObservouNaOperacaoEQualSuaParticipacaoParaQueOEventoAcidenteOcorrenciaEtcOcorresseOQuePoderiaSerFeitoNaSuaOpiniaoParaEvitarNovaOcorrencia() {
        return quaisAsAnomaliasVoceObservouNaOperacaoEQualSuaParticipacaoParaQueOEventoAcidenteOcorrenciaEtcOcorresseOQuePoderiaSerFeitoNaSuaOpiniaoParaEvitarNovaOcorrencia;
    }

    public String getDescrevaAOperacaoDoTremNoMomentoDoDescarrilamento() {
        return descrevaAOperacaoDoTremNoMomentoDoDescarrilamento;
    }

    public String getQuantosVagoesEstavamEngatadosNaLocomotivaNoMomentoDoDescarrilamento() {
        return quantosVagoesEstavamEngatadosNaLocomotivaNoMomentoDoDescarrilamento;
    }

    public boolean isEstavaSendoMovimentadoUmAMVPorUmMembroDaTripulacaoNoMomentoDoDescarrilamento() {
        return estavaSendoMovimentadoUmAMVPorUmMembroDaTripulacaoNoMomentoDoDescarrilamento;
    }

    public int getAlgumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoIndex() {
        return algumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoIndex;
    }

    public String getAlgumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoValue() {
        return algumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoValue;
    }

    public void setAlgumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoValue(String algumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoValue) {
        this.algumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoValue = algumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoValue;
    }

    public int getoFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoIndex() {
        return oFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoIndex;
    }

    public String getoFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoValue() {
        return oFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoValue;
    }

    public void setoFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoValue(String oFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoValue) {
        this.oFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoValue = oFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoValue;
    }

    public String getEmQuantosVagoes() {
        return emQuantosVagoes;
    }

    public int getHaviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoIndex() {
        return haviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoIndex;
    }

    public String getHaviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoValue() {
        return haviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoValue;
    }

    public void setHaviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoValue(String haviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoValue) {
        this.haviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoValue = haviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoValue;
    }

    public int getHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoIndex() {
        return haviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoIndex;
    }

    public String getHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoValue() {
        return haviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoValue;
    }

    public void setHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoValue(String haviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoValue) {
        this.haviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoValue = haviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoValue;
    }

    public String getExpliqueHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamento() {
        return expliqueHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamento;
    }

    public int getoDescarrilamentoOcorreuDuranteOEngateIndex() {
        return oDescarrilamentoOcorreuDuranteOEngateIndex;
    }

    public String getoDescarrilamentoOcorreuDuranteOEngateValue() {
        return oDescarrilamentoOcorreuDuranteOEngateValue;
    }

    public void setoDescarrilamentoOcorreuDuranteOEngateValue(String oDescarrilamentoOcorreuDuranteOEngateValue) {
        this.oDescarrilamentoOcorreuDuranteOEngateValue = oDescarrilamentoOcorreuDuranteOEngateValue;
    }

    public int getOsEngatesEstavamAlinhadosIndex() {
        return osEngatesEstavamAlinhadosIndex;
    }

    public String getOsEngatesEstavamAlinhadosValue() {
        return osEngatesEstavamAlinhadosValue;
    }

    public void setOsEngatesEstavamAlinhadosValue(String osEngatesEstavamAlinhadosValue) {
        this.osEngatesEstavamAlinhadosValue = osEngatesEstavamAlinhadosValue;
    }

    public int getoDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosIndex() {
        return oDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosIndex;
    }

    public String getoDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosValue() {
        return oDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosValue;
    }

    public void setoDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosValue(String oDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosValue) {
        this.oDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosValue = oDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosValue;
    }

    public String getExpliqueODescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2Veiculos() {
        return expliqueODescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2Veiculos;
    }

    public int getFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoIndex() {
        return foiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoIndex;
    }

    public String getFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoValue() {
        return foiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoValue;
    }

    public void setFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoValue(String foiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoValue) {
        this.foiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoValue = foiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoValue;
    }

    public String getExpliqueFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamento() {
        return expliqueFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamento;
    }

    public String getAondeEstavaOMaquinistaNoMomentoDoDescarrilamento() {
        return aondeEstavaOMaquinistaNoMomentoDoDescarrilamento;
    }

    public String getAondeEstavaOAjudanteDeMaquinistaNoMomentoDoDescarrilamento() {
        return aondeEstavaOAjudanteDeMaquinistaNoMomentoDoDescarrilamento;
    }

    public String getAondeEstavaOManobradorAssistenteDePatioNoMomentoDoDescarrilamento() {
        return aondeEstavaOManobradorAssistenteDePatioNoMomentoDoDescarrilamento;
    }


}
