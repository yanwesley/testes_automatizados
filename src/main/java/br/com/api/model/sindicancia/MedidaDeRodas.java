package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MedidaDeRodas {
    private final int QUANTIDADE_CAMPOS_BITOLA = 4;
    private final int QUANTIDADE_CAMPOS_OUTROS = 8;
    private final int LIMITE_TAMANHO_NUMEROS = 999999;
    private Faker faker;
    private List<Long> espessuraDoFriso = new ArrayList<>();
    private List<Long> alturaDoFriso = new ArrayList<>();
    private List<Long> bandagem = new ArrayList<>();
    private List<Long> larguraDaRoda = new ArrayList<>();
    private List<Long> concavidade = new ArrayList<>();
    private List<String> bitolaM1 = new ArrayList<>();
    private List<String> bitolaM2 = new ArrayList<>();
    private List<String> bitolaM3 = new ArrayList<>();

    public MedidaDeRodas() {
        faker = faker = new Faker(new Locale("pt-BR"));
        setEspessuraDoFriso();
        setAlturaDoFriso();
        setBandagem();
        setLarguraDaRoda();
        setConcavidade();
        setBitolaM1();
        setBitolaM2();
        setBitolaM3();
    }

    private void setBitolaM3() {
        for (int i = 0; i < QUANTIDADE_CAMPOS_BITOLA; i++) {
            this.bitolaM3.add(String.valueOf(faker.random().nextInt(LIMITE_TAMANHO_NUMEROS)));
        }
    }

    private void setBitolaM2() {
        for (int i = 0; i < QUANTIDADE_CAMPOS_BITOLA; i++) {
            this.bitolaM2.add(String.valueOf(faker.random().nextInt(LIMITE_TAMANHO_NUMEROS)));
        }
    }

    private void setBitolaM1() {
        for (int i = 0; i < QUANTIDADE_CAMPOS_BITOLA; i++) {
            this.bitolaM1.add(String.valueOf(faker.random().nextInt(LIMITE_TAMANHO_NUMEROS)));
        }
    }

    private void setConcavidade() {
        for (int i = 0; i < QUANTIDADE_CAMPOS_OUTROS; i++) {
            this.concavidade.add(faker.random().nextLong(LIMITE_TAMANHO_NUMEROS));
        }
    }

    private void setLarguraDaRoda() {
        for (int i = 0; i < QUANTIDADE_CAMPOS_OUTROS; i++) {
            this.larguraDaRoda.add(faker.random().nextLong(LIMITE_TAMANHO_NUMEROS));
        }
    }

    private void setBandagem() {
        for (int i = 0; i < QUANTIDADE_CAMPOS_OUTROS; i++) {
            this.bandagem.add(faker.random().nextLong(LIMITE_TAMANHO_NUMEROS));
        }
    }

    private void setAlturaDoFriso() {
        for (int i = 0; i < QUANTIDADE_CAMPOS_OUTROS; i++) {
            this.alturaDoFriso.add(faker.random().nextLong(LIMITE_TAMANHO_NUMEROS));
        }
    }

    private void setEspessuraDoFriso() {
        for (int i = 0; i < QUANTIDADE_CAMPOS_OUTROS; i++) {
            this.espessuraDoFriso.add(faker.random().nextLong(LIMITE_TAMANHO_NUMEROS));
        }
    }

    public List<Long> getEspessuraDoFriso() {
        return espessuraDoFriso;
    }

    public List<Long> getAlturaDoFriso() {
        return alturaDoFriso;
    }

    public List<Long> getBandagem() {
        return bandagem;
    }

    public List<Long> getLarguraDaRoda() {
        return larguraDaRoda;
    }

    public List<Long> getConcavidade() {
        return concavidade;
    }

    public List<String> getBitolaM1() {
        return bitolaM1;
    }

    public List<String> getBitolaM2() {
        return bitolaM2;
    }

    public List<String> getBitolaM3() {
        return bitolaM3;
    }
}
