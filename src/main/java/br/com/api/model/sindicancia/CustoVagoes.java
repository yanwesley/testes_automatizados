package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

public class CustoVagoes {
    final int LIMITE_QTD = 999;
    final int LIMITE_VALOR = 999999999;

    private Faker faker;
    private String qtdRodeiros;
    private String qtdTruque;
    private String qtdEngate;
    private String qtdEstrutura;
    private String qtdTremonha;
    private String qtdSistemaDeFreio;
    private String qtdTransporteRemocao;
    private String qtdMaoDeObra;
    private String qtdOutros;
    private long valorRodeiros;
    private long valorTruque;
    private long valorEngate;
    private long valorEstrutura;
    private long valorTremonha;
    private long valorSistemaDeFreio;
    private long valorTransporteRemocao;
    private long valorMaoDeObra;
    private long valorOutros;

    public CustoVagoes() {
        faker = new Faker(new Locale("pt-BR"));
        //qtd
        qtdRodeiros = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdTruque = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdEngate = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdEstrutura = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdTremonha = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdSistemaDeFreio = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdTransporteRemocao = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdMaoDeObra = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdOutros = faker.random().nextInt(0, LIMITE_QTD).toString();
        //valores
        valorRodeiros = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorTruque = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorEngate = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorEstrutura = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorTremonha = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorSistemaDeFreio = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorTransporteRemocao = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorMaoDeObra = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorOutros = faker.random().nextInt(0, LIMITE_VALOR).longValue();
    }

    public long getTotal() {
        return
                (Integer.valueOf(qtdRodeiros) * valorRodeiros) +
                        (Integer.valueOf(qtdTruque) * valorTruque) +
                        (Integer.valueOf(qtdEngate) * valorEngate) +
                        (Integer.valueOf(qtdEstrutura) * valorEstrutura) +
                        (Integer.valueOf(qtdTremonha) * valorTremonha) +
                        (Integer.valueOf(qtdSistemaDeFreio) * valorSistemaDeFreio) +
                        (Integer.valueOf(qtdTransporteRemocao) * valorTransporteRemocao) +
                        (Integer.valueOf(qtdMaoDeObra) * valorMaoDeObra) +
                        (Integer.valueOf(qtdOutros) * valorOutros);
    }

    public String getQtdRodeiros() {
        return qtdRodeiros;
    }

    public void setQtdRodeiros(String qtdRodeiros) {
        this.qtdRodeiros = qtdRodeiros;
    }

    public String getQtdTruque() {
        return qtdTruque;
    }

    public void setQtdTruque(String qtdTruque) {
        this.qtdTruque = qtdTruque;
    }

    public String getQtdEngate() {
        return qtdEngate;
    }

    public void setQtdEngate(String qtdEngate) {
        this.qtdEngate = qtdEngate;
    }

    public String getQtdEstrutura() {
        return qtdEstrutura;
    }

    public void setQtdEstrutura(String qtdEstrutura) {
        this.qtdEstrutura = qtdEstrutura;
    }

    public String getQtdTremonha() {
        return qtdTremonha;
    }

    public void setQtdTremonha(String qtdTremonha) {
        this.qtdTremonha = qtdTremonha;
    }

    public String getQtdSistemaDeFreio() {
        return qtdSistemaDeFreio;
    }

    public void setQtdSistemaDeFreio(String qtdSistemaDeFreio) {
        this.qtdSistemaDeFreio = qtdSistemaDeFreio;
    }

    public String getQtdTransporteRemocao() {
        return qtdTransporteRemocao;
    }

    public void setQtdTransporteRemocao(String qtdTransporteRemocao) {
        this.qtdTransporteRemocao = qtdTransporteRemocao;
    }

    public String getQtdMaoDeObra() {
        return qtdMaoDeObra;
    }

    public void setQtdMaoDeObra(String qtdMaoDeObra) {
        this.qtdMaoDeObra = qtdMaoDeObra;
    }

    public String getQtdOutros() {
        return qtdOutros;
    }

    public void setQtdOutros(String qtdOutros) {
        this.qtdOutros = qtdOutros;
    }

    public long getValorRodeiros() {
        return valorRodeiros;
    }

    public void setValorRodeiros(long valorRodeiros) {
        this.valorRodeiros = valorRodeiros;
    }

    public long getValorTruque() {
        return valorTruque;
    }

    public void setValorTruque(long valorTruque) {
        this.valorTruque = valorTruque;
    }

    public long getValorEngate() {
        return valorEngate;
    }

    public void setValorEngate(long valorEngate) {
        this.valorEngate = valorEngate;
    }

    public long getValorEstrutura() {
        return valorEstrutura;
    }

    public void setValorEstrutura(long valorEstrutura) {
        this.valorEstrutura = valorEstrutura;
    }

    public long getValorTremonha() {
        return valorTremonha;
    }

    public void setValorTremonha(long valorTremonha) {
        this.valorTremonha = valorTremonha;
    }

    public long getValorSistemaDeFreio() {
        return valorSistemaDeFreio;
    }

    public void setValorSistemaDeFreio(long valorSistemaDeFreio) {
        this.valorSistemaDeFreio = valorSistemaDeFreio;
    }

    public long getValorTransporteRemocao() {
        return valorTransporteRemocao;
    }

    public void setValorTransporteRemocao(long valorTransporteRemocao) {
        this.valorTransporteRemocao = valorTransporteRemocao;
    }

    public long getValorMaoDeObra() {
        return valorMaoDeObra;
    }

    public void setValorMaoDeObra(long valorMaoDeObra) {
        this.valorMaoDeObra = valorMaoDeObra;
    }

    public long getValorOutros() {
        return valorOutros;
    }

    public void setValorOutros(long valorOutros) {
        this.valorOutros = valorOutros;
    }

}
