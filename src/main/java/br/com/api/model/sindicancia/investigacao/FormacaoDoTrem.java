package br.com.api.model.sindicancia.investigacao;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;

public class FormacaoDoTrem {

    Faker faker;
    private boolean haviamVagoesGato;
    private String quantosVagoesGato;

    public FormacaoDoTrem() {
        faker = new Faker(new Locale("pt-BR"));
        haviamVagoesGato = faker.bool().bool();
        quantosVagoesGato = String.valueOf(getRandom(null));
    }

    public boolean isHaviamVagoesGato() {
        return haviamVagoesGato;
    }

    public String getQuantosVagoesGato() {
        return quantosVagoesGato;
    }

}
