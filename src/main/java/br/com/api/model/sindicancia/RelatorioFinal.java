package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class RelatorioFinal {
    Faker faker;
    private String responsavel;
    private String superior;
    private String areaDonoDoAcidente;
    private String fatoOcorrido;
    private int mecanismoDescarrilhamentoIndex;
    private String mecanismoDescarrilhamentoValue;
    private boolean distribuicaoDeGravidade;
    private int bandeiraIndex;
    private String bandeiraValue;
    private int causaIndex;
    private String causaValue;
    private int areaResponsavelIndex;
    private String areaResponsavelValue;
    private String matriculaDono;

    public RelatorioFinal() {
        faker = new Faker(new Locale("pt-BR"));
        fatoOcorrido = getRandomCharacters(511);
        responsavel = "Marcio Rodrigues de Freitas";
        mecanismoDescarrilhamentoIndex = getRandom(6);
        distribuicaoDeGravidade = faker.bool().bool();
        bandeiraIndex = faker.options().option(0, 1);
        areaResponsavelIndex = faker.options().option(0, 1, 2);
        causaIndex = faker.options().option(0);
    }

    public void setMatriculaDono(String matriculaDono) {
        this.matriculaDono = matriculaDono;
    }

    public int getAreaResponsavelIndex() {
        return areaResponsavelIndex;
    }

    public String getAreaResponsavelValue() {
        return areaResponsavelValue;
    }

    public void setAreaResponsavelValue(String areaResponsavelValue) {
        this.areaResponsavelValue = areaResponsavelValue;
    }

    public int getCausaIndex() {
        return causaIndex;
    }

    public String getMecanismoDescarrilhamentoValue() {
        return mecanismoDescarrilhamentoValue;
    }

    public void setMecanismoDescarrilhamentoValue(String mecanismoDescarrilhamentoValue) {
        this.mecanismoDescarrilhamentoValue = mecanismoDescarrilhamentoValue;
    }

    public String getCausaValue() {
        return causaValue;
    }

    public void setCausaValue(String causaValue) {
        this.causaValue = causaValue;
    }

    public String getBandeiraValue() {
        return bandeiraValue;
    }

    public void setBandeiraValue(String bandeiraValue) {
        this.bandeiraValue = bandeiraValue;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getSuperior() {
        return superior;
    }

    public void setSuperior(String superior) {
        this.superior = superior;
    }

    public String getAreaDonoDoAcidente() {
        return areaDonoDoAcidente;
    }

    public void setAreaDonoDoAcidente(String areaDonoDoAcidente) {
        this.areaDonoDoAcidente = areaDonoDoAcidente;
    }

    public String getFatoOcorrido() {
        return fatoOcorrido;
    }

    public int getMecanismoDescarrilhamentoIndex() {
        return mecanismoDescarrilhamentoIndex;
    }

    public boolean isDistribuicaoDeGravidade() {
        return distribuicaoDeGravidade;
    }

    public int getBandeiraIndex() {
        return bandeiraIndex;
    }

    public String getMatriculoDono() {
        return matriculaDono;
    }

}


