package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.*;

public class Vagao {

    private Faker faker;
    private int parametrizacaoIndex;
    private String parametrizacaoValue;
    private int bitolaIndex;
    private String bitolaValue;
    private String estadoDoVagao;
    private boolean haviaDDVNoLocal;
    private boolean estavaOperacional;
    private boolean atuou;
    private String comentarioDDV;
    private int comoEstavaACargaIndex;
    private String comoEstavaACargaValue;
    private String tipoDeMercadoria;
    private long pesoBrutoDoVeiculo;
    private boolean etiquetadoOuRecomendadoOficina;
    private String medicaoRealizada;
    private String localDaMedicao;
    private String dataUltimaManutencaoPreventiva;
    private String tipoUltimaManutencaoPreventiva;
    private String localUltimaManutencaoPreventiva;
    private String dataUltimaManutencaoCorretiva;
    private String tratativaUltimaManutencaoCorretiva;
    private String localUltimaManutencaoCorretiva;
    private int primeiroRodeiroADescarrilarPosicaoIndex;
    private String primeiroRodeiroADescarrilarPosicaoValue;
    private boolean rodeirosDescarriladosR1;
    private boolean rodeirosDescarriladosR2;
    private boolean rodeirosDescarriladosR3;
    private boolean rodeirosDescarriladosR4;
    private String situacaoDaRodaValue;
    private int situacaoDaRodaIndex;
    private String fabricacaoDaRodaValue;
    private int fabricacaoDaRodaIndex;
    private String eixoValue;
    private int eixoIndex;
    private String tipoDeTruqueValue;
    private int tipoDeTruqueIndex;
    private String mangaDoVagaoValue;
    private int mangaDoVagaoIndex;
    private boolean sinaisDeSuperAquecimentoNaRoda;
    private long medidaAquecimento;
    private String tipoDoRolamentoValue;
    private int tipoDoRolamentoIndex;
    private String corLubrificacaoDoRolamentoValue;
    private int corLubrificacaoDoRolamentoIndex;
    private String identificacaoDoEixo;
    private String numeroDaFITA;
    private String numeroDoEixo;
    private boolean infovagaoCroquiErRodeirosDescarriladosR1;
    private boolean infovagaoCroquiErRodeirosDescarriladosR2;
    private boolean infovagaoCroquiErRodeirosDescarriladosR3;
    private boolean infovagaoCroquiErRodeirosDescarriladosR4;
    private boolean infovagaoCroquiCabeceiraA;
    private boolean infovagaoCroquiCabeceiraB;
    private boolean infovagaoCroquiDrRodeirosDescarriladosR1;
    private boolean infovagaoCroquiDrRodeirosDescarriladosR2;
    private boolean infovagaoCroquiDrRodeirosDescarriladosR3;
    private boolean infovagaoCroquiDrRodeirosDescarriladosR4;
    private String marcarOSentidoDoDeslocamentoDoVagao;
    private String tipoPratoPiao;
    private String folgaNoDiametro;
    private String folgaVertical;
    private String diametroPratoPiaoInferior;
    private String profundidadePratoPiaoInferior;
    private String diametroPratoPiaoSuperior;
    private String alturaPratoPiaoSuperior;
    private String direita1CunhaDeFriccaoValue;
    private int direita1CunhaDeFriccaoIndex;
    private String direita2CunhaDeFriccaoValue;
    private int direita2CunhaDeFriccaoIndex;
    private String direita3CunhaDeFriccaoValue;
    private int direita3CunhaDeFriccaoIndex;
    private String direita4CunhaDeFriccaoValue;
    private int direita4CunhaDeFriccaoIndex;
    private String esquerda1CunhaDeFriccaoValue;
    private int esquerda1CunhaDeFriccaoIndex;
    private String esquerda2CunhaDeFriccaoValue;
    private int esquerda2CunhaDeFriccaoIndex;
    private String esquerda3CunhaDeFriccaoValue;
    private int esquerda3CunhaDeFriccaoIndex;
    private String esquerda4CunhaDeFriccaoValue;
    private int esquerda4CunhaDeFriccaoIndex;
    private String direita1PlacaDeDesgasteColunaLatValue;
    private int direita1PlacaDeDesgasteColunaLatIndex;
    private String direita2PlacaDeDesgasteColunaLatValue;
    private int direita2PlacaDeDesgasteColunaLatIndex;
    private String direita3PlacaDeDesgasteColunaLatValue;
    private int direita3PlacaDeDesgasteColunaLatIndex;
    private String direita4PlacaDeDesgasteColunaLatValue;
    private int direita4PlacaDeDesgasteColunaLatIndex;
    private String esquerda1PlacaDeDesgasteColunaLatValue;
    private int esquerda1PlacaDeDesgasteColunaLatIndex;
    private String esquerda2PlacaDeDesgasteColunaLatValue;
    private int esquerda2PlacaDeDesgasteColunaLatIndex;
    private String esquerda3PlacaDeDesgasteColunaLatValue;
    private int esquerda3PlacaDeDesgasteColunaLatIndex;
    private String esquerda4PlacaDeDesgasteColunaLatValue;
    private int esquerda4PlacaDeDesgasteColunaLatIndex;
    private long direitaABotoesLateralDoTruque;
    private long esquerdaABotoesLateralDoTruque;
    private long direitaBBotoesLateralDoTruque;
    private long esquerdaBBotoesLateralDoTruque;
    private long diferencaABotoesLateralDoTruque;
    private long diferencaBBotoesLateralDoTruque;
    private long alturaDasCunhasCabALadoDiteiro1;
    private long alturaDasCunhasCabALadoDiteiro2;
    private long alturaDasCunhasCabALadoEsquerdo1;
    private long alturaDasCunhasCabALadoEsquerdo2;
    private long alturaDasCunhasCabBLadoDiteiro3;
    private long alturaDasCunhasCabBLadoDiteiro4;
    private long alturaDasCunhasCabBLadoEsquerdo3;
    private long alturaDasCunhasCabBLadoEsquerdo4;
    private long externaAlturaDasMolasDoTruqueDescarrilado;
    private long intermediariaAlturaDasMolasDoTruqueDescarrilado;
    private long internaAlturaDasMolasDoTruqueDescarrilado;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD5;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD6;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD7;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD8;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD5;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD6;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD7;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD8;
    private long molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1;
    private long molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2;
    private long molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3;
    private long molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE5;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE6;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE7;
    private long molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE8;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE5;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE6;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE7;
    private long molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE8;
    private long molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1;
    private long molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2;
    private long molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3;
    private long molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4;
    private int sobreAlturaDasMolasCunhasIndex;
    private String sobreAlturaDasMolasCunhasValue;
    private long molasExternasAlturaDasMolasCunhasLadoDireitoLD1;
    private long molasExternasAlturaDasMolasCunhasLadoDireitoLD2;
    private long molasInternasAlturaDasMolasCunhasLadoDireitoLD1;
    private long molasInternasAlturaDasMolasCunhasLadoDireitoLD2;
    private long molasExternasAlturaDasMolasCunhasLadoEsquerdoLE1;
    private long molasExternasAlturaDasMolasCunhasLadoEsquerdoLE2;
    private long molasInternasAlturaDasMolasCunhasLadoEsquerdoLE1;
    private long molasInternasAlturaDasMolasCunhasLadoEsquerdoLE2;
    /*
    ALTURA DAS MOLAS - TRUQUE DA OUTRA CABECEIRA - Lado direito
 */
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD5;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD6;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD7;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD8;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD5;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD6;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD7;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD8;
    private long molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1;
    private long molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2;
    private long molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3;
    private long molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4;
    /*
        ALTURA DAS MOLAS - TRUQUE DA OUTRA CABECEIRA - Lado esquerdo
     */
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE5;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE6;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE7;
    private long molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE8;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE5;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE6;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE7;
    private long molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE8;
    private long molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1;
    private long molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2;
    private long molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3;
    private long molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4;
    private int sobreAlturaDasMolasCunhasOutraCabeceiraIndex;
    private String sobreAlturaDasMolasCunhasOutraCabeceiraValue;
    private long molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1;
    private long molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2;
    private long molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1;
    private long molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2;
    private long molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1;
    private long molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2;
    private long molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1;
    private long molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2;
    /*
    FOLGAS - MEDIDAS VÁLIDAS ANTES DO ACIDENTE
     */
    private String localFolgasMedidasValidasAntesDoAcidente;
    private String dataFolgasMedidasValidasAntesDoAcidente;
    private String tipoManutencaoFolgasMedidasValidasAntesDoAcidenteValue;
    private int tipoManutencaoFolgasMedidasValidasAntesDoAcidenteIndex;
    private long aEsquerdaFolgasMedidasValidasAntesDoAcidente;
    private long aDireitaFolgasMedidasValidasAntesDoAcidente;
    private long bEsquerdaFolgasMedidasValidasAntesDoAcidente;
    private long bDireitaFolgasMedidasValidasAntesDoAcidente;
    private long aEsquerdaMaisBDireitaFolgasMedidasValidasAntesDoAcidente;
    private long aDireitaMaisBEsquerdaFolgasMedidasValidasAntesDoAcidente;
    private long valorAbsolutoFolgasMedidasValidasAntesDoAcidente;
    private long aEsquerdaFolgasAposOAcidente;
    private long aDireitaFolgasAposOAcidente;
    private long bEsquerdaFolgasAposOAcidente;
    private long bDireitaFolgasAposOAcidente;
    private long aEsquerdaMaisBDireitaFolgasAposOAcidente;
    private long aDireitaMaisBEsquerdaFolgasAposOAcidente;
    private long valorAbsolutoFolgasAposOAcidente;
    /*
    OUTRAS INFORMAÇÕES
     */
    private String extensaoDoTrafegoDescarrilado;
    private int modeloAmparaBalancoIndex;
    private String modeloAmparaBalancoValue;
    /*
    CASTANHA/TAMPA DO AMPARA BALANÇO
     */
    private String condicaoCastanhaTampaDoAmparaBalancoValue;
    private int condicaoCastanhaTampaDoAmparaBalancoIndex;
    /*
    PARAFUSOS DE FIXAÇÃO (CONDIÇÃO)
     */
    private long aEsquerdaParafusosDeFixacao;
    private long bEsquerdaParafusosDeFixacao;
    private long aDireitaParafusosDeFixacao;
    private long bDireitaParafusosDeFixacao;
    /*
    MEDIDA DE DESGASTE DA CHAPA SUPERIOR DO AB
     */
    private long aEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB;
    private long bEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB;
    private long aDireitaMedidasDeDesgasteDaChapaSuperiorDoAB;
    private long bDireitaMedidasDeDesgasteDaChapaSuperiorDoAB;
    private String valvulaDeFreioValue;
    private int valvulaDeFreioIndex;
    private String freioManualValue;
    private int freioManualIndex;
    private String torneiraAngularEGValue;
    private int torneiraAngularEGIndex;
    private String dispositivoVazioCarregadoValue;
    private int dispositivoVazioCarregadoIndex;
    private String torneiraIsolamentoDeFreioValue;
    private int torneiraIsolamentoDeFreioIndex;
    private String posicaoDaValvulaRetentoraValue;
    private int posicaoDaValvulaRetentoraIndex;
    /*
    DDV (DETECTOR DE DESCARRILAMENTO DE VAGÃO)
     */
    private String preDescarrilamentoValue;
    private int preDescarrilamentoIndex;
    private String dDVAcionouRodeiro1Value;
    private int dDVAcionouRodeiro1Index;
    private String dDVAcionouRodeiro2Value;
    private int dDVAcionouRodeiro2Index;
    private String dDVAcionouRodeiro3Value;
    private int dDVAcionouRodeiro3Index;
    private String dDVAcionouRodeiro4Value;
    private int dDVAcionouRodeiro4Index;
    /*
    ESTRUTURA DO VAGÃO
     */
    private String condicaoLongarinaValue;
    private int condicaoLongarinaIndex;
    private String localizacaoLongarinaValue;
    private int localizacaoLongarinaIndex;
    /*
    TRAVESSA DO PRATO DO PIÃO SUPERIOR
     */
    private String condicaoTravessaDoPratoDoPiaoSuperiorValue;
    private int condicaoTravessaDoPratoDoPiaoSuperiorIndex;
    private String localizacaoTravessaDoPratoDoPiaoSuperiorValue;
    private int localizacaoTravessaDoPratoDoPiaoSuperiorIndex;
    /*
    LONGARINA LATERAL
     */
    private String condicaoLongarinaLateralValue;
    private int condicaoLongarinaLateralIndex;
    private int localizacaoLongarinaLateralIndex;
    private String localizacaoLongarinaLateralValue;
    /*
    ENGATES E ACTS
     */
    private String qualOTipoDoEngateValue;
    private int qualOTipoDoEngateIndex;
    private String alturaEngateA;
    private String alturaEngateB;
    private String engateEstaNiveladoValue;
    private int engateEstaNiveladoIndex;
    private String existeMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaValue;
    private int existeMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaIndex;
    /*
    ACT
     */
    private String qualOModeloValue;
    private int qualOModeloIndex;
    private boolean existeFolgaNoACT;
    private long qualAMedidaDaFolga;
    /*
    COMPONENTES DO ACT/ENGATE
     */
    private String engateComponentesACTEngateValue;
    private int engateComponentesACTEngateIndex;
    private String chavetaEPinoTValue;
    private int chavetaEPinoTIndex;
    private String mandibulaEPinoValue;
    private int mandibulaEPinoIndex;
    private String pinoY47ETravaValue;
    private int pinoY47ETravaIndex;
    /*
    INFORMAÇÕES ADICIONAIS
     */
    private String paraPecasFraturadasEspecificarTrincaAntigaOuRecente;
    private boolean localDoPODTemMarcasDescarrilamentoAnterior;
    private boolean localDoPODTemHistoricoDeAcidentes06Meses;
    private boolean haMarcasDePecaDoVagaoArrastandoNosDormentesAntesDoPOD;
    private String qualPeca;

    public Vagao() {
        faker = new Faker(new Locale("pt-BR"));
        parametrizacaoIndex = faker.options().option(0, 1, 2);
        bitolaIndex = faker.options().option(0, 1);
        estadoDoVagao = faker.options().option("vazio", "carregado", "inoperante");
        haviaDDVNoLocal = faker.bool().bool();
        estavaOperacional = faker.bool().bool();
        atuou = faker.bool().bool();
        comentarioDDV = getRandomCharacters(2000);
        comoEstavaACargaIndex = faker.options().option(1, 2, 3, 4, 5, 6);
        tipoDeMercadoria = faker.options().option("UREIA ACONDICIONADA", "SAL GROSSO GRANEL");
        pesoBrutoDoVeiculo = faker.random().nextLong(999999999l);
        etiquetadoOuRecomendadoOficina = faker.bool().bool();
        medicaoRealizada = faker.options().option("pmv", "patio");
        localDaMedicao = getRandomCharacters(512);
        dataUltimaManutencaoPreventiva = getDayMinus(100);
        tipoUltimaManutencaoPreventiva = getRandomCharacters(512);
        localUltimaManutencaoPreventiva = getRandomCharacters(56);
        dataUltimaManutencaoCorretiva = getDayMinus(90);
        tratativaUltimaManutencaoCorretiva = getRandomCharacters(512);
        localUltimaManutencaoCorretiva = getRandomCharacters(56);
        primeiroRodeiroADescarrilarPosicaoIndex = faker.options().option(1, 2);
        rodeirosDescarriladosR1 = faker.bool().bool();
        rodeirosDescarriladosR2 = faker.bool().bool();
        rodeirosDescarriladosR3 = faker.bool().bool();
        rodeirosDescarriladosR4 = faker.bool().bool();
        situacaoDaRodaIndex = faker.options().option(1, 2, 3, 4, 5);
        fabricacaoDaRodaIndex = faker.options().option(1, 2);
        eixoIndex = faker.options().option(1, 2, 3, 4, 5);
        tipoDeTruqueIndex = faker.options().option(1, 2, 3, 4, 5, 6, 7, 8);
        mangaDoVagaoIndex = faker.options().option(1, 2, 3, 4, 5);
        sinaisDeSuperAquecimentoNaRoda = faker.bool().bool();
        medidaAquecimento = faker.random().nextLong(999999999l);
        tipoDoRolamentoIndex = faker.options().option(1, 2, 3);
        corLubrificacaoDoRolamentoIndex = faker.options().option(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        identificacaoDoEixo = faker.options().option("fita", "numero-eixo");
        numeroDaFITA = getRandomCharacters(32);
        numeroDoEixo = String.valueOf(getRandom(null));
        infovagaoCroquiErRodeirosDescarriladosR1 = faker.bool().bool();
        infovagaoCroquiErRodeirosDescarriladosR2 = !infovagaoCroquiErRodeirosDescarriladosR1;
        infovagaoCroquiErRodeirosDescarriladosR3 = faker.bool().bool();
        infovagaoCroquiErRodeirosDescarriladosR4 = faker.bool().bool();
        infovagaoCroquiCabeceiraA = true;
        infovagaoCroquiCabeceiraB = true;
        infovagaoCroquiDrRodeirosDescarriladosR1 = faker.bool().bool();
        infovagaoCroquiDrRodeirosDescarriladosR2 = !infovagaoCroquiDrRodeirosDescarriladosR1;
        infovagaoCroquiDrRodeirosDescarriladosR3 = faker.bool().bool();
        infovagaoCroquiDrRodeirosDescarriladosR4 = faker.bool().bool();
        marcarOSentidoDoDeslocamentoDoVagao = faker.options().option("esquerda", "direita");
        tipoPratoPiao = String.valueOf(getRandom(null));
        folgaNoDiametro = String.valueOf(getRandom(null));
        folgaVertical = String.valueOf(getRandom(null));


        diametroPratoPiaoInferior = String.valueOf(getRandom(null));
        profundidadePratoPiaoInferior = String.valueOf(getRandom(null));
        diametroPratoPiaoSuperior = String.valueOf(getRandom(null));

        alturaPratoPiaoSuperior = String.valueOf(getRandom(null));
        direita1CunhaDeFriccaoIndex = faker.options().option(1, 2, 3, 4, 5);
        direita2CunhaDeFriccaoIndex = faker.options().option(1, 2, 3, 4, 5);
        direita3CunhaDeFriccaoIndex = faker.options().option(1, 2, 3, 4, 5);
        direita4CunhaDeFriccaoIndex = faker.options().option(1, 2, 3, 4, 5);
        esquerda1CunhaDeFriccaoIndex = faker.options().option(1, 2, 3, 4, 5);
        esquerda2CunhaDeFriccaoIndex = faker.options().option(1, 2, 3, 4, 5);
        esquerda3CunhaDeFriccaoIndex = faker.options().option(1, 2, 3, 4, 5);
        esquerda4CunhaDeFriccaoIndex = faker.options().option(1, 2, 3, 4, 5);

        direita1PlacaDeDesgasteColunaLatIndex = faker.options().option(1, 2, 3, 4, 5);
        direita2PlacaDeDesgasteColunaLatIndex = faker.options().option(1, 2, 3, 4, 5);
        direita3PlacaDeDesgasteColunaLatIndex = faker.options().option(1, 2, 3, 4, 5);
        direita4PlacaDeDesgasteColunaLatIndex = faker.options().option(1, 2, 3, 4, 5);
        esquerda1PlacaDeDesgasteColunaLatIndex = faker.options().option(1, 2, 3, 4, 5);
        esquerda2PlacaDeDesgasteColunaLatIndex = faker.options().option(1, 2, 3, 4, 5);
        esquerda3PlacaDeDesgasteColunaLatIndex = faker.options().option(1, 2, 3, 4, 5);
        esquerda4PlacaDeDesgasteColunaLatIndex = faker.options().option(1, 2, 3, 4, 5);


        direitaABotoesLateralDoTruque = faker.random().nextLong(999999999l);
        esquerdaABotoesLateralDoTruque = faker.random().nextLong(999999999l);
        direitaBBotoesLateralDoTruque = faker.random().nextLong(999999999l);
        esquerdaBBotoesLateralDoTruque = faker.random().nextLong(999999999l);
        diferencaABotoesLateralDoTruque = Math.abs(direitaABotoesLateralDoTruque - esquerdaABotoesLateralDoTruque);
        diferencaBBotoesLateralDoTruque = Math.abs(direitaBBotoesLateralDoTruque - esquerdaBBotoesLateralDoTruque);

        alturaDasCunhasCabALadoDiteiro1 = faker.random().nextLong(999999999l);
        alturaDasCunhasCabALadoDiteiro2 = faker.random().nextLong(999999999l);
        alturaDasCunhasCabALadoEsquerdo1 = faker.random().nextLong(999999999l);
        alturaDasCunhasCabALadoEsquerdo2 = faker.random().nextLong(999999999l);

        alturaDasCunhasCabBLadoDiteiro3 = faker.random().nextLong(999999999l);
        alturaDasCunhasCabBLadoDiteiro4 = faker.random().nextLong(999999999l);
        alturaDasCunhasCabBLadoEsquerdo3 = faker.random().nextLong(999999999l);
        alturaDasCunhasCabBLadoEsquerdo4 = faker.random().nextLong(999999999l);

        externaAlturaDasMolasDoTruqueDescarrilado = faker.random().nextLong(999999999l);
        intermediariaAlturaDasMolasDoTruqueDescarrilado = faker.random().nextLong(999999999l);
        internaAlturaDasMolasDoTruqueDescarrilado = faker.random().nextLong(999999999l);

        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD5 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD6 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD7 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD8 = faker.random().nextLong(999999999l);

        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD5 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD6 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD7 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD8 = faker.random().nextLong(999999999l);

        molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4 = faker.random().nextLong(999999999l);


    /*
    Lado esquerdo
     */

        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE5 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE6 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE7 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE8 = faker.random().nextLong(999999999l);

        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE5 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE6 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE7 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE8 = faker.random().nextLong(999999999l);

        molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4 = faker.random().nextLong(999999999l);

        sobreAlturaDasMolasCunhasIndex = faker.options().option(1, 2, 3, 4, 5, 6, 7, 8);

        molasExternasAlturaDasMolasCunhasLadoDireitoLD1 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasCunhasLadoDireitoLD2 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasCunhasLadoDireitoLD1 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasCunhasLadoDireitoLD2 = faker.random().nextLong(999999999l);

        molasExternasAlturaDasMolasCunhasLadoEsquerdoLE1 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasCunhasLadoEsquerdoLE2 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasCunhasLadoEsquerdoLE1 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasCunhasLadoEsquerdoLE2 = faker.random().nextLong(999999999l);

    /*
    ALTURA DAS MOLAS - TRUQUE DA OUTRA CABECEIRA - Lado direito
 */
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD5 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD6 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD7 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD8 = faker.random().nextLong(999999999l);

        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD5 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD6 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD7 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD8 = faker.random().nextLong(999999999l);

        molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4 = faker.random().nextLong(999999999l);


    /*
        ALTURA DAS MOLAS - TRUQUE DA OUTRA CABECEIRA - Lado esquerdo
     */
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE5 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE6 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE7 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE8 = faker.random().nextLong(999999999l);

        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE5 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE6 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE7 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE8 = faker.random().nextLong(999999999l);

        molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3 = faker.random().nextLong(999999999l);
        molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4 = faker.random().nextLong(999999999l);

    /*
    ALTURA DAS MOLAS DAS CUNHAS OUTRA CABECEIRA
     */

        sobreAlturaDasMolasCunhasOutraCabeceiraIndex = faker.options().option(1, 2, 3, 4, 5, 6, 7, 8);

        molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2 = faker.random().nextLong(999999999l);

        molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1 = faker.random().nextLong(999999999l);
        molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1 = faker.random().nextLong(999999999l);
        molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2 = faker.random().nextLong(999999999l);

    /*
    FOLGAS - MEDIDAS VÁLIDAS ANTES DO ACIDENTE
     */

        localFolgasMedidasValidasAntesDoAcidente = getRandomCharacters(60);
        dataFolgasMedidasValidasAntesDoAcidente = getDayMinus(30);
        tipoManutencaoFolgasMedidasValidasAntesDoAcidenteIndex = faker.options().option(1, 2, 3);

        aEsquerdaFolgasMedidasValidasAntesDoAcidente = faker.random().nextLong(999999999l);
        aDireitaFolgasMedidasValidasAntesDoAcidente = faker.random().nextLong(999999999l);
        bEsquerdaFolgasMedidasValidasAntesDoAcidente = faker.random().nextLong(999999999l);
        bDireitaFolgasMedidasValidasAntesDoAcidente = faker.random().nextLong(999999999l);

        aEsquerdaMaisBDireitaFolgasMedidasValidasAntesDoAcidente = aEsquerdaFolgasMedidasValidasAntesDoAcidente + bDireitaFolgasMedidasValidasAntesDoAcidente;
        aDireitaMaisBEsquerdaFolgasMedidasValidasAntesDoAcidente = aDireitaFolgasMedidasValidasAntesDoAcidente + bEsquerdaFolgasMedidasValidasAntesDoAcidente;
        valorAbsolutoFolgasMedidasValidasAntesDoAcidente = Math.abs(aEsquerdaMaisBDireitaFolgasMedidasValidasAntesDoAcidente - aDireitaMaisBEsquerdaFolgasMedidasValidasAntesDoAcidente);

    /*
    FOLGAS - MEDIDAS APÓS O ACIDENTE
     */

        aEsquerdaFolgasAposOAcidente = faker.random().nextLong(999999999l);
        aDireitaFolgasAposOAcidente = faker.random().nextLong(999999999l);
        bEsquerdaFolgasAposOAcidente = faker.random().nextLong(999999999l);
        bDireitaFolgasAposOAcidente = faker.random().nextLong(999999999l);
        aEsquerdaMaisBDireitaFolgasAposOAcidente = aEsquerdaFolgasAposOAcidente + bDireitaFolgasAposOAcidente;
        aDireitaMaisBEsquerdaFolgasAposOAcidente = aDireitaFolgasAposOAcidente + bEsquerdaFolgasAposOAcidente;
        valorAbsolutoFolgasAposOAcidente = Math.abs(aEsquerdaMaisBDireitaFolgasAposOAcidente - aDireitaMaisBEsquerdaFolgasAposOAcidente);

    /*
    OUTRAS INFORMAÇÕES
     */
        extensaoDoTrafegoDescarrilado = String.valueOf(getRandom(null));
        modeloAmparaBalancoIndex = faker.options().option(1, 2, 3, 4, 5, 6, 7);

    /*
    CASTANHA/TAMPA DO AMPARA BALANÇO
     */
        condicaoCastanhaTampaDoAmparaBalancoIndex = faker.options().option(1, 2, 3, 4, 5, 6, 7, 8);

    /*
    PARAFUSOS DE FIXAÇÃO (CONDIÇÃO)
     */
        aEsquerdaParafusosDeFixacao = faker.random().nextLong(999999999l);
        bEsquerdaParafusosDeFixacao = faker.random().nextLong(999999999l);
        aDireitaParafusosDeFixacao = faker.random().nextLong(999999999l);
        bDireitaParafusosDeFixacao = faker.random().nextLong(999999999l);

    /*
    MEDIDA DE DESGASTE DA CHAPA SUPERIOR DO AB
     */
        aEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB = faker.random().nextLong(999999999l);
        bEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB = faker.random().nextLong(999999999l);
        aDireitaMedidasDeDesgasteDaChapaSuperiorDoAB = faker.random().nextLong(999999999l);
        bDireitaMedidasDeDesgasteDaChapaSuperiorDoAB = faker.random().nextLong(999999999l);

    /*
    FREIO
     */

        valvulaDeFreioIndex = faker.options().option(1, 2, 3, 4, 5);
        freioManualIndex = faker.options().option(1, 2, 3);
        torneiraAngularEGIndex = faker.options().option(1, 2);
        dispositivoVazioCarregadoIndex = faker.options().option(1, 2, 3, 4);
        torneiraIsolamentoDeFreioIndex = faker.options().option(1, 2, 3);
        posicaoDaValvulaRetentoraIndex = faker.options().option(1, 2, 3);

    /*
    DDV (DETECTOR DE DESCARRILAMENTO DE VAGÃO)
     */
        preDescarrilamentoIndex = faker.options().option(1, 2, 3);

        dDVAcionouRodeiro1Index = faker.options().option(1, 2);
        dDVAcionouRodeiro2Index = faker.options().option(1, 2);
        dDVAcionouRodeiro3Index = faker.options().option(1, 2);
        dDVAcionouRodeiro4Index = faker.options().option(1, 2);

    /*
    ESTRUTURA DO VAGÃO
     */
        condicaoLongarinaIndex = faker.options().option(1, 2, 3, 4, 5);
        localizacaoLongarinaIndex = faker.options().option(1, 2, 3, 4, 5, 6, 7);

    /*
    TRAVESSA DO PRATO DO PIÃO SUPERIOR
     */
        condicaoTravessaDoPratoDoPiaoSuperiorIndex = faker.options().option(1, 2, 3, 4, 5);
        localizacaoTravessaDoPratoDoPiaoSuperiorIndex = faker.options().option(1, 2, 3, 4, 5, 6, 7);

    /*
    LONGARINA LATERAL
     */
        condicaoLongarinaLateralIndex = faker.options().option(1, 2, 3, 4, 5);
        localizacaoLongarinaLateralIndex = faker.options().option(1, 2, 3, 4, 5, 6, 7);

    /*
    ENGATES E ACTS
     */
        qualOTipoDoEngateIndex = faker.options().option(1, 2, 3, 4, 5, 6, 7);
        alturaEngateA = String.valueOf(getRandom(null));
        alturaEngateB = String.valueOf(getRandom(null));
        engateEstaNiveladoIndex = faker.options().option(1, 2);
        existeMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaIndex = faker.options().option(1, 2);

    /*
    ACT
     */
        qualOModeloIndex = faker.options().option(1, 2, 3, 4, 5, 6);
        existeFolgaNoACT = faker.bool().bool();
        qualAMedidaDaFolga = faker.random().nextLong(999999999l);

    /*
    COMPONENTES DO ACT/ENGATE
     */
        engateComponentesACTEngateIndex = faker.options().option(1, 2, 3, 4, 5);
        chavetaEPinoTIndex = faker.options().option(1, 2, 3, 4, 5);
        mandibulaEPinoIndex = faker.options().option(1, 2, 3, 4, 5);
        pinoY47ETravaIndex = faker.options().option(1, 2, 3, 4, 5);

    /*
    INFORMAÇÕES ADICIONAIS
     */
        paraPecasFraturadasEspecificarTrincaAntigaOuRecente = getRandomCharacters(2000);
        localDoPODTemMarcasDescarrilamentoAnterior = faker.bool().bool();
        localDoPODTemHistoricoDeAcidentes06Meses = faker.bool().bool();
        haMarcasDePecaDoVagaoArrastandoNosDormentesAntesDoPOD = faker.bool().bool();
        qualPeca = getRandomCharacters(512);
    }

    public int getParametrizacaoIndex() {
        return parametrizacaoIndex;
    }

    public String getParametrizacaoValue() {
        return parametrizacaoValue;
    }

    public void setParametrizacaoValue(String parametrizacaoValue) {
        this.parametrizacaoValue = parametrizacaoValue;
    }

    public int getBitolaIndex() {
        return bitolaIndex;
    }

    public String getBitolaValue() {
        return bitolaValue;
    }

    public void setBitolaValue(String bitolaValue) {
        this.bitolaValue = bitolaValue;
    }

    public String getEstadoDoVagao() {
        return estadoDoVagao;
    }

    public boolean isHaviaDDVNoLocal() {
        return haviaDDVNoLocal;
    }

    public boolean isEstavaOperacional() {
        return estavaOperacional;
    }

    public boolean isAtuou() {
        return atuou;
    }

    public String getComentarioDDV() {
        return comentarioDDV;
    }

    public int getComoEstavaACargaIndex() {
        return comoEstavaACargaIndex;
    }

    public String getComoEstavaACargaValue() {
        return comoEstavaACargaValue;
    }

    public void setComoEstavaACargaValue(String comoEstavaACargaValue) {
        this.comoEstavaACargaValue = comoEstavaACargaValue;
    }

    public String getTipoDeMercadoria() {
        return tipoDeMercadoria;
    }

    public long getPesoBrutoDoVeiculo() {
        return pesoBrutoDoVeiculo;
    }

    public boolean isEtiquetadoOuRecomendadoOficina() {
        return etiquetadoOuRecomendadoOficina;
    }

    public String getMedicaoRealizada() {
        return medicaoRealizada;
    }

    public String getLocalDaMedicao() {
        return localDaMedicao;
    }

    public String getDataUltimaManutencaoPreventiva() {
        return dataUltimaManutencaoPreventiva;
    }

    public String getTipoUltimaManutencaoPreventiva() {
        return tipoUltimaManutencaoPreventiva;
    }

    public String getLocalUltimaManutencaoPreventiva() {
        return localUltimaManutencaoPreventiva;
    }

    public String getDataUltimaManutencaoCorretiva() {
        return dataUltimaManutencaoCorretiva;
    }

    public String getTratativaUltimaManutencaoCorretiva() {
        return tratativaUltimaManutencaoCorretiva;
    }

    public String getLocalUltimaManutencaoCorretiva() {
        return localUltimaManutencaoCorretiva;
    }

    public int getPrimeiroRodeiroADescarrilarPosicaoIndex() {
        return primeiroRodeiroADescarrilarPosicaoIndex;
    }

    public String getPrimeiroRodeiroADescarrilarPosicaoValue() {
        return primeiroRodeiroADescarrilarPosicaoValue;
    }

    public void setPrimeiroRodeiroADescarrilarPosicaoValue(String primeiroRodeiroADescarrilarPosicaoValue) {
        this.primeiroRodeiroADescarrilarPosicaoValue = primeiroRodeiroADescarrilarPosicaoValue;
    }

    public boolean isRodeirosDescarriladosR1() {
        return rodeirosDescarriladosR1;
    }

    public boolean isRodeirosDescarriladosR2() {
        return rodeirosDescarriladosR2;
    }

    public boolean isRodeirosDescarriladosR3() {
        return rodeirosDescarriladosR3;
    }

    public boolean isRodeirosDescarriladosR4() {
        return rodeirosDescarriladosR4;
    }

    public String getSituacaoDaRodaValue() {
        return situacaoDaRodaValue;
    }

    public void setSituacaoDaRodaValue(String situacaoDaRodaValue) {
        this.situacaoDaRodaValue = situacaoDaRodaValue;
    }

    public int getSituacaoDaRodaIndex() {
        return situacaoDaRodaIndex;
    }

    public String getFabricacaoDaRodaValue() {
        return fabricacaoDaRodaValue;
    }

    public void setFabricacaoDaRodaValue(String fabricacaoDaRodaValue) {
        this.fabricacaoDaRodaValue = fabricacaoDaRodaValue;
    }

    public int getFabricacaoDaRodaIndex() {
        return fabricacaoDaRodaIndex;
    }

    public String getEixoValue() {
        return eixoValue;
    }

    public void setEixoValue(String eixoValue) {
        this.eixoValue = eixoValue;
    }

    public int getEixoIndex() {
        return eixoIndex;
    }

    public String getTipoDeTruqueValue() {
        return tipoDeTruqueValue;
    }

    public void setTipoDeTruqueValue(String tipoDeTruqueValue) {
        this.tipoDeTruqueValue = tipoDeTruqueValue;
    }

    public int getTipoDeTruqueIndex() {
        return tipoDeTruqueIndex;
    }

    public String getMangaDoVagaoValue() {
        return mangaDoVagaoValue;
    }

    public void setMangaDoVagaoValue(String mangaDoVagaoValue) {
        this.mangaDoVagaoValue = mangaDoVagaoValue;
    }

    public int getMangaDoVagaoIndex() {
        return mangaDoVagaoIndex;
    }

    public boolean isSinaisDeSuperAquecimentoNaRoda() {
        return sinaisDeSuperAquecimentoNaRoda;
    }

    public long getMedidaAquecimento() {
        return medidaAquecimento;
    }

    public String getTipoDoRolamentoValue() {
        return tipoDoRolamentoValue;
    }

    public void setTipoDoRolamentoValue(String tipoDoRolamentoValue) {
        this.tipoDoRolamentoValue = tipoDoRolamentoValue;
    }

    public int getTipoDoRolamentoIndex() {
        return tipoDoRolamentoIndex;
    }

    public String getCorLubrificacaoDoRolamentoValue() {
        return corLubrificacaoDoRolamentoValue;
    }

    public void setCorLubrificacaoDoRolamentoValue(String corLubrificacaoDoRolamentoValue) {
        this.corLubrificacaoDoRolamentoValue = corLubrificacaoDoRolamentoValue;
    }

    public int getCorLubrificacaoDoRolamentoIndex() {
        return corLubrificacaoDoRolamentoIndex;
    }

    public String getIdentificacaoDoEixo() {
        return identificacaoDoEixo;
    }

    public String getNumeroDaFITA() {
        return numeroDaFITA;
    }

    public String getNumeroDoEixo() {
        return numeroDoEixo;
    }

    public boolean isInfovagaoCroquiErRodeirosDescarriladosR1() {
        return infovagaoCroquiErRodeirosDescarriladosR1;
    }

    public boolean isInfovagaoCroquiErRodeirosDescarriladosR2() {
        return infovagaoCroquiErRodeirosDescarriladosR2;
    }

    public boolean isInfovagaoCroquiErRodeirosDescarriladosR3() {
        return infovagaoCroquiErRodeirosDescarriladosR3;
    }

    public boolean isInfovagaoCroquiErRodeirosDescarriladosR4() {
        return infovagaoCroquiErRodeirosDescarriladosR4;
    }

    public boolean isInfovagaoCroquiCabeceiraA() {
        return infovagaoCroquiCabeceiraA;
    }

    public boolean isInfovagaoCroquiCabeceiraB() {
        return infovagaoCroquiCabeceiraB;
    }

    public boolean isInfovagaoCroquiDrRodeirosDescarriladosR1() {
        return infovagaoCroquiDrRodeirosDescarriladosR1;
    }

    public boolean isInfovagaoCroquiDrRodeirosDescarriladosR2() {
        return infovagaoCroquiDrRodeirosDescarriladosR2;
    }

    public boolean isInfovagaoCroquiDrRodeirosDescarriladosR3() {
        return infovagaoCroquiDrRodeirosDescarriladosR3;
    }

    public boolean isInfovagaoCroquiDrRodeirosDescarriladosR4() {
        return infovagaoCroquiDrRodeirosDescarriladosR4;
    }

    public String getMarcarOSentidoDoDeslocamentoDoVagao() {
        return marcarOSentidoDoDeslocamentoDoVagao;
    }

    public String getTipoPratoPiao() {
        return tipoPratoPiao;
    }

    public String getFolgaNoDiametro() {
        return folgaNoDiametro;
    }

    public String getFolgaVertical() {
        return folgaVertical;
    }

    public String getDiametroPratoPiaoInferior() {
        return diametroPratoPiaoInferior;
    }

    public String getProfundidadePratoPiaoInferior() {
        return profundidadePratoPiaoInferior;
    }

    public String getDiametroPratoPiaoSuperior() {
        return diametroPratoPiaoSuperior;
    }

    public String getAlturaPratoPiaoSuperior() {
        return alturaPratoPiaoSuperior;
    }

    public String getDireita1CunhaDeFriccaoValue() {
        return direita1CunhaDeFriccaoValue;
    }

    public void setDireita1CunhaDeFriccaoValue(String direita1CunhaDeFriccaoValue) {
        this.direita1CunhaDeFriccaoValue = direita1CunhaDeFriccaoValue;
    }

    public int getDireita1CunhaDeFriccaoIndex() {
        return direita1CunhaDeFriccaoIndex;
    }

    public String getDireita2CunhaDeFriccaoValue() {
        return direita2CunhaDeFriccaoValue;
    }

    public void setDireita2CunhaDeFriccaoValue(String direita2CunhaDeFriccaoValue) {
        this.direita2CunhaDeFriccaoValue = direita2CunhaDeFriccaoValue;
    }

    public int getDireita2CunhaDeFriccaoIndex() {
        return direita2CunhaDeFriccaoIndex;
    }

    public String getDireita3CunhaDeFriccaoValue() {
        return direita3CunhaDeFriccaoValue;
    }

    public void setDireita3CunhaDeFriccaoValue(String direita3CunhaDeFriccaoValue) {
        this.direita3CunhaDeFriccaoValue = direita3CunhaDeFriccaoValue;
    }

    public int getDireita3CunhaDeFriccaoIndex() {
        return direita3CunhaDeFriccaoIndex;
    }

    public String getDireita4CunhaDeFriccaoValue() {
        return direita4CunhaDeFriccaoValue;
    }

    public void setDireita4CunhaDeFriccaoValue(String direita4CunhaDeFriccaoValue) {
        this.direita4CunhaDeFriccaoValue = direita4CunhaDeFriccaoValue;
    }

    public int getDireita4CunhaDeFriccaoIndex() {
        return direita4CunhaDeFriccaoIndex;
    }

    public String getEsquerda1CunhaDeFriccaoValue() {
        return esquerda1CunhaDeFriccaoValue;
    }

    public void setEsquerda1CunhaDeFriccaoValue(String esquerda1CunhaDeFriccaoValue) {
        this.esquerda1CunhaDeFriccaoValue = esquerda1CunhaDeFriccaoValue;
    }

    public int getEsquerda1CunhaDeFriccaoIndex() {
        return esquerda1CunhaDeFriccaoIndex;
    }

    public String getEsquerda2CunhaDeFriccaoValue() {
        return esquerda2CunhaDeFriccaoValue;
    }

    public void setEsquerda2CunhaDeFriccaoValue(String esquerda2CunhaDeFriccaoValue) {
        this.esquerda2CunhaDeFriccaoValue = esquerda2CunhaDeFriccaoValue;
    }

    public int getEsquerda2CunhaDeFriccaoIndex() {
        return esquerda2CunhaDeFriccaoIndex;
    }

    public String getEsquerda3CunhaDeFriccaoValue() {
        return esquerda3CunhaDeFriccaoValue;
    }

    public void setEsquerda3CunhaDeFriccaoValue(String esquerda3CunhaDeFriccaoValue) {
        this.esquerda3CunhaDeFriccaoValue = esquerda3CunhaDeFriccaoValue;
    }

    public int getEsquerda3CunhaDeFriccaoIndex() {
        return esquerda3CunhaDeFriccaoIndex;
    }

    public String getEsquerda4CunhaDeFriccaoValue() {
        return esquerda4CunhaDeFriccaoValue;
    }

    public void setEsquerda4CunhaDeFriccaoValue(String esquerda4CunhaDeFriccaoValue) {
        this.esquerda4CunhaDeFriccaoValue = esquerda4CunhaDeFriccaoValue;
    }

    public int getEsquerda4CunhaDeFriccaoIndex() {
        return esquerda4CunhaDeFriccaoIndex;
    }

    public String getDireita1PlacaDeDesgasteColunaLatValue() {
        return direita1PlacaDeDesgasteColunaLatValue;
    }

    public void setDireita1PlacaDeDesgasteColunaLatValue(String direita1PlacaDeDesgasteColunaLatValue) {
        this.direita1PlacaDeDesgasteColunaLatValue = direita1PlacaDeDesgasteColunaLatValue;
    }

    public int getDireita1PlacaDeDesgasteColunaLatIndex() {
        return direita1PlacaDeDesgasteColunaLatIndex;
    }

    public String getDireita2PlacaDeDesgasteColunaLatValue() {
        return direita2PlacaDeDesgasteColunaLatValue;
    }

    public void setDireita2PlacaDeDesgasteColunaLatValue(String direita2PlacaDeDesgasteColunaLatValue) {
        this.direita2PlacaDeDesgasteColunaLatValue = direita2PlacaDeDesgasteColunaLatValue;
    }

    public int getDireita2PlacaDeDesgasteColunaLatIndex() {
        return direita2PlacaDeDesgasteColunaLatIndex;
    }

    public String getDireita3PlacaDeDesgasteColunaLatValue() {
        return direita3PlacaDeDesgasteColunaLatValue;
    }

    public void setDireita3PlacaDeDesgasteColunaLatValue(String direita3PlacaDeDesgasteColunaLatValue) {
        this.direita3PlacaDeDesgasteColunaLatValue = direita3PlacaDeDesgasteColunaLatValue;
    }

    public int getDireita3PlacaDeDesgasteColunaLatIndex() {
        return direita3PlacaDeDesgasteColunaLatIndex;
    }

    public String getDireita4PlacaDeDesgasteColunaLatValue() {
        return direita4PlacaDeDesgasteColunaLatValue;
    }

    public void setDireita4PlacaDeDesgasteColunaLatValue(String direita4PlacaDeDesgasteColunaLatValue) {
        this.direita4PlacaDeDesgasteColunaLatValue = direita4PlacaDeDesgasteColunaLatValue;
    }

    public int getDireita4PlacaDeDesgasteColunaLatIndex() {
        return direita4PlacaDeDesgasteColunaLatIndex;
    }

    public String getEsquerda1PlacaDeDesgasteColunaLatValue() {
        return esquerda1PlacaDeDesgasteColunaLatValue;
    }

    public void setEsquerda1PlacaDeDesgasteColunaLatValue(String esquerda1PlacaDeDesgasteColunaLatValue) {
        this.esquerda1PlacaDeDesgasteColunaLatValue = esquerda1PlacaDeDesgasteColunaLatValue;
    }

    public int getEsquerda1PlacaDeDesgasteColunaLatIndex() {
        return esquerda1PlacaDeDesgasteColunaLatIndex;
    }

    public String getEsquerda2PlacaDeDesgasteColunaLatValue() {
        return esquerda2PlacaDeDesgasteColunaLatValue;
    }

    public void setEsquerda2PlacaDeDesgasteColunaLatValue(String esquerda2PlacaDeDesgasteColunaLatValue) {
        this.esquerda2PlacaDeDesgasteColunaLatValue = esquerda2PlacaDeDesgasteColunaLatValue;
    }

    public int getEsquerda2PlacaDeDesgasteColunaLatIndex() {
        return esquerda2PlacaDeDesgasteColunaLatIndex;
    }

    public String getEsquerda3PlacaDeDesgasteColunaLatValue() {
        return esquerda3PlacaDeDesgasteColunaLatValue;
    }

    public void setEsquerda3PlacaDeDesgasteColunaLatValue(String esquerda3PlacaDeDesgasteColunaLatValue) {
        this.esquerda3PlacaDeDesgasteColunaLatValue = esquerda3PlacaDeDesgasteColunaLatValue;
    }

    public int getEsquerda3PlacaDeDesgasteColunaLatIndex() {
        return esquerda3PlacaDeDesgasteColunaLatIndex;
    }

    public String getEsquerda4PlacaDeDesgasteColunaLatValue() {
        return esquerda4PlacaDeDesgasteColunaLatValue;
    }

    public void setEsquerda4PlacaDeDesgasteColunaLatValue(String esquerda4PlacaDeDesgasteColunaLatValue) {
        this.esquerda4PlacaDeDesgasteColunaLatValue = esquerda4PlacaDeDesgasteColunaLatValue;
    }

    public int getEsquerda4PlacaDeDesgasteColunaLatIndex() {
        return esquerda4PlacaDeDesgasteColunaLatIndex;
    }

    public long getDireitaABotoesLateralDoTruque() {
        return direitaABotoesLateralDoTruque;
    }

    public long getEsquerdaABotoesLateralDoTruque() {
        return esquerdaABotoesLateralDoTruque;
    }

    public long getDireitaBBotoesLateralDoTruque() {
        return direitaBBotoesLateralDoTruque;
    }

    public long getEsquerdaBBotoesLateralDoTruque() {
        return esquerdaBBotoesLateralDoTruque;
    }

    public long getDiferencaABotoesLateralDoTruque() {
        return diferencaABotoesLateralDoTruque;
    }

    public long getDiferencaBBotoesLateralDoTruque() {
        return diferencaBBotoesLateralDoTruque;
    }

    public long getAlturaDasCunhasCabALadoDiteiro1() {
        return alturaDasCunhasCabALadoDiteiro1;
    }

    public long getAlturaDasCunhasCabALadoDiteiro2() {
        return alturaDasCunhasCabALadoDiteiro2;
    }

    public long getAlturaDasCunhasCabALadoEsquerdo1() {
        return alturaDasCunhasCabALadoEsquerdo1;
    }

    public long getAlturaDasCunhasCabALadoEsquerdo2() {
        return alturaDasCunhasCabALadoEsquerdo2;
    }

    public long getAlturaDasCunhasCabBLadoDiteiro3() {
        return alturaDasCunhasCabBLadoDiteiro3;
    }

    public long getAlturaDasCunhasCabBLadoDiteiro4() {
        return alturaDasCunhasCabBLadoDiteiro4;
    }

    public long getAlturaDasCunhasCabBLadoEsquerdo3() {
        return alturaDasCunhasCabBLadoEsquerdo3;
    }

    public long getAlturaDasCunhasCabBLadoEsquerdo4() {
        return alturaDasCunhasCabBLadoEsquerdo4;
    }

    public long getExternaAlturaDasMolasDoTruqueDescarrilado() {
        return externaAlturaDasMolasDoTruqueDescarrilado;
    }

    public long getIntermediariaAlturaDasMolasDoTruqueDescarrilado() {
        return intermediariaAlturaDasMolasDoTruqueDescarrilado;
    }

    public long getInternaAlturaDasMolasDoTruqueDescarrilado() {
        return internaAlturaDasMolasDoTruqueDescarrilado;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD5() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD5;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD6() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD6;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD7() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD7;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD8() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD8;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD5() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD5;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD6() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD6;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD7() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD7;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD8() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD8;
    }

    public long getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1() {
        return molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD1;
    }

    public long getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2() {
        return molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD2;
    }

    public long getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3() {
        return molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD3;
    }

    public long getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4() {
        return molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD4;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE5() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE5;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE6() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE6;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE7() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE7;
    }

    public long getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE8() {
        return molasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE8;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE5() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE5;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE6() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE6;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE7() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE7;
    }

    public long getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE8() {
        return molasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE8;
    }

    public long getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1() {
        return molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE1;
    }

    public long getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2() {
        return molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE2;
    }

    public long getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3() {
        return molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE3;
    }

    public long getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4() {
        return molasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE4;
    }

    public int getSobreAlturaDasMolasCunhasIndex() {
        return sobreAlturaDasMolasCunhasIndex;
    }

    public String getSobreAlturaDasMolasCunhasValue() {
        return sobreAlturaDasMolasCunhasValue;
    }

    public void setSobreAlturaDasMolasCunhasValue(String sobreAlturaDasMolasCunhasValue) {
        this.sobreAlturaDasMolasCunhasValue = sobreAlturaDasMolasCunhasValue;
    }

    public long getMolasExternasAlturaDasMolasCunhasLadoDireitoLD1() {
        return molasExternasAlturaDasMolasCunhasLadoDireitoLD1;
    }

    public long getMolasExternasAlturaDasMolasCunhasLadoDireitoLD2() {
        return molasExternasAlturaDasMolasCunhasLadoDireitoLD2;
    }

    public long getMolasInternasAlturaDasMolasCunhasLadoDireitoLD1() {
        return molasInternasAlturaDasMolasCunhasLadoDireitoLD1;
    }

    public long getMolasInternasAlturaDasMolasCunhasLadoDireitoLD2() {
        return molasInternasAlturaDasMolasCunhasLadoDireitoLD2;
    }

    public long getMolasExternasAlturaDasMolasCunhasLadoEsquerdoLE1() {
        return molasExternasAlturaDasMolasCunhasLadoEsquerdoLE1;
    }

    public long getMolasExternasAlturaDasMolasCunhasLadoEsquerdoLE2() {
        return molasExternasAlturaDasMolasCunhasLadoEsquerdoLE2;
    }


    /*
    Lado esquerdo
     */

    public long getMolasInternasAlturaDasMolasCunhasLadoEsquerdoLE1() {
        return molasInternasAlturaDasMolasCunhasLadoEsquerdoLE1;
    }

    public long getMolasInternasAlturaDasMolasCunhasLadoEsquerdoLE2() {
        return molasInternasAlturaDasMolasCunhasLadoEsquerdoLE2;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD5() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD5;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD6() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD6;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD7() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD7;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD8() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD8;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD5() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD5;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD6() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD6;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD7() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD7;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD8() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD8;
    }

    public long getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1() {
        return molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD1;
    }

    public long getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2() {
        return molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD2;
    }

    public long getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3() {
        return molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD3;
    }

    public long getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4() {
        return molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD4;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE5() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE5;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE6() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE6;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE7() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE7;
    }

    public long getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE8() {
        return molasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE8;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE5() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE5;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE6() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE6;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE7() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE7;
    }

    public long getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE8() {
        return molasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE8;
    }

    public long getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1() {
        return molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE1;
    }

    public long getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2() {
        return molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE2;
    }

    public long getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3() {
        return molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE3;
    }

    public long getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4() {
        return molasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE4;
    }

    public int getSobreAlturaDasMolasCunhasOutraCabeceiraIndex() {
        return sobreAlturaDasMolasCunhasOutraCabeceiraIndex;
    }

    public String getSobreAlturaDasMolasCunhasOutraCabeceiraValue() {
        return sobreAlturaDasMolasCunhasOutraCabeceiraValue;
    }

    public void setSobreAlturaDasMolasCunhasOutraCabeceiraValue(String sobreAlturaDasMolasCunhasOutraCabeceiraValue) {
        this.sobreAlturaDasMolasCunhasOutraCabeceiraValue = sobreAlturaDasMolasCunhasOutraCabeceiraValue;
    }

    public long getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1() {
        return molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1;
    }

    public long getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2() {
        return molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2;
    }

    public long getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1() {
        return molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1;
    }

    public long getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2() {
        return molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2;
    }

    public long getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1() {
        return molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1;
    }

    public long getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2() {
        return molasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2;
    }

    public long getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1() {
        return molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1;
    }

    public long getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2() {
        return molasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2;
    }

    public String getLocalFolgasMedidasValidasAntesDoAcidente() {
        return localFolgasMedidasValidasAntesDoAcidente;
    }

    public String getDataFolgasMedidasValidasAntesDoAcidente() {
        return dataFolgasMedidasValidasAntesDoAcidente;
    }

    public String getTipoManutencaoFolgasMedidasValidasAntesDoAcidenteValue() {
        return tipoManutencaoFolgasMedidasValidasAntesDoAcidenteValue;
    }

    public void setTipoManutencaoFolgasMedidasValidasAntesDoAcidenteValue(String tipoManutencaoFolgasMedidasValidasAntesDoAcidenteValue) {
        this.tipoManutencaoFolgasMedidasValidasAntesDoAcidenteValue = tipoManutencaoFolgasMedidasValidasAntesDoAcidenteValue;
    }

    public int getTipoManutencaoFolgasMedidasValidasAntesDoAcidenteIndex() {
        return tipoManutencaoFolgasMedidasValidasAntesDoAcidenteIndex;
    }

    public long getaEsquerdaFolgasMedidasValidasAntesDoAcidente() {
        return aEsquerdaFolgasMedidasValidasAntesDoAcidente;
    }

    public long getaDireitaFolgasMedidasValidasAntesDoAcidente() {
        return aDireitaFolgasMedidasValidasAntesDoAcidente;
    }

    public long getbEsquerdaFolgasMedidasValidasAntesDoAcidente() {
        return bEsquerdaFolgasMedidasValidasAntesDoAcidente;
    }

    public long getbDireitaFolgasMedidasValidasAntesDoAcidente() {
        return bDireitaFolgasMedidasValidasAntesDoAcidente;
    }

    public long getaEsquerdaMaisBDireitaFolgasMedidasValidasAntesDoAcidente() {
        return aEsquerdaMaisBDireitaFolgasMedidasValidasAntesDoAcidente;
    }

    public long getaDireitaMaisBEsquerdaFolgasMedidasValidasAntesDoAcidente() {
        return aDireitaMaisBEsquerdaFolgasMedidasValidasAntesDoAcidente;
    }

    public long getValorAbsolutoFolgasMedidasValidasAntesDoAcidente() {
        return valorAbsolutoFolgasMedidasValidasAntesDoAcidente;
    }

    public long getaEsquerdaFolgasAposOAcidente() {
        return aEsquerdaFolgasAposOAcidente;
    }

    public long getaDireitaFolgasAposOAcidente() {
        return aDireitaFolgasAposOAcidente;
    }

    public long getbEsquerdaFolgasAposOAcidente() {
        return bEsquerdaFolgasAposOAcidente;
    }

    public long getbDireitaFolgasAposOAcidente() {
        return bDireitaFolgasAposOAcidente;
    }

    public long getaEsquerdaMaisBDireitaFolgasAposOAcidente() {
        return aEsquerdaMaisBDireitaFolgasAposOAcidente;
    }

    /*
    ALTURA DAS MOLAS DAS CUNHAS OUTRA CABECEIRA
     */

    public long getaDireitaMaisBEsquerdaFolgasAposOAcidente() {
        return aDireitaMaisBEsquerdaFolgasAposOAcidente;
    }

    public long getValorAbsolutoFolgasAposOAcidente() {
        return valorAbsolutoFolgasAposOAcidente;
    }

    public String getExtensaoDoTrafegoDescarrilado() {
        return extensaoDoTrafegoDescarrilado;
    }

    public int getModeloAmparaBalancoIndex() {
        return modeloAmparaBalancoIndex;
    }

    public String getModeloAmparaBalancoValue() {
        return modeloAmparaBalancoValue;
    }

    public void setModeloAmparaBalancoValue(String modeloAmparaBalancoValue) {
        this.modeloAmparaBalancoValue = modeloAmparaBalancoValue;
    }

    public String getCondicaoCastanhaTampaDoAmparaBalancoValue() {
        return condicaoCastanhaTampaDoAmparaBalancoValue;
    }

    public void setCondicaoCastanhaTampaDoAmparaBalancoValue(String condicaoCastanhaTampaDoAmparaBalancoValue) {
        this.condicaoCastanhaTampaDoAmparaBalancoValue = condicaoCastanhaTampaDoAmparaBalancoValue;
    }

    public int getCondicaoCastanhaTampaDoAmparaBalancoIndex() {
        return condicaoCastanhaTampaDoAmparaBalancoIndex;
    }

    public long getaEsquerdaParafusosDeFixacao() {
        return aEsquerdaParafusosDeFixacao;
    }

    public long getbEsquerdaParafusosDeFixacao() {
        return bEsquerdaParafusosDeFixacao;
    }

    public long getaDireitaParafusosDeFixacao() {
        return aDireitaParafusosDeFixacao;
    }

    public long getbDireitaParafusosDeFixacao() {
        return bDireitaParafusosDeFixacao;
    }

    public long getaEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB() {
        return aEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB;
    }

    public long getbEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB() {
        return bEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB;
    }

    public long getaDireitaMedidasDeDesgasteDaChapaSuperiorDoAB() {
        return aDireitaMedidasDeDesgasteDaChapaSuperiorDoAB;
    }

    public long getbDireitaMedidasDeDesgasteDaChapaSuperiorDoAB() {
        return bDireitaMedidasDeDesgasteDaChapaSuperiorDoAB;
    }

    public String getValvulaDeFreioValue() {
        return valvulaDeFreioValue;
    }

    public void setValvulaDeFreioValue(String valvulaDeFreioValue) {
        this.valvulaDeFreioValue = valvulaDeFreioValue;
    }

    public int getValvulaDeFreioIndex() {
        return valvulaDeFreioIndex;
    }

    public String getFreioManualValue() {
        return freioManualValue;
    }

    /*
    FOLGAS - MEDIDAS APÓS O ACIDENTE
     */

    public void setFreioManualValue(String freioManualValue) {
        this.freioManualValue = freioManualValue;
    }

    public int getFreioManualIndex() {
        return freioManualIndex;
    }

    public String getTorneiraAngularEGValue() {
        return torneiraAngularEGValue;
    }

    public void setTorneiraAngularEGValue(String torneiraAngularEGValue) {
        this.torneiraAngularEGValue = torneiraAngularEGValue;
    }

    public int getTorneiraAngularEGIndex() {
        return torneiraAngularEGIndex;
    }

    public String getDispositivoVazioCarregadoValue() {
        return dispositivoVazioCarregadoValue;
    }

    public void setDispositivoVazioCarregadoValue(String dispositivoVazioCarregadoValue) {
        this.dispositivoVazioCarregadoValue = dispositivoVazioCarregadoValue;
    }

    public int getDispositivoVazioCarregadoIndex() {
        return dispositivoVazioCarregadoIndex;
    }

    public String getTorneiraIsolamentoDeFreioValue() {
        return torneiraIsolamentoDeFreioValue;
    }

    public void setTorneiraIsolamentoDeFreioValue(String torneiraIsolamentoDeFreioValue) {
        this.torneiraIsolamentoDeFreioValue = torneiraIsolamentoDeFreioValue;
    }

    public int getTorneiraIsolamentoDeFreioIndex() {
        return torneiraIsolamentoDeFreioIndex;
    }

    public String getPosicaoDaValvulaRetentoraValue() {
        return posicaoDaValvulaRetentoraValue;
    }

    public void setPosicaoDaValvulaRetentoraValue(String posicaoDaValvulaRetentoraValue) {
        this.posicaoDaValvulaRetentoraValue = posicaoDaValvulaRetentoraValue;
    }

    public int getPosicaoDaValvulaRetentoraIndex() {
        return posicaoDaValvulaRetentoraIndex;
    }

    public String getPreDescarrilamentoValue() {
        return preDescarrilamentoValue;
    }

    public void setPreDescarrilamentoValue(String preDescarrilamentoValue) {
        this.preDescarrilamentoValue = preDescarrilamentoValue;
    }

    public int getPreDescarrilamentoIndex() {
        return preDescarrilamentoIndex;
    }

    public String getdDVAcionouRodeiro1Value() {
        return dDVAcionouRodeiro1Value;
    }

    public void setdDVAcionouRodeiro1Value(String dDVAcionouRodeiro1Value) {
        this.dDVAcionouRodeiro1Value = dDVAcionouRodeiro1Value;
    }

    public int getdDVAcionouRodeiro1Index() {
        return dDVAcionouRodeiro1Index;
    }

    /*
    FREIO
     */

    public String getdDVAcionouRodeiro2Value() {
        return dDVAcionouRodeiro2Value;
    }

    public void setdDVAcionouRodeiro2Value(String dDVAcionouRodeiro2Value) {
        this.dDVAcionouRodeiro2Value = dDVAcionouRodeiro2Value;
    }

    public int getdDVAcionouRodeiro2Index() {
        return dDVAcionouRodeiro2Index;
    }

    public String getdDVAcionouRodeiro3Value() {
        return dDVAcionouRodeiro3Value;
    }

    public void setdDVAcionouRodeiro3Value(String dDVAcionouRodeiro3Value) {
        this.dDVAcionouRodeiro3Value = dDVAcionouRodeiro3Value;
    }

    public int getdDVAcionouRodeiro3Index() {
        return dDVAcionouRodeiro3Index;
    }

    public String getdDVAcionouRodeiro4Value() {
        return dDVAcionouRodeiro4Value;
    }

    public void setdDVAcionouRodeiro4Value(String dDVAcionouRodeiro4Value) {
        this.dDVAcionouRodeiro4Value = dDVAcionouRodeiro4Value;
    }

    public int getdDVAcionouRodeiro4Index() {
        return dDVAcionouRodeiro4Index;
    }

    public String getCondicaoLongarinaValue() {
        return condicaoLongarinaValue;
    }

    public void setCondicaoLongarinaValue(String condicaoLongarinaValue) {
        this.condicaoLongarinaValue = condicaoLongarinaValue;
    }

    public int getCondicaoLongarinaIndex() {
        return condicaoLongarinaIndex;
    }

    public String getLocalizacaoLongarinaValue() {
        return localizacaoLongarinaValue;
    }

    public void setLocalizacaoLongarinaValue(String localizacaoLongarinaValue) {
        this.localizacaoLongarinaValue = localizacaoLongarinaValue;
    }

    public int getLocalizacaoLongarinaIndex() {
        return localizacaoLongarinaIndex;
    }

    public String getCondicaoTravessaDoPratoDoPiaoSuperiorValue() {
        return condicaoTravessaDoPratoDoPiaoSuperiorValue;
    }

    public void setCondicaoTravessaDoPratoDoPiaoSuperiorValue(String condicaoTravessaDoPratoDoPiaoSuperiorValue) {
        this.condicaoTravessaDoPratoDoPiaoSuperiorValue = condicaoTravessaDoPratoDoPiaoSuperiorValue;
    }

    public int getCondicaoTravessaDoPratoDoPiaoSuperiorIndex() {
        return condicaoTravessaDoPratoDoPiaoSuperiorIndex;
    }

    public String getLocalizacaoTravessaDoPratoDoPiaoSuperiorValue() {
        return localizacaoTravessaDoPratoDoPiaoSuperiorValue;
    }

    public void setLocalizacaoTravessaDoPratoDoPiaoSuperiorValue(String localizacaoTravessaDoPratoDoPiaoSuperiorValue) {
        this.localizacaoTravessaDoPratoDoPiaoSuperiorValue = localizacaoTravessaDoPratoDoPiaoSuperiorValue;
    }

    public int getLocalizacaoTravessaDoPratoDoPiaoSuperiorIndex() {
        return localizacaoTravessaDoPratoDoPiaoSuperiorIndex;
    }

    public String getCondicaoLongarinaLateralValue() {
        return condicaoLongarinaLateralValue;
    }

    public void setCondicaoLongarinaLateralValue(String condicaoLongarinaLateralValue) {
        this.condicaoLongarinaLateralValue = condicaoLongarinaLateralValue;
    }

    public int getCondicaoLongarinaLateralIndex() {
        return condicaoLongarinaLateralIndex;
    }

    public int getLocalizacaoLongarinaLateralIndex() {
        return localizacaoLongarinaLateralIndex;
    }

    public String getLocalizacaoLongarinaLateralValue() {
        return localizacaoLongarinaLateralValue;
    }

    public void setLocalizacaoLongarinaLateralValue(String localizacaoLongarinaLateralValue) {
        this.localizacaoLongarinaLateralValue = localizacaoLongarinaLateralValue;
    }

    public String getQualOTipoDoEngateValue() {
        return qualOTipoDoEngateValue;
    }

    public void setQualOTipoDoEngateValue(String qualOTipoDoEngateValue) {
        this.qualOTipoDoEngateValue = qualOTipoDoEngateValue;
    }

    public int getQualOTipoDoEngateIndex() {
        return qualOTipoDoEngateIndex;
    }

    public String getAlturaEngateA() {
        return alturaEngateA;
    }

    public String getAlturaEngateB() {
        return alturaEngateB;
    }

    public String getEngateEstaNiveladoValue() {
        return engateEstaNiveladoValue;
    }

    public void setEngateEstaNiveladoValue(String engateEstaNiveladoValue) {
        this.engateEstaNiveladoValue = engateEstaNiveladoValue;
    }

    public int getEngateEstaNiveladoIndex() {
        return engateEstaNiveladoIndex;
    }

    public String getExisteMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaValue() {
        return existeMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaValue;
    }

    public void setExisteMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaValue(String existeMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaValue) {
        this.existeMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaValue = existeMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaValue;
    }

    public int getExisteMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaIndex() {
        return existeMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaIndex;
    }

    public String getQualOModeloValue() {
        return qualOModeloValue;
    }

    public void setQualOModeloValue(String qualOModeloValue) {
        this.qualOModeloValue = qualOModeloValue;
    }

    public int getQualOModeloIndex() {
        return qualOModeloIndex;
    }

    public boolean isExisteFolgaNoACT() {
        return existeFolgaNoACT;
    }

    public long getQualAMedidaDaFolga() {
        return qualAMedidaDaFolga;
    }

    public String getEngateComponentesACTEngateValue() {
        return engateComponentesACTEngateValue;
    }

    public void setEngateComponentesACTEngateValue(String engateComponentesACTEngateValue) {
        this.engateComponentesACTEngateValue = engateComponentesACTEngateValue;
    }

    public int getEngateComponentesACTEngateIndex() {
        return engateComponentesACTEngateIndex;
    }

    public String getChavetaEPinoTValue() {
        return chavetaEPinoTValue;
    }

    public void setChavetaEPinoTValue(String chavetaEPinoTValue) {
        this.chavetaEPinoTValue = chavetaEPinoTValue;
    }

    public int getChavetaEPinoTIndex() {
        return chavetaEPinoTIndex;
    }

    public String getMandibulaEPinoValue() {
        return mandibulaEPinoValue;
    }

    public void setMandibulaEPinoValue(String mandibulaEPinoValue) {
        this.mandibulaEPinoValue = mandibulaEPinoValue;
    }

    public int getMandibulaEPinoIndex() {
        return mandibulaEPinoIndex;
    }

    public String getPinoY47ETravaValue() {
        return pinoY47ETravaValue;
    }

    public void setPinoY47ETravaValue(String pinoY47ETravaValue) {
        this.pinoY47ETravaValue = pinoY47ETravaValue;
    }

    public int getPinoY47ETravaIndex() {
        return pinoY47ETravaIndex;
    }

    public String getParaPecasFraturadasEspecificarTrincaAntigaOuRecente() {
        return paraPecasFraturadasEspecificarTrincaAntigaOuRecente;
    }

    public boolean isLocalDoPODTemMarcasDescarrilamentoAnterior() {
        return localDoPODTemMarcasDescarrilamentoAnterior;
    }

    public boolean isLocalDoPODTemHistoricoDeAcidentes06Meses() {
        return localDoPODTemHistoricoDeAcidentes06Meses;
    }

    public boolean isHaMarcasDePecaDoVagaoArrastandoNosDormentesAntesDoPOD() {
        return haMarcasDePecaDoVagaoArrastandoNosDormentesAntesDoPOD;
    }

    public String getQualPeca() {
        return qualPeca;
    }
}
