package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.*;

public class SobreOLocal {
    Faker faker;
    private int estadoIndex;
    private String estadoValue;
    private int municipioIndex;
    private String municipioValue;
    private String horarioDoAcionamento;
    private String horarioDoAtendimento;
    private String observacoesUnidadesDeAtendimentoPresentesNoLocal;
    private boolean policiaCivil;
    private boolean corpoDeBombeiros;
    private boolean policiaMilitar;
    private boolean samu;
    private boolean guardaMunicipal;
    private boolean outros;
    private String observacoesAtivosCompanhia;
    private String tipoPerimetro;
    private String localAcidente;
    private boolean passagemClandestina;
    private String clandestinaNomeCampo;
    private boolean placaPare;
    private boolean placaPareEscute;
    private boolean rocada;
    private int cruzSantoAndreIndex;
    private String cruzSantoAndreValue;
    private boolean sinalizacaoHorizontal;
    private boolean pavimentada;
    private boolean estavaOperacional;
    private String observacoesLocalAcidente;
    private String motivoVisibilidade;
    private String nomeDaPnOuPnp;
    private boolean visibilidadeBoaOuRuim;
    private boolean possuiSinalizacao;
    private boolean sinalizacaoAtiva;
    private boolean pnOuPnp;
    private String descricaoImagem;
    private String distanciaPnOuPNP;

    public SobreOLocal() {
        faker = new Faker(new Locale("pt-BR"));
        estadoIndex = getRandom(4);
        municipioIndex = getRandom(10);
        horarioDoAcionamento = getTimeNow();
        horarioDoAtendimento = getTimeNow();
        policiaCivil = faker.bool().bool();
        corpoDeBombeiros = faker.bool().bool();
        policiaMilitar = faker.bool().bool();
        samu = faker.bool().bool();
        guardaMunicipal = faker.bool().bool();
        outros = faker.bool().bool();
        observacoesUnidadesDeAtendimentoPresentesNoLocal = getRandomCharacters(2000);
        observacoesAtivosCompanhia = getRandomCharacters(2000);
        tipoPerimetro = faker.options().option("Urbano", "Rural");
        localAcidente = faker.options().option("PN", "PNP", "Trecho");
        passagemClandestina = faker.bool().bool();
        clandestinaNomeCampo = getRandomCharacters(30);
        placaPare = faker.bool().bool();
        placaPareEscute = faker.bool().bool();
        rocada = faker.bool().bool();
        cruzSantoAndreIndex = getRandom(2);
        sinalizacaoHorizontal = faker.bool().bool();
        pavimentada = faker.bool().bool();
        estavaOperacional = faker.bool().bool();
        observacoesLocalAcidente = getRandomCharacters(2000);
        motivoVisibilidade = getRandomCharacters(2000);
        nomeDaPnOuPnp = getRandomCharacters(512);
        visibilidadeBoaOuRuim = faker.bool().bool();
        possuiSinalizacao = faker.bool().bool();
        sinalizacaoAtiva = faker.bool().bool();
        pnOuPnp = faker.bool().bool();
        descricaoImagem = getRandomCharacters(2000);
        distanciaPnOuPNP = String.valueOf(getRandom(null));
    }

    public boolean isRocada() {
        return rocada;
    }

    public boolean isPossuiSinalizacao() {
        return possuiSinalizacao;
    }

    public String getNomeDaPnOuPnp() {
        return nomeDaPnOuPnp;
    }

    public String getDescricaoImagem() {
        return descricaoImagem;
    }

    public String getDistanciaPnOuPNP() {
        return distanciaPnOuPNP;
    }

    public boolean isSinalizacaoAtiva() {
        return sinalizacaoAtiva;
    }

    public boolean isPnOuPnp() {
        return pnOuPnp;
    }

    public boolean isVisibilidadeBoaOuRuim() {
        return visibilidadeBoaOuRuim;
    }

    public String getMotivoVisibilidade() {
        return motivoVisibilidade;
    }

    public String observacoesLocalAcidente() {
        return observacoesLocalAcidente;
    }

    public boolean isEstavaOperacional() {
        return estavaOperacional;
    }

    public boolean isSinalizacaoHorizontal() {
        return sinalizacaoHorizontal;
    }

    public boolean isPavimentada() {
        return pavimentada;
    }

    public int getCruzSantoAndreIndex() {
        return cruzSantoAndreIndex;
    }

    public String getCruzSantoAndreValue() {
        return cruzSantoAndreValue;
    }

    public void setCruzSantoAndreValue(String cruzSantoAndreValue) {
        this.cruzSantoAndreValue = cruzSantoAndreValue;
    }

    public boolean isPlacaPareEscute() {
        return placaPareEscute;
    }

    public String clandestinaNomeCampo() {
        return clandestinaNomeCampo;
    }

    public boolean isPassagemClandestina() {
        return passagemClandestina;
    }

    public boolean isPlacaPare() {
        return placaPare;
    }

    public String getLocalAcidente() {
        return localAcidente;
    }

    public String getClandestinaNomeCampo() {
        return clandestinaNomeCampo;
    }

    public String getObservacoesAtivosCompanhia() {
        return observacoesAtivosCompanhia;
    }

    public String getTipoPerimetro() {
        return tipoPerimetro;
    }

    public int getEstadoIndex() {
        return estadoIndex;
    }

    public String getEstadoValue() {
        return estadoValue;
    }

    public void setEstadoValue(String estadoValue) {
        this.estadoValue = estadoValue;
    }

    public int getMunicipioIndex() {
        return municipioIndex;
    }

    public String getMunicipioValue() {
        return municipioValue;
    }

    public void setMunicipioValue(String municipioValue) {
        this.municipioValue = municipioValue;
    }

    public String getHorarioDoAcionamento() {
        return horarioDoAcionamento;
    }

    public String getHorarioDoAtendimento() {
        return horarioDoAtendimento;
    }

    public String getObservacoesUnidadesDeAtendimentoPresentesNoLocal() {
        return observacoesUnidadesDeAtendimentoPresentesNoLocal;
    }

    public boolean isPoliciaCivil() {
        return policiaCivil;
    }

    public boolean isCorpoDeBombeiros() {
        return corpoDeBombeiros;
    }

    public boolean isPoliciaMilitar() {
        return policiaMilitar;
    }

    public boolean isSamu() {
        return samu;
    }

    public boolean isGuardaMunicipal() {
        return guardaMunicipal;
    }

    public boolean isOutros() {
        return outros;
    }


}
