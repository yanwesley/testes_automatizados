package br.com.api.model.sindicancia.investigacao;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class SobreOAMV {

    Faker faker;
    private boolean acidenteEnvolvendoAMV;
    private int localDoAMVIndex;
    private String localDoAMVValue;
    private int iluminacaoDoLocalIndex;
    private String iluminacaoDoLocalValue;
    private int posicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVIndex;
    private String posicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVValue;
    private boolean todosOsEPIsEramAdequadosParaAOperacao;
    private int quemEstavaComandandoAManobraIndex;
    private String quemEstavaComandandoAManobraValue;
    private String qualFoiAUltimaOrdemRecebidaPeloMaquinistaAntesDoEvento;
    private int haviaCoberturaNaCaudaIndex;
    private String haviaCoberturaNaCaudaValue;
    private int frenteDaLocomotivaIndex;
    private String frenteDaLocomotivaValue;
    private boolean maquinistaVisualizouAPosicaoDaChaveAMV;
    private boolean manobradorVisualizouAPosicaoDaChaveAMV;

    public SobreOAMV() {
        faker = new Faker(new Locale("pt-BR"));
        acidenteEnvolvendoAMV = faker.bool().bool();
        localDoAMVIndex = faker.options().option(0, 1, 2, 3);
        iluminacaoDoLocalIndex = faker.options().option(0, 1, 2, 3, 4, 5);
        posicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVIndex = faker.options().option(0, 1, 2, 3);
        todosOsEPIsEramAdequadosParaAOperacao = faker.bool().bool();
        quemEstavaComandandoAManobraIndex = faker.options().option(0, 1, 2);
        qualFoiAUltimaOrdemRecebidaPeloMaquinistaAntesDoEvento = getRandomCharacters(2000);
        haviaCoberturaNaCaudaIndex = faker.options().option(0, 1, 2);
        frenteDaLocomotivaIndex = faker.options().option(0, 1, 2);
        maquinistaVisualizouAPosicaoDaChaveAMV = faker.bool().bool();
        manobradorVisualizouAPosicaoDaChaveAMV = faker.bool().bool();
    }

    public String getLocalDoAMVValue() {
        return localDoAMVValue;
    }

    public void setLocalDoAMVValue(String localDoAMVValue) {
        this.localDoAMVValue = localDoAMVValue;
    }

    public String getIluminacaoDoLocalValue() {
        return iluminacaoDoLocalValue;
    }

    public void setIluminacaoDoLocalValue(String iluminacaoDoLocalValue) {
        this.iluminacaoDoLocalValue = iluminacaoDoLocalValue;
    }

    public String getPosicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVValue() {
        return posicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVValue;
    }

    public void setPosicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVValue(String posicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVValue) {
        this.posicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVValue = posicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVValue;
    }

    public String getQuemEstavaComandandoAManobraValue() {
        return quemEstavaComandandoAManobraValue;
    }

    public void setQuemEstavaComandandoAManobraValue(String quemEstavaComandandoAManobraValue) {
        this.quemEstavaComandandoAManobraValue = quemEstavaComandandoAManobraValue;
    }

    public String getHaviaCoberturaNaCaudaValue() {
        return haviaCoberturaNaCaudaValue;
    }

    public void setHaviaCoberturaNaCaudaValue(String haviaCoberturaNaCaudaValue) {
        this.haviaCoberturaNaCaudaValue = haviaCoberturaNaCaudaValue;
    }

    public String getFrenteDaLocomotivaValue() {
        return frenteDaLocomotivaValue;
    }

    public void setFrenteDaLocomotivaValue(String frenteDaLocomotivaValue) {
        this.frenteDaLocomotivaValue = frenteDaLocomotivaValue;
    }

    public boolean isAcidenteEnvolvendoAMV() {
        return acidenteEnvolvendoAMV;
    }

    public int getLocalDoAMVIndex() {
        return localDoAMVIndex;
    }

    public int getIluminacaoDoLocalIndex() {
        return iluminacaoDoLocalIndex;
    }

    public int getPosicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVIndex() {
        return posicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVIndex;
    }

    public boolean isTodosOsEPIsEramAdequadosParaAOperacao() {
        return todosOsEPIsEramAdequadosParaAOperacao;
    }

    public int getQuemEstavaComandandoAManobraIndex() {
        return quemEstavaComandandoAManobraIndex;
    }

    public String getQualFoiAUltimaOrdemRecebidaPeloMaquinistaAntesDoEvento() {
        return qualFoiAUltimaOrdemRecebidaPeloMaquinistaAntesDoEvento;
    }

    public int getHaviaCoberturaNaCaudaIndex() {
        return haviaCoberturaNaCaudaIndex;
    }

    public int getFrenteDaLocomotivaIndex() {
        return frenteDaLocomotivaIndex;
    }

    public boolean isMaquinistaVisualizouAPosicaoDaChaveAMV() {
        return maquinistaVisualizouAPosicaoDaChaveAMV;
    }

    public boolean isManobradorVisualizouAPosicaoDaChaveAMV() {
        return manobradorVisualizouAPosicaoDaChaveAMV;
    }
}
