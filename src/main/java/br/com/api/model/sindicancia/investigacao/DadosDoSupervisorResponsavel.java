package br.com.api.model.sindicancia.investigacao;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class DadosDoSupervisorResponsavel {

    Faker faker;
    private boolean oSupervisorResponsavelEDaRumo;
    private String nomeDoSupervisor;
    private String documento;
    private String CS;

    private String testesAplicadosRelacionadosComOAmbienteMesAtual;
    private String testesAplicadosRelacionadosComOAmbienteMesM1;
    private String testesAplicadosRelacionadosComOAmbienteMesM2;

    private String testesInsatisfatoriosRelacionadosAoAcidenteMesAtual;
    private String testesInsatisfatoriosRelacionadosAoAcidenteMesM1;
    private String testesInsatisfatoriosRelacionadosAoAcidenteMesM2;

    public DadosDoSupervisorResponsavel() {
        faker = new Faker(new Locale("pt-BR"));
        oSupervisorResponsavelEDaRumo = faker.bool().bool();
        nomeDoSupervisor = "Raul Herculano Fagundes";
        documento = getRandomCharacters(30);

        testesAplicadosRelacionadosComOAmbienteMesAtual = String.valueOf(getRandom(null));
        testesAplicadosRelacionadosComOAmbienteMesM1 = String.valueOf(getRandom(null));
        testesAplicadosRelacionadosComOAmbienteMesM2 = String.valueOf(getRandom(null));

        testesInsatisfatoriosRelacionadosAoAcidenteMesAtual = String.valueOf(getRandom(null));
        testesInsatisfatoriosRelacionadosAoAcidenteMesM1 = String.valueOf(getRandom(null));
        testesInsatisfatoriosRelacionadosAoAcidenteMesM2 = String.valueOf(getRandom(null));

    }

    public boolean isoSupervisorResponsavelEDaRumo() {
        return oSupervisorResponsavelEDaRumo;
    }

    public String getNomeDoSupervisor() {
        return nomeDoSupervisor;
    }

    public void setNomeDoSupervisor(String nomeDoSupervisor) {
        this.nomeDoSupervisor = nomeDoSupervisor;
    }

    public String getDocumento() {
        return documento;
    }

    public String getCS() {
        return CS;
    }

    public void setCS(String CS) {
        this.CS = CS;
    }

    public String getTestesAplicadosRelacionadosComOAmbienteMesAtual() {
        return testesAplicadosRelacionadosComOAmbienteMesAtual;
    }

    public String getTestesAplicadosRelacionadosComOAmbienteMesM1() {
        return testesAplicadosRelacionadosComOAmbienteMesM1;
    }

    public String getTestesAplicadosRelacionadosComOAmbienteMesM2() {
        return testesAplicadosRelacionadosComOAmbienteMesM2;
    }

    public String getTestesInsatisfatoriosRelacionadosAoAcidenteMesAtual() {
        return testesInsatisfatoriosRelacionadosAoAcidenteMesAtual;
    }

    public String getTestesInsatisfatoriosRelacionadosAoAcidenteMesM1() {
        return testesInsatisfatoriosRelacionadosAoAcidenteMesM1;
    }

    public String getTestesInsatisfatoriosRelacionadosAoAcidenteMesM2() {
        return testesInsatisfatoriosRelacionadosAoAcidenteMesM2;
    }

}
