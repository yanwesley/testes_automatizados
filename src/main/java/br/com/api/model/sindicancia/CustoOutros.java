package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getDay;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class CustoOutros {
    private Faker faker;
    private String data;
    private long quantidade;
    private long valorUnitario;
    private String descricao;

    public CustoOutros() {
        faker = new Faker(new Locale("pt-BR"));
        descricao = getRandomCharacters(255);
        data = getDay();
        quantidade = faker.random().nextInt(0, 999999).longValue();
        valorUnitario = faker.random().nextInt(0, 999999).longValue();
    }

    public String getData() {
        return data;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public long getValorUnitario() {
        return valorUnitario;
    }

    public String getDescricao() {
        return descricao;
    }
}
