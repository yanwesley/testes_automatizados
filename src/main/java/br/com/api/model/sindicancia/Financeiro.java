package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;

public class Financeiro {
    Faker faker;
    private String numeroDaOrdemDeServico;
    private String numeroDaRC;
    private String numeroDePedidos;

    public Financeiro() {
        faker = new Faker(new Locale("pt-BR"));
        numeroDaOrdemDeServico = String.valueOf(getRandom(null));
        numeroDaRC = String.valueOf(getRandom(null));
        numeroDePedidos = String.valueOf(getRandom(null));
    }

    public String getNumeroDaOrdemDeServico() {
        return numeroDaOrdemDeServico;
    }

    public String getNumeroDaRC() {
        return numeroDaRC;
    }

    public String getNumeroDePedidos() {
        return numeroDePedidos;
    }

}
