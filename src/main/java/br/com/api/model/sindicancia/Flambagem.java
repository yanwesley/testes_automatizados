package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static br.com.api.GeradorDeMassa.getDay;
import static br.com.api.GeradorDeMassa.getRandom;

public class Flambagem {

    Faker faker;

    private boolean localDaFlambagemUmBoolean;
    private boolean localDaFlambagemDoisBoolean;

    private String localDaFlambagemUm;
    private String localDaFlambagemDois;
    private String localDaFlambagemTres;

    private int temperaturaEmCampo;
    private int comprimentoDaBarra;
    private int caminhamentoDaTR;

    private boolean historicoDaFlambagemReincidenciaBoolean;
    private String historicoDaFlambagemData;
    private String historicoDaFlambagemHorario;

    private boolean historicoDoAcidenteReincidenciaBoolean;
    private String historicoDoAcidenteData;
    private String historicoDoAcidenteHorario;

    private String solucaoDefinitivaUm;
    private String solucaoDefinitivaDois;
    private String solucaoDefinitivaTres;

    public Flambagem() {
        faker = new Faker(new Locale("pt-BR"));
        localDaFlambagemUmBoolean = faker.options().option(true, false);
        localDaFlambagemDoisBoolean = faker.options().option(true, false);
        historicoDaFlambagemReincidenciaBoolean = faker.options().option(true, false);
        historicoDoAcidenteReincidenciaBoolean = faker.options().option(true, false);
        temperaturaEmCampo = getRandom(5);
        comprimentoDaBarra = getRandom(5);
        caminhamentoDaTR = getRandom(5);
        historicoDaFlambagemData = getDay();
        historicoDaFlambagemHorario = LocalDateTime.now().minusMinutes(1).format(DateTimeFormatter.ofPattern("HH:mm"));
        historicoDoAcidenteData = getDay();
        historicoDoAcidenteHorario = LocalDateTime.now().minusMinutes(1).format(DateTimeFormatter.ofPattern("HH:mm"));
    }

    public boolean isLocalDaFlambagemUmBoolean() {
        return localDaFlambagemUmBoolean;
    }

    public void setLocalDaFlambagemUmBoolean(boolean localDaFlambagemUmBoolean) {
        this.localDaFlambagemUmBoolean = localDaFlambagemUmBoolean;
    }

    public boolean isLocalDaFlambagemDoisBoolean() {
        return localDaFlambagemDoisBoolean;
    }

    public void setLocalDaFlambagemDoisBoolean(boolean localDaFlambagemDoisBoolean) {
        this.localDaFlambagemDoisBoolean = localDaFlambagemDoisBoolean;
    }

    public String getLocalDaFlambagemUm() {
        return localDaFlambagemUm;
    }

    public void setLocalDaFlambagemUm(String localDaFlambagemUm) {
        this.localDaFlambagemUm = localDaFlambagemUm;
    }

    public String getLocalDaFlambagemDois() {
        return localDaFlambagemDois;
    }

    public void setLocalDaFlambagemDois(String localDaFlambagemDois) {
        this.localDaFlambagemDois = localDaFlambagemDois;
    }

    public String getLocalDaFlambagemTres() {
        return localDaFlambagemTres;
    }

    public void setLocalDaFlambagemTres(String localDaFlambagemTres) {
        this.localDaFlambagemTres = localDaFlambagemTres;
    }

    public int getTemperaturaEmCampo() {
        return temperaturaEmCampo;
    }

    public void setTemperaturaEmCampo(int temperaturaEmCampo) {
        this.temperaturaEmCampo = temperaturaEmCampo;
    }

    public int getComprimentoDaBarra() {
        return comprimentoDaBarra;
    }

    public void setComprimentoDaBarra(int comprimentoDaBarra) {
        this.comprimentoDaBarra = comprimentoDaBarra;
    }

    public int getCaminhamentoDaTR() {
        return caminhamentoDaTR;
    }

    public void setCaminhamentoDaTR(int caminhamentoDaTR) {
        this.caminhamentoDaTR = caminhamentoDaTR;
    }

    public boolean isHistoricoDaFlambagemReincidenciaBoolean() {
        return historicoDaFlambagemReincidenciaBoolean;
    }

    public void setHistoricoDaFlambagemReincidenciaBoolean(boolean historicoDaFlambagemReincidenciaBoolean) {
        this.historicoDaFlambagemReincidenciaBoolean = historicoDaFlambagemReincidenciaBoolean;
    }


    public String getHistoricoDaFlambagemData() {
        return historicoDaFlambagemData;
    }

    public void setHistoricoDaFlambagemData(String historicoDaFlambagemData) {
        this.historicoDaFlambagemData = historicoDaFlambagemData;
    }

    public String getHistoricoDaFlambagemHorario() {
        return historicoDaFlambagemHorario;
    }

    public void setHistoricoDaFlambagemHorario(String historicoDaFlambagemHorario) {
        this.historicoDaFlambagemHorario = historicoDaFlambagemHorario;
    }

    public boolean isHistoricoDoAcidenteReincidenciaBoolean() {
        return historicoDoAcidenteReincidenciaBoolean;
    }

    public void setHistoricoDoAcidenteReincidenciaBoolean(boolean historicoDoAcidenteReincidenciaBoolean) {
        this.historicoDoAcidenteReincidenciaBoolean = historicoDoAcidenteReincidenciaBoolean;
    }

    public String getHistoricoDoAcidenteData() {
        return historicoDoAcidenteData;
    }

    public void setHistoricoDoAcidenteData(String historicoDoAcidenteData) {
        this.historicoDoAcidenteData = historicoDoAcidenteData;
    }

    public String getHistoricoDoAcidenteHorario() {
        return historicoDoAcidenteHorario;
    }

    public void setHistoricoDoAcidenteHorario(String historicoDoAcidenteHorario) {
        this.historicoDoAcidenteHorario = historicoDoAcidenteHorario;
    }

    public String getSolucaoDefinitivaUm() {
        return solucaoDefinitivaUm;
    }

    public void setSolucaoDefinitivaUm(String solucaoDefinitivaUm) {
        this.solucaoDefinitivaUm = solucaoDefinitivaUm;
    }

    public String getSolucaoDefinitivaDois() {
        return solucaoDefinitivaDois;
    }

    public void setSolucaoDefinitivaDois(String solucaoDefinitivaDois) {
        this.solucaoDefinitivaDois = solucaoDefinitivaDois;
    }

    public String getSolucaoDefinitivaTres() {
        return solucaoDefinitivaTres;
    }

    public void setSolucaoDefinitivaTres(String solucaoDefinitivaTres) {
        this.solucaoDefinitivaTres = solucaoDefinitivaTres;
    }
}
