package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

public class CustoLocomotiva {
    final int LIMITE_QTD = 999;
    final int LIMITE_VALOR = 999999999;
    private Faker faker;
    private String qtdRodeiros;
    private String qtdTanque;
    private String qtdEngate;
    private String qtdEstrutura;
    private String qtdTruque;
    private String qtdCaixaDeGracha;
    private String qtdLimpaTrilho;
    private String qtdMaoDeObra;
    private String qtdOutros;
    private long valorRodeiros;
    private long valorTanque;
    private long valorEngate;
    private long valorEstrutura;
    private long valorTruque;
    private long valorCaixaDeGracha;
    private long valorLimpaTrilho;
    private long valorMaoDeObra;
    private long valorOutros;

    public CustoLocomotiva() {
        faker = new Faker(new Locale("pt-BR"));
        //qtd
        qtdRodeiros = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdTanque = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdEngate = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdEstrutura = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdTruque = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdCaixaDeGracha = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdLimpaTrilho = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdMaoDeObra = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdOutros = faker.random().nextInt(0, LIMITE_QTD).toString();
        //valores
        valorRodeiros = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorTanque = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorEngate = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorEstrutura = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorTruque = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorCaixaDeGracha = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorLimpaTrilho = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorMaoDeObra = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorOutros = faker.random().nextInt(0, LIMITE_VALOR).longValue();
    }

    public String getQtdRodeiros() {
        return qtdRodeiros;
    }

    public void setQtdRodeiros(String qtdRodeiros) {
        this.qtdRodeiros = qtdRodeiros;
    }

    public String getQtdTanque() {
        return qtdTanque;
    }

    public void setQtdTanque(String qtdTanque) {
        this.qtdTanque = qtdTanque;
    }

    public String getQtdEngate() {
        return qtdEngate;
    }

    public void setQtdEngate(String qtdEngate) {
        this.qtdEngate = qtdEngate;
    }

    public String getQtdEstrutura() {
        return qtdEstrutura;
    }

    public void setQtdEstrutura(String qtdEstrutura) {
        this.qtdEstrutura = qtdEstrutura;
    }

    public String getQtdTruque() {
        return qtdTruque;
    }

    public void setQtdTruque(String qtdTruque) {
        this.qtdTruque = qtdTruque;
    }

    public String getQtdCaixaDeGracha() {
        return qtdCaixaDeGracha;
    }

    public void setQtdCaixaDeGracha(String qtdCaixaDeGracha) {
        this.qtdCaixaDeGracha = qtdCaixaDeGracha;
    }

    public String getQtdLimpaTrilho() {
        return qtdLimpaTrilho;
    }

    public void setQtdLimpaTrilho(String qtdLimpaTrilho) {
        this.qtdLimpaTrilho = qtdLimpaTrilho;
    }

    public String getQtdMaoDeObra() {
        return qtdMaoDeObra;
    }

    public void setQtdMaoDeObra(String qtdMaoDeObra) {
        this.qtdMaoDeObra = qtdMaoDeObra;
    }

    public String getQtdOutros() {
        return qtdOutros;
    }

    public void setQtdOutros(String qtdOutros) {
        this.qtdOutros = qtdOutros;
    }

    public long getValorRodeiros() {
        return valorRodeiros;
    }

    public void setValorRodeiros(long valorRodeiros) {
        this.valorRodeiros = valorRodeiros;
    }

    public long getValorTanque() {
        return valorTanque;
    }

    public void setValorTanque(long valorTanque) {
        this.valorTanque = valorTanque;
    }

    public long getValorEngate() {
        return valorEngate;
    }

    public void setValorEngate(long valorEngate) {
        this.valorEngate = valorEngate;
    }

    public long getValorEstrutura() {
        return valorEstrutura;
    }

    public void setValorEstrutura(long valorEstrutura) {
        this.valorEstrutura = valorEstrutura;
    }

    public long getValorTruque() {
        return valorTruque;
    }

    public void setValorTruque(long valorTruque) {
        this.valorTruque = valorTruque;
    }

    public long getValorCaixaDeGracha() {
        return valorCaixaDeGracha;
    }

    public void setValorCaixaDeGracha(long valorCaixaDeGracha) {
        this.valorCaixaDeGracha = valorCaixaDeGracha;
    }

    public long getValorLimpaTrilho() {
        return valorLimpaTrilho;
    }

    public void setValorLimpaTrilho(long valorLimpaTrilho) {
        this.valorLimpaTrilho = valorLimpaTrilho;
    }

    public long getValorMaoDeObra() {
        return valorMaoDeObra;
    }

    public void setValorMaoDeObra(long valorMaoDeObra) {
        this.valorMaoDeObra = valorMaoDeObra;
    }

    public long getValorOutros() {
        return valorOutros;
    }

    public void setValorOutros(long valorOutros) {
        this.valorOutros = valorOutros;
    }

    public long getTotal() {
        return Integer.valueOf(getQtdRodeiros()) * getValorRodeiros() +
                Integer.valueOf(getQtdTruque()) * getValorTruque() +
                Integer.valueOf(getQtdEngate()) * getValorEngate() +
                Integer.valueOf(getQtdEstrutura()) * getValorEstrutura() +
                Integer.valueOf(getQtdLimpaTrilho()) * getValorLimpaTrilho() +
                Integer.valueOf(getQtdTanque()) * getValorTanque() +
                Integer.valueOf(getQtdCaixaDeGracha()) * getValorCaixaDeGracha() +
                Integer.valueOf(getQtdMaoDeObra()) * getValorMaoDeObra() +
                Integer.valueOf(getQtdOutros()) * getValorOutros();
    }
}
