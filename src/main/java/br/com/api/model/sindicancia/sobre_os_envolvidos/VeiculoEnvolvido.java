package br.com.api.model.sindicancia.sobre_os_envolvidos;

import br.com.api.model.sinistro.VeiculoDeTerceiro;
import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class VeiculoEnvolvido extends VeiculoDeTerceiro {
    private Faker faker;
    private String motorista;
    private String RG;
    private String CPF;
    private String endereco;
    private boolean danosAoVeiculo;
    private String descricaoDanosAoVeiculo;
    private boolean evadiuDoLocal;

    public VeiculoEnvolvido() {
        super();
        faker = new Faker(new Locale("pt-BR"));
        motorista = getRandomCharacters(512);
        RG = getRandomCharacters(12);
        CPF = "641.092.340-69";
        endereco = getRandomCharacters(512);
        danosAoVeiculo = faker.bool().bool();
        descricaoDanosAoVeiculo = getRandomCharacters(2000);
        evadiuDoLocal = faker.bool().bool();
    }

    public String getMotorista() {
        return motorista;
    }

    public String getRG() {
        return RG;
    }

    public String getCPF() {
        return CPF;
    }

    public String getEndereco() {
        return endereco;
    }

    public boolean isDanosAoVeiculo() {
        return danosAoVeiculo;
    }

    public String getDescricaoDanosAoVeiculo() {
        return descricaoDanosAoVeiculo;
    }

    public boolean isEvadiuDoLocal() {
        return evadiuDoLocal;
    }

}
