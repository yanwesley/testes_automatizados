package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.*;

public class AMV {
    private Faker faker;

    //DADOS GERAIS
    private int tipoBitolaIndex;
    private String tipoBitolaValue;
    private String equipamentoDadosGerais;
    private String supervisorDadosGerais;
    private String patioDadosGerais;
    //PRINCIPAL - COTAS DE SALVAGUARDA
    private String bitolaPontaAgulhaCota1PrincipalLarga;
    private String bitolaLivrePassagemPontaAgulhaCota2PrincipalLarga;
    private String bitolaPontaAgulhaCota1PrincipalMetrica;
    private String bitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica;
    private String bitolaProtecaoPontaJacareCota3Principal;
    private String pontaDoJacareAnteriorMetricaPrincipal;
    private String pontaDoJacareAnteriorLargaPrincipal;
    private String pontaDoJacarePosteriorMetricaPrincipal;
    private String pontaDoJacarePosteriorLargaPrincipal;
    private String bicoPosteriorDoJacareDuploMetricaPrincipal;
    private String bicoPosteriorDoJacareDuploLargaPrincipal;
    //PRINCIPAL - FUNCIONAMENTO
    private boolean pressaoAgulhaEstaOkPrincipal;
    private String porcentagemDosDormentesInserviveisPrincipal;
    private boolean condicaoDoLastroEstaOkPrincipal;
    private boolean fixacaoEstaOkPrincipal;
    //PRINCIPAL - SEGURANÇA
    private String sistemaDeSegurancaDoAMVPrincipal;
    private boolean existeExcessoDeMatoNoAMVPrincipal;
    private boolean frequenciaDoCheckListEstaOkPrincipal;
    private boolean bandeirolaEstaPintadaESinalizadaPrincipal;
    private boolean frequenciaDeInspecaoProgramadaEstaOkPrincipal;
    //PRINCIPAL - BARRA DE CONJUGAÇÃO
    private String quantidadeBarraDeConjugacaoPrincipal;
    private boolean barraConjugacaoSaoIsoladasPrincipal;
    //PRINCIPAL - AGULHAS
    private String detalheAgulhaEsquerdaPrincipal;
    private String comprimentoAgulhaEsquerdaPrincipal;
    private String detalheAgulhaDireitaPrincipal;
    private String comprimentoAgulhaDireitaPrincipal;

    //DESVIADA - COTAS DE SALVAGUARDA
    private String bitolaPontaAgulhaCota1DesviadaLarga;
    private String bitolaLivrePassagemPontaAgulhaCota2DesviadaLarga;
    private String bitolaPontaAgulhaCota1DesviadaMetrica;
    private String bitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica;
    private String bitolaProtecaoPontaJacareCota3Desviada;
    private String pontaDoJacareAnteriorMetricaDesviada;
    private String pontaDoJacareAnteriorLargaDesviada;
    private String pontaDoJacarePosteriorMetricaDesviada;
    private String pontaDoJacarePosteriorLargaDesviada;
    private String bicoPosteriorDoJacareDuploMetricaDesviada;
    private String bicoPosteriorDoJacareDuploLargaDesviada;
    //DESVIADA - FUNCIONAMENTO
    private boolean pressaoAgulhaEstaOkDesviada;
    private String porcentagemDosDormentesInserviveisDesviada;
    private boolean condicaoDoLastroEstaOkDesviada;
    private boolean fixacaoEstaOkDesviada;
    //DESVIADA - SEGURANÇA
    private String sistemaDeSegurancaDoAMVDesviada;
    private boolean existeExcessoDeMatoNoAMVDesviada;
    private boolean frequenciaDoCheckListEstaOkDesviada;
    private boolean bandeirolaEstaPintadaESinalizadaDesviada;
    private boolean frequenciaDeInspecaoProgramadaEstaOkDesviada;
    //TR ENCOSTO
    private boolean trEncostroEsquerdo;
    private boolean trEncostroDireito;
    //CHANFRO
    private boolean chanfroEsquerdo;
    private boolean chanfroDireito;
    //ABERTURA AMV
    private String horaAberturaDoAMV;
    //TRILHO
    private int tipoPerfilTrilhoIndex;
    private String tipoPerfilTrilhoValue;

    public AMV() {
        faker = new Faker(new Locale("pt-BR"));
        //DADOS GERAIS
        equipamentoDadosGerais = getRandomCharacters(512);
        supervisorDadosGerais = "João";
        patioDadosGerais = getRandomCharacters(32);
        //PRINCIPAL - COTAS DE SALVAGUARDA
        bitolaPontaAgulhaCota1PrincipalLarga = String.valueOf(getRandom(null));
        bitolaLivrePassagemPontaAgulhaCota2PrincipalLarga = String.valueOf(getRandom(null));
        bitolaPontaAgulhaCota1PrincipalMetrica = String.valueOf(getRandom(null));
        bitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica = String.valueOf(getRandom(null));
        bitolaProtecaoPontaJacareCota3Principal = String.valueOf(getRandom(null));
        pontaDoJacareAnteriorMetricaPrincipal = String.valueOf(getRandom(null));
        pontaDoJacareAnteriorLargaPrincipal = String.valueOf(getRandom(null));
        pontaDoJacarePosteriorMetricaPrincipal = String.valueOf(getRandom(null));
        pontaDoJacarePosteriorLargaPrincipal = String.valueOf(getRandom(null));
        bicoPosteriorDoJacareDuploMetricaPrincipal = String.valueOf(getRandom(null));
        bicoPosteriorDoJacareDuploLargaPrincipal = String.valueOf(getRandom(null));
        //PRINCIPAL - FUNCIONAMENTO
        pressaoAgulhaEstaOkPrincipal = faker.bool().bool();
        porcentagemDosDormentesInserviveisPrincipal = String.valueOf(getRandom(null));
        condicaoDoLastroEstaOkPrincipal = faker.bool().bool();
        fixacaoEstaOkPrincipal = faker.bool().bool();
        //PRINCIPAL - SEGURANÇA
        sistemaDeSegurancaDoAMVPrincipal = getRandomCharacters(512);
        existeExcessoDeMatoNoAMVPrincipal = faker.bool().bool();
        frequenciaDoCheckListEstaOkPrincipal = faker.bool().bool();
        bandeirolaEstaPintadaESinalizadaPrincipal = faker.bool().bool();
        frequenciaDeInspecaoProgramadaEstaOkPrincipal = faker.bool().bool();
        //PRINCIPAL - BARRA DE CONJUGAÇÃO
        quantidadeBarraDeConjugacaoPrincipal = String.valueOf(getRandom(null));
        barraConjugacaoSaoIsoladasPrincipal = faker.bool().bool();
        //PRINCIPAL - AGULHAS
        detalheAgulhaEsquerdaPrincipal = String.valueOf(getRandom(null));
        comprimentoAgulhaEsquerdaPrincipal = String.valueOf(getRandom(null));
        detalheAgulhaDireitaPrincipal = String.valueOf(getRandom(null));
        comprimentoAgulhaDireitaPrincipal = String.valueOf(getRandom(null));

        //DESVIADA - COTAS DE SALVAGUARDA
        bitolaPontaAgulhaCota1DesviadaLarga = String.valueOf(getRandom(null));
        bitolaLivrePassagemPontaAgulhaCota2DesviadaLarga = String.valueOf(getRandom(null));
        bitolaPontaAgulhaCota1DesviadaMetrica = String.valueOf(getRandom(null));
        bitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica = String.valueOf(getRandom(null));
        bitolaProtecaoPontaJacareCota3Desviada = String.valueOf(getRandom(null));
        pontaDoJacareAnteriorMetricaDesviada = String.valueOf(getRandom(null));
        pontaDoJacareAnteriorLargaDesviada = String.valueOf(getRandom(null));
        pontaDoJacarePosteriorMetricaDesviada = String.valueOf(getRandom(null));
        pontaDoJacarePosteriorLargaDesviada = String.valueOf(getRandom(null));
        bicoPosteriorDoJacareDuploMetricaDesviada = String.valueOf(getRandom(null));
        bicoPosteriorDoJacareDuploLargaDesviada = String.valueOf(getRandom(null));
        //DESVIADA - FUNCIONAMENTO
        pressaoAgulhaEstaOkDesviada = faker.bool().bool();
        porcentagemDosDormentesInserviveisDesviada = String.valueOf(getRandom(null));
        condicaoDoLastroEstaOkDesviada = faker.bool().bool();
        fixacaoEstaOkDesviada = faker.bool().bool();
        //DESVIADA - SEGURANÇA
        sistemaDeSegurancaDoAMVDesviada = getRandomCharacters(512);
        existeExcessoDeMatoNoAMVDesviada = faker.bool().bool();
        frequenciaDoCheckListEstaOkDesviada = faker.bool().bool();
        bandeirolaEstaPintadaESinalizadaDesviada = faker.bool().bool();
        frequenciaDeInspecaoProgramadaEstaOkDesviada = faker.bool().bool();
        //TR ENCOSTO
        trEncostroEsquerdo = faker.bool().bool();
        trEncostroDireito = faker.bool().bool();
        //CHANFRO
        chanfroEsquerdo = faker.bool().bool();
        chanfroDireito = faker.bool().bool();
        //ABERTURA AMV
        horaAberturaDoAMV = getTimeNow();
        //TRILHO
        tipoPerfilTrilhoIndex = getRandom(10);
    }

    public int getTipoBitolaIndex() {
        return tipoBitolaIndex;
    }

    public void setTipoBitolaIndex(int tipoBitolaIndex) {
        this.tipoBitolaIndex = tipoBitolaIndex;
    }

    public String getTipoBitolaValue() {
        return tipoBitolaValue;
    }

    public void setTipoBitolaValue(String tipoBitolaValue) {
        this.tipoBitolaValue = tipoBitolaValue;
    }

    public String getEquipamentoDadosGerais() {
        return equipamentoDadosGerais;
    }

    public void setEquipamentoDadosGerais(String equipamentoDadosGerais) {
        this.equipamentoDadosGerais = equipamentoDadosGerais;
    }

    public String getSupervisorDadosGerais() {
        return supervisorDadosGerais;
    }

    public void setSupervisorDadosGerais(String supervisorDadosGerais) {
        this.supervisorDadosGerais = supervisorDadosGerais;
    }

    public String getPatioDadosGerais() {
        return patioDadosGerais;
    }

    public void setPatioDadosGerais(String patioDadosGerais) {
        this.patioDadosGerais = patioDadosGerais;
    }

    public String getBitolaPontaAgulhaCota1PrincipalLarga() {
        return bitolaPontaAgulhaCota1PrincipalLarga;
    }

    public void setBitolaPontaAgulhaCota1PrincipalLarga(String bitolaPontaAgulhaCota1PrincipalLarga) {
        this.bitolaPontaAgulhaCota1PrincipalLarga = bitolaPontaAgulhaCota1PrincipalLarga;
    }

    public String getBitolaLivrePassagemPontaAgulhaCota2PrincipalLarga() {
        return bitolaLivrePassagemPontaAgulhaCota2PrincipalLarga;
    }

    public void setBitolaLivrePassagemPontaAgulhaCota2PrincipalLarga(String bitolaLivrePassagemPontaAgulhaCota2PrincipalLarga) {
        this.bitolaLivrePassagemPontaAgulhaCota2PrincipalLarga = bitolaLivrePassagemPontaAgulhaCota2PrincipalLarga;
    }

    public String getBitolaPontaAgulhaCota1PrincipalMetrica() {
        return bitolaPontaAgulhaCota1PrincipalMetrica;
    }

    public void setBitolaPontaAgulhaCota1PrincipalMetrica(String bitolaPontaAgulhaCota1PrincipalMetrica) {
        this.bitolaPontaAgulhaCota1PrincipalMetrica = bitolaPontaAgulhaCota1PrincipalMetrica;
    }

    public String getBitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica() {
        return bitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica;
    }

    public void setBitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica(String bitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica) {
        this.bitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica = bitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica;
    }

    public String getBitolaProtecaoPontaJacareCota3Principal() {
        return bitolaProtecaoPontaJacareCota3Principal;
    }

    public void setBitolaProtecaoPontaJacareCota3Principal(String bitolaProtecaoPontaJacareCota3Principal) {
        this.bitolaProtecaoPontaJacareCota3Principal = bitolaProtecaoPontaJacareCota3Principal;
    }

    public String getPontaDoJacareAnteriorMetricaPrincipal() {
        return pontaDoJacareAnteriorMetricaPrincipal;
    }

    public void setPontaDoJacareAnteriorMetricaPrincipal(String pontaDoJacareAnteriorMetricaPrincipal) {
        this.pontaDoJacareAnteriorMetricaPrincipal = pontaDoJacareAnteriorMetricaPrincipal;
    }

    public String getPontaDoJacareAnteriorLargaPrincipal() {
        return pontaDoJacareAnteriorLargaPrincipal;
    }

    public void setPontaDoJacareAnteriorLargaPrincipal(String pontaDoJacareAnteriorLargaPrincipal) {
        this.pontaDoJacareAnteriorLargaPrincipal = pontaDoJacareAnteriorLargaPrincipal;
    }

    public String getPontaDoJacarePosteriorMetricaPrincipal() {
        return pontaDoJacarePosteriorMetricaPrincipal;
    }

    public void setPontaDoJacarePosteriorMetricaPrincipal(String pontaDoJacarePosteriorMetricaPrincipal) {
        this.pontaDoJacarePosteriorMetricaPrincipal = pontaDoJacarePosteriorMetricaPrincipal;
    }

    public String getPontaDoJacarePosteriorLargaPrincipal() {
        return pontaDoJacarePosteriorLargaPrincipal;
    }

    public void setPontaDoJacarePosteriorLargaPrincipal(String pontaDoJacarePosteriorLargaPrincipal) {
        this.pontaDoJacarePosteriorLargaPrincipal = pontaDoJacarePosteriorLargaPrincipal;
    }

    public String getBicoPosteriorDoJacareDuploMetricaPrincipal() {
        return bicoPosteriorDoJacareDuploMetricaPrincipal;
    }

    public void setBicoPosteriorDoJacareDuploMetricaPrincipal(String bicoPosteriorDoJacareDuploMetricaPrincipal) {
        this.bicoPosteriorDoJacareDuploMetricaPrincipal = bicoPosteriorDoJacareDuploMetricaPrincipal;
    }

    public String getBicoPosteriorDoJacareDuploLargaPrincipal() {
        return bicoPosteriorDoJacareDuploLargaPrincipal;
    }

    public void setBicoPosteriorDoJacareDuploLargaPrincipal(String bicoPosteriorDoJacareDuploLargaPrincipal) {
        this.bicoPosteriorDoJacareDuploLargaPrincipal = bicoPosteriorDoJacareDuploLargaPrincipal;
    }

    public boolean isPressaoAgulhaEstaOkPrincipal() {
        return pressaoAgulhaEstaOkPrincipal;
    }

    public void setPressaoAgulhaEstaOkPrincipal(boolean pressaoAgulhaEstaOkPrincipal) {
        this.pressaoAgulhaEstaOkPrincipal = pressaoAgulhaEstaOkPrincipal;
    }

    public String getPorcentagemDosDormentesInserviveisPrincipal() {
        return porcentagemDosDormentesInserviveisPrincipal;
    }

    public void setPorcentagemDosDormentesInserviveisPrincipal(String porcentagemDosDormentesInserviveisPrincipal) {
        this.porcentagemDosDormentesInserviveisPrincipal = porcentagemDosDormentesInserviveisPrincipal;
    }

    public boolean isCondicaoDoLastroEstaOkPrincipal() {
        return condicaoDoLastroEstaOkPrincipal;
    }

    public void setCondicaoDoLastroEstaOkPrincipal(boolean condicaoDoLastroEstaOkPrincipal) {
        this.condicaoDoLastroEstaOkPrincipal = condicaoDoLastroEstaOkPrincipal;
    }

    public boolean isFixacaoEstaOkPrincipal() {
        return fixacaoEstaOkPrincipal;
    }

    public void setFixacaoEstaOkPrincipal(boolean fixacaoEstaOkPrincipal) {
        this.fixacaoEstaOkPrincipal = fixacaoEstaOkPrincipal;
    }

    public String getSistemaDeSegurancaDoAMVPrincipal() {
        return sistemaDeSegurancaDoAMVPrincipal;
    }

    public void setSistemaDeSegurancaDoAMVPrincipal(String sistemaDeSegurancaDoAMVPrincipal) {
        this.sistemaDeSegurancaDoAMVPrincipal = sistemaDeSegurancaDoAMVPrincipal;
    }

    public boolean isExisteExcessoDeMatoNoAMVPrincipal() {
        return existeExcessoDeMatoNoAMVPrincipal;
    }

    public void setExisteExcessoDeMatoNoAMVPrincipal(boolean existeExcessoDeMatoNoAMVPrincipal) {
        this.existeExcessoDeMatoNoAMVPrincipal = existeExcessoDeMatoNoAMVPrincipal;
    }

    public boolean isFrequenciaDoCheckListEstaOkPrincipal() {
        return frequenciaDoCheckListEstaOkPrincipal;
    }

    public void setFrequenciaDoCheckListEstaOkPrincipal(boolean frequenciaDoCheckListEstaOkPrincipal) {
        this.frequenciaDoCheckListEstaOkPrincipal = frequenciaDoCheckListEstaOkPrincipal;
    }

    public boolean isBandeirolaEstaPintadaESinalizadaPrincipal() {
        return bandeirolaEstaPintadaESinalizadaPrincipal;
    }

    public void setBandeirolaEstaPintadaESinalizadaPrincipal(boolean bandeirolaEstaPintadaESinalizadaPrincipal) {
        this.bandeirolaEstaPintadaESinalizadaPrincipal = bandeirolaEstaPintadaESinalizadaPrincipal;
    }

    public boolean isFrequenciaDeInspecaoProgramadaEstaOkPrincipal() {
        return frequenciaDeInspecaoProgramadaEstaOkPrincipal;
    }

    public void setFrequenciaDeInspecaoProgramadaEstaOkPrincipal(boolean frequenciaDeInspecaoProgramadaEstaOkPrincipal) {
        this.frequenciaDeInspecaoProgramadaEstaOkPrincipal = frequenciaDeInspecaoProgramadaEstaOkPrincipal;
    }

    public String getQuantidadeBarraDeConjugacaoPrincipal() {
        return quantidadeBarraDeConjugacaoPrincipal;
    }

    public void setQuantidadeBarraDeConjugacaoPrincipal(String quantidadeBarraDeConjugacaoPrincipal) {
        this.quantidadeBarraDeConjugacaoPrincipal = quantidadeBarraDeConjugacaoPrincipal;
    }

    public boolean isBarraConjugacaoSaoIsoladasPrincipal() {
        return barraConjugacaoSaoIsoladasPrincipal;
    }

    public void setBarraConjugacaoSaoIsoladasPrincipal(boolean barraConjugacaoSaoIsoladasPrincipal) {
        this.barraConjugacaoSaoIsoladasPrincipal = barraConjugacaoSaoIsoladasPrincipal;
    }

    public String getDetalheAgulhaEsquerdaPrincipal() {
        return detalheAgulhaEsquerdaPrincipal;
    }

    public void setDetalheAgulhaEsquerdaPrincipal(String detalheAgulhaEsquerdaPrincipal) {
        this.detalheAgulhaEsquerdaPrincipal = detalheAgulhaEsquerdaPrincipal;
    }

    public String getComprimentoAgulhaEsquerdaPrincipal() {
        return comprimentoAgulhaEsquerdaPrincipal;
    }

    public void setComprimentoAgulhaEsquerdaPrincipal(String comprimentoAgulhaEsquerdaPrincipal) {
        this.comprimentoAgulhaEsquerdaPrincipal = comprimentoAgulhaEsquerdaPrincipal;
    }

    public String getDetalheAgulhaDireitaPrincipal() {
        return detalheAgulhaDireitaPrincipal;
    }

    public void setDetalheAgulhaDireitaPrincipal(String detalheAgulhaDireitaPrincipal) {
        this.detalheAgulhaDireitaPrincipal = detalheAgulhaDireitaPrincipal;
    }

    public String getComprimentoAgulhaDireitaPrincipal() {
        return comprimentoAgulhaDireitaPrincipal;
    }

    public void setComprimentoAgulhaDireitaPrincipal(String comprimentoAgulhaDireitaPrincipal) {
        this.comprimentoAgulhaDireitaPrincipal = comprimentoAgulhaDireitaPrincipal;
    }

    public String getBitolaPontaAgulhaCota1DesviadaLarga() {
        return bitolaPontaAgulhaCota1DesviadaLarga;
    }

    public void setBitolaPontaAgulhaCota1DesviadaLarga(String bitolaPontaAgulhaCota1DesviadaLarga) {
        this.bitolaPontaAgulhaCota1DesviadaLarga = bitolaPontaAgulhaCota1DesviadaLarga;
    }

    public String getBitolaLivrePassagemPontaAgulhaCota2DesviadaLarga() {
        return bitolaLivrePassagemPontaAgulhaCota2DesviadaLarga;
    }

    public void setBitolaLivrePassagemPontaAgulhaCota2DesviadaLarga(String bitolaLivrePassagemPontaAgulhaCota2DesviadaLarga) {
        this.bitolaLivrePassagemPontaAgulhaCota2DesviadaLarga = bitolaLivrePassagemPontaAgulhaCota2DesviadaLarga;
    }

    public String getBitolaPontaAgulhaCota1DesviadaMetrica() {
        return bitolaPontaAgulhaCota1DesviadaMetrica;
    }

    public void setBitolaPontaAgulhaCota1DesviadaMetrica(String bitolaPontaAgulhaCota1DesviadaMetrica) {
        this.bitolaPontaAgulhaCota1DesviadaMetrica = bitolaPontaAgulhaCota1DesviadaMetrica;
    }

    public String getBitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica() {
        return bitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica;
    }

    public void setBitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica(String bitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica) {
        this.bitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica = bitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica;
    }

    public String getBitolaProtecaoPontaJacareCota3Desviada() {
        return bitolaProtecaoPontaJacareCota3Desviada;
    }

    public void setBitolaProtecaoPontaJacareCota3Desviada(String bitolaProtecaoPontaJacareCota3Desviada) {
        this.bitolaProtecaoPontaJacareCota3Desviada = bitolaProtecaoPontaJacareCota3Desviada;
    }

    public String getPontaDoJacareAnteriorMetricaDesviada() {
        return pontaDoJacareAnteriorMetricaDesviada;
    }

    public void setPontaDoJacareAnteriorMetricaDesviada(String pontaDoJacareAnteriorMetricaDesviada) {
        this.pontaDoJacareAnteriorMetricaDesviada = pontaDoJacareAnteriorMetricaDesviada;
    }

    public String getPontaDoJacareAnteriorLargaDesviada() {
        return pontaDoJacareAnteriorLargaDesviada;
    }

    public void setPontaDoJacareAnteriorLargaDesviada(String pontaDoJacareAnteriorLargaDesviada) {
        this.pontaDoJacareAnteriorLargaDesviada = pontaDoJacareAnteriorLargaDesviada;
    }

    public String getPontaDoJacarePosteriorMetricaDesviada() {
        return pontaDoJacarePosteriorMetricaDesviada;
    }

    public void setPontaDoJacarePosteriorMetricaDesviada(String pontaDoJacarePosteriorMetricaDesviada) {
        this.pontaDoJacarePosteriorMetricaDesviada = pontaDoJacarePosteriorMetricaDesviada;
    }

    public String getPontaDoJacarePosteriorLargaDesviada() {
        return pontaDoJacarePosteriorLargaDesviada;
    }

    public void setPontaDoJacarePosteriorLargaDesviada(String pontaDoJacarePosteriorLargaDesviada) {
        this.pontaDoJacarePosteriorLargaDesviada = pontaDoJacarePosteriorLargaDesviada;
    }

    public String getBicoPosteriorDoJacareDuploMetricaDesviada() {
        return bicoPosteriorDoJacareDuploMetricaDesviada;
    }

    public void setBicoPosteriorDoJacareDuploMetricaDesviada(String bicoPosteriorDoJacareDuploMetricaDesviada) {
        this.bicoPosteriorDoJacareDuploMetricaDesviada = bicoPosteriorDoJacareDuploMetricaDesviada;
    }

    public String getBicoPosteriorDoJacareDuploLargaDesviada() {
        return bicoPosteriorDoJacareDuploLargaDesviada;
    }

    public void setBicoPosteriorDoJacareDuploLargaDesviada(String bicoPosteriorDoJacareDuploLargaDesviada) {
        this.bicoPosteriorDoJacareDuploLargaDesviada = bicoPosteriorDoJacareDuploLargaDesviada;
    }

    public boolean isPressaoAgulhaEstaOkDesviada() {
        return pressaoAgulhaEstaOkDesviada;
    }

    public void setPressaoAgulhaEstaOkDesviada(boolean pressaoAgulhaEstaOkDesviada) {
        this.pressaoAgulhaEstaOkDesviada = pressaoAgulhaEstaOkDesviada;
    }

    public String getPorcentagemDosDormentesInserviveisDesviada() {
        return porcentagemDosDormentesInserviveisDesviada;
    }

    public void setPorcentagemDosDormentesInserviveisDesviada(String porcentagemDosDormentesInserviveisDesviada) {
        this.porcentagemDosDormentesInserviveisDesviada = porcentagemDosDormentesInserviveisDesviada;
    }

    public boolean isCondicaoDoLastroEstaOkDesviada() {
        return condicaoDoLastroEstaOkDesviada;
    }

    public void setCondicaoDoLastroEstaOkDesviada(boolean condicaoDoLastroEstaOkDesviada) {
        this.condicaoDoLastroEstaOkDesviada = condicaoDoLastroEstaOkDesviada;
    }

    public boolean isFixacaoEstaOkDesviada() {
        return fixacaoEstaOkDesviada;
    }

    public void setFixacaoEstaOkDesviada(boolean fixacaoEstaOkDesviada) {
        this.fixacaoEstaOkDesviada = fixacaoEstaOkDesviada;
    }

    public String getSistemaDeSegurancaDoAMVDesviada() {
        return sistemaDeSegurancaDoAMVDesviada;
    }

    public void setSistemaDeSegurancaDoAMVDesviada(String sistemaDeSegurancaDoAMVDesviada) {
        this.sistemaDeSegurancaDoAMVDesviada = sistemaDeSegurancaDoAMVDesviada;
    }

    public boolean isExisteExcessoDeMatoNoAMVDesviada() {
        return existeExcessoDeMatoNoAMVDesviada;
    }

    public void setExisteExcessoDeMatoNoAMVDesviada(boolean existeExcessoDeMatoNoAMVDesviada) {
        this.existeExcessoDeMatoNoAMVDesviada = existeExcessoDeMatoNoAMVDesviada;
    }

    public boolean isFrequenciaDoCheckListEstaOkDesviada() {
        return frequenciaDoCheckListEstaOkDesviada;
    }

    public void setFrequenciaDoCheckListEstaOkDesviada(boolean frequenciaDoCheckListEstaOkDesviada) {
        this.frequenciaDoCheckListEstaOkDesviada = frequenciaDoCheckListEstaOkDesviada;
    }

    public boolean isBandeirolaEstaPintadaESinalizadaDesviada() {
        return bandeirolaEstaPintadaESinalizadaDesviada;
    }

    public void setBandeirolaEstaPintadaESinalizadaDesviada(boolean bandeirolaEstaPintadaESinalizadaDesviada) {
        this.bandeirolaEstaPintadaESinalizadaDesviada = bandeirolaEstaPintadaESinalizadaDesviada;
    }

    public boolean isFrequenciaDeInspecaoProgramadaEstaOkDesviada() {
        return frequenciaDeInspecaoProgramadaEstaOkDesviada;
    }

    public void setFrequenciaDeInspecaoProgramadaEstaOkDesviada(boolean frequenciaDeInspecaoProgramadaEstaOkDesviada) {
        this.frequenciaDeInspecaoProgramadaEstaOkDesviada = frequenciaDeInspecaoProgramadaEstaOkDesviada;
    }

    public boolean isTrEncostroEsquerdo() {
        return trEncostroEsquerdo;
    }

    public void setTrEncostroEsquerdo(boolean trEncostroEsquerdo) {
        this.trEncostroEsquerdo = trEncostroEsquerdo;
    }

    public boolean isTrEncostroDireito() {
        return trEncostroDireito;
    }

    public void setTrEncostroDireito(boolean trEncostroDireito) {
        this.trEncostroDireito = trEncostroDireito;
    }

    public boolean isChanfroEsquerdo() {
        return chanfroEsquerdo;
    }

    public void setChanfroEsquerdo(boolean chanfroEsquerdo) {
        this.chanfroEsquerdo = chanfroEsquerdo;
    }

    public boolean isChanfroDireito() {
        return chanfroDireito;
    }

    public void setChanfroDireito(boolean chanfroDireito) {
        this.chanfroDireito = chanfroDireito;
    }

    public String getHoraAberturaDoAMV() {
        return horaAberturaDoAMV;
    }

    public void setHoraAberturaDoAMV(String horaAberturaDoAMV) {
        this.horaAberturaDoAMV = horaAberturaDoAMV;
    }

    public int getTipoPerfilTrilhoIndex() {
        return tipoPerfilTrilhoIndex;
    }

    public void setTipoPerfilTrilhoIndex(int tipoPerfilTrilhoIndex) {
        this.tipoPerfilTrilhoIndex = tipoPerfilTrilhoIndex;
    }

    public String getTipoPerfilTrilhoValue() {
        return tipoPerfilTrilhoValue;
    }

    public void setTipoPerfilTrilhoValue(String tipoPerfilTrilhoValue) {
        this.tipoPerfilTrilhoValue = tipoPerfilTrilhoValue;
    }
}
