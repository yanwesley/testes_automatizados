package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class AnaliseDeCausa {
    Faker faker;
    private String primeiroPorque;
    private String segundoPorque;
    private String terceiroPorque;
    private String quartoPorque;
    private String quintoPorque;
    private String fatosConstatados;
    private String fatoOcorrido;
    private String causaIdentificada;

    public AnaliseDeCausa() {
        faker = new Faker(new Locale("pt-BR"));
        primeiroPorque = getRandomCharacters(2000);
        segundoPorque = getRandomCharacters(2000);
        terceiroPorque = getRandomCharacters(2000);
        quartoPorque = getRandomCharacters(2000);
        quintoPorque = getRandomCharacters(2000);
        fatosConstatados = getRandomCharacters(2000);
        fatoOcorrido = getRandomCharacters(15);
        causaIdentificada = getRandomCharacters(2000);
    }

    public String getPrimeiroPorque() {
        return primeiroPorque;
    }

    public String getSegundoPorque() {
        return segundoPorque;
    }

    public String getTerceiroPorque() {
        return terceiroPorque;
    }

    public String getQuartoPorque() {
        return quartoPorque;
    }

    public String getQuintoPorque() {
        return quintoPorque;
    }

    public String getFatosConstatados() {
        return fatosConstatados;
    }

    public String getFatoOcorrido() {
        return fatoOcorrido;
    }

    public String getCausaIdentificada() {
        return causaIdentificada;
    }
}

