package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class AnularSindicancia {
    Faker faker;
    private String justificativa;

    public AnularSindicancia() {
        justificativa = getRandomCharacters(2000);
    }

    public String getJustificativa() {
        return justificativa;
    }
}


