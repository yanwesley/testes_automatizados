package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class DadosVia {
    private Faker faker;
    private String justificativaInserida;
    private int configuracaoIndex;
    private String configuracaoValue;
    private int bitolaIndex;
    private String bitolaValue;
    private int extensaoPercorridaDescarriladoMetros;
    private int vagaoVazioOuVeiculoDeVia;
    private int vagaoCarregadoOuLocomotiva;
    private boolean possuiPlanimetria;
    private boolean curva;
    private int raioDaCurvaMetros;
    private int pd;
    private int pod;
    private int distancia;
    private int tipoDeMarcaNoTrilhoIndex;
    private String tipoDeMarcaNoTrilhoValue;
    private int topologiaDaMarcaIndex;
    private String topologiaDaMarcaValue;
    private int altimetriaIndex;
    private String altimetriaValue;
    private int rampa;
    private int restricaoIndex;
    private String restricaoValue;
    private int velocidadeDaRestricao;
    private int planimetriaIndex;
    private String planimetriaValue;
    private int lastroIndex;
    private String lastroValue;
    private int alturaLastro;
    private int fixacaoIndex;
    private String fixacaoValue;
    private int tirefondIndex;
    private String tirefondValue;
    private int dormentacaoIndex;
    private String dormentacaoValue;
    private int totalDormentes;
    private int servivelDormentes;
    private int inservivelDormentes;
    private boolean havidaDDCNoLocal;
    private boolean oAtivoDescarriladoPassouPeloDDC;
    private boolean oDDCEstavaOperacional;
    private boolean oDDCAtuou;
    private String comentarioDDC;
    private boolean houveQuedaDePedras_Barreira;
    private boolean haviaDQBNoLocal;
    private boolean oDQBEstavaOperacional;
    private boolean oDQBAtuou;
    private String comentarioDQB;
    private String VMATrecho;
    private String equipamento;

    public DadosVia() {
        faker = new Faker(new Locale("pt-BR"));
        configuracaoIndex = faker.options().option(0);
        bitolaIndex = faker.options().option(0, 1, 2);
        extensaoPercorridaDescarriladoMetros = getRandom(null);
        vagaoVazioOuVeiculoDeVia = getRandom(null);
        vagaoCarregadoOuLocomotiva = getRandom(null);
        possuiPlanimetria = faker.options().option(true, false);
        curva = faker.options().option(true, false);
        raioDaCurvaMetros = getRandom(null);
        pd = getRandom(null);
        pod = getRandom(null);
        distancia = pd - pod;
        tipoDeMarcaNoTrilhoIndex = getRandom(5);
        topologiaDaMarcaIndex = getRandom(2);
        altimetriaIndex = getRandom(2);
        rampa = getRandom(null);
        restricaoIndex = getRandom(2);
        velocidadeDaRestricao = getRandom(null);
        planimetriaIndex = getRandom(1);
        lastroIndex = getRandom(3);
        alturaLastro = getRandom(null);
        fixacaoIndex = getRandom(1);
        tirefondIndex = getRandom(1);
        dormentacaoIndex = getRandom(2);
        totalDormentes = getRandom(null);
        servivelDormentes = getRandom(null);
        inservivelDormentes = getRandom(null);
        havidaDDCNoLocal = faker.options().option(true, false);
        oAtivoDescarriladoPassouPeloDDC = faker.options().option(true, false);
        oDDCEstavaOperacional = faker.options().option(true, false);
        oDDCAtuou = faker.options().option(true, false);
        comentarioDDC = getRandomCharacters(4000);
        houveQuedaDePedras_Barreira = faker.options().option(true, false);
        haviaDQBNoLocal = faker.options().option(true, false);
        oDQBEstavaOperacional = faker.options().option(true, false);
        oDQBAtuou = faker.options().option(true, false);
        comentarioDQB = getRandomCharacters(4000);
        justificativaInserida = getRandomCharacters(2000);
        VMATrecho = String.valueOf(getRandom(99999999));
        equipamento = getRandomCharacters(512);
    }

    public String getJustificativaInserida() {
        return justificativaInserida;
    }

    public int getConfiguracaoIndex() {
        return configuracaoIndex;
    }

    public String getConfiguracaoValue() {
        return configuracaoValue;
    }

    public void setConfiguracaoValue(String configuracaoValue) {
        this.configuracaoValue = configuracaoValue;
    }

    public int getBitolaIndex() {
        return bitolaIndex;
    }

    public String getBitolaValue() {
        return bitolaValue;
    }

    public void setBitolaValue(String bitolaValue) {
        this.bitolaValue = bitolaValue;
    }

    public int getExtensaoPercorridaDescarriladoMetros() {
        return extensaoPercorridaDescarriladoMetros;
    }

    public int getVagaoVazioOuVeiculoDeVia() {
        return vagaoVazioOuVeiculoDeVia;
    }

    public int getVagaoCarregadoOuLocomotiva() {
        return vagaoCarregadoOuLocomotiva;
    }

    public boolean isPossuiPlanimetria() {
        return possuiPlanimetria;
    }

    public boolean isCurva() {
        return curva;
    }

    public int getRaioDaCurvaMetros() {
        return raioDaCurvaMetros;
    }

    public int getPd() {
        return pd;
    }

    public int getPod() {
        return pod;
    }

    public int getDistancia() {
        return distancia;
    }

    public int getTipoDeMarcaNoTrilhoIndex() {
        return tipoDeMarcaNoTrilhoIndex;
    }

    public String getTipoDeMarcaNoTrilhoValue() {
        return tipoDeMarcaNoTrilhoValue;
    }

    public void setTipoDeMarcaNoTrilhoValue(String tipoDeMarcaNoTrilhoValue) {
        this.tipoDeMarcaNoTrilhoValue = tipoDeMarcaNoTrilhoValue;
    }

    public int getTopologiaDaMarcaIndex() {
        return topologiaDaMarcaIndex;
    }

    public String getTopologiaDaMarcaValue() {
        return topologiaDaMarcaValue;
    }

    public void setTopologiaDaMarcaValue(String topologiaDaMarcaValue) {
        this.topologiaDaMarcaValue = topologiaDaMarcaValue;
    }

    public int getAltimetriaIndex() {
        return altimetriaIndex;
    }

    public String getAltimetriaValue() {
        return altimetriaValue;
    }

    public void setAltimetriaValue(String altimetriaValue) {
        this.altimetriaValue = altimetriaValue;
    }

    public int getRampa() {
        return rampa;
    }

    public int getRestricaoIndex() {
        return restricaoIndex;
    }

    public String getRestricaoValue() {
        return restricaoValue;
    }

    public void setRestricaoValue(String restricaoValue) {
        this.restricaoValue = restricaoValue;
    }

    public int getVelocidadeDaRestricao() {
        return velocidadeDaRestricao;
    }

    public int getPlanimetriaIndex() {
        return planimetriaIndex;
    }

    public String getPlanimetriaValue() {
        return planimetriaValue;
    }

    public void setPlanimetriaValue(String planimetriaValue) {
        this.planimetriaValue = planimetriaValue;
    }

    public int getLastroIndex() {
        return lastroIndex;
    }

    public String getLastroValue() {
        return lastroValue;
    }

    public void setLastroValue(String lastroValue) {
        this.lastroValue = lastroValue;
    }

    public int getAlturaLastro() {
        return alturaLastro;
    }

    public int getFixacaoIndex() {
        return fixacaoIndex;
    }

    public String getFixacaoValue() {
        return fixacaoValue;
    }

    public void setFixacaoValue(String fixacaoValue) {
        this.fixacaoValue = fixacaoValue;
    }

    public int getTirefondIndex() {
        return tirefondIndex;
    }

    public String getTirefondValue() {
        return tirefondValue;
    }

    public void setTirefondValue(String tirefondValue) {
        this.tirefondValue = tirefondValue;
    }

    public int getDormentacaoIndex() {
        return dormentacaoIndex;
    }

    public String getDormentacaoValue() {
        return dormentacaoValue;
    }

    public void setDormentacaoValue(String dormentacaoValue) {
        this.dormentacaoValue = dormentacaoValue;
    }

    public int getTotalDormentes() {
        return totalDormentes;
    }

    public int getServivelDormentes() {
        return servivelDormentes;
    }

    public int getInservivelDormentes() {
        return inservivelDormentes;
    }

    public boolean isHavidaDDCNoLocal() {
        return havidaDDCNoLocal;
    }

    public boolean isoAtivoDescarriladoPassouPeloDDC() {
        return oAtivoDescarriladoPassouPeloDDC;
    }

    public boolean isoDDCEstavaOperacional() {
        return oDDCEstavaOperacional;
    }

    public boolean isoDDCAtuou() {
        return oDDCAtuou;
    }

    public String getComentarioDDC() {
        return comentarioDDC;
    }

    public boolean isHouveQuedaDePedras_Barreira() {
        return houveQuedaDePedras_Barreira;
    }

    public boolean isHaviaDQBNoLocal() {
        return haviaDQBNoLocal;
    }

    public boolean isoDQBEstavaOperacional() {
        return oDQBEstavaOperacional;
    }

    public boolean isoDQBAtuou() {
        return oDQBAtuou;
    }

    public String getComentarioDQB() {
        return comentarioDQB;
    }

    public String getVMATrecho() {
        return VMATrecho;
    }

    public void setVMATrecho(String VMATrecho) {
        this.VMATrecho = VMATrecho;
    }

    public String getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(String equipamento) {
        this.equipamento = equipamento;
    }
}
