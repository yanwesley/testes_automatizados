package br.com.api.model.sindicancia;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class CustoVeiculosVia {

    final int LIMITE_QTD = 999;
    final int LIMITE_VALOR = 999999999;
    private Faker faker;
    private String qtdRodeiros;
    private String qtdTruque;
    private String qtdEngate;
    private String qtdEstrutura;
    private String qtdSistemaDeFreio;
    private String qtdTanque;
    private String qtdCaixaDeGracha;
    private String qtdMaoDeObra;
    private String qtdOutros;
    private String descricao;
    private String qtdTremonha;
    private int quantidade;
    private int valoUnitario;
    private long valorRodeiros;
    private long valorTruque;
    private long valorEngate;
    private long valorEstrutura;
    private long valorSistemaDeFreio;
    private long valorTanque;
    private long valorCaixaDeGracha;
    private long valorMaoDeObra;
    private long valorOutros;
    private long valorTremonha;

    public CustoVeiculosVia() {

        faker = new Faker(new Locale("pt-BR"));
        //qtd
        qtdRodeiros = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdTruque = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdEngate = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdEstrutura = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdSistemaDeFreio = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdTanque = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdCaixaDeGracha = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdMaoDeObra = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdOutros = faker.random().nextInt(0, LIMITE_QTD).toString();
        qtdTremonha = faker.random().nextInt(0, LIMITE_QTD).toString();
        //valores
        valorRodeiros = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorTruque = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorEngate = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorEstrutura = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorSistemaDeFreio = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorTanque = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorCaixaDeGracha = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorMaoDeObra = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorOutros = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        valorTremonha = faker.random().nextInt(0, LIMITE_VALOR).longValue();
        descricao = getRandomCharacters(255);
        quantidade = getRandom(null);
        valoUnitario = getRandom(null);
    }

    public long getTotal() {
        return Integer.valueOf(getQtdRodeiros()) * getValorRodeiros() +
                Integer.valueOf(getQtdTruque()) * getValorTruque() +
                Integer.valueOf(getQtdEngate()) * getValorEngate() +
                Integer.valueOf(getQtdEstrutura()) * getValorEstrutura() +
                Integer.valueOf(getQtdSistemaDeFreio()) * getValorSistemaDeFreio() +
                Integer.valueOf(getQtdTanque()) * getValorTanque() +
                Integer.valueOf(getQtdCaixaDeGracha()) * getValorCaixaDeGracha() +
                Integer.valueOf(getQtdMaoDeObra()) * getValorMaoDeObra() +
                Integer.valueOf(getQtdOutros()) * getValorOutros();
    }

    public String getQtdRodeiros() {
        return qtdRodeiros;
    }

    public void setQtdRodeiros(String qtdRodeiros) {
        this.qtdRodeiros = qtdRodeiros;
    }

    public String getQtdTruque() {
        return qtdTruque;
    }

    public void setQtdTruque(String qtdTruque) {
        this.qtdTruque = qtdTruque;
    }

    public String getQtdEngate() {
        return qtdEngate;
    }

    public void setQtdEngate(String qtdEngate) {
        this.qtdEngate = qtdEngate;
    }

    public String getQtdEstrutura() {
        return qtdEstrutura;
    }

    public void setQtdEstrutura(String qtdEstrutura) {
        this.qtdEstrutura = qtdEstrutura;
    }

    public String getQtdSistemaDeFreio() {
        return qtdSistemaDeFreio;
    }

    public void setQtdSistemaDeFreio(String qtdSistemaDeFreio) {
        this.qtdSistemaDeFreio = qtdSistemaDeFreio;
    }

    public String getQtdTanque() {
        return qtdTanque;
    }

    public void setQtdTanque(String qtdTanque) {
        this.qtdTanque = qtdTanque;
    }

    public String getQtdCaixaDeGracha() {
        return qtdCaixaDeGracha;
    }

    public void setQtdCaixaDeGracha(String qtdCaixaDeGracha) {
        this.qtdCaixaDeGracha = qtdCaixaDeGracha;
    }

    public String getQtdMaoDeObra() {
        return qtdMaoDeObra;
    }

    public void setQtdMaoDeObra(String qtdMaoDeObra) {
        this.qtdMaoDeObra = qtdMaoDeObra;
    }

    public String getQtdOutros() {
        return qtdOutros;
    }

    public void setQtdOutros(String qtdOutros) {
        this.qtdOutros = qtdOutros;
    }

    public long getValorRodeiros() {
        return valorRodeiros;
    }

    public void setValorRodeiros(long valorRodeiros) {
        this.valorRodeiros = valorRodeiros;
    }

    public long getValorTruque() {
        return valorTruque;
    }

    public void setValorTruque(long valorTruque) {
        this.valorTruque = valorTruque;
    }

    public long getValorEngate() {
        return valorEngate;
    }

    public void setValorEngate(long valorEngate) {
        this.valorEngate = valorEngate;
    }

    public long getValorEstrutura() {
        return valorEstrutura;
    }

    public void setValorEstrutura(long valorEstrutura) {
        this.valorEstrutura = valorEstrutura;
    }

    public long getValorSistemaDeFreio() {
        return valorSistemaDeFreio;
    }

    public void setValorSistemaDeFreio(long valorSistemaDeFreio) {
        this.valorSistemaDeFreio = valorSistemaDeFreio;
    }

    public long getValorTanque() {
        return valorTanque;
    }

    public void setValorTanque(long valorTanque) {
        this.valorTanque = valorTanque;
    }

    public long getValorCaixaDeGracha() {
        return valorCaixaDeGracha;
    }

    public void setValorCaixaDeGracha(long valorCaixaDeGracha) {
        this.valorCaixaDeGracha = valorCaixaDeGracha;
    }

    public long getValorMaoDeObra() {
        return valorMaoDeObra;
    }

    public void setValorMaoDeObra(long valorMaoDeObra) {
        this.valorMaoDeObra = valorMaoDeObra;
    }

    public long getValorOutros() {
        return valorOutros;
    }

    public void setValorOutros(long valorOutros) {
        this.valorOutros = valorOutros;
    }

    public String getDescricao() {
        return descricao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public int getValoUnitario() {
        return valoUnitario;
    }

    public String getQtdTremonha() {
        return qtdTremonha;
    }

    public long getValorTremonha() {
        return valorTremonha;
    }
}
