package br.com.api.model.incidente;

import com.github.javafaker.Faker;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static br.com.api.GeradorDeMassa.*;
import static br.com.api.GeradorDePlaca.generateLetters;

public class Incidente {

    private Faker faker;
    private Integer idIncidente;
    private String data;
    private String hora;
    private int areaResponsavelInicialIndex;
    private String areaResponsavelInicialValue;
    private int tipoIndex;
    private String tipoValue;
    private int subTipoIndex;
    private String subTipoValue;
    private String criticidade;
    private int fonteDoIncidenteIndex;
    private String fonteDoIncidenteValue;
    private String descricaoDoIncidente;
    private int secaoBloqueioSB;
    private String km;
    private int subdivisaoIndex;
    private String subdivisaoValue;
    private int complexoIndex;
    private String complexoValue;
    private int corredorIndex;
    private String corredorValue;
    private int operacaoIndex;
    private String operacaoValue;
    private boolean existePrefixoDoTrem;
    private int tipoDeComposicaoIndex;
    private String tipoDeComposicaoValue;
    private String prefixo;
    private String OS;
    private boolean ativosForamEnvolvidos;

    /*
    Dados área responsavel
     */
    private int areaResponsavelIndex;
    private String areaResponsavelValue;
    private String coordenadorNomeAreaResponsavel;
    private String coordenadorMatriculaAreaResponsavel;
    private String gerenteNomeAreaResponsavel;
    private String gerenteMatriculaAreaResponsavel;
    private String responsavelNomeAreaResponsavel;
    private String responsavelMatriculaAreaResponsavel;

    private String matriculaUsuarioCriacao;
    private String nomeUsuarioCriacao;

    public Incidente() {
        faker = new Faker(new Locale("pt-BR"));
        data = getDay();
        hora = LocalDateTime.now().minusHours(2).format(DateTimeFormatter.ofPattern("HH:mm"));
        areaResponsavelInicialIndex = faker.options().option(1);
        tipoIndex = faker.options().option(0, 2);
        subTipoIndex = faker.options().option(0);
        fonteDoIncidenteIndex = faker.options().option(1);
        descricaoDoIncidente = getRandomCharacters(2000);
        secaoBloqueioSB = getRandom(null);
        km = faker.options().option("10,000", "11,501", "122,351");
        subdivisaoIndex = getRandom(2);
        subdivisaoIndex = faker.options().option(1);
        complexoIndex = faker.options().option(1);
        corredorIndex = faker.options().option(1);
        operacaoIndex = faker.options().option(0, 1);
        existePrefixoDoTrem = faker.bool().bool();
        prefixo = generateLetters(3);
        OS = String.valueOf(getRandom(9999));
        tipoDeComposicaoIndex = faker.options().option(1);
        ativosForamEnvolvidos = faker.bool().bool();
        areaResponsavelIndex = faker.options().option(0, 1, 2, 3);
        coordenadorNomeAreaResponsavel = faker.options().option("Marcio Rodrigues de Freitas", "José Hailton da Silva", "Lenaldo Monteiro", "Josenildo Mariano da Silva");
        gerenteNomeAreaResponsavel = faker.options().option("Marcio Rodrigues de Freitas", "José Hailton da Silva", "Lenaldo Monteiro", "Josenildo Mariano da Silva");
        responsavelNomeAreaResponsavel = faker.options().option("Marcio Rodrigues de Freitas", "José Hailton da Silva", "Lenaldo Monteiro", "Josenildo Mariano da Silva");
        matriculaUsuarioCriacao = "00189367";
        nomeUsuarioCriacao = "João Firmino dos Santos";

    }

    public int getAreaResponsavelIndex() {
        return areaResponsavelIndex;
    }

    public String getAreaResponsavelValue() {
        return areaResponsavelValue;
    }

    public void setAreaResponsavelValue(String areaResponsavelValue) {
        this.areaResponsavelValue = areaResponsavelValue;
    }

    public String getCoordenadorNomeAreaResponsavel() {
        return coordenadorNomeAreaResponsavel;
    }

    public String getCoordenadorMatriculaAreaResponsavel() {
        return coordenadorMatriculaAreaResponsavel;
    }

    public void setCoordenadorMatriculaAreaResponsavel(String coordenadorMatriculaAreaResponsavel) {
        this.coordenadorMatriculaAreaResponsavel = coordenadorMatriculaAreaResponsavel;
    }

    public String getGerenteNomeAreaResponsavel() {
        return gerenteNomeAreaResponsavel;
    }

    public String getGerenteMatriculaAreaResponsavel() {
        return gerenteMatriculaAreaResponsavel;
    }

    public void setGerenteMatriculaAreaResponsavel(String gerenteMatriculaAreaResponsavel) {
        this.gerenteMatriculaAreaResponsavel = gerenteMatriculaAreaResponsavel;
    }

    public String getResponsavelNomeAreaResponsavel() {
        return responsavelNomeAreaResponsavel;
    }

    public String getResponsavelMatriculaAreaResponsavel() {
        return responsavelMatriculaAreaResponsavel;
    }

    public void setResponsavelMatriculaAreaResponsavel(String responsavelMatriculaAreaResponsavel) {
        this.responsavelMatriculaAreaResponsavel = responsavelMatriculaAreaResponsavel;
    }

    public Integer getIdIncidente() {
        return idIncidente;
    }

    public void setIdIncidente(Integer idIncidente) {
        this.idIncidente = idIncidente;
    }

    public int getTipoDeComposicaoIndex() {
        return tipoDeComposicaoIndex;
    }

    public String getTipoDeComposicaoValue() {
        return tipoDeComposicaoValue;
    }

    public void setTipoDeComposicaoValue(String tipoDeComposicaoValue) {
        this.tipoDeComposicaoValue = tipoDeComposicaoValue;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public String getOS() {
        return OS;
    }

    public String getData() {
        return data;
    }

    public String getHora() {
        return hora;
    }

    public int getAreaResponsavelInicialIndex() {
        return areaResponsavelInicialIndex;
    }

    public String getAreaResponsavelInicialValue() {
        return areaResponsavelInicialValue;
    }

    public void setAreaResponsavelInicialValue(String areaResponsavelInicialValue) {
        this.areaResponsavelInicialValue = areaResponsavelInicialValue;
    }

    public int getTipoIndex() {
        return tipoIndex;
    }

    public String getTipoValue() {
        return tipoValue;
    }

    public void setTipoValue(String tipoValue) {
        this.tipoValue = tipoValue;
    }

    public int getSubTipoIndex() {
        return subTipoIndex;
    }

    public String getSubTipoValue() {
        return subTipoValue;
    }

    public void setSubTipoValue(String subTipoValue) {
        this.subTipoValue = subTipoValue;
    }

    public String getCriticidade() {
        return criticidade;
    }

    public void setCriticidade(String criticidade) {
        this.criticidade = criticidade;
    }

    public int getFonteDoIncidenteIndex() {
        return fonteDoIncidenteIndex;
    }

    public String getFonteDoIncidenteValue() {
        return fonteDoIncidenteValue;
    }

    public void setFonteDoIncidenteValue(String fonteDoIncidenteValue) {
        this.fonteDoIncidenteValue = fonteDoIncidenteValue;
    }

    public String getDescricaoDoIncidente() {
        return descricaoDoIncidente;
    }

    public int getSecaoBloqueioSB() {
        return secaoBloqueioSB;
    }

    public String getKm() {
        return km;
    }

    public int getSubdivisaoIndex() {
        return subdivisaoIndex;
    }

    public String getSubdivisaoValue() {
        return subdivisaoValue;
    }

    public void setSubdivisaoValue(String subdivisaoValue) {
        this.subdivisaoValue = subdivisaoValue;
    }

    public int getComplexoIndex() {
        return complexoIndex;
    }

    public String getComplexoValue() {
        return complexoValue;
    }

    public void setComplexoValue(String complexoValue) {
        this.complexoValue = complexoValue;
    }

    public int getCorredorIndex() {
        return corredorIndex;
    }

    public String getCorredorValue() {
        return corredorValue;
    }

    public void setCorredorValue(String corredorValue) {
        this.corredorValue = corredorValue;
    }

    public int getOperacaoIndex() {
        return operacaoIndex;
    }

    public String getOperacaoValue() {
        return operacaoValue;
    }

    public void setOperacaoValue(String operacaoValue) {
        this.operacaoValue = operacaoValue;
    }

    public boolean isExistePrefixoDoTrem() {
        return existePrefixoDoTrem;
    }

    public void setExistePrefixoDoTrem(boolean value) {
        this.existePrefixoDoTrem = value;
    }

    public boolean isAtivosForamEnvolvidos() {
        return ativosForamEnvolvidos;
    }

    public void setAtivosForamEnvolvidos(boolean b) {
        this.ativosForamEnvolvidos = b;
    }

    public String getMatriculaUsuarioCriacao() {
        return matriculaUsuarioCriacao;
    }

    public String getNomeUsuarioCriacao() {
        return nomeUsuarioCriacao;
    }
}