package br.com.api.model.central_de_acoes;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getDay;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class Acao {

    public int idAcao;
    private Faker faker;
    private int idPlanoAcao;
    private int focoCausaDaAcaoIndex;
    private String focoCausaDaAcaoValue;
    private int qualOImpactoDaAcaoIndex;
    private String qualOImpactoDaAcaoValue;
    private int qualOTipoDaAcaoIndex;
    private String qualOTipoDaAcaoValue;
    private String qualEOPrazoParaExecucao;
    private boolean acaoPublica;
    private String descritivoDaAcao;
    private boolean operacao;
    private boolean complexo;
    private String dataDeFinalizacao;
    private String resultadosAlcancados;
    private String comentarios;
    private String sugestoesDeAcoesFuturas;
    private boolean houveAVerificacaoDaEficaciaDaAcao;
    private String comentarioComprovacao;
    private String dataDaComprovacao;
    private String justificativaReabertura;
    private boolean recusarAcaoDesdobrada;
    private String justificativaRecusarAcaoDesdobrada;
    private String porQueVoceEstaRecusandoAAcao;
    private String descricaoDaAcaoSugiraUmaNovaDescricaoDaAcao;
    private String qualEOPrazoParaExecucaoSoliciteUmNovoPrazo;
    private String justificativaDiscordaDoResponsavel;
    private String justificativaAceitarAlteracao;
    private String motivoRecusaDesdobrada;
    private String justificativaAcaoRecusada;
    private String origemAcao;

    public Acao() {
        faker = new Faker(new Locale("pt-BR"));
        focoCausaDaAcaoIndex = faker.options().option(0, 1, 2);
        qualOImpactoDaAcaoIndex = faker.options().option(0, 1, 2);
        qualOTipoDaAcaoIndex = faker.options().option(0, 1, 2);
        qualEOPrazoParaExecucao = faker.options().option(getDay(10), getDay(9), getDay(8), getDay(7), getDay(6), getDay(5));
        dataDeFinalizacao = faker.options().option(getDay(10), getDay(9), getDay(8), getDay(7), getDay(6), getDay(5));
        qualEOPrazoParaExecucaoSoliciteUmNovoPrazo = faker.options().option(getDay(10), getDay(9), getDay(8), getDay(7), getDay(6), getDay(5));
        dataDaComprovacao = getDay();
        acaoPublica = faker.bool().bool();
        descritivoDaAcao = getRandomCharacters(2000);
        resultadosAlcancados = getRandomCharacters(2000);
        comentarios = getRandomCharacters(2000);
        sugestoesDeAcoesFuturas = getRandomCharacters(2000);
        comentarioComprovacao = getRandomCharacters(2000);
        justificativaReabertura = getRandomCharacters(2000);
        descricaoDaAcaoSugiraUmaNovaDescricaoDaAcao = getRandomCharacters(2000);
        justificativaDiscordaDoResponsavel = getRandomCharacters(2000);
        justificativaRecusarAcaoDesdobrada = getRandomCharacters(2000);
        justificativaAcaoRecusada = getRandomCharacters(2000);
        justificativaAceitarAlteracao = getRandomCharacters(2000);
        operacao = faker.bool().bool();
        complexo = faker.bool().bool();
        houveAVerificacaoDaEficaciaDaAcao = faker.bool().bool();
        recusarAcaoDesdobrada = faker.bool().bool();
    }

    public String getJustificativaAceitarAlteracao() {
        return justificativaAceitarAlteracao;
    }

    public String getMotivoRecusaDesdobrada() {
        return motivoRecusaDesdobrada;
    }

    public void setMotivoRecusaDesdobrada(String value) {
        this.motivoRecusaDesdobrada = value;
    }

    public String getJustificativaDiscordaDoResponsavel() {
        return justificativaDiscordaDoResponsavel;
    }

    public String getQualEOPrazoParaExecucaoSoliciteUmNovoPrazo() {
        return qualEOPrazoParaExecucaoSoliciteUmNovoPrazo;
    }

    public String getPorQueVoceEstaRecusandoAAcao() {
        return porQueVoceEstaRecusandoAAcao;
    }

    public void setPorQueVoceEstaRecusandoAAcao(String porQueVoceEstaRecusandoAAcao) {
        this.porQueVoceEstaRecusandoAAcao = porQueVoceEstaRecusandoAAcao;
    }

    public String getDescricaoDaAcaoSugiraUmaNovaDescricaoDaAcao() {
        return descricaoDaAcaoSugiraUmaNovaDescricaoDaAcao;
    }

    public String getOrigemAcao() {
        return origemAcao;
    }

    public void setOrigemAcao(String origemAcao) {
        this.origemAcao = origemAcao;
    }

    public String getJustificativaReabertura() {
        return justificativaReabertura;
    }

    public boolean isHouveAVerificacaoDaEficaciaDaAcao() {
        return houveAVerificacaoDaEficaciaDaAcao;
    }

    public void setHouveAVerificacaoDaEficaciaDaAcao(boolean houveAVerificacaoDaEficaciaDaAcao) {
        this.houveAVerificacaoDaEficaciaDaAcao = houveAVerificacaoDaEficaciaDaAcao;
    }

    public String getJustificativaRecusarAcaoDesdobrada() {
        return justificativaRecusarAcaoDesdobrada;
    }

    public boolean isRecusarAcaoDesdobrada() {
        return recusarAcaoDesdobrada;
    }

    public String getComentarioComprovacao() {
        return comentarioComprovacao;
    }

    public String getDataDaComprovacao() {
        return dataDaComprovacao;
    }

    public String getDataDeFinalizacao() {
        return dataDeFinalizacao;
    }

    public String getResultadosAlcancados() {
        return resultadosAlcancados;
    }

    public String getComentarios() {
        return comentarios;
    }

    public String getSugestoesDeAcoesFuturas() {
        return sugestoesDeAcoesFuturas;
    }

    public int getIdAcao() {
        return idAcao;
    }

    public void setIdAcao(int idAcao) {
        this.idAcao = idAcao;
    }

    public int getFocoCausaDaAcaoIndex() {
        return focoCausaDaAcaoIndex;
    }

    public String getFocoCausaDaAcaoValue() {
        return focoCausaDaAcaoValue;
    }

    public void setFocoCausaDaAcaoValue(String focoCausaDaAcaoValue) {
        this.focoCausaDaAcaoValue = focoCausaDaAcaoValue;
    }

    public int getQualOImpactoDaAcaoIndex() {
        return qualOImpactoDaAcaoIndex;
    }

    public String getQualOImpactoDaAcaoValue() {
        return qualOImpactoDaAcaoValue;
    }

    public void setQualOImpactoDaAcaoValue(String qualOImpactoDaAcaoValue) {
        this.qualOImpactoDaAcaoValue = qualOImpactoDaAcaoValue;
    }

    public int getQualOTipoDaAcaoIndex() {
        return qualOTipoDaAcaoIndex;
    }

    public String getQualOTipoDaAcaoValue() {
        return qualOTipoDaAcaoValue;
    }

    public void setQualOTipoDaAcaoValue(String qualOTipoDaAcaoValue) {
        this.qualOTipoDaAcaoValue = qualOTipoDaAcaoValue;
    }

    public String getQualEOPrazoParaExecucao() {
        return qualEOPrazoParaExecucao;
    }

    public boolean isAcaoPublica() {
        return acaoPublica;
    }

    public String getDescritivoDaAcao() {
        return descritivoDaAcao;
    }

    public int getIdPlanoAcao() {
        return idPlanoAcao;
    }

    public void setIdPlanoAcao(int idPlanoAcao) {
        this.idPlanoAcao = idPlanoAcao;
    }

    public boolean isOperacao() {
        return operacao;
    }

    public void setOperacao(boolean operacao) {
        this.operacao = operacao;
    }

    public boolean isComplexo() {
        return complexo;
    }

    public void setComplexo(boolean complexo) {
        this.complexo = complexo;
    }

    public String getJustificativaAcaoRecusada() {
        return justificativaAcaoRecusada;
    }
}
