package br.com.api.model.central_de_acoes;

import br.com.api.model.sindicancia.RelatorioFinal;
import br.com.api.model.sinistro.Sinistro;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class PlanoDeAcao {
    private Integer idPlanoAcao;
    private int idSindicancia;
    private int idPlanoCausaAcao;
    private String origem;
    private String operacao;
    private String complexo;
    private String bandeira;
    private String fatoOcorrido;
    private String causaProvavelIdentificada;
    private String solicitante;
    private String solicitanteMatricula;
    private String nomePlanoAcaoAvulso;

    public PlanoDeAcao() {
        this.nomePlanoAcaoAvulso = getRandomCharacters(512);
    }

    public String getSolicitanteMatricula() {
        return solicitanteMatricula;
    }

    public void setSolicitanteMatricula(String solicitanteMatricula) {
        this.solicitanteMatricula = solicitanteMatricula;
    }

    public int getIdPlanoCausaAcao() {
        return idPlanoCausaAcao;
    }

    public void setIdPlanoCausaAcao(int idPlanoCausaAcao) {
        this.idPlanoCausaAcao = idPlanoCausaAcao;
    }

    public void setValues(Sinistro sinistro, RelatorioFinal relatorioFinal, Integer idPlanoAcao, String complexo, String operacao, Integer idPlanoCausaAcao) {
        this.idSindicancia = sinistro.getIdSinistro();
        this.idPlanoAcao = idPlanoAcao;
        this.origem = sinistro.getClassificacao().equalsIgnoreCase("OCORRENCIA") ? "Ocorrência" : "Acidente";
        this.operacao = operacao;
        this.complexo = complexo;
        this.bandeira = relatorioFinal.getBandeiraValue();
        this.fatoOcorrido = relatorioFinal.getFatoOcorrido();
        this.causaProvavelIdentificada = relatorioFinal.getCausaValue();
        this.solicitante = relatorioFinal.getResponsavel();
        this.idPlanoCausaAcao = idPlanoCausaAcao;
        this.solicitanteMatricula = relatorioFinal.getMatriculoDono();
    }

    public int getIdSindicancia() {
        return idSindicancia;
    }

    public void setIdSindicancia(int idSindicancia) {
        this.idSindicancia = idSindicancia;
    }

    public String getBandeira() {
        return bandeira;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    public String getFatoOcorrido() {
        return fatoOcorrido;
    }

    public void setFatoOcorrido(String fatoOcorrido) {
        this.fatoOcorrido = fatoOcorrido;
    }

    public String getCausaProvavelIdentificada() {
        return causaProvavelIdentificada;
    }

    public void setCausaProvavelIdentificada(String causaProvavelIdentificada) {
        this.causaProvavelIdentificada = causaProvavelIdentificada;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public int getIdPlanoAcao() {
        return idPlanoAcao;
    }

    public void setIdPlanoAcao(Integer idPlanoAcao) {
        this.idPlanoAcao = idPlanoAcao;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public String getComplexo() {
        return complexo;
    }

    public void setComplexo(String complexo) {
        this.complexo = complexo;
    }

    public String getNomePlanoAcaoAvulso() {
        return nomePlanoAcaoAvulso;
    }

    public void setNomePlanoAcaoAvulso(String nomePlanoAcaoAvulso) {
        this.nomePlanoAcaoAvulso = nomePlanoAcaoAvulso;
    }
}
