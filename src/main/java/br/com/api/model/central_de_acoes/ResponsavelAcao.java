package br.com.api.model.central_de_acoes;

import com.github.javafaker.Faker;

import java.util.Locale;

public class ResponsavelAcao {
    private Faker faker;
    private String qualEAOperacaoValue;
    private int qualEAOperacaoIndex;
    private String qualEOComplexoValue;
    private int qualEOComplexoIndex;
    private String responsavel;
    private String gerente;
    private String area;

    public ResponsavelAcao() {
        faker = new Faker(new Locale("pt-BR"));
        qualEAOperacaoIndex = faker.options().option(0, 1);
        qualEOComplexoIndex = faker.options().option(0, 1, 2);
        responsavel = faker.options().option("Marcio Rodrigues de Freitas", "José Hailton da Silva", "Lenaldo Monteiro", "Josenildo Mariano da Silva");
    }

    public String getQualEAOperacaoValue() {
        return qualEAOperacaoValue;
    }

    public void setQualEAOperacaoValue(String qualEAOperacaoValue) {
        this.qualEAOperacaoValue = qualEAOperacaoValue;
    }

    public int getQualEAOperacaoIndex() {
        return qualEAOperacaoIndex;
    }

    public String getQualEOComplexoValue() {
        return qualEOComplexoValue;
    }

    public void setQualEOComplexoValue(String qualEOComplexoValue) {
        this.qualEOComplexoValue = qualEOComplexoValue;
    }

    public int getQualEOComplexoIndex() {
        return qualEOComplexoIndex;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getGerente() {
        return gerente;
    }

    public void setGerente(String gerente) {
        this.gerente = gerente;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

}
