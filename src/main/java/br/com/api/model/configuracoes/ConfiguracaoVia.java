package br.com.api.model.configuracoes;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class ConfiguracaoVia {
    private Faker faker;
    /*
        Configuração
         */
    private String nomeDaConfiguracao;
    private boolean status;
    /*
    Faixa
     */
    private String menorVelocidade;
    private String maiorVelocidade;
    private String tangenteTorcao;
    private String tangenteVariacao;
    private String curvaTorcao;
    private String curvaVariacao;
    private String tangenteTorcao2;
    private String tangenteVariacao2;
    private String curvaTorcao2;
    private String curvaVariacao2;
    private String torcaoTwist;
    private String bitolaMinima;
    private String bitolaMaxima;
    private String variacaoBitola;
    private String flecha;
    private String defeitoAlinhamentoTGECV;
    private String defeitoAlinhamentoTransicao;
    private String classeVia;
    private String superiorADMAG;

    public ConfiguracaoVia() {
        faker = new Faker(new Locale("pt-BR"));
        nomeDaConfiguracao = getRandomCharacters(30);
        status = faker.bool().bool();
        menorVelocidade = String.valueOf(getRandom(100));
        maiorVelocidade = String.valueOf(getRandom(9999));
        tangenteTorcao = String.valueOf(getRandom(null));
        tangenteVariacao = String.valueOf(getRandom(null));
        curvaTorcao = String.valueOf(getRandom(null));
        curvaVariacao = String.valueOf(getRandom(null));
        tangenteTorcao2 = String.valueOf(getRandom(null));
        tangenteVariacao2 = String.valueOf(getRandom(null));
        curvaTorcao2 = String.valueOf(getRandom(null));
        curvaVariacao2 = String.valueOf(getRandom(null));
        torcaoTwist = String.valueOf(getRandom(null));
        bitolaMinima = String.valueOf(getRandom(null));
        bitolaMaxima = String.valueOf(getRandom(null));
        variacaoBitola = String.valueOf(getRandom(null));
        flecha = String.valueOf(getRandom(null));
        defeitoAlinhamentoTGECV = String.valueOf(getRandom(null));
        defeitoAlinhamentoTransicao = String.valueOf(getRandom(null));
        superiorADMAG = String.valueOf(getRandom(null));
        classeVia = getRandomCharacters(3);
    }

    public String getNomeDaConfiguracao() {
        return nomeDaConfiguracao;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMenorVelocidade() {
        return menorVelocidade;
    }

    public String getMaiorVelocidade() {
        return maiorVelocidade;
    }

    public String getTangenteTorcao() {
        return tangenteTorcao;
    }

    public String getTangenteVariacao() {
        return tangenteVariacao;
    }

    public String getCurvaTorcao() {
        return curvaTorcao;
    }

    public String getCurvaVariacao() {
        return curvaVariacao;
    }

    public String getTangenteTorcao2() {
        return tangenteTorcao2;
    }

    public String getTangenteVariacao2() {
        return tangenteVariacao2;
    }

    public String getCurvaTorcao2() {
        return curvaTorcao2;
    }

    public String getCurvaVariacao2() {
        return curvaVariacao2;
    }

    public String getTorcaoTwist() {
        return torcaoTwist;
    }

    public String getBitolaMinima() {
        return bitolaMinima;
    }

    public String getBitolaMaxima() {
        return bitolaMaxima;
    }

    public String getVariacaoBitola() {
        return variacaoBitola;
    }

    public String getFlecha() {
        return flecha;
    }

    public String getDefeitoAlinhamentoTGECV() {
        return defeitoAlinhamentoTGECV;
    }

    public String getDefeitoAlinhamentoTransicao() {
        return defeitoAlinhamentoTransicao;
    }

    public String getClasseVia() {
        return classeVia;
    }

    public String getSuperiorADMAG() {
        return superiorADMAG;
    }
}
