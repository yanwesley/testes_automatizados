package br.com.api.model.configuracoes;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class ConfiguracaoMaterialRodanteLocomotiva {
    private Faker faker;
    private int modeloIndex;
    private String modeloValue;
    private String nomeDoBloco;
    private Boolean configuracaoAtiva;
    private String escolhaOModelo;
    private boolean configuracaoAtivaConfiguracao;

    /*
    Configuração
     */
    private String folgaVerticalValorMinimo;
    private String folgaVerticalValorMaximo;
    private String bitolaInternaDoRodeiroValorMinimo;
    private String bitolaInternaDoRodeiroValorMaximo;
    private String espessuraDeFrisoValorMinimo;
    private String espessuraDeFrisoValorMaximo;
    private String alturaDeFrisoValorMinimo;
    private String alturaDeFrisoValorMaximo;
    private String espessuraDeBandagemValorMinimo;
    private String concavidadeValorMaximo;
    private Boolean frisoVertical;
    private String rodasNoMesmoEixoValorMaximo;
    private String rodasNoMesmoTruqueValorMaximo;
    private String rodasNaMesmaLocomotivaValorMaximo;
    private String distanciaVerticalEntreBoletoLimpaTrilhoValorMinimo;
    private String distanciaVerticalEntreBoletoLimpaTrilhoValorMaximo;
    private String alturaEntreCentroDaMandibulaTopoBoletoValorMinimo;
    private String alturaEntreCentroDaMandibulaTopoBoletoValorMaximo;

    public ConfiguracaoMaterialRodanteLocomotiva() {
        faker = new Faker(new Locale("pt-BR"));
        nomeDoBloco = getRandomCharacters(255);
        configuracaoAtiva = faker.bool().bool();
        configuracaoAtivaConfiguracao = faker.bool().bool();
        folgaVerticalValorMinimo = String.valueOf(getRandom(null));
        folgaVerticalValorMaximo = String.valueOf(getRandom(null));
        bitolaInternaDoRodeiroValorMinimo = String.valueOf(getRandom(null));
        bitolaInternaDoRodeiroValorMaximo = String.valueOf(getRandom(null));
        espessuraDeFrisoValorMinimo = String.valueOf(getRandom(null));
        espessuraDeFrisoValorMaximo = String.valueOf(getRandom(null));
        alturaDeFrisoValorMinimo = String.valueOf(getRandom(null));
        alturaDeFrisoValorMaximo = String.valueOf(getRandom(null));
        espessuraDeBandagemValorMinimo = String.valueOf(getRandom(null));
        concavidadeValorMaximo = String.valueOf(getRandom(null));
        frisoVertical = faker.bool().bool();
        rodasNoMesmoEixoValorMaximo = String.valueOf(getRandom(null));
        rodasNoMesmoTruqueValorMaximo = String.valueOf(getRandom(null));
        rodasNaMesmaLocomotivaValorMaximo = String.valueOf(getRandom(null));
        distanciaVerticalEntreBoletoLimpaTrilhoValorMinimo = String.valueOf(getRandom(null));
        distanciaVerticalEntreBoletoLimpaTrilhoValorMaximo = String.valueOf(getRandom(null));
        alturaEntreCentroDaMandibulaTopoBoletoValorMinimo = String.valueOf(getRandom(null));
        alturaEntreCentroDaMandibulaTopoBoletoValorMaximo = String.valueOf(getRandom(null));
        modeloIndex = getRandom(10);
    }

    public String getModeloValue() {
        return modeloValue;
    }

    public void setModeloValue(String modeloValue) {
        this.modeloValue = modeloValue;
    }

    public String getNomeDoBloco() {
        return nomeDoBloco;
    }

    public Boolean getConfiguracaoAtiva() {
        return configuracaoAtiva;
    }

    public String getEscolhaOModelo() {
        return escolhaOModelo;
    }

    public void setEscolhaOModelo(String escolhaOModelo) {
        this.escolhaOModelo = escolhaOModelo;
    }

    public boolean isConfiguracaoAtivaConfiguracao() {
        return configuracaoAtivaConfiguracao;
    }

    public String getFolgaVerticalValorMinimo() {
        return folgaVerticalValorMinimo;
    }

    public String getFolgaVerticalValorMaximo() {
        return folgaVerticalValorMaximo;
    }

    public String getBitolaInternaDoRodeiroValorMinimo() {
        return bitolaInternaDoRodeiroValorMinimo;
    }

    public String getBitolaInternaDoRodeiroValorMaximo() {
        return bitolaInternaDoRodeiroValorMaximo;
    }

    public String getEspessuraDeFrisoValorMinimo() {
        return espessuraDeFrisoValorMinimo;
    }

    public String getEspessuraDeFrisoValorMaximo() {
        return espessuraDeFrisoValorMaximo;
    }

    public String getAlturaDeFrisoValorMinimo() {
        return alturaDeFrisoValorMinimo;
    }

    public String getAlturaDeFrisoValorMaximo() {
        return alturaDeFrisoValorMaximo;
    }

    public String getEspessuraDeBandagemValorMinimo() {
        return espessuraDeBandagemValorMinimo;
    }

    public String getConcavidadeValorMaximo() {
        return concavidadeValorMaximo;
    }

    public Boolean getFrisoVertical() {
        return frisoVertical;
    }

    public String getRodasNoMesmoEixoValorMaximo() {
        return rodasNoMesmoEixoValorMaximo;
    }

    public String getRodasNoMesmoTruqueValorMaximo() {
        return rodasNoMesmoTruqueValorMaximo;
    }

    public String getRodasNaMesmaLocomotivaValorMaximo() {
        return rodasNaMesmaLocomotivaValorMaximo;
    }

    public String getDistanciaVerticalEntreBoletoLimpaTrilhoValorMinimo() {
        return distanciaVerticalEntreBoletoLimpaTrilhoValorMinimo;
    }

    public String getDistanciaVerticalEntreBoletoLimpaTrilhoValorMaximo() {
        return distanciaVerticalEntreBoletoLimpaTrilhoValorMaximo;
    }

    public String getAlturaEntreCentroDaMandibulaTopoBoletoValorMinimo() {
        return alturaEntreCentroDaMandibulaTopoBoletoValorMinimo;
    }

    public String getAlturaEntreCentroDaMandibulaTopoBoletoValorMaximo() {
        return alturaEntreCentroDaMandibulaTopoBoletoValorMaximo;
    }

    public int getModeloIndex() {
        return modeloIndex;
    }

    public void validarValoresConfiguracaoCopiada() {

    }
}
