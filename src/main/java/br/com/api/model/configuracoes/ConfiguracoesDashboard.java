package br.com.api.model.configuracoes;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;

import static br.com.api.GeradorDeMassa.getRandom;

public class ConfiguracoesDashboard {
    private List<Integer> toleranciaAcidenteSul = new ArrayList<>();
    private List<Integer> toleranciaAcidenteNorte = new ArrayList<>();
    private List<Integer> toleranciaAcidenteCentral = new ArrayList<>();
    private List<Long> toleranciaGravidadeSul = new ArrayList<>();
    private List<Long> toleranciaGravidadeNorte = new ArrayList<>();
    private List<Long> toleranciaGravidadeCentral = new ArrayList<>();
    private Faker faker = new Faker();

    public ConfiguracoesDashboard() {
        setToleranciaAcidenteSul();
        setToleranciaAcidenteNorte();
        setToleranciaAcidenteCentral();
        setToleranciaGravidadeSul();
        setToleranciaGravidadeNorte();
        setToleranciaGravidadeCentral();
    }

    private void setToleranciaAcidenteSul() {
        for (int i = 0; i <= 12; i++) {
            this.toleranciaAcidenteSul.add(getRandom(null));
        }
    }

    private void setToleranciaAcidenteNorte() {
        for (int i = 0; i <= 12; i++) {
            this.toleranciaAcidenteNorte.add(getRandom(null));
        }
    }

    private void setToleranciaAcidenteCentral() {
        for (int i = 0; i <= 12; i++) {
            this.toleranciaAcidenteCentral.add(getRandom(null));
        }
    }

    private void setToleranciaGravidadeSul() {
        for (long i = 0; i <= 12; i++) {
            this.toleranciaGravidadeSul.add(faker.random().nextLong(999999999L));
        }
    }

    private void setToleranciaGravidadeNorte() {
        for (long i = 0; i <= 12; i++) {
            this.toleranciaGravidadeNorte.add(faker.random().nextLong(999999999L));
        }
    }

    private void setToleranciaGravidadeCentral() {
        for (long i = 0; i <= 12; i++) {
            this.toleranciaGravidadeCentral.add(faker.random().nextLong(999999999L));
        }
    }

    public List<Integer> getToleranciaAcidenteSul() {
        return toleranciaAcidenteSul;
    }

    public List<Integer> getToleranciaAcidenteNorte() {
        return toleranciaAcidenteNorte;
    }

    public List<Long> getToleranciaGravidadeSul() {
        return toleranciaGravidadeSul;
    }

    public List<Long> getToleranciaGravidadeNorte() {return toleranciaGravidadeNorte; }

    public List<Integer> getToleranciaAcidenteCentral() {return toleranciaAcidenteCentral;}

    public List<Long> getToleranciaGravidadeCentral() {return toleranciaGravidadeCentral;}
}
