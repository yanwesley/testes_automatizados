package br.com.api.model.configuracoes;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandom;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class ConfiguracaoMaterialRodanteVagao {
    private Faker faker;
    //Configuração vagão
    private String nomeConfiguracaoVagao;
    private boolean configuracaoVagaoAtiva;

    //Limites do rodeiro
    private String bitolaMinimaLarga;
    private String bitolaMinimaMetrica;
    private String bitolaMaximaLarga;
    private String bitolaMaximaMetrica;
    private String espessuraFrisoLarga;
    private String espessuraFrisoMetrica;
    private String alturaFrisoLarga;
    private String alturaFrisoMetrica;
    private String bandagemLarga;
    private String bandagemMetrica;
    private String hollowLarga;
    private String hollowMetrica;
    private String diferencaMaximaEspessuraDeFriso;

    //Informação sobre a manga de eixo
    private String manga;
    private String letraNaBitolaMetrica;
    private String letraNaBitolaLarga;

    //Tipo de manutenção
    private String tiposManutencao;
    private boolean bitolaTipoManutencao;

    //Limite de cunhas de fricção
    private String truqueCunhaFriccao;
    private int limiteAlturaCunhaFriccao;

    //Limite de diferença de botões
    private int limiteDiferencaBotoesMaximo;

    //Limites de prato de pião
    private String pratoPiao;
    private boolean bitolaLimitePratoDePiao;
    private String supMin;
    private String supMax;
    private String altMin;
    private String altMax;
    private String infMin;
    private String infMax;
    private String profMin;
    private String profMax;

    //Limites de Folga de Prato Pião
    private int folgaMinimaPrato;
    private int folgaMaximaPrato;
    private int folgaVerticalMinimaPrato;
    private int folgaVerticalMaximaPrato;

    //Limites de amparo balanço
    private String tipoDeAmparoBalanco;
    private int amparoBalancoMinimo;
    private int amparoBalancoMaximo;
    private int amparoBalancoFolgaMinima;
    private int amparoBalancoFolgaMaxima;
    private int amparoBalancoDiferencaMaximaSomaCruzada;
    private int amparoBalancoDesgasteConcavoDaChapa;

    //Limites de altura de engate
    private int alturaEngateMetricaVazioAltMin;
    private int alturaEngateMetricaVazioAltMax;
    private int alturaEngateMetricaCarregadoAltMin;
    private int alturaEngateMetricaCarregadoAltMax;
    private int alturaEngateLargaVazioAltMin;
    private int alturaEngateLargaVazioAltMax;
    private int alturaEngateLargaCarregadoAltMin;
    private int alturaEngateLargaCarregadoAltMax;

    //Limite de molas
    private String limiteDeMolasMola;
    private int limiteDeMolasExtMax;
    private int limiteDeMolasExtMin;
    private int limiteDeMolasIntMax;
    private int limiteDeMolasIntMin;
    private int limiteDeMolasIntermMax;
    private int limiteDeMolasIntermMin;

    //Mola cunha
    private String molaCunhaTipoDeMola;
    private int molaCunhaExtMax;
    private int molaCunhaExtMin;
    private int molaCunhaIntMax;
    private int molaCunhaIntMin;

    //Condição do vagão quanto a carga

    //Informação do rolamento

    //Cor e ano de substituição ou lubrificação do rolamento

    public ConfiguracaoMaterialRodanteVagao() {
        faker = new Faker(new Locale("pt-BR"));

        //Configuração vagão
        nomeConfiguracaoVagao = getRandomCharacters(256);
        configuracaoVagaoAtiva = faker.bool().bool();
        //Limites do rodeiro
        bitolaMinimaLarga = "11";
        bitolaMinimaMetrica = "11";
        bitolaMaximaLarga = "13";
        bitolaMaximaMetrica = "14";
        espessuraFrisoLarga = "14";
        espessuraFrisoMetrica = "12";
        alturaFrisoLarga = "15";
        alturaFrisoMetrica = "11";
        bandagemLarga = "16";
        bandagemMetrica = "122";
        hollowLarga = "17";
        hollowMetrica = "121";
        diferencaMaximaEspessuraDeFriso = "156431";
        //Informação sobre a manga de eixo
        manga = getRandomCharacters(8);
        letraNaBitolaMetrica = "X";
        letraNaBitolaLarga = "Y";
        //Tipo de manutenção
        tiposManutencao = getRandomCharacters(255);
        bitolaTipoManutencao = faker.bool().bool();
        //Limite de cunhas de fricção
        truqueCunhaFriccao = getRandomCharacters(255);
        limiteAlturaCunhaFriccao = 9845735;
        //Limite de diferença de botões
        limiteDiferencaBotoesMaximo = getRandom(9999999);
        //Limites de prato de pião
        pratoPiao = getRandomCharacters(8);
        bitolaLimitePratoDePiao = faker.bool().bool();
        supMin = "111222";
        supMax = "111223";
        altMin = "5465454";
        altMax = "5465464";
        infMin = "2423432";
        infMax = "2423435";
        profMin = "879860";
        profMax = "879877";
        //Limites de Folga de Prato Pião
        folgaMinimaPrato = 1;
        folgaMaximaPrato = 5;
        folgaVerticalMinimaPrato = 2;
        folgaVerticalMaximaPrato = 10;
        //Limites de amparo balanço
        tipoDeAmparoBalanco = getRandomCharacters(8);
        amparoBalancoMinimo = 122323;
        amparoBalancoMaximo = 562284;
        amparoBalancoFolgaMinima = 151513;
        amparoBalancoFolgaMaxima = 9887984;
        amparoBalancoDiferencaMaximaSomaCruzada = 987165;
        amparoBalancoDesgasteConcavoDaChapa = 999984;
        //Limites de altura de engate
        alturaEngateMetricaVazioAltMin = 1;
        alturaEngateMetricaVazioAltMax = 5;
        alturaEngateMetricaCarregadoAltMin = 2;
        alturaEngateMetricaCarregadoAltMax = 6;
        alturaEngateLargaVazioAltMin = 3;
        alturaEngateLargaVazioAltMax = 7;
        alturaEngateLargaCarregadoAltMin = 4;
        alturaEngateLargaCarregadoAltMax = 8;
        //Limite de molas
        limiteDeMolasMola = getRandomCharacters(8);
        limiteDeMolasExtMax = 10;
        limiteDeMolasExtMin = 5;
        limiteDeMolasIntMax = 11;
        limiteDeMolasIntMin = 6;
        limiteDeMolasIntermMax = 12;
        limiteDeMolasIntermMin = 7;
        //Mola cunha
        molaCunhaTipoDeMola = getRandomCharacters(8);
        molaCunhaExtMax = 20;
        molaCunhaExtMin = 10;
        molaCunhaIntMax = 40;
        molaCunhaIntMin = 20;
        //Condição do vagão quanto a carga
        //Informação do rolamento
        //Cor e ano de substituição ou lubrificação do rolamento
    }


    public String getNomeConfiguracaoVagao() {
        return nomeConfiguracaoVagao;
    }

    public boolean isConfiguracaoVagaoAtiva() {
        return configuracaoVagaoAtiva;
    }

    public String getBitolaMinimaLarga() {
        return bitolaMinimaLarga;
    }

    public String getBitolaMinimaMetrica() {
        return bitolaMinimaMetrica;
    }

    public String getBitolaMaximaLarga() {
        return bitolaMaximaLarga;
    }

    public String getBitolaMaximaMetrica() {
        return bitolaMaximaMetrica;
    }

    public String getEspessuraFrisoLarga() {
        return espessuraFrisoLarga;
    }

    public String getEspessuraFrisoMetrica() {
        return espessuraFrisoMetrica;
    }

    public String getAlturaFrisoLarga() {
        return alturaFrisoLarga;
    }

    public String getAlturaFrisoMetrica() {
        return alturaFrisoMetrica;
    }

    public String getBandagemLarga() {
        return bandagemLarga;
    }

    public String getBandagemMetrica() {
        return bandagemMetrica;
    }

    public String getHollowLarga() {
        return hollowLarga;
    }

    public String getHollowMetrica() {
        return hollowMetrica;
    }

    public String getDiferencaMaximaEspessuraDeFriso() {
        return diferencaMaximaEspessuraDeFriso;
    }

    public String getManga() {
        return manga;
    }

    public String getLetraNaBitolaMetrica() {
        return letraNaBitolaMetrica;
    }

    public boolean isBitolaLimitePratoDePiao() {
        return bitolaLimitePratoDePiao;
    }

    public String getLetraNaBitolaLarga() {
        return letraNaBitolaLarga;
    }

    public String getTiposManutencao() {
        return tiposManutencao;
    }

    public boolean isBitolaTipoManutencao() {
        return bitolaTipoManutencao;
    }

    public int getLimiteDiferencaBotoesMaximo() {
        return limiteDiferencaBotoesMaximo;
    }

    public String getPratoPiao() {
        return pratoPiao;
    }

    public String getSupMin() {
        return supMin;
    }

    public String getSupMax() {
        return supMax;
    }

    public String getAltMin() {
        return altMin;
    }

    public String getAltMax() {
        return altMax;
    }

    public String getInfMin() {
        return infMin;
    }

    public String getInfMax() {
        return infMax;
    }

    public String getProfMin() {
        return profMin;
    }

    public String getProfMax() {
        return profMax;
    }

    public int getFolgaMinimaPrato() {
        return folgaMinimaPrato;
    }

    public int getFolgaMaximaPrato() {
        return folgaMaximaPrato;
    }

    public int getFolgaVerticalMinimaPrato() {
        return folgaVerticalMinimaPrato;
    }

    public int getFolgaVerticalMaximaPrato() {
        return folgaVerticalMaximaPrato;
    }

    public String getTruqueCunhaFriccao() {
        return truqueCunhaFriccao;
    }

    public int getLimiteAlturaCunhaFriccao() {
        return limiteAlturaCunhaFriccao;
    }

    public String getTipoDeAmpartoBalanco() {
        return tipoDeAmparoBalanco;
    }

    public int getAmpartoBalancoMinimo() {
        return amparoBalancoMinimo;
    }

    public int getAmpartoBalancoMaximo() {
        return amparoBalancoMaximo;
    }

    public int getAmpartoBalancoFolgaMinima() {
        return amparoBalancoFolgaMinima;
    }

    public int getAmpartoBalancoFolgaMaxima() {
        return amparoBalancoFolgaMaxima;
    }

    public int getAmpartoBalancoDiferencaMaximaSomaCruzada() {
        return amparoBalancoDiferencaMaximaSomaCruzada;
    }

    public int getAmpartoBalancoDesgasteConcavoDaChapa() {
        return amparoBalancoDesgasteConcavoDaChapa;
    }

    public int getAlturaEngateMetricaVazioAltMin() {
        return alturaEngateMetricaVazioAltMin;
    }

    public int getAlturaEngateMetricaVazioAltMax() {
        return alturaEngateMetricaVazioAltMax;
    }

    public int getAlturaEngateMetricaCarregadoAltMin() {
        return alturaEngateMetricaCarregadoAltMin;
    }

    public int getAlturaEngateMetricaCarregadoAltMax() {
        return alturaEngateMetricaCarregadoAltMax;
    }

    public int getAlturaEngateLargaCarregadoAltMin() {
        return alturaEngateLargaCarregadoAltMin;
    }

    public int getAlturaEngateLargaCarregadoAltMax() {
        return alturaEngateLargaCarregadoAltMax;
    }

    public String getLimiteDeMolasMola() {
        return limiteDeMolasMola;
    }

    public int getLimiteDeMolasExtMax() {
        return limiteDeMolasExtMax;
    }

    public int getLimiteDeMolasExtMin() {
        return limiteDeMolasExtMin;
    }

    public int getLimiteDeMolasIntMax() {
        return limiteDeMolasIntMax;
    }

    public int getLimiteDeMolasIntMin() {
        return limiteDeMolasIntMin;
    }

    public int getLimiteDeMolasIntermMax() {
        return limiteDeMolasIntermMax;
    }

    public int getLimiteDeMolasIntermMin() {
        return limiteDeMolasIntermMin;
    }

    public String getMolaCunhaTipoDeMola() {
        return molaCunhaTipoDeMola;
    }

    public int getMolaCunhaExtMax() {
        return molaCunhaExtMax;
    }

    public int getMolaCunhaExtMin() {
        return molaCunhaExtMin;
    }

    public int getMolaCunhaIntMax() {
        return molaCunhaIntMax;
    }

    public int getMolaCunhaIntMin() {
        return molaCunhaIntMin;
    }

    public int getAlturaEngateLargaVazioAltMin() {
        return alturaEngateLargaVazioAltMin;
    }

    public int getAlturaEngateLargaVazioAltMax() {
        return alturaEngateLargaVazioAltMax;
    }
}
