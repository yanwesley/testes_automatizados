package br.com.api.model.sinistro;


public class RodeirosPorTipo {
    public Integer quantidade;
    public boolean editavel;

    public RodeirosPorTipo(Integer quantidade, boolean editavel) {
        this.quantidade = quantidade;
        this.editavel = editavel;
    }
}
