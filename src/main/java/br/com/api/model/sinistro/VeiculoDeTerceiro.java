package br.com.api.model.sinistro;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static br.com.api.GeradorDePlaca.generateLicensePlate;

public class VeiculoDeTerceiro {
    private String marca;
    private String modelo;
    private String placa;

    public VeiculoDeTerceiro() {
        setMarca();
        setModelo();
        this.placa = generateLicensePlate();
    }

    public String getMarca() {
        return marca;
    }

    private void setMarca() {
        List<String> listMarca = new ArrayList<>();
        listMarca.add("Toyota");
        listMarca.add("BMW");
        listMarca.add("Mercedes - Benz");
        listMarca.add("Honda");
        listMarca.add("Ford");
        listMarca.add("Fiat");
        listMarca.add("VW");
        this.marca = listMarca.get(ThreadLocalRandom.current().nextInt(0, listMarca.size() - 1));
    }

    public String getModelo() {
        return modelo;
    }

    private void setModelo() {
        List<String> listModelo = new ArrayList<>();
        listModelo.add("Agile");
        listModelo.add("Astra Hatch");
        listModelo.add("Cruze");
        listModelo.add("Zafira");
        listModelo.add("Montana");
        listModelo.add("Spin");
        listModelo.add("Trailblazer");
        this.modelo = listModelo.get(ThreadLocalRandom.current().nextInt(0, listModelo.size() - 1));
    }

    public String getPlaca() {
        return placa;
    }

}
