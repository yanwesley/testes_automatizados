package br.com.api.model.sinistro;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static br.com.api.GeradorDePlaca.generateDigits;

public class ResponsavelAcionado {

    private String nome;
    private String cargo;
    private String matricula;
    private String data;
    private String hora;
    private String telefone;

    public ResponsavelAcionado(boolean editado) {
        setNome();
        setCargo();
        setMatricula();
        setHora();
        setTelefone();
        setData(editado);
    }

    private void setHora() {
        final String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
        this.hora = time;
    }

    private void setTelefone() {
        final String fone = "(41) " + generateDigits(5) + "-" + generateDigits(4);
        this.telefone = fone;
    }

    private void setNome() {
        this.nome = "João";
    }

    //   TODO - Veirificar melhor forma de mapear o cargo.
    private void setCargo() {
        this.cargo = "";
    }

    private void setMatricula() {
        this.matricula = "275329";
    }

    public String getData() {
        return data;
    }

    private void setData(boolean editado) {
        if (editado) {
            this.data = LocalDate.now().plusDays(2).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        } else {
            this.data = LocalDate.now().plusDays(1).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        }
    }

    public String getHora() {
        return hora;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getNome() {
        return nome;
    }

    public String getCargo() {
        return cargo;
    }

    public String getMatricula() {
        return matricula;
    }

}
