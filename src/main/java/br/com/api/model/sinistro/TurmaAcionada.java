package br.com.api.model.sinistro;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.*;

public class TurmaAcionada {
    private Faker faker;
    private String recurso;
    private int quantidade;
    private String origem;
    private String horaPrevista;
    private String horaLocal;
    private String ladoEntrada;
    private String empresa;
    private String responsavel;
    private String telefoneDoResponsavel;
    private String observacao;

    public TurmaAcionada() {
        faker = new Faker(new Locale("pt-BR"));
        recurso = getRandomCharacters(60);
        quantidade = getRandom(null);
        origem = getRandomCharacters(255);
        horaPrevista = getTimeNow();
        horaLocal = formatTime(getTime().plusHours(1));
        ladoEntrada = getRandomCharacters(60);
        empresa = getRandomCharacters(60);
        responsavel = getRandomCharacters(60);
        telefoneDoResponsavel = "(41) 99999-9999";
        observacao = getRandomCharacters(4000);
    }

    public String getLadoEntrada() {
        return ladoEntrada;
    }

    public String getRecurso() {
        return recurso;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public String getOrigem() {
        return origem;
    }

    public String getHoraPrevista() {
        return horaPrevista;
    }

    public String getHoraLocal() {
        return horaLocal;
    }

    public String getEmpresa() {
        return empresa;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public String getTelefoneDoResponsavel() {
        return telefoneDoResponsavel;
    }

    public String getObservacao() {
        return observacao;
    }

}
