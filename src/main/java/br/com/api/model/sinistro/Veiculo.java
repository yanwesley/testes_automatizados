package br.com.api.model.sinistro;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static br.com.api.GeradorDePlaca.generateDigits;

public class Veiculo {

    private Faker faker;

    private int posicao;
    private String numero;
    private int indexTipo;
    private String tipoValue;
    private int modeloIndex;
    private String modelo;
    private int mercadoriaIndex;
    private String mercadoria;
    private String perdaValue;
    private boolean perdaProduto;
    private int situacaoIndex;
    private String situacaoValue;
    private int qtdRodeiros;
    private String icone;
    private List<TipoVeiculo> tipoVeiculosList = new ArrayList<>();

    public Veiculo() {
        faker = new Faker(new Locale("pt-BR"));
        posicao = faker.random().nextInt(100, 200);
        numero = generateDigits(30);
        indexTipo = faker.random().nextInt(2);
        modelo = "A";
        modeloIndex = faker.random().nextInt(1);
        mercadoria = "A";
        mercadoriaIndex = faker.random().nextInt(1);
        situacaoIndex = faker.random().nextInt(6);
        perdaValue = "Sim";
        perdaProduto = true;
        tipoVeiculosList.add(new TipoVeiculo("Locomotiva", "Ícone - Locomotiva - Alerta"));
        tipoVeiculosList.add(new TipoVeiculo("Vagão", "Ícone - Vagão - Alerta"));
        tipoVeiculosList.add(new TipoVeiculo("Veículo de Via", "Ícone - Veículo de Via - Alerta"));
    }

    public boolean isPerdaProduto() {
        return perdaProduto;
    }

    public int getModeloIndex() {
        return modeloIndex;
    }

    public int getMercadoriaIndex() {
        return mercadoriaIndex;
    }

    public List<TipoVeiculo> getTipoVeiculosList() {
        return tipoVeiculosList;
    }

    public Integer getQtdRodeiros() {
        Rodeiros rodeiros = new Rodeiros();
        qtdRodeiros = rodeiros.getRodeiros(tipoValue, situacaoValue).quantidade;
        return qtdRodeiros;
    }

    public String getIcone() {
        switch (getTipoValue()) {
            case "Vagão":
                this.icone = "Ícone - Vagão - Alerta";
                break;
            case "Locomotiva":
                this.icone = "Ícone - Locomotiva - Alerta";
                break;
            case "Veículo de Via":
                this.icone = "Ícone - Veículo de Via - Alerta";
                break;
            default:
                this.icone = "";
                break;
        }
        return icone;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public int getIndexTipo() {
        return indexTipo;
    }

    public int getSituacaoIndex() {
        return situacaoIndex;
    }

    public String getPerdaValue() {
        return perdaValue;
    }


    public String getNumero() {
        return numero;
    }

    public String getTipoValue() {
        return tipoValue;
    }

    public void setTipoValue(String tipoValue) {
        this.tipoValue = tipoValue;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMercadoria() {
        return mercadoria;
    }

    public void setMercadoria(String mercadoria) {
        this.mercadoria = mercadoria;
    }

    public String getSituacaoValue() {
        return situacaoValue;
    }

    public void setSituacaoValue(String situacaoValue) {
        this.situacaoValue = situacaoValue;
    }

}
