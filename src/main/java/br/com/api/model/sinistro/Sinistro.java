package br.com.api.model.sinistro;

import com.github.javafaker.Faker;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static br.com.api.GeradorDeMassa.*;
import static br.com.api.GeradorDePlaca.generateLetters;

public class Sinistro {

    final private String hora;
    private Faker faker;
    private int idSinistro;
    private int tipoIndex;
    private int subdivisaoIndex;
    private int produtoIndex;
    private String data;
    private String dataPrevisaoLiberacao;
    private String horaOS;
    private String dateOS;
    private String tipo;
    private int extensaoDaViaDanificada;
    private boolean interdicaoDaCirculacao;
    private int secaoBloqueioSB;
    private String km;
    private String subdivisao;
    private String prefixo;
    private String numeroOS;
    private String taraBrutaTB;
    private int comprimento;
    private String produto;
    private boolean condutorOuMaquinistaDaRumo;
    private ResponsavelAcionado condutorDados;
    private String relato;
    private boolean existemVitimasEnvolvidas;
    private int vitimasFatais;
    private int vitimasGraves;
    private int vitimasLeves;
    private boolean existemVeiculosDeTerceiros;
    private VeiculoDeTerceiro veiculoDeTerceiro;
    private boolean incendioOuExplosaoComDestruicaoTotal;
    private boolean houveDanoAmbiental;
    private boolean vazamentoDeCombustivelDaLocomotiva;
    private boolean vazamentoDeProdutoEmRiosELagos;
    private boolean houveOutrosImpactosAPopulacao;
    private boolean pnTrancadaPorMaisDe1HoraEmPerimetroUrbano;
    private boolean tombamentoQueDanificouEdificacoesDeTerceiros;
    private String status;
    private int unidadeProducaoIndex;
    private String unidadeProducao;
    private String classificacao;
    private String qualEAOperacaoValue;
    private int qualEAOperacaoIndex;
    private String qualEOComplexoValue;
    private int qualEOComplexoIndex;
    private String nomeCondutor;
    private String documentoCondutor;

    public Sinistro() {
        faker = new Faker(new Locale("pt-BR"));
        data = getDay();
        hora = LocalDateTime.now().minusHours(2).format(DateTimeFormatter.ofPattern("HH:mm"));
        dataPrevisaoLiberacao = LocalDateTime.now().plusHours(10).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
        extensaoDaViaDanificada = getRandom(null);
        interdicaoDaCirculacao = false;
        tipoIndex = 1;
        secaoBloqueioSB = getRandom(null);
        km = "10,111";
        subdivisaoIndex = getRandom(2);
        prefixo = generateLetters(3);
//        TODO Para não carregar a ficha do trem quando for manual.
//        numeroOS = String.valueOf(getRandom(null));
        numeroOS = "";
        taraBrutaTB = "13,12";
        comprimento = getRandom(null);
        produtoIndex = getRandom(2);
        condutorOuMaquinistaDaRumo = faker.bool().bool();
        condutorDados = getResponsavelAcionado();
        relato = faker.lorem().characters(1, 4000);
        existemVitimasEnvolvidas = true;
        vitimasFatais = getRandom(null);
        vitimasGraves = getRandom(null);
        vitimasLeves = getRandom(null);
        existemVeiculosDeTerceiros = true;
        veiculoDeTerceiro = new VeiculoDeTerceiro();
        incendioOuExplosaoComDestruicaoTotal = true;
        houveDanoAmbiental = true;
        vazamentoDeCombustivelDaLocomotiva = true;
        vazamentoDeProdutoEmRiosELagos = true;
        houveOutrosImpactosAPopulacao = false;
        pnTrancadaPorMaisDe1HoraEmPerimetroUrbano = false;
        tombamentoQueDanificouEdificacoesDeTerceiros = true;
        unidadeProducaoIndex = getRandom(4);
        classificacao = faker.options().option("ACIDENTE", "OCORRENCIA");
        qualEAOperacaoIndex = faker.options().option(0, 1);
        nomeCondutor = getRandomCharacters(255);
        documentoCondutor = getRandomCharacters(30);
        qualEOComplexoIndex = faker.options().option(0, 1);
    }

    public String getQualEAOperacaoValue() {
        return qualEAOperacaoValue;
    }

    public void setQualEAOperacaoValue(String qualEAOperacaoValue) {
        this.qualEAOperacaoValue = qualEAOperacaoValue;
    }

    public int getQualEAOperacaoIndex() {
        return qualEAOperacaoIndex;
    }

    public String getQualEOComplexoValue() {
        return qualEOComplexoValue;
    }

    public void setQualEOComplexoValue(String qualEOComplexoValue) {
        this.qualEOComplexoValue = qualEOComplexoValue;
    }

    public int getQualEOComplexoIndex() {
        return qualEOComplexoIndex;
    }

    public String getDataPrevisaoLiberacao() {
        return dataPrevisaoLiberacao;
    }

    public String getDateOS() {
        return dateOS;
    }

    public void setDateOS(String dateOS) {
        this.dateOS = dateOS;
    }

    public int getIdSinistro() {
        return idSinistro;
    }

    public void setIdSinistro(int idSinistro) {
        this.idSinistro = idSinistro;
    }

    public int getUnidadeProducaoIndex() {
        return unidadeProducaoIndex;
    }

    public String getUnidadeProducao() {
        return unidadeProducao;
    }

    public void setUnidadeProducao(String unidadeProducao) {
        this.unidadeProducao = unidadeProducao;
    }

    public ResponsavelAcionado getCondutorDados() {
        return condutorDados;
    }

    public boolean isTombamentoQueDanificouEdificacoesDeTerceiros() {
        return tombamentoQueDanificouEdificacoesDeTerceiros;
    }

    public boolean isHouveDanoAmbiental() {
        return houveDanoAmbiental;
    }

    public boolean isVazamentoDeCombustivelDaLocomotiva() {
        return vazamentoDeCombustivelDaLocomotiva;
    }

    public boolean isVazamentoDeProdutoEmRiosELagos() {
        return vazamentoDeProdutoEmRiosELagos;
    }

    public boolean isHouveOutrosImpactosAPopulacao() {
        return houveOutrosImpactosAPopulacao;
    }

    public boolean isPnTrancadaPorMaisDe1HoraEmPerimetroUrbano() {
        return pnTrancadaPorMaisDe1HoraEmPerimetroUrbano;
    }

    public int getTipoIndex() {
        return tipoIndex;
    }

    public int getSubdivisaoIndex() {
        return subdivisaoIndex;
    }

    public int getProdutoIndex() {
        return produtoIndex;
    }

    public boolean isExistemVeiculosDeTerceiros() {
        return existemVeiculosDeTerceiros;
    }

    public VeiculoDeTerceiro getVeiculoDeTerceiro() {
        return veiculoDeTerceiro;
    }

    public boolean isIncendioOuExplosaoComDestruicaoTotal() {
        return incendioOuExplosaoComDestruicaoTotal;
    }

    public String getDate() {
        return data;
    }

    public String getHora() {
        return hora;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHoraOS() {
        return horaOS;
    }

    public void setHoraOS(String horaOS) {
        this.horaOS = horaOS;
    }

    public int getExtensaoDaViaDanificada() {
        return extensaoDaViaDanificada;
    }

    public boolean isInterdicaoDaCirculacao() {
        return interdicaoDaCirculacao;
    }

    public int getSecaoBloqueioSB() {
        return secaoBloqueioSB;
    }

    public String getKm() {
        return km;
    }

    public String getSubdivisao() {
        return subdivisao;
    }

    public void setSubdivisao(String subdivisao) {
        this.subdivisao = subdivisao;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public String getNumeroOS() {
        return numeroOS;
    }

    public void setNumeroOS(String numeroOS) {
        this.numeroOS = numeroOS;
    }

    public String getTaraBrutaTB() {
        return taraBrutaTB;
    }

    public int getComprimento() {
        return comprimento;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public boolean isCondutorOuMaquinistaDaRumo() {
        return condutorOuMaquinistaDaRumo;
    }

    public String getRelato() {
        return relato;
    }

    public boolean isExistemVitimasEnvolvidas() {
        return existemVitimasEnvolvidas;
    }

    public int getVitimasFatais() {
        return vitimasFatais;
    }

    public int getVitimasGraves() {
        return vitimasGraves;
    }

    public int getVitimasLeves() {
        return vitimasLeves;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public String getNomeCondutor() {
        return nomeCondutor;
    }

    public String getDocumentoCondutor() {
        return documentoCondutor;
    }
}
