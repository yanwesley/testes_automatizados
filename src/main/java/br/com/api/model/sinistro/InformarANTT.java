package br.com.api.model.sinistro;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class InformarANTT {

    private String ferrovia;
    private String acidentePN;
    private String perimetroUrb;
    private String trecho;
    private String natureza;
    private String motivo;
    private String causaProvavel;
    private String relato;

    public InformarANTT() {
        trecho = getRandomCharacters(20);
    }

    public String getFerrovia() {
        return ferrovia;
    }

    public void setFerrovia(String ferrovia) {
        this.ferrovia = ferrovia;
    }

    public String getAcidentePN() {
        return acidentePN;
    }

    public void setAcidentePN(String acidentePN) {
        this.acidentePN = acidentePN;
    }

    public String getPerimetroUrb() {
        return perimetroUrb;
    }

    public void setPerimetroUrb(String perimetroUrb) {
        this.perimetroUrb = perimetroUrb;
    }

    public String getTrecho() {
        return trecho;
    }

    public void setTrecho(String trecho) {
        this.trecho = trecho;
    }

    public String getNatureza() {
        return natureza;
    }

    public void setNatureza(String natureza) {
        this.natureza = natureza;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getCausaProvavel() {
        return causaProvavel;
    }

    public void setCausaProvavel(String causaProvavel) {
        this.causaProvavel = causaProvavel;
    }

    public String getRelato() {
        return relato;
    }

    public void setRelato(String relato) {
        this.relato = relato;
    }

}
