package br.com.api.model.sinistro;

public class TipoVeiculo {
    public String tipo;
    public String icone;

    public TipoVeiculo(String tipo, String icone) {
        this.tipo = tipo;
        this.icone = icone;
    }
}
