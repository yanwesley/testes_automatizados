package br.com.api.model.sinistro;

public class Rodeiros {

    public RodeirosPorTipo getRodeiros(String tipo, String situacao) {
        switch (tipo) {
            case "Veículo de Via":
            case "Vagão": {
                return getRodeirosTipo(situacao, 4);
            }
            default: {
                return getRodeirosTipo(situacao, 8);
            }
        }
    }

    private RodeirosPorTipo getRodeirosTipo(String situacao, Integer quantidadeRodeiros) {
        if (situacao.equals("Tombado")
                || situacao.equals("Perda Total")
                || situacao.equals("Incendiado")
        ) {
            return new RodeirosPorTipo(quantidadeRodeiros, false);
        }
        if (situacao.equals("Descarrilado")) {
            return new RodeirosPorTipo(0, true);
        }

        if (situacao.equals("Semi-Tombado")) {
            return new RodeirosPorTipo(quantidadeRodeiros, true);
        }

        if (situacao.equals("Envolvido")) {
            return new RodeirosPorTipo(0, false);
        }

        return null;
    }
}
