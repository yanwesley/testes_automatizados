package br.com.api.model.sinistro;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.*;

public class MaquinaAcionada {
    private Faker faker;
    private String recurso;
    private int tamanho;
    private String origem;
    private String horaPrevista;
    private String horaLocal;
    private String empresa;
    private String responsavel;
    private String telefoneDoResponsavel;
    private String observacao;

    public MaquinaAcionada() {
        faker = new Faker(new Locale("pt-BR"));
        recurso = getRandomCharacters(60);
        tamanho = getRandom(null);
        origem = faker.address().fullAddress();
        horaPrevista = getTimeNow();
        horaLocal = formatTime(getTime().plusHours(1));
        empresa = getRandomCharacters(60);
        responsavel = getRandomCharacters(60);
        telefoneDoResponsavel = "(41) 99999-9999";
        observacao = getRandomCharacters(4000);
    }

    public String getRecurso() {
        return recurso;
    }

    public int getTamanho() {
        return tamanho;
    }

    public String getOrigem() {
        return origem;
    }

    public String getHoraPrevista() {
        return horaPrevista;
    }

    public String getHoraLocal() {
        return horaLocal;
    }

    public String getEmpresa() {
        return empresa;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public String getTelefoneDoResponsavel() {
        return telefoneDoResponsavel;
    }

    public String getObservacao() {
        return observacao;
    }

}
