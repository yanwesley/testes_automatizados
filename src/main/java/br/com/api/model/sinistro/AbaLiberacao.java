package br.com.api.model.sinistro;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AbaLiberacao {
    private String timeFormat = "HH:mm";
    private String dateFormat = "dd/MM/yyyy";

    private String inputHorarioLiberacao;
    private String inputDataLiberacao;
    private String inputDataLiberacaoParcial;
    private String inputHorarioLiberacaoParcial;
    private String inputDataLiberacaoMecanica;
    private String inputHorarioLiberacaoMecanica;
    private String inputDataLiberacaoVia;
    private String inputHorarioLiberacaoVia;
    private String inputDataLiberacaoFinal;
    private String inputHorarioLiberacaoFinal;

    public AbaLiberacao() {
        inputHorarioLiberacao = LocalDateTime.now().format(DateTimeFormatter.ofPattern(timeFormat));
        inputDataLiberacao = LocalDate.now().format(DateTimeFormatter.ofPattern(dateFormat));
        inputDataLiberacaoParcial = LocalDate.now().format(DateTimeFormatter.ofPattern(dateFormat));
        inputHorarioLiberacaoParcial = LocalDateTime.now().format(DateTimeFormatter.ofPattern(timeFormat));
        inputDataLiberacaoMecanica = LocalDate.now().format(DateTimeFormatter.ofPattern(dateFormat));
        inputHorarioLiberacaoMecanica = LocalDateTime.now().format(DateTimeFormatter.ofPattern(timeFormat));
        inputDataLiberacaoVia = LocalDate.now().format(DateTimeFormatter.ofPattern(dateFormat));
        inputHorarioLiberacaoVia = LocalDateTime.now().format(DateTimeFormatter.ofPattern(timeFormat));
        inputDataLiberacaoFinal = LocalDate.now().format(DateTimeFormatter.ofPattern(dateFormat));
        inputHorarioLiberacaoFinal = LocalDateTime.now().format(DateTimeFormatter.ofPattern(timeFormat));
    }

    public String getInputDataPrevisaoLiberacao() {
        return inputDataLiberacao;
    }

    public String getInputHorarioPrevisaoLiberacao() {
        return inputHorarioLiberacao;
    }

    public String getInputDataLiberacaoParcial() {
        return inputDataLiberacaoParcial;
    }

    public String getInputHorarioLiberacaoParcial() {
        return inputHorarioLiberacaoParcial;
    }

    public String getInputDataLiberacaoMecanica() {
        return inputDataLiberacaoMecanica;
    }

    public String getInputHorarioLiberacaoMecanica() {
        return inputHorarioLiberacaoMecanica;
    }

    public String getInputHorarioLiberacaoVia() {
        return inputHorarioLiberacaoVia;
    }

    public String getInputDataLiberacaoVia() {
        return inputDataLiberacaoVia;
    }

    public String getInputDataLiberacaoFinal() {
        return inputDataLiberacaoFinal;
    }

    public String getInputHorarioLiberacaoFinal() {
        return inputHorarioLiberacaoFinal;
    }

}
