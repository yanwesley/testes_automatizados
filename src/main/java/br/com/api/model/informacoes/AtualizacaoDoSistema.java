package br.com.api.model.informacoes;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.*;

public class AtualizacaoDoSistema {
    private Faker faker;
    private String versao;
    private String descricao;
    private String data;
    private String realizadoPor;
    private String documentacaoOficialDoProjeto;

    public AtualizacaoDoSistema() {
        faker = new Faker(new Locale("pt-BR"));
        versao = faker.name().name();
        descricao = getRandomCharacters(256);
        data = getDayMinus(1);
        realizadoPor = getRandomCharacters(256);
        documentacaoOficialDoProjeto = getHTMLText();
    }

    public String getVersao() {
        return versao;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getData() {
        return data;
    }

    public String getRealizadoPor() {
        return realizadoPor;
    }

    public String getDocumentacaoOficialDoProjeto() {
        return documentacaoOficialDoProjeto;
    }
}
