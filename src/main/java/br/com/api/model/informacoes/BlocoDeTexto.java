package br.com.api.model.informacoes;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getHTMLText;

public class BlocoDeTexto {
    private Faker faker;
    private String titulo;
    private String descricao;

    public BlocoDeTexto() {
        faker = new Faker(new Locale("pt-BR"));
        titulo = faker.name().name();
        descricao = getHTMLText();
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescricao() {
        return descricao;
    }


}
