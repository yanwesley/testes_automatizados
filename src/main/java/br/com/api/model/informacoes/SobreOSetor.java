package br.com.api.model.informacoes;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getHTMLText;
import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class SobreOSetor {
    private Faker faker;
    private String sobreSetor;
    private String telefoneDoSetor;
    private String localizacaoFisica;

    public SobreOSetor() {
        faker = new Faker(new Locale("pt-BR"));
        sobreSetor = getHTMLText();
        telefoneDoSetor = faker.options().option("(12) 31231-2312", "(43) 99999-4949");
        localizacaoFisica = getRandomCharacters(256);
    }

    public String getSobreSetor() {
        return sobreSetor;
    }

    public String getTelefoneDoSetor() {
        return telefoneDoSetor;
    }

    public String getLocalizacaoFisica() {
        return localizacaoFisica;
    }

}
