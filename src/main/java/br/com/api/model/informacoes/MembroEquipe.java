package br.com.api.model.informacoes;

import com.github.javafaker.Faker;

import java.util.Locale;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class MembroEquipe {

    private Faker faker;
    private boolean exibirDadosEquipe;
    private String nome;
    private String cargo;
    private String email;
    private String telefone;

    public MembroEquipe() {
        faker = new Faker(new Locale("pt-BR"));
        nome = getRandomCharacters(256);
        cargo = getRandomCharacters(256);
        email = faker.options().option("emailtest1@email.com", "emailfake@email.com", "faketeste@email.com");
        telefone = faker.options().option("(41) 15846-2548", "(11) 82468-1547", "(41) 98765-4321");
        exibirDadosEquipe = faker.bool().bool();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public boolean isExibirDadosEquipe() {
        return exibirDadosEquipe;
    }
}
