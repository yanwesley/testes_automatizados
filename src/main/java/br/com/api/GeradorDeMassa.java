package br.com.api;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.api.model.configuracoes.ConfiguracaoMaterialRodanteLocomotiva;
import br.com.api.model.configuracoes.ConfiguracaoMaterialRodanteVagao;
import br.com.api.model.configuracoes.ConfiguracaoVia;
import br.com.api.model.configuracoes.ConfiguracoesDashboard;
import br.com.api.model.incidente.Incidente;
import br.com.api.model.informacoes.AtualizacaoDoSistema;
import br.com.api.model.informacoes.BlocoDeTexto;
import br.com.api.model.informacoes.MembroEquipe;
import br.com.api.model.informacoes.SobreOSetor;
import br.com.api.model.sindicancia.*;
import br.com.api.model.sindicancia.investigacao.*;
import br.com.api.model.sindicancia.questionario.QuestionarioCheckListAcidente;
import br.com.api.model.sindicancia.questionario.QuestionarioManobrador;
import br.com.api.model.sindicancia.questionario.QuestionarioMaquinista;
import br.com.api.model.sindicancia.sobre_os_envolvidos.VeiculoEnvolvido;
import br.com.api.model.sindicancia.sobre_os_envolvidos.VitimaEnvolvida;
import br.com.api.model.sinistro.*;
import org.apache.commons.lang.RandomStringUtils;

import javax.annotation.Nullable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class GeradorDeMassa {

    private static final int LIMIT_RANDOM = 99999999;
    public static String featureName;
    private static VeiculoDeTerceiro veiculoDeTerceiro;
    private static Veiculo veiculo;
    private static ResponsavelAcionado responsavelAcionado;
    private static Sinistro sinistro;
    private static MaquinaAcionada maquinaAcionada;
    private static TurmaAcionada turmaAcionada;
    private static AbaLiberacao abaLiberacao;
    private static DadosVia dadosVia;
    private static TabelaAnomalias tabelaAnomalias;
    private static Flambagem flambagem;
    private static CustoMaterial custoMaterial;
    private static CustoMaquina custoMaquina;
    private static CustoOutros custoOutros;
    private static CustoTransporte custoTransporte;
    private static CustoColaborador custoColaborador;
    private static CustoLocomotiva custoLocomotiva;
    private static CustoVagoes custoVagoes;
    private static CustoVeiculosVia custoVeiculosVia;
    private static AMV amv;
    private static Trilho trilho;
    private static SobreOAMV sobreOAMV;
    private static Estacionamento estacionamento;
    private static FormacaoDoTrem formacaoDoTrem;
    private static Cargas cargas;
    private static DadosDoSupervisorResponsavel dadosDoSupervisorResponsavel;
    private static DadosDoMaquinistaCondutor dadosDoMaquinistaCondutor;
    private static DadosDoManobradorAjudante dadosDoManobradorAjudante;
    private static QuestionarioMaquinista questionarioMaquinista;
    private static AnaliseDeViagem analiseDeViagem;
    private static CriticaDeViagem criticaDeViagem;
    private static QuestionarioCheckListAcidente questionarioCheckListAcidente;
    private static QuestionarioManobrador questionarioManobrador;
    private static RegistroDoAcidente registroDoAcidente;
    private static VitimaEnvolvida vitimaEnvolvida;
    private static VeiculoEnvolvido veiculoEnvolvido;
    private static SobreOLocal sobreOLocal;
    private static RelatorioFinal relatorioFinal;
    private static String descricao;
    private static String conclusao;
    private static String comentario4000Char;
    private static AnaliseDeCausa analiseDeCausa;
    private static PlanoDeAcao planoDeAcao;
    private static ResponsavelAcao responsavelAcao;
    private static ResponsavelAcao responsavelAcaoEditado;
    private static Acao acao;
    private static Vagao vagao;
    private static MedidaDeRodas medidaDeRodas;
    private static Incidente incidente;
    private static SobreOSetor sobreOSetor;
    private static MembroEquipe membroEquipe;
    private static AtualizacaoDoSistema atualizacaoDoSistema;
    private static ConfiguracoesDashboard configuracoesDashboard;
    private static BlocoDeTexto blocoDeTexto;
    private static ConfiguracaoMaterialRodanteLocomotiva configuracaoMaterialRodanteLocomotiva;
    private static ConfiguracaoVia configuracaoVia;
    private static ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao;
    private static Financeiro financeiro;

    public GeradorDeMassa() {
        getMaquinaAcionada();
        getResponsavelAcionado();
        getVeiculoDeTerceiro();
        getVeiculo();
        getSinistro();
        getAbaLiberacao();
        getDadosVia();
        getFlambagem();
        getCustoMaterial();
        getCustoMaquina();
        getCustoOutros();
        getCustoTransporte();
        getCustoColaborador();
        getAMV();
        getTrilho();
        getSobreOAMV();
        getFormacaoDoTrem();
        getCargas();
        getDadosDoSupervisorResponsavel();
        getDadosDoMaquinistaCondutor();
        getDadosDoManobradorAjudante();
        getAnaliseDeViagem();
        getCriticaDeViagem();
        getQuestionarioMaquinista();
        getQuestionarioCheckListAcidente();
        getQuestionarioManobrador();
        getRegistroDoAcidente();
        getSobreOLocal();
        getVitimaEnvolvida();
        getVeiculoEnvolvido();
        getRelatorioFinal();
        getDescricao();
        getConclusao();
        getComentario4000Char();
        getPlanoDeAcao();
        getResponsavelAcao();
        getAcao();
        getVagao();
        getResponsavelAcaoEditado();
        getIncidente();
        getMedidaDeRodas();
        getSobreOSetor();
        getMembroEquipe();
        getAtualizacaoDoSistema();
        getConfiguracoesDashboard();
        getBlocoDeTexto();
        getConfiguracaoMaterialRodanteLocomotiva();
        getConfiguracaoVia();
        getFinanceiro();
    }

    public static void updateValuesMassas() {
        veiculoDeTerceiro = null;
        veiculo = null;
        responsavelAcionado = null;
        sinistro = null;
        maquinaAcionada = null;
        turmaAcionada = null;
        abaLiberacao = null;
        dadosVia = null;
        tabelaAnomalias = null;
        flambagem = null;
        custoMaterial = null;
        custoMaquina = null;
        custoOutros = null;
        custoTransporte = null;
        custoColaborador = null;
        amv = null;
        trilho = null;
        sobreOAMV = null;
        estacionamento = null;
        formacaoDoTrem = null;
        cargas = null;
        dadosDoSupervisorResponsavel = null;
        dadosDoMaquinistaCondutor = null;
        dadosDoManobradorAjudante = null;
        questionarioMaquinista = null;
        analiseDeViagem = null;
        criticaDeViagem = null;
        questionarioCheckListAcidente = null;
        questionarioManobrador = null;
        registroDoAcidente = null;
        vitimaEnvolvida = null;
        veiculoEnvolvido = null;
        sobreOLocal = null;
        relatorioFinal = null;
        descricao = null;
        conclusao = null;
        comentario4000Char = null;
        analiseDeCausa = null;
        planoDeAcao = null;
        responsavelAcao = null;
        responsavelAcaoEditado = null;
        acao = null;
        incidente = null;
        vagao = null;
        medidaDeRodas = null;
        sobreOSetor = null;
        atualizacaoDoSistema = null;
        blocoDeTexto = null;
        configuracaoMaterialRodanteLocomotiva = null;
        configuracaoVia = null;
        configuracaoMaterialRodanteVagao = null;
        financeiro = null;
    }

    public static Financeiro getNewFinanceiro() {
        return new Financeiro();
    }

    public static Financeiro getFinanceiro() {
        if (financeiro == null) {
            financeiro = getNewFinanceiro();
        }
        return financeiro;
    }

    public static ConfiguracaoMaterialRodanteVagao getNewConfiguracaoMaterialRodanteVagao() {
        return new ConfiguracaoMaterialRodanteVagao();
    }

    public static ConfiguracaoMaterialRodanteVagao getConfiguracaoMaterialRodanteVagao() {
        if (configuracaoMaterialRodanteVagao == null) {
            configuracaoMaterialRodanteVagao = getNewConfiguracaoMaterialRodanteVagao();
        }
        return configuracaoMaterialRodanteVagao;
    }

    public static ConfiguracaoVia getNewConfiguracaoVia() {
        return new ConfiguracaoVia();
    }

    public static ConfiguracaoVia getConfiguracaoVia() {
        if (configuracaoVia == null) {
            configuracaoVia = getNewConfiguracaoVia();
        }
        return configuracaoVia;
    }

    public static BlocoDeTexto getNewBlocoDeTexto() {
        return new BlocoDeTexto();
    }

    public static BlocoDeTexto getBlocoDeTexto() {
        if (blocoDeTexto == null) {
            blocoDeTexto = getNewBlocoDeTexto();
        }
        return blocoDeTexto;
    }

    public static ConfiguracaoMaterialRodanteLocomotiva getNewConfiguracaoMaterialRodanteLocomotiva() {
        return new ConfiguracaoMaterialRodanteLocomotiva();
    }

    public static ConfiguracaoMaterialRodanteLocomotiva getConfiguracaoMaterialRodanteLocomotiva() {
        if (configuracaoMaterialRodanteLocomotiva == null) {
            configuracaoMaterialRodanteLocomotiva = getNewConfiguracaoMaterialRodanteLocomotiva();
        }
        return configuracaoMaterialRodanteLocomotiva;
    }

    public static AtualizacaoDoSistema getNewAtualizacaoDoSistema() {
        return new AtualizacaoDoSistema();
    }

    public static AtualizacaoDoSistema getAtualizacaoDoSistema() {
        if (atualizacaoDoSistema == null) {
            atualizacaoDoSistema = getNewAtualizacaoDoSistema();
        }
        return atualizacaoDoSistema;
    }

    public static SobreOSetor getNewSobreOSetor() {
        return new SobreOSetor();
    }

    public static SobreOSetor getSobreOSetor() {
        if (sobreOSetor == null) {
            sobreOSetor = getNewSobreOSetor();
        }
        return sobreOSetor;
    }

    public static Vagao getNewVagao() {
        return new Vagao();
    }

    public static Vagao getVagao() {
        if (vagao == null) {
            vagao = getNewVagao();
        }
        return vagao;
    }

    public static Acao getNewAcao() {
        return new Acao();
    }

    public static Acao getAcao() {
        if (acao == null) {
            acao = getNewAcao();
        }
        return acao;
    }

    public static MedidaDeRodas getNewMedidaDeRodas() {
        return new MedidaDeRodas();
    }

    public static MedidaDeRodas getMedidaDeRodas() {
        if (medidaDeRodas == null) {
            medidaDeRodas = getNewMedidaDeRodas();
        }
        return medidaDeRodas;
    }

    public static ResponsavelAcao getNewResponsavelAcao() {
        return new ResponsavelAcao();
    }

    public static ResponsavelAcao getResponsavelAcao() {
        if (responsavelAcao == null) {
            responsavelAcao = getNewResponsavelAcao();
        }
        return responsavelAcao;
    }

    public static ResponsavelAcao getResponsavelAcaoEditado() {
        if (responsavelAcaoEditado == null) {
            responsavelAcaoEditado = getNewResponsavelAcao();
        }
        return responsavelAcaoEditado;
    }

    public static PlanoDeAcao getNewPlanoDeAcao() {
        return new PlanoDeAcao();
    }

    public static PlanoDeAcao getPlanoDeAcao() {
        if (planoDeAcao == null) {
            planoDeAcao = getNewPlanoDeAcao();
        }
        return planoDeAcao;
    }

    public static String getNewComentario4000Char() {
        return getRandomCharacters(4000);
    }

    public static String getComentario4000Char() {
        if (comentario4000Char == null) {
            comentario4000Char = getNewComentario4000Char();
        }
        return comentario4000Char;
    }

    public static String getNewDescricao() {
        return getRandomCharacters(2000);
    }

    public static String getDescricao() {
        if (descricao == null) {
            descricao = getNewDescricao();
        }
        return descricao;
    }

    public static String getNewConclusao() {
        return getRandomCharacters(2000);
    }

    public static String getConclusao() {
        if (conclusao == null) {
            conclusao = getNewConclusao();
        }
        return conclusao;
    }

    public static SobreOLocal getNewSobreOLocal() {
        return new SobreOLocal();
    }

    public static SobreOLocal getSobreOLocal() {
        if (sobreOLocal == null) {
            sobreOLocal = getNewSobreOLocal();
        }
        return sobreOLocal;
    }

    public static AnaliseDeCausa getNewAnaliseDeCausa() {
        return new AnaliseDeCausa();
    }

    public static AnaliseDeCausa getAnaliseDeCausa() {
        if (analiseDeCausa == null) {
            analiseDeCausa = getNewAnaliseDeCausa();
        }
        return analiseDeCausa;
    }


    public static RelatorioFinal getNewRelatorioFinal() {
        return new RelatorioFinal();
    }

    public static RelatorioFinal getRelatorioFinal() {
        if (relatorioFinal == null) {
            relatorioFinal = getNewRelatorioFinal();
        }
        return relatorioFinal;
    }

    public static VeiculoEnvolvido getNewVeiculoEnvolvido() {
        return new VeiculoEnvolvido();
    }

    public static VeiculoEnvolvido getVeiculoEnvolvido() {
        if (veiculoEnvolvido == null) {
            veiculoEnvolvido = getNewVeiculoEnvolvido();
        }
        return veiculoEnvolvido;
    }

    public static VitimaEnvolvida getNewVitimaEnvolvida() {
        return new VitimaEnvolvida();
    }

    public static VitimaEnvolvida getVitimaEnvolvida() {
        if (vitimaEnvolvida == null) {
            vitimaEnvolvida = getNewVitimaEnvolvida();
        }
        return vitimaEnvolvida;
    }

    public static RegistroDoAcidente getNewRegistroDoAcidente() {
        return new RegistroDoAcidente();
    }

    public static RegistroDoAcidente getRegistroDoAcidente() {
        if (registroDoAcidente == null) {
            registroDoAcidente = getNewRegistroDoAcidente();
        }
        return registroDoAcidente;
    }

    public static CriticaDeViagem getNewCriticaDeViagem() {
        return new CriticaDeViagem();
    }

    public static CriticaDeViagem getCriticaDeViagem() {
        if (criticaDeViagem == null) {
            criticaDeViagem = getNewCriticaDeViagem();
        }
        return criticaDeViagem;
    }


    public static AnaliseDeViagem getNewAnaliseDeViagem() {
        return new AnaliseDeViagem();
    }

    public static AnaliseDeViagem getAnaliseDeViagem() {
        if (analiseDeViagem == null) {
            analiseDeViagem = getNewAnaliseDeViagem();
        }
        return analiseDeViagem;
    }

    public static QuestionarioManobrador getNewQuestionarioManobrador() {
        return new QuestionarioManobrador();
    }

    public static QuestionarioManobrador getQuestionarioManobrador() {
        if (questionarioManobrador == null) {
            questionarioManobrador = getNewQuestionarioManobrador();
        }
        return questionarioManobrador;
    }

    public static QuestionarioCheckListAcidente getNewQuestionarioCheckListAcidente() {
        return new QuestionarioCheckListAcidente();
    }

    public static QuestionarioCheckListAcidente getQuestionarioCheckListAcidente() {
        if (questionarioCheckListAcidente == null) {
            questionarioCheckListAcidente = getNewQuestionarioCheckListAcidente();
        }
        return questionarioCheckListAcidente;
    }

    public static QuestionarioMaquinista getNewQuestionarioMaquinista() {
        return new QuestionarioMaquinista();
    }

    public static QuestionarioMaquinista getQuestionarioMaquinista() {
        if (questionarioMaquinista == null) {
            questionarioMaquinista = getNewQuestionarioMaquinista();
        }
        return questionarioMaquinista;
    }

    public static DadosDoManobradorAjudante getNewDadosDoManobradorAjudante() {
        return new DadosDoManobradorAjudante();
    }

    public static DadosDoManobradorAjudante getDadosDoManobradorAjudante() {
        if (dadosDoManobradorAjudante == null) {
            dadosDoManobradorAjudante = getNewDadosDoManobradorAjudante();
        }
        return dadosDoManobradorAjudante;
    }

    public static DadosDoMaquinistaCondutor getNewDadosDoMaquinistaCondutor() {
        return new DadosDoMaquinistaCondutor();
    }

    public static DadosDoMaquinistaCondutor getDadosDoMaquinistaCondutor() {
        if (dadosDoMaquinistaCondutor == null) {
            dadosDoMaquinistaCondutor = getNewDadosDoMaquinistaCondutor();
        }
        return dadosDoMaquinistaCondutor;
    }

    public static DadosDoSupervisorResponsavel getNewDadosDoSupervisorResponsavel() {
        return new DadosDoSupervisorResponsavel();
    }

    public static DadosDoSupervisorResponsavel getDadosDoSupervisorResponsavel() {
        if (dadosDoSupervisorResponsavel == null) {
            dadosDoSupervisorResponsavel = getNewDadosDoSupervisorResponsavel();
        }
        return dadosDoSupervisorResponsavel;
    }

    public static Cargas getNewCargas() {
        return new Cargas();
    }

    public static Cargas getCargas() {
        if (cargas == null) {
            cargas = getNewCargas();
        }
        return cargas;
    }

    public static FormacaoDoTrem getNewFormacaoDoTrem() {
        return new FormacaoDoTrem();
    }

    public static FormacaoDoTrem getFormacaoDoTrem() {
        if (formacaoDoTrem == null) {
            formacaoDoTrem = getNewFormacaoDoTrem();
        }
        return formacaoDoTrem;
    }

    public static Estacionamento getNewEstacionamento() {
        return new Estacionamento();
    }

    public static Estacionamento getEstacionamento() {
        if (estacionamento == null) {
            estacionamento = getNewEstacionamento();
        }
        return estacionamento;
    }

    public static SobreOAMV getNewSobreOAMV() {
        return new SobreOAMV();
    }

    public static SobreOAMV getSobreOAMV() {
        if (sobreOAMV == null) {
            sobreOAMV = getNewSobreOAMV();
        }
        return sobreOAMV;
    }

    public static AMV getNewAMV() {
        return new AMV();
    }

    public static AMV getAMV() {
        if (amv == null) {
            amv = getNewAMV();
        }
        return amv;
    }

    public static Trilho getNewTrilho() {
        return new Trilho();
    }

    public static Trilho getTrilho() {
        if (trilho == null) {
            trilho = getNewTrilho();
        }
        return trilho;
    }

    public static CustoColaborador getNewCustoColaborador() {
        return new CustoColaborador();
    }

    public static CustoColaborador getCustoColaborador() {
        if (custoColaborador == null) {
            custoColaborador = getNewCustoColaborador();
        }
        return custoColaborador;
    }

    public static CustoTransporte getNewCustoTransporte() {
        return new CustoTransporte();
    }

    public static CustoTransporte getCustoTransporte() {
        if (custoTransporte == null) {
            custoTransporte = getNewCustoTransporte();
        }
        return custoTransporte;
    }

    public static CustoOutros getNewCustoOutros() {
        return new CustoOutros();
    }

    public static CustoOutros getCustoOutros() {
        if (custoOutros == null) {
            custoOutros = getNewCustoOutros();
        }
        return custoOutros;
    }

    public static CustoMaquina getNewCustoMaquina() {
        return new CustoMaquina();
    }

    public static CustoMaquina getCustoMaquina() {
        if (custoMaquina == null) {
            custoMaquina = getNewCustoMaquina();
        }
        return custoMaquina;
    }

    public static CustoMaterial getNewCustoMaterial() {
        return new CustoMaterial();
    }

    public static CustoMaterial getCustoMaterial() {
        if (custoMaterial == null) {
            custoMaterial = getNewCustoMaterial();
        }
        return custoMaterial;
    }

    public static CustoLocomotiva getNewCustoLocomotiva() {
        return new CustoLocomotiva();
    }

    public static CustoLocomotiva getCustoLocomotiva() {
        if (custoLocomotiva == null) {
            custoLocomotiva = getNewCustoLocomotiva();
        }
        return custoLocomotiva;
    }

    public static CustoVagoes getNewCustoVagoes() {
        return new CustoVagoes();
    }

    public static CustoVagoes getCustoVagoes() {
        if (custoVagoes == null) {
            custoVagoes = getNewCustoVagoes();
        }
        return custoVagoes;
    }

    public static CustoVeiculosVia getNewCustoVeiculosVia() {
        return new CustoVeiculosVia();
    }

    public static CustoVeiculosVia getCustoVeiculosVia() {
        if (custoVeiculosVia == null) {
            custoVeiculosVia = getNewCustoVeiculosVia();
        }
        return custoVeiculosVia;
    }

    public static TabelaAnomalias getNewTabelaAnomalias() {
        return new TabelaAnomalias();
    }

    public static TabelaAnomalias getTabelaAnomalias() {
        if (tabelaAnomalias == null) {
            tabelaAnomalias = getNewTabelaAnomalias();
        }
        return tabelaAnomalias;
    }

    public static VeiculoDeTerceiro getNewVeiculoDeTerceiro() {
        return new VeiculoDeTerceiro();
    }

    public static VeiculoDeTerceiro getVeiculoDeTerceiro() {
        if (veiculoDeTerceiro == null) {
            veiculoDeTerceiro = getNewVeiculoDeTerceiro();
        }
        return veiculoDeTerceiro;
    }

    public static Veiculo getNewVeiculo() {
        return new Veiculo();
    }

    public static Veiculo getVeiculo() {
        if (veiculo == null) {
            veiculo = getNewVeiculo();
        }
        return veiculo;
    }

    public static ResponsavelAcionado getNewResponsavelAcionado(boolean editado) {
        return new ResponsavelAcionado(editado);
    }

    public static ResponsavelAcionado getResponsavelAcionado() {
        if (responsavelAcionado == null) {
            responsavelAcionado = getNewResponsavelAcionado(false);
        }
        return responsavelAcionado;
    }

    public static Sinistro getNewSinistro() {
        return new Sinistro();
    }

    public static Sinistro getSinistro() {
        if (sinistro == null) {
            sinistro = getNewSinistro();
        }
        return sinistro;
    }

    public static MaquinaAcionada getMaquinaAcionada() {
        if (maquinaAcionada == null) {
            maquinaAcionada = getNewMaquinaAcionada();
        }
        return maquinaAcionada;
    }

    public static MaquinaAcionada getNewMaquinaAcionada() {
        return new MaquinaAcionada();
    }

    public static TurmaAcionada getTurmaAcionada() {
        if (turmaAcionada == null) {
            turmaAcionada = getNewTurmaAcionada();
        }
        return turmaAcionada;
    }

    public static TurmaAcionada getNewTurmaAcionada() {
        return new TurmaAcionada();
    }

    public static AbaLiberacao getAbaLiberacao() {
        if (abaLiberacao == null) {
            abaLiberacao = getNewAbaLiberacao();
        }
        return abaLiberacao;
    }

    public static AbaLiberacao getNewAbaLiberacao() {
        return new AbaLiberacao();
    }

    public static int getRandom(@Nullable Integer limite) {
        if (limite != null) {
            return new Random().nextInt(limite) + 1; // [0...limite]
        } else {
            return new Random().nextInt(LIMIT_RANDOM) + 1; // [0...LIMIT_RANDOM]
        }
    }

    public static String getTimeNow() {
        return formatTime(LocalDateTime.now());
    }

    public static LocalDateTime getTime() {
        return LocalDateTime.now();
    }

    public static String getDay() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public static String getDay(long daysPlus) {
        return LocalDate.now().plusDays(daysPlus).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public static String getDayMinus(long minusDays) {
        return LocalDate.now().minusDays(minusDays).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public static String formatTime(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ofPattern("HH:mm"));
    }

    public static DadosVia getDadosVia() {
        if (dadosVia == null) {
            dadosVia = getNewDadosVia();
        }
        return dadosVia;
    }

    public static DadosVia getNewDadosVia() {
        return new DadosVia();
    }

    public static Flambagem getNewFlambagem() {
        return new Flambagem();
    }

    public static Flambagem getFlambagem() {
        if (flambagem == null) {
            flambagem = getNewFlambagem();
        }
        return flambagem;
    }

    public static Incidente getNewIncidente() {
        return new Incidente();
    }

    public static Incidente getIncidente() {
        if (incidente == null) {
            incidente = getNewIncidente();
        }
        return incidente;
    }

    public static MembroEquipe getNewMembroEquipe() {
        return new MembroEquipe();
    }

    public static MembroEquipe getMembroEquipe() {
        if (membroEquipe == null) {
            membroEquipe = getNewMembroEquipe();
        }
        return membroEquipe;
    }

    public static ConfiguracoesDashboard getNewConfiguracoesDashboard() {
        return new ConfiguracoesDashboard();
    }

    public static ConfiguracoesDashboard getConfiguracoesDashboard() {
        if (configuracoesDashboard == null) {
            configuracoesDashboard = getNewConfiguracoesDashboard();
        }
        return configuracoesDashboard;
    }

    public static String getRandomCharacters(int tamanho) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzáéíóúçâêîôû0123456789!@#$&*()-_=+[{]}|;:,<.>/?";
        return RandomStringUtils.random(tamanho, characters);
    }

    public static String getHTMLText() {
        String aHTML = "<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong> ;</p><p><i>Aliquam luctus turpis lectus, at sollicitudin purus molestie et</i>. ;</p><p><a href=\"https://br.lipsum.com/feed/html\">Aliquam interdum eros a velit pretium molestie eget a ante</a>. ;</p><ol><li>Praesent ligula mi, lobortis vel neque at, euismod fringilla justo. ;</li></ol><ul><li>Morbi sit amet placerat diam, sit amet vestibulum tellus.<ul><li>;Nam suscipit lectus ut scelerisque dapibus. Cras ut sem eu orci semper aliquam. Quisque porttitor ligula arcu. Curabitur pulvinar ipsum quam, sed pharetra eros dapibus a. ;</li><li>Cras lacinia, leo et feugiat cursus, lectus tortor facilisis turpis, in lobortis eros nunc in ante. Aliquam nec sodales ipsum, et semper ipsum. Aliquam eu blandit lacus. Etiam.</li></ul></li></ul>";
        return aHTML;
    }

}
