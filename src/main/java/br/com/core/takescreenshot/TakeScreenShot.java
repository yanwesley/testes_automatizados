package br.com.core.takescreenshot;


import br.com.core.properties.PropertiesManager;
import br.com.utils.WebDriverHelper;
import org.codehaus.plexus.util.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static br.com.utils.ReporterUtils.addLogToReport;

public class TakeScreenShot extends WebDriverHelper {

    static PropertiesManager setupProperties = new PropertiesManager("Setup.properties");

    /**
     * Create a file with a screenshot from the browser (Webdriver object)
     *
     * @return The path to the file of the screenshot
     */
    public static String imageFile(WebDriver driver, String path) {
        if (path == null) {
            path = System.getProperty("user.dir") + "\\target\\image\\";
        }
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            path += new SimpleDateFormat("ddMMyyyyHHmmss").format(Calendar.getInstance().getTime()) + ".png";
            FileUtils.copyFile(srcFile, new File(path));
        } catch (IOException e) {
            addLogToReport(e.getMessage());
        }
        return path;
    }

    public synchronized static byte[] getImageBytes(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

}
