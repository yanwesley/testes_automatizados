package br.com.pom.api;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public class UtilsAPI {
    private static String typeUser = "TLG.CCO";
    RestAssured restAssured = new RestAssured();

    public static void setTypeUser(String typeUser) {
        UtilsAPI.typeUser = typeUser;
    }

    public static RequestSpecification getRestAssured() {
        return RestAssured.given()
                .header("perfilNome", typeUser)
                .header("usuarioLogin", "00000001")
                .header("usuarioNome", "Usuário DEV");
    }


}
