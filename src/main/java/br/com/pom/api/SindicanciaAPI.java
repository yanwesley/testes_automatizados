package br.com.pom.api;

import br.com.api.model.sindicancia.RelatorioFinal;
import br.com.api.model.sinistro.Sinistro;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static br.com.api.GeradorDeMassa.getDay;
import static br.com.api.GeradorDeMassa.getTimeNow;
import static br.com.pom.sro.GeralPage.convertDateTimeToUTC;
import static br.com.utils.WebDriverHelper.urlSegurancaPatrimonial;
import static br.com.utils.WebDriverHelper.urlSindicancia;

public class SindicanciaAPI extends UtilsAPI {

    public static void concluirAreasSindicancia(Sinistro sinistro, String conclusao) {
        getRestAssured()
                .contentType("application/merge-patch+json;charset=utf-8")
                .body("{" +
                        "  \"idSindicancia\": " + sinistro.getIdSinistro() + "," +
                        "  \"statusVia\": \"FINALIZADO\"," +
                        "  \"statusMaterialRodante\": \"FINALIZADO\"," +
                        "  \"statusOperacao\": \"FINALIZADO\"," +
                        "  \"statusSegurancaPatrimonial\": \"FINALIZADO\"," +
                        "  \"statusCipiaPresidente\": \"ABERTA\"," +
                        "  \"conclusaoFinalVia\": \"" + conclusao + "\"," +
                        "  \"conclusaoFinalMaterialRodante\": \"" + conclusao + "\"," +
                        "  \"conclusaoFinalOperacao\": \"" + conclusao + "\"" +
                        "}")
                .patch(urlSindicancia + "sindicancia/" + sinistro.getIdSinistro())
                .then()
                .statusCode(200)
                .extract()
                .response();
    }

    public static void preencherDescricaoSegurancaPatrimonial(Sinistro sinistro, String descricao) {
        String json = "{" +
                "\"idSegurancaPatrimonial\":" + sinistro.getIdSinistro() + "," +
                "\"descritivoAcidente\":\"" + descricao + "\"" +
                "}";
        getRestAssured()
                .contentType(ContentType.JSON)
                .body(json)
                .put(urlSegurancaPatrimonial + "segurancaPatrimonial/" + sinistro.getIdSinistro())
                .then()
                .statusCode(200)
                .extract()
                .response();
    }

    public static void preencherConclusaoCipiaTecnica(Sinistro sinistro, String conclusao) {
        String datetimeNow = getDay().concat(" " + getTimeNow());

        //campos passaram a ser enviado quando o cipia presidente envia para aprovação
//                "   \"statusCipiaPresidente\": \"FINALIZADO\"," +
//                "   \"dataHoraConclusao\":\"" + convertDateTimeToUTC(datetimeNow) + "\"," +
//                "   \"flagConclusaoAnaliseTecnica\":true," +
        String json = "{" +
                "   \"idSindicancia\":" + sinistro.getIdSinistro() + "," +
                "   \"conclusaoFinalAnaliseTecnica\":\"" + conclusao + "\"," +
                "   \"dataHoraConclusaoAnaliseTecnica\":\"" + convertDateTimeToUTC(datetimeNow) + "\"," +
                "   \"status\":\"ABERTA\"" +
                "}";

        getRestAssured()
                .contentType("application/merge-patch+json;charset=utf-8")
                .body(json)
                .patch(urlSindicancia + "sindicancia/" + sinistro.getIdSinistro())
                .then()
                .statusCode(200)
                .extract()
                .response();
    }

    public static void preencherRelatorioFinalSindicancia(Sinistro sinistro, RelatorioFinal relatorioFinal) {

        String json = "{" +
                "  \"idSindicancia\": " + sinistro.getIdSinistro() + "," +
                "  \"causaSindicanciaPrimaria\": {" +
                "    \"idCausa\": 531," +
                "    \"nome\": \"VAGÕES_TRUQUES_AMORTECIMENTO_SUSPENSÃO MOLA FALTANDO\"," +
                "    \"nomeBandeira\": \"AMORTECIMENTO\"," +
                "    \"area\": \"Área Responsável 4\"," +
                "    \"idBandeira\": 60" +
                "  }," +
                "  \"nomeDono\": \"André Pires de Souza\"," +
                "  \"emailDono\": \"dnzl@gft.com\"," +
                "  \"matriculaDono\": \"00000054\"," +
                "  \"nomeSuperiorDono\": \"Marcelo Tappis Dias\"," +
                "  \"matriculaSuperiorDono\": \"00256674\"," +
                "  \"areaDono\": \"RUMO - Ger Compras Serviços\"," +
                "  \"fatoOcorrido\": \" " + relatorioFinal.getFatoOcorrido() + "\"," +
                "  \"causaSindicanciaPrimariaPercentual\": 100," +
                "  \"causaSindicanciaPrimariaValorGravidade\": 5.01," +
                "  \"idMecanismoDescarrilhamento\": 6" +
                "}";

        Response response = getRestAssured()
                .contentType("application/merge-patch+json;charset=utf-8")
                .body(json)
                .patch(urlSindicancia + "sindicancia/" + sinistro.getIdSinistro())
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(response.body().asString());
        relatorioFinal.setSuperior(jsonPath.get("listaSindicancia.nomeSuperiorDono[0]"));
        relatorioFinal.setResponsavel(jsonPath.get("listaSindicancia.nomeDono[0]"));
        relatorioFinal.setAreaDonoDoAcidente(jsonPath.get("listaSindicancia.areaDono[0]"));
        relatorioFinal.setMecanismoDescarrilhamentoValue("Deslocamento da grade");
        relatorioFinal.setMatriculaDono(jsonPath.get("listaSindicancia.matriculaDono[0]"));
        relatorioFinal.setBandeiraValue(jsonPath.get("listaSindicancia.causaSindicanciaPrimaria.nomeBandeira[0]"));
        relatorioFinal.setCausaValue(jsonPath.get("listaSindicancia.causaSindicanciaPrimaria.nome[0]"));
        relatorioFinal.setAreaResponsavelValue(jsonPath.get("listaSindicancia.causaSindicanciaPrimaria.area[0]"));
    }

}
