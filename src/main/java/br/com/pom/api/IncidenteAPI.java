package br.com.pom.api;

import br.com.api.model.incidente.Incidente;
import br.com.api.model.sinistro.Veiculo;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static br.com.pom.sro.GeralPage.convertDateTimeToUTC;
import static br.com.utils.WebDriverHelper.urlIncidente;

public class IncidenteAPI extends UtilsAPI {

    public static void criarIncidente(Incidente incidente, Veiculo veiculo) {
        String datetime = incidente.getData().concat(" ").concat(incidente.getHora());

        String body = "{" +
                "   \"dataHoraIncidente\":\"" + convertDateTimeToUTC(datetime).toString() + "\"," +
                "   \"idAreaResponsavel\":21," +
                "   \"matriculaUsuarioCriacao\":\"" + incidente.getMatriculaUsuarioCriacao() + "\"," +
                "   \"nomeUsuarioCriacao\":\"" + incidente.getNomeUsuarioCriacao() + "\"," +
                "   \"descricaoAreaResponsavel\":\"Área Responsável 1\"," +
                "   \"idAreaResponsavelInicial\":21," +
                "   \"descricaoAreaResponsavelInicial\":\"Área Responsável 1\"," +
                "   \"idTipoIncidente\":1," +
                "   \"descricaoTipoIncidente\":\"Tipo incidente 1\"," +
                "   \"idSubtipoIncidente\":1," +
                "   \"descricaoSubtipoIncidente\":\"Subtipo Incidente 1\"," +
                "   \"criticidade\":5," +
                "   \"idFonteIncidente\":1," +
                "   \"descricaoFonteIncidente\":\"Fonte incidente 1\"," +
                "   \"descricaoIncidente\":\"" + incidente.getDescricaoDoIncidente() + "\"," +
                "   \"sb\":\"" + incidente.getSecaoBloqueioSB() + "\"," +
                "   \"km\":" + incidente.getKm().replace(",", ".") + "," +
                "   \"idSubdivisao\":1," +
                "   \"descricaoSubdivisao\":\"1 - LONDRINA - CIANORTE\"," +
                "   \"operacao\":\"NORTE\"," +
                "   \"idComplexo\":1," +
                "   \"descricaoComplexo\":\"Complexo 1\"," +
                "   \"idCorredor\":1," +
                "   \"descricaoCorredor\":\"Corredor 1\"," +
                "   \"tremEnvolvido\":true," +
                "   \"prefixoTrem\":\"" + incidente.getPrefixo() + "\"," +
                "   \"idComposicaoTrem\": 2 ," +
                "   \"nomeComposicaoTrem\":\"Tipo Composição 2\"," +
                "   \"numeroOS\":\"" + incidente.getOS() + "\"," +
                "   \"status\":\"ABERTO\"" +
                "}";

        Response response =
                getRestAssured()
                        .contentType(ContentType.JSON)
                        .body(body)
                        .post(urlIncidente + "incidente")
                        .then()
                        .statusCode(200)
                        .extract()
                        .response();

        JsonPath jsonPath = new JsonPath(response.body().asString());
        incidente.setIdIncidente(jsonPath.get("lista.idIncidente[0]"));
        incidente.setAreaResponsavelInicialValue(jsonPath.get("lista.descricaoAreaResponsavelInicial[0]"));
        incidente.setTipoValue(jsonPath.get("lista.descricaoTipoIncidente[0]"));
        incidente.setSubTipoValue(jsonPath.get("lista.descricaoSubtipoIncidente[0]"));
        incidente.setFonteDoIncidenteValue(jsonPath.get("lista.descricaoFonteIncidente[0]"));
        incidente.setSubdivisaoValue(jsonPath.get("lista.descricaoSubdivisao[0]"));
        incidente.setComplexoValue(jsonPath.get("lista.descricaoComplexo[0]"));
        incidente.setCorredorValue(jsonPath.get("lista.descricaoCorredor[0]"));
        incidente.setOperacaoValue(jsonPath.get("lista.operacao[0]"));
        incidente.setCriticidade(jsonPath.get("lista.criticidade[0]").toString());
        incidente.setTipoDeComposicaoValue(jsonPath.get("lista.nomeComposicaoTrem[0]"));
        incidente.setExistePrefixoDoTrem(jsonPath.get("lista.tremEnvolvido[0]"));

        String bodyVeiculo = "[{" +
                "      \"idIncidente\":" + incidente.getIdIncidente() + "," +
                "      \"posicao\":" + veiculo.getPosicao() + "," +
                "      \"numero\":\"" + veiculo.getNumero() + "\"," +
                "      \"tipoAtivoEnvolvido\":\"VEICULO_VIA\"," +
                "      \"modelo\":\"G12/USA\"" +
                "   }]";

        Response responseVeiculo =
                getRestAssured()
                        .contentType(ContentType.JSON)
                        .body(bodyVeiculo)
                        .post(urlIncidente + "incidente/" + incidente.getIdIncidente() + "/ativoEnvolvido")
                        .then()
                        .statusCode(200)
                        .extract()
                        .response();

        JsonPath jsonPathVeiculo = new JsonPath(responseVeiculo.body().asString());
        incidente.setAtivosForamEnvolvidos(true);
        veiculo.setModelo(jsonPathVeiculo.get("lista.modelo[0]"));
        veiculo.setTipoValue("Veículo de Via");


        String envioFlag = "{" +
                "   \"idIncidente\":" + incidente.getIdIncidente() + "," +
                "   \"flagAbriuIncidente\":true," +
                "   \"matriculaUsuarioAlteracao\":\"" + incidente.getMatriculaUsuarioCriacao() + "\"," +
                "   \"nomeUsuarioAlteracao\":\"" + incidente.getNomeUsuarioCriacao() + "\"," +
                "   \"emailUsuarioAlteracao\":\"email@email.com\"" +
                "}";

        Response responsePatch =
                getRestAssured()
                        .contentType("application/merge-patch+json;charset=utf-8")
                        .body(envioFlag)
                        .patch(urlIncidente + "incidente/" + incidente.getIdIncidente())
                        .then()
                        .statusCode(200)
                        .extract()
                        .response();
    }

}
