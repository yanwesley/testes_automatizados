package br.com.pom.api;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.api.model.sinistro.Sinistro;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static br.com.api.GeradorDeMassa.getDay;
import static br.com.api.GeradorDeMassa.getTimeNow;
import static br.com.pom.sro.GeralPage.convertDateTimeToUTC;
import static br.com.utils.WebDriverHelper.urlPlanoAcao;

public class AcaoAPI extends UtilsAPI {

    public static void preencherAcao(Sinistro sinistro, PlanoDeAcao planoDeAcao, Acao acao, ResponsavelAcao responsavelAcao) {
        String datetime = sinistro.getDate().concat(" ").concat(sinistro.getHora());
        String datetimePrazo = acao.getQualEOPrazoParaExecucao().concat(" ").concat("00:00");
        String origemDescricao = (sinistro.getClassificacao().equalsIgnoreCase("ocorrência") ? "Ocorrência" : "Acidente") + " #" + sinistro.getIdSinistro();
        acao.setOrigemAcao(origemDescricao);

        String json = "[" +
                "   {" +
                "      \"origemAcao\":\"" + sinistro.getClassificacao() + "\"," +
                "      \"descricaOrigem\":\"" + acao.getOrigemAcao() + "\"," +
                "      \"nomeSolicitante\":\"" + planoDeAcao.getSolicitante() + "\"," +
                "      \"emailSolicitante\":\"dnzl@gft.com\"," +
                "      \"matriculaSolicitante\":\"" + planoDeAcao.getSolicitanteMatricula() + "\"," +
                "      \"focoCausaDescricao\":\"Bandeira 2\"," +
                "      \"impacto\":\"BAIXO\"," +
                "      \"tipoAcao\":\"DESDOBRAMENTO_DE_RISCO\"," +
                "      \"idPlanoAcao\":" + planoDeAcao.getIdPlanoAcao() + "," +
                "      \"prazo\":\"" + convertDateTimeToUTC(datetimePrazo) + "\"," +
                "      \"publica\":" + acao.isAcaoPublica() + "," +
                "      \"descricaoAcao\":\"" + acao.getDescritivoDaAcao() + "\"," +
                "      \"dataCriacao\":\"" + convertDateTimeToUTC(datetime) + "\"," +
                "      \"status\":\"AGUARDANDO_ACEITE\"," +
                "      \"idCausaPlanoAcao\":" + planoDeAcao.getIdPlanoCausaAcao() + "," +
                "      \"operacao\":\"SUL\"," +
                "      \"idComplexo\":1," +
                "      \"nomeComplexo\":\"Complexo 1\"," +
                "      \"matriculaResponsavel\":\"00000029\"," +
                "      \"nomeResponsavel\":\"Lenaldo Monteiro\"," +
                "      \"emailResponsavel\":\"CS000029@COSAN.REDE\"," +
                "      \"nomeAreaResponsavel\":\"RUMO - Manut. Mecânica Turno\"," +
                "      \"idAreaResponsavel\":\"00026450\"," +
                "      \"nomeSuperior\":\"Fabiano Burghi Xavier\"," +
                "      \"matriculaSuperior\":\"00237969\"" +
                "   }" +
                "]";

        Response response = getRestAssured()
                .contentType(ContentType.JSON)
                .body(json)
                .post(urlPlanoAcao + "acao")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(response.body().asString());

        acao.setIdAcao(jsonPath.get("lista[0].idAcao"));
        acao.setIdPlanoAcao(jsonPath.get("lista[0].idPlanoAcao"));
        acao.setQualOTipoDaAcaoValue("Desdobramento de risco");
        acao.setQualOImpactoDaAcaoValue(jsonPath.get("lista.impacto[0]"));
        acao.setFocoCausaDaAcaoValue(jsonPath.get("lista.focoCausaDescricao[0]"));
        responsavelAcao.setArea(jsonPath.get("lista.nomeAreaResponsavel[0]"));
        responsavelAcao.setGerente(jsonPath.get("lista.nomeSuperior[0]"));
        responsavelAcao.setQualEOComplexoValue(jsonPath.get("lista.nomeComplexo[0]"));
        responsavelAcao.setQualEAOperacaoValue(jsonPath.get("lista.operacao[0]"));
        responsavelAcao.setResponsavel(jsonPath.get("lista.nomeResponsavel[0]"));
    }

    public static void aceitarAcao(Acao acao) {
        String datetimeNow = getDay().concat(" " + getTimeNow());

        String json = "{" +
                "   \"idAcao\":" + acao.getIdAcao() + "," +
                "   \"idPlanoAcao\":" + acao.getIdPlanoAcao() + "," +
                "   \"status\":\"ABERTA\"," +
                "   \"dataAceite\":\"" + convertDateTimeToUTC(datetimeNow) + "\"," +
                "   \"nomeAceite\":\"Usuário DEV\"," +
                "   \"matriculaAceite\":\"00000001\"," +
                "   \"processoAceiteMultipla\":false" +
                "}";

        System.out.println("acao json :" + json);
        Response response = getRestAssured()
                .contentType("application/merge-patch+json;charset=utf-8")
                .body(json)
                .patch(urlPlanoAcao + "acao/" + acao.getIdAcao())
                .then()
                .statusCode(200)
                .extract()
                .response();

    }

    public static void anularAcao(Acao acao) {
        String json = "{" +
                "   \"idAcao\":" + acao.getIdAcao() + "," +
                "   \"status\":\"ANULADA\"" +
                "}";

        Response response = getRestAssured()
                .contentType("application/merge-patch+json;charset=utf-8")
                .body(json)
                .patch(urlPlanoAcao + "acao/" + acao.getIdAcao())
                .then()
                .statusCode(200)
                .extract()
                .response();

    }
}
