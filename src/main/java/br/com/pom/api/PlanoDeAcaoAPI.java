package br.com.pom.api;

import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.sindicancia.RelatorioFinal;
import br.com.api.model.sinistro.Sinistro;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static br.com.pom.sro.GeralPage.convertDateTimeToUTC;
import static br.com.utils.WebDriverHelper.urlPlanoAcao;

public class PlanoDeAcaoAPI extends UtilsAPI {

    public static void preencherAnaliseDeCausas(Sinistro sinistro, RelatorioFinal relatorioFinal, PlanoDeAcao planoDeAcao) {
        String datetime = sinistro.getDate().concat(" ").concat(sinistro.getHora());
        String complexo = "Complexo 1";
        String operacao = "SUL";

        String json = "{" +
                "  \"dataCriacao\": \"" + convertDateTimeToUTC(datetime).toString() + "\"," +
                "  \"descricaoTipoAcidente\": " + sinistro.getTipo() + "," +
                "  \"idSindicancia\": " + sinistro.getIdSinistro() + "," +
                "  \"idTipoAcidente\": " + sinistro.getTipoIndex() + "," +
                "  \"matriculaSolicitante\": \"" + relatorioFinal.getMatriculoDono() + "\"," +
                "  \"nomeSolicitante\": \"" + relatorioFinal.getResponsavel() + "\"," +
                "  \"emailSolicitante\":\"dnzl@gft.com\"," +
                "  \"prefixo\": \"" + sinistro.getPrefixo() + "\"," +
                "  \"statusPlanoAcao\": \"ELABORAR_PLANO\"," +
                "  \"origemPlanoAcao\": \"" + sinistro.getClassificacao() + "\"," +
                " \"numAcoesAceitas\": 0," +
                " \"numAcoesCriadas\": 0," +
                " \"operacao\": \"" + operacao + "\"," +
                " \"idComplexo\": 1," +
                " \"nomeComplexo\": \"" + complexo + "\"," +
                " \"numAcoesEncerradas\": 0" +
                "}";

        Response response = getRestAssured()
                .contentType(ContentType.JSON)
                .body(json)
                .post(urlPlanoAcao + "planoAcao")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(response.body().asString());
        Integer idPlanoAcao = jsonPath.get("lista[0].idPlanoAcao");

        String jsonCausa = "{" +
                "  \"areaResponsavel\": \"TLG.CIPIA_OPERACAO\"," +
                "  \"causaIdentificada\": \"" + relatorioFinal.getCausaValue() + "\"," +
                "  \"descricaoBandeira\": \" " + relatorioFinal.getBandeiraValue() + "\"," +
                "  \"fatoOcorrido\": \"" + relatorioFinal.getFatoOcorrido() + "\"," +
                "  \"idBandeira\": 1," +
                "  \"idCausa\": 3," +
                "  \"idPlanoAcao\": " + idPlanoAcao + "," +
                "  \"nomeCausa\": \"CAUSA PRIMÁRIA\"," +
                "  \"tipoCausa\": \"PRIMARIA\"," +
                "  \"percentual\": 100," +
                "  \"valorGravidade\": 5.01" +
                "}";

        Response causa = getRestAssured()
                .contentType("application/json;charset=utf-8")
                .body(jsonCausa)
                .post(urlPlanoAcao + "planoAcao/" + idPlanoAcao + "/causa")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPathCausa = new JsonPath(causa.body().asString());
        Integer idPlanoCausaAcao = jsonPathCausa.get("lista[0].idCausaPlanoAcao");

        planoDeAcao.setValues(sinistro, relatorioFinal, idPlanoAcao, complexo, operacao, idPlanoCausaAcao);
    }
}
