package br.com.pom.api;

import br.com.api.model.sinistro.Sinistro;
import br.com.api.model.sinistro.Veiculo;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static br.com.pom.sro.GeralPage.convertDateTimeToUTC;
import static br.com.utils.WebDriverHelper.urlSinistro;

public class SinistroAPI extends UtilsAPI {

    public static void criarSinistro(Sinistro sinistro) {
        String datetime = sinistro.getDate().concat(" ").concat(sinistro.getHora());
        String datetimePrevisao = sinistro.getDataPrevisaoLiberacao();

        Response response =
                getRestAssured()
                        .contentType(ContentType.JSON)
                        .body("{" +
                                "   \"dataHoraAcidente\":\"" + convertDateTimeToUTC(datetime).toString() + "\"," +
                                "   \"dataHoraPrevistaLiberacao\":\"" + convertDateTimeToUTC(datetimePrevisao).toString() + "\"," +
                                "   \"tipoAcidente\":{" +
                                "      \"idTipoAcidente\":" + sinistro.getTipoIndex() + "," +
                                "      \"descricao\":null" +
                                "   }," +
                                "   \"extensaoViaDanificada\":" + sinistro.getExtensaoDaViaDanificada() + "," +
                                "   \"flagInterdicaoCirculacao\":" + sinistro.isInterdicaoDaCirculacao() + "," +
                                "   \"secaoBloqueio\":\"" + sinistro.getSecaoBloqueioSB() + "\"," +
                                "   \"kmLocalAcidente\":" + sinistro.getKm().replace(",", ".") + "," +
                                "   \"idSubdivisao\":" + sinistro.getSubdivisaoIndex() + "," +
                                "   \"unidadeProducao\":\"UP PR/SC\"," +
                                "   \"nomeSubdivisao\":\"2 - OURINHOS - LONDRINA\"," +
                                "   \"prefixoTrem\":\"" + sinistro.getPrefixo() + "\"," +
                                "   \"operacao\":\"SUL\"," +
                                "   \"idComplexo\": 2," +
                                "   \"nomeComplexo\":\"Complexo 2\"," +
                                "   \"numeroOS\":\"" + sinistro.getNumeroOS() + "\"," +
                                "   \"taraBruta\":" + sinistro.getTaraBrutaTB().replace(",", ".") + "," +
                                "   \"comprimento\": " + sinistro.getComprimento() + "," +
                                "   \"tipoProdutoPerigoso\":{" +
                                "      \"idTipoProdutoPerigoso\":" + sinistro.getProdutoIndex() + "," +
                                "      \"descricao\":null" +
                                "   }," +
                                "   \"flagCondutorRumo\":" + sinistro.isCondutorOuMaquinistaDaRumo() + "," +
                                "   \"nomeCondutor\":\"Raul Herculano Fagundes\"," +
                                "   \"matriculaCondutor\":256011," +
                                "   \"relato\":\"" + sinistro.getRelato() + "\"," +
                                "   \"existemVitimasEnvolvidas\": " + sinistro.isExistemVitimasEnvolvidas() + "," +
                                "   \"qtdVitimasFatais\":" + sinistro.getVitimasFatais() + "," +
                                "   \"qtdVitimasGraves\":" + sinistro.getVitimasGraves() + "," +
                                "   \"qtdVitimasLeves\":" + sinistro.getVitimasLeves() + "," +
                                "   \"flagEmailEnviadoANTT\":null," +
                                "   \"destruicaoTotal\":" + sinistro.isIncendioOuExplosaoComDestruicaoTotal() + "," +
                                "   \"danoAmbiental\":" + sinistro.isHouveDanoAmbiental() + "," +
                                "   \"danoAmbientalVazComBloco\":" + sinistro.isVazamentoDeCombustivelDaLocomotiva() + "," +
                                "   \"danoAmbientalVazProdRioLago\":" + sinistro.isVazamentoDeProdutoEmRiosELagos() + "," +
                                "   \"impactoPopulacao\":" + sinistro.isHouveOutrosImpactosAPopulacao() + "," +
                                "   \"impactoPopulacaoPNTrancada1hora\":" + sinistro.isPnTrancadaPorMaisDe1HoraEmPerimetroUrbano() + "," +
                                "   \"impactoPopulacaoDanificouEdiTerceiros\":" + sinistro.isTombamentoQueDanificouEdificacoesDeTerceiros() + "," +
                                "   \"dataCadastro\":\"" + ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT) + "\"," +
                                "   \"status\":\"ABERTO\"," +
                                "   \"classificacaoSinistro\":\"" + sinistro.getClassificacao() + "\"," +
                                "   \"origem\":\"MANUAL\"}")
                        .post(urlSinistro + "ferrovia/sro/sinistros/sinistro")
                        .then()
                        .statusCode(200)
                        .extract()
                        .response();

        JsonPath jsonPath = new JsonPath(response.body().asString());
        sinistro.setProduto(jsonPath.get("listaSinistro.tipoProdutoPerigoso.descricao[0]"));
        sinistro.setUnidadeProducao(jsonPath.get("listaSinistro.unidadeProducao[0]"));
        sinistro.setSubdivisao(jsonPath.get("listaSinistro.nomeSubdivisao[0]"));
        sinistro.setTipo(jsonPath.get("listaSinistro.tipoAcidente.descricao[0]"));
        sinistro.setIdSinistro(jsonPath.get("listaSinistro.idSinistro[0]"));
        sinistro.setQualEOComplexoValue("Complexo 2");
        sinistro.setQualEAOperacaoValue("Sul");


        String bodyLiberacao = "[" +
                "   {" +
                "      \"dataLiberacao\":\"" + convertDateTimeToUTC(datetimePrevisao).toString() + "\"," +
                "      \"tipoLiberacao\":\"LIBERACAO_PREVISAO\"," +
                "      \"idSinistro\":" + sinistro.getIdSinistro() +
                "   }" +
                "]";

        Response responseLiberacao =
                getRestAssured()
                        .contentType(ContentType.JSON)
                        .body(bodyLiberacao)
                        .post(urlSinistro + "ferrovia/sro/sinistros/sinistro/" + sinistro.getIdSinistro() + "/liberacao")
                        .then()
                        .statusCode(200)
                        .extract()
                        .response();


    }

    public static void adicionarVeiculoAoSinistro(Sinistro sinistro, Veiculo veiculo) {
        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body("[" +
                        "   {" +
                        "      \"idSinistro\":" + sinistro.getIdSinistro() + "," +
                        "      \"tipoOrigemInfo\":\"MANUAL\"," +
                        "      \"qtdRodeiros\":4," +
                        "      \"posicao\":" + veiculo.getPosicao() + "," +
                        "      \"numero\":\"" + veiculo.getNumero() + "\"," +
                        "      \"tipo\":\"VAGAO\"," +
                        "      \"modelo\":\"AAP\"," +
                        "      \"perdaProduto\":" + veiculo.isPerdaProduto() + "," +
                        "      \"mercadoria\":\"SAL GROSSO\"," +
                        "      \"situacaoVeiculo\":\"DESCARRILADO\"," +
                        "        \"listaRodeiro\":[" +
                        "            {" +
                        "                \"rodeiro\":\"R1\"," +
                        "                \"descricao\":\"R1\"" +
                        "            }," +
                        "            {" +
                        "                \"rodeiro\":\"R2\"," +
                        "                \"descricao\":\"R2\"" +
                        "            }," +
                        "            {" +
                        "                \"rodeiro\":\"R3\"," +
                        "                \"descricao\":\"R3\"" +
                        "            }," +
                        "            {" +
                        "                \"rodeiro\":\"R4\"," +
                        "                \"descricao\":\"R4\"" +
                        "            }" +
                        "        ]" +
                        "   }" +
                        "]")
                .post(urlSinistro + "ferrovia/sro/sinistros/sinistro/" + sinistro.getIdSinistro() + "/veiculos")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(response.body().asString());
        veiculo.setTipoValue(jsonPath.get("listaVeiculo.tipo[0]"));
        veiculo.setSituacaoValue(jsonPath.get("listaVeiculo.situacaoVeiculo[0]"));
        veiculo.setMercadoria(jsonPath.get("listaVeiculo.mercadoria[0]"));
        veiculo.setModelo(jsonPath.get("listaVeiculo.modelo[0]"));
    }

    public static void adicionarVagaoAoSinistro(Sinistro sinistro, Veiculo veiculo) {
        Response response = getRestAssured()
                .contentType(ContentType.JSON)
                .body("[" +
                        "   {" +
                        "      \"idSinistro\":" + sinistro.getIdSinistro() + "," +
                        "      \"tipoOrigemInfo\":\"MANUAL\"," +
                        "      \"qtdRodeiros\":4," +
                        "      \"posicao\":" + veiculo.getPosicao() + "," +
                        "      \"numero\":\"" + veiculo.getNumero() + "\"," +
                        "      \"tipo\":\"VAGAO\"," +
                        "      \"modelo\":\"AAP\"," +
                        "      \"perdaProduto\":" + veiculo.isPerdaProduto() + "," +
                        "      \"mercadoria\":\"SAL GROSSO\"," +
                        "      \"situacaoVeiculo\":\"DESCARRILADO\"," +
                        "        \"listaRodeiro\":[" +
                        "            {" +
                        "                \"rodeiro\":\"R1\"," +
                        "                \"descricao\":\"R1\"" +
                        "            }," +
                        "            {" +
                        "                \"rodeiro\":\"R2\"," +
                        "                \"descricao\":\"R2\"" +
                        "            }," +
                        "            {" +
                        "                \"rodeiro\":\"R3\"," +
                        "                \"descricao\":\"R3\"" +
                        "            }," +
                        "            {" +
                        "                \"rodeiro\":\"R4\"," +
                        "                \"descricao\":\"R4\"" +
                        "            }" +
                        "        ]" +
                        "   }" +
                        "]")
                .post(urlSinistro + "ferrovia/sro/sinistros/sinistro/" + sinistro.getIdSinistro() + "/veiculos")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(response.body().asString());
        veiculo.setTipoValue(jsonPath.get("listaVeiculo.tipo[0]"));
        veiculo.setSituacaoValue(jsonPath.get("listaVeiculo.situacaoVeiculo[0]"));
        veiculo.setMercadoria(jsonPath.get("listaVeiculo.mercadoria[0]"));
        veiculo.setModelo(jsonPath.get("listaVeiculo.modelo[0]"));
    }

    public static void adicionarLocomotivaAoSinistro(Sinistro sinistro, Veiculo veiculo) {
        Response response = getRestAssured()
                .contentType(ContentType.JSON)
                .body("[" +
                        "    {" +
                        "        \"idSinistro\":" + sinistro.getIdSinistro() + "," +
                        "        \"tipoOrigemInfo\":\"MANUAL\"," +
                        "        \"qtdRodeiros\":8," +
                        "        \"posicao\":1," +
                        "        \"numero\":\"1\"," +
                        "        \"tipo\":\"LOCOMOTIVA\"," +
                        "        \"modelo\":\"U12B\"," +
                        "        \"perdaProduto\":null," +
                        "        \"mercadoria\":null," +
                        "        \"situacaoVeiculo\":\"DESCARRILADO\"," +
                        "        \"listaRodeiro\":[" +
                        "            {" +
                        "                \"rodeiro\":\"R1\"," +
                        "                \"descricao\":\"R1\"" +
                        "            }," +
                        "            {" +
                        "                \"rodeiro\":\"R2\"," +
                        "                \"descricao\":\"R2\"" +
                        "            }" +
                        "        ]" +
                        "    }" +
                        "]")
                .post(urlSinistro + "ferrovia/sro/sinistros/sinistro/" + sinistro.getIdSinistro() + "/veiculos")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(response.body().asString());
        veiculo.setTipoValue(jsonPath.get("listaVeiculo.tipo[0]"));
        veiculo.setSituacaoValue(jsonPath.get("listaVeiculo.situacaoVeiculo[0]"));
        veiculo.setMercadoria(jsonPath.get("listaVeiculo.mercadoria[0]"));
        veiculo.setModelo(jsonPath.get("listaVeiculo.modelo[0]"));
    }


    public static void adicionarVeiculoDeViaAoSinistro(Sinistro sinistro, Veiculo veiculo) {
        Response response = getRestAssured()
                .contentType(ContentType.JSON)
                .body("[" +
                        "   {" +
                        "      \"idSinistro\":" + sinistro.getIdSinistro() + "," +
                        "      \"tipoOrigemInfo\":\"MANUAL\"," +
                        "      \"qtdRodeiros\":4," +
                        "      \"posicao\":" + veiculo.getPosicao() + "," +
                        "      \"numero\":\"" + veiculo.getNumero() + "\"," +
                        "      \"tipo\":\"VEICULO_VIA\"," +
                        "      \"modelo\":\"AAP\"," +
                        "      \"perdaProduto\":" + veiculo.isPerdaProduto() + "," +
                        "      \"situacaoVeiculo\":\"DESCARRILADO\"," +
                        "        \"listaRodeiro\":[]" +
                        "   }" +
                        "]")
                .post(urlSinistro + "ferrovia/sro/sinistros/sinistro/" + sinistro.getIdSinistro() + "/veiculos")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(response.body().asString());
        veiculo.setTipoValue(jsonPath.get("listaVeiculo.tipo[0]"));
        veiculo.setSituacaoValue(jsonPath.get("listaVeiculo.situacaoVeiculo[0]"));
        veiculo.setMercadoria(jsonPath.get("listaVeiculo.mercadoria[0]"));
        veiculo.setModelo(jsonPath.get("listaVeiculo.modelo[0]"));
    }

}
