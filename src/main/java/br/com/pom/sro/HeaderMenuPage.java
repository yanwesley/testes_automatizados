package br.com.pom.sro;

import org.openqa.selenium.By;

import java.util.List;

import static br.com.utils.ReporterUtils.addScreenshotToReport;

public class HeaderMenuPage extends GeralPage {

    public By labelMenu = By.cssSelector("body > sro-root > sro-shell > sro-header > mat-toolbar > mat-toolbar-row:nth-child(1) > div > span");
    public By btnMenuDashboard = By.id("menu-dashboard");
    public By btnMenuSinistros = By.id("menu-sinistro");
    public By btnMenuSindicancias = By.id("menu-sindicancia");
    public By btnMenuIncidente = By.id("menu-incidente");
    public By btnMenuAcoes = By.id("menu-acoes");
    public By btnMenuInfo = By.id("menu-info");
    public By btnCentralAcoes = By.id("menu-acoes");
    public By btnInvestigação = By.id("tab-investigacao");
    public By btnMenuConfiguracoes = By.id("menu-configuracoes");

    public HeaderMenuPage() {

    }

    public void validaLayoutMenuUsuarioSRO() {
        expectElementVisible(btnMenuDashboard);
        expectElementVisible(btnMenuSinistros);
        expectElementVisible(btnMenuSindicancias);
        expectElementVisible(btnMenuIncidente);
        expectElementVisible(btnMenuAcoes);
        expectElementVisible(btnMenuInfo);
    }

    public void validaLayoutMenuUsuarioCCO() {
        expectElementVisible(btnMenuDashboard);
        expectElementVisible(btnMenuSinistros);
        expectElementNotVisible(btnMenuSindicancias);
        expectElementVisible(btnMenuIncidente);
        expectElementVisible(btnMenuAcoes);
        expectElementVisible(btnMenuInfo);
    }

    public void validaLayoutMenuUsuarioCCO_VIA() {
        expectElementVisible(btnMenuDashboard);
        expectElementVisible(btnMenuSinistros);
        expectElementNotVisible(btnMenuSindicancias);
        expectElementVisible(btnMenuIncidente);
        expectElementVisible(btnMenuAcoes);
        expectElementVisible(btnMenuInfo);
    }

    public void validaLayoutMenuUsuarioCIPIAVIA() {
        expectElementVisible(btnMenuDashboard);
        expectElementNotVisible(btnMenuSinistros);
        expectElementVisible(btnMenuSindicancias);
        expectElementVisible(btnMenuIncidente);
        expectElementVisible(btnMenuAcoes);
        expectElementVisible(btnMenuInfo);
    }

    public void clicarTABSinistros() {
        clickAndHighlight(btnMenuSinistros);
    }

    public void validaNavegacaoDoMenu(List<String> rotas) {
        clickAndHighlight(btnMenuDashboard);
        assert expectTextInUrl(rotas.get(0));
        addScreenshotToReport("Validado navegação Dashboard.");

        clickAndHighlight(btnMenuSinistros);
        assert expectTextInUrl(rotas.get(1));
        addScreenshotToReport("Validado navegação Sinistros.");

        clickAndHighlight(btnMenuSindicancias);
        assert expectTextInUrl(rotas.get(2));
        addScreenshotToReport("Validado navegação Sindicâncias.");

        clickAndHighlight(btnMenuIncidente);
        assert expectTextInUrl(rotas.get(3));
        addScreenshotToReport("Validado navegação Incidentes.");

        clickAndHighlight(btnMenuAcoes);
        assert expectTextInUrl(rotas.get(4));
        addScreenshotToReport("Validado navegação Centrar de Ações.");

        clickAndHighlight(btnMenuInfo);
        assert expectTextInUrl(rotas.get(5));
        addScreenshotToReport("Validado navegação Informações");
    }

    public void clicarTABSindicancias() {
        clickAndHighlight(btnMenuSindicancias);
    }

    public void clicarTABCentralAcoes() {
        clickAndHighlight(btnCentralAcoes);
    }

    public void clicarTABDashboard() {
        clickAndHighlight(btnMenuDashboard);
    }

    public void clicarTABIncidentes() {
        clickAndHighlight(btnMenuIncidente);
    }

    public void clicarTABInvestigacao() {
        clickAndHighlight(btnInvestigação);
    }

    public void clicarTABInformacoes() {
        clickAndHighlight(btnMenuInfo);
    }

    public void clicarTABConfiguracoes() {
        clickAndHighlight(btnMenuConfiguracoes);
    }
}
