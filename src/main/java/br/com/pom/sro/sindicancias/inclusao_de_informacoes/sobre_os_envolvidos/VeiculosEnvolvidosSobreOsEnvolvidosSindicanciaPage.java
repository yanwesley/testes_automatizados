package br.com.pom.sro.sindicancias.inclusao_de_informacoes.sobre_os_envolvidos;

import br.com.api.model.sindicancia.sobre_os_envolvidos.VeiculoEnvolvido;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;
import java.util.function.IntFunction;

public class VeiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage extends GeralPage {
    Function<String, By> optionLocalDoObito = (index) -> By.id("obitoOcorreu-" + index);
    IntFunction<By> tdTableVeiculosEnvolvidosCellMarcaVeiculoPlaca = (index) -> By.id("table-veiculos-envolvidos-cell-marcaVeiculoPlaca-" + index);
    IntFunction<By> tdTableVeiculosEnvolvidosCellMotoristaRgCpfEndereco = (index) -> By.id("table-veiculos-envolvidos-cell-motoristaRgCpfEndereco-" + index);
    IntFunction<By> tdTableVeiculosEnvolvidosCellEvadiuLocal = (index) -> By.id("table-veiculos-envolvidos-cell-evadiuLocal-" + index);
    IntFunction<By> tdTableVeiculosEnvolvidosCellDanosAoVeiculoDescricao = (index) -> By.id("table-veiculos-envolvidos-cell-danosAoVeiculoDescricao-" + index);
    IntFunction<By> tdTableVeiculosEnvolvidosCellActions = (index) -> By.id("table-veiculos-envolvidos-cell-actions-" + index);
    IntFunction<By> buttonButtonEditVeiculoEnvolvido = (index) -> By.id("button-edit-veiculo-envolvido-" + index);
    IntFunction<By> buttonButtonRemoveVeiculoEnvolvido = (index) -> By.id("button-remove-veiculo-envolvido-" + index);
    IntFunction<By> buttonButtonDownloadVeiculoEnvolvido = (index) -> By.id("button-download-veiculo-envolvido-" + index);

    private By separatorVeiculosEnvolvidos = By.name("Veículos envolvidos");
    private By radioGroupHasVeiculosEnvolvidos = By.id("hasVeiculosEnvolvidos");
    private By radioButtonHasVeiculosEnvolvidosSim = By.id("hasVeiculosEnvolvidos-sim");
    private By inputHasVeiculosEnvolvidosSimInput = By.id("hasVeiculosEnvolvidos-sim-input");
    private By radioButtonHasVeiculosEnvolvidosNao = By.id("hasVeiculosEnvolvidos-nao");
    private By inputHasVeiculosEnvolvidosNaoInput = By.id("hasVeiculosEnvolvidos-nao-input");
    private By buttonButtonAddVeiculoEnvolvido = By.id("button-add-veiculo-envolvido");
    private By thTableVeiculosEnvolvidosHeaderMarcaVeiculoPlaca = By.id("table-veiculos-envolvidos-header-marcaVeiculoPlaca");
    private By thTableVeiculosEnvolvidosHeaderMotoristaRgCpfEndereco = By.id("table-veiculos-envolvidos-header-motoristaRgCpfEndereco");
    private By thTableVeiculosEnvolvidosHeaderEvadiuLocal = By.id("table-veiculos-envolvidos-header-evadiuLocal");
    private By thTableVeiculosEnvolvidosHeaderDanosAoVeiculoDescricao = By.id("table-veiculos-envolvidos-header-danosAoVeiculoDescricao");
    private By thTableVeiculosEnvolvidosHeaderActions = By.id("table-veiculos-envolvidos-header-actions");

    /*
    Modal add veículo
     */
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By inputMarca = By.id("marca");
    private By inputVeiculo = By.id("veiculo");
    private By inputPlaca = By.id("placa");
    private By inputMotorista = By.id("motorista");
    private By inputRg = By.id("rg");
    private By inputCpf = By.id("cpf");
    private By textareaEndereco = By.id("endereco");
    private By radioGroupHouveDanosAoVeiculo = By.id("houveDanosAoVeiculo");
    private By radioButtonHouveDanosAoVeiculoSim = By.id("houveDanosAoVeiculo-sim");
    private By inputHouveDanosAoVeiculoSimInput = By.id("houveDanosAoVeiculo-sim-input");
    private By radioButtonHouveDanosAoVeiculoNao = By.id("houveDanosAoVeiculo-nao");
    private By inputHouveDanosAoVeiculoNaoInput = By.id("houveDanosAoVeiculo-nao-input");
    private By textareaDanosAoVeiculoDescricao = By.id("danosAoVeiculoDescricao");
    private By radioGroupEvadiuLocal = By.id("evadiuLocal");
    private By radioButtonEvadiuLocalSim = By.id("evadiuLocal-sim");
    private By inputEvadiuLocalSimInput = By.id("evadiuLocal-sim-input");
    private By radioButtonEvadiuLocalNao = By.id("evadiuLocal-nao");
    private By inputEvadiuLocalNaoInput = By.id("evadiuLocal-nao-input");
    private By sroTitleLabelImagensDoVeiculoEnvolvido = By.name("Imagens do veículo envolvido");
    private By divImagensDoVeiculoEnvolvido = By.name("Imagens do veículo envolvido");
    private By sroFileUploadImagensUploadComponent = By.id("imagens-upload-component");
    private By inputInputFileUpload = By.id("input-file-upload");
    private By buttonBtnInputFileUpload = By.id("btn-input-file-upload");
    private By buttonBtnFileUploadSearch = By.id("btn-file-upload-search");
    private By ulFileUploadComponentListFiles = By.id("file-upload-component-list-files");
    private By buttonDialogButtonCancel = By.id("dialog-button-cancel");
    private By buttonDialogButtonSave = By.id("dialog-button-save");
    private By buttonDialogButtonSaveAddOther = By.id("dialog-button-save-add-other");


    /*
    Modal exclusao
     */

    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");

    public void clicarBtnSimModalExclusao() {
        clickAndHighlight(buttonDialogButtonYes);
    }


    public void validaExibicaoDaPagina() {
        expectElementVisible(separatorVeiculosEnvolvidos);
    }

    public void clicarBtnSimHouveVeiculos() {
        clickWithAction(radioButtonHasVeiculosEnvolvidosSim);
    }

    public void clicarBtnAdicionarVeiculo() {
        clickAndHighlight(buttonButtonAddVeiculoEnvolvido);
    }

    public void validaModalDeAdicionarNovaVeiculo() {
        expectElementVisible(inputMarca);
        expectElementVisible(inputVeiculo);
        expectElementVisible(inputPlaca);
        expectElementVisible(inputMotorista);
        expectElementVisible(inputRg);
        expectElementVisible(inputCpf);
        expectElementVisible(textareaEndereco);
        expectElementVisible(buttonDialogButtonCancel);
        expectElementVisible(buttonDialogButtonSave);
        expectElementVisible(buttonDialogButtonSaveAddOther);
    }

    public void preencherModalVeiculo(VeiculoEnvolvido veiculoEnvolvido, boolean edicao) {
        sendKeysWithJavaScript(inputMarca, veiculoEnvolvido.getMarca());
        sendKeysWithJavaScript(inputVeiculo, veiculoEnvolvido.getModelo());
        sendKeysWithJavaScript(inputPlaca, veiculoEnvolvido.getPlaca());
        sendKeysWithJavaScript(inputMotorista, veiculoEnvolvido.getMotorista());
        sendKeysWithJavaScript(inputRg, veiculoEnvolvido.getRG());
        sendKeysWithJavaScript(inputCpf, veiculoEnvolvido.getCPF());
        sendKeysWithJavaScript(textareaEndereco, veiculoEnvolvido.getEndereco());
        if (veiculoEnvolvido.isDanosAoVeiculo()) {
            clickWithAction(radioButtonHouveDanosAoVeiculoSim);
            sendKeysWithJavaScript(textareaDanosAoVeiculoDescricao, veiculoEnvolvido.getDescricaoDanosAoVeiculo());
        } else {
            clickWithAction(radioButtonHouveDanosAoVeiculoNao);
        }

        if (veiculoEnvolvido.isEvadiuDoLocal()) {
            clickWithAction(radioButtonEvadiuLocalSim);
        } else {
            clickWithAction(radioButtonEvadiuLocalNao);
        }

        if (!edicao) {
            uploadArquivo(2);
        }
    }


    public void clicarBtnSalvarModal() {
        clickAndHighlight(buttonDialogButtonSave);
    }

    public void validaVeiculoAdicionado(VeiculoEnvolvido veiculoEnvolvido, int linha) {
        linha--;

        String marcaVeiculoPlaca = veiculoEnvolvido.getMarca() + "\n" + veiculoEnvolvido.getModelo() + "\n" + veiculoEnvolvido.getPlaca();
        Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellMarcaVeiculoPlaca.apply(linha)), marcaVeiculoPlaca);


        String motoristaRgCpfEndereco =
                veiculoEnvolvido.getMotorista() + "\n" + veiculoEnvolvido.getRG() + "\n" + veiculoEnvolvido.getCPF() + "\n" + veiculoEnvolvido.getEndereco();
        Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellMotoristaRgCpfEndereco.apply(linha)), motoristaRgCpfEndereco);


        if (veiculoEnvolvido.isDanosAoVeiculo()) {
            Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellDanosAoVeiculoDescricao.apply(linha)), veiculoEnvolvido.getDescricaoDanosAoVeiculo());
        } else {
            Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellDanosAoVeiculoDescricao.apply(linha)), "--");
        }

        if (veiculoEnvolvido.isEvadiuDoLocal()) {
            Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellEvadiuLocal.apply(linha)), "Sim");
        } else {
            Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellEvadiuLocal.apply(linha)), "Não");
        }

        expectElementVisible(buttonButtonDownloadVeiculoEnvolvido.apply(linha));
    }

    public void validaQueTemVeiculo() {
        expectElementVisible(tdTableVeiculosEnvolvidosCellMarcaVeiculoPlaca.apply(0));
    }

    public void clicarBtnEditarVeiculo(int linha) {
        linha--;
        clickAndHighlight(buttonButtonEditVeiculoEnvolvido.apply(linha));
    }

    public void clicarBtnExcluirVeiculo(int linha) {
        linha--;
        clickAndHighlight(buttonButtonRemoveVeiculoEnvolvido.apply(linha));
    }

    public void validaQueOVeiculoFoiExcluido() {
        expectElementNotVisible(tdTableVeiculosEnvolvidosCellMarcaVeiculoPlaca.apply(0));
        expectElementNotVisible(tdTableVeiculosEnvolvidosCellMotoristaRgCpfEndereco.apply(0));
    }

    public void validaVeiculoAdicionadoReadonly(VeiculoEnvolvido veiculoEnvolvido, int linha) {
        linha--;

        String marcaVeiculoPlaca = veiculoEnvolvido.getMarca() + "\n" + veiculoEnvolvido.getModelo() + "\n" + veiculoEnvolvido.getPlaca();
        Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellMarcaVeiculoPlaca.apply(linha)), marcaVeiculoPlaca);


        String motoristaRgCpfEndereco =
                veiculoEnvolvido.getMotorista() + "\n" + veiculoEnvolvido.getRG() + "\n" + veiculoEnvolvido.getCPF() + "\n" + veiculoEnvolvido.getEndereco();
        Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellMotoristaRgCpfEndereco.apply(linha)), motoristaRgCpfEndereco);


        if (veiculoEnvolvido.isDanosAoVeiculo()) {
            Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellDanosAoVeiculoDescricao.apply(linha)), veiculoEnvolvido.getDescricaoDanosAoVeiculo());
        } else {
            Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellDanosAoVeiculoDescricao.apply(linha)), "--");
        }

        if (veiculoEnvolvido.isEvadiuDoLocal()) {
            Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellEvadiuLocal.apply(linha)), "Sim");
        } else {
            Assert.assertEquals(getText(tdTableVeiculosEnvolvidosCellEvadiuLocal.apply(linha)), "Não");
        }

        expectElementVisible(buttonButtonDownloadVeiculoEnvolvido.apply(linha));
        expectElementNotVisible(buttonButtonRemoveVeiculoEnvolvido.apply(linha));
        expectElementNotVisible(buttonButtonEditVeiculoEnvolvido.apply(linha));
        expectElementNotVisible(buttonButtonAddVeiculoEnvolvido);
    }
}
