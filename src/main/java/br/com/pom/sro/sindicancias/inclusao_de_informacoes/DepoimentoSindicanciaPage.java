package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.GeralPage;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class DepoimentoSindicanciaPage extends GeralPage {

    Function<Integer, By> cellBtnExcluirArquivo = (Integer index) -> By.id("table-anexos-sindicancias-btn-excluir-" + index);
    private Faker faker = new Faker();
    private String depoimentoInserido;
    private By separatorDepoimento = By.name("DEPOIMENTO");
    private By textAreaObservacao = By.id("observacaoControl");
    private By btnSalvarInformacao = By.id("salvarDadosVia");
    private By btnSalvarEAvancar = By.id("salvarAvancarDadosVia");
    private By tbArquivos = By.cssSelector("#table-anexos-sindicancias > tbody");
    private By btnConfirmarExclusaoArquivo = By.id("dialog-button-yes");

    public void validaQueEstaNaPaginaDeDepoimento() {
        expectElementVisible(separatorDepoimento);
    }

    public void insereArquivo(int qtdArquivo) {
        uploadArquivo(qtdArquivo);
    }

    public void insereObservacao() {
        depoimentoInserido = getRandomCharacters(2000);
        sendKeysWithJavaScript(textAreaObservacao, depoimentoInserido);
    }

    public void clicarBtnSalvarInformacao() {
        clickAndHighlight(btnSalvarInformacao);
    }

    public void validaQueOsArquivosForamAdicionados(int qtdArquivosAdicionados) {
        Assert.assertEquals(qtdArquivosAdicionados, countChildElement(tbArquivos, "tr"));
    }

    public void validaObservacaoInserida() {
        Assert.assertEquals(getValue(textAreaObservacao), depoimentoInserido);
    }

    public void clicarBtnSalvarEAvancar() {
        clickAndHighlight(btnSalvarEAvancar);
    }

    public void excluirArquivos(int qtdArquivosExcluidos) {
        if (countChildElement(tbArquivos, "tr") >= qtdArquivosExcluidos) {
            for (int i = 0; i < qtdArquivosExcluidos; i++) {
                clickAndHighlight(cellBtnExcluirArquivo.apply(0));
                clickAndHighlight(btnConfirmarExclusaoArquivo);
                validaMensagemSnackBar("Arquivo excluído com sucesso");
                waitTime(500);
            }
        }
    }

}
