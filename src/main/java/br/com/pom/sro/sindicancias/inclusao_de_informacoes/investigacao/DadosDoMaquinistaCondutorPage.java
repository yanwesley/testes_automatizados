package br.com.pom.sro.sindicancias.inclusao_de_informacoes.investigacao;

import br.com.api.model.sindicancia.investigacao.DadosDoMaquinistaCondutor;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class DadosDoMaquinistaCondutorPage extends GeralPage {

    private By radioGroupMaquinistaRumo = By.id("maquinistaRumo");
    private By radioButtonMaquinistaRumoSim = By.id("maquinistaRumo-sim");
    private By inputMaquinistaRumoSimInput = By.id("maquinistaRumo-sim-input");
    private By radioButtonMaquinistaRumoNao = By.id("maquinistaRumo-nao");
    private By inputMaquinistaRumoNaoInput = By.id("maquinistaRumo-nao-input");

    private By inputNomeMaquinista = By.id("nomeMaquinista");
    private By inputMatriculaMaquinista = By.id("matriculaMaquinista");
    private By inputTeSimuladoAssociadaCausa = By.id("teSimuladoAssociadaCausa");
    private By sroTitleLabelValoresDeTestes = By.name("Valores de testes");
    private By inputTesteSofridoAtual = By.id("testeSofridoAtual");
    private By inputTesteSofridoM1 = By.id("testeSofridoM1");
    private By inputTesteSofridoM2 = By.id("testeSofridoM2");
    private By inputTesteSofridoRelacionadoAcidenteAtual = By.id("testeSofridoRelacionadoAcidenteAtual");
    private By inputTesteSofridoRelacionadoAcidenteM1 = By.id("testeSofridoRelacionadoAcidenteM1");
    private By inputTesteSofridoRelacionadoAcidenteM2 = By.id("testeSofridoRelacionadoAcidenteM2");
    private By inputOutrosTestesInsatisfatoriosAtual = By.id("outrosTestesInsatisfatoriosAtual");
    private By inputOutrosTestesInsatisfatoriosM1 = By.id("outrosTestesInsatisfatoriosM1");
    private By inputOutrosTestesInsatisfatoriosM2 = By.id("outrosTestesInsatisfatoriosM2");
    private By inputNotaProvaAtual = By.id("notaProvaAtual");
    private By inputNotaProvaM1 = By.id("notaProvaM1");
    private By inputNotaProvaM2 = By.id("notaProvaM2");
    private By inputDataReciclagemRO = By.id("dataReciclagemRO");
    private By inputNotaUltimaReciclagem = By.id("notaUltimaReciclagem");
    private By inputDataUltimaReciclagemRelacionadoAcidente = By.id("dataUltimaReciclagemRelacionadoAcidente");
    private By inputNotaUltimaReciclagemRelacionadoAcidente = By.id("notaUltimaReciclagemRelacionadoAcidente");
    private By textAreaQuestoesRespondidasIncorretamente = By.id("questoesRespondidasIncorretamente");
    private By inputHorasServicoMomentoAcidente = By.id("horasServicoMomentoAcidente");
    private By inputDiasAposUltimoDescanso = By.id("diasAposUltimoDescanso");
    private By inputMesesAposFerias = By.id("mesesAposFerias");
    private By btnSearchMaquinista = By.id("button-search-maquinista");

    public void preencherDados(DadosDoMaquinistaCondutor dadosDoMaquinistaCondutor) {
        scrollToElement(radioGroupMaquinistaRumo);
        waitTime(500);

        if (dadosDoMaquinistaCondutor.isoMaquinistaEDaRumo()) {
            clickWithAction(radioButtonMaquinistaRumoSim);

            clickAndHighlight(btnSearchMaquinista);

            selecionarResponsavel(dadosDoMaquinistaCondutor.getNomeDoMaquinista());
            dadosDoMaquinistaCondutor.setNomeDoMaquinista(getValue(inputNomeMaquinista));
            dadosDoMaquinistaCondutor.setCS(getValue(inputMatriculaMaquinista));

        } else {
            clickWithAction(radioButtonMaquinistaRumoNao);
            expectElementClickable(inputNomeMaquinista);
            sendKeys(inputNomeMaquinista, dadosDoMaquinistaCondutor.getNomeDoMaquinista());
            sendKeys(inputMatriculaMaquinista, dadosDoMaquinistaCondutor.getDocumento());
        }

        sendKeys(inputTeSimuladoAssociadaCausa, dadosDoMaquinistaCondutor.getTESimuladoAssociadaACausa());

        sendKeys(inputTesteSofridoAtual, dadosDoMaquinistaCondutor.getTestesSofridosMesAtual());
        sendKeys(inputTesteSofridoM1, dadosDoMaquinistaCondutor.getTestesSofridosMesM1());
        sendKeys(inputTesteSofridoM2, dadosDoMaquinistaCondutor.getTestesSofridosMesM2());

        sendKeys(inputTesteSofridoRelacionadoAcidenteAtual, dadosDoMaquinistaCondutor.getTestesSofridosRelacionadoComOAcidenteMesAtual());
        sendKeys(inputTesteSofridoRelacionadoAcidenteM1, dadosDoMaquinistaCondutor.getTestesSofridosRelacionadoComOAcidenteMesM1());
        sendKeys(inputTesteSofridoRelacionadoAcidenteM2, dadosDoMaquinistaCondutor.getTestesSofridosRelacionadoComOAcidenteMesM2());

        sendKeys(inputOutrosTestesInsatisfatoriosAtual, dadosDoMaquinistaCondutor.getOutrosTestesInsatisfatoriosMesAtual());
        sendKeys(inputOutrosTestesInsatisfatoriosM1, dadosDoMaquinistaCondutor.getOutrosTestesInsatisfatoriosMesM1());
        sendKeys(inputOutrosTestesInsatisfatoriosM2, dadosDoMaquinistaCondutor.getOutrosTestesInsatisfatoriosMesM2());

        sendKeys(inputNotaProvaAtual, dadosDoMaquinistaCondutor.getNotaProvaMesAtual());
        sendKeys(inputNotaProvaM1, dadosDoMaquinistaCondutor.getNotaProvaMesM1());
        sendKeys(inputNotaProvaM2, dadosDoMaquinistaCondutor.getNotaProvaMesM2());

        clearForce(inputDataReciclagemRO);
        sendKeys(inputDataReciclagemRO, dadosDoMaquinistaCondutor.getDataUltimaReciclagemOuTreinamentoRO());
        sendKeys(inputNotaUltimaReciclagem, dadosDoMaquinistaCondutor.getNotaUltimaReciclagemRO());

        clearForce(inputDataUltimaReciclagemRelacionadoAcidente);
        sendKeys(inputDataUltimaReciclagemRelacionadoAcidente, dadosDoMaquinistaCondutor.getDataUltimaReciclagemOuTreinamentoPORelacionadoAoAcidente());
        sendKeys(inputNotaUltimaReciclagemRelacionadoAcidente, dadosDoMaquinistaCondutor.getNotaUltimaReciclagemPORelacionadoAoAcidente());

        sendKeysWithJavaScript(textAreaQuestoesRespondidasIncorretamente, dadosDoMaquinistaCondutor.getQuestoesRespondidasIncorretamente());

        sendKeys(inputHorasServicoMomentoAcidente, dadosDoMaquinistaCondutor.getHorasDeServicosNoMomentoDoAcidente());
        sendKeys(inputDiasAposUltimoDescanso, dadosDoMaquinistaCondutor.getDiasAposOUltimoDescansoSemanal());
        sendKeys(inputMesesAposFerias, dadosDoMaquinistaCondutor.getMesesAposAsUltimasFerias());

    }

    public void validarDados(DadosDoMaquinistaCondutor dadosDoMaquinistaCondutor) {
        scrollToElement(radioGroupMaquinistaRumo);

        if (dadosDoMaquinistaCondutor.isoMaquinistaEDaRumo()) {
            Assert.assertTrue(isRadioChecked(radioButtonMaquinistaRumoSim));
            Assert.assertEquals(getValue(inputMatriculaMaquinista), dadosDoMaquinistaCondutor.getCS());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonMaquinistaRumoNao));
            Assert.assertEquals(getValue(inputMatriculaMaquinista), dadosDoMaquinistaCondutor.getDocumento());
        }

        Assert.assertEquals(getValue(inputNomeMaquinista), dadosDoMaquinistaCondutor.getNomeDoMaquinista());

        Assert.assertEquals(getValue(inputTeSimuladoAssociadaCausa), dadosDoMaquinistaCondutor.getTESimuladoAssociadaACausa());

        Assert.assertEquals(getValue(inputTesteSofridoAtual), dadosDoMaquinistaCondutor.getTestesSofridosMesAtual());
        Assert.assertEquals(getValue(inputTesteSofridoM1), dadosDoMaquinistaCondutor.getTestesSofridosMesM1());
        Assert.assertEquals(getValue(inputTesteSofridoM2), dadosDoMaquinistaCondutor.getTestesSofridosMesM2());

        Assert.assertEquals(getValue(inputTesteSofridoRelacionadoAcidenteAtual), dadosDoMaquinistaCondutor.getTestesSofridosRelacionadoComOAcidenteMesAtual());
        Assert.assertEquals(getValue(inputTesteSofridoRelacionadoAcidenteM1), dadosDoMaquinistaCondutor.getTestesSofridosRelacionadoComOAcidenteMesM1());
        Assert.assertEquals(getValue(inputTesteSofridoRelacionadoAcidenteM2), dadosDoMaquinistaCondutor.getTestesSofridosRelacionadoComOAcidenteMesM2());

        Assert.assertEquals(getValue(inputOutrosTestesInsatisfatoriosAtual), dadosDoMaquinistaCondutor.getOutrosTestesInsatisfatoriosMesAtual());
        Assert.assertEquals(getValue(inputOutrosTestesInsatisfatoriosM1), dadosDoMaquinistaCondutor.getOutrosTestesInsatisfatoriosMesM1());
        Assert.assertEquals(getValue(inputOutrosTestesInsatisfatoriosM2), dadosDoMaquinistaCondutor.getOutrosTestesInsatisfatoriosMesM2());

        Assert.assertEquals(getValue(inputNotaProvaAtual), dadosDoMaquinistaCondutor.getNotaProvaMesAtual());
        Assert.assertEquals(getValue(inputNotaProvaM1), dadosDoMaquinistaCondutor.getNotaProvaMesM1());
        Assert.assertEquals(getValue(inputNotaProvaM2), dadosDoMaquinistaCondutor.getNotaProvaMesM2());

        Assert.assertEquals(getValue(inputDataReciclagemRO), dadosDoMaquinistaCondutor.getDataUltimaReciclagemOuTreinamentoRO());
        Assert.assertEquals(getValue(inputNotaUltimaReciclagem), dadosDoMaquinistaCondutor.getNotaUltimaReciclagemRO());

        Assert.assertEquals(getValue(inputDataUltimaReciclagemRelacionadoAcidente), dadosDoMaquinistaCondutor.getDataUltimaReciclagemOuTreinamentoPORelacionadoAoAcidente());
        Assert.assertEquals(getValue(inputNotaUltimaReciclagemRelacionadoAcidente), dadosDoMaquinistaCondutor.getNotaUltimaReciclagemPORelacionadoAoAcidente());

        Assert.assertEquals(getValue(textAreaQuestoesRespondidasIncorretamente), dadosDoMaquinistaCondutor.getQuestoesRespondidasIncorretamente());

        Assert.assertEquals(getValue(inputHorasServicoMomentoAcidente), dadosDoMaquinistaCondutor.getHorasDeServicosNoMomentoDoAcidente());
        Assert.assertEquals(getValue(inputDiasAposUltimoDescanso), dadosDoMaquinistaCondutor.getDiasAposOUltimoDescansoSemanal());
        Assert.assertEquals(getValue(inputMesesAposFerias), dadosDoMaquinistaCondutor.getMesesAposAsUltimasFerias());
    }
}
