package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.DadosVia;
import br.com.api.model.sindicancia.investigacao.CriticaDeViagem;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import static br.com.utils.ReporterUtils.addScreenshotToReport;

public class CriticaDaViagemSindicanciaPage extends GeralPage {

    private By separatorCriticaDeViagem = By.name("Crítica de viagem");
    private By separatorAnaliseDaViagem = By.name("Análise de viagem");
    private By separatorAnaliseDaViagemComparativaUm = By.name("Análise de viagem comparativa 1");
    private By separatorAnaliseDaViagemComparativaDois = By.name("Análise de viagem comparativa 2");
    private By separatorLogDaLocomotiva = By.name("Log da locomotiva");
    private By textAreaConducaoDoTrem = By.id("conducaoTrem");
    private By textAreaCriticaDaViagem = By.id("criticaViagem");
    private By textAreaConsideracoesFinais = By.id("condisideracoesFinais");
    private By descricaoJustificativa = By.id("justificativa-descricao");
    private By imgAnaliseViagem = By.id("arquivo-sindicancia-analise-viagem");
    private By imgAnaliseViagemComparativaUm = By.id("arquivo-sindicancia-analise-comparativa-1");
    private By imgAnaliseViagemComparativaDois = By.id("arquivo-sindicancia-analise-comparativa-2");
    private By imgLogDaLocomotiva = By.id("arquivo-sindicancia-log-locomotiva");
    private By btnSalvarInformacoes = By.id("salvar-informacoes-trilho");


    public void validaQueEstaNaPaginaDeCriticaDaViagem() {
        expectElementVisible(separatorCriticaDeViagem);
    }

    public void validaInformacoesPreenchidasNaTelaAnaliseDaViagem(DadosVia dadosVia) {

        Assert.assertEquals(getText(descricaoJustificativa), dadosVia.getJustificativaInserida());
        addScreenshotToReport("Validou Justificativa");

        scrollToElement(separatorAnaliseDaViagem);
        Assert.assertTrue(getAttribute(imgAnaliseViagem, "src").contains("download"));
        addScreenshotToReport("Validou Análise da viagem");

        scrollToElement(separatorAnaliseDaViagemComparativaUm);
        Assert.assertTrue(getAttribute(imgAnaliseViagemComparativaUm, "src").contains("download"));
        addScreenshotToReport("Validou Análise da viagem comparativa 1");

        scrollToElement(separatorAnaliseDaViagemComparativaDois);
        Assert.assertTrue(getAttribute(imgAnaliseViagemComparativaDois, "src").contains("download"));
        addScreenshotToReport("Validou Análise da viagem comparativa 2");

        scrollToElement(separatorLogDaLocomotiva);
        Assert.assertTrue(getAttribute(imgLogDaLocomotiva, "src").contains("download"));
        addScreenshotToReport("Validou Log da locomotiva");
    }

    public void preencheCriticaDeViagem(CriticaDeViagem criticaDeViagem) {
        scrollToElement(separatorCriticaDeViagem);
        sendKeysWithJavaScript(textAreaConducaoDoTrem, criticaDeViagem.getComentarioConducaoDoTrem());
        sendKeysWithJavaScript(textAreaCriticaDaViagem, criticaDeViagem.getComentarioCriticaDaViagem());
        sendKeysWithJavaScript(textAreaConsideracoesFinais, criticaDeViagem.getComentarioConsideracoesFinais());
    }

    public void clicarBtnSalvarInformacoesCriticaDaViagem() {
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void validaCriticaDeViagem(CriticaDeViagem criticaDeViagem) {
        scrollToElement(separatorCriticaDeViagem);
        Assert.assertEquals(getValue(textAreaConducaoDoTrem), criticaDeViagem.getComentarioConducaoDoTrem());
        Assert.assertEquals(getValue(textAreaCriticaDaViagem), criticaDeViagem.getComentarioCriticaDaViagem());
        Assert.assertEquals(getValue(textAreaConsideracoesFinais), criticaDeViagem.getComentarioConsideracoesFinais());
    }

    public void validaBotaoSalvarInformacoesDisabled() {
        scrollToElement(btnSalvarInformacoes);
        Assert.assertTrue(isDisabled(btnSalvarInformacoes));
    }
}
