package br.com.pom.sro.sindicancias.inclusao_de_informacoes.investigacao;

import br.com.api.model.sindicancia.investigacao.FormacaoDoTrem;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class FormacaoDoTremPage extends GeralPage {

    private By radioGroupHaviamVagoesGatos = By.id("haviamVagoesGatos");
    private By radioButtonHaviamVagoesGatosSim = By.id("haviamVagoesGatos-sim");
    private By inputHaviamVagoesGatosSimInput = By.id("haviamVagoesGatos-sim-input");
    private By radioButtonHaviamVagoesGatosNao = By.id("haviamVagoesGatos-nao");
    private By inputHaviamVagoesGatosNaoInput = By.id("haviamVagoesGatos-nao-input");
    private By inputQuantosVagoesGatos = By.id("quantosVagoesGatos");

    public void preencherDados(FormacaoDoTrem formacaoDoTrem) {
        waitTime(200);
        if (formacaoDoTrem.isHaviamVagoesGato()) {
            clickWithAction(radioButtonHaviamVagoesGatosSim);
            sendKeys(inputQuantosVagoesGatos, formacaoDoTrem.getQuantosVagoesGato());
        } else {
            clickWithAction(radioButtonHaviamVagoesGatosNao);
        }
    }

    public void validarDados(FormacaoDoTrem formacaoDoTrem) {
        scrollToElement(radioGroupHaviamVagoesGatos);
        if (formacaoDoTrem.isHaviamVagoesGato()) {
            Assert.assertTrue(isRadioChecked(radioButtonHaviamVagoesGatosSim));
            Assert.assertEquals(getValue(inputQuantosVagoesGatos), formacaoDoTrem.getQuantosVagoesGato());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonHaviamVagoesGatosNao));
        }
    }
}
