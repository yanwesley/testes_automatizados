package br.com.pom.sro.sindicancias.inclusao_de_informacoes.questionario_maquinista;

import br.com.api.model.sindicancia.questionario.QuestionarioMaquinista;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class QuestionarioMaquinistaSindicanciaPage extends GeralPage {

    IntFunction<By> radioButtonHaviaRestricaoVelocidade = (index) -> By.id("haviaRestricaoVelocidade-" + index);
    IntFunction<By> radioButtonRestricaoObservadaBoletim = (index) -> By.id("restricaoObservadaBoletim-" + index);
    IntFunction<By> radioButtonCondicaoIrregularVista = (index) -> By.id("condicaoIrregularVista-" + index);
    IntFunction<By> radioButtonHaviaCondicaoIrregularEquipamento = (index) -> By.id("haviaCondicaoIrregularEquipamento-" + index);
    IntFunction<By> radioButtonFoiSentidaAcaoFolgaTrem = (index) -> By.id("foiSentidaAcaoFolgaTrem-" + index);
    IntFunction<By> radioButtonQualAcaoFolgq = (index) -> By.id("qualAcaoFolga-" + index);
    IntFunction<By> radioButtonHouveAlgumaIndicacaoAnormalFluxoAr = (index) -> By.id("houveAlgumaIndicacaoAnormalFluxoAr-" + index);
    IntFunction<By> radioButtonHouveMudancaBruscaVelocidade = (index) -> By.id("houveMudancaBruscaVelocidade-" + index);
    IntFunction<By> radioButtonTremEstava = (index) -> By.id("tremEstava-" + index);
    IntFunction<By> radioButtonNaHoraLocomotivaEstava = (index) -> By.id("naHoraLocomotivaEstava-" + index);
    IntFunction<By> radioButtonLocomotivasEstavamTracionadasAdequadamente = (index) -> By.id("locomotivasEstavamTracionadasAdequadamente-" + index);
    IntFunction<By> radioButtonHouveAlgumaLocomotivaDeslizando = (index) -> By.id("houveAlgumaLocomotivaDeslizando-" + index);
    IntFunction<By> radioButtonEfeitoNaAcaoFolgaTrem = (index) -> By.id("efeitoNaAcaoFolgaTrem-" + index);
    IntFunction<By> radioButtonHouveVazamento = (index) -> By.id("houveVazamento-" + index);
    IntFunction<By> radioButtonFreioIndependenteUtilizado = (index) -> By.id("freioIndependenteUtilizado-" + index);
    IntFunction<By> radioButtonFreioDinamicoOscilando = (index) -> By.id("freioDinamicoOscilando-" + index);
    IntFunction<By> radioButtonIndicacaoExcessoFrenagem = (index) -> By.id("indicacaoExcessoFrenagem-" + index);
    private By radioGroupAcidenteDuranteManobra = By.id("acidenteDuranteManobra");
    private By radioButtonAcidenteDuranteManobraSim = By.id("acidenteDuranteManobra-sim");
    private By inputAcidenteDuranteManobraSimInput = By.id("acidenteDuranteManobra-sim-input");
    private By radioButtonAcidenteDuranteManobraNao = By.id("acidenteDuranteManobra-nao");
    private By inputAcidenteDuranteManobraNaoInput = By.id("acidenteDuranteManobra-nao-input");
    private By inputKmParadaLocomotiva = By.id("kmParadaLocomotiva");
    private By sroTitleLabelDados = By.name("Dados");
    private By radioGroupHaviaRestricaoVelocidade = By.id("haviaRestricaoVelocidade");
    private By inputVelocidadeRestricao = By.id("velocidadeRestricao");
    private By radioGroupRestricaoObservadaBoletim = By.id("restricaoObservadaBoletim");
    private By inputVelocidadeRestricaoBoletim = By.id("velocidadeRestricaoBoletim");
    private By radioGroupCondicaoIrregularVista = By.id("condicaoIrregularVista");
    private By textareaQualCondicaoIrregular = By.id("qualCondicaoIrregular");
    private By radioGroupHaviaCondicaoIrregularEquipamento = By.id("haviaCondicaoIrregularEquipamento");
    private By textareaQualCondicaoIrregularEquipamento = By.id("qualCondicaoIrregularEquipamento");
    private By radioGroupFoiSentidaAcaoFolgaTrem = By.id("foiSentidaAcaoFolgaTrem");
    private By radioGroupQualAcaoFolga = By.id("qualAcaoFolga");
    private By textareaOnde = By.id("onde");
    private By radioGroupHouveAlgumaIndicacaoAnormalFluxoAr = By.id("houveAlgumaIndicacaoAnormalFluxoAr");
    private By textareaQualIndicacaoAnormalFluxoAr = By.id("qualIndicacaoAnormalFluxoAr");
    private By radioGroupHouveMudancaBruscaVelocidade = By.id("houveMudancaBruscaVelocidade");
    private By textareaExpliqueMudancaBrusca = By.id("expliqueMudancaBrusca");
    private By radioGroupTremEstava = By.id("tremEstava");
    private By radioGroupNaHoraLocomotivaEstava = By.id("naHoraLocomotivaEstava");
    private By inputQualAmperagem = By.id("qualAmperagem");
    private By sroTitleLabelTracaoUtilizadaAntesDoDescarrilamento = By.name("Tração utilizada antes do descarrilamento");
    private By radioGroupLocomotivasEstavamTracionadasAdequadamente = By.id("locomotivasEstavamTracionadasAdequadamente");
    private By radioGroupHouveAlgumaLocomotivaDeslizando = By.id("houveAlgumaLocomotivaDeslizando");
    private By radioGroupEfeitoNaAcaoFolgaTrem = By.id("efeitoNaAcaoFolgaTrem");
    private By sroTitleLabelAplicacaoDeFreioAArEAliVioAntesDoDescarrilamento = By.name("Aplicação de freio a ar e alívio antes do descarrilamento");
    private By radioGroupHouveVazamento = By.id("houveVazamento");
    private By inputQuantasLibrasMinuto = By.id("quantasLibrasMinuto");
    private By radioGroupFreioIndependenteUtilizado = By.id("freioIndependenteUtilizado");
    private By inputFreioQualKm = By.id("freioQualKm");
    private By inputQualAplicacaoFreio = By.id("qualAplicacaoFreio");
    private By sroTitleLabelFreioDinamicoUtilizadoAntesENoMomentoDoDescarrilamento = By.name("Freio dinâmico utilizado antes e no momento do descarrilamento");
    private By inputQuantasLocomotivasFreioDinamicoFuncionando = By.id("quantasLocomotivasFreioDinamicoFuncionando");
    private By inputAmperagemAntesDescarrilamento = By.id("amperagemAntesDescarrilamento");
    private By radioGroupFreioDinamicoOscilando = By.id("freioDinamicoOscilando");
    private By inputOscilandoQuanto = By.id("oscilandoQuanto");
    private By radioGroupIndicacaoExcessoFrenagem = By.id("indicacaoExcessoFrenagem");
    private By radioGroupHouveDeslizamento = By.id("houveDeslizamento");
    private By radioButtonHouveDeslizamentoSim = By.id("houveDeslizamento-sim");
    private By radioButtonHouveDeslizamentoNao = By.id("houveDeslizamento-nao");

    public void preencherDados(QuestionarioMaquinista questionarioMaquinista) {
        waitTime(300);
        if (questionarioMaquinista.isAcidenteDuranteAObra()) {

            clickWithAction(radioButtonAcidenteDuranteManobraSim);

            sendKeys(inputKmParadaLocomotiva, questionarioMaquinista.getInformeOKMDaParadaDaLocomotiva());
        } else {
            clickWithAction(radioButtonAcidenteDuranteManobraNao);
        }

        questionarioMaquinista.setHaviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeValue(
                selectOptionAndReturnValue(radioButtonHaviaRestricaoVelocidade,
                        questionarioMaquinista.getHaviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeIndex()
                )
        );

        if (questionarioMaquinista.getHaviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeIndex() == 0) {
            sendKeys(inputVelocidadeRestricao, questionarioMaquinista.getQualAVelocidadePermitidaNoLocalObservada());
        }

        questionarioMaquinista.setHaviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesValue(
                selectOptionAndReturnValue(radioButtonRestricaoObservadaBoletim,
                        questionarioMaquinista.getHaviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesIndex()
                )
        );


        if (questionarioMaquinista.getHaviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesIndex() == 0) {
            sendKeys(inputVelocidadeRestricaoBoletim, questionarioMaquinista.getQualAVelocidadePermitidaNoLocalNaoObservada());
        }


        questionarioMaquinista.setHaviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteValue(
                selectOptionAndReturnValue(radioButtonCondicaoIrregularVista,
                        questionarioMaquinista.getHaviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteIndex()
                )
        );

        if (questionarioMaquinista.getHaviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteIndex() == 0) {
            sendKeysWithJavaScript(textareaQualCondicaoIrregular, questionarioMaquinista.getQualCondicaoHaviaAlgumaCondicaoIrregularNoEquipamento());
        }


        questionarioMaquinista.setHaviaAlgumaCondicaoIrregularNoEquipamentoValue(
                selectOptionAndReturnValue(radioButtonHaviaCondicaoIrregularEquipamento,
                        questionarioMaquinista.getHaviaAlgumaCondicaoIrregularNoEquipamentoIndex()
                )
        );

        if (questionarioMaquinista.getHaviaAlgumaCondicaoIrregularNoEquipamentoIndex() == 0) {
            sendKeysWithJavaScript(textareaQualCondicaoIrregularEquipamento, questionarioMaquinista.getQualCondicaoHaviaAlgumaCondicaoIrregularNoEquipamento());
        }


        questionarioMaquinista.setFoiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremValue(
                selectOptionAndReturnValue(radioButtonFoiSentidaAcaoFolgaTrem,
                        questionarioMaquinista.getFoiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremIndex()
                )
        );


        if (questionarioMaquinista.getFoiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremIndex() == 0) {

            questionarioMaquinista.setIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue(
                    selectOptionAndReturnValue(radioButtonQualAcaoFolgq,
                            questionarioMaquinista.getIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex()
                    )
            );

            if (questionarioMaquinista.getIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex() == 0) {

                sendKeysWithJavaScript(textareaOnde, questionarioMaquinista.getOndeIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTrem());

            }

        }

        questionarioMaquinista.setHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArValue(
                selectOptionAndReturnValue(radioButtonHouveAlgumaIndicacaoAnormalFluxoAr,
                        questionarioMaquinista.getHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArIndex()
                )
        );

        if (questionarioMaquinista.getHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArIndex() == 0) {
            sendKeysWithJavaScript(textareaQualIndicacaoAnormalFluxoAr, questionarioMaquinista.getQualHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeAr());
        }


        questionarioMaquinista.setHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoValue(
                selectOptionAndReturnValue(radioButtonHouveMudancaBruscaVelocidade,
                        questionarioMaquinista.getHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoIndex()
                )
        );

        if (questionarioMaquinista.getHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoIndex() == 0) {
            sendKeysWithJavaScript(textareaExpliqueMudancaBrusca, questionarioMaquinista.getExpliqueHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamento());
        }


        questionarioMaquinista.setoTremEstavaValue(
                selectOptionAndReturnValue(radioButtonTremEstava,
                        questionarioMaquinista.getoTremEstavaIndex()
                )
        );

        questionarioMaquinista.setNaHoraDoAcidenteAsLocomotivasEstavamEmValue(
                selectOptionAndReturnValue(radioButtonNaHoraLocomotivaEstava,
                        questionarioMaquinista.getNaHoraDoAcidenteAsLocomotivasEstavamEmIndex()
                )
        );

        sendKeys(inputQualAmperagem, questionarioMaquinista.getQualAAmperagem());


        questionarioMaquinista.setPeloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteValue(
                selectOptionAndReturnValue(radioButtonLocomotivasEstavamTracionadasAdequadamente,
                        questionarioMaquinista.getPeloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteIndex()
                )
        );


        questionarioMaquinista.setHaviaAlgumaLocomotivaDeslizandoOuPerdendoForcaValue(
                selectOptionAndReturnValue(radioButtonHouveAlgumaLocomotivaDeslizando,
                        questionarioMaquinista.getHaviaAlgumaLocomotivaDeslizandoOuPerdendoForcaIndex()
                )
        );

        if (questionarioMaquinista.getHaviaAlgumaLocomotivaDeslizandoOuPerdendoForcaIndex() == 0) {
            questionarioMaquinista.setIssoTeveAlgumEfeitoNaAcaoDasFolgasDoTremValue(
                    selectOptionAndReturnValue(radioButtonEfeitoNaAcaoFolgaTrem,
                            questionarioMaquinista.getIssoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex()
                    )
            );

        }

        questionarioMaquinista.setHouveVazamentoDoEncanamentoGeralValue(
                selectOptionAndReturnValue(radioButtonHouveVazamento,
                        questionarioMaquinista.getHouveVazamentoDoEncanamentoGeralIndex()
                )
        );

        if (questionarioMaquinista.getHouveVazamentoDoEncanamentoGeralIndex() == 0) {
            sendKeys(inputQuantasLibrasMinuto, questionarioMaquinista.getQuantasLibrasPorMinuto());
        }


        questionarioMaquinista.setoFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoValue(
                selectOptionAndReturnValue(radioButtonFreioIndependenteUtilizado,
                        questionarioMaquinista.getOFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoIndex()
                )
        );


        if (questionarioMaquinista.getOFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoIndex() == 0) {
            sendKeys(inputFreioQualKm, questionarioMaquinista.getEmQualKM());
            sendKeys(inputQualAplicacaoFreio, questionarioMaquinista.geteQualFoiAAplicacao());
        }

        sendKeys(inputQuantasLocomotivasFreioDinamicoFuncionando, questionarioMaquinista.getPeloConhecimentoDoMaquinistaQuantasLocomotivasEstavamComOFreioDinamicoFuncionando());
        sendKeys(inputAmperagemAntesDescarrilamento, questionarioMaquinista.getQualAAmperagemAntesDoDescarrilamento());


        questionarioMaquinista.setoFreioDinamicoEstavaOscilandoValue(
                selectOptionAndReturnValue(radioButtonFreioDinamicoOscilando,
                        questionarioMaquinista.getoFreioDinamicoEstavaOscilandoIndex()
                )
        );

        if (questionarioMaquinista.getoFreioDinamicoEstavaOscilandoIndex() == 0) {
            sendKeys(inputOscilandoQuanto, questionarioMaquinista.getQuantoOFreioDinamicoEstavaOscilando());
        }

        questionarioMaquinista.setOuveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoValue(
                selectOptionAndReturnValue(radioButtonIndicacaoExcessoFrenagem,
                        questionarioMaquinista.getOuveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoIndex()
                )
        );

        if (questionarioMaquinista.isHouveDeslizamentoDeRodaAntesOuNoMomentoDoDescarrilamento()) {
            clickWithAction(radioButtonHouveDeslizamentoSim);
        } else {
            clickWithAction(radioButtonHouveDeslizamentoNao);
        }
    }


    public void validarDados(QuestionarioMaquinista questionarioMaquinista) {
        if (questionarioMaquinista.isAcidenteDuranteAObra()) {
            Assert.assertTrue(isRadioChecked(radioButtonAcidenteDuranteManobraSim));
            Assert.assertEquals(getValue(inputKmParadaLocomotiva), questionarioMaquinista.getInformeOKMDaParadaDaLocomotiva());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonAcidenteDuranteManobraNao));
        }

        Assert.assertTrue(isRadioChecked(radioButtonHaviaRestricaoVelocidade.apply(
                questionarioMaquinista.getHaviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeIndex()
                ))
        );

        if (questionarioMaquinista.getHaviaAlgumaRestricaoDeVelocidadeNoBoletimDeRestricoesNoLocalOuProximidadeIndex() == 0) {
            Assert.assertEquals(getValue(inputVelocidadeRestricao), questionarioMaquinista.getQualAVelocidadePermitidaNoLocalObservada());
        }

        Assert.assertTrue(isRadioChecked(radioButtonRestricaoObservadaBoletim.apply(
                questionarioMaquinista.getHaviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesIndex()
                ))
        );


        if (questionarioMaquinista.getHaviaAlgumaRestricaoDeVelocidadeNaoObservadaNoBoletimDeRestricoesNoLocalOuProximidadesIndex() == 0) {
            Assert.assertEquals(getValue(inputVelocidadeRestricaoBoletim), questionarioMaquinista.getQualAVelocidadePermitidaNoLocalNaoObservada());
        }


        Assert.assertTrue(isRadioChecked(radioButtonCondicaoIrregularVista.apply(
                questionarioMaquinista.getHaviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteIndex()
                ))
        );

        if (questionarioMaquinista.getHaviaAlgumaCondicaoIrregularVistaOuSentidaNaViaPermanenteOuProximidadesOuNoLocalDoAcidenteIndex() == 0) {
            Assert.assertEquals(getValue(textareaQualCondicaoIrregular), questionarioMaquinista.getQualCondicaoHaviaAlgumaCondicaoIrregularNoEquipamento());
        }


        Assert.assertTrue(isRadioChecked(radioButtonHaviaCondicaoIrregularEquipamento.apply(
                questionarioMaquinista.getHaviaAlgumaCondicaoIrregularNoEquipamentoIndex()
                ))
        );

        if (questionarioMaquinista.getHaviaAlgumaCondicaoIrregularNoEquipamentoIndex() == 0) {
            Assert.assertEquals(getValue(textareaQualCondicaoIrregularEquipamento), questionarioMaquinista.getQualCondicaoHaviaAlgumaCondicaoIrregularNoEquipamento());
        }


        Assert.assertTrue(isRadioChecked(radioButtonFoiSentidaAcaoFolgaTrem.apply(
                questionarioMaquinista.getFoiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremIndex()
                ))
        );


        if (questionarioMaquinista.getFoiSentidaNaLocomotivaAlgumaAcaoNasFolgasDoTremIndex() == 0) {

            Assert.assertTrue(isRadioChecked(radioButtonQualAcaoFolgq.apply(
                    questionarioMaquinista.getIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex()
                    ))
            );

            if (questionarioMaquinista.getIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex() == 0) {

                Assert.assertEquals(getValue(textareaOnde), questionarioMaquinista.getOndeIstoTeveAlgumEfeitoNaAcaoDasFolgasDoTrem());

            }

        }

        Assert.assertTrue(isRadioChecked(radioButtonHouveAlgumaIndicacaoAnormalFluxoAr.apply(
                questionarioMaquinista.getHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArIndex()
                ))
        );

        if (questionarioMaquinista.getHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeArIndex() == 0) {
            Assert.assertEquals(getValue(textareaQualIndicacaoAnormalFluxoAr), questionarioMaquinista.getQualHouveAlgumaIndicacaoAnormalNoIndicadorDeFluxoDeAr());
        }


        Assert.assertTrue(isRadioChecked(radioButtonHouveMudancaBruscaVelocidade.apply(
                questionarioMaquinista.getHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoIndex()
                ))
        );

        if (questionarioMaquinista.getHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamentoIndex() == 0) {
            Assert.assertEquals(getValue(textareaExpliqueMudancaBrusca), questionarioMaquinista.getExpliqueHouveAlgumaMudancaBruscaDeVelocidadeAntesDoDescarrilamento());
        }


        Assert.assertTrue(isRadioChecked(radioButtonTremEstava.apply(
                questionarioMaquinista.getoTremEstavaIndex()
                ))
        );

        Assert.assertTrue(isRadioChecked(radioButtonNaHoraLocomotivaEstava.apply(
                questionarioMaquinista.getNaHoraDoAcidenteAsLocomotivasEstavamEmIndex()
                ))
        );

        Assert.assertEquals(getValue(inputQualAmperagem), questionarioMaquinista.getQualAAmperagem());


        Assert.assertTrue(isRadioChecked(radioButtonLocomotivasEstavamTracionadasAdequadamente.apply(
                questionarioMaquinista.getPeloSeuConhecimentoTodasAsLocomotivasEstavaTracionadasAdequadamenteIndex()
                ))
        );


        Assert.assertTrue(isRadioChecked(radioButtonHouveAlgumaLocomotivaDeslizando.apply(
                questionarioMaquinista.getHaviaAlgumaLocomotivaDeslizandoOuPerdendoForcaIndex()
                ))
        );

        if (questionarioMaquinista.getHaviaAlgumaLocomotivaDeslizandoOuPerdendoForcaIndex() == 0) {
            Assert.assertTrue(isRadioChecked(radioButtonEfeitoNaAcaoFolgaTrem.apply(
                    questionarioMaquinista.getIssoTeveAlgumEfeitoNaAcaoDasFolgasDoTremIndex()
                    ))
            );

        }

        Assert.assertTrue(isRadioChecked(radioButtonHouveVazamento.apply(
                questionarioMaquinista.getHouveVazamentoDoEncanamentoGeralIndex()
                ))
        );

        if (questionarioMaquinista.getHouveVazamentoDoEncanamentoGeralIndex() == 0) {
            Assert.assertEquals(getValue(inputQuantasLibrasMinuto), questionarioMaquinista.getQuantasLibrasPorMinuto());
        }


        Assert.assertTrue(isRadioChecked(radioButtonFreioIndependenteUtilizado.apply(
                questionarioMaquinista.getOFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoIndex()
                ))
        );


        if (questionarioMaquinista.getOFreioIndependenteFoiUtilizadoOuAliviadoAntesDoDescarrilamentoIndex() == 0) {
            Assert.assertEquals(getValue(inputFreioQualKm), questionarioMaquinista.getEmQualKM());
            Assert.assertEquals(getValue(inputQualAplicacaoFreio), questionarioMaquinista.geteQualFoiAAplicacao());
        }

        Assert.assertEquals(getValue(inputQuantasLocomotivasFreioDinamicoFuncionando), questionarioMaquinista.getPeloConhecimentoDoMaquinistaQuantasLocomotivasEstavamComOFreioDinamicoFuncionando());
        Assert.assertEquals(getValue(inputAmperagemAntesDescarrilamento), questionarioMaquinista.getQualAAmperagemAntesDoDescarrilamento());


        Assert.assertTrue(isRadioChecked(radioButtonFreioDinamicoOscilando.apply(
                questionarioMaquinista.getoFreioDinamicoEstavaOscilandoIndex()
                ))
        );

        if (questionarioMaquinista.getoFreioDinamicoEstavaOscilandoIndex() == 0) {
            Assert.assertEquals(getValue(inputOscilandoQuanto), questionarioMaquinista.getQuantoOFreioDinamicoEstavaOscilando());
        }

        Assert.assertTrue(isRadioChecked(radioButtonIndicacaoExcessoFrenagem.apply(
                questionarioMaquinista.getOuveIndicacaoDeExcessoDeFrenagemDinamicaAntesOuNoMomentoDoDescarrilamentoIndex()
                ))
        );

        if (questionarioMaquinista.isHouveDeslizamentoDeRodaAntesOuNoMomentoDoDescarrilamento()) {
            Assert.assertTrue(isRadioChecked(radioButtonHouveDeslizamentoSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonHouveDeslizamentoNao));
        }
    }
}
