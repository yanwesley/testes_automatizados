package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.*;
import br.com.api.model.sinistro.Veiculo;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class CustosSindicanciaPage extends GeralPage {
    /*
    Materiais
     */

    IntFunction<By> cellMateriaisData = index -> By.id("table-custo-material-cell-data-" + index);
    IntFunction<By> cellMateriaisMaterial = index -> By.id("table-custo-material-cell-material-" + index);
    IntFunction<By> cellMateriaisQuantidade = index -> By.id("table-custo-material-cell-quantidade-" + index);
    IntFunction<By> cellMateriaisValorUnitario = index -> By.id("table-custo-material-cell-valorUnitario-" + index);
    IntFunction<By> cellMateriaisValorTotal = index -> By.id("table-custo-material-cell-valorTotal-" + index);
    IntFunction<By> btnMaterialCellDownload = index -> By.id("button-download-custo-material-" + index);
    IntFunction<By> btnMaterialCellEdit = index -> By.id("button-edit-custo-material-" + index);
    IntFunction<By> btnMaterialCellRemove = index -> By.id("button-remove-custo-material-" + index);
    /*
    Maquinas
     */

    IntFunction<By> cellMaquinasData = index -> By.id("table-custo-maquina-cell-data-" + index);
    IntFunction<By> cellMaquinasEmpreiteira_Contrato_Maquina = index -> By.id("table-custo-maquina-cell-maquina-" + index);
    IntFunction<By> cellMaquinasQuantidade = index -> By.id("table-custo-maquina-cell-quantidade-" + index);
    IntFunction<By> cellMaquinasValorUnitario = index -> By.id("table-custo-maquina-cell-valorUnitario-" + index);
    IntFunction<By> cellMaquinasValorTotal = index -> By.id("table-custo-maquina-cell-valorTotal-" + index);
    IntFunction<By> btnMaquinasCellDownload = index -> By.id("button-download-custo-maquina-" + index);
    IntFunction<By> btnMaquinasCellEdit = index -> By.id("button-edit-custo-maquina-" + index);
    IntFunction<By> btnMaquinasCellRemove = index -> By.id("button-remove-custo-maquina-" + index);

    /*
    Transporte
     */

    IntFunction<By> cellTransporteData = index -> By.id("table-custo-transporte-cell-data-" + index);
    IntFunction<By> cellTransporteTipo = index -> By.id("table-custo-transporte-cell-subTipoCusto-" + index);
    IntFunction<By> cellTransporteColaboradorAreaCidade = index -> By.id("table-custo-transporte-cell-colaborador-" + index);
    IntFunction<By> cellTransporteQuantidade = index -> By.id("table-custo-transporte-cell-quantidade-" + index);
    IntFunction<By> cellTransporteValorUnitario = index -> By.id("table-custo-transporte-cell-valorUnitario-" + index);
    IntFunction<By> cellTransporteValorTotal = index -> By.id("table-custo-transporte-cell-valorTotal-" + index);
    IntFunction<By> btnTransporteCellDownload = index -> By.id("button-download-custo-transporte-" + index);
    IntFunction<By> btnTransporteCellEdit = index -> By.id("button-edit-custo-transporte-" + index);
    IntFunction<By> btnTransporteCellRemove = index -> By.id("button-remove-custo-transporte-" + index);
    /*
    Colaborador
     */

    IntFunction<By> cellColaboradorColaborador_cargo_turma = index -> By.id("table-custo-colaborador-cell-colaborador-" + index);
    IntFunction<By> cellColaboradorHorasTrabalhadas = index -> By.id("table-custo-colaborador-cell-quantidade-" + index);
    IntFunction<By> cellColaboradorValorTotal = index -> By.id("table-custo-colaborador-cell-valorTotal-" + index);
    IntFunction<By> btnColaboradorCellDownload = index -> By.id("button-download-custo-colaborador-" + index);
    IntFunction<By> btnColaboradorCellEdit = index -> By.id("button-edit-custo-colaborador-" + index);
    IntFunction<By> btnColaboradorCellRemove = index -> By.id("button-remove-custo-colaborador-" + index);

    /*
    Dados via outros
     */

    IntFunction<By> tdTableCustoOutrosCellDescricao = (index) -> By.id("table-custo-outros-cell-descricao-" + index);
    IntFunction<By> tdTableCustoOutrosCellQuantidade = (index) -> By.id("table-custo-outros-cell-quantidade-" + index);
    IntFunction<By> tdTableCustoOutrosCellValorUnitario = (index) -> By.id("table-custo-outros-cell-valorUnitario-" + index);
    IntFunction<By> tdTableCustoOutrosCellValorTotal = (index) -> By.id("table-custo-outros-cell-valorTotal-" + index);
    IntFunction<By> tdTableCustoOutrosCellActions = (index) -> By.id("table-custo-outros-cell-actions-" + index);
    IntFunction<By> buttonButtonDownloadCustoOutros = (index) -> By.id("button-download-custo-outros-" + index);
    IntFunction<By> buttonButtonEditCustoOutros = (index) -> By.id("button-edit-custo-outros-" + index);
    IntFunction<By> buttonButtonRemoveCustoOutros = (index) -> By.id("button-remove-custo-outros-" + index);
    IntFunction<By> cellCustoData = index -> By.id("table-custo-cell-data-" + index);
    IntFunction<By> cellCustoDescricao = index -> By.id("table-custo-cell-descricao-" + index);
    IntFunction<By> cellCustoQuantidade = index -> By.id("table-custo-cell-quantidade-" + index);
    IntFunction<By> cellCustoValorUnitario = index -> By.id("table-custo-cell-valorUnitario-" + index);
    IntFunction<By> cellCustoValorTotal = index -> By.id("table-custo-cell-valorTotal-" + index);
    IntFunction<By> btnCustoCellDownload = index -> By.id("button-download-custo-" + index);
    IntFunction<By> btnCustoCellEdit = index -> By.id("button-edit-custo-" + index);
    IntFunction<By> btnCustoCellRemove = index -> By.id("button-remove-custo-" + index);
    IntFunction<By> btnRemoverDia = index -> By.id("button-remove-dia-" + index);
    IntFunction<By> inputDataHoraInicio = index -> By.id("input-hora-inicio-" + index);
    IntFunction<By> inputDataHoraFim = index -> By.id("input-hora-fim-" + index);
    IntFunction<By> cellCheckBoxSemCustoVeiculoVia = index -> By.id("table-custos-veiculo-VEICULO_VIA-cell-custo-" + index + "-checkbox-input");
    IntFunction<By> cellBtnAddCustoVeiculoVia = index -> By.id("button-add-custo-veiculo-VEICULO_VIA-" + index);
    IntFunction<By> cellBtnEditarCustoVeiculoVia = index -> By.id("button-edit-custo-veiculo-VEICULO_VIA-" + index);
    IntFunction<By> cellValorCustoVeiculoVia = index -> By.id("table-custos-veiculo-VEICULO_VIA-cell-valor-" + index);
    IntFunction<By> cellCheckBoxSemCustoLocomotiva = index -> By.id("table-custos-veiculo-LOCOMOTIVA-cell-custo-" + index + "-checkbox-input");
    IntFunction<By> cellBtnAddCustoLocomotiva = index -> By.id("button-add-custo-veiculo-LOCOMOTIVA-" + index);
    IntFunction<By> cellBtnEditarCustoLocomotiva = index -> By.id("button-edit-custo-veiculo-LOCOMOTIVA-" + index);
    IntFunction<By> cellValorCustoLocomotiva = index -> By.id("table-custos-veiculo-LOCOMOTIVA-cell-valor-" + index);
    /*
    Custos Veículos de via
  */
    private By separatorCustosVeiculoDeVia = By.name("Custos Veículo de Via");
    private By tableCustosVeiculoDeVia = By.id("table-custos-veiculo-VEICULO_VIA");
    private By inputQtdRodeiro = By.id("input-rodeiro-quantidade");
    private By inputValorRodeiro = By.id("input-rodeiro-valor-unitario");
    private By inputQtdTruque = By.id("input-truque-quantidade");
    private By inputValorTruque = By.id("input-truque-valor-unitario");
    private By inputQtdEngate = By.id("input-engate-quantidade");
    private By inputValorEngate = By.id("input-engate-valor-unitario");
    private By inputQtdEstrutura = By.id("input-estrutura-quantidade");
    private By inputValorEstrutura = By.id("input-estrutura-valor-unitario");
    private By inputQtdSistemaFreio = By.id("input-sistemaFreio-quantidade");
    private By inputValorSistemaFreio = By.id("input-sistemaFreio-valor-unitario");
    private By inputQtdTanque = By.id("input-tanque-quantidade");
    private By inputValorTanque = By.id("input-tanque-valor-unitario");
    private By inputQtdCaixaGraxa = By.id("input-caixaGraxa-quantidade");
    private By inputValorCaixaGraxa = By.id("input-caixaGraxa-valor-unitario");
    private By inputQtdMaoDeObra = By.id("input-maoObra-quantidade");
    private By inputValorMaoDeObra = By.id("input-maoObra-valor-unitario");
    private By inputQtdOutros = By.id("input-outros-quantidade");
    private By inputValorOutros = By.id("input-outros-valor-unitario");
    private By btnSalvarModalAddCusto = By.id("dialog-button-yes");
    private By btnSalvarInformacoes = By.id("salvar-custos");
    /*
        Custos Locomotiva
    */
    private By separatorCustosLocomotiva = By.name("Custos Locomotiva");
    private By tableCustosLocomotiva = By.id("table-custos-veiculo-LOCOMOTIVA");
    private By inputQtdLimpaTrilho = By.id("input-limpaTrilho-quantidade");
    private By inputValorLimpaTrilho = By.id("input-limpaTrilho-valor-unitario");

    /*
    Outros
     */
    private By sroTitleLabelOutros = By.name("Outros");
    private By separatorCustosDaArea = By.name("Custos da área");
    private By inputHasCostsRadioSimInput = By.id("hasCostsRadio-sim-input");
    private By inputHasCostsRadioNaoInput = By.id("hasCostsRadio-nao-input");
    private By optionHouveCustosAreaSim = By.id("hasCostsRadio-sim");
    private By optionHouveCustosAreaSimLabel = By.cssSelector("#hasCostsRadio-sim > label");
    private By optionHouveCustosAreaNao = By.id("hasCostsRadio-nao");
    private By optionHouveCustosAreaNaoLabel = By.cssSelector("#hasCostsRadio-nao > label");
    private By btnIncluirCusto = By.id("button-add-cost");
    private By labelCustoTotal = By.id("total-cost");

    /*
    Modal adicionar materias
     */
    private By labelBtnInformacoesSalvas = By.id("button-informacoes-salvas");
    private By titleMateriais = By.name("Materiais");
    private By titleMaquinas = By.name("Máquinas");
    private By radioBtnMateriais = By.id("radio-button-tipo-custo-MATERIAIS");
    private By radioBtnMateriaisLabel = By.cssSelector("#radio-button-tipo-custo-MATERIAIS > label");
    private By radioBtnMaquinas = By.id("radio-button-tipo-custo-MAQUINAS");
    private By radioBtnMaquinasLabel = By.cssSelector("#radio-button-tipo-custo-MAQUINAS > label");
    private By radioBtnTransportes = By.id("radio-button-tipo-custo-TRANSPORTE");
    private By radioBtnTransportesLabel = By.cssSelector("#radio-button-tipo-custo-TRANSPORTE > label");
    private By radioBtnColaborador = By.id("radio-button-tipo-custo-COLABORADOR");
    private By radioBtnColaboradorLabel = By.cssSelector("#radio-button-tipo-custo-COLABORADOR > label");
    private By radioBtnOutros = By.id("radio-button-tipo-custo-OUTROS");
    private By radioBtnOutrosLabel = By.cssSelector("#radio-button-tipo-custo-OUTROS > label");
    private By btnCancelarModal = By.id("dialog-button-cancel");
    private By btnSalvarModal = By.id("dialog-button-save");
    private By btnSalvarEAddOutroModal = By.id("dialog-button-save-add-other");
    /*
    Modal exclusão
     */
    private By btnModalExclusaoNao = By.id("dialog-button-no");
    private By btnModalExclusaoSim = By.id("dialog-button-yes");
    private By titleModalExclusao = By.id("dialog-header");
    /*
        Material
     */
    private By inputMaterial = By.id("input-material");
    private By inputUnidade = By.id("input-unidade");
    private By inputQuantidade = By.id("input-quantidade");
    private By inputValorUnitario = By.id("input-valor-unitario");
    private By radioBtnUnidadeHoras = By.id("radio-button-tipo-unidade-HORAS");
    private By radioBtnUnidadeHorasLabel = By.cssSelector("#radio-button-tipo-unidade-HORAS > label");
    private By radioBtnUnidadeUnidades = By.id("radio-button-tipo-unidade-UNIDADE");
    private By radioBtnUnidadeUnidadesLabel = By.cssSelector("#radio-button-tipo-unidade-UNIDADE > label");
    /*
        Máquinas
     */
    private By inputEmpreiteira = By.id("input-empreiteira");
    private By inputContrato = By.id("input-contrato");
    private By inputMaquina = By.id("input-maquina");
    private By inputData = By.id("input-data");
    /*
    Transporte
 */
    private By selectSubTipo = By.id("select-sub-tipo");
    private By optionSubTipoHotel = By.id("option-sub-tipo-HOTEL");
    private By optionSubTipoPedagio = By.id("option-sub-tipo-PEDAGIO");
    private By optionSubTipoAlimentacao = By.id("option-sub-tipo-ALIMENTACAO");

    /*
    Colaborador
 */
    private By inputColaborador = By.id("input-colaborador");
    private By inputCargo = By.id("input-cargo");
    private By btnSearchColaborador = By.id("button-search");
    private By inputArea = By.id("input-areaColaborador");
    private By inputCidade = By.id("input-cidade");
    private By inputTurma = By.id("input-turma");
    private By inputValorTotal = By.id("input-valor-total");
    private By btnAddDiaTrabalho = By.id("button-add-day");

    /*
    Custo outros
     */

    private By inputDescricao = By.id("input-descricao");
    private By tableTableCusto = By.id("table-custo");
    private By thTableCustoHeaderArea = By.id("table-custo-header-area");
    private By thTableCustoHeaderTotal = By.id("table-custo-header-total");
    private By tdTableCustoCellAreaMAT_RODANTE = By.id("table-custo-cell-area-MAT_RODANTE");
    private By tdTableCustoCellTotalMAT_RODANTE = By.id("table-custo-cell-total-MAT_RODANTE");
    private By tdTableCustoCellAreaOPERACAO = By.id("table-custo-cell-area-OPERACAO");
    private By tdTableCustoCellTotalOPERACAO = By.id("table-custo-cell-total-OPERACAO");
    private By tdTableCustoCellAreaVIA = By.id("table-custo-cell-area-VIA");
    private By tdTableCustoCellTotalVIA = By.id("table-custo-cell-total-VIA");
    private By tdTableCustoCellAreaCONTROLE_PERDAS = By.id("table-custo-cell-area-CONTROLE_PERDAS");
    private By tdTableCustoCellTotalCONTROLE_PERDAS = By.id("table-custo-cell-total-CONTROLE_PERDAS");
    private By tdTableCustoCellAreaMEIO_AMBIENTE = By.id("table-custo-cell-area-MEIO_AMBIENTE");
    private By tdTableCustoCellTotalMEIO_AMBIENTE = By.id("table-custo-cell-total-MEIO_AMBIENTE");
    private By tdTableCustoCellAreaPATRIMONIAL = By.id("table-custo-cell-area-PATRIMONIAL");
    private By tdTableCustoCellTotalPATRIMONIAL = By.id("table-custo-cell-total-PATRIMONIAL");
    private By tdTableCustoCellAreaSST = By.id("table-custo-cell-area-SST");
    private By tdTableCustoCellTotalSST = By.id("table-custo-cell-total-SST");
    private By tdTableCustoCellAreaTO = By.id("table-custo-cell-area-TO");
    private By tdTableCustoCellTotalTO = By.id("table-custo-cell-total-TO");

    /*
    Custo de atendimento
     */
    private By radioBtnCustoAtendimentoSim = By.id("custo-atendimento-VEICULO_VIA-radio-sim");
    private By btnIncluirCustoAtendimento = By.id("button-add-cost-atendimento");
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By divDialogCustoAtendimento = By.id("dialog-custo-atendimento");
    private By inputInputDescricao = By.id("input-descricao");
    private By inputInputQuantidade = By.id("input-quantidade");
    private By inputInputValorUnitario = By.id("input-valor-unitario");
    private By inputInputValorTotal = By.id("input-valor-total");
    private By buttonBotaoCancelar = By.id("botao-cancelar");
    private By buttonBotaoSalvar = By.id("botao-salvar");
    private By buttonBotaoSalvarAddOutro = By.id("botao-salvar-add-outro");
    private By iconRemoverCustoAtendimento = By.id("button-remove-custo-atendimento-VEICULO_VIA-0");
    private By radioGroupCustoAtendimentoVEICULO_VIARadio = By.id("custo-atendimento-VEICULO_VIA-radio");
    private By radioButtonCustoAtendimentoVEICULO_VIARadioSim = By.id("custo-atendimento-VEICULO_VIA-radio-sim");
    private By inputCustoAtendimentoVEICULO_VIARadioSimInput = By.id("custo-atendimento-VEICULO_VIA-radio-sim-input");
    private By radioButtonCustoAtendimentoVEICULO_VIARadioNao = By.id("custo-atendimento-VEICULO_VIA-radio-nao");
    private By inputCustoAtendimentoVEICULO_VIARadioNaoInput = By.id("custo-atendimento-VEICULO_VIA-radio-nao-input");
    private By tableTableCustosAtendimentoVEICULO_VIA = By.id("table-custos-atendimento-VEICULO_VIA");
    private By thTableCustosAtendimentoVEICULO_VIAHeaderDescricao = By.id("table-custos-atendimento-VEICULO_VIA-header-descricao");
    private By thTableCustosAtendimentoVEICULO_VIAHeaderQuantidade = By.id("table-custos-atendimento-VEICULO_VIA-header-quantidade");
    private By thTableCustosAtendimentoVEICULO_VIAHeaderValor_unitario = By.id("table-custos-atendimento-VEICULO_VIA-header-valor_unitario");
    private By thTableCustosAtendimentoVEICULO_VIAHeaderValor_total = By.id("table-custos-atendimento-VEICULO_VIA-header-valor_total");
    private By thTableCustosAtendimentoVEICULO_VIAHeaderRemove = By.id("table-custos-atendimento-VEICULO_VIA-header-remove");
    private By tdTableCustosAtendimentoVEICULO_VIACellDescricao0 = By.id("table-custos-atendimento-VEICULO_VIA-cell-descricao-0");
    private By tdTableCustosAtendimentoVEICULO_VIACellDescricao1 = By.id("table-custos-atendimento-VEICULO_VIA-cell-descricao-1");
    private By tdTableCustosAtendimentoVEICULO_VIACellQuantidade0 = By.id("table-custos-atendimento-VEICULO_VIA-cell-quantidade-0");
    private By tdTableCustosAtendimentoVEICULO_VIACellQuantidade1 = By.id("table-custos-atendimento-VEICULO_VIA-cell-quantidade-1");
    private By tdTableCustosAtendimentoVEICULO_VIACellValor_unitario0 = By.id("table-custos-atendimento-VEICULO_VIA-cell-valor_unitario-0");
    private By tdTableCustosAtendimentoVEICULO_VIACellValor_unitario1 = By.id("table-custos-atendimento-VEICULO_VIA-cell-valor_unitario-1");
    private By tdTableCustosAtendimentoVEICULO_VIACellValor_total0 = By.id("table-custos-atendimento-VEICULO_VIA-cell-valor_total-0");
    private By tdTableCustosAtendimentoVEICULO_VIACellRemove0 = By.id("table-custos-atendimento-VEICULO_VIA-cell-remove-0");
    private By buttonButtonEditCustoAtendimentoVEICULO_VIA0 = By.id("button-edit-custo-atendimento-VEICULO_VIA-0");
    private By buttonButtonRemoveCustoAtendimentoVEICULO_VIA0 = By.id("button-remove-custo-atendimento-VEICULO_VIA-0");
    private By divCustosVeiculoVEICULO_VIALabelTotal = By.id("custos-veiculo-VEICULO_VIA-label-total");
    private By divCustosVeiculoVEICULO_VIASomaTotal = By.id("custos-veiculo-VEICULO_VIA-soma-total");
    private By buttonButtonAddCostAtendimento = By.id("button-add-cost-atendimento");
    private By btnEditarCustoAtendimento = By.id("button-edit-custo-atendimento-VEICULO_VIA-0");
    private By checkBoxSemCusto = By.id("table-custos-veiculo-VEICULO_VIA-cell-custo-0-checkbox");
    private By btnSalvarCustos = By.id("salvar-custos");
    private By btnAdicionarCustosVagao = By.id("button-add-custo-veiculo-VAGAO-0");

    /*
    Custos vagão
     */
    private By divDialogCustoVagao = By.id("dialog-custo-vagao");
    private By inputInputRodeiroQuantidade = By.id("input-rodeiro-quantidade");
    private By inputInputRodeiroValorUnitario = By.id("input-rodeiro-valor-unitario");
    private By inputInputTruqueQuantidade = By.id("input-truque-quantidade");
    private By inputInputTruqueValorUnitario = By.id("input-truque-valor-unitario");
    private By inputInputEngateQuantidade = By.id("input-engate-quantidade");
    private By inputInputEngateValorUnitario = By.id("input-engate-valor-unitario");
    private By inputInputEstruturaQuantidade = By.id("input-estrutura-quantidade");
    private By inputInputEstruturaValorUnitario = By.id("input-estrutura-valor-unitario");
    private By inputInputTremonhaQuantidade = By.id("input-tremonha-quantidade");
    private By inputInputTremonhaValorUnitario = By.id("input-tremonha-valor-unitario");
    private By inputInputSistemaFreioQuantidade = By.id("input-sistemaFreio-quantidade");
    private By inputInputSistemaFreioValorUnitario = By.id("input-sistemaFreio-valor-unitario");
    private By inputInputTransporteQuantidade = By.id("input-transporte-quantidade");
    private By inputInputTransporteValorUnitario = By.id("input-transporte-valor-unitario");
    private By inputInputMaoObraQuantidade = By.id("input-maoObra-quantidade");
    private By inputInputMaoObraValorUnitario = By.id("input-maoObra-valor-unitario");
    private By inputInputOutrosQuantidade = By.id("input-outros-quantidade");
    private By inputInputOutrosValorUnitario = By.id("input-outros-valor-unitario");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");
    private By iconEditarCustoVagao = By.id("button-edit-custo-veiculo-VAGAO-0");
    private By numeroVagao = By.id("table-custos-veiculo-VAGAO-cell-numero-0");
    private By valorTotalCustos = By.id("table-custos-veiculo-VAGAO-cell-valor-0");

    /*
    modal remover custo
     */

    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNoRemover = By.id("dialog-button-no");
    private By buttonDialogButtonYesRemover = By.id("dialog-button-yes");

    public void validaExibicaoDaOpcaoDeHouveCustos() {
        expectElementVisible(separatorCustosDaArea);
    }

    public void clicarOptionHouveCustosSim() {
        expectLoading();
        clickWithAction(optionHouveCustosAreaSim);
        clickWithAction(optionHouveCustosAreaSim);
    }

    public void validaExibicaoDoBtnIncluirCusto() {
        expectElementVisible(btnIncluirCusto);
    }

    public void adicionarNovoCustoMaterial(CustoMaterial custoMaterial) {
        clickWithAction(radioBtnMateriaisLabel);
        sendKeysWithJavaScript(inputMaterial, custoMaterial.getMaterial());
//        Alterado para campo aberto.
//        if (custoMaterial.getUnidade().equals("Horas")) {
//            clickWithAction(radioBtnUnidadeHorasLabel);
//        } else {
//            clickWithAction(radioBtnUnidadeUnidadesLabel);
//        }

        sendKeys(inputUnidade, String.valueOf(custoMaterial.getUnidade()));
        sendKeys(inputQuantidade, String.valueOf(custoMaterial.getQuantidade()));
        sendKeys(inputValorUnitario, String.valueOf(custoMaterial.getValorUnitario()));
        uploadArquivo(1);
    }

    public void clicarBtnSalvarModal() {
        clickAndHighlight(btnSalvarModal);
    }

    public void validaCustoMaterial(CustoMaterial custoMaterial, int linha) {
        linha--;
        scrollToElement(titleMateriais);
        Assert.assertEquals(getText(cellMateriaisMaterial.apply(linha)), custoMaterial.getMaterial());
        Assert.assertEquals(getText(cellMateriaisQuantidade.apply(linha)), String.valueOf(custoMaterial.getQuantidade()));
        Assert.assertEquals(getText(cellMateriaisValorUnitario.apply(linha)), formatToCurrency(custoMaterial.getValorUnitario()));
        Assert.assertEquals(getText(cellMateriaisValorTotal.apply(linha)), formatToCurrency(custoMaterial.getValorUnitario() * custoMaterial.getQuantidade()));
    }

    public void editarCustoMaterial(CustoMaterial custoMaterial) {
        sendKeysWithJavaScript(inputMaterial, custoMaterial.getMaterial());
//        Alterado para campo aberto.
//        if (custoMaterial.getUnidade().equals("Horas")) {
//            clickWithAction(radioBtnUnidadeHorasLabel);
//        } else {
//            clickWithAction(radioBtnUnidadeUnidadesLabel);
//        }
        sendKeys(inputUnidade, String.valueOf(custoMaterial.getUnidade()));
        sendKeys(inputQuantidade, String.valueOf(custoMaterial.getQuantidade()));
        sendKeys(inputValorUnitario, String.valueOf(custoMaterial.getValorUnitario()));
    }

    public void clicarBtnEditarMaterial(int linha) {
        linha--;
        scrollToElement(titleMateriais);
        clickAndHighlight(btnMaterialCellEdit.apply(linha));
    }

    public void clicarBtnExcluirCustoMaterial(int linha) {
        linha--;
        scrollToElement(titleMateriais);
        clickAndHighlight(btnMaterialCellRemove.apply(linha));
    }

    public void validaModalDeExclusaoDoCusto() {
        expectElementVisible(titleModalExclusao);
        expectElementClickable(btnModalExclusaoNao);
        expectElementClickable(btnModalExclusaoSim);
    }

    public void clicarBtnSimModalExclusao() {
        clickAndHighlight(btnModalExclusaoSim);
    }

    public void validaQueOCustoMaterialFoiExcluido() {
        expectElementNotVisible(cellMateriaisData.apply(0));
        expectElementNotVisible(cellMateriaisMaterial.apply(0));
        expectElementNotVisible(cellMateriaisQuantidade.apply(0));
        expectElementNotVisible(labelCustoTotal);
    }

    public void clicarBtnIncluirCusto() {
        clickAndHighlight(btnIncluirCusto);
    }

    public void adicionarNovoCustoMaquinas(CustoMaquina custoMaquina) {
        clickWithAction(radioBtnMaquinasLabel);
        sendKeysWithJavaScript(inputEmpreiteira, custoMaquina.getEmpreiteira());
        sendKeysWithJavaScript(inputContrato, custoMaquina.getContrato());
        sendKeysWithJavaScript(inputMaquina, custoMaquina.getMaquina());
        clearForce(inputData);
        sendKeys(inputData, custoMaquina.getData());
        if (custoMaquina.getUnidade().equals("Horas")) {
            clickWithAction(radioBtnUnidadeHorasLabel);
        } else {
            clickWithAction(radioBtnUnidadeUnidadesLabel);
        }
        sendKeys(inputQuantidade, String.valueOf(custoMaquina.getQuantidade()));
        sendKeys(inputValorUnitario, String.valueOf(custoMaquina.getValorUnitario()));
        uploadArquivo(1);
    }

    public void validaCustoMaquina(CustoMaquina custoMaquina, int linha) {
        linha--;
        scrollToElement(titleMaquinas);
        Assert.assertEquals(getText(cellMaquinasData.apply(linha)), custoMaquina.getData());
        String empreiteira_contrato_maquina = custoMaquina.getEmpreiteira() + " / " + custoMaquina.getContrato() + " / " + custoMaquina.getMaquina();
        Assert.assertEquals(getText(cellMaquinasEmpreiteira_Contrato_Maquina.apply(linha)), empreiteira_contrato_maquina);
        Assert.assertEquals(getText(cellMaquinasQuantidade.apply(linha)), String.valueOf(custoMaquina.getQuantidade()));
        Assert.assertEquals(getText(cellMaquinasValorUnitario.apply(linha)), formatToCurrency(custoMaquina.getValorUnitario()));
        Assert.assertEquals(getText(cellMaquinasValorTotal.apply(linha)), formatToCurrency(custoMaquina.getValorUnitario() * custoMaquina.getQuantidade()));
    }

    public void clicarBtnEditarMaquina(int linha) {
        linha--;
        scrollToElement(titleMaquinas);
        clickAndHighlight(btnMaquinasCellEdit.apply(linha));
    }

    public void editarCustoMaquina(CustoMaquina custoMaquinaEditado) {
        sendKeysWithJavaScript(inputEmpreiteira, custoMaquinaEditado.getEmpreiteira());
        sendKeysWithJavaScript(inputContrato, custoMaquinaEditado.getContrato());
        sendKeysWithJavaScript(inputMaquina, custoMaquinaEditado.getMaquina());
        clearForce(inputData);
        sendKeys(inputData, custoMaquinaEditado.getData());
        if (custoMaquinaEditado.getUnidade().equals("Horas")) {
            clickWithAction(radioBtnUnidadeHorasLabel);
        } else {
            clickWithAction(radioBtnUnidadeUnidadesLabel);
        }
        sendKeys(inputQuantidade, String.valueOf(custoMaquinaEditado.getQuantidade()));
        sendKeys(inputValorUnitario, String.valueOf(custoMaquinaEditado.getValorUnitario()));
    }

    public void clicarBtnExcluirCustoMaquina(int linha) {
        linha--;
        scrollToElement(titleMaquinas);
        clickAndHighlight(btnMaquinasCellRemove.apply(linha));
    }

    public void validaQueOCustoMaquinaFoiExcluido() {
        expectElementNotVisible(cellMaquinasQuantidade.apply(0));
        expectElementNotVisible(cellMaquinasData.apply(0));
        expectElementNotVisible(cellMaquinasEmpreiteira_Contrato_Maquina.apply(0));
    }

    public void adicionarNovoCustoOutros(CustoOutros custoOutros) {
        sendKeys(inputQuantidade, custoOutros.getQuantidade());
        sendKeys(inputValorUnitario, custoOutros.getValorUnitario());
        sendKeysWithJavaScript(inputDescricao, custoOutros.getDescricao());
        uploadArquivo(1);
    }

    public void validaCustoOutros(CustoOutros custoOutros, int linha) {
        linha--;
        Assert.assertEquals(getText(cellCustoDescricao.apply(linha)), custoOutros.getDescricao());
        Assert.assertEquals(getText(cellCustoQuantidade.apply(linha)), String.valueOf(custoOutros.getQuantidade()));
        Assert.assertEquals(getText(cellCustoValorUnitario.apply(linha)), formatToCurrency(custoOutros.getValorUnitario()));
        Assert.assertEquals(getText(cellCustoValorTotal.apply(linha)), formatToCurrency(custoOutros.getValorUnitario() * custoOutros.getQuantidade()));
    }

    public void clicarBtnEditarCustoOutros(int linha) {
        linha--;
        clickAndHighlight(btnCustoCellEdit.apply(linha));
    }

    public void editarCustoOutros(CustoOutros custoOutros) {
        sendKeys(inputQuantidade, custoOutros.getQuantidade());
        sendKeys(inputValorUnitario, custoOutros.getValorUnitario());
        sendKeysWithJavaScript(inputDescricao, custoOutros.getDescricao());
    }

    public void clicarBtnExcluirCustoOutros(int linha) {
        linha--;
        clickAndHighlight(btnCustoCellRemove.apply(linha));
    }

    public void validaQueOCustoOutrosFoiExcluido() {
        expectElementNotVisible(cellCustoQuantidade.apply(0));
        expectElementNotVisible(cellCustoDescricao.apply(0));
    }

    public void adicionarNovoCustoTransporte(CustoTransporte custoTransporte) {
        clickWithAction(radioBtnTransportesLabel);
        clickAndHighlight(selectSubTipo);
        switch (custoTransporte.getSubTipo()) {
            case "Hotel":
                clickAndHighlight(optionSubTipoHotel);
                break;
            case "Pedágio":
                clickAndHighlight(optionSubTipoPedagio);
                break;
            case "Alimentação":
                clickAndHighlight(optionSubTipoAlimentacao);
                break;
        }

        clickAndHighlight(btnSearchColaborador);
        selecionarResponsavel(custoTransporte.getColaborador());
        custoTransporte.setColaborador(getValue(inputColaborador));
        sendKeysWithJavaScript(inputArea, custoTransporte.getArea());
        sendKeysWithJavaScript(inputCidade, custoTransporte.getCidade());
        sendKeys(inputQuantidade, custoTransporte.getQuantidade());
        sendKeys(inputValorUnitario, custoTransporte.getValorUnitario());
        uploadArquivo(1);
    }

    public void validaCustoTransporte(CustoTransporte custoTransporte, int linha) {
        linha--;
        Assert.assertEquals(getText(cellTransporteTipo.apply(linha)), custoTransporte.getSubTipo());
        String colaborador = custoTransporte.getColaborador() + " / " + custoTransporte.getArea() + " / " + custoTransporte.getCidade();
        Assert.assertEquals(getText(cellTransporteColaboradorAreaCidade.apply(linha)), colaborador);
        Assert.assertEquals(getText(cellTransporteQuantidade.apply(linha)), String.valueOf(custoTransporte.getQuantidade()));
        Assert.assertEquals(getText(cellTransporteValorUnitario.apply(linha)), formatToCurrency(custoTransporte.getValorUnitario()));
        Assert.assertEquals(getText(cellTransporteValorTotal.apply(linha)), formatToCurrency(custoTransporte.getValorUnitario() * custoTransporte.getQuantidade()));
    }

    public void clicarBtnExcluirTransporte(int linha) {
        linha--;
        clickAndHighlight(btnTransporteCellRemove.apply(linha));
    }

    public void validaQueOCustoTransporteFoiExcluido() {
        expectElementNotVisible(cellTransporteData.apply(0));
        expectElementNotVisible(cellTransporteTipo.apply(0));
        expectElementNotVisible(cellTransporteColaboradorAreaCidade.apply(0));
    }

    public void clicarBtnEditarCustoTransporte(int linha) {
        linha--;
        clickAndHighlight(btnTransporteCellEdit.apply(linha));
    }

    public void editarCustoTransporte(CustoTransporte custoTransporteEditado) {
        clickAndHighlight(selectSubTipo);
        switch (custoTransporteEditado.getSubTipo()) {
            case "Hotel":
                clickAndHighlight(optionSubTipoHotel);
                break;
            case "Pedágio":
                clickAndHighlight(optionSubTipoPedagio);
                break;
            case "Alimentação":
                clickAndHighlight(optionSubTipoAlimentacao);
                break;
        }
        clickAndHighlight(btnSearchColaborador);
        selecionarResponsavel(custoTransporteEditado.getColaborador());
        custoTransporteEditado.setColaborador(getValue(inputColaborador));
        sendKeysWithJavaScript(inputArea, custoTransporteEditado.getArea());
        sendKeysWithJavaScript(inputCidade, custoTransporteEditado.getCidade());
        sendKeys(inputQuantidade, custoTransporteEditado.getQuantidade());
        sendKeys(inputValorUnitario, custoTransporteEditado.getValorUnitario());
    }

    public void adicionarNovoCustoColaborador(CustoColaborador custoColaborador) {
        clickWithAction(radioBtnColaboradorLabel);
        clickAndHighlight(btnSearchColaborador);
        selecionarResponsavel(custoColaborador.getColaborador());
        custoColaborador.setColaborador(getValue(inputColaborador));
        custoColaborador.setCargo(getValue(inputCargo));
        sendKeysWithJavaScript(inputTurma, custoColaborador.getTurma());
        sendKeys(inputDataHoraInicio.apply(0), custoColaborador.getHoraInicio());
        sendKeys(inputDataHoraFim.apply(0), custoColaborador.getHoraFim());
        sendKeys(inputValorTotal, custoColaborador.getValorTotal());
        uploadArquivo(1);
    }

    public void validaCustoColaborador(CustoColaborador custoColaborador, int linha) {
        linha--;
        String colaborador = custoColaborador.getColaborador() + " / " + custoColaborador.getCargo() + " / " + custoColaborador.getTurma();
        Assert.assertEquals(getText(cellColaboradorColaborador_cargo_turma.apply(linha)), colaborador);
//        Assert.assertEquals(getText(cellColaboradorValorUnitario.apply(linha)), formatToCurrency(custoColaborador.hora()));
        Assert.assertEquals(getText(cellColaboradorValorTotal.apply(linha)), formatToCurrency(custoColaborador.getValorTotal()));

    }

    public void clicarBtnEditarCustoColaborador(int linha) {
        linha--;
        clickAndHighlight(btnColaboradorCellEdit.apply(linha));
    }

    public void editarCustoColaborador(CustoColaborador custoColaboradorEditado) {
        clickWithAction(radioBtnColaboradorLabel);
        clickAndHighlight(btnSearchColaborador);
        selecionarResponsavel(custoColaboradorEditado.getColaborador());
        custoColaboradorEditado.setColaborador(getValue(inputColaborador));
        custoColaboradorEditado.setCargo(getValue(inputCargo));
        sendKeysWithJavaScript(inputTurma, custoColaboradorEditado.getTurma());
        sendKeys(inputDataHoraInicio.apply(0), custoColaboradorEditado.getHoraInicio());
        sendKeys(inputDataHoraFim.apply(0), custoColaboradorEditado.getHoraFim());
        sendKeys(inputValorTotal, custoColaboradorEditado.getValorTotal());
    }

    public void clicarBtnExcluirColaborador(int linha) {
        linha--;
        clickAndHighlight(btnColaboradorCellRemove.apply(linha));
    }

    public void validaQueOCustoColaboradorFoiExcluido() {
        expectElementNotVisible(cellColaboradorColaborador_cargo_turma.apply(0));
        expectElementNotVisible(cellColaboradorValorTotal.apply(0));
    }

    public void validoValorTotalCustos(CustoOutros custoOutros, CustoMaterial custoMaterial, CustoLocomotiva custoLocomotiva) {
        expectElementVisibleWithoutHighlight(labelCustoTotal);
        int quantidadeDeCustoOutros = 6;
        long valorTotal = ((custoOutros.getValorUnitario() * custoOutros.getQuantidade() * quantidadeDeCustoOutros)
                + (custoMaterial.getValorUnitario() * custoMaterial.getQuantidade()))
                + custoLocomotiva.getTotal();

        expectText(labelCustoTotal, formatToCurrency(valorTotal));
        Assert.assertEquals(getText(labelCustoTotal), formatToCurrency(valorTotal));
    }

    public void validoValorCustosDeCadaArea(CustoOutros custoOutros, CustoMaterial custoMaterial, CustoLocomotiva custoLocomotiva) {
        long custoOutrosTotal = custoOutros.getValorUnitario() * custoOutros.getQuantidade();
        long custoMaterialTotal = custoMaterial.getValorUnitario() * custoMaterial.getQuantidade();

        Assert.assertEquals(getText(tdTableCustoCellTotalMEIO_AMBIENTE), formatToCurrency(custoOutrosTotal));
        Assert.assertEquals(getText(tdTableCustoCellTotalCONTROLE_PERDAS), formatToCurrency(custoOutrosTotal));
        Assert.assertEquals(getText(tdTableCustoCellTotalSST), formatToCurrency(custoOutrosTotal));
        Assert.assertEquals(getText(tdTableCustoCellTotalTO), formatToCurrency(custoOutrosTotal));
        Assert.assertEquals(getText(tdTableCustoCellTotalMAT_RODANTE), formatToCurrency(custoLocomotiva.getTotal()));
        Assert.assertEquals(getText(tdTableCustoCellTotalOPERACAO), formatToCurrency(custoOutrosTotal));
        Assert.assertEquals(getText(tdTableCustoCellTotalVIA), formatToCurrency(custoMaterialTotal));
        Assert.assertEquals(getText(tdTableCustoCellTotalPATRIMONIAL), formatToCurrency(custoOutrosTotal));
    }

    public void clicarOpcaoOutros() {
        clickWithAction(radioBtnOutrosLabel);
    }

    public void clicarBtnEditarOutros(int linha) {
        linha--;
        clickAndHighlight(buttonButtonEditCustoOutros.apply(linha));
    }

    public void validaCustoOutrosEditadoDadosVia(CustoOutros custoOutros, int linha) {
        linha--;
        scrollToElement(sroTitleLabelOutros);
        Assert.assertEquals(getText(tdTableCustoOutrosCellDescricao.apply(linha)), String.valueOf(custoOutros.getDescricao()));
        Assert.assertEquals(getText(tdTableCustoOutrosCellQuantidade.apply(linha)), String.valueOf(custoOutros.getQuantidade()));
        Assert.assertEquals(getText(tdTableCustoOutrosCellValorUnitario.apply(linha)), formatToCurrency(custoOutros.getValorUnitario()));
        Assert.assertEquals(getText(tdTableCustoOutrosCellValorTotal.apply(linha)), formatToCurrency(custoOutros.getValorUnitario() * custoOutros.getQuantidade()));
    }

    public void clicarBtnExcluirCustoOutrosDadosVia(int linha) {
        linha--;
        clickAndHighlight(buttonButtonRemoveCustoOutros.apply(linha));
    }

    public void validaQueOCustoOutrosDadosViaFoiExcluido() {
        expectElementNotVisible(tdTableCustoOutrosCellDescricao.apply(0));
        expectElementNotVisible(tdTableCustoOutrosCellQuantidade.apply(0));
        expectElementNotVisible(tdTableCustoOutrosCellValorUnitario.apply(0));
    }

    public void selecionarCustosAtendimento() {
        clickWithAction(radioBtnCustoAtendimentoSim);
    }

    public void clicarBtnIncluirCustoAtendimento() {
        clickAndHighlight(btnIncluirCustoAtendimento);
    }

    public void adicionarCustoAtendimento(CustoVeiculosVia custoVeiculosVia) {
        sendKeysWithJavaScript(inputInputDescricao, custoVeiculosVia.getDescricao());
        sendKeys(inputInputQuantidade, custoVeiculosVia.getQuantidade());
        sendKeys(inputInputValorUnitario, custoVeiculosVia.getValoUnitario());
        clickAndHighlight(buttonBotaoSalvar);
    }

    public void removerCustoAtendimento() {
        clickAndHighlight(iconRemoverCustoAtendimento);
    }

    public void validoQueValoresPersistiram(CustoVeiculosVia custoVeiculosVia) {
        scrollToElement(tdTableCustosAtendimentoVEICULO_VIACellDescricao0);
        Assert.assertEquals(getText(tdTableCustosAtendimentoVEICULO_VIACellDescricao0), custoVeiculosVia.getDescricao());
        Assert.assertEquals(getText(tdTableCustosAtendimentoVEICULO_VIACellQuantidade0), String.valueOf(custoVeiculosVia.getQuantidade()));
        Assert.assertEquals(getText(tdTableCustosAtendimentoVEICULO_VIACellValor_unitario0), formatToCurrency(custoVeiculosVia.getValoUnitario()));
    }

    public void editarCustoAtendimento(CustoVeiculosVia custoVeiculosViaEditado) {
        clickAndHighlight(btnEditarCustoAtendimento);
        sendKeysWithJavaScript(inputInputDescricao, custoVeiculosViaEditado.getDescricao());
        sendKeys(inputInputQuantidade, custoVeiculosViaEditado.getQuantidade());
        sendKeys(inputInputValorUnitario, custoVeiculosViaEditado.getValoUnitario());
        clickAndHighlight(buttonBotaoSalvar);
    }

    public void validoQueValoresEditadosPersistiram(CustoVeiculosVia custoVeiculosViaEditado) {
        scrollToElement(tdTableCustosAtendimentoVEICULO_VIACellDescricao0);
        Assert.assertEquals(getText(tdTableCustosAtendimentoVEICULO_VIACellDescricao0), custoVeiculosViaEditado.getDescricao());
        Assert.assertEquals(getText(tdTableCustosAtendimentoVEICULO_VIACellQuantidade0), String.valueOf(custoVeiculosViaEditado.getQuantidade()));
        Assert.assertEquals(getText(tdTableCustosAtendimentoVEICULO_VIACellValor_unitario0), formatToCurrency(custoVeiculosViaEditado.getValoUnitario()));
    }

    public void validarCustoAtendimentoRemovido() {
        expectElementNotVisible(iconRemoverCustoAtendimento);
    }

    public void salvarCustosAtendimento() {
        clickAndHighlight(checkBoxSemCusto);
        clickAndHighlight(btnSalvarCustos);
    }

    public void clicarBtnAdicionarCustosVagao() {
        clickAndHighlight(btnAdicionarCustosVagao);
    }

    public void adicionarCustosVagao(CustoVagoes custoVagoes) {
        sendKeys(inputInputRodeiroQuantidade, custoVagoes.getQtdRodeiros());
        sendKeys(inputInputRodeiroValorUnitario, custoVagoes.getValorRodeiros());

        sendKeys(inputInputTruqueQuantidade, custoVagoes.getQtdTruque());
        sendKeys(inputInputTruqueValorUnitario, custoVagoes.getValorTruque());

        sendKeys(inputInputEngateQuantidade, custoVagoes.getQtdEngate());
        sendKeys(inputInputEngateValorUnitario, custoVagoes.getValorEngate());

        sendKeys(inputInputEstruturaQuantidade, custoVagoes.getQtdEstrutura());
        sendKeys(inputInputEstruturaValorUnitario, custoVagoes.getValorEstrutura());

        sendKeys(inputInputTremonhaQuantidade, custoVagoes.getQtdTremonha());
        sendKeys(inputInputTremonhaValorUnitario, custoVagoes.getValorTremonha());

        sendKeys(inputInputSistemaFreioQuantidade, custoVagoes.getQtdSistemaDeFreio());
        sendKeys(inputInputSistemaFreioValorUnitario, custoVagoes.getValorSistemaDeFreio());

        sendKeys(inputInputTransporteQuantidade, custoVagoes.getQtdTransporteRemocao());
        sendKeys(inputInputTransporteValorUnitario, custoVagoes.getValorTransporteRemocao());

        sendKeys(inputInputMaoObraQuantidade, custoVagoes.getQtdMaoDeObra());
        sendKeys(inputInputMaoObraValorUnitario, custoVagoes.getValorMaoDeObra());

        sendKeys(inputInputOutrosQuantidade, custoVagoes.getQtdOutros());
        sendKeys(inputInputOutrosValorUnitario, custoVagoes.getValorOutros());
    }

    public void clicarBtnSalvarCusto() {
        clickAndHighlight(buttonDialogButtonYes);
    }

    public void clicarBtnSalvar() {
        scrollToElement(btnSalvarCustos);
        clickAndHighlight(btnSalvarCustos);
    }

    public void validarCustosVagao(Veiculo veiculo, CustoVagoes custoVagoes) {
        expectText(numeroVagao, veiculo.getNumero());
        Assert.assertEquals(getText(numeroVagao), veiculo.getNumero());
        System.out.println("totla : " + custoVagoes.getTotal());
        Assert.assertEquals(getText(valorTotalCustos), formatToCurrency(custoVagoes.getTotal()));
    }

    public void clicarBtnEditarCustosVagao() {
        clickAndHighlight(iconEditarCustoVagao);
    }

    public void validarCustosVagaoEditado(CustoVagoes custoVagoesEditado) {
        expectText(valorTotalCustos, formatToCurrency(custoVagoesEditado.getTotal()));
        Assert.assertEquals(getText(valorTotalCustos), formatToCurrency(custoVagoesEditado.getTotal()));
    }

    public void validaQueATableaDeCustosDoVeiculoDeViaEstaNaTela() {
        scrollToElement(separatorCustosVeiculoDeVia);
        expectElementVisible(tableCustosVeiculoDeVia);
    }

    public void clicarBtnAddCustoVeiculoDeVia() {
        clickAndHighlight(cellBtnAddCustoVeiculoVia.apply(0));
    }

    public void preencheModalCustoVeiculoDeVia(CustoVeiculosVia custoVeiculosVia) {
        sendKeys(inputQtdRodeiro, custoVeiculosVia.getQtdRodeiros());
        sendKeys(inputValorRodeiro, custoVeiculosVia.getValorRodeiros());

        sendKeys(inputQtdTruque, custoVeiculosVia.getQtdTruque());
        sendKeys(inputValorTruque, custoVeiculosVia.getValorTruque());

        sendKeys(inputQtdEngate, custoVeiculosVia.getQtdEngate());
        sendKeys(inputValorEngate, custoVeiculosVia.getValorEngate());

        sendKeys(inputQtdEstrutura, custoVeiculosVia.getQtdEstrutura());
        sendKeys(inputValorEstrutura, custoVeiculosVia.getValorEstrutura());

        sendKeys(inputQtdSistemaFreio, custoVeiculosVia.getQtdSistemaDeFreio());
        sendKeys(inputValorSistemaFreio, custoVeiculosVia.getValorSistemaDeFreio());

        sendKeys(inputQtdTanque, custoVeiculosVia.getQtdTanque());
        sendKeys(inputValorTanque, custoVeiculosVia.getValorTanque());

        sendKeys(inputQtdCaixaGraxa, custoVeiculosVia.getQtdCaixaDeGracha());
        sendKeys(inputValorCaixaGraxa, custoVeiculosVia.getValorCaixaDeGracha());

        sendKeys(inputQtdMaoDeObra, custoVeiculosVia.getQtdMaoDeObra());
        sendKeys(inputValorMaoDeObra, custoVeiculosVia.getValorMaoDeObra());

        sendKeys(inputQtdOutros, custoVeiculosVia.getQtdOutros());
        sendKeys(inputValorOutros, custoVeiculosVia.getValorOutros());
    }

    public void clicarBtnSalvarModalCustoDeVia() {
        clickAndHighlight(btnSalvarModalAddCusto);
    }

    public void validaCustoAdicionadoNaTabelaDeCustoVeiculoDeVia(CustoVeiculosVia custoVeiculosVia) {
        expectText(cellValorCustoVeiculoVia.apply(0), formatToCurrency(custoVeiculosVia.getTotal()));
        Assert.assertEquals(getText(cellValorCustoVeiculoVia.apply(0)), formatToCurrency(custoVeiculosVia.getTotal()));
    }

    public void clicarBtnSalvarCustos() {
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void clicarBtnEditarCustosVeiculoDeVia() {
        clickAndHighlight(cellBtnEditarCustoVeiculoVia.apply(0));
    }

    public void selecionarCheckBoxSemCustosVeiculosVia() {
        clickWithJavaScript(cellCheckBoxSemCustoVeiculoVia.apply(0));
    }

    public void validaCustoZeradoNaTabelaDeCustosDoVeiculoDeVia() {
        expectText(cellValorCustoVeiculoVia.apply(0), "R$ 0,00");
        Assert.assertEquals(getText(cellValorCustoVeiculoVia.apply(0)), "R$ 0,00");
    }

    public void validaQueATableaDeCustosDaLocomotivaEstaNaTela() {
        scrollToElement(separatorCustosLocomotiva);
        expectElementVisible(tableCustosLocomotiva);
    }

    public void clicarBtnAddCustoLocomotiva() {
        clickAndHighlight(cellBtnAddCustoLocomotiva.apply(0));
    }

    public void preencheModalCustoLocomotiva(CustoLocomotiva custoLocomotiva) {
        sendKeys(inputQtdRodeiro, custoLocomotiva.getQtdRodeiros());
        sendKeys(inputValorRodeiro, custoLocomotiva.getValorRodeiros());

        sendKeys(inputQtdTanque, custoLocomotiva.getQtdTanque());
        sendKeys(inputValorTanque, custoLocomotiva.getValorTanque());

        sendKeys(inputQtdEngate, custoLocomotiva.getQtdEngate());
        sendKeys(inputValorEngate, custoLocomotiva.getValorEngate());

        sendKeys(inputQtdEstrutura, custoLocomotiva.getQtdEstrutura());
        sendKeys(inputValorEstrutura, custoLocomotiva.getValorEstrutura());

        sendKeys(inputQtdTruque, custoLocomotiva.getQtdTruque());
        sendKeys(inputValorTruque, custoLocomotiva.getValorTruque());

        sendKeys(inputQtdCaixaGraxa, custoLocomotiva.getQtdCaixaDeGracha());
        sendKeys(inputValorCaixaGraxa, custoLocomotiva.getValorCaixaDeGracha());

        sendKeys(inputQtdLimpaTrilho, custoLocomotiva.getQtdLimpaTrilho());
        sendKeys(inputValorLimpaTrilho, custoLocomotiva.getValorLimpaTrilho());

        sendKeys(inputQtdMaoDeObra, custoLocomotiva.getQtdMaoDeObra());
        sendKeys(inputValorMaoDeObra, custoLocomotiva.getValorMaoDeObra());

        sendKeys(inputQtdOutros, custoLocomotiva.getQtdOutros());
        sendKeys(inputValorOutros, custoLocomotiva.getValorOutros());
    }

    public void clicarBtnSalvarModalCustoLocomotiva() {
        clickAndHighlight(btnSalvarModalAddCusto);
    }

    public void validaCustoAdicionadoNaTabelaDeCustoLocomotiva(CustoLocomotiva custoLocomotiva) {
        expectText(cellValorCustoLocomotiva.apply(0), formatToCurrency(custoLocomotiva.getTotal()));
        Assert.assertEquals(getText(cellValorCustoLocomotiva.apply(0)), formatToCurrency(custoLocomotiva.getTotal()));
    }

    public void clicarBtnEditarCustosLocomotiva() {
        clickAndHighlight(cellBtnEditarCustoLocomotiva.apply(0));
    }

    public void selecionarCheckBoxSemCustosLocomotiva() {
        clickWithJavaScript(cellCheckBoxSemCustoLocomotiva.apply(0));
    }

    public void validaCustoZeradoNaTabelaDeCustosDaLocomotiva() {
        expectText(cellValorCustoLocomotiva.apply(0), "R$ 0,00");
        Assert.assertEquals(getText(cellValorCustoLocomotiva.apply(0)), "R$ 0,00");
    }

    public void clicarBtnOkModalRemoverCusto() {
        clickAndHighlight(buttonDialogButtonYesRemover);
    }

    public void validaFormReadOnlyCustoOutros() {
        Assert.assertTrue(isDisabled(inputHasCostsRadioSimInput));
        Assert.assertTrue(isDisabled(inputHasCostsRadioNaoInput));
    }
}
