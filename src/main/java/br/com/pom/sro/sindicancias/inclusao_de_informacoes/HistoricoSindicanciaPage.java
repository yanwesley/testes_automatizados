package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

import java.util.function.IntFunction;

public class HistoricoSindicanciaPage extends GeralPage {
    IntFunction<By> tdTableCellData = (index) -> By.id("table-cell-data-" + index);
    IntFunction<By> tdTableCellUsuario = (index) -> By.id("table-cell-usuario-" + index);
    IntFunction<By> tdTableCellMatricula = (index) -> By.id("table-cell-matricula-" + index);
    IntFunction<By> tdTableCellPerfilAcesso = (index) -> By.id("table-cell-perfilAcesso-" + index);
    IntFunction<By> tdTableCellTipoEvento = (index) -> By.id("table-cell-tipoEvento-" + index);
    private By separatorHistorico = By.name("Histórico");
    private By selectSelectProfileFilter = By.id("select-profile-filter");
    private By thTableHeaderData = By.id("table-header-data");
    private By thTableHeaderUsuario = By.id("table-header-usuario");
    private By thTableHeaderMatricula = By.id("table-header-matricula");
    private By thTableHeaderPerfilAcesso = By.id("table-header-perfilAcesso");
    private By thTableHeaderTipoEvento = By.id("table-header-tipoEvento");
    private By buttonFooterBackButton = By.id("footer-back-button");

    public void validaExibicaoDaPagina() {
        expectElementVisible(separatorHistorico);
        expectElementVisible(selectSelectProfileFilter);
    }
}
