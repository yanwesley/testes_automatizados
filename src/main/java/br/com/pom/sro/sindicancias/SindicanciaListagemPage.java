package br.com.pom.sro.sindicancias;

import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

import static br.com.api.GeradorDeMassa.getDay;
import static br.com.utils.ReporterUtils.addLogToReport;

public class SindicanciaListagemPage extends GeralPage {

    Function<Integer, By> labelCellId = (Integer index) -> By.id("table-cell-idSindicancia-" + index);
    Function<Integer, By> labelCellDataHora = (Integer index) -> By.id("table-cell-dataHoraAcidente-" + index);
    Function<Integer, By> labelCellPrefixoTrem = (Integer index) -> By.id("table-cell-prefixoTrem-" + index);
    Function<Integer, By> labelCellTipo = (Integer index) -> By.id("table-cell-tipoAcidente-" + index);
    Function<Integer, By> labelCellSecaoBloqueioSB = (Integer index) -> By.id("table-cell-secaoBloqueio-" + index);
    Function<Integer, By> labelCellClassificacao = (Integer index) -> By.id("table-cell-classificacaoSindicancia-" + index);
    Function<Integer, By> labelCellConclusao = (Integer index) -> By.id("table-cell-conclusao-" + index);
    Function<Integer, By> labelCellDiasEmAberto = (Integer index) -> By.id("table-cell-diasEmAberto-" + index);
    Function<Integer, By> labelCellStatus = (Integer index) -> By.id("table-cell-status-" + index);
    Function<Integer, By> cellActions = (Integer index) -> By.id("table-cell-actions-0" + index);
    Function<Integer, By> btnEditarSindicancia = (Integer index) -> By.id("button-edit-sindicancia-" + index);
    Function<Integer, By> iconeConclusaoSro = (Integer index) -> By.cssSelector("#table-cell-conclusao-" + index + " > sro-conclusao-sindicancia-icon > img");
    Function<Integer, By> iconeConclusaoArea = (Integer index) -> By.cssSelector("#table-cell-conclusao-" + index + " > sro-conclusao-area-icon > img");

    private By labelTitulo = By.tagName("sro-default-title");
    private By inputSearch = By.id("search-sinistro");
    private By btnExportar = By.id("button-export");
    private By btnFilter = By.id("button-filter");
    private By tabAnaliseDeDados = By.id("tab-analise");

    /*
    Tabela
     */

    private By headerId = By.id("table-header-idSindicancia");
    private By headerDataHora = By.id("table-header-dataHoraAcidente");
    private By headerPrefixo = By.id("table-header-prefixoTrem");
    private By headerTipo = By.id("table-header-tipoAcidente");
    private By headerSecaoBloqueioSB = By.id("table-header-secaoBloqueio");
    private By headerClassificacao = By.id("table-header-classificacaoSindicancia");
    private By headerConclusao = By.id("table-header-conclusao");
    private By headerDiasEmAberto = By.id("table-header-diasEmAberto");
    private By headerStatus = By.id("table-header-status");
    private By headerActions = By.id("table-header-actions");
    private By tagClassificacao = By.id("classificacao-tag");


    private By headerEdit = By.id("table-header-edit");
    private By headerShow = By.id("table-header-show");
    private By labelCellSecaoBloqueio0 = By.id("table-cell-secaoBloqueio-0");
    private By tableListagem = By.cssSelector("body > sro-root > sro-shell > div > sro-tabs-sindicancia > sro-table-sindicancia > div > table > tbody");

        /*
    Campos do filtro de data
     */

    private By inputDataInicio = By.id("input-advanced-filter-date-since");
    private By inputDataFim = By.id("input-advanced-filter-date-until");
    private By btnAplicarFiltro = By.id("button-advanced-filter-apply");
    private By btnRemoverFiltro = By.id("button-chip-clear-filter");

    public void selecionarTABColetaDeDados() {
        clickAndHighlight(tabAnaliseDeDados);
    }

    public void validaExibicaoDaPagina() {
        expectElementVisible(labelTitulo);
        expectElementVisible(inputSearch);
        expectElementVisible(btnFilter);
        expectElementVisible(tabAnaliseDeDados);
    }

    public void validaRegistro(int linhas) {
        Assert.assertTrue(countChildElement(tableListagem, "tr") > 0);
    }

    public void validarDiasEmAberto(int linha) {
        long dias = getDayCount(getText(labelCellDataHora.apply(linha - 1)), getDay());
        Assert.assertEquals(getText(labelCellDiasEmAberto.apply(linha - 1)), String.valueOf(dias));
        addLogToReport("Validado que os Dias em aberto deveria ser " + dias + " e é " + getText(labelCellDiasEmAberto.apply(linha - 1)));
    }

    public void ordenarIdDecrescente() {
        clickAndHighlight(headerId);
        expectLoading();
        clickAndHighlight(headerId);
        waitTime();
    }

    public void clicarBtnEditarListagem(int linha) {
        clickAndHighlight(btnEditarSindicancia.apply(linha - 1));
    }

    public void validarStatusConclusaoNaoIniciado() {
        mouseOver(iconeConclusaoSro.apply(0));
        Assert.assertEquals(getAttribute(iconeConclusaoSro.apply(0), "alt"), "CIPIA VIA: Não iniciado\n" +
                "    CIPIA Material Rodante: Não iniciado\n" +
                "    CIPIA Operação: Não iniciado\n" +
                "    Segurança Patrimonial: Não iniciado");
        String imgEmAndamento = getAttribute(iconeConclusaoSro.apply(0), "src");
        Assert.assertTrue(imgEmAndamento.endsWith("icone-status-nao-iniciado.svg"), "é esperado que o icone termine com 'icone-status-nao-iniciado.svg'");
    }

    public void validarStatusConclusaoFinalizado() {
        expectLoading();
        mouseOver(iconeConclusaoSro.apply(0));
        Assert.assertEquals(getAttribute(iconeConclusaoSro.apply(0), "alt"), "CIPIA VIA: Finalizado\n" +
                "    CIPIA Material Rodante: Finalizado\n" +
                "    CIPIA Operação: Finalizado\n" +
                "    Segurança Patrimonial: Finalizado");
        String imgConclusao = getAttribute(iconeConclusaoSro.apply(0), "src");
        Assert.assertTrue(imgConclusao.endsWith("icone-status-finalizado.svg"), "é esperado que o icone termine com'icone-status-finalizado.svg'");
    }

    public void validarStatusConclusaoDaAreaFinalizado() {
        expectLoading();
        expectElementVisible(iconeConclusaoArea.apply(0));
        mouseOver(iconeConclusaoArea.apply(0));
        Assert.assertEquals(getAttribute(iconeConclusaoArea.apply(0), "alt"), "Finalizado");
        String imgConclusao = getAttribute(iconeConclusaoArea.apply(0), "src");
        Assert.assertTrue(imgConclusao.endsWith("icone-status-finalizado.svg"), "é esperado que o icone termine com'icone-status-finalizado.svg'");
    }

    public void validaStatusClassificacao(int linha, String classificacao) {
        expectLoading();
        linha--;
        Assert.assertEquals(getText(labelCellClassificacao.apply(linha)).toUpperCase(), classificacao);
    }

    public void validarStatusSindicancia(String status, int linha) {
        linha--;
        expectLoading();
        Assert.assertEquals(getText(labelCellStatus.apply(linha)), status);
    }

    public void validarStatusConclusaoIniciado() {
        expectLoading();
        mouseOver(iconeConclusaoSro.apply(0));
        String alt = getAttribute(iconeConclusaoSro.apply(0), "alt");
        Assert.assertEquals(alt, "CIPIA VIA: Iniciado\n" +
                "    CIPIA Material Rodante: Iniciado\n" +
                "    CIPIA Operação: Iniciado\n" +
                "    Segurança Patrimonial: Iniciado");
        String imgConclusao = getAttribute(iconeConclusaoSro.apply(0), "src");
        Assert.assertTrue(imgConclusao.endsWith("icone-status-em-edicao.svg"), "é esperado que o icone termine com'icone-status-em-edicao.svg'" + " atual: " + imgConclusao);

    }

    public void validaSindicanciaFinalizada(Sinistro sinistro, int linha) {
        linha--;
        Assert.assertEquals(getText(labelCellId.apply(linha)), String.valueOf(sinistro.getIdSinistro()));
    }
}

