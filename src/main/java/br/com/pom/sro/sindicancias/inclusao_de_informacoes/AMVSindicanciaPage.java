package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.AMV;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class AMVSindicanciaPage extends GeralPage {

    Function<Integer, By> optionPerfilTrilho = (Integer index) -> By.id("option-amv-perfil-trilho-" + index);
    private By separatorDadosGerais = By.name("DADOS GERAIS");
    private By separatorPrincipal = By.name("Principal");
    private By separatorCotasDeSalvaGuardaPrincipal = By.cssSelector("#formDadosAMV > div > div:nth-child(5) > sro-title-label > div");
    private By separatorFuncionamentoPrincipal = By.cssSelector("#formDadosAMV > div > div:nth-child(6) > sro-title-label > div");
    private By separatorSegurancaPrincipal = By.cssSelector("#formDadosAMV > div > div:nth-child(7) > sro-title-label > div");
    private By separatorBarraDeConjugacao = By.name("Barra de conjugação");
    private By separatorAgulhas = By.name("Agulhas");
    private By separatorDesviada = By.name("Desviada");
    private By separatorCotasDeSalvaGuardaDesviada = By.cssSelector("#formDadosAMV > div > div:nth-child(13) > sro-title-label > div ");
    private By separatorFuncionamentoDesviada = By.cssSelector("#formDadosAMV > div > div:nth-child(14) > sro-title-label > div ");
    private By separatorSegurancaDesviada = By.cssSelector("#formDadosAMV > div > div:nth-child(15) > sro-title-label > div ");
    private By separatorTREncontro = By.name("Tr encosto");
    private By separatorChanfro = By.name("Chanfro");
    private By separatorAberturaAMV = By.name("Abertura AMV");
    private By separatorTrilho = By.name("Trilho");
    private By optionAmvTipoBitolaLarga = By.id("option-amv-tipo-bitola-larga");
    private By optionAmvTipoBitolaMetrica = By.id("option-amv-tipo-bitola-metrica");
    private By optionAmvTipoBitolaMista = By.id("option-amv-tipo-bitola-mista");
    private By selectAmvTipoBitola = By.id("select-amv-tipo-bitola");
    private By inputAmvEquipamento = By.id("input-amv-equipamento");
    private By inputAmvSupervisor = By.id("input-amv-supervisor");
    private By buttonButtonSearchSupervisor = By.id("button-search-supervisor");
    private By inputAmvPatio = By.id("input-amv-patio");
    private By inputInputAmvBitolapontaagulhametricaprincipal = By.id("input-amv-bitolaPontaAgulhaMetricaPrincipal");
    private By inputInputAmvBitolaPontaAgulhaLargaPrincipal = By.id("input-amv-bitolaPontaAgulhaLargaPrincipal");
    private By inputInputAmvLivrepassagempontaagulhalargaprincipal = By.id("input-amv-livrePassagemPontaAgulhaLargaPrincipal");
    private By inputInputAmvLivrepassagempontaagulhametricaprincipal = By.id("input-amv-livrePassagemPontaAgulhaMetricaPrincipal");
    private By inputInputAmvProtecaopontajacareanteriormetricaprincipal = By.id("input-amv-protecaoPontaJacareAnteriorMetricaPrincipal");
    private By inputInputAmvProtecaopontajacareanteriorlargaprincipal = By.id("input-amv-protecaoPontaJacareAnteriorLargaPrincipal");
    private By inputInputAmvProtecaopontajacarelargaprincipal = By.id("input-amv-protecaoPontaJacareLargaPrincipal");
    private By inputInputAmvProtecaopontajacaremetricaprincipal = By.id("input-amv-protecaoPontaJacareMetricaPrincipal");
    private By inputInputAmvProtecaopontajacareposteriormetricaprincipal = By.id("input-amv-protecaoPontaJacarePosteriorMetricaPrincipal");
    private By inputInputAmvProtecaopontajacareposteriorlargaprincipal = By.id("input-amv-protecaoPontaJacarePosteriorLargaPrincipal");
    private By inputInputAmvBitolabicoposteriorjacareduplometricaprincipal = By.id("input-amv-bitolaBicoPosteriorJacareDuploMetricaPrincipal");
    private By inputInputAmvBitolabicoposteriorjacareduplolargaprincipal = By.id("input-amv-bitolaBicoPosteriorJacareDuploLargaPrincipal");
    private By radioGroupRadioGroupAmvPressaoagulhaokprincipal = By.id("radio-group-amv-pressaoAgulhaOKPrincipal");
    private By radioButtonRadioGroupAmvPressaoagulhaokprincipalSim = By.id("radio-group-amv-pressaoAgulhaOKPrincipal-sim");
    private By inputRadioGroupAmvPressaoagulhaokprincipalSimInput = By.id("radio-group-amv-pressaoAgulhaOKPrincipal-sim-input");
    private By radioButtonRadioGroupAmvPressaoagulhaokprincipalNao = By.id("radio-group-amv-pressaoAgulhaOKPrincipal-nao");
    private By inputRadioGroupAmvPressaoagulhaokprincipalNaoInput = By.id("radio-group-amv-pressaoAgulhaOKPrincipal-nao-input");
    private By radioGroupRadioGroupAmvCondicaolastrookprincipal = By.id("radio-group-amv-condicaoLastroOKPrincipal");
    private By radioButtonRadioGroupAmvCondicaolastrookprincipalSim = By.id("radio-group-amv-condicaoLastroOKPrincipal-sim");
    private By inputRadioGroupAmvCondicaolastrookprincipalSimInput = By.id("radio-group-amv-condicaoLastroOKPrincipal-sim-input");
    private By radioButtonRadioGroupAmvCondicaolastrookprincipalNao = By.id("radio-group-amv-condicaoLastroOKPrincipal-nao");
    private By inputRadioGroupAmvCondicaolastrookprincipalNaoInput = By.id("radio-group-amv-condicaoLastroOKPrincipal-nao-input");
    private By inputInputAmvPercdormentesinserviveisprincipal = By.id("input-amv-percDormentesInserviveisPrincipal");
    private By radioGroupRadioGroupAmvFixacaookprincipal = By.id("radio-group-amv-fixacaoOKPrincipal");
    private By radioButtonRadioGroupAmvFixacaookprincipalSim = By.id("radio-group-amv-fixacaoOKPrincipal-sim");
    private By inputRadioGroupAmvFixacaookprincipalSimInput = By.id("radio-group-amv-fixacaoOKPrincipal-sim-input");
    private By radioButtonRadioGroupAmvFixacaookprincipalNao = By.id("radio-group-amv-fixacaoOKPrincipal-nao");
    private By inputRadioGroupAmvFixacaookprincipalNaoInput = By.id("radio-group-amv-fixacaoOKPrincipal-nao-input");
    private By inputInputAmvSistemasegurancaamvprincipal = By.id("input-amv-sistemaSegurancaAMVPrincipal");
    private By radioGroupRadioGroupAmvBandeirolapintadasinalizadaprincipal = By.id("radio-group-amv-bandeirolaPintadaSinalizadaPrincipal");
    private By radioButtonRadioGroupAmvBandeirolapintadasinalizadaprincipalSim = By.id("radio-group-amv-bandeirolaPintadaSinalizadaPrincipal-sim");
    private By inputRadioGroupAmvBandeirolapintadasinalizadaprincipalSimInput = By.id("radio-group-amv-bandeirolaPintadaSinalizadaPrincipal-sim-input");
    private By radioButtonRadioGroupAmvBandeirolapintadasinalizadaprincipalNao = By.id("radio-group-amv-bandeirolaPintadaSinalizadaPrincipal-nao");
    private By inputRadioGroupAmvBandeirolapintadasinalizadaprincipalNaoInput = By.id("radio-group-amv-bandeirolaPintadaSinalizadaPrincipal-nao-input");
    private By radioGroupRadioGroupAmvExisteexcessomatoamvprincipal = By.id("radio-group-amv-existeExcessoMatoAMVPrincipal");
    private By radioButtonRadioGroupAmvExisteexcessomatoamvprincipalSim = By.id("radio-group-amv-existeExcessoMatoAMVPrincipal-sim");
    private By inputRadioGroupAmvExisteexcessomatoamvprincipalSimInput = By.id("radio-group-amv-existeExcessoMatoAMVPrincipal-sim-input");
    private By radioButtonRadioGroupAmvExisteexcessomatoamvprincipalNao = By.id("radio-group-amv-existeExcessoMatoAMVPrincipal-nao");
    private By inputRadioGroupAmvExisteexcessomatoamvprincipalNaoInput = By.id("radio-group-amv-existeExcessoMatoAMVPrincipal-nao-input");
    private By radioGroupRadioGroupAmvFrequenciainspecaoprogramadaprincipal = By.id("radio-group-amv-frequenciaInspecaoProgramadaPrincipal");
    private By radioButtonRadioGroupAmvFrequenciainspecaoprogramadaprincipalSim = By.id("radio-group-amv-frequenciaInspecaoProgramadaPrincipal-sim");
    private By inputRadioGroupAmvFrequenciainspecaoprogramadaprincipalSimInput = By.id("radio-group-amv-frequenciaInspecaoProgramadaPrincipal-sim-input");
    private By radioButtonRadioGroupAmvFrequenciainspecaoprogramadaprincipalNao = By.id("radio-group-amv-frequenciaInspecaoProgramadaPrincipal-nao");
    private By inputRadioGroupAmvFrequenciainspecaoprogramadaprincipalNaoInput = By.id("radio-group-amv-frequenciaInspecaoProgramadaPrincipal-nao-input");
    private By radioGroupRadioGroupAmvFrequenciachecklistemergencialprincipal = By.id("radio-group-amv-frequenciaCheckListEmergencialPrincipal");
    private By radioButtonRadioGroupAmvFrequenciachecklistemergencialprincipalSim = By.id("radio-group-amv-frequenciaCheckListEmergencialPrincipal-sim");
    private By inputRadioGroupAmvFrequenciachecklistemergencialprincipalSimInput = By.id("radio-group-amv-frequenciaCheckListEmergencialPrincipal-sim-input");
    private By radioButtonRadioGroupAmvFrequenciachecklistemergencialprincipalNao = By.id("radio-group-amv-frequenciaCheckListEmergencialPrincipal-nao");
    private By inputRadioGroupAmvFrequenciachecklistemergencialprincipalNaoInput = By.id("radio-group-amv-frequenciaCheckListEmergencialPrincipal-nao-input");
    private By inputInputAmvBarraconjugacaoqtd = By.id("input-amv-barraConjugacaoQtd");
    private By radioGroupRadioGroupAmvBarraconjugacaoisoladas = By.id("radio-group-amv-barraConjugacaoIsoladas");
    private By radioButtonRadioGroupAmvBarraconjugacaoisoladasSim = By.id("radio-group-amv-barraConjugacaoIsoladas-sim");
    private By inputRadioGroupAmvBarraconjugacaoisoladasSimInput = By.id("radio-group-amv-barraConjugacaoIsoladas-sim-input");
    private By radioButtonRadioGroupAmvBarraconjugacaoisoladasNao = By.id("radio-group-amv-barraConjugacaoIsoladas-nao");
    private By inputRadioGroupAmvBarraconjugacaoisoladasNaoInput = By.id("radio-group-amv-barraConjugacaoIsoladas-nao-input");
    private By inputInputAmvPrincipalAgulhaesquerdadetalhe = By.id("input-amv-principal-agulhaEsquerdaDetalhe");
    private By inputInputAmvPrincipalAgulhaesquerdacomprimento = By.id("input-amv-principal-agulhaEsquerdaComprimento");
    private By inputInputAmvPrincipalAgulhadireitadetalhe = By.id("input-amv-principal-agulhaDireitaDetalhe");
    private By inputInputAmvPrincipalAgulhadireitacomprimento = By.id("input-amv-principal-agulhaDireitaComprimento");
    private By inputInputAmvBitolapontaagulhametricadesviada = By.id("input-amv-bitolaPontaAgulhaMetricaDesviada");
    private By inputInputAmvBitolapontaagulhalargadesviada = By.id("input-amv-bitolaPontaAgulhaLargaDesviada");
    private By inputInputAmvLivrepassagempontaagulhalargadesviada = By.id("input-amv-livrePassagemPontaAgulhaLargaDesviada");
    private By inputInputAmvLivrepassagempontaagulhametricadesviada = By.id("input-amv-livrePassagemPontaAgulhaMetricaDesviada");
    private By inputInputAmvProtecaopontajacareanteriormetricadesviada = By.id("input-amv-protecaoPontaJacareAnteriorMetricaDesviada");
    private By inputInputAmvProtecaopontajacareanteriorlargadesviada = By.id("input-amv-protecaoPontaJacareAnteriorLargaDesviada");
    private By inputInputAmvProtecaopontajacarelargadesviada = By.id("input-amv-protecaoPontaJacareLargaDesviada");
    private By inputInputAmvProtecaopontajacaremetricadesviada = By.id("input-amv-protecaoPontaJacareMetricaDesviada");
    private By inputInputAmvProtecaopontajacareposteriormetricadesviada = By.id("input-amv-protecaoPontaJacarePosteriorMetricaDesviada");
    private By inputInputAmvProtecaopontajacareposteriorlargadesviada = By.id("input-amv-protecaoPontaJacarePosteriorLargaDesviada");
    private By inputInputAmvBitolabicoposteriorjacareduplometricadesviada = By.id("input-amv-bitolaBicoPosteriorJacareDuploMetricaDesviada");
    private By inputInputAmvBitolabicoposteriorjacareduplolargadesviada = By.id("input-amv-bitolaBicoPosteriorJacareDuploLargaDesviada");
    private By radioGroupRadioGroupAmvPressaoagulhaokdesviada = By.id("radio-group-amv-pressaoAgulhaOKDesviada");
    private By radioButtonRadioGroupAmvPressaoagulhaokdesviadaSim = By.id("radio-group-amv-pressaoAgulhaOKDesviada-sim");
    private By inputRadioGroupAmvPressaoagulhaokdesviadaSimInput = By.id("radio-group-amv-pressaoAgulhaOKDesviada-sim-input");
    private By radioButtonRadioGroupAmvPressaoagulhaokdesviadaNao = By.id("radio-group-amv-pressaoAgulhaOKDesviada-nao");
    private By inputRadioGroupAmvPressaoagulhaokdesviadaNaoInput = By.id("radio-group-amv-pressaoAgulhaOKDesviada-nao-input");
    private By radioGroupRadioGroupAmvCondicaolastrookdesviada = By.id("radio-group-amv-condicaoLastroOKDesviada");
    private By radioButtonRadioGroupAmvCondicaolastrookdesviadaSim = By.id("radio-group-amv-condicaoLastroOKDesviada-sim");
    private By inputRadioGroupAmvCondicaolastrookdesviadaSimInput = By.id("radio-group-amv-condicaoLastroOKDesviada-sim-input");
    private By radioButtonRadioGroupAmvCondicaolastrookdesviadaNao = By.id("radio-group-amv-condicaoLastroOKDesviada-nao");
    private By inputRadioGroupAmvCondicaolastrookdesviadaNaoInput = By.id("radio-group-amv-condicaoLastroOKDesviada-nao-input");
    private By inputInputAmvPercdormentesinserviveisdesviada = By.id("input-amv-percDormentesInserviveisDesviada");
    private By radioGroupRadioGroupAmvFixacaookdesviada = By.id("radio-group-amv-fixacaoOKDesviada");
    private By radioButtonRadioGroupAmvFixacaookdesviadaSim = By.id("radio-group-amv-fixacaoOKDesviada-sim");
    private By inputRadioGroupAmvFixacaookdesviadaSimInput = By.id("radio-group-amv-fixacaoOKDesviada-sim-input");
    private By radioButtonRadioGroupAmvFixacaookdesviadaNao = By.id("radio-group-amv-fixacaoOKDesviada-nao");
    private By inputRadioGroupAmvFixacaookdesviadaNaoInput = By.id("radio-group-amv-fixacaoOKDesviada-nao-input");
    private By inputInputAmvSistemasegurancaamvdesviada = By.id("input-amv-sistemaSegurancaAMVDesviada");
    private By radioGroupRadioGroupAmvBandeirolapintadasinalizadadesviada = By.id("radio-group-amv-bandeirolaPintadaSinalizadaDesviada");
    private By radioButtonRadioGroupAmvBandeirolapintadasinalizadadesviadaSim = By.id("radio-group-amv-bandeirolaPintadaSinalizadaDesviada-sim");
    private By inputRadioGroupAmvBandeirolapintadasinalizadadesviadaSimInput = By.id("radio-group-amv-bandeirolaPintadaSinalizadaDesviada-sim-input");
    private By radioButtonRadioGroupAmvBandeirolapintadasinalizadadesviadaNao = By.id("radio-group-amv-bandeirolaPintadaSinalizadaDesviada-nao");
    private By inputRadioGroupAmvBandeirolapintadasinalizadadesviadaNaoInput = By.id("radio-group-amv-bandeirolaPintadaSinalizadaDesviada-nao-input");
    private By radioGroupRadioGroupAmvExisteexcessomatoamvdesviada = By.id("radio-group-amv-existeExcessoMatoAMVDesviada");
    private By radioButtonRadioGroupAmvExisteexcessomatoamvdesviadaSim = By.id("radio-group-amv-existeExcessoMatoAMVDesviada-sim");
    private By inputRadioGroupAmvExisteexcessomatoamvdesviadaSimInput = By.id("radio-group-amv-existeExcessoMatoAMVDesviada-sim-input");
    private By radioButtonRadioGroupAmvExisteexcessomatoamvdesviadaNao = By.id("radio-group-amv-existeExcessoMatoAMVDesviada-nao");
    private By inputRadioGroupAmvExisteexcessomatoamvdesviadaNaoInput = By.id("radio-group-amv-existeExcessoMatoAMVDesviada-nao-input");
    private By radioGroupRadioGroupAmvFrequenciainspecaoprogramadadesviada = By.id("radio-group-amv-frequenciaInspecaoProgramadaDesviada");
    private By radioButtonRadioGroupAmvFrequenciainspecaoprogramadadesviadaSim = By.id("radio-group-amv-frequenciaInspecaoProgramadaDesviada-sim");
    private By inputRadioGroupAmvFrequenciainspecaoprogramadadesviadaSimInput = By.id("radio-group-amv-frequenciaInspecaoProgramadaDesviada-sim-input");
    private By radioButtonRadioGroupAmvFrequenciainspecaoprogramadadesviadaNao = By.id("radio-group-amv-frequenciaInspecaoProgramadaDesviada-nao");
    private By inputRadioGroupAmvFrequenciainspecaoprogramadadesviadaNaoInput = By.id("radio-group-amv-frequenciaInspecaoProgramadaDesviada-nao-input");
    private By radioGroupRadioGroupAmvFrequenciachecklistemergencialdesviada = By.id("radio-group-amv-frequenciaCheckListEmergencialDesviada");
    private By radioButtonRadioGroupAmvFrequenciachecklistemergencialdesviadaSim = By.id("radio-group-amv-frequenciaCheckListEmergencialDesviada-sim");
    private By inputRadioGroupAmvFrequenciachecklistemergencialdesviadaSimInput = By.id("radio-group-amv-frequenciaCheckListEmergencialDesviada-sim-input");
    private By radioButtonRadioGroupAmvFrequenciachecklistemergencialdesviadaNao = By.id("radio-group-amv-frequenciaCheckListEmergencialDesviada-nao");
    private By inputRadioGroupAmvFrequenciachecklistemergencialdesviadaNaoInput = By.id("radio-group-amv-frequenciaCheckListEmergencialDesviada-nao-input");
    private By radioGroupRadioGroupAmvTrencostoesquerda = By.id("radio-group-amv-trEncostoEsquerda");
    private By radioButtonRadioGroupAmvTrencostoesquerdaReto = By.id("radio-group-amv-trEncostoEsquerda-reto");
    private By inputRadioGroupAmvTrencostoesquerdaRetoInput = By.id("radio-group-amv-trEncostoEsquerda-reto-input");
    private By radioButtonRadioGroupAmvTrencostoesquerdaCurvo = By.id("radio-group-amv-trEncostoEsquerda-curvo");
    private By inputRadioGroupAmvTrencostoesquerdaCurvoInput = By.id("radio-group-amv-trEncostoEsquerda-curvo-input");
    private By radioGroupRadioGroupAmvTrencostodireito = By.id("radio-group-amv-trEncostoDireito");
    private By radioButtonRadioGroupAmvTrencostodireitoReto = By.id("radio-group-amv-trEncostoDireito-reto");
    private By inputRadioGroupAmvTrencostodireitoRetoInput = By.id("radio-group-amv-trEncostoDireito-reto-input");
    private By radioButtonRadioGroupAmvTrencostodireitoCurvo = By.id("radio-group-amv-trEncostoDireito-curvo");
    private By inputRadioGroupAmvTrencostodireitoCurvoInput = By.id("radio-group-amv-trEncostoDireito-curvo-input");
    private By radioGroupRadioGroupAmvChanfroesquerda = By.id("radio-group-amv-chanfroEsquerda");
    private By radioButtonRadioGroupAmvChanfroesquerdaCom = By.id("radio-group-amv-chanfroEsquerda-com");
    private By inputRadioGroupAmvChanfroesquerdaComInput = By.id("radio-group-amv-chanfroEsquerda-com-input");
    private By radioButtonRadioGroupAmvChanfroesquerdaSem = By.id("radio-group-amv-chanfroEsquerda-sem");
    private By inputRadioGroupAmvChanfroesquerdaSemInput = By.id("radio-group-amv-chanfroEsquerda-sem-input");
    private By radioGroupRadioGroupAmvChanfrodireito = By.id("radio-group-amv-chanfroDireito");
    private By radioButtonRadioGroupAmvChanfrodireitoCom = By.id("radio-group-amv-chanfroDireito-com");
    private By inputRadioGroupAmvChanfrodireitoComInput = By.id("radio-group-amv-chanfroDireito-com-input");
    private By radioButtonRadioGroupAmvChanfrodireitoSem = By.id("radio-group-amv-chanfroDireito-sem");
    private By inputRadioGroupAmvChanfrodireitoSemInput = By.id("radio-group-amv-chanfroDireito-sem-input");
    private By inputInputAberturaamv = By.id("input-aberturaAMV");
    private By selectSelectAmvPerfilTrilho = By.id("select-amv-perfil-trilho");
    private By buttonBtnAmvSalvar = By.id("btn-amv-salvar");
    private By buttonBtnAmvSalvarAvancar = By.id("btn-amv-salvar-avancar");
    private By buttonBtnAmvCancelar = By.id("btn-amv-cancelar");

    public void validaQueEstaNaPaginaDeAMV() {
        expectElementVisible(separatorPrincipal);
    }

    public void preencheDadosGeraisLargaPrincipal(AMV amv) {
        scrollToElement(separatorDadosGerais);
        waitTime(500);
        clickAndHighlight(selectAmvTipoBitola);
        clickAndHighlight(optionAmvTipoBitolaLarga);
        sendKeysWithJavaScript(inputAmvEquipamento, amv.getEquipamentoDadosGerais());
        clickAndHighlight(buttonButtonSearchSupervisor);
        selecionarResponsavel(amv.getSupervisorDadosGerais());
        amv.setSupervisorDadosGerais(getValue(inputAmvSupervisor));
        sendKeys(inputAmvPatio, amv.getPatioDadosGerais());
    }

    public void preencheCotasDeSalvaguardaLargaPrincipal(AMV amv) {
        scrollToElement(separatorCotasDeSalvaGuardaPrincipal);
        waitTime(500);
        sendKeys(inputInputAmvBitolaPontaAgulhaLargaPrincipal, amv.getBitolaPontaAgulhaCota1PrincipalLarga());
        sendKeys(inputInputAmvLivrepassagempontaagulhalargaprincipal, amv.getBitolaLivrePassagemPontaAgulhaCota2PrincipalLarga());
        sendKeys(inputInputAmvProtecaopontajacarelargaprincipal, amv.getBitolaProtecaoPontaJacareCota3Principal());
    }

    public void preencheFuncionamentoPrincipal(AMV amv) {
        scrollToElement(separatorFuncionamentoPrincipal);
        waitTime(500);
        if (amv.isPressaoAgulhaEstaOkPrincipal()) {
            clickWithAction(radioButtonRadioGroupAmvPressaoagulhaokprincipalSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvPressaoagulhaokprincipalNao);
        }

        sendKeys(inputInputAmvPercdormentesinserviveisprincipal, amv.getPorcentagemDosDormentesInserviveisPrincipal());

        if (amv.isCondicaoDoLastroEstaOkPrincipal()) {
            clickWithAction(radioButtonRadioGroupAmvCondicaolastrookprincipalSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvCondicaolastrookprincipalNao);
        }

        if (amv.isFixacaoEstaOkPrincipal()) {
            clickWithAction(radioButtonRadioGroupAmvFixacaookprincipalSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvFixacaookprincipalNao);
        }
    }

    public void preencheSegurancaPrincipal(AMV amv) {
        scrollToElement(separatorSegurancaPrincipal);
        waitTime(500);
        sendKeysWithJavaScript(inputInputAmvSistemasegurancaamvprincipal, amv.getSistemaDeSegurancaDoAMVPrincipal());

        if (amv.isExisteExcessoDeMatoNoAMVPrincipal()) {
            clickWithAction(radioButtonRadioGroupAmvExisteexcessomatoamvprincipalSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvExisteexcessomatoamvprincipalNao);
        }

        if (amv.isFrequenciaDoCheckListEstaOkPrincipal()) {
            clickWithAction(radioButtonRadioGroupAmvFrequenciachecklistemergencialprincipalSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvFrequenciachecklistemergencialprincipalNao);
        }

        if (amv.isBandeirolaEstaPintadaESinalizadaPrincipal()) {
            clickWithAction(radioButtonRadioGroupAmvBandeirolapintadasinalizadaprincipalSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvBandeirolapintadasinalizadaprincipalNao);
        }

        if (amv.isFrequenciaDeInspecaoProgramadaEstaOkPrincipal()) {
            clickWithAction(radioButtonRadioGroupAmvFrequenciainspecaoprogramadaprincipalSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvFrequenciainspecaoprogramadaprincipalNao);
        }
    }

    public void preencheBarraDeConjugacaoPrincipal(AMV amv) {
        scrollToElement(separatorBarraDeConjugacao);
        waitTime(500);
        sendKeys(inputInputAmvBarraconjugacaoqtd, amv.getQuantidadeBarraDeConjugacaoPrincipal());

        if (amv.isBarraConjugacaoSaoIsoladasPrincipal()) {
            clickWithAction(radioButtonRadioGroupAmvBarraconjugacaoisoladasSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvBarraconjugacaoisoladasNao);
        }
    }

    public void preencheAgulhasPrincipal(AMV amv) {
        scrollToElement(separatorAgulhas);
        waitTime(500);
        sendKeys(inputInputAmvPrincipalAgulhaesquerdadetalhe, amv.getDetalheAgulhaEsquerdaPrincipal());
        sendKeys(inputInputAmvPrincipalAgulhaesquerdacomprimento, amv.getComprimentoAgulhaEsquerdaPrincipal());
        sendKeys(inputInputAmvPrincipalAgulhadireitadetalhe, amv.getDetalheAgulhaDireitaPrincipal());
        sendKeys(inputInputAmvPrincipalAgulhadireitacomprimento, amv.getComprimentoAgulhaDireitaPrincipal());
    }

    public void preencheCotasDeSalvaguardaLargaDesviada(AMV amv) {
        scrollToElement(separatorCotasDeSalvaGuardaDesviada);
        waitTime(500);
        sendKeys(inputInputAmvBitolapontaagulhalargadesviada, amv.getBitolaPontaAgulhaCota1DesviadaLarga());
        sendKeys(inputInputAmvLivrepassagempontaagulhalargadesviada, amv.getBitolaLivrePassagemPontaAgulhaCota2DesviadaLarga());
        sendKeys(inputInputAmvProtecaopontajacarelargadesviada, amv.getBitolaProtecaoPontaJacareCota3Desviada());
    }

    public void preencheFuncionamentoDesviada(AMV amv) {
        scrollToElement(separatorFuncionamentoDesviada);
        waitTime(500);
        if (amv.isPressaoAgulhaEstaOkDesviada()) {
            clickWithAction(radioButtonRadioGroupAmvPressaoagulhaokdesviadaSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvPressaoagulhaokdesviadaNao);
        }

        sendKeys(inputInputAmvPercdormentesinserviveisdesviada, amv.getPorcentagemDosDormentesInserviveisDesviada());

        if (amv.isCondicaoDoLastroEstaOkDesviada()) {
            clickWithAction(radioButtonRadioGroupAmvCondicaolastrookdesviadaSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvCondicaolastrookdesviadaNao);
        }

        if (amv.isFixacaoEstaOkDesviada()) {
            clickWithAction(radioButtonRadioGroupAmvFixacaookdesviadaSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvFixacaookdesviadaNao);
        }
    }

    public void preencheSegurancaDesviada(AMV amv) {
        scrollToElement(separatorSegurancaDesviada);
        waitTime(500);
        sendKeysWithJavaScript(inputInputAmvSistemasegurancaamvdesviada, amv.getSistemaDeSegurancaDoAMVDesviada());

        if (amv.isExisteExcessoDeMatoNoAMVDesviada()) {
            clickWithAction(radioButtonRadioGroupAmvExisteexcessomatoamvdesviadaSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvExisteexcessomatoamvdesviadaNao);
        }

        if (amv.isFrequenciaDoCheckListEstaOkDesviada()) {
            clickWithAction(radioButtonRadioGroupAmvFrequenciachecklistemergencialdesviadaSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvFrequenciachecklistemergencialdesviadaNao);
        }

        if (amv.isBandeirolaEstaPintadaESinalizadaDesviada()) {
            clickWithAction(radioButtonRadioGroupAmvBandeirolapintadasinalizadadesviadaSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvBandeirolapintadasinalizadadesviadaNao);
        }

        if (amv.isFrequenciaDeInspecaoProgramadaEstaOkDesviada()) {
            clickWithAction(radioButtonRadioGroupAmvFrequenciainspecaoprogramadadesviadaSim);
        } else {
            clickWithAction(radioButtonRadioGroupAmvFrequenciainspecaoprogramadadesviadaNao);
        }
    }

    public void preencheTREncostoDesviada(AMV amv) {
        scrollToElement(separatorTREncontro);
        waitTime(500);
        if (amv.isTrEncostroEsquerdo()) {
            clickWithAction(radioButtonRadioGroupAmvTrencostoesquerdaReto);
        } else {
            clickWithAction(radioButtonRadioGroupAmvTrencostoesquerdaCurvo);
        }

        if (amv.isTrEncostroDireito()) {
            clickWithAction(radioButtonRadioGroupAmvTrencostodireitoReto);
        } else {
            clickWithAction(radioButtonRadioGroupAmvTrencostodireitoCurvo);
        }
    }

    public void preencheChanfroDesviada(AMV amv) {
        scrollToElement(separatorChanfro);
        waitTime(500);
        if (amv.isChanfroEsquerdo()) {
            clickWithAction(radioButtonRadioGroupAmvChanfroesquerdaCom);
        } else {
            clickWithAction(radioButtonRadioGroupAmvChanfroesquerdaSem);
        }

        if (amv.isChanfroDireito()) {
            clickWithAction(radioButtonRadioGroupAmvChanfrodireitoCom);
        } else {
            clickWithAction(radioButtonRadioGroupAmvChanfrodireitoSem);
        }
    }

    public void preencheAberturaAMVDesviada(AMV amv) {
        scrollToElement(separatorAberturaAMV);
        waitTime(500);
        sendKeys(inputInputAberturaamv, amv.getHoraAberturaDoAMV());
    }

    public void preencheTrilhoDesviada(AMV amv) {
        scrollToElement(separatorTrilho);
        waitTime(500);
        clickAndHighlight(selectSelectAmvPerfilTrilho);
        clickAndHighlight(optionPerfilTrilho.apply(amv.getTipoPerfilTrilhoIndex()));
        amv.setTipoPerfilTrilhoValue(getValue(selectSelectAmvPerfilTrilho));
    }

    public void preencheDadosGeraisMetricaPrincipal(AMV amv) {
        clickAndHighlight(selectAmvTipoBitola);
        clickAndHighlight(optionAmvTipoBitolaMetrica);
        sendKeysWithJavaScript(inputAmvEquipamento, amv.getEquipamentoDadosGerais());
        clickAndHighlight(buttonButtonSearchSupervisor);
        selecionarResponsavel(amv.getSupervisorDadosGerais());
        amv.setSupervisorDadosGerais(getValue(inputAmvSupervisor));
        sendKeys(inputAmvPatio, amv.getPatioDadosGerais());
    }

    public void preencheDadosGeraisMistaPrincipal(AMV amv) {
        clickAndHighlight(selectAmvTipoBitola);
        clickAndHighlight(optionAmvTipoBitolaMista);
        sendKeysWithJavaScript(inputAmvEquipamento, amv.getEquipamentoDadosGerais());
        clickAndHighlight(buttonButtonSearchSupervisor);
        selecionarResponsavel(amv.getSupervisorDadosGerais());
        amv.setSupervisorDadosGerais(getValue(inputAmvSupervisor));
        sendKeys(inputAmvPatio, amv.getPatioDadosGerais());
    }

    public void preencheCotasDeSalvaguardaMetricaPrincipal(AMV amv) {
        sendKeys(inputInputAmvBitolapontaagulhametricaprincipal, amv.getBitolaPontaAgulhaCota1PrincipalMetrica());
        sendKeys(inputInputAmvLivrepassagempontaagulhametricaprincipal, amv.getBitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica());
        sendKeys(inputInputAmvProtecaopontajacaremetricaprincipal, amv.getBitolaProtecaoPontaJacareCota3Principal());
    }

    public void preencheCotasDeSalvaguardaMetricaDesviada(AMV amv) {
        sendKeys(inputInputAmvBitolapontaagulhametricadesviada, amv.getBitolaPontaAgulhaCota1DesviadaMetrica());
        sendKeys(inputInputAmvLivrepassagempontaagulhametricadesviada, amv.getBitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica());
        sendKeys(inputInputAmvProtecaopontajacaremetricadesviada, amv.getBitolaProtecaoPontaJacareCota3Desviada());
    }

    public void preencheCotasDeSalvaguardaMistaPrincipal(AMV amv) {
        //Métrica
        sendKeys(inputInputAmvBitolapontaagulhametricaprincipal, amv.getBitolaPontaAgulhaCota1PrincipalMetrica());
        sendKeys(inputInputAmvLivrepassagempontaagulhametricaprincipal, amv.getBitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica());
        sendKeys(inputInputAmvProtecaopontajacareanteriormetricaprincipal, amv.getPontaDoJacareAnteriorMetricaPrincipal());
        sendKeys(inputInputAmvProtecaopontajacareposteriormetricaprincipal, amv.getPontaDoJacarePosteriorMetricaPrincipal());
        sendKeys(inputInputAmvBitolabicoposteriorjacareduplometricaprincipal, amv.getBicoPosteriorDoJacareDuploMetricaPrincipal());
        //Larga
        sendKeys(inputInputAmvBitolaPontaAgulhaLargaPrincipal, amv.getBitolaPontaAgulhaCota1PrincipalLarga());
        sendKeys(inputInputAmvLivrepassagempontaagulhalargaprincipal, amv.getBitolaLivrePassagemPontaAgulhaCota2PrincipalLarga());
        sendKeys(inputInputAmvProtecaopontajacareanteriorlargaprincipal, amv.getPontaDoJacareAnteriorLargaPrincipal());
        sendKeys(inputInputAmvProtecaopontajacareposteriorlargaprincipal, amv.getPontaDoJacarePosteriorLargaPrincipal());
        sendKeys(inputInputAmvBitolabicoposteriorjacareduplolargaprincipal, amv.getBicoPosteriorDoJacareDuploLargaPrincipal());
    }

    public void preencheCotasDeSalvaguardaMistaDesviada(AMV amv) {
        //Métrica
        sendKeys(inputInputAmvBitolapontaagulhametricadesviada, amv.getBitolaPontaAgulhaCota1DesviadaMetrica());
        sendKeys(inputInputAmvLivrepassagempontaagulhametricadesviada, amv.getBitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica());
        sendKeys(inputInputAmvProtecaopontajacareanteriormetricadesviada, amv.getPontaDoJacareAnteriorMetricaDesviada());
        sendKeys(inputInputAmvProtecaopontajacareposteriormetricadesviada, amv.getPontaDoJacarePosteriorMetricaDesviada());
        sendKeys(inputInputAmvBitolabicoposteriorjacareduplometricadesviada, amv.getBicoPosteriorDoJacareDuploMetricaDesviada());
        //Larga
        sendKeys(inputInputAmvBitolapontaagulhalargadesviada, amv.getBitolaPontaAgulhaCota1DesviadaLarga());
        sendKeys(inputInputAmvLivrepassagempontaagulhalargadesviada, amv.getBitolaLivrePassagemPontaAgulhaCota2DesviadaLarga());
        sendKeys(inputInputAmvProtecaopontajacareanteriorlargadesviada, amv.getPontaDoJacareAnteriorLargaDesviada());
        sendKeys(inputInputAmvProtecaopontajacareposteriorlargadesviada, amv.getPontaDoJacarePosteriorLargaDesviada());
        sendKeys(inputInputAmvBitolabicoposteriorjacareduplolargadesviada, amv.getBicoPosteriorDoJacareDuploLargaDesviada());
    }

    public void clicarBtnSalvarInformacoes() {
        clickWithAction(buttonBtnAmvSalvar);
    }

    public void validaDadosGeraisLargaPrincipal(AMV amv) {
        scrollToElement(separatorDadosGerais);
        waitTime(500);
        Assert.assertEquals(getText(selectAmvTipoBitola), "Larga");
        Assert.assertEquals(getValue(inputAmvEquipamento), amv.getEquipamentoDadosGerais());
        Assert.assertEquals(getValue(inputAmvPatio), amv.getPatioDadosGerais());
    }

    public void validaCotasDeSalvaguardaLargaPrincipal(AMV amv) {
        scrollToElement(separatorCotasDeSalvaGuardaPrincipal);
        waitTime(500);
        Assert.assertEquals(getValue(inputInputAmvBitolaPontaAgulhaLargaPrincipal), amv.getBitolaPontaAgulhaCota1PrincipalLarga());
        Assert.assertEquals(getValue(inputInputAmvLivrepassagempontaagulhalargaprincipal), amv.getBitolaLivrePassagemPontaAgulhaCota2PrincipalLarga());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacarelargaprincipal), amv.getBitolaProtecaoPontaJacareCota3Principal());
    }

    public void validaFuncionamentoPrincipal(AMV amv) {
        scrollToElement(separatorFuncionamentoPrincipal);
        waitTime(500);
        if (amv.isPressaoAgulhaEstaOkPrincipal()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvPressaoagulhaokprincipalSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvPressaoagulhaokprincipalNao));
        }
    }

    public void validaSegurancaPrincipal(AMV amv) {
        scrollToElement(separatorSegurancaPrincipal);
        waitTime(500);
        Assert.assertEquals(getValue(inputInputAmvSistemasegurancaamvprincipal), amv.getSistemaDeSegurancaDoAMVPrincipal());
        if (amv.isExisteExcessoDeMatoNoAMVPrincipal()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvExisteexcessomatoamvprincipalSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvExisteexcessomatoamvprincipalNao));
        }

        if (amv.isFrequenciaDoCheckListEstaOkPrincipal()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvFrequenciachecklistemergencialprincipalSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvFrequenciachecklistemergencialprincipalNao));
        }

        if (amv.isBandeirolaEstaPintadaESinalizadaPrincipal()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvBandeirolapintadasinalizadaprincipalSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvBandeirolapintadasinalizadaprincipalNao));
        }

        if (amv.isFrequenciaDeInspecaoProgramadaEstaOkPrincipal()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvFrequenciainspecaoprogramadaprincipalSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvFrequenciainspecaoprogramadaprincipalNao));
        }
    }

    public void validaBarraDeConjugacaoPrincipal(AMV amv) {
        scrollToElement(separatorBarraDeConjugacao);
        waitTime(500);
        Assert.assertEquals(getValue(inputInputAmvBarraconjugacaoqtd), amv.getQuantidadeBarraDeConjugacaoPrincipal());
        if (amv.isBarraConjugacaoSaoIsoladasPrincipal()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvBarraconjugacaoisoladasSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvBarraconjugacaoisoladasNao));
        }
    }

    public void validaAgulhasPrincipal(AMV amv) {
        scrollToElement(separatorAgulhas);
        waitTime(500);
        Assert.assertEquals(getValue(inputInputAmvPrincipalAgulhaesquerdadetalhe), amv.getDetalheAgulhaEsquerdaPrincipal());
        Assert.assertEquals(getValue(inputInputAmvPrincipalAgulhaesquerdacomprimento), amv.getComprimentoAgulhaEsquerdaPrincipal());
        Assert.assertEquals(getValue(inputInputAmvPrincipalAgulhadireitadetalhe), amv.getDetalheAgulhaDireitaPrincipal());
        Assert.assertEquals(getValue(inputInputAmvPrincipalAgulhadireitacomprimento), amv.getComprimentoAgulhaDireitaPrincipal());
    }

    public void validaCotasDeSalvaguardaLargaDesviada(AMV amv) {
        Assert.assertEquals(getValue(inputInputAmvBitolapontaagulhalargadesviada), amv.getBitolaPontaAgulhaCota1DesviadaLarga());
        Assert.assertEquals(getValue(inputInputAmvLivrepassagempontaagulhalargadesviada), amv.getBitolaLivrePassagemPontaAgulhaCota2DesviadaLarga());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacarelargadesviada), amv.getBitolaProtecaoPontaJacareCota3Desviada());
    }

    public void validaFuncionamentoDesviada(AMV amv) {
        if (amv.isPressaoAgulhaEstaOkDesviada()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvPressaoagulhaokdesviadaSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvPressaoagulhaokdesviadaNao));
        }

        Assert.assertEquals(getValue(inputInputAmvPercdormentesinserviveisdesviada), amv.getPorcentagemDosDormentesInserviveisDesviada());

        if (amv.isCondicaoDoLastroEstaOkDesviada()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvCondicaolastrookdesviadaSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvCondicaolastrookdesviadaNao));
        }

        if (amv.isFixacaoEstaOkDesviada()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvFixacaookdesviadaSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvFixacaookdesviadaNao));
        }
    }

    public void validaSegurancaDesviada(AMV amv) {
        Assert.assertEquals(getValue(inputInputAmvSistemasegurancaamvdesviada), amv.getSistemaDeSegurancaDoAMVDesviada());

        if (amv.isExisteExcessoDeMatoNoAMVDesviada()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvExisteexcessomatoamvdesviadaSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvExisteexcessomatoamvdesviadaNao));
        }

        if (amv.isFrequenciaDoCheckListEstaOkDesviada()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvFrequenciachecklistemergencialdesviadaSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvFrequenciachecklistemergencialdesviadaNao));
        }

        if (amv.isBandeirolaEstaPintadaESinalizadaDesviada()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvBandeirolapintadasinalizadadesviadaSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvBandeirolapintadasinalizadadesviadaNao));
        }

        if (amv.isFrequenciaDeInspecaoProgramadaEstaOkDesviada()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvFrequenciainspecaoprogramadadesviadaSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvFrequenciainspecaoprogramadadesviadaNao));
        }
    }

    public void validaTREncontroDesviada(AMV amv) {
        if (amv.isTrEncostroEsquerdo()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvTrencostoesquerdaReto));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvTrencostoesquerdaCurvo));
        }

        if (amv.isTrEncostroDireito()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvTrencostodireitoReto));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvTrencostodireitoCurvo));
        }
    }

    public void validaChanfroDesviada(AMV amv) {
        if (amv.isChanfroEsquerdo()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvChanfroesquerdaCom));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvChanfroesquerdaSem));
        }

        if (amv.isChanfroDireito()) {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvChanfrodireitoCom));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonRadioGroupAmvChanfrodireitoSem));
        }
    }

    public void validaAberturaAMVDesviada(AMV amv) {
        Assert.assertEquals(getValue(inputInputAberturaamv), amv.getHoraAberturaDoAMV());
    }

    public void validaTrilhoDesviada(AMV amv) {
        Assert.assertEquals(getValue(selectSelectAmvPerfilTrilho), amv.getTipoPerfilTrilhoValue());
    }

    public void validaDadosGeraisMetricaPrincipal(AMV amv) {
        scrollToElement(separatorDadosGerais);
        Assert.assertEquals(getText(selectAmvTipoBitola), "Métrica");
        Assert.assertEquals(getValue(inputAmvEquipamento), amv.getEquipamentoDadosGerais());
        Assert.assertEquals(getValue(inputAmvPatio), amv.getPatioDadosGerais());
    }

    public void validaCotasDeSalvaguardaMetricaPrincipal(AMV amv) {
        Assert.assertEquals(getValue(inputInputAmvBitolapontaagulhametricaprincipal), amv.getBitolaPontaAgulhaCota1PrincipalMetrica());
        Assert.assertEquals(getValue(inputInputAmvLivrepassagempontaagulhametricaprincipal), amv.getBitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacaremetricaprincipal), amv.getBitolaProtecaoPontaJacareCota3Principal());
    }

    public void validaCotasDeSalvaguardaMetricaDesviada(AMV amv) {
        Assert.assertEquals(getValue(inputInputAmvBitolapontaagulhametricadesviada), amv.getBitolaPontaAgulhaCota1DesviadaMetrica());
        Assert.assertEquals(getValue(inputInputAmvLivrepassagempontaagulhametricadesviada), amv.getBitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacaremetricadesviada), amv.getBitolaProtecaoPontaJacareCota3Desviada());
    }

    public void validaDadosGeraisMistaPrincipal(AMV amv) {
        scrollToElement(separatorDadosGerais);
        Assert.assertEquals(getText(selectAmvTipoBitola), "Mista");
        Assert.assertEquals(getValue(inputAmvEquipamento), amv.getEquipamentoDadosGerais());
        Assert.assertEquals(getValue(inputAmvPatio), amv.getPatioDadosGerais());
    }

    public void validaCotasDeSalvaguardaMistaPrincipal(AMV amv) {
        //Métrica
        Assert.assertEquals(getValue(inputInputAmvBitolapontaagulhametricaprincipal), amv.getBitolaPontaAgulhaCota1PrincipalMetrica());
        Assert.assertEquals(getValue(inputInputAmvLivrepassagempontaagulhametricaprincipal), amv.getBitolaLivrePassagemPontaAgulhaCota2PrincipalMetrica());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacareanteriormetricaprincipal), amv.getPontaDoJacareAnteriorMetricaPrincipal());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacareposteriormetricaprincipal), amv.getPontaDoJacarePosteriorMetricaPrincipal());
        Assert.assertEquals(getValue(inputInputAmvBitolabicoposteriorjacareduplometricaprincipal), amv.getBicoPosteriorDoJacareDuploMetricaPrincipal());
        //Larga
        Assert.assertEquals(getValue(inputInputAmvBitolaPontaAgulhaLargaPrincipal), amv.getBitolaPontaAgulhaCota1PrincipalLarga());
        Assert.assertEquals(getValue(inputInputAmvLivrepassagempontaagulhalargaprincipal), amv.getBitolaLivrePassagemPontaAgulhaCota2PrincipalLarga());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacareanteriorlargaprincipal), amv.getPontaDoJacareAnteriorLargaPrincipal());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacareposteriorlargaprincipal), amv.getPontaDoJacarePosteriorLargaPrincipal());
        Assert.assertEquals(getValue(inputInputAmvBitolabicoposteriorjacareduplolargaprincipal), amv.getBicoPosteriorDoJacareDuploLargaPrincipal());
    }

    public void validaCotasDeSalvaguardaMistaDesviada(AMV amv) {
        //Métrica
        Assert.assertEquals(getValue(inputInputAmvBitolapontaagulhametricadesviada), amv.getBitolaPontaAgulhaCota1DesviadaMetrica());
        Assert.assertEquals(getValue(inputInputAmvLivrepassagempontaagulhametricadesviada), amv.getBitolaLivrePassagemPontaAgulhaCota2DesviadaMetrica());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacareanteriormetricadesviada), amv.getPontaDoJacareAnteriorMetricaDesviada());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacareposteriormetricadesviada), amv.getPontaDoJacarePosteriorMetricaDesviada());
        Assert.assertEquals(getValue(inputInputAmvBitolabicoposteriorjacareduplometricadesviada), amv.getBicoPosteriorDoJacareDuploMetricaDesviada());
        //Larga
        Assert.assertEquals(getValue(inputInputAmvBitolapontaagulhalargadesviada), amv.getBitolaPontaAgulhaCota1DesviadaLarga());
        Assert.assertEquals(getValue(inputInputAmvLivrepassagempontaagulhalargadesviada), amv.getBitolaLivrePassagemPontaAgulhaCota2DesviadaLarga());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacareanteriorlargadesviada), amv.getPontaDoJacareAnteriorLargaDesviada());
        Assert.assertEquals(getValue(inputInputAmvProtecaopontajacareposteriorlargadesviada), amv.getPontaDoJacarePosteriorLargaDesviada());
        Assert.assertEquals(getValue(inputInputAmvBitolabicoposteriorjacareduplolargadesviada), amv.getBicoPosteriorDoJacareDuploLargaDesviada());
    }

    public void preencheDadosGeraisLargaPrincipalParcial(AMV amv) {
        scrollToElement(separatorDadosGerais);
        waitTime(500);
        clickAndHighlight(selectAmvTipoBitola);
        clickAndHighlight(optionAmvTipoBitolaLarga);
        sendKeysWithJavaScript(inputAmvEquipamento, amv.getEquipamentoDadosGerais());
        clickAndHighlight(buttonButtonSearchSupervisor);
        selecionarResponsavel(amv.getSupervisorDadosGerais());
        amv.setSupervisorDadosGerais(getValue(inputAmvSupervisor));
    }

    public void preencheDadosGeraisMetricaPrincipalParcial(AMV amv) {
        clickAndHighlight(selectAmvTipoBitola);
        clickAndHighlight(optionAmvTipoBitolaMetrica);
        sendKeysWithJavaScript(inputAmvEquipamento, amv.getEquipamentoDadosGerais());
        clickAndHighlight(buttonButtonSearchSupervisor);
        selecionarResponsavel(amv.getSupervisorDadosGerais());
        amv.setSupervisorDadosGerais(getValue(inputAmvSupervisor));
    }

    public void preencheDadosGeraisMistaPrincipalParcial(AMV amv) {
        clickAndHighlight(selectAmvTipoBitola);
        clickAndHighlight(optionAmvTipoBitolaMista);
        sendKeysWithJavaScript(inputAmvEquipamento, amv.getEquipamentoDadosGerais());
        clickAndHighlight(buttonButtonSearchSupervisor);
        selecionarResponsavel(amv.getSupervisorDadosGerais());
        amv.setSupervisorDadosGerais(getValue(inputAmvSupervisor));
    }
}
