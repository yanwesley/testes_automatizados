package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.SobreOLocal;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class SobreOLocalSindicanciaPage extends GeralPage {
    IntFunction<By> optionEstadoLocal = (index) -> By.id("estado-" + index);
    IntFunction<By> optionMunicipio = (index) -> By.id("municipioCodigo-" + index);
    IntFunction<By> optionCruzSantoAndre = (index) -> By.id("cruzDeSantoAndre-" + index);

    private By observacoesUnidadeAtendimento = By.id("unidadeAtendimentoObservacao");
    private By sobreOLocal = By.name("SOBRE O LOCAL");
    private By inputAtivosCompanhia = By.id("danosAtivosCompanhiaDescricao");
    private By buttonBackButton = By.id("back-button");
    private By aButtonExport = By.id("button-export");
    private By divClassificacaoTag = By.id("classificacao-tag");
    private By aBtnFichaTrem = By.id("btn-ficha-trem");
    private By aButtonFichaAcidente = By.id("button-ficha-acidente");
    private By buttonTabVia = By.id("tab-via");
    private By imgIconStatus = By.id("icon-status");
    private By buttonTabMaterialrodante = By.id("tab-materialrodante");
    private By buttonTabOperacao = By.id("tab-operacao");
    private By buttonTabSegurancapatrimonial = By.id("tab-segurancapatrimonial");
    private By buttonTabOutros = By.id("tab-outros");
    private By aSideMenuDescricao = By.id("side-menu-descricao");
    private By aSideMenuSobrelocal = By.id("side-menu-sobrelocal");
    private By aSideMenuSobreenvolvidos = By.id("side-menu-sobreenvolvidos");
    private By aSideMenuRegistroacidente = By.id("side-menu-registroacidente");
    private By aSideMenuCustos = By.id("side-menu-custos");
    private By aSideMenuAnexos = By.id("side-menu-anexos");
    private By formSobreLocalForm = By.cssSelector("sro-sobre-local > form");
    private By separatorSobreOLocal = By.name("SOBRE O LOCAL");
    private By selectEstadoLocal = By.id("estado");
    private By selectMunicipio = By.id("municipioCodigo");
    private By inputHorarioAcionamento = By.id("horarioAcionamento");
    private By inputHorarioAtendimento = By.id("horarioAtendimento");
    private By separatorUnidadesDeAtendimento = By.name("UNIDADES DE ATENDIMENTO");
    private By checkboxCheckboxAtendimentoPoliciaCivil = By.id("atendimentoPoliciaCivil");
    private By inputCheckboxAtendimentoPoliciaCivilInput = By.id("checkbox-atendimento-policia-civil-input");
    private By checkboxCheckboxAtendimentoPoliciaMilitar = By.id("atendimentoPoliciaMilitar");
    private By inputCheckboxAtendimentoPoliciaMilitarInput = By.id("checkbox-atendimento-policia-militar-input");
    private By checkboxCheckboxAtendimentoGuardaMunicipal = By.id("atendimentoGuardaMunicipal");
    private By inputCheckboxAtendimentoGuardaMunicipalInput = By.id("checkbox-atendimento-guarda-municipal-input");
    private By checkboxCheckboxAtendimentoCorpoDeBombeiros = By.id("atendimentoCorpoBombeiro");
    private By inputCheckboxAtendimentoCorpoDeBombeirosInput = By.id("checkbox-atendimento-corpo-de-bombeiros-input");
    private By checkboxCheckboxAtendimentoSamu = By.id("atendimentoSamu");
    private By inputCheckboxAtendimentoSamuInput = By.id("checkbox-atendimento-samu-input");
    private By checkboxCheckboxAtendimentoOutros = By.id("atendimentoOutro");
    private By inputCheckboxAtendimentoOutrosInput = By.id("checkbox-atendimento-outros-input");
    private By textareaUnidadeAtendimentoObservacao = By.id("unidadeAtendimentoObservacao");
    private By separatorAtivosDaCompanhia = By.name("ATIVOS DA COMPANHIA");
    private By radioGroupDanosAtivosCompanhia = By.id("danosAtivosCompanhia");
    private By radioButtonDanosAtivosCompanhiaSim = By.id("danosAtivosCompanhia-sim");
    private By inputDanosAtivosCompanhiaSimInput = By.id("danosAtivosCompanhia-sim-input");
    private By radioButtonDanosAtivosCompanhiaNao = By.id("danosAtivosCompanhia-nao");
    private By inputDanosAtivosCompanhiaNaoInput = By.id("danosAtivosCompanhia-nao-input");
    private By textareaDanosAtivosCompanhiaDescricao = By.id("danosAtivosCompanhiaDescricao");
    private By separatorSobreOPerimetro = By.name("SOBRE O PERÍMETRO");
    private By radioGroupPerimetro = By.id("perimetro");
    private By radioButtonPerimetroUrbano = By.id("perimetro-URBANO");
    private By radioButtonPerimetroRural = By.id("perimetro-RURAL");
    private By separatorLocalDoAcidente = By.name("LOCAL DO ACIDENTE");
    private By radioGroupAcidenteAconteceuEm = By.id("acidenteAconteceuEm");
    private By radioButtonAcidenteAconteceuEmPN = By.id("acidenteAconteceuEm-PN");
    private By inputAcidenteAconteceuEmPNInput = By.id("acidenteAconteceuEm-PN-input");
    private By radioButtonAcidenteAconteceuEmPNP = By.id("acidenteAconteceuEm-PNP");
    private By inputAcidenteAconteceuEmPNPInput = By.id("acidenteAconteceuEm-PNP-input");
    private By radioButtonAcidenteAconteceuEmTrecho = By.id("acidenteAconteceuEm-TRECHO");
    private By inputAcidenteAconteceuEmTrechoInput = By.id("acidenteAconteceuEm-Trecho-input");
    private By radioGroupPnPnpProximo = By.id("pnPnpProximo");
    private By radioButtonPnPnpProximoSim = By.id("pnpnpproximo-sim");
    private By inputPnPnpProximoSimInput = By.id("pnPnpProximo-sim-input");
    private By radioButtonPnPnpProximoNao = By.id("pnpnpproximo-nao");
    private By inputPnPnpProximoNaoInput = By.id("pnPnpProximo-nao-input");
    private By inputNomePnPnp = By.id("nomePnPnp");
    private By inputDistanciaPnPnp = By.id("distanciaPnPnp");
    private By sroTitleLabelImagemDaPassagemDeNivelProximaAoLocalDoOcorrido = By.name("Imagem da Passagem de nível próxima ao local do ocorrido");
    private By divImagemDaPassagemDeNivelProximaAoLocalDoOcorrido = By.name("Imagem da Passagem de nível próxima ao local do ocorrido");
    private By buttonButtonAddImage = By.id("button-add-image");
    private By radioGroupVisibilidadeMomentoAcidente = By.id("visibilidadeMomentoAcidente");
    private By radioButtonVisibilidadeMomentoAcidenteBoa = By.id("visibilidadeMomentoAcidente-BOA");
    private By inputVisibilidadeMomentoAcidenteBoaInput = By.id("visibilidadeMomentoAcidente-Boa-input");
    private By radioButtonVisibilidadeMomentoAcidenteRuim = By.id("visibilidadeMomentoAcidente-RUIM");
    private By inputVisibilidadeMomentoAcidenteRuimInput = By.id("visibilidadeMomentoAcidente-Ruim-input");
    private By textareaMotivoVisibilidadeRuim = By.id("motivoVisibilidadeRuim");
    private By buttonCancelarSobreLocal = By.id("cancelarSobreLocal");
    private By sroTitleLabelPassagemDeNivel = By.name("Passagem de Nível");
    private By divPassagemDeNivel = By.name("Passagem de Nível");
    private By selectClandestina = By.id("clandestina");
    private By inputClandestinaNome = By.id("clandestinaNome");
    private By selectPlacaPare = By.id("placaPare");
    private By selectPlacaPareOlheEscute = By.id("placaPareOlheEscute");
    private By selectRocada = By.id("rocada");
    private By selectCruzDeSantoAndre = By.id("cruzDeSantoAndre");
    private By selectSinalizacaoHorizontal = By.id("sinalizacaoHorizontal");
    private By selectPavimentada = By.id("pavimentada");
    private By radioGroupSinalizacaoAtiva = By.id("sinalizacaoAtiva");
    private By radioButtonSinalizacaoAtivaSim = By.id("sinalizacaoAtiva-sim");
    private By inputSinalizacaoAtivaSimInput = By.id("sinalizacaoAtiva-sim-input");
    private By radioButtonSinalizacaoAtivaNao = By.id("sinalizacaoAtiva-nao");
    private By inputSinalizacaoAtivaNaoInput = By.id("sinalizacaoAtiva-nao-input");
    private By textareaObservacao = By.id("observacao");
    private By clandestinaSim = By.id("option-clandestina-sim");
    private By clandestinaNao = By.id("option-clandestina-nao");
    private By placaPareSim = By.id("option-placaPare-sim");
    private By placaPareNao = By.id("option-placaPare-nao");
    private By placaPareEscuteSim = By.id("option-placaPareOlheEscute-sim");
    private By placaPareEscuteNao = By.id("option-placaPareOlheEscute-nao");
    private By rocadaBoa = By.id("rocada-0");
    private By rocadaRuim = By.id("rocada-1");
    private By sinalizacaoHorizontalSim = By.id("option-sinalizacaoHorizontal-sim");
    private By sinalizacaoHorizontalNao = By.id("option-sinalizacaoHorizontal-nao");
    private By pavimentadaSim = By.id("option-pavimentada-sim");
    private By pavimentadaNao = By.id("option-pavimentada-nao");
    private By estavaOperacionalSim = By.id("option-estavaOperacional-sim");
    private By estavaOperacionalNao = By.id("option-estavaOperacional-nao");
    private By possuiSinalizacaoSim = By.id("possuiSinalizacao-sim");
    private By possuiSinalizacaoNao = By.id("possuiSinalizacao-nao");
    private By estavaOperacional = By.id("estavaOperacional");
    private By btnImagem = By.id("button-add-image");
    private By textArearDetalhesImagem = By.id("dialog-anexo-imagem-descricao");
    private By btnSalvarModalImagem = By.id("dialog-button-anexo-imagem-salvar");

    private By buttonButtonSalvarRascunho = By.id("button-salvar-rascunho");
    private By buttonButtonSalvar = By.id("button-salvar");
    private By buttonButtonSalvarAvancar = By.id("button-salvar-avancar");
    private By buttonButtonCancelar = By.id("button-cancelar");

    private By buttonButtonVoltar = By.id("button-voltar");
    private By buttonButtonProximo = By.id("button-proximo");


    public void validoQueEstouSobreOLocal() {
        expectElementVisible(sobreOLocal);
    }

    public void preencherInformacoesSobreOLocal(SobreOLocal sobreOLocal) {
        sobreOLocal.setEstadoValue(
                selectOptionAndReturnValue(
                        selectEstadoLocal,
                        optionEstadoLocal,
                        sobreOLocal.getEstadoIndex()
                )
        );

        sobreOLocal.setMunicipioValue(
                selectOptionAndReturnValue(
                        selectMunicipio,
                        optionMunicipio,
                        sobreOLocal.getMunicipioIndex()
                )
        );

        sendKeys(inputHorarioAcionamento, sobreOLocal.getHorarioDoAcionamento());
        sendKeys(inputHorarioAtendimento, sobreOLocal.getHorarioDoAtendimento());
    }

    public void preencherUnidadeDeAtendimento(SobreOLocal sobreOLocal) {
        scrollToElement(separatorUnidadesDeAtendimento);

        if ((sobreOLocal.isPoliciaCivil() && !isCheckboxChecked(checkboxCheckboxAtendimentoPoliciaCivil))
                || (!sobreOLocal.isPoliciaCivil() && isCheckboxChecked(checkboxCheckboxAtendimentoPoliciaCivil))) {
            clickAndHighlight(checkboxCheckboxAtendimentoPoliciaCivil);
        }
        if ((sobreOLocal.isCorpoDeBombeiros() && !isCheckboxChecked(checkboxCheckboxAtendimentoCorpoDeBombeiros))
                || (!sobreOLocal.isCorpoDeBombeiros() && isCheckboxChecked(checkboxCheckboxAtendimentoCorpoDeBombeiros))) {
            clickAndHighlight(checkboxCheckboxAtendimentoCorpoDeBombeiros);
        }
        if ((sobreOLocal.isPoliciaMilitar()) && !isCheckboxChecked(checkboxCheckboxAtendimentoPoliciaMilitar)
                || (!sobreOLocal.isPoliciaMilitar() && isCheckboxChecked(checkboxCheckboxAtendimentoPoliciaMilitar))) {
            clickAndHighlight(checkboxCheckboxAtendimentoPoliciaMilitar);
        }
        if ((sobreOLocal.isSamu()) && !isCheckboxChecked(checkboxCheckboxAtendimentoSamu)
                || (!sobreOLocal.isSamu() && isCheckboxChecked(checkboxCheckboxAtendimentoSamu))) {
            clickAndHighlight(checkboxCheckboxAtendimentoSamu);
        }
        if ((sobreOLocal.isGuardaMunicipal()) && !isCheckboxChecked(checkboxCheckboxAtendimentoGuardaMunicipal)
                || (!sobreOLocal.isGuardaMunicipal() && isCheckboxChecked(checkboxCheckboxAtendimentoGuardaMunicipal))) {
            clickAndHighlight(checkboxCheckboxAtendimentoGuardaMunicipal);
        }
        if ((sobreOLocal.isOutros()) && !isCheckboxChecked(checkboxCheckboxAtendimentoOutros)
                || (!sobreOLocal.isOutros() && isCheckboxChecked(checkboxCheckboxAtendimentoOutros))) {
            clickAndHighlight(checkboxCheckboxAtendimentoOutros);
        }

        sendKeysWithJavaScript(observacoesUnidadeAtendimento, sobreOLocal.getObservacoesUnidadesDeAtendimentoPresentesNoLocal());
    }

    public void preencherAtivosDaCompanhia(SobreOLocal sobreOLocal) {
        scrollToElement((separatorAtivosDaCompanhia));

        clickWithAction(radioButtonDanosAtivosCompanhiaSim);
        sendKeysWithJavaScript(inputAtivosCompanhia, sobreOLocal.getObservacoesAtivosCompanhia());

    }

    public void preencherTipoPerimetro(SobreOLocal sobreOLocal) {
        scrollToElement((separatorSobreOPerimetro));
        if (sobreOLocal.getTipoPerimetro().equals("Urbano")) {
            clickWithAction(radioButtonPerimetroUrbano);

        } else {
            clickWithAction(radioButtonPerimetroRural);
        }
    }

    public void preencherLocalAcidente(SobreOLocal sobreOLocal) {
        scrollToElement(separatorLocalDoAcidente);

        switch (sobreOLocal.getLocalAcidente()) {
            case "PN": {
                clickWithAction(radioButtonAcidenteAconteceuEmPN);

                clickAndHighlight(selectClandestina);

                if (sobreOLocal.isPassagemClandestina()) {
                    clickWithAction(clandestinaSim);
                } else {
                    clickWithAction(clandestinaNao);
                }

                sendKeys(inputClandestinaNome, sobreOLocal.clandestinaNomeCampo());

                clickAndHighlight(selectPlacaPare);
                if (sobreOLocal.isPlacaPare()) {
                    clickWithAction(placaPareSim);
                } else {
                    clickWithAction(placaPareNao);
                }

                clickAndHighlight(selectPlacaPareOlheEscute);
                if (sobreOLocal.isPlacaPareEscute()) {
                    clickWithAction(placaPareEscuteSim);
                } else {
                    clickWithAction(placaPareEscuteNao);
                }

                clickAndHighlight(selectRocada);
                if (sobreOLocal.isRocada()) {
                    clickWithAction(rocadaBoa);
                } else {
                    clickWithAction(rocadaRuim);
                }

                sobreOLocal.setCruzSantoAndreValue(
                        selectOptionAndReturnValue(
                                selectCruzDeSantoAndre,
                                optionCruzSantoAndre,
                                sobreOLocal.getCruzSantoAndreIndex()
                        ));

                clickAndHighlight(selectSinalizacaoHorizontal);
                if (sobreOLocal.isSinalizacaoHorizontal()) {
                    clickWithAction(sinalizacaoHorizontalSim);
                } else {
                    clickWithAction(sinalizacaoHorizontalNao);
                }

                clickAndHighlight(selectPavimentada);
                if (sobreOLocal.isPavimentada()) {
                    clickWithAction(pavimentadaSim);
                } else {
                    clickWithAction(pavimentadaNao);
                }

                if (sobreOLocal.isSinalizacaoAtiva()) {

                    clickWithAction(radioButtonSinalizacaoAtivaSim);

                    clickAndHighlight(estavaOperacional);
                    if (sobreOLocal.isEstavaOperacional()) {
                        clickWithAction(estavaOperacionalSim);
                    } else {
                        clickWithAction(estavaOperacionalNao);
                    }
                } else {
                    clickWithAction(radioButtonSinalizacaoAtivaNao);
                }

                sendKeysWithJavaScript(textareaObservacao, sobreOLocal.observacoesLocalAcidente());

                if (sobreOLocal.isVisibilidadeBoaOuRuim()) {
                    clickWithAction(radioButtonVisibilidadeMomentoAcidenteRuim);
                    sendKeysWithJavaScript(textareaMotivoVisibilidadeRuim, sobreOLocal.getMotivoVisibilidade());
                } else {
                    clickWithAction(radioButtonVisibilidadeMomentoAcidenteBoa);
                }

                break;

            }
            case "PNP": {
                clickWithAction(radioButtonAcidenteAconteceuEmPNP);

                clickAndHighlight(selectClandestina);

                if (sobreOLocal.isPassagemClandestina()) {
                    clickWithAction(clandestinaSim);
                } else {
                    clickWithAction(clandestinaNao);
                }

                sendKeys(inputClandestinaNome, sobreOLocal.clandestinaNomeCampo());

                if (sobreOLocal.isPossuiSinalizacao()) {
                    clickWithAction(possuiSinalizacaoSim);
                    clickAndHighlight(selectPlacaPare);
                    if (sobreOLocal.isPlacaPare()) {
                        clickWithAction(placaPareSim);
                    } else {
                        clickWithAction(placaPareNao);
                    }

                    clickAndHighlight(selectPlacaPareOlheEscute);
                    if (sobreOLocal.isPlacaPareEscute()) {
                        clickWithAction(placaPareEscuteSim);
                    } else {
                        clickWithAction(placaPareEscuteNao);
                    }

                    clickAndHighlight(selectRocada);
                    if (sobreOLocal.isRocada()) {
                        clickWithAction(rocadaBoa);
                    } else {
                        clickWithAction(rocadaRuim);
                    }

                    sobreOLocal.setCruzSantoAndreValue(
                            selectOptionAndReturnValue(
                                    selectCruzDeSantoAndre,
                                    optionCruzSantoAndre,
                                    sobreOLocal.getCruzSantoAndreIndex()
                            ));

                    clickAndHighlight(selectSinalizacaoHorizontal);
                    if (sobreOLocal.isSinalizacaoHorizontal()) {
                        clickWithAction(sinalizacaoHorizontalSim);
                    } else {
                        clickWithAction(sinalizacaoHorizontalNao);
                    }

                    clickAndHighlight(selectPavimentada);
                    if (sobreOLocal.isPavimentada()) {
                        clickWithAction(pavimentadaSim);
                    } else {
                        clickWithAction(pavimentadaNao);
                    }

                    if (sobreOLocal.isSinalizacaoAtiva()) {
                        clickWithAction(radioButtonSinalizacaoAtivaSim);
                        clickAndHighlight(estavaOperacional);
                        if (sobreOLocal.isEstavaOperacional()) {
                            clickWithAction(estavaOperacionalSim);
                        } else {
                            clickWithAction(estavaOperacionalNao);
                        }
                    } else {
                        clickWithAction(radioButtonSinalizacaoAtivaNao);
                    }
                } else {
                    clickWithAction(possuiSinalizacaoNao);
                }

                sendKeysWithJavaScript(textareaObservacao, sobreOLocal.observacoesLocalAcidente());
                if (sobreOLocal.isVisibilidadeBoaOuRuim()) {
                    clickWithAction(radioButtonVisibilidadeMomentoAcidenteBoa);
                } else {
                    clickWithAction(radioButtonVisibilidadeMomentoAcidenteRuim);
                    sendKeysWithJavaScript(textareaMotivoVisibilidadeRuim, sobreOLocal.getMotivoVisibilidade());
                }

                break;
            }
            case "Trecho": {
                clickWithAction(radioButtonAcidenteAconteceuEmTrecho);
                if (sobreOLocal.isPnOuPnp()) {
                    clickWithAction(radioButtonPnPnpProximoSim);
                    sendKeys(inputNomePnPnp, sobreOLocal.getNomeDaPnOuPnp());
                    sendKeys(inputDistanciaPnPnp, sobreOLocal.getDistanciaPnOuPNP());
                    clickAndHighlight(btnImagem);
                    uploadImagem();
                    sendKeysWithJavaScript(textArearDetalhesImagem, sobreOLocal.getDescricaoImagem());
                    clickAndHighlight(btnSalvarModalImagem);
                    validaMensagemSnackBar("Imagem anexada com sucesso");
                } else {
                    clickWithAction(radioButtonPnPnpProximoNao);
                }

                if (sobreOLocal.isVisibilidadeBoaOuRuim()) {
                    clickWithAction(radioButtonVisibilidadeMomentoAcidenteBoa);
                } else {
                    clickWithAction(radioButtonVisibilidadeMomentoAcidenteRuim);
                    waitTime(300);
                    sendKeysWithJavaScript(textareaMotivoVisibilidadeRuim, sobreOLocal.getMotivoVisibilidade());
                }
                break;
            }
        }
    }

    public void clicarEmSalvarSobreOLocal() {
        clickAndHighlight(buttonButtonSalvar);
    }

    public void validarInformacoesSobreOLocal(SobreOLocal sobreOLocal) {
        Assert.assertEquals(getText(selectEstadoLocal), sobreOLocal.getEstadoValue());
        Assert.assertEquals(getText(selectMunicipio), sobreOLocal.getMunicipioValue());
        Assert.assertEquals(getValue(inputHorarioAcionamento), sobreOLocal.getHorarioDoAcionamento());
        Assert.assertEquals(getValue(inputHorarioAtendimento), sobreOLocal.getHorarioDoAtendimento());
    }

    public void validarUnidadeDeAtendimento(SobreOLocal sobreOLocal) {
        scrollToElement(separatorUnidadesDeAtendimento);
        if (sobreOLocal.isPoliciaCivil()) {
            Assert.assertTrue(isCheckboxChecked(checkboxCheckboxAtendimentoPoliciaCivil), "Esperava esta checked");
        }
        if (sobreOLocal.isCorpoDeBombeiros()) {
            Assert.assertTrue(isCheckboxChecked(checkboxCheckboxAtendimentoCorpoDeBombeiros), "Esperava esta checked");
        }
        if (sobreOLocal.isPoliciaMilitar()) {
            Assert.assertTrue(isCheckboxChecked(checkboxCheckboxAtendimentoPoliciaMilitar), "Esperava esta checked");
        }
        if (sobreOLocal.isSamu()) {
            Assert.assertTrue(isCheckboxChecked(checkboxCheckboxAtendimentoSamu), "Esperava esta checked");
        }
        if (sobreOLocal.isGuardaMunicipal()) {
            Assert.assertTrue(isCheckboxChecked(checkboxCheckboxAtendimentoGuardaMunicipal), "Esperava esta checked");
        }
        if (sobreOLocal.isOutros()) {
            Assert.assertTrue(isCheckboxChecked(checkboxCheckboxAtendimentoOutros), "Esperava esta checked");
        }

        Assert.assertEquals(getValue(observacoesUnidadeAtendimento), sobreOLocal.getObservacoesUnidadesDeAtendimentoPresentesNoLocal());
    }

    public void validarAtivosDaCompanhia(SobreOLocal sobreOLocal) {
        scrollToElement((separatorAtivosDaCompanhia));

        Assert.assertTrue(isRadioChecked(radioButtonDanosAtivosCompanhiaSim));
        Assert.assertEquals(getValue(inputAtivosCompanhia), sobreOLocal.getObservacoesAtivosCompanhia());
    }

    public void validarTipoPerimetro(SobreOLocal sobreOLocal) {
        scrollToElement((separatorSobreOPerimetro));

        if (sobreOLocal.getTipoPerimetro().equals("Urbano")) {
            Assert.assertTrue(isRadioChecked(radioButtonPerimetroUrbano));

        } else {
            Assert.assertTrue(isRadioChecked(radioButtonPerimetroRural));
        }
    }

    public void validarLocalAcidente(SobreOLocal sobreOLocal) {
        scrollToElement(separatorLocalDoAcidente);

        switch (sobreOLocal.getLocalAcidente()) {
            case "PN": {
                Assert.assertTrue(isRadioChecked(radioButtonAcidenteAconteceuEmPN));

                if (sobreOLocal.isPassagemClandestina()) {
                    Assert.assertEquals(getText(selectClandestina), "Sim");
                } else {
                    Assert.assertEquals(getText(selectClandestina), "Não");
                }

                Assert.assertEquals(getValue(inputClandestinaNome), sobreOLocal.clandestinaNomeCampo());

                if (sobreOLocal.isPlacaPare()) {
                    Assert.assertEquals(getText(selectPlacaPare), "Sim");
                } else {
                    Assert.assertEquals(getText(selectPlacaPare), "Não");
                }

                if (sobreOLocal.isPlacaPareEscute()) {
                    Assert.assertEquals(getText(selectPlacaPareOlheEscute), "Sim");
                } else {
                    Assert.assertEquals(getText(selectPlacaPareOlheEscute), "Não");
                }

                if (sobreOLocal.isRocada()) {
                    Assert.assertEquals(getText(selectRocada), "Boa");
                } else {
                    Assert.assertEquals(getText(selectRocada), "Ruim");
                }

                Assert.assertEquals(getText(selectCruzDeSantoAndre), sobreOLocal.getCruzSantoAndreValue());

                if (sobreOLocal.isSinalizacaoHorizontal()) {
                    Assert.assertEquals(getText(selectSinalizacaoHorizontal), "Sim");
                } else {
                    Assert.assertEquals(getText(selectSinalizacaoHorizontal), "Não");
                }

                if (sobreOLocal.isPavimentada()) {
                    Assert.assertEquals(getText(selectPavimentada), "Sim");
                } else {
                    Assert.assertEquals(getText(selectPavimentada), "Não");
                }

                if (sobreOLocal.isSinalizacaoAtiva()) {

                    Assert.assertTrue(isRadioChecked(radioButtonSinalizacaoAtivaSim));

                    if (sobreOLocal.isEstavaOperacional()) {
                        Assert.assertEquals(getText(estavaOperacional), "Sim");
                    } else {
                        Assert.assertEquals(getText(estavaOperacional), "Não");
                    }
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonSinalizacaoAtivaNao));
                }

                Assert.assertEquals(getValue(textareaObservacao), sobreOLocal.observacoesLocalAcidente());

                if (sobreOLocal.isVisibilidadeBoaOuRuim()) {
                    Assert.assertTrue(isRadioChecked(radioButtonVisibilidadeMomentoAcidenteRuim), "Ruim");
                    Assert.assertEquals(getValue(textareaMotivoVisibilidadeRuim), sobreOLocal.getMotivoVisibilidade());
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonVisibilidadeMomentoAcidenteBoa), "Boa");
                }

                break;

            }
            case "PNP": {
                Assert.assertTrue(isRadioChecked(radioButtonAcidenteAconteceuEmPNP));

                if (sobreOLocal.isPassagemClandestina()) {
                    Assert.assertEquals(getText(selectClandestina), "Sim");
                } else {
                    Assert.assertEquals(getText(selectClandestina), "Não");
                }

                Assert.assertEquals(getValue(inputClandestinaNome), sobreOLocal.clandestinaNomeCampo());

                if (sobreOLocal.isPossuiSinalizacao()) {
                    Assert.assertTrue(isRadioChecked(possuiSinalizacaoSim), "Sim");
                    if (sobreOLocal.isPlacaPare()) {
                        Assert.assertEquals(getText(selectPlacaPare), "Sim");
                    } else {
                        Assert.assertEquals(getText(selectPlacaPare), "Não");
                    }

                    if (sobreOLocal.isPlacaPareEscute()) {
                        Assert.assertEquals(getText(selectPlacaPareOlheEscute), "Sim");
                    } else {
                        Assert.assertEquals(getText(selectPlacaPareOlheEscute), "Não");
                    }

                    if (sobreOLocal.isRocada()) {
                        Assert.assertEquals(getText(selectRocada), "Boa");
                    } else {
                        Assert.assertEquals(getText(selectRocada), "Ruim");
                    }

                    Assert.assertEquals(getText(selectCruzDeSantoAndre), sobreOLocal.getCruzSantoAndreValue());

                    if (sobreOLocal.isSinalizacaoHorizontal()) {
                        Assert.assertEquals(getText(selectSinalizacaoHorizontal), "Sim");
                    } else {
                        Assert.assertEquals(getText(selectSinalizacaoHorizontal), "Não");
                    }

                    if (sobreOLocal.isPavimentada()) {
                        Assert.assertEquals(getText(selectPavimentada), "Sim");
                    } else {
                        Assert.assertEquals(getText(selectPavimentada), "Não");
                    }

                    if (sobreOLocal.isSinalizacaoAtiva()) {
                        Assert.assertTrue(isRadioChecked(radioButtonSinalizacaoAtivaSim));
                        if (sobreOLocal.isEstavaOperacional()) {
                            Assert.assertEquals(getText(estavaOperacional), "Sim");
                        } else {
                            Assert.assertEquals(getText(estavaOperacional), "Não");
                        }
                    } else {
                        Assert.assertTrue(isRadioChecked(radioButtonSinalizacaoAtivaNao), "Não");
                    }
                } else {
                    Assert.assertTrue(isRadioChecked(possuiSinalizacaoNao), "Não");
                }

                Assert.assertEquals(getValue(textareaObservacao), sobreOLocal.observacoesLocalAcidente());
                if (sobreOLocal.isVisibilidadeBoaOuRuim()) {
                    Assert.assertTrue(isRadioChecked(radioButtonVisibilidadeMomentoAcidenteBoa), "Boa");
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonVisibilidadeMomentoAcidenteRuim), "Ruim");
                    Assert.assertEquals(getValue(textareaMotivoVisibilidadeRuim), sobreOLocal.getMotivoVisibilidade());
                }

                break;
            }
            case "Trecho": {
                Assert.assertTrue(isRadioChecked(radioButtonAcidenteAconteceuEmTrecho));
                if (sobreOLocal.isPnOuPnp()) {
                    Assert.assertTrue(isRadioChecked(radioButtonPnPnpProximoSim), "Sim");
                    Assert.assertEquals(getValue(inputNomePnPnp), sobreOLocal.getNomeDaPnOuPnp());
                    Assert.assertEquals(getValue(inputDistanciaPnPnp), sobreOLocal.getDistanciaPnOuPNP());
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonPnPnpProximoNao), "Não");
                }

                if (sobreOLocal.isVisibilidadeBoaOuRuim()) {
                    Assert.assertTrue(isRadioChecked(radioButtonVisibilidadeMomentoAcidenteBoa), "Boa");
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonVisibilidadeMomentoAcidenteRuim), "Não");
                    Assert.assertEquals(getValue(textareaMotivoVisibilidadeRuim), sobreOLocal.getMotivoVisibilidade());
                }

                break;
            }
        }
    }

    public void validarBotoesDisabled() {
        expectElementNotVisible(buttonButtonSalvar);
        expectElementNotVisible(buttonButtonSalvarRascunho);
        expectElementNotVisible(buttonButtonSalvarAvancar);
        expectElementNotVisible(buttonButtonCancelar);
        expectElementVisible(buttonButtonProximo);
        expectElementVisible(buttonButtonVoltar);
    }

    public void validaFormReadOnly() {
        isReadonlyForm(formSobreLocalForm, "input");
        isReadonlyForm(formSobreLocalForm, "mat-select");
    }
}



