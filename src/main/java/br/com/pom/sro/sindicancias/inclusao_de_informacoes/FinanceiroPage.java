package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.Financeiro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

import static br.com.utils.ReporterUtils.addScreenshotToReport;

public class FinanceiroPage extends GeralPage {

    private By separatorInformacoes = By.name("INFORMAÇÕES");
    private By formFormNumeroOrdem = By.id("form-Numero-Ordem");
    private By inputNumeroOrdemServico = By.id("numeroOrdemServico");

    /*
    RCS
     */

    private By divTitleLabelLISTADERCS = By.id("title-label-LISTA DE RCS");
    private By thTableHeaderRcNumero = By.id("table-header-rc-numero");
    private IntFunction<By> tdTableCellRcNumero = (index) -> By.id("table-cell-rc-numero-" + index);
    private IntFunction<By> inputRcNumero = (index) -> By.id("rc-" + index + "-numero");
    private IntFunction<By> tdTableCellRcActions = (index) -> By.id("table-cell-rc-actions-" + index);
    private IntFunction<By> buttonTableBtnRcAccept = (index) -> By.id("table-btn-rc-accept-" + index);
    private IntFunction<By> buttonTableBtnRcEdit = (index) -> By.id("table-btn-rc-edit-" + index);
    private IntFunction<By> buttonTableBtnRcDelete = (index) -> By.id("table-btn-rc-delete-" + index);
    private By buttonAddRc = By.id("add-rc");

    /*
    Lista de pedidos
     */

    private By divTitleLabelLISTADEPEDIDOS = By.id("title-label-LISTA DE PEDIDOS");
    private By thTableHeaderPedidoNumero = By.id("table-header-pedido-numero");
    private IntFunction<By> tdTableCellPedidoNumero = (index) -> By.id("table-cell-pedido-numero-" + index);
    private IntFunction<By> inputPedidoNumero = (index) -> By.id("pedido-0-numero");
    private IntFunction<By> tdTableCellPedidoActions = (index) -> By.id("table-cell-pedido-actions-" + index);
    private IntFunction<By> buttonTableBtnPedidoAccept = (index) -> By.id("table-btn-pedido-accept-" + index);
    private IntFunction<By> buttonTableBtnPedidoDelete = (index) -> By.id("table-btn-pedido-delete-" + index);
    private IntFunction<By> buttonTableBtnPedidoEdit = (index) -> By.id("table-btn-pedido-edit-" + index);
    private By buttonAddPedido = By.id("add-pedido");

    private By buttonButtonSalvar = By.id("button-salvar");
    private By buttonButtonCancelar = By.id("button-cancelar");

    /*
    modal delete
     */

    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");

    public void preencherOrdemDeServico(Financeiro financeiro) {
        sendKeys(inputNumeroOrdemServico, financeiro.getNumeroDaOrdemDeServico());
    }

    public void adicionarPedido(Financeiro financeiro, int linha) {
        linha--;
        clickAndHighlight(buttonAddPedido);
        sendKeys(inputPedidoNumero.apply(linha), financeiro.getNumeroDePedidos());
        clickAndHighlight(buttonTableBtnPedidoAccept.apply(linha));
    }

    public void adicionarRC(Financeiro financeiro, int linha) {
        linha--;
        clickAndHighlight(buttonAddRc);
        sendKeys(inputRcNumero.apply(linha), financeiro.getNumeroDaRC());
        clickAndHighlight(buttonTableBtnRcAccept.apply(linha));
    }

    public void clicarBtnSalvarInformacoes() {
        clickAndHighlight(buttonButtonSalvar);
    }

    public void validarDados(Financeiro financeiro, int linha) {
        linha--;
        Assert.assertEquals(getValue(inputNumeroOrdemServico), financeiro.getNumeroDaOrdemDeServico());
        Assert.assertEquals(getValue(inputRcNumero.apply(linha)), financeiro.getNumeroDaRC());
        Assert.assertEquals(getValue(inputPedidoNumero.apply(linha)), financeiro.getNumeroDePedidos());
    }

    public void editarRC(Financeiro financeiroEditado, int linha) {
        linha--;
        clickAndHighlight(buttonTableBtnRcEdit.apply(linha));
        sendKeys(inputRcNumero.apply(linha), financeiroEditado.getNumeroDaRC());
        clickAndHighlight(buttonTableBtnRcAccept.apply(linha));
    }

    public void editarPedido(Financeiro financeiroEditado, int linha) {
        linha--;
        clickAndHighlight(buttonTableBtnPedidoEdit.apply(linha));
        sendKeys(inputPedidoNumero.apply(linha), financeiroEditado.getNumeroDePedidos());
        clickAndHighlight(buttonTableBtnPedidoAccept.apply(linha));
    }

    public void excluirRC(int linha) {
        linha--;
        clickAndHighlight(buttonTableBtnRcDelete.apply(linha));
        expectText(divDialogConfirmationMessage, "Deseja mesmo excluir essa Requisição de Compra?");
        addScreenshotToReport("Validei a modal de exclusão de RC");
        clickAndHighlight(buttonDialogButtonYes);

    }

    public void excluirPedido(int linha) {
        linha--;
        clickAndHighlight(buttonTableBtnPedidoDelete.apply(linha));
        expectText(divDialogConfirmationMessage, "Deseja mesmo excluir esse pedido?");
        addScreenshotToReport("Validei a modal de exclusão de Pedido");
        clickAndHighlight(buttonDialogButtonYes);
    }

    public void validarDadosExcluidos(int linha) {
        linha--;
        expectElementNotVisible(inputRcNumero.apply(linha));
        expectElementNotVisible(inputPedidoNumero.apply(linha));
    }
}