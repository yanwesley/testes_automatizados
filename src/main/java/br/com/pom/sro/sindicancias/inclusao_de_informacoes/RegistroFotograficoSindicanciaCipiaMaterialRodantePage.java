package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

import static br.com.utils.ReporterUtils.addScreenshotToReport;

public class RegistroFotograficoSindicanciaCipiaMaterialRodantePage extends GeralPage {


    IntFunction<By> buttonButtonBoxAddImage = (index) -> By.id("button-box-add-image-" + index);
    IntFunction<By> buttonButtonBoxExcluir = (index) -> By.id("button-box-excluir-" + index);
    IntFunction<By> buttonButtonBoxExcluirOutros = (index) -> By.id("button-box-excluir-outros-" + index);
    IntFunction<By> imgImgRegistroFotografico = (index) -> By.id("img-registro-fotografico-" + index);
    IntFunction<By> imgImgRegistroFotograficoOutros = (index) -> By.id("img-registro-fotografico-outros-" + index);
    private By separatorRegistroFotográfico = By.name("REGISTRO FOTOGRÁFICO");
    private By separatorInformacoesDoPrimeiroTruqueADescarrilar = By.name("Informações do primeiro truque a descarrilar");
    private By buttonButtonAddImagemParte1 = By.id("button-add-imagem-parte-1");
    private By buttonButtonSavedInfo = By.id("button-saved-info");
    private By buttonSalvarAvancarRegistroFotografico = By.id("salvarAvancarRegistroFotografico");
    private By btnSalvarModal = By.id("dialog-button-yes");
    private By btnCancelarModal = By.id("dialog-button-no");
    private By inputTituloImagem = By.id("input-titulo-imagem");
    private By dialogContent = By.id("dialog-confirmation-message");

    public void validarItemsDaTela() {
        expectElementVisible(separatorRegistroFotográfico);
        expectElementVisible(separatorInformacoesDoPrimeiroTruqueADescarrilar);
        for (int i = 0; i < 9; i++) {
            expectElementVisible(buttonButtonBoxAddImage.apply(i));
        }
        expectElementVisible(buttonButtonAddImagemParte1);
        expectElementVisible(buttonButtonSavedInfo);
        expectElementVisible(buttonSalvarAvancarRegistroFotografico);
    }

    public void adicionarImagensComTituloDefinido() {
        for (int i = 0; i < 9; i++) {
            scrollToElement(buttonButtonBoxAddImage.apply(i));
            clickAndHighlight(buttonButtonBoxAddImage.apply(i));
            uploadImagem();
            clickAndHighlight(btnSalvarModal);
            validaMensagemSnackBar("Registro fotográfico salvo com sucesso");
            addScreenshotToReport("Imagem adicionada");
        }
    }

    public void validarImagensComTituloDefinido() {
        for (int i = 0; i < 9; i++) {
            scrollToElement(imgImgRegistroFotografico.apply(i));
            Assert.assertTrue(getAttribute(imgImgRegistroFotografico.apply(i), "src").contains("download"));
            addScreenshotToReport("Validado imagem adicionada");
        }
    }

    public void excluirImagensComTituloDefinido() {
        for (int i = 0; i < 9; i++) {
            scrollToElement(imgImgRegistroFotografico.apply(i));
            clickAndHighlight(buttonButtonBoxExcluir.apply(i));
            expectText(dialogContent, "Deseja mesmo excluir o registro fotográfico?");
            addScreenshotToReport("Validado modal de exclusão");
            clickAndHighlight(btnSalvarModal);
            validaMensagemSnackBar("Registro fotográfico excluído com sucesso");
        }
    }

    public void validaExclusaoImagensComTituloDefinido() {
        for (int i = 0; i < 9; i++) {
            expectElementNotVisible(imgImgRegistroFotografico.apply(i));
            addScreenshotToReport("Validado Exclusão");
        }
    }

    public void inserirImagemAdicional(int quantidade) {
        for (int i = 0; i < quantidade; i++) {
            scrollToElement(buttonButtonAddImagemParte1);
            clickAndHighlight(buttonButtonAddImagemParte1);
            sendKeys(inputTituloImagem, "Teste " + i);
            uploadImagem();
            clickAndHighlight(btnSalvarModal);
            validaMensagemSnackBar("Registro fotográfico salvo com sucesso");
            addScreenshotToReport("Imagem adicionada");
        }
    }

    public void validarImagemAdicional(int quantidade) {
        for (int i = 0; i < quantidade; i++) {
            scrollToElement(imgImgRegistroFotograficoOutros.apply(i));
            Assert.assertTrue(getAttribute(imgImgRegistroFotograficoOutros.apply(i), "src").contains("download"));
            addScreenshotToReport("Validado imagem adicionada");
        }
    }
}


