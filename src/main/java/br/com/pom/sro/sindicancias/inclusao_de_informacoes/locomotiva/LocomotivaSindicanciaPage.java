package br.com.pom.sro.sindicancias.inclusao_de_informacoes.locomotiva;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

import java.util.function.IntFunction;

public class LocomotivaSindicanciaPage extends GeralPage {

    IntFunction<By> radioButtonOptionMatRodanteLocomotivas = (index) -> By.id("option-mat-rodante-locomotivas-" + index);
    private By aLinkOpenModalJustificativa = By.id("link-open-modal-justificativa");
    private By separatorLocomotiva = By.name("Locomotiva");
    private By radioGroupRadioGroupMatRodanteLocomotivas = By.id("radio-group-mat-rodante-locomotivas");

    private By buttonComecarZeroInformacoesLocomotiva = By.id("comecar-zero-informacoes-locomotiva");
    private By formFormLocomotivaMaterialRodante = By.id("formLocomotivaMaterialRodante");
    private By inputInputMatRodanteLocomotivaLocal0 = By.id("input-mat-rodante-locomotiva-local-0");
    private By inputInputMatRodanteLocomotivaResponsavelNome0 = By.id("input-mat-rodante-locomotiva-responsavelNome-0");
    private By buttonButtonSearchMatRodanteLocomotivaResponsavelNome0 = By.id("button-search-mat-rodante-locomotiva-responsavelNome-0");
    private By inputInputMatRodanteLocomotivaData0 = By.id("input-mat-rodante-locomotiva-data-0");
    private By separatorFichaDeMedicaoPosAcidente = By.name("Ficha de medição - pós acidente");
    private By buttonBtnModalFichaPosAcidente = By.id("btn-modal-ficha-pos-acidente");
    private By separatorFichaDeInspecaoPneumatica = By.name("FICHA DE INSPEÇÃO - PNEUMÁTICA");
    private By buttonBtnModalFichaPneumatica = By.id("btn-modal-ficha-pneumatica");
    private By separatorFichaDeInspecaoEletrica = By.name("FICHA DE INSPEÇÃO - ELÉTRICA");
    private By buttonBtnModalFichaEletrica = By.id("btn-modal-ficha-eletrica");
    private By separatorFichaDeInspecaoConsequenciaDoAcidente = By.name("FICHA DE INSPEÇÃO - CONSEQUÊNCIA DO ACIDENTE");
    private By buttonBtnModalFichaConsequencias = By.id("btn-modal-ficha-consequencias");
    private By separatorFichaDeMedicoesFinaisExamesPosAcidente = By.name("FICHA DE MEDIÇÕES FINAIS - EXAMES PÓS ACIDENTE");
    private By buttonBtnSalvarInformacoesLocomotivas = By.id("btn-salvar-informacoes-locomotivas");
    private By buttonBtnSalvarAvancarInformacoesLocomotivas = By.id("btn-salvar-avancar-informacoes-locomotivas");
    private By buttonBtnCancelarInformacoesLocomotivas = By.id("btn-cancelar-informacoes-locomotivas");
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");


    public void validaQueEstaNaPaginaDeLocomotiva() {
        expectElementVisible(separatorLocomotiva);
    }

    public void validarBotaoSalvarDisabled() {
        clickWithAction(radioButtonOptionMatRodanteLocomotivas.apply(0));
        isDisabled(buttonBtnSalvarInformacoesLocomotivas);
    }
}
