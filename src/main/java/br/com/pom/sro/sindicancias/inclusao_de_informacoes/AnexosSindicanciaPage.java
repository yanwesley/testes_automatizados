package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class AnexosSindicanciaPage extends GeralPage {

    Function<Integer, By> cellBtnEditarArquivo = (Integer index) -> By.id("table-anexos-sindicancias-btn-editar-" + index);
    Function<Integer, By> cellBtnExcluirArquivo = (Integer index) -> By.id("table-anexos-sindicancias-btn-excluir-" + index);
    Function<Integer, By> cellComentarioAnexo = (Integer index) -> By.id("table-anexos-sindicancias-observacao-" + index);
    private By separatorAnexosDaArea = By.name("ANEXOS DA ÁREA");
    private By btnIncluirAnexo = By.id("anexos-sindicancia-btn");
    private By textAreaComentario = By.id("dialog-anexos-sindicancia-observacao-textarea");
    private By btnCancelar = By.id("dialog-anexos-sindicancia-cancelar-btn");
    private By btnSalvar = By.id("dialog-anexos-incidente-salvar-btn");
    private By btnSalvarSindicancia = By.id("dialog-anexos-sindicancia-salvar-btn");
    private By tableArquivosInseridos = By.cssSelector("sro-anexos-sindicancia > div > div > table > tbody");
    private By btnConfirmarExclusaoArquivo = By.id("dialog-button-yes");

    public void validaQueEstaNaPaginaDeAnexos() {
        expectElementVisible(separatorAnexosDaArea);
    }

    public void clicarBtnIncluirAnexos() {
        clickAndHighlight(btnIncluirAnexo);
    }

    public void insereArquivo(int qtdArquivo) {
        uploadArquivo(qtdArquivo);
    }

    public void insereComentario(String comentario) {
        sendKeysWithJavaScript(textAreaComentario, comentario);
    }

    public void clicarBtnSalvar() {
        clickAndHighlight(btnSalvarSindicancia);
    }

    public void validaArquivoFoiInserido(int qtdArquivosAdicionados) {
        waitTime(1000);
        Assert.assertEquals(qtdArquivosAdicionados, countChildElement(tableArquivosInseridos, "tr"));
    }

    public void validaMensagemSnackBar(String msg) {
        expectText(snackBarContent, msg);
        String textoSnackBar = getTextSnackBar();
        Assert.assertEquals(textoSnackBar, msg);
        expectElementNotVisible(snackBarContent);
    }

    public void excluirArquivos(int qtdArquivosExcluidos) {
        if (countChildElement(tableArquivosInseridos, "tr") >= qtdArquivosExcluidos) {
            for (int i = 0; i < qtdArquivosExcluidos; i++) {
                clickAndHighlight(cellBtnExcluirArquivo.apply(0));
                clickAndHighlight(btnConfirmarExclusaoArquivo);
                validaMensagemSnackBar("Anexo excluído com sucesso");
            }
        }
    }

    public void insereArquivosComComentario(int qtdArquivo, String comentario) {
        for (int i = 0; i < qtdArquivo; i++) {
            expectElementVisible(btnIncluirAnexo);
            clicarBtnIncluirAnexos();
            insereArquivo(1);
            insereComentario(comentario);
            clicarBtnSalvar();
            validaMensagemSnackBar("A informação foi registrada com sucesso!");
        }
    }

    public void clicarBtnEditarAnexo() {
        clickAndHighlight(cellBtnEditarArquivo.apply(0));
    }

    public void editaComentarioDoAnexo(String comentarioEditado) {
        sendKeysWithJavaScript(textAreaComentario, comentarioEditado);
    }

    public void validaComentarioEditado(String comentarioEditado) {
        Assert.assertEquals(getText(cellComentarioAnexo.apply(0)), comentarioEditado);
    }
}
