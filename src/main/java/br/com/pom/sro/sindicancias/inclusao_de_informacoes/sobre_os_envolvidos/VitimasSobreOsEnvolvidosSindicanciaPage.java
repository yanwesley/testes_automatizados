package br.com.pom.sro.sindicancias.inclusao_de_informacoes.sobre_os_envolvidos;

import br.com.api.model.sindicancia.sobre_os_envolvidos.VitimaEnvolvida;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;
import java.util.function.IntFunction;

public class VitimasSobreOsEnvolvidosSindicanciaPage extends GeralPage {
    Function<String, By> optionLocalDoObito = (index) -> By.id("obitoOcorreu-" + index);
    IntFunction<By> tdTableVitimasCellNomeCompleto = (index) -> By.id("table-vitimas-cell-nomeCompleto-" + index);
    IntFunction<By> tdTableVitimasCellRg = (index) -> By.id("table-vitimas-cell-rg-" + index);
    IntFunction<By> tdTableVitimasCellCpf = (index) -> By.id("table-vitimas-cell-cpf-" + index);
    IntFunction<By> tdTableVitimasCellObservacao = (index) -> By.id("table-vitimas-cell-observacao-" + index);
    IntFunction<By> tdTableVitimasCellActions = (index) -> By.id("table-vitimas-cell-actions-" + index);
    IntFunction<By> buttonButtonEditVitima = (index) -> By.id("button-edit-vitima-" + index);
    IntFunction<By> buttonButtonRemoveVitima = (index) -> By.id("button-remove-vitima-" + index);

    private By separatorVitimas = By.name("Vítimas");
    private By radioGroupHasVitimas = By.id("hasVitimas");
    private By radioButtonHasVitimasSim = By.id("hasVitimas-sim");
    private By inputHasVitimasSimInput = By.id("hasVitimas-sim-input");
    private By radioButtonHasVitimasNao = By.id("hasVitimas-nao");
    private By inputHasVitimasNaoInput = By.id("hasVitimas-nao-input");
    private By buttonButtonAddVictim = By.id("button-add-victim");
    private By thTableVitimasHeaderNomeCompleto = By.id("table-vitimas-header-nomeCompleto");
    private By thTableVitimasHeaderRg = By.id("table-vitimas-header-rg");
    private By thTableVitimasHeaderCpf = By.id("table-vitimas-header-cpf");
    private By thTableVitimasHeaderObservacao = By.id("table-vitimas-header-observacao");
    private By thTableVitimasHeaderActions = By.id("table-vitimas-header-actions");
    private By textareaObservacaoVitima = By.id("observacaoVitima");

    /*
    Modal add vitimas
     */
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By inputNomeCompleto = By.id("nomeCompleto");
    private By inputRg = By.id("rg");
    private By inputCpf = By.id("cpf");
    private By radioGroupFatal = By.id("fatal");
    private By radioButtonFatalSim = By.id("fatal-sim");
    private By inputFatalSimInput = By.id("fatal-sim-input");
    private By radioButtonFatalNao = By.id("fatal-nao");
    private By inputFatalNaoInput = By.id("fatal-nao-input");
    private By selectObitoOcorreu = By.id("obitoOcorreu");

    private By radioGroupHouveDesmembramento = By.id("houveDesmembramento");
    private By radioButtonHouveDesmembramentoSim = By.id("houveDesmembramento-sim");
    private By inputHouveDesmembramentoSimInput = By.id("houveDesmembramento-sim-input");
    private By radioButtonHouveDesmembramentoNao = By.id("houveDesmembramento-nao");
    private By inputHouveDesmembramentoNaoInput = By.id("houveDesmembramento-nao-input");

    private By buttonDialogButtonCancel = By.id("dialog-button-cancel");
    private By buttonDialogButtonSave = By.id("dialog-button-save");
    private By buttonDialogButtonSaveAddOther = By.id("dialog-button-save-add-other");


    /*
    Modal exclusao
     */

    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");

    public void clicarBtnSimModalExclusao() {
        clickAndHighlight(buttonDialogButtonYes);
    }


    public void validaExibicaoDaPagina() {
        expectElementVisible(separatorVitimas);
    }

    public void clicarBtnSimHouveVitimas() {
        clickWithAction(radioButtonHasVitimasSim);
    }

    public void clicarBtnAdicionarVitima() {
        clickAndHighlight(buttonButtonAddVictim);
    }

    public void validaModalDeAdicionarNovaVitima() {
        expectElementVisible(inputNomeCompleto);
        expectElementVisible(inputRg);
        expectElementVisible(inputCpf);
        expectElementVisible(inputCpf);
        expectElementVisible(radioGroupFatal);
        expectElementVisible(buttonDialogButtonCancel);
        expectElementVisible(buttonDialogButtonSave);
        expectElementVisible(buttonDialogButtonSaveAddOther);
    }

    public void preencherModalVitima(VitimaEnvolvida vitimaEnvolvida) {

        sendKeysWithJavaScript(inputNomeCompleto, vitimaEnvolvida.getNomeCompleto());
        sendKeysWithJavaScript(inputRg, vitimaEnvolvida.getRG());
        sendKeysWithJavaScript(inputCpf, vitimaEnvolvida.getCPF());
        if (vitimaEnvolvida.isFatal()) {
            clickWithAction(radioButtonFatalSim);
            vitimaEnvolvida.setLocalDoObitoValue(
                    selectOptionAndReturnValue(
                            selectObitoOcorreu,
                            optionLocalDoObito,
                            vitimaEnvolvida.getLocalDoObitoIndex()
                    )
            );

        } else {
            clickWithAction(radioButtonFatalNao);

            if (vitimaEnvolvida.isDesmembramento()) {
                clickWithAction(radioButtonHouveDesmembramentoSim);
            } else {
                clickWithAction(radioButtonHouveDesmembramentoNao);
            }
        }
    }

    public void clicarBtnSalvarModal() {
        clickAndHighlight(buttonDialogButtonSave);
    }

    public void validaVitimaAdicionada(VitimaEnvolvida vitimaEnvolvida, int linha) {
        linha--;
        Assert.assertEquals(getText(tdTableVitimasCellNomeCompleto.apply(linha)), vitimaEnvolvida.getNomeCompleto());
        Assert.assertEquals(getText(tdTableVitimasCellRg.apply(linha)), vitimaEnvolvida.getRG());
        Assert.assertEquals(getText(tdTableVitimasCellCpf.apply(linha)), vitimaEnvolvida.getCPF());

        if (vitimaEnvolvida.isFatal()) {
            Assert.assertEquals(getText(tdTableVitimasCellObservacao.apply(linha)), "Óbito - " + vitimaEnvolvida.getLocalDoObitoValue());
        } else {
            if (vitimaEnvolvida.isDesmembramento()) {
                Assert.assertEquals(getText(tdTableVitimasCellObservacao.apply(linha)), "Desmembramento");
            } else {
                Assert.assertEquals(getText(tdTableVitimasCellObservacao.apply(linha)), "--", "Observação deveria ser --");
            }
        }
    }

    public void validaQueTemVitima() {
        expectElementVisible(tdTableVitimasCellObservacao.apply(0));
    }

    public void clicarBotaoEditarVitima(int linha) {
        linha--;
        clickAndHighlight(buttonButtonEditVitima.apply(linha));
    }

    public void clicarBotaoExcluirVitima(int linha) {
        linha--;
        clickAndHighlight(buttonButtonRemoveVitima.apply(linha));
    }

    public void validaQueAVitimaFoiExcluida() {
        expectElementNotVisible(tdTableVitimasCellNomeCompleto.apply(0));
        expectElementNotVisible(tdTableVitimasCellRg.apply(0));
    }

    public void preencherObservacao(VitimaEnvolvida vitimaEnvolvida) {
        sendKeysWithJavaScript(textareaObservacaoVitima, vitimaEnvolvida.getObservacao());
    }

    public void validaObservacao(VitimaEnvolvida vitimaEnvolvida) {
        Assert.assertEquals(getValue(textareaObservacaoVitima), vitimaEnvolvida.getObservacao());
    }

    public void validaVitimaAdicionadaNoModoReadonly(VitimaEnvolvida vitimaEnvolvida, int linha) {
        linha--;
        Assert.assertEquals(getText(tdTableVitimasCellNomeCompleto.apply(linha)), vitimaEnvolvida.getNomeCompleto());
        Assert.assertEquals(getText(tdTableVitimasCellRg.apply(linha)), vitimaEnvolvida.getRG());
        Assert.assertEquals(getText(tdTableVitimasCellCpf.apply(linha)), vitimaEnvolvida.getCPF());

        if (vitimaEnvolvida.isFatal()) {
            Assert.assertEquals(getText(tdTableVitimasCellObservacao.apply(linha)), "Óbito - " + vitimaEnvolvida.getLocalDoObitoValue());
        } else {
            if (vitimaEnvolvida.isDesmembramento()) {
                Assert.assertEquals(getText(tdTableVitimasCellObservacao.apply(linha)), "Desmembramento");
            } else {
                Assert.assertEquals(getText(tdTableVitimasCellObservacao.apply(linha)), "--", "Observação deveria ser --");
            }
        }

        expectElementNotVisible(buttonButtonAddVictim);
    }
}
