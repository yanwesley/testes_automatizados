package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class TabsMenuSindicanciaPage extends GeralPage {

    private By tabOperacao = By.id("tab-operacao");
    private By tabOutros = By.id("tab-outros");
    private By tabMaterialRodante = By.id("tab-materialrodante");
    private By tabSegurancaPatrimonial = By.id("tab-segurancapatrimonial");
    private By tabAnaliseTecnica = By.id("tab-analisetecnica");
    private By menuFinanceiro = By.id("tab-financeiro");
    private By menuVia = By.id("tab-via");

    public void clicarTabOperacao() {
        expectLoading();
        expectElementClickable(tabOperacao);
        expectLoading();
        clickAndHighlight(tabOperacao);
    }

    public void clicarTabOutros() {
        expectElementClickable(tabOutros);
        clickAndHighlight(tabOutros);
    }

    public void clicarTabMaterialRodante() {
        expectLoading();
        expectElementClickable(tabMaterialRodante);
        clickAndHighlight(tabMaterialRodante);
    }

    public void clicarTabSegurancaPatrimonial() {
        expectLoading();
        expectElementClickable(tabSegurancaPatrimonial);
        clickAndHighlight(tabSegurancaPatrimonial);
    }

    public void clicarTabAnaliseTecnica() {
        expectLoading();
        expectElementClickable(tabAnaliseTecnica);
        clickAndHighlight(tabAnaliseTecnica);
    }

    public void validaExibicaoMenuFinanceiro() {
        expectElementVisible(menuFinanceiro);
    }

    public void validaQueOMenuFinanceiroNaoEstaAparecendo() {
        expectElementNotVisible(menuFinanceiro);
    }

    public void clicarTabVia() {
        expectLoading();
        expectElementClickable(menuVia);
        clickAndHighlight(menuVia);
    }
}