package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import static br.com.utils.ReporterUtils.addScreenshotToReport;

public class RegistroFotograficoSindicanciaCipiaViaPage extends GeralPage {

    private By separatorRegistroFotografico = By.name("REGISTRO FOTOGRÁFICO");
    private By separatorParteUm = By.name("parte 1");
    private By separatorParteDois = By.name("parte 2");
    private By btnUploadImgVisaoGeralDoAcidente = By.id("button-box-add-image-1");
    private By btnUploadImgCroquiDoLocal = By.id("button-box-add-image-2");
    private By btnUploadImgVisaoGeralDoPod = By.id("button-box-add-image-3");
    private By btnUploadImgCloseDoPod = By.id("button-box-add-image-4");
    private By btnUploadDetalheMarcaUm = By.id("button-box-add-image-5");
    private By btnUploadDetalheMarcaDois = By.id("button-box-add-image-6");
    private By btnAdicionarNovaImgParteUm = By.id("button-add-imagem-parte-1");
    private By btnAdicionarNovaImgParteDois = By.id("button-add-imagem-parte-2");

    private By divImgVisaoGeralDoAcidente = By.id("img-registro-fotografico-1");
    private By divImgCroquiDoLocal = By.id("img-registro-fotografico-2");
    private By divImgVisaoGeralDoPod = By.id("img-registro-fotografico-3");
    private By divImgCloseDoPod = By.id("img-registro-fotografico-4");
    private By divImgDetalheMarcaUm = By.id("img-registro-fotografico-5");
    private By divImgDetalheMarcaDois = By.id("img-registro-fotografico-6");
    private By divNovaImgParteUm = By.id("img-registro-fotografico-primeira-secao-0");
    private By divNovaImgParteDois = By.id("img-registro-fotografico-segunda-secao-0");

    private By btnExcluirVisaoGeralDoAcidente = By.id("button-box-excluir-1");
    private By btnExcluirCroquiDoLocal = By.id("button-box-excluir-2");
    private By btnExcluirVisaoGeralDoPod = By.id("button-box-excluir-3");
    private By btnExcluirCloseDoPod = By.id("button-box-excluir-4");
    private By btnExcluirDetalheMarcaUm = By.id("button-box-excluir-5");
    private By btnExcluirDetalheMarcaDois = By.id("button-box-excluir-6");
    private By btnExcluirNovaImgParteUm = By.id("button-box-excluir-primeira-secao-0");
    private By btnExcluirNovaImgParteDois = By.id("button-box-excluir-segunda-secao-0");

    private By btnAvancar = By.id("salvarAvancarRegistroFotografico");
    private By btnSalvarUploadImg = By.id("dialog-button-yes");
    private By btnConfirmarExclusao = By.id("dialog-button-yes");
    private By inputTituloDaImagem = By.id("input-titulo-imagem");

    public void validaQueEstaNaPaginaDeRegistroFotografico() {
        expectElementVisible(separatorRegistroFotografico);
    }

    public void validaTelaRegistroFotografico() {
        scrollToElement(separatorParteUm);
        expectElementVisible(btnUploadImgVisaoGeralDoAcidente);
        expectElementVisible(btnUploadImgCroquiDoLocal);
        expectElementVisible(btnAdicionarNovaImgParteUm);
        addScreenshotToReport("Primeira parte da tela de registro fotográfico");
        scrollToElement(separatorParteDois);
        expectElementVisible(btnUploadImgVisaoGeralDoPod);
        expectElementVisible(btnUploadImgCloseDoPod);
        addScreenshotToReport("Segunda parte da tela de registro fotográfico");
        scrollToElement(btnUploadDetalheMarcaUm);
        expectElementVisible(btnUploadDetalheMarcaUm);
        expectElementVisible(btnUploadDetalheMarcaDois);
        expectElementVisible(btnAdicionarNovaImgParteDois);
        addScreenshotToReport("Segunda parte da tela de registro fotográfico");
    }

    public void insereImgVisaoGeralDoAcidente() {
        scrollToElement(btnUploadImgVisaoGeralDoAcidente);
        clickWithJavaScript(btnUploadImgVisaoGeralDoAcidente);
        uploadImagem();
        clickAndHighlight(btnSalvarUploadImg);
        validaMensagemSnackBar("Registro fotográfico salvo com sucesso");
    }

    public void insereImgCroquiDoLocal() {
        scrollToElement(btnUploadImgCroquiDoLocal);
        clickWithJavaScript(btnUploadImgCroquiDoLocal);
        uploadImagem();
        clickAndHighlight(btnSalvarUploadImg);
        validaMensagemSnackBar("Registro fotográfico salvo com sucesso");
    }

    public void insereNovaImagemParteUm() {
        scrollToElement(btnAdicionarNovaImgParteUm);
        clickWithJavaScript(btnAdicionarNovaImgParteUm);
        sendKeys(inputTituloDaImagem, "Nova-imagem-parte-1");
        uploadImagem();
        clickAndHighlight(btnSalvarUploadImg);
        validaMensagemSnackBar("Registro fotográfico salvo com sucesso");
    }

    public void insereImgVisaoGeralDoPod() {
        scrollToElement(btnUploadImgVisaoGeralDoPod);
        clickWithJavaScript(btnUploadImgVisaoGeralDoPod);
        uploadImagem();
        clickAndHighlight(btnSalvarUploadImg);
        validaMensagemSnackBar("Registro fotográfico salvo com sucesso");
    }

    public void insereImgCloseDoPod() {
        scrollToElement(btnUploadImgCloseDoPod);
        clickWithJavaScript(btnUploadImgCloseDoPod);
        uploadImagem();
        clickAndHighlight(btnSalvarUploadImg);
        validaMensagemSnackBar("Registro fotográfico salvo com sucesso");
    }

    public void insereImgDetalheMarcaUm() {
        scrollToElement(btnUploadDetalheMarcaUm);
        clickWithJavaScript(btnUploadDetalheMarcaUm);
        uploadImagem();
        clickAndHighlight(btnSalvarUploadImg);
        validaMensagemSnackBar("Registro fotográfico salvo com sucesso");
    }

    public void insereImgDetalheMarcaDois() {
        scrollToElement(btnUploadDetalheMarcaDois);
        clickWithJavaScript(btnUploadDetalheMarcaDois);
        uploadImagem();
        clickAndHighlight(btnSalvarUploadImg);
        validaMensagemSnackBar("Registro fotográfico salvo com sucesso");
    }

    public void insereNovaImagemParteDois() {
        scrollToElement(btnAdicionarNovaImgParteDois);
        clickWithJavaScript(btnAdicionarNovaImgParteDois);
        sendKeys(inputTituloDaImagem, "Nova-imagem-parte-2");
        uploadImagem();
        clickAndHighlight(btnSalvarUploadImg);
        validaMensagemSnackBar("Registro fotográfico salvo com sucesso");
    }

    public void validaImagensInseridasNaTelaRegistroFotografico() {
        scrollToElement(divImgVisaoGeralDoAcidente);
        Assert.assertTrue(getAttribute(divImgVisaoGeralDoAcidente, "src").contains("download"));
        addScreenshotToReport("Imagem de Visão geral do Acidente adicionada");
        Assert.assertTrue(getAttribute(divImgCroquiDoLocal, "src").contains("download"));
        addScreenshotToReport("Imagem Croqui do Local adicionada");

        scrollToElement(divNovaImgParteUm);
        Assert.assertTrue(getAttribute(divNovaImgParteUm, "src").contains("download"));
        addScreenshotToReport("Nova Imagem Parte 1 adicionada");

        scrollToElement(divImgVisaoGeralDoPod);
        Assert.assertTrue(getAttribute(divImgVisaoGeralDoPod, "src").contains("download"));
        addScreenshotToReport("Imagem de Visão geral do Pod adicionada");
        Assert.assertTrue(getAttribute(divImgCloseDoPod, "src").contains("download"));
        addScreenshotToReport("Imagem Close do Pod adicionada");

        scrollToElement(divImgDetalheMarcaUm);
        Assert.assertTrue(getAttribute(divImgDetalheMarcaUm, "src").contains("download"));
        addScreenshotToReport("Imagem Detalhe Marca I adicionada");
        Assert.assertTrue(getAttribute(divImgDetalheMarcaDois, "src").contains("download"));
        addScreenshotToReport("Imagem Detalhe Marca II adicionada");

        scrollToElement(divNovaImgParteDois);
        Assert.assertTrue(getAttribute(divNovaImgParteDois, "src").contains("download"));
        addScreenshotToReport("Nova Imagem Parte 2 adicionada");
    }

    public void excluiImagensTelaRegistroFotografico() {
        clickAndHighlight(btnExcluirVisaoGeralDoAcidente);
        clickAndHighlight(btnConfirmarExclusao);
        validaMensagemSnackBar("Registro fotográfico excluído com sucesso");

        clickAndHighlight(btnExcluirCroquiDoLocal);
        clickAndHighlight(btnConfirmarExclusao);
        validaMensagemSnackBar("Registro fotográfico excluído com sucesso");

        clickAndHighlight(btnExcluirNovaImgParteUm);
        clickAndHighlight(btnConfirmarExclusao);
        validaMensagemSnackBar("Registro fotográfico excluído com sucesso");

        clickAndHighlight(btnExcluirVisaoGeralDoPod);
        clickAndHighlight(btnConfirmarExclusao);
        validaMensagemSnackBar("Registro fotográfico excluído com sucesso");

        clickAndHighlight(btnExcluirCloseDoPod);
        clickAndHighlight(btnConfirmarExclusao);
        validaMensagemSnackBar("Registro fotográfico excluído com sucesso");

        clickAndHighlight(btnExcluirDetalheMarcaUm);
        clickAndHighlight(btnConfirmarExclusao);
        validaMensagemSnackBar("Registro fotográfico excluído com sucesso");

        clickAndHighlight(btnExcluirDetalheMarcaDois);
        clickAndHighlight(btnConfirmarExclusao);
        validaMensagemSnackBar("Registro fotográfico excluído com sucesso");

        clickAndHighlight(btnExcluirNovaImgParteDois);
        clickAndHighlight(btnConfirmarExclusao);
        validaMensagemSnackBar("Registro fotográfico excluído com sucesso");
    }

    public void validaMensagemSnackBar(String msg) {
        expectText(snackBarContent, msg);
        String textoSnackBar = getTextSnackBar();
        Assert.assertEquals(textoSnackBar, msg);
        expectElementNotVisible(snackBarContent);
    }

    public void clicarBtnAvancar() {
        clickAndHighlight(btnAvancar);
    }
}
