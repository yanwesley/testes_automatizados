package br.com.pom.sro.sindicancias.inclusao_de_informacoes.investigacao;

import br.com.api.model.sindicancia.investigacao.DadosDoSupervisorResponsavel;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class DadosDoSupervisorResponsavelPage extends GeralPage {

    private By radioGroupResponsavelRumo = By.id("responsavelRumo");
    private By radioButtonResponsavelRumoSim = By.id("responsavelRumo-sim");
    private By inputResponsavelRumoSimInput = By.id("responsavelRumo-sim-input");
    private By radioButtonResponsavelRumoNao = By.id("responsavelRumo-nao");
    private By inputResponsavelRumoNaoInput = By.id("responsavelRumo-nao-input");
    private By sroTitleLabelValoresDeTestes = By.name("Valores de testes");

    private By inputNomeResponsavel = By.id("nomeResponsavel");
    private By buttonButtonSearchSupervisorName = By.id("button-search-supervisor");
    private By inputMatriculaResponsavel = By.id("matriculaResponsavel");
    private By buttonButtonSearchSupervisorCs = By.id("button-search-supervisor-cs");
    private By inputTesteAplicadoRelacionadoAcidente = By.id("testeAplicadoRelacionadoAcidente");
    private By inputTesteAplicadoRelacionadoAcidenteM1 = By.id("testeAplicadoRelacionadoAcidenteM1");
    private By inputTesteAplicadoRelacionadoAcidenteM2 = By.id("testeAplicadoRelacionadoAcidenteM2");
    private By inputTesteInsatisfatorioRelacionadoAcidente = By.id("testeInsatisfatorioRelacionadoAcidente");
    private By inputTesteInsatisfatorioRelacionadoAcidenteM1 = By.id("testeInsatisfatorioRelacionadoAcidenteM1");
    private By inputTesteInsatisfatorioRelacionadoAcidenteM2 = By.id("testeInsatisfatorioRelacionadoAcidenteM2");


    public void preencherDados(DadosDoSupervisorResponsavel dadosDoSupervisorResponsavel) {
        waitTime(200);
        if (dadosDoSupervisorResponsavel.isoSupervisorResponsavelEDaRumo()) {
            clickWithAction(radioButtonResponsavelRumoSim);
            clickAndHighlight(buttonButtonSearchSupervisorName);
            selecionarResponsavel(dadosDoSupervisorResponsavel.getNomeDoSupervisor());
            dadosDoSupervisorResponsavel.setNomeDoSupervisor(getValue(inputNomeResponsavel));
            dadosDoSupervisorResponsavel.setCS(getValue(inputMatriculaResponsavel));

        } else {
            clickWithAction(radioButtonResponsavelRumoNao);
            waitTime(500);
            sendKeys(inputNomeResponsavel, dadosDoSupervisorResponsavel.getNomeDoSupervisor());
            sendKeys(inputMatriculaResponsavel, dadosDoSupervisorResponsavel.getDocumento());
        }

        sendKeys(inputTesteAplicadoRelacionadoAcidente, dadosDoSupervisorResponsavel.getTestesAplicadosRelacionadosComOAmbienteMesAtual());
        sendKeys(inputTesteAplicadoRelacionadoAcidenteM1, dadosDoSupervisorResponsavel.getTestesAplicadosRelacionadosComOAmbienteMesM1());
        sendKeys(inputTesteAplicadoRelacionadoAcidenteM2, dadosDoSupervisorResponsavel.getTestesAplicadosRelacionadosComOAmbienteMesM2());

        sendKeys(inputTesteInsatisfatorioRelacionadoAcidente, dadosDoSupervisorResponsavel.getTestesInsatisfatoriosRelacionadosAoAcidenteMesAtual());
        sendKeys(inputTesteInsatisfatorioRelacionadoAcidenteM1, dadosDoSupervisorResponsavel.getTestesInsatisfatoriosRelacionadosAoAcidenteMesM1());
        sendKeys(inputTesteInsatisfatorioRelacionadoAcidenteM2, dadosDoSupervisorResponsavel.getTestesInsatisfatoriosRelacionadosAoAcidenteMesM2());
    }

    public void validarDados(DadosDoSupervisorResponsavel dadosDoSupervisorResponsavel) {
        scrollToElement(radioGroupResponsavelRumo);
        if (dadosDoSupervisorResponsavel.isoSupervisorResponsavelEDaRumo()) {
            Assert.assertTrue(isRadioChecked(radioButtonResponsavelRumoSim));
            Assert.assertEquals(getValue(inputMatriculaResponsavel), dadosDoSupervisorResponsavel.getCS());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonResponsavelRumoNao));
            Assert.assertEquals(getValue(inputMatriculaResponsavel), dadosDoSupervisorResponsavel.getDocumento());
        }

        Assert.assertEquals(getValue(inputNomeResponsavel), dadosDoSupervisorResponsavel.getNomeDoSupervisor());

        Assert.assertEquals(getValue(inputTesteAplicadoRelacionadoAcidente), dadosDoSupervisorResponsavel.getTestesAplicadosRelacionadosComOAmbienteMesAtual());
        Assert.assertEquals(getValue(inputTesteAplicadoRelacionadoAcidenteM1), dadosDoSupervisorResponsavel.getTestesAplicadosRelacionadosComOAmbienteMesM1());
        Assert.assertEquals(getValue(inputTesteAplicadoRelacionadoAcidenteM2), dadosDoSupervisorResponsavel.getTestesAplicadosRelacionadosComOAmbienteMesM2());

        Assert.assertEquals(getValue(inputTesteInsatisfatorioRelacionadoAcidente), dadosDoSupervisorResponsavel.getTestesInsatisfatoriosRelacionadosAoAcidenteMesAtual());
        Assert.assertEquals(getValue(inputTesteInsatisfatorioRelacionadoAcidenteM1), dadosDoSupervisorResponsavel.getTestesInsatisfatoriosRelacionadosAoAcidenteMesM1());
        Assert.assertEquals(getValue(inputTesteInsatisfatorioRelacionadoAcidenteM2), dadosDoSupervisorResponsavel.getTestesInsatisfatoriosRelacionadosAoAcidenteMesM2());
    }
}
