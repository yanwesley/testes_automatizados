package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class DescricaoSindicanciaPage extends GeralPage {
    private By buttonTabSegurancaPatrimonial = By.id("tab-segurancapatrimonial");
    private By aSideMenuDescricao = By.id("side-menu-descricao");
    private By separatorDescricaoDosFatos = By.name("DESCRIÇÃO DOS FATOS");
    private By textareaDescritivoAcidente = By.id("descritivoAcidente");
    private By buttonSalvarDescricao = By.id("salvarDescricao");
    private By buttonSalvarAvancarDescricao = By.id("salvarAvancarDescricao");
    private By buttonCancelarDescricao = By.id("cancelarDescricao");

    public void validoQueEstouNaPaginaDescricao() {
        expectElementVisible(buttonTabSegurancaPatrimonial);
        expectElementVisible(aSideMenuDescricao);
    }

    public void preenchoOCampoDescricao(String descricao) {
        sendKeysWithJavaScript(textareaDescritivoAcidente, descricao);
    }

    public void clicarbtnSalvarEAvancar() {
        clickAndHighlight(buttonSalvarAvancarDescricao);
    }

    public void validoDescricao(String descricao) {
        Assert.assertEquals(getValue(textareaDescritivoAcidente), descricao);
    }

    public void clicarBtnSalvar() {
        clickAndHighlight(buttonSalvarDescricao);
    }

    public void validoDescricaoReadonly(String descricao) {
        Assert.assertEquals(getValue(textareaDescritivoAcidente), descricao);
        expectElementNotVisible(buttonSalvarDescricao);
        expectElementNotVisible(buttonSalvarAvancarDescricao);
    }
}
