package br.com.pom.sro.sindicancias.inclusao_de_informacoes.sobre_os_envolvidos;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class SobreOsEnvolvidosSindicanciaPage extends GeralPage {
    private By buttonButtonSavedInfo = By.id("button-saved-info");
    private By buttonButtonAdvance = By.id("button-advance");
    private By buttonButtonCancel = By.id("button-cancel");

    public void clicarBtnSalvarInformacoes() {
        clickAndHighlight(buttonButtonSavedInfo);
    }
}
