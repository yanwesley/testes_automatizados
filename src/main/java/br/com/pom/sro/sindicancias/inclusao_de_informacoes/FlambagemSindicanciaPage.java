package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.Flambagem;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

import static br.com.utils.ReporterUtils.addScreenshotToReport;

public class FlambagemSindicanciaPage extends GeralPage {

    Function<Integer, By> optionLocalDaFlambagemUm = (Integer index) -> By.id("option-local-flambagem1-" + index);
    Function<Integer, By> optionLocalDaFlambagemDois = (Integer index) -> By.id("option-local-flambagem2-" + index);
    Function<Integer, By> optionLocalDaFlambagemTres = (Integer index) -> By.id("option-local-flambagem3-" + index);
    Function<Integer, By> optionSolucaoDefinitivaUm = (Integer index) -> By.id("option-solucao-definitiva1-" + index);
    Function<Integer, By> optionSolucaoDefinitivaDois = (Integer index) -> By.id("option-solucao-definitiva2-" + index);
    Function<Integer, By> optionSolucaoDefinitivaTres = (Integer index) -> By.id("option-solucao-definitiva3-" + index);

    private By separatorLocalDaFlambagem = By.name("LOCAL DA FLAMBAGEM");
    private By separatorCaracteristicasDoTrecho = By.name("CARACTERÍSTICAS DO TRECHO");
    private By separatorHistoricoDaFlambagem = By.name("HISTÓRICO DA FLAMBAGEM");
    private By separatorHistoricoDoAcidente = By.name("HISTÓRICO DO ACIDENTE");
    private By separatorSolucaoDefinitiva = By.name("SOLUÇÃO DEFINITIVA");

    private By selectLocalDaFlambagemUm = By.id("localFlambagem1");
    private By selectLocalDaFlambagemDois = By.id("localFlambagem2");
    private By selectLocalDaFlambagemTres = By.id("localFlambagem3");
    private By inputTemperaturaEmCampo = By.id("temperaturaEmCampo");
    private By inputComprimentoDaBarra = By.id("comprimentoBarra");
    private By inputCaminhamentoDeTR = By.id("caminhamentoTR");
    private By radioButtonHistFlambagemReincidenciaSim = By.id("flambagemReincidencia-sim");
    private By radioButtonHistFlambagemReincidenciaNao = By.id("flambagemReincidencia-nao");
    private By histFlambagemData = By.id("dataFlambagemReincidencia");
    private By histFlambagemHorario = By.id("horarioFlambagemReincidencia");
    private By radioButtonHistDoAcidenteReincidenciaSim = By.id("acidenteReincidencia-sim");
    private By radioButtonHistDoAcidenteReincidenciaNao = By.id("acidenteReincidencia-nao");
    private By histDoAcidenteData = By.id("dataAcidenteReincidencia");
    private By histDoAcidenteHorario = By.id("horarioAcidenteReincidencia");
    private By selectSolucaoDefinitivaUm = By.id("solucaoDefinitiva1");
    private By selectSolucaoDefinitivaDois = By.id("solucaoDefinitiva2");
    private By selectSolucaoDefinitivaTres = By.id("solucaoDeifinitiva3");
    private By btnSalvarInformacoes = By.id("salvarFlambagem");
    private By btnSalvarEAvancar = By.id("salvarAvancarFlambagem");


    public void validaExibicaoDaPageDeFlambagem() {
        expectElementVisible(separatorLocalDaFlambagem);
    }

    public void validaRegraSecaoLocalDaFlambagem() {
        expectElementNotVisible(selectLocalDaFlambagemDois);
        expectElementNotVisible(selectLocalDaFlambagemTres);
        addScreenshotToReport("Valida que os campos de 'Qual o local da flambagem 2?' e 'Qual o local da flambagem 3?' não estão visiveis");

        clickAndHighlight(selectLocalDaFlambagemUm);
        clickAndHighlight(optionLocalDaFlambagemUm.apply(1));
        expectElementNotVisible(selectLocalDaFlambagemTres);
        addScreenshotToReport("Valida que o campo de 'Qual o local da flambagem 3?' não está visivel");

        clickAndHighlight(selectLocalDaFlambagemDois);
        clickAndHighlight(optionLocalDaFlambagemDois.apply(1));
        expectElementVisible(selectLocalDaFlambagemUm);
        expectElementVisible(selectLocalDaFlambagemDois);
        expectElementVisible(selectLocalDaFlambagemTres);
        addScreenshotToReport("Valida que os 3 comboBox de local da Flambagem estão sendo apresentados em tela");
    }

    public void validaRegraSecaoHistoricoDaFlambagem() {
        scrollToElement(separatorHistoricoDaFlambagem);
        expectElementNotVisible(histFlambagemData);
        expectElementNotVisible(histFlambagemHorario);
        addScreenshotToReport("Valida que os campos de 'Data' e 'Horário' da seção de Histórico da Flambagem não estão visiveis");

        clickWithAction(radioButtonHistFlambagemReincidenciaSim);
        waitTime(200);
        expectElementVisible(histFlambagemData);
        expectElementVisible(histFlambagemHorario);
        addScreenshotToReport("Valida que os campos de 'Data' e 'Horário' da seção de Histórico da Flambagem ESTÃO visiveis");
    }

    public void validaRegraSecaoHistoricoDoAcidente() {
        scrollToElement(separatorHistoricoDoAcidente);
        expectElementNotVisible(histDoAcidenteData);
        expectElementNotVisible(histDoAcidenteHorario);
        addScreenshotToReport("Valida que os campos de 'Data' e 'Horário' da seção de Histórico da Acidente não estão visiveis");

        clickWithAction(radioButtonHistDoAcidenteReincidenciaSim);
        waitTime(200);
        expectElementVisible(histDoAcidenteData);
        expectElementVisible(histDoAcidenteHorario);
        addScreenshotToReport("Valida que os campos de 'Data' e 'Horário' da seção de Histórico da Acidente ESTÃO visiveis");
    }


    public void clicarBtnSalvarInformacoes() {
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void clicarBtnSalvarEAvancar() {
        clickAndHighlight(btnSalvarEAvancar);
    }

    public void preencheSecaoLocalDaFlambagem(Flambagem flambagem) {
        if (flambagem.isLocalDaFlambagemUmBoolean()) {
            clickAndHighlight(selectLocalDaFlambagemUm);
            clickAndHighlight(optionLocalDaFlambagemUm.apply(1));
            flambagem.setLocalDaFlambagemUm(getValue(selectLocalDaFlambagemUm));
            if (flambagem.isLocalDaFlambagemDoisBoolean()) {
                clickAndHighlight(selectLocalDaFlambagemDois);
                clickAndHighlight(optionLocalDaFlambagemDois.apply(1));
                flambagem.setLocalDaFlambagemUm(getValue(selectLocalDaFlambagemDois));

                clickAndHighlight(selectLocalDaFlambagemTres);
                clickAndHighlight(optionLocalDaFlambagemTres.apply(1));
                flambagem.setLocalDaFlambagemUm(getValue(selectLocalDaFlambagemTres));
            }
        }
    }

    public void preencheSecaoCaracteristicasDoTrecho(Flambagem flambagem) {
        scrollToElement(separatorCaracteristicasDoTrecho);
        sendKeys(inputTemperaturaEmCampo, flambagem.getTemperaturaEmCampo());


        sendKeys(inputComprimentoDaBarra, flambagem.getComprimentoDaBarra());
        sendKeys(inputCaminhamentoDeTR, flambagem.getCaminhamentoDaTR());
    }

    public void preencheSecaoHistoricoDaFlambagem(Flambagem flambagem) {
        if (flambagem.isHistoricoDaFlambagemReincidenciaBoolean()) {
            clickWithAction(radioButtonHistFlambagemReincidenciaSim);
            sendKeys(histFlambagemData, flambagem.getHistoricoDaFlambagemData());
            sendKeys(histFlambagemHorario, flambagem.getHistoricoDaFlambagemHorario());

        } else {
            clickWithAction(radioButtonHistFlambagemReincidenciaNao);
        }
    }

    public void preencheSecaoHistoricoDoAcidente(Flambagem flambagem) {
        if (flambagem.isHistoricoDoAcidenteReincidenciaBoolean()) {
            clickWithAction(radioButtonHistDoAcidenteReincidenciaSim);
            sendKeys(histDoAcidenteData, flambagem.getHistoricoDoAcidenteData());
            sendKeys(histDoAcidenteHorario, flambagem.getHistoricoDoAcidenteHorario());

        } else {
            clickWithAction(radioButtonHistDoAcidenteReincidenciaNao);
        }
    }

    public void preencheSecaoSolucaoDefinitiva(Flambagem flambagem) {
        scrollToElement(separatorSolucaoDefinitiva);
        clickAndHighlight(selectSolucaoDefinitivaUm);
        clickAndHighlight(optionSolucaoDefinitivaUm.apply(1));
        flambagem.setSolucaoDefinitivaUm(getValue(selectSolucaoDefinitivaUm));

        clickAndHighlight(selectSolucaoDefinitivaDois);
        clickAndHighlight(optionSolucaoDefinitivaDois.apply(1));
        flambagem.setSolucaoDefinitivaUm(getValue(selectSolucaoDefinitivaDois));

        clickAndHighlight(selectSolucaoDefinitivaTres);
        clickAndHighlight(optionSolucaoDefinitivaTres.apply(1));
        flambagem.setSolucaoDefinitivaUm(getValue(selectSolucaoDefinitivaTres));
    }

    public void validoSecaoLocalDaFlambagem(Flambagem flambagem) {
        scrollToElement(separatorLocalDaFlambagem);
        if (flambagem.isLocalDaFlambagemUmBoolean()) {
            Assert.assertEquals(getValue(selectLocalDaFlambagemUm), flambagem.getLocalDaFlambagemUm());
            if (flambagem.isLocalDaFlambagemDoisBoolean()) {
                Assert.assertEquals(getValue(selectLocalDaFlambagemDois), flambagem.getLocalDaFlambagemDois());
                Assert.assertEquals(getValue(selectLocalDaFlambagemTres), flambagem.getLocalDaFlambagemTres());
            }
        }
    }

    public void validoSecaoCaracteristicasDoTrecho(Flambagem flambagem) {
        scrollToElement(separatorCaracteristicasDoTrecho);
        Assert.assertEquals(getValue(inputTemperaturaEmCampo), String.valueOf(flambagem.getTemperaturaEmCampo()));
        Assert.assertEquals(getValue(inputComprimentoDaBarra), String.valueOf(flambagem.getComprimentoDaBarra()));
        Assert.assertEquals(getValue(inputCaminhamentoDeTR), String.valueOf(flambagem.getCaminhamentoDaTR()));
    }

    public void validoSecaoHistoricoDaFlambagem(Flambagem flambagem) {
        scrollToElement(separatorHistoricoDaFlambagem);
        if (flambagem.isHistoricoDaFlambagemReincidenciaBoolean()) {
            Assert.assertTrue(isRadioChecked(radioButtonHistFlambagemReincidenciaSim));

            Assert.assertEquals(getValue(histFlambagemData), String.valueOf(flambagem.getHistoricoDaFlambagemData()));
            Assert.assertEquals(getValue(histFlambagemHorario), String.valueOf(flambagem.getHistoricoDaFlambagemHorario()));

        } else {
            Assert.assertTrue(isRadioChecked(radioButtonHistFlambagemReincidenciaNao));
        }
    }

    public void validoSecaoHistoricoDoAcidente(Flambagem flambagem) {
        scrollToElement(separatorHistoricoDoAcidente);
        if (flambagem.isHistoricoDoAcidenteReincidenciaBoolean()) {
            Assert.assertTrue(isRadioChecked(radioButtonHistDoAcidenteReincidenciaSim));

            Assert.assertEquals(getValue(histDoAcidenteData), String.valueOf(flambagem.getHistoricoDoAcidenteData()));
            Assert.assertEquals(getValue(histDoAcidenteHorario), String.valueOf(flambagem.getHistoricoDoAcidenteHorario()));

        } else {
            Assert.assertTrue(isRadioChecked(radioButtonHistDoAcidenteReincidenciaNao));
        }
    }

    public void validoSecaoSolucaoDefinitiva(Flambagem flambagem) {
        scrollToElement(separatorSolucaoDefinitiva);
        Assert.assertEquals(getValue(selectSolucaoDefinitivaUm), flambagem.getSolucaoDefinitivaUm());
        Assert.assertEquals(getValue(selectSolucaoDefinitivaDois), flambagem.getSolucaoDefinitivaDois());
        Assert.assertEquals(getValue(selectSolucaoDefinitivaTres), flambagem.getSolucaoDefinitivaTres());
    }

    public void validaExibicaoDaPageAMV() {
        naoImplementado();
    }
}
