package br.com.pom.sro.sindicancias.inclusao_de_informacoes.sobre_os_envolvidos;

import br.com.api.model.sindicancia.sobre_os_envolvidos.VitimaEnvolvida;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class TestemunhasSobreOsEnvolvidosSindicanciaPage extends GeralPage {
    IntFunction<By> tdTableTestemunhasCellNomeCompleto = (index) -> By.id("table-testemunhas-cell-nomeCompleto-" + index);
    IntFunction<By> tdTableTestemunhasCellRg = (index) -> By.id("table-testemunhas-cell-rg-" + index);
    IntFunction<By> tdTableTestemunhasCellCpf = (index) -> By.id("table-testemunhas-cell-cpf-" + index);
    IntFunction<By> tdTableTestemunhasCellActions = (index) -> By.id("table-testemunhas-cell-actions-" + index);
    IntFunction<By> buttonButtonEditTestemunha = (index) -> By.id("button-edit-testemunha-" + index);
    IntFunction<By> buttonButtonRemoveTestemunha = (index) -> By.id("button-remove-testemunha-" + index);

    private By separatorTestemunhas = By.name("Testemunhas");
    private By radioGroupHasTestemunhas = By.id("hasTestemunhas");
    private By radioButtonHasTestemunhasSim = By.id("hasTestemunhas-sim");
    private By inputHasTestemunhasSimInput = By.id("hasTestemunhas-sim-input");
    private By radioButtonHasTestemunhasNao = By.id("hasTestemunhas-nao");
    private By inputHasTestemunhasNaoInput = By.id("hasTestemunhas-nao-input");
    private By buttonButtonAddTestemunha = By.id("button-add-testemunha");
    private By thTableTestemunhasHeaderNomeCompleto = By.id("table-testemunhas-header-nomeCompleto");
    private By thTableTestemunhasHeaderRg = By.id("table-testemunhas-header-rg");
    private By thTableTestemunhasHeaderCpf = By.id("table-testemunhas-header-cpf");
    private By thTableTestemunhasHeaderActions = By.id("table-testemunhas-header-actions");

    /*
    Modal add vitimas
     */
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By inputNomeCompleto = By.id("nomeCompleto");
    private By inputRg = By.id("rg");
    private By inputCpf = By.id("cpf");
    private By buttonDialogButtonCancel = By.id("dialog-button-cancel");
    private By buttonDialogButtonSave = By.id("dialog-button-save");
    private By buttonDialogButtonSaveAddOther = By.id("dialog-button-save-add-other");

    /*
    Modal exclusao
     */

    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");

    public void clicarBtnSimModalExclusao() {
        clickAndHighlight(buttonDialogButtonYes);
    }

    public void validaExibicaoDaPagina() {
        expectElementVisible(separatorTestemunhas);
    }

    public void clicarBtnSimHouveTestemunhas() {
        clickWithAction(radioButtonHasTestemunhasSim);
    }

    public void clicarBtnAdicionarTestemunha() {
        clickAndHighlight(buttonButtonAddTestemunha);
    }

    public void validaModalDeAdicionarNovaTestemunha() {
        expectElementVisible(inputNomeCompleto);
        expectElementVisible(inputRg);
        expectElementVisible(inputCpf);
        expectElementVisible(buttonDialogButtonCancel);
        expectElementVisible(buttonDialogButtonSave);
        expectElementVisible(buttonDialogButtonSaveAddOther);
    }

    public void preencherModalTestemunha(VitimaEnvolvida vitimaEnvolvida) {
        sendKeysWithJavaScript(inputNomeCompleto, vitimaEnvolvida.getNomeCompleto());
        sendKeysWithJavaScript(inputRg, vitimaEnvolvida.getRG());
        sendKeysWithJavaScript(inputCpf, vitimaEnvolvida.getCPF());

    }

    public void clicarBtnSalvarModal() {
        clickAndHighlight(buttonDialogButtonSave);
    }

    public void validaTestemunhaFoiAdicionada(VitimaEnvolvida vitimaEnvolvida, int linha) {
        scrollToElement(separatorTestemunhas);
        linha--;
        Assert.assertEquals(getText(tdTableTestemunhasCellNomeCompleto.apply(linha)), vitimaEnvolvida.getNomeCompleto());
        Assert.assertEquals(getText(tdTableTestemunhasCellRg.apply(linha)), vitimaEnvolvida.getRG());
        Assert.assertEquals(getText(tdTableTestemunhasCellCpf.apply(linha)), vitimaEnvolvida.getCPF());
    }

    public void validaQueTemTestemunha() {
        expectElementVisible(tdTableTestemunhasCellNomeCompleto.apply(0));
    }

    public void clicarBotaoEditarTestemunha(int linha) {
        linha--;
        clickAndHighlight(buttonButtonEditTestemunha.apply(linha));
    }

    public void clicarBotaoExcluirTestemunha(int linha) {
        linha--;
        clickAndHighlight(buttonButtonRemoveTestemunha.apply(linha));
    }

    public void validaQueATestemunhaFoiExcluida() {
        expectElementNotVisible(tdTableTestemunhasCellNomeCompleto.apply(0));
        expectElementNotVisible(tdTableTestemunhasCellRg.apply(0));
    }

    public void validaTestemunhaFoiAdicionadaNoModoReadOnly(VitimaEnvolvida vitimaEnvolvida, int linha) {
        scrollToElement(separatorTestemunhas);
        linha--;
        Assert.assertEquals(getText(tdTableTestemunhasCellNomeCompleto.apply(linha)), vitimaEnvolvida.getNomeCompleto());
        Assert.assertEquals(getText(tdTableTestemunhasCellRg.apply(linha)), vitimaEnvolvida.getRG());
        Assert.assertEquals(getText(tdTableTestemunhasCellCpf.apply(linha)), vitimaEnvolvida.getCPF());
        expectElementNotVisible(buttonButtonAddTestemunha);
    }
}
