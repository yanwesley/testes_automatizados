package br.com.pom.sro.sindicancias.inclusao_de_informacoes.investigacao;

import br.com.api.model.sindicancia.investigacao.Estacionamento;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class EstacionamentoSindicanciaPage extends GeralPage {

    private By radioGroupHouveDeslocamentoAtivos = By.id("houveDeslocamentoAtivos");
    private By radioButtonHouveDeslocamentoAtivosSim = By.id("houveDeslocamentoAtivos-sim");
    private By inputHouveDeslocamentoAtivosSimInput = By.id("houveDeslocamentoAtivos-sim-input");
    private By radioButtonHouveDeslocamentoAtivosNao = By.id("houveDeslocamentoAtivos-nao");
    private By inputHouveDeslocamentoAtivosNaoInput = By.id("houveDeslocamentoAtivos-nao-input");
    private By inputVagoesCorreram = By.id("vagoesCorreram");
    private By inputVagoesFreioManual = By.id("vagoesFreioManual");
    private By inputVagoesCarregados = By.id("vagoesCarregados");
    private By inputVagoesVazios = By.id("vagoesVazios");
    private By radioGroupFreioManualAplicado = By.id("freioManualAplicado");
    private By radioButtonFreioManualAplicadoSim = By.id("freioManualAplicado-sim");
    private By inputFreioManualAplicadoSimInput = By.id("freioManualAplicado-sim-input");
    private By radioButtonFreioManualAplicadoNao = By.id("freioManualAplicado-nao");
    private By inputFreioManualAplicadoNaoInput = By.id("freioManualAplicado-nao-input");
    private By radioGroupLocomotivaCalcada = By.id("locomotivaCalcada");
    private By radioButtonLocomotivaCalcadaSim = By.id("locomotivaCalcada-sim");
    private By inputLocomotivaCalcadaSimInput = By.id("locomotivaCalcada-sim-input");
    private By radioButtonLocomotivaCalcadaNao = By.id("locomotivaCalcada-nao");
    private By inputLocomotivaCalcadaNaoInput = By.id("locomotivaCalcada-nao-input");

    private By radioGroupCalco = By.id("calco");
    private By radioButtonCalcoMadeira = By.id("calco-0");
    private By inputCalcoMadeiraInput = By.id("calco-0-input");
    private By radioButtonCalcoMetalico = By.id("calco-1");
    private By inputCalcoMetalicoInput = By.id("calco-1-input");

    private By inputQtdVeiculosDescalcados = By.id("qtdVeiculosDescalcados");
    private By radioGroupHaviaFreioManualInoperante = By.id("haviaFreioManualInoperante");
    private By radioButtonHaviaFreioManualInoperanteSim = By.id("haviaFreioManualInoperante-sim");
    private By inputHaviaFreioManualInoperanteSimInput = By.id("haviaFreioManualInoperante-sim-input");
    private By radioButtonHaviaFreioManualInoperanteNao = By.id("haviaFreioManualInoperante-nao");
    private By inputHaviaFreioManualInoperanteNaoInput = By.id("haviaFreioManualInoperante-nao-input");
    private By inputQuantosFreioInoperante = By.id("quantosFreioInoperante");

    public void preencherDados(Estacionamento estacionamento) {
        waitTime(300);
        if (estacionamento.isHouveDeslocamentoDeAtivos()) {
            clickWithAction(radioButtonHouveDeslocamentoAtivosSim);

            scrollToElement(inputVagoesCorreram);
            sendKeys(inputVagoesCorreram, estacionamento.getQuantosVagoesCorreram());
            sendKeys(inputVagoesFreioManual, estacionamento.getQuantosVagoesEstavamComFreioManualAplicado());
            sendKeys(inputVagoesCarregados, estacionamento.getVagoesCarregados());
            sendKeys(inputVagoesVazios, estacionamento.getVagoesVazios());

            if (estacionamento.isaLocomotivaEstavaComFreioManualAplicado()) {
                clickWithAction(radioButtonFreioManualAplicadoSim);
            } else {
                clickWithAction(radioButtonFreioManualAplicadoNao);
            }

            if (estacionamento.isaLocomotivaEstavaCalcada()) {
                clickWithAction(radioButtonLocomotivaCalcadaSim);

                if (estacionamento.isComoEraOCalco()) {
                    clickWithAction(radioButtonCalcoMadeira);
                } else {
                    clickWithAction(radioButtonCalcoMetalico);
                }

                sendKeys(inputQtdVeiculosDescalcados, estacionamento.getQuantosVeiculosEstavamDescalcos());


            } else {
                clickWithAction(radioButtonLocomotivaCalcadaNao);
            }

            if (estacionamento.isHaviaFreioManualInoperante()) {
                clickWithAction(radioButtonHaviaFreioManualInoperanteSim);

                sendKeys(inputQuantosFreioInoperante, estacionamento.getQuantosFreiosManualInoperante());

            } else {
                clickWithAction(radioButtonHaviaFreioManualInoperanteNao);
            }

        } else {
            clickWithAction(radioButtonHouveDeslocamentoAtivosNao);
        }
    }

    public void validarDados(Estacionamento estacionamento) {
        scrollToElement(radioGroupHouveDeslocamentoAtivos);
        if (estacionamento.isHouveDeslocamentoDeAtivos()) {
            Assert.assertTrue(isRadioChecked(radioButtonHouveDeslocamentoAtivosSim));

            Assert.assertEquals(getValue(inputVagoesCorreram), estacionamento.getQuantosVagoesCorreram());
            Assert.assertEquals(getValue(inputVagoesFreioManual), estacionamento.getQuantosVagoesEstavamComFreioManualAplicado());
            Assert.assertEquals(getValue(inputVagoesCarregados), estacionamento.getVagoesCarregados());
            Assert.assertEquals(getValue(inputVagoesVazios), estacionamento.getVagoesVazios());

            if (estacionamento.isaLocomotivaEstavaComFreioManualAplicado()) {
                Assert.assertTrue(isRadioChecked(radioButtonFreioManualAplicadoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonFreioManualAplicadoNao));
            }

            if (estacionamento.isaLocomotivaEstavaCalcada()) {
                Assert.assertTrue(isRadioChecked(radioButtonLocomotivaCalcadaSim));

                if (estacionamento.isComoEraOCalco()) {
                    Assert.assertTrue(isRadioChecked(radioButtonCalcoMadeira));
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonCalcoMetalico));
                }

                Assert.assertEquals(getValue(inputQtdVeiculosDescalcados), estacionamento.getQuantosVeiculosEstavamDescalcos());


            } else {
                Assert.assertTrue(isRadioChecked(radioButtonLocomotivaCalcadaNao));
            }

            if (estacionamento.isHaviaFreioManualInoperante()) {
                Assert.assertTrue(isRadioChecked(radioButtonHaviaFreioManualInoperanteSim));

                Assert.assertEquals(getValue(inputQuantosFreioInoperante), estacionamento.getQuantosFreiosManualInoperante());

            } else {
                Assert.assertTrue(isRadioChecked(radioButtonHaviaFreioManualInoperanteNao));
            }

        } else {
            Assert.assertTrue(isRadioChecked(radioButtonHouveDeslocamentoAtivosNao));
        }
    }
}
