package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.TabelaAnomalias;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;
import java.util.function.Function;

public class TabelaAnomaliasPage extends GeralPage {

    Function<Integer, By> inputCellSuperel = (Integer index) -> By.id("tbAnomalias-superel-input-" + index);
    Function<Integer, By> inputCellEfeitoDinamico = (Integer index) -> By.id("tbAnomalias-efeitoDinamico-input-" + index);
    Function<Integer, By> inputCellBitola = (Integer index) -> By.id("tbAnomalias-bitola-input-" + index);
    Function<Integer, By> inputCellFlechaCorda = (Integer index) -> By.id("tbAnomalias-flecha-input-" + index);
    private By btnInformacoesSobreATabela = By.id("button-tabela-anomalias-info");
    private By btnSairDaTelaCheia = By.id("button-tabela-anomalias-sair");
    private By tableBodyAnomalias = By.cssSelector("#tabela-anomalias > tbody");
    private By inputSuperelevacaoMinima = By.id("input-superelevacaoMinima");
    private By inputSuperelevacaoMaxima = By.id("input-superelevacaoMaxima");
    private By inputVelocidadeCirculacao = By.id("input-velocidadeCirculacao");
    private By inputMaiorSuperelevacao = By.id("input-maiorSuperel");
    private By labelClasse = By.className("classe");
    private By btnSalvar = By.id("dialog-button-save");
    private By btnSalvarEFecharModal = By.id("dialog-button-save-and-close");
    private By btnCancelarModal = By.id("dialog-button-cancel");
    private By divTabela = By.tagName("sro-dialog-tabela-anomalias");
    private By btnTodasAsColunas = By.id("btn-tabela-anomalias-visualizacao-todas-colunas");

    public void validaExibicao() {
        expectElementVisible(btnInformacoesSobreATabela);
        expectElementVisible(btnSairDaTelaCheia);
        expectElementVisible(inputSuperelevacaoMinima);
        expectElementVisible(inputSuperelevacaoMaxima);
        expectElementVisible(inputVelocidadeCirculacao);
        expectElementVisible(inputMaiorSuperelevacao);
        expectElementVisible(tableBodyAnomalias);
    }

    public void preencherDadosClasse(TabelaAnomalias tabelaAnomalias) {
        sendKeys(inputSuperelevacaoMinima, tabelaAnomalias.getSuperelevacaoMinima());
        sendKeys(inputSuperelevacaoMaxima, tabelaAnomalias.getSuperelevacaoMaxima());
        sendKeys(inputVelocidadeCirculacao, tabelaAnomalias.getVelocidadeDeCirculacao());
    }

    public void preencherSuperel(TabelaAnomalias tabelaAnomalias) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].style.overflow = 'visible';", driver.findElement(By.cssSelector("sro-dialog-tabela-anomalias > div > div.content-wrapper")));

        List<WebElement> allHead = driver.findElement(By.cssSelector("#tabela-anomalias > thead")).findElements(By.tagName("th"));
        for (WebElement element : allHead) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.position = 'unset';", element);
        }

        List<WebElement> all = driver.findElement(By.cssSelector("#tabela-anomalias > tfoot")).findElements(By.tagName("td"));
        for (WebElement element : all) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.position = 'unset';", element);
        }

        for (int i = 0; i < countChildElement(tableBodyAnomalias, "tr"); i++) {
            sendKeys(inputCellSuperel.apply(i), tabelaAnomalias.getSuperel().get(i));
        }

    }

    public void preencherEfeitoDinamico(TabelaAnomalias tabelaAnomalias) {
        for (int i = 0; i < countChildElement(tableBodyAnomalias, "tr"); i++) {
            sendKeys(inputCellEfeitoDinamico.apply(i), tabelaAnomalias.getEfeitoDinamico().get(i));
        }
    }

    public void preencherBitola(TabelaAnomalias tabelaAnomalias) {
        for (int i = 0; i < countChildElement(tableBodyAnomalias, "tr"); i++) {
            sendKeys(inputCellBitola.apply(i), tabelaAnomalias.getBitola().get(i));
        }
    }

    public void preencherFlechaCorda(TabelaAnomalias tabelaAnomalias) {
        for (int i = 19; i < 41; i++) {
            sendKeys(inputCellFlechaCorda.apply(i), tabelaAnomalias.getFlechaCorda().get(i));
        }
    }

    public void clicarBtnSalvar() {
        clickAndHighlight(btnSalvar);
    }

    public void validaValoresDaTabela() {
        scrollToElement(tableBodyAnomalias);
        Assert.assertEquals(getText(tableBodyAnomalias), "-32 0 2 1 2 1 2 1 2 1 20 2 0.4\n" +
                "-31 2 4 2 4 2 4 2 4 2 20 3 0.6\n" +
                "-30 4 4 2 4 2 4 2 4 2 20 4 0.8\n" +
                "-29 6 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-28 8 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-27 10 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-26 12 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-25 14 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-24 16 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-23 18 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-22 20 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-21 22 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-20 24 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-19 26 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-18 28 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-17 30 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-16 32 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-15 34 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-14 36 4 2 4 2 4 2 4 2 20 5 1\n" +
                "-13 38 4 2 4 2 4 2 4 2 20 5 1 0 0\n" +
                "-12 40 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-11 42 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-10 44 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-9 46 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-8 48 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-7 50 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-6 52 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-5 54 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-4 56 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-3 58 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-2 60 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "-1 62 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "0 64 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "1 66 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "2 68 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "3 70 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "4 72 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "5 74 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "6 76 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "7 78 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "8 80 4 2 4 2 4 2 4 2 20 5 1 1 1\n" +
                "9 82 4 2 4 2 4 2 4 2 20 5 1\n" +
                "10 84 4 2 4 2 4 2 4 2 20 5 1\n" +
                "11 86 4 2 4 2 4 2 4 2 20 5 1\n" +
                "12 88 4 2 4 2 4 2 4 2 20 5 1\n" +
                "13 90 4 2 4 2 4 2 4 2 20 5 1\n" +
                "14 92 4 2 4 2 4 2 4 2 20 5 1\n" +
                "15 94 4 2 4 2 4 2 4 2 20 5 1\n" +
                "16 96 4 2 4 2 4 2 4 2 20 4 0.8\n" +
                "17 98 4 2 4 2 4 2 4 2 20 3 0.6\n" +
                "18 100 98 49 98 49 98 49 98 49 20 2 0.4");
    }


    public void clicarBtnTodasAsColunas() {
        clickAndHighlight(btnTodasAsColunas);
    }
}
