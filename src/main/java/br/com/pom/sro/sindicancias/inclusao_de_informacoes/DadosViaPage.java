package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.DadosVia;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class DadosViaPage extends GeralPage {

    Function<Integer, By> optionConfiguracoes = (Integer index) -> By.id("option-configuracaoVia-" + index);
    Function<Integer, By> optionBitola = (Integer index) -> By.id("option-bitola-" + index);
    Function<Integer, By> optionTipoMarcaTrilho = (Integer index) -> By.id("option-tipoMarcaTrilho-" + index);
    Function<Integer, By> optionTopologia = (Integer index) -> By.id("option-topologia-" + index);
    Function<Integer, By> optionAltimetria = (Integer index) -> By.id("option-altimetria-" + index);
    Function<Integer, By> optionRestricao = (Integer index) -> By.id("option-idRestricao-" + index);
    Function<Integer, By> optionPlanimetria = (Integer index) -> By.id("option-planimetria-" + index);
    Function<Integer, By> optionLastro = (Integer index) -> By.id("option-lastro-" + index);
    Function<Integer, By> optionFixacao = (Integer index) -> By.id("option-fixacao-" + index);
    Function<Integer, By> optionTirefond = (Integer index) -> By.id("option-tirefond-" + index);
    Function<Integer, By> optionDormentacao = (Integer index) -> By.id("option-dormentacao-" + index);


    private By title = By.cssSelector("sro-detail-sindicancia > sro-header-sindicancia > div.header-sindicancia > div > div:nth-child(1) > sro-default-title > div > h1");
    private By linkModalJustificativa = By.id("link-open-modal-justificativa");
    private By inputJustificativaModalImpossibilidadeRegistro = By.id("descricaoControl");
    private By btnSalvarModalJustificativa = By.id("dialog-button-yes");
    private By btnExcluirJustificativa = By.id("button-justificativa-excluir");
    private By labelJustificativaDescricao = By.id("justificativa-descricao");
    private By formDadosVia = By.id("formDadosVia");

    private By separatorDadosVia = By.name("DADOS VIA");
    private By separatorPlanimetria = By.name("PLANIMETRIA");
    private By separatorPontoDeDescarrilamento = By.name("PONTO DE DESCARRILAMENTO");
    private By separatorOutrasMedidas = By.name("OUTRAS MEDIDAS");
    private By separatorDormentes = By.name("DORMENTES");
    private By separatorInformacoesDoDDC = By.name("INFORMAÇÕES DO DDC");
    private By separatorInformacoesDoDQB = By.name("INFORMAÇÕES DO DQB");
    private By separatorTabelaDeGeometriaDaVia = By.name("TABELA DE GEOMETRIA DA VIA");
    private By selectConfiguracoes = By.id("configuracaoVia");
    private By selectBitola = By.id("bitola");
    private By inputExtensaoPercorridaDescarrilado = By.id("extensaoPercorridaDescarrilado");
    private By inputLinhaDanificadaVagaoVazioOuVeiculoVia = By.id("linhaDanificadaVagaoVazioOuVeiculoVia");
    private By inputLinhaDanificadaVagaoCarregadoOuLocomotiva = By.id("linhaDanificadaVagaoCarregadoOuLocomotiva");
    private By labelOptionPlanimetriaTangente = By.cssSelector("#planimetria-tangente > label");
    private By optionPlanimetriaTangente = By.id("planimetria-tangente");
    private By optionPlanimetriaCurva = By.id("planimetria-curva");
    private By labelOptionPlanimetriaCurva = By.cssSelector("#planimetria-curva > label");
    private By labelOptionCurvaSim = By.cssSelector("#curva-sim > label");
    private By optionCurvaSim = By.cssSelector("#curva-sim");
    private By optionCurvaNao = By.cssSelector("#curva-nao");
    private By labelOptionCurvaNao = By.cssSelector("#curva-nao > label");
    private By inputRaioCurva = By.id("raioCurva");
    private By inputPontoDescarrilamentoPD = By.id("pd");
    private By inputPontoDescarrilamentoPOD = By.id("pod");
    private By inputDistanciaPercorridaSobreBoleto = By.id("distanciaPercorridaSobreBoleto");
    private By selectTipoMarcaTrilho = By.id("tipoMarcaTrilho");
    private By selectTopologia = By.id("topologia");
    private By selectAltimetria = By.id("altimetria");
    private By inputRampa = By.id("rampa");
    private By selectRestricao = By.id("idRestricao");
    private By inputVelocidadeDaRestricao = By.id("velocidadeRestricao");
    private By selectLastro = By.id("lastro");
    private By inputAlturaLastro = By.id("altura");
    private By selectFixacao = By.id("fixacao");
    private By selectTirefond = By.id("tirefond");
    private By selectDormentacao = By.id("dormentacao");
    private By inputDormentesTotal = By.id("dormentesTotal");
    private By inputDormentesServivel = By.id("dormentesServivel");
    private By inputDormentesInservivel = By.id("dormentesInservivel");
    private By labelVia = By.cssSelector("#tab-via > div.tab-area-label");

    private By labelOptionHaviaDDCLocalSim = By.cssSelector("#haviaDDCLocal-sim > label");
    private By optionHaviaDDCLocalSim = By.cssSelector("#haviaDDCLocal-sim");
    private By optionHaviaDDCLocalNao = By.cssSelector("#haviaDDCLocal-nao");
    private By labelOptionHaviaDDCLocalNao = By.cssSelector("#haviaDDCLocal-nao > label");

    private By labelOptionHaviaDQBLocalSim = By.cssSelector("#haviaDQBLocal-sim > label");
    private By optionHaviaDQBLocalSim = By.cssSelector("#haviaDQBLocal-sim");
    private By optionHaviaDQBLocalNao = By.cssSelector("#haviaDQBLocal-nao");
    private By labelOptionHaviaDQBLocalNao = By.cssSelector("#haviaDQBLocal-nao > label");

    private By labelOptionAtivoDescarriladoPassouPeloDDCSim = By.cssSelector("#ativoDescarriladoPassouPeloDDC-sim > label");
    private By optionAtivoDescarriladoPassouPeloDDCSim = By.cssSelector("#ativoDescarriladoPassouPeloDDC-sim");
    private By optionAtivoDescarriladoPassouPeloDDCNao = By.cssSelector("#ativoDescarriladoPassouPeloDDC-nao");
    private By labelOptionAtivoDescarriladoPassouPeloDDCNao = By.cssSelector("#ativoDescarriladoPassouPeloDDC-nao > label");

    private By labelOptionAtuouDDCSim = By.cssSelector("#atuouDDC-sim > label");
    private By optionAtuouDDCSim = By.cssSelector("#atuouDDC-sim");
    private By optionAtuouDDCNao = By.cssSelector("#atuouDDC-nao");
    private By labelOptionAtuouDDCNao = By.cssSelector("#atuouDDC-nao > label");
    private By inputComentarioDDc = By.id("comentarioDDC");

    private By labelOptionHouveQuedaPedrasBarreiraSim = By.cssSelector("#houveQuedaPedrasBarreira-sim > label");
    private By optionHouveQuedaPedrasBarreiraSim = By.cssSelector("#houveQuedaPedrasBarreira-sim");
    private By optionHouveQuedaPedrasBarreiraNao = By.cssSelector("#houveQuedaPedrasBarreira-nao");
    private By labelOptionHouveQuedaPedrasBarreiraNao = By.cssSelector("#houveQuedaPedrasBarreira-nao > label");

    private By labelOptionEstavaOperacionalDDCSim = By.cssSelector("#estavaOperacionalDDC-sim > label");
    private By optionEstavaOperacionalDDCSim = By.cssSelector("#estavaOperacionalDDC-sim");
    private By optionEstavaOperacionalDDCNao = By.cssSelector("#estavaOperacionalDDC-nao");
    private By labelOptionEstavaOperacionalDDCNao = By.cssSelector("#estavaOperacionalDDC-nao > label");

    private By labelOptionEstavaOperacionalDQBSim = By.cssSelector("#estavaOperacionalDQB-sim > label");
    private By optionEstavaOperacionalDQBSim = By.cssSelector("#estavaOperacionalDQB-sim");
    private By optionEstavaOperacionalDQBNao = By.cssSelector("#estavaOperacionalDQB-nao");
    private By labelOptionEstavaOperacionalDQBNao = By.cssSelector("#estavaOperacionalDQB-nao > label");

    private By labelOptionEquipamentoDQBAtuouSim = By.cssSelector("#equipamentoDQBAtuou-sim > label");
    private By optionEquipamentoDQBAtuouSim = By.cssSelector("#equipamentoDQBAtuou-sim");
    private By optionEquipamentoDQBAtuouNao = By.cssSelector("#equipamentoDQBAtuou-nao");
    private By labelOptionEquipamentoDQBAtuouNao = By.cssSelector("#equipamentoDQBAtuou-nao > label");

    private By inputComentarioDQB = By.id("comentarioDQB");
    private By btnPreencherTabelaAnomalias = By.id("button-tabela-anomalias");
    private By btnSalvarInformacoes = By.id("salvarDadosVia");
    private By btnSalvarEAvancar = By.id("salvarAvancarDadosVia");
    private By btnCancelarDadosVia = By.id("cancelarDadosVia");
    private By inputVMATrecho = By.id("vmaTrecho");
    private By inputEquipamento = By.id("equipamento");


    public void validaExibisPaginaInclusaoDeInformacoes() {
        expectLoading();
        expectElementClickable(title);
    }

    public void preencheSecaoDadosVia(DadosVia dadosVia) {
        clickAndHighlight(selectConfiguracoes);
        expectLoading();
        clickAndHighlight(optionConfiguracoes.apply(dadosVia.getConfiguracaoIndex()));
        dadosVia.setConfiguracaoValue(getText(selectConfiguracoes));

        clickAndHighlight(selectBitola);
        expectLoading();
        clickAndHighlight(optionBitola.apply(dadosVia.getBitolaIndex()));
        dadosVia.setBitolaValue(getText(selectBitola));

        sendKeys(inputVMATrecho, dadosVia.getVMATrecho());
        sendKeys(inputExtensaoPercorridaDescarrilado, dadosVia.getExtensaoPercorridaDescarriladoMetros());
        sendKeysWithJavaScript(inputEquipamento, dadosVia.getEquipamento());
        sendKeys(inputLinhaDanificadaVagaoVazioOuVeiculoVia, dadosVia.getVagaoVazioOuVeiculoDeVia());
        sendKeys(inputLinhaDanificadaVagaoCarregadoOuLocomotiva, dadosVia.getVagaoCarregadoOuLocomotiva());
    }

    public void preencheSecaoPlanimetria(DadosVia dadosVia) {
        scrollToElement(separatorPlanimetria);
        if (dadosVia.isPossuiPlanimetria()) {
            clickWithAction(optionPlanimetriaTangente);
        } else {
            clickWithAction(optionPlanimetriaCurva);
            if (dadosVia.isCurva()) {
                clickWithAction(optionCurvaSim);
            } else {
                clickWithAction(optionCurvaNao);
            }
            sendKeys(inputRaioCurva, dadosVia.getRaioDaCurvaMetros());
        }
    }

    public void preencheSecaoPontoDeDescarrilamento(DadosVia dadosVia) {
        scrollToElement(separatorPontoDeDescarrilamento);

        sendKeys(inputPontoDescarrilamentoPD, dadosVia.getPd());
        sendKeys(inputPontoDescarrilamentoPOD, dadosVia.getPod());
    }

    public void preencheSecaoOutrasMedidas(DadosVia dadosVia) {

        scrollToElement(separatorOutrasMedidas);

        clickAndHighlight(selectTipoMarcaTrilho);
        clickAndHighlight(optionTipoMarcaTrilho.apply(dadosVia.getTipoDeMarcaNoTrilhoIndex()));
        dadosVia.setTipoDeMarcaNoTrilhoValue(getText(selectTipoMarcaTrilho));

        clickAndHighlight(selectAltimetria);
        clickAndHighlight(optionAltimetria.apply(dadosVia.getAltimetriaIndex()));
        dadosVia.setAltimetriaValue(getText(selectAltimetria));

        clickAndHighlight(selectRestricao);
        clickAndHighlight(optionRestricao.apply(dadosVia.getRestricaoIndex()));
        dadosVia.setRestricaoValue(getText(selectRestricao));

        sendKeys(inputVelocidadeDaRestricao, dadosVia.getVelocidadeDaRestricao());

        clickAndHighlight(selectLastro);
        clickAndHighlight(optionLastro.apply(dadosVia.getLastroIndex()));
        dadosVia.setLastroValue(getText(selectLastro));

        sendKeys(inputAlturaLastro, dadosVia.getAlturaLastro());

        clickAndHighlight(selectTirefond);
        clickAndHighlight(optionTirefond.apply(dadosVia.getTirefondIndex()));
        dadosVia.setTirefondValue(getText(selectTirefond));

        sendKeys(inputRampa, dadosVia.getRampa());

        clickAndHighlight(selectFixacao);
        clickAndHighlight(optionFixacao.apply(dadosVia.getFixacaoIndex()));
        dadosVia.setFixacaoValue(getText(selectFixacao));

        clickAndHighlight(selectDormentacao);
        clickAndHighlight(optionDormentacao.apply(dadosVia.getDormentacaoIndex()));
        dadosVia.setDormentacaoValue(getText(selectDormentacao));
    }

    public void preencheSecaoDormentes(DadosVia dadosVia) {
        scrollToElement(separatorDormentes);
        sendKeys(inputDormentesTotal, dadosVia.getTotalDormentes());
        sendKeys(inputDormentesServivel, dadosVia.getServivelDormentes());
        sendKeys(inputDormentesInservivel, dadosVia.getInservivelDormentes());
    }

    public void preencheSecaoInformacoesDoDDC(DadosVia dadosVia) {
        scrollToElement(separatorInformacoesDoDDC);

        if (dadosVia.isHavidaDDCNoLocal()) {
            clickWithAction(optionHaviaDDCLocalSim);
            if (dadosVia.isoAtivoDescarriladoPassouPeloDDC()) {
                clickWithAction(optionAtivoDescarriladoPassouPeloDDCSim);
                if (dadosVia.isoDDCEstavaOperacional()) {
                    clickWithAction(optionEstavaOperacionalDDCSim);
                    if (dadosVia.isoDDCAtuou()) {
                        clickWithAction(optionAtuouDDCSim);
                        sendKeysWithJavaScript(inputComentarioDDc, dadosVia.getComentarioDDC());
                    } else {
                        clickWithAction(optionAtuouDDCNao);
                    }
                } else {
                    clickWithAction(optionEstavaOperacionalDDCNao);
                }
            } else {
                clickWithAction(optionAtivoDescarriladoPassouPeloDDCNao);
            }
        } else {
            clickWithAction(optionHaviaDDCLocalNao);
        }
    }

    public void preencheSecaoInformacoesDoDQB(DadosVia dadosVia) {
        scrollToElement(separatorInformacoesDoDQB);

        if (dadosVia.isHouveQuedaDePedras_Barreira()) {
            clickWithAction(optionHouveQuedaPedrasBarreiraSim);
            if (dadosVia.isHaviaDQBNoLocal()) {
                clickWithAction(optionHaviaDQBLocalSim);
                if (dadosVia.isoDQBEstavaOperacional()) {
                    clickWithAction(optionEstavaOperacionalDQBSim);
                    if (dadosVia.isoDQBAtuou()) {
                        clickWithAction(optionEquipamentoDQBAtuouSim);
                    } else {
                        clickWithAction(optionEquipamentoDQBAtuouNao);
                    }
                } else {
                    clickWithAction(optionEstavaOperacionalDQBNao);
                }
            } else {
                clickWithAction(optionHaviaDQBLocalNao);
            }
        } else {
            clickWithAction(optionHouveQuedaPedrasBarreiraNao);
        }
    }

    private void validaDistancia(DadosVia dadosVia) {

        int distancia = dadosVia.getPd() - dadosVia.getPod();
        if (distancia < 0) {
            distancia = distancia * -1;
        }

        Assert.assertEquals(String.valueOf(distancia), getValue(inputDistanciaPercorridaSobreBoleto));
    }

    public void clicarBtnSalvarInformacoes() {
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void validaSecaoDadosVia(DadosVia dadosVia) {
        Assert.assertEquals(getText(selectConfiguracoes), dadosVia.getConfiguracaoValue());
        Assert.assertEquals(getText(selectBitola), dadosVia.getBitolaValue());
        Assert.assertEquals(getValue(inputVMATrecho), String.valueOf(dadosVia.getVMATrecho()));
        Assert.assertEquals(getValue(inputExtensaoPercorridaDescarrilado), String.valueOf(dadosVia.getExtensaoPercorridaDescarriladoMetros()));
        Assert.assertEquals(getValue(inputEquipamento), String.valueOf(dadosVia.getEquipamento()));
        Assert.assertEquals(getValue(inputLinhaDanificadaVagaoVazioOuVeiculoVia), String.valueOf(dadosVia.getVagaoVazioOuVeiculoDeVia()));
        Assert.assertEquals(getValue(inputLinhaDanificadaVagaoCarregadoOuLocomotiva), String.valueOf(dadosVia.getVagaoCarregadoOuLocomotiva()));
    }

    public void validaSecaoPlanimetria(DadosVia dadosVia) {
        scrollToElement(separatorPlanimetria);
        if (dadosVia.isPossuiPlanimetria()) {
            Assert.assertEquals(getText(labelOptionPlanimetriaTangente), "Tangente");
        } else {
            Assert.assertEquals(getText(labelOptionPlanimetriaCurva), "Curva");
            if (dadosVia.isCurva()) {
                Assert.assertEquals(getText(labelOptionCurvaSim), "Circular");
            } else {
                Assert.assertEquals(getText(labelOptionCurvaNao), "Transição");
            }
            Assert.assertEquals(getValue(inputRaioCurva), String.valueOf(dadosVia.getRaioDaCurvaMetros()));
        }
    }

    public void validaSecaoPontoDeDescarrilamento(DadosVia dadosVia) {
        scrollToElement(separatorPontoDeDescarrilamento);
        Assert.assertEquals(getValue(inputPontoDescarrilamentoPD), String.valueOf(dadosVia.getPd()));
        Assert.assertEquals(getValue(inputPontoDescarrilamentoPOD), String.valueOf(dadosVia.getPod()));
    }

    public void validaSecaoOutrasMedidas(DadosVia dadosVia) {
        scrollToElement(separatorOutrasMedidas);
        Assert.assertEquals(getText(selectTipoMarcaTrilho), String.valueOf(dadosVia.getTipoDeMarcaNoTrilhoValue()));
        Assert.assertEquals(getText(selectAltimetria), String.valueOf(dadosVia.getAltimetriaValue()));
        Assert.assertEquals(getText(selectRestricao), String.valueOf(dadosVia.getRestricaoValue()));
        Assert.assertEquals(getValue(inputVelocidadeDaRestricao), String.valueOf(dadosVia.getVelocidadeDaRestricao()));
        Assert.assertEquals(getText(selectLastro), String.valueOf(dadosVia.getLastroValue()));
        Assert.assertEquals(getValue(inputAlturaLastro), String.valueOf(dadosVia.getAlturaLastro()));
        Assert.assertEquals(getText(selectTirefond), String.valueOf(dadosVia.getTirefondValue()));
        Assert.assertEquals(getValue(inputRampa), String.valueOf(dadosVia.getRampa()));
        Assert.assertEquals(getText(selectFixacao), String.valueOf(dadosVia.getFixacaoValue()));
        Assert.assertEquals(getText(selectDormentacao), String.valueOf(dadosVia.getDormentacaoValue()));
    }

    public void validaSecaoDormentes(DadosVia dadosVia) {
        scrollToElement(separatorDormentes);
        Assert.assertEquals(getValue(inputDormentesTotal), String.valueOf(dadosVia.getTotalDormentes()));
        Assert.assertEquals(getValue(inputDormentesServivel), String.valueOf(dadosVia.getServivelDormentes()));
        Assert.assertEquals(getValue(inputDormentesInservivel), String.valueOf(dadosVia.getInservivelDormentes()));
    }

    public void validaSecaoInformacoesDoDDC(DadosVia dadosVia) {
        scrollToElement(separatorInformacoesDoDDC);

        if (dadosVia.isHavidaDDCNoLocal()) {
            Assert.assertEquals(getText(labelOptionHaviaDDCLocalSim), "Sim");
            if (dadosVia.isoAtivoDescarriladoPassouPeloDDC()) {
                Assert.assertEquals(getText(labelOptionAtivoDescarriladoPassouPeloDDCSim), "Sim");
                if (dadosVia.isoDDCEstavaOperacional()) {
                    Assert.assertEquals(getText(labelOptionEstavaOperacionalDDCSim), "Sim");
                    if (dadosVia.isoDDCAtuou()) {
                        Assert.assertEquals(getText(labelOptionAtuouDDCSim), "Sim");
                    } else {
                        Assert.assertEquals(getText(labelOptionAtuouDDCNao), "Não");
                        Assert.assertEquals(getValue(inputComentarioDDc), dadosVia.getComentarioDDC());

                    }
                } else {
                    Assert.assertEquals(getText(labelOptionEstavaOperacionalDDCNao), "Não");
                }
            } else {
                Assert.assertEquals(getText(labelOptionAtivoDescarriladoPassouPeloDDCNao), "Não");
            }
        } else {
            Assert.assertEquals(getText(labelOptionHaviaDDCLocalNao), "Não");
        }
    }

    public void validaSecaoInformacoesDoDQB(DadosVia dadosVia) {
        scrollToElement(separatorInformacoesDoDQB);

        if (dadosVia.isHouveQuedaDePedras_Barreira()) {
            Assert.assertEquals(getText(labelOptionHouveQuedaPedrasBarreiraSim), "Sim");
            if (dadosVia.isHaviaDQBNoLocal()) {
                Assert.assertEquals(getText(labelOptionHaviaDQBLocalSim), "Sim");
                if (dadosVia.isoDQBEstavaOperacional()) {
                    Assert.assertEquals(getText(labelOptionEstavaOperacionalDQBSim), "Sim");
                    if (dadosVia.isoDQBAtuou()) {
                        Assert.assertEquals(getText(labelOptionEquipamentoDQBAtuouSim), "Sim");
                    } else {
                        Assert.assertEquals(getText(labelOptionEquipamentoDQBAtuouNao), "Não");
                    }
                } else {
                    Assert.assertEquals(getText(labelOptionEstavaOperacionalDQBNao), "Não");
                }
            } else {
                Assert.assertEquals(getText(labelOptionHaviaDQBLocalNao), "Não");
            }
        } else {
            Assert.assertEquals(getText(labelOptionHouveQuedaPedrasBarreiraNao), "Não");
        }
    }

    public void clicarLinkAbrirJustificativa() {
        scrollToElement(linkModalJustificativa);
        clickWithJavaScript(linkModalJustificativa);
    }

    public void preencherModalImpossivelMedir(DadosVia dadosVia) {
        sendKeysWithJavaScript(inputJustificativaModalImpossibilidadeRegistro, dadosVia.getJustificativaInserida());
        uploadArquivo(1);
    }

    public void clicarBtnSalvarModalJustificativa() {
        clickAndHighlight(btnSalvarModalJustificativa);
    }

    public void validarJustificativaInserida(DadosVia dadosVia) {
        expectElementVisibleWithoutHighlight(labelJustificativaDescricao);
        scrollToElement(labelJustificativaDescricao);
        Assert.assertEquals(getText(labelJustificativaDescricao), dadosVia.getJustificativaInserida());
    }

    public void clicarBtnExcluirJustificativa() {
        clickAndHighlight(btnExcluirJustificativa);
    }

    public void validaBotaoSalvarInformacoesDisabled() {
        Assert.assertTrue(isDisabled(btnSalvarInformacoes));
    }

    public void clicarBtnAbrirModalTabelaAnomalias() {
        clickAndHighlight(btnPreencherTabelaAnomalias);
    }

    public void clicoNaTabVia() {
        clickAndHighlight(labelVia);
    }

    public void validaFormReadOnly() {
        isReadonlyForm(formDadosVia, "input");
    }

}
