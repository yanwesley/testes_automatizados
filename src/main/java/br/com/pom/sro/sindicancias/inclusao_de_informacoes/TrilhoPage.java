package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.Trilho;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class TrilhoPage extends GeralPage {

    Function<Integer, By> optionPerfilDoTrilho = (Integer index) -> By.id("option-perfil-trilho-" + index);
    Function<Integer, By> optionPerna = (Integer index) -> By.id("option-perna-" + index);
    Function<Integer, By> optionFila = (Integer index) -> By.id("option-fila-" + index);
    Function<Integer, By> optionFabricante = (Integer index) -> By.id("option-fabricante-" + index);
    Function<Integer, By> optionCondicaoSuperficialDoTrilho = (Integer index) -> By.id("option-condicao-superficial-trilho-" + index);
    Function<Integer, By> optionCurtaLonga = (Integer index) -> By.id("option-curta-longa-" + index);
    Function<Integer, By> optionEstadoPatim = (Integer index) -> By.id("option-estado-patim-" + index);
    Function<Integer, By> optionTipoDoDefeitoTrilho = (Integer index) -> By.id("option-tipo-defeito-" + index);
    Function<Integer, By> optionTipoSolda = (Integer index) -> By.id("option-tipo-solda-" + index);
    Function<Integer, By> optionTipoReducao = (Integer index) -> By.id("option-tipo-reducao-" + index);

    private By formFormDadosTrilho = By.id("formDadosTrilho");
    private By separatorDadosGerais = By.cssSelector("#formDadosTrilho > div > div.separator > sro-topic-separator > div > span");
    private By selectPerfilTrilho = By.id("perfil-trilho");
    private By selectPerna = By.id("perna");
    private By selectFila = By.id("fila");
    private By selectFabricante = By.id("fabricante");
    private By inputDataFabricacaoTrilho = By.id("input-data-fabricacao-trilho");
    private By inputComprimentoBarra = By.id("comprimento-barra");
    private By selectCondicaoSuperficialTrilho = By.id("condicao-superficial-trilho");
    private By selectCurtaLonga = By.id("curta-longa");
    private By selectEstadoPatim = By.id("estado-patim");
    private By inputDesgasteVertical = By.id("desgaste-vertical");
    private By inputDesgasteHorizontalInterno = By.id("desgaste-horizontal-interno");
    private By inputDesgasteHorizontalExterno = By.id("desgaste-horizontal-externo");
    private By inputWModuloResistenciaTr = By.id("w-modulo-resistencia-TR");
    private By radioGroupCaminhamentoTrilho = By.id("caminhamento-trilho");
    private By radioButtonCaminhamentoTrilhoSim = By.id("caminhamento-trilho-sim");
    private By inputCaminhamentoTrilhoSimInput = By.id("caminhamento-trilho-sim-input");
    private By radioButtonCaminhamentoTrilhoNao = By.id("caminhamento-trilho-nao");
    private By inputCaminhamentoTrilhoNaoInput = By.id("caminhamento-trilho-nao-input");
    private By radioGroupLinhaLubrificada = By.id("linha-lubrificada");
    private By radioButtonLinhaLubrificadaSim = By.id("linha-lubrificada-sim");
    private By inputLinhaLubrificadaSimInput = By.id("linha-lubrificada-sim-input");
    private By radioButtonLinhaLubrificadaNao = By.id("linha-lubrificada-nao");
    private By inputLinhaLubrificadaNaoInput = By.id("linha-lubrificada-nao-input");
    private By separatorDadosDoTrilho = By.name("DADOS DO TRILHO");
    private By sroTitleLabelTrilho = By.name("TRILHO");
    private By formFormTrilho = By.id("formTrilho");
    private By radioGroupFraturaTrilho = By.id("fratura-trilho");
    private By radioButtonFraturaTrilhoSim = By.id("fratura-trilho-sim");
    private By inputFraturaTrilhoSimInput = By.id("fratura-trilho-sim-input");
    private By radioButtonFraturaTrilhoNao = By.id("fratura-trilho-nao");
    private By inputFraturaTrilhoNaoInput = By.id("fratura-trilho-nao-input");
    private By radioGroupDurantePassagemTrem = By.id("durante-passagem-trem");
    private By radioButtonDurantePassagemTremSim = By.id("durante-passagem-trem-sim");
    private By inputDurantePassagemTremSimInput = By.id("durante-passagem-trem-sim-input");
    private By radioButtonDurantePassagemTremNao = By.id("durante-passagem-trem-nao");
    private By inputDurantePassagemTremNaoInput = By.id("durante-passagem-trem-nao-input");
    private By selectTipoDefeito = By.id("tipo-defeito");
    private By sroTitleLabelSolda = By.name("SOLDA");
    private By formFormSolda = By.id("formSolda");
    private By radioGroupFraturaSolda = By.id("fratura-solda");
    private By radioButtonFraturaSoldaSim = By.id("fratura-solda-sim");
    private By inputFraturaSoldaSimInput = By.id("fratura-solda-sim-input");
    private By radioButtonFraturaSoldaNao = By.id("fratura-solda-nao");
    private By inputFraturaSoldaNaoInput = By.id("fratura-solda-nao-input");
    private By radioGroupCondicaoSolda = By.id("condicao-solda");
    private By radioButtonCondicaoSoldaBoa = By.id("condicao-solda-boa");
    private By inputCondicaoSoldaBoaInput = By.id("condicao-solda-boa-input");
    private By radioButtonCondicaoSoldaRuim = By.id("condicao-solda-ruim");
    private By inputCondicaoSoldaRuimInput = By.id("condicao-solda-ruim-input");
    private By selectTipoSolda = By.id("tipo-solda");
    private By textAreaComentarioSolda = By.id("comentarioSolda");
    private By sroTitleLabelJunta = By.name("JUNTA");
    private By formFormJunta = By.id("formJunta");
    private By radioGroupFraturaJunta = By.id("fratura-junta");
    private By radioButtonFraturaJuntaSim = By.id("fratura-junta-sim");
    private By inputFraturaJuntaSimInput = By.id("fratura-junta-sim-input");
    private By radioButtonFraturaJuntaNao = By.id("fratura-junta-nao");
    private By inputFraturaJuntaNaoInput = By.id("fratura-junta-nao-input");
    private By radioGroupCondicaoJunta = By.id("condicao-junta");
    private By radioButtonCondicaoJuntaFaltandoParafusos = By.id("condicao-junta-faltando-parafusos");
    private By inputCondicaoJuntaFaltandoParafusosInput = By.id("condicao-junta-faltando-parafusos-input");
    private By radioButtonCondicaoJuntaParafusosFrouxos = By.id("condicao-junta-parafusos-frouxos");
    private By inputCondicaoJuntaParafusosFrouxosInput = By.id("condicao-junta-parafusos-frouxos-input");
    private By radioButtonCondicaoJuntaParafusosBemApertados = By.id("condicao-junta-parafusos-bem-apertados");
    private By inputCondicaoJuntaParafusosBemApertadosInput = By.id("condicao-junta-parafusos-bem-apertados-input");
    private By inputDistanciaPrimeiroFuro = By.id("distancia-primeiro-furo");
    private By inputDistanciaEntreFuro = By.id("distancia-entre-furo");
    private By textAreaComentarioJunta = By.id("comentarioJunta");
    private By sroTitleLabelReducao = By.name("REDUÇÃO");
    private By formFormReducao = By.id("formReducao");
    private By radioGroupFraturaReducao = By.id("fraturaReducao");
    private By radioButtonFraturaReducaoSim = By.id("fraturaReducao-sim");
    private By inputFraturaReducaoSimInput = By.id("fraturaReducao-sim-input");
    private By radioButtonFraturaReducaoNao = By.id("fraturaReducao-nao");
    private By inputFraturaReducaoNaoInput = By.id("fraturaReducao-nao-input");
    private By selectTipoReducao = By.id("tipo-reducao");
    private By separatorInformacaoDoDtq = By.name("INFORMAÇÃO DO DTQ");
    private By formFormInfoDtq = By.id("formInfoDTQ");
    private By radioGroupHaviaDtqLocal = By.id("havia-dtq-local");
    private By radioButtonHaviaDtqLocalSim = By.id("havia-dtq-local-sim");
    private By inputHaviaDtqLocalSimInput = By.id("havia-dtq-local-sim-input");
    private By radioButtonHaviaDtqLocalNao = By.id("havia-dtq-local-nao");
    private By inputHaviaDtqLocalNaoInput = By.id("havia-dtq-local-nao-input");
    private By radioGroupOperacionalDtq = By.id("operacional-dtq");
    private By radioButtonOperacionalDtqSim = By.id("operacional-dtq-sim");
    private By inputOperacionalDtqSimInput = By.id("operacional-dtq-sim-input");
    private By radioButtonOperacionalDtqNao = By.id("operacional-dtq-nao");
    private By inputOperacionalDtqNaoInput = By.id("operacional-dtq-nao-input");
    private By radioGroupAtuouDtq = By.id("atuou-dtq");
    private By radioButtonAtuouDtqSim = By.id("atuou-dtq-sim");
    private By inputAtuouDtqSimInput = By.id("atuou-dtq-sim-input");
    private By radioButtonAtuouDtqNao = By.id("atuou-dtq-nao");
    private By inputAtuouDtqNaoInput = By.id("atuou-dtq-nao-input");
    private By textAreaComentarioDtq = By.id("comentarioDTQ");
    private By separatorDadosDaTemperatura = By.name("DADOS DA TEMPERATURA");
    private By formFormDadosTemperatura = By.id("formDadosTemperatura");
    private By inputTemperaturaDeteccao = By.id("tempreatura-deteccao");
    private By inputTemperaturaMinima = By.id("temperatura-minima");
    private By inputTemperaturaMaxima = By.id("temperatura-maxima");
    private By buttonSalvarInformacoesTrilho = By.id("salvar-informacoes-trilho");
    private By buttonSalvarAvancarInformacoesTrilho = By.id("salvar-avancar-informacoes-trilho");
    private By buttonCancelarDadosVia = By.id("cancelarDadosVia");

    public void preencherDadosGerais(Trilho trilho) {
        scrollToElement(separatorDadosGerais);

        clickAndHighlight(selectPerfilTrilho);
        clickAndHighlight(optionPerfilDoTrilho.apply(trilho.getPerfilTrilhoIndex()));
        trilho.setPerfilTrilhoValue(getValue(selectPerfilTrilho));


        clickAndHighlight(selectPerna);
        clickAndHighlight(optionPerna.apply(trilho.getPernaIndex()));
        trilho.setPernaValue(getValue(selectPerna));


        clickAndHighlight(selectFila);
        clickAndHighlight(optionFila.apply(trilho.getFilaIndex()));
        trilho.setFilaValue(getValue(selectFila));

        clickAndHighlight(selectFabricante);
        clickAndHighlight(optionFabricante.apply(trilho.getFabricanteIndex()));
        trilho.setFabricanteValue(getValue(selectFabricante));

        clearForce(inputDataFabricacaoTrilho);
        sendKeys(inputDataFabricacaoTrilho, trilho.getDataFabricacaoTrilho());

        sendKeys(inputComprimentoBarra, trilho.getComprimentoDaBarra());

        clickAndHighlight(selectCondicaoSuperficialTrilho);
        clickAndHighlight(optionCondicaoSuperficialDoTrilho.apply(trilho.getCondicaoSuperficialDoTrilhoIndex()));
        trilho.setCondicaoSuperficialDoTrilhoValue(getValue(selectCondicaoSuperficialTrilho));

        clickAndHighlight(selectCurtaLonga);
        clickAndHighlight(optionCurtaLonga.apply(trilho.getCurtaLongaIndex()));
        trilho.setCurtaLongaValue(getValue(selectCurtaLonga));

        clickAndHighlight(selectEstadoPatim);
        clickAndHighlight(optionEstadoPatim.apply(trilho.getEstadoDoPatimIndex()));
        trilho.setEstadoDoPatimValue(getValue(selectEstadoPatim));

        sendKeys(inputDesgasteVertical, trilho.getDesgasteVertical());
        sendKeys(inputDesgasteHorizontalInterno, trilho.getDesgasteHorizontalInterno());
        sendKeys(inputDesgasteHorizontalExterno, trilho.getDesgasteHorizontalExterno());
        sendKeys(inputWModuloResistenciaTr, trilho.getwModuloDeResistenciaTR());

        if (trilho.isExisteCaminhamentoDeTrilho()) {
            clickWithAction(radioButtonCaminhamentoTrilhoSim);
        } else {
            clickWithAction(radioButtonCaminhamentoTrilhoNao);
        }

        if (trilho.isaLinhaEstavaLubrificada()) {
            clickWithAction(radioButtonLinhaLubrificadaSim);
        } else {
            clickWithAction(radioButtonLinhaLubrificadaNao);
        }

    }

    public void preencherDadosDoTrilho(Trilho trilho) {
        scrollToElement(separatorDadosDoTrilho);
        waitTime(500);
        if (trilho.isHouveFraturaDoTrilho()) {
            clickWithAction(radioButtonFraturaTrilhoSim);

            if (trilho.isOcorreuDuranteAPassagemDoTrem()) {
                clickWithAction(radioButtonDurantePassagemTremSim);
            } else {
                clickWithAction(radioButtonDurantePassagemTremNao);
            }

            clickAndHighlight(selectTipoDefeito);
            clickAndHighlight(optionTipoDoDefeitoTrilho.apply(trilho.getQualTipoDoDefeitoIndex()));
            trilho.setQualTipoDoDefeitoValue(getValue(selectTipoDefeito));

            if (trilho.isFraturaEmSolda()) {

                clickWithAction(radioButtonFraturaSoldaSim);

                if (trilho.isQualEraACondicaoDaSolda()) {
                    clickWithAction(radioButtonCondicaoSoldaBoa);
                } else {
                    clickWithAction(radioButtonCondicaoSoldaRuim);
                }


                clickAndHighlight(selectTipoSolda);
                clickAndHighlight(optionTipoSolda.apply(trilho.getQualTipoDaSoldaIndex()));
                trilho.setQualTipoDaSoldaValue(getValue(selectTipoSolda));

                sendKeysWithJavaScript(textAreaComentarioSolda, trilho.getComentarioSolda());

            } else {
                clickWithAction(radioButtonFraturaSoldaNao);
            }


            if (trilho.isFraturaEmJunta()) {
                clickWithAction(radioButtonFraturaJuntaSim);
                switch (trilho.getQualEraACondicaoDaJunta()) {
                    case 0:
                        clickWithAction(radioButtonCondicaoJuntaFaltandoParafusos);
                        break;
                    case 1:
                        clickWithAction(radioButtonCondicaoJuntaParafusosFrouxos);
                        break;
                    case 2:
                        clickWithAction(radioButtonCondicaoJuntaParafusosBemApertados);
                        break;
                }

                sendKeys(inputDistanciaPrimeiroFuro, trilho.getDistancia1Furo());
                sendKeys(inputDistanciaEntreFuro, trilho.getDistanciaEntreFuro());
                sendKeysWithJavaScript(textAreaComentarioJunta, trilho.getComentarioJunta());

            } else {
                clickWithAction(radioButtonFraturaJuntaNao);
            }

            if (trilho.isFraturaEmReducao()) {
                clickWithAction(radioButtonFraturaReducaoSim);

                waitTime(500);

                clickAndHighlight(selectTipoReducao);
                clickAndHighlight(optionTipoReducao.apply(trilho.getQualTipoDaReducaoIndex()));
                trilho.setQualTipoDaReducaoValue(getValue(selectTipoReducao));

            } else {
                clickWithAction(radioButtonFraturaReducaoNao);
            }

        } else {
            clickWithAction(radioButtonFraturaTrilhoNao);
        }
    }

    public void preencherInformacoesDoDTQ(Trilho trilho) {

        scrollToElement(separatorInformacaoDoDtq);
        waitTime(500);
        if (trilho.isHaviaDTQNoLocal()) {
            clickWithAction(radioButtonHaviaDtqLocalSim);

            if (trilho.isEstavaOperacionalDTQ()) {
                waitTime(500);
                clickWithAction(radioButtonOperacionalDtqSim);

                if (trilho.isAtuouDTQ()) {

                    clickWithAction(radioButtonAtuouDtqSim);
                } else {
                    clickWithAction(radioButtonAtuouDtqNao);
                    sendKeysWithJavaScript(textAreaComentarioDtq, trilho.getComentarioDTQ());
                }


            } else {
                clickWithAction(radioButtonOperacionalDtqNao);
            }

        } else {
            clickWithAction(radioButtonHaviaDtqLocalNao);
        }

    }

    public void preencherDadosDaTemperatura(Trilho trilho) {
        scrollToElement(separatorDadosDaTemperatura);
        sendKeys(inputTemperaturaDeteccao, trilho.getTemperaturaDaDeteccao());
        sendKeys(inputTemperaturaMinima, trilho.getTemperaturaMinima());
        sendKeys(inputTemperaturaMaxima, trilho.getTemperaturaMaxima());
    }

    public void clicarBtnSalvarInformacoes() {
        clickAndHighlight(buttonSalvarInformacoesTrilho);
    }

    public void validarDadosGerais(Trilho trilho) {
        scrollToElement(separatorDadosGerais);

        Assert.assertEquals(getValue(selectPerfilTrilho), trilho.getPerfilTrilhoValue());
        Assert.assertEquals(getValue(selectPerna), trilho.getPernaValue());
        Assert.assertEquals(getValue(selectFila), trilho.getFilaValue());
        Assert.assertEquals(getValue(selectFabricante), trilho.getFabricanteValue());
        Assert.assertEquals(getValue(inputDataFabricacaoTrilho), trilho.getDataFabricacaoTrilho());
        Assert.assertEquals(getValue(inputComprimentoBarra), trilho.getComprimentoDaBarra());
        Assert.assertEquals(getValue(selectCondicaoSuperficialTrilho), trilho.getCondicaoSuperficialDoTrilhoValue());
        Assert.assertEquals(getValue(selectCurtaLonga), trilho.getCurtaLongaValue());
        Assert.assertEquals(getValue(selectEstadoPatim), trilho.getEstadoDoPatimValue());
        Assert.assertEquals(getValue(inputDesgasteVertical), trilho.getDesgasteVertical());
        Assert.assertEquals(getValue(inputDesgasteHorizontalInterno), trilho.getDesgasteHorizontalInterno());
        Assert.assertEquals(getValue(inputDesgasteHorizontalExterno), trilho.getDesgasteHorizontalExterno());
        Assert.assertEquals(getValue(inputWModuloResistenciaTr), trilho.getwModuloDeResistenciaTR());

        if (trilho.isExisteCaminhamentoDeTrilho()) {
            Assert.assertTrue(isRadioChecked(radioButtonCaminhamentoTrilhoSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonCaminhamentoTrilhoNao));
        }

        if (trilho.isaLinhaEstavaLubrificada()) {
            Assert.assertTrue(isRadioChecked(radioButtonLinhaLubrificadaSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonLinhaLubrificadaNao));
        }

    }

    public void validarDadosDoTrilho(Trilho trilho) {

        scrollToElement(separatorDadosDoTrilho);
        if (trilho.isHouveFraturaDoTrilho()) {
            Assert.assertTrue(isRadioChecked(radioButtonFraturaTrilhoSim));

            if (trilho.isOcorreuDuranteAPassagemDoTrem()) {
                Assert.assertTrue(isRadioChecked(radioButtonDurantePassagemTremSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonDurantePassagemTremNao));
            }

            Assert.assertEquals(getValue(selectTipoDefeito), trilho.getQualTipoDoDefeitoValue());

            if (trilho.isFraturaEmSolda()) {
                Assert.assertTrue(isRadioChecked(radioButtonFraturaSoldaSim));

                if (trilho.isQualEraACondicaoDaSolda()) {
                    Assert.assertTrue(isRadioChecked(radioButtonCondicaoSoldaBoa));
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonCondicaoSoldaRuim));
                }

                Assert.assertEquals(getValue(selectTipoSolda), trilho.getQualTipoDaSoldaValue());
                Assert.assertEquals(getValue(textAreaComentarioSolda), trilho.getComentarioSolda());

            } else {
                Assert.assertTrue(isRadioChecked(radioButtonFraturaSoldaNao));
            }

            if (trilho.isFraturaEmJunta()) {
                clickWithAction(radioButtonFraturaJuntaSim);
                switch (trilho.getQualEraACondicaoDaJunta()) {
                    case 0:
                        Assert.assertTrue(isRadioChecked(radioButtonCondicaoJuntaFaltandoParafusos));
                        break;
                    case 1:
                        Assert.assertTrue(isRadioChecked(radioButtonCondicaoJuntaParafusosFrouxos));
                        break;
                    case 2:
                        Assert.assertTrue(isRadioChecked(radioButtonCondicaoJuntaParafusosBemApertados));
                        break;
                }


                Assert.assertEquals(getValue(inputDistanciaPrimeiroFuro), trilho.getDistancia1Furo());
                Assert.assertEquals(getValue(inputDistanciaEntreFuro), trilho.getDistanciaEntreFuro());
                Assert.assertEquals(getValue(textAreaComentarioJunta), trilho.getComentarioJunta());

            } else {
                Assert.assertTrue(isRadioChecked(radioButtonFraturaJuntaNao));
            }

            if (trilho.isFraturaEmReducao()) {

                Assert.assertTrue(isRadioChecked(radioButtonFraturaReducaoSim));
                Assert.assertEquals(getValue(selectTipoReducao), trilho.getQualTipoDaReducaoValue());
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonFraturaReducaoNao));
            }

        } else {
            Assert.assertTrue(isRadioChecked(radioButtonFraturaTrilhoNao));
        }

    }

    public void validarInformacoesDoDTQ(Trilho trilho) {

        scrollToElement(separatorInformacaoDoDtq);

        if (trilho.isHaviaDTQNoLocal()) {
            Assert.assertTrue(isRadioChecked(radioButtonHaviaDtqLocalSim));

            if (trilho.isEstavaOperacionalDTQ()) {

                Assert.assertTrue(isRadioChecked(radioButtonOperacionalDtqSim));

                if (trilho.isAtuouDTQ()) {

                    Assert.assertTrue(isRadioChecked(radioButtonAtuouDtqSim));
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonAtuouDtqNao));
                    Assert.assertEquals(getValue(textAreaComentarioDtq), trilho.getComentarioDTQ());
                }

            } else {
                Assert.assertTrue(isRadioChecked(radioButtonOperacionalDtqNao));
            }

        } else {
            Assert.assertTrue(isRadioChecked(radioButtonHaviaDtqLocalNao));
        }

    }

    public void validarDadosDaTemperatura(Trilho trilho) {
        scrollToElement(separatorDadosDaTemperatura);

        Assert.assertEquals(getValue(inputTemperaturaDeteccao), trilho.getTemperaturaDaDeteccao());
        Assert.assertEquals(getValue(inputTemperaturaMinima), trilho.getTemperaturaMinima());
        Assert.assertEquals(getValue(inputTemperaturaMaxima), trilho.getTemperaturaMaxima());
    }
}
