package br.com.pom.sro.sindicancias.inclusao_de_informacoes.questionario_maquinista;

import br.com.api.model.sindicancia.questionario.QuestionarioManobrador;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class QuestionarioManobradorSindicanciaPage extends GeralPage {

    IntFunction<By> radioButtonTipoSinalizacao = (index) -> By.id("tipoSinalizacao-" + index);
    IntFunction<By> radioButtonAlgumVeiculoMovimentado = (index) -> By.id("algumVeiculoMovimentado-" + index);
    IntFunction<By> radioButtonFreioDosVagoesUtilizados = (index) -> By.id("freioDosVagoesUtilizados-" + index);
    IntFunction<By> radioButtonHaviaCondicaoIrregularEstrutura = (index) -> By.id("haviaCondicaoIrregularEstrutura-" + index);
    IntFunction<By> radioButtonHaviaCondicaoIrregularEquipamento = (index) -> By.id("haviaCondicaoIrregularEquipamento-" + index);
    IntFunction<By> radioButtonDescarrilamentoOcorreuDuranteEngate = (index) -> By.id("descarrilamentoOcorreuDuranteEngate-" + index);
    IntFunction<By> radioButtonEngatesAlinhados = (index) -> By.id("engatesAlinhados-" + index);
    IntFunction<By> radioButtonDescarrilamentoDevidoResultadoColisao = (index) -> By.id("descarrilamentoDevidoResultadoColisao-" + index);
    IntFunction<By> radioButtonFoiNotadaAcaoFolgasNaoComum = (index) -> By.id("foiNotadaAcaoFolgasNaoComum-" + index);
    private By radioGroupAcidenteDuranteManobra = By.id("acidenteDuranteManobra");
    private By radioButtonAcidenteDuranteManobraSim = By.id("acidenteDuranteManobra-sim");
    private By inputAcidenteDuranteManobraSimInput = By.id("acidenteDuranteManobra-sim-input");
    private By radioButtonAcidenteDuranteManobraNao = By.id("acidenteDuranteManobra-nao");
    private By inputAcidenteDuranteManobraNaoInput = By.id("acidenteDuranteManobra-nao-input");
    private By radioGroupTipoSinalizacao = By.id("tipoSinalizacao");
    private By inputQuemEstavaTransmitindo = By.id("quemEstavaTransmitindo");
    private By textareaQualUltimaOrdem = By.id("qualUltimaOrdem");
    private By radioGroupHaviaCoberturaTrem = By.id("haviaCoberturaTrem");
    private By radioButtonHaviaCoberturaTremSim = By.id("haviaCoberturaTrem-sim");
    private By inputHaviaCoberturaTremSimInput = By.id("haviaCoberturaTrem-sim-input");
    private By radioButtonHaviaCoberturaTremNao = By.id("haviaCoberturaTrem-nao");
    private By inputHaviaCoberturaTremNaoInput = By.id("haviaCoberturaTrem-nao-input");
    private By textareaSinaisDistanciaInformadosComo = By.id("sinaisDistanciaInformadosComo");
    private By radioGroupVisualizouPosicaoAMV = By.id("visualizouPosicaoAMV");
    private By radioButtonVisualizouPosicaoAMVSim = By.id("visualizouPosicaoAMV-sim");
    private By inputVisualizouPosicaoAMVSimInput = By.id("visualizouPosicaoAMV-sim-input");
    private By radioButtonVisualizouPosicaoAMVNao = By.id("visualizouPosicaoAMV-nao");
    private By inputVisualizouPosicaoAMVNaoInput = By.id("visualizouPosicaoAMV-nao-input");
    private By radioGroupUltrapassouAMV = By.id("ultrapassouAMV");
    private By radioButtonUltrapassouAMVSim = By.id("ultrapassouAMV-sim");
    private By inputUltrapassouAMVSimInput = By.id("ultrapassouAMV-sim-input");
    private By radioButtonUltrapassouAMVNao = By.id("ultrapassouAMV-nao");
    private By inputUltrapassouAMVNaoInput = By.id("ultrapassouAMV-nao-input");
    private By inputQuantoUltrapassadoAMV = By.id("quantoUltrapassadoAMV");
    private By textareaComoImobilizarLocomotiva = By.id("comoImobilizarLocomotiva");
    private By textareaAnomaliasObservadas = By.id("anomaliasObservadas");
    private By textareaDescrevaOperacaoTrem = By.id("descrevaOperacaoTrem");
    private By inputQuantosVagoesEstavamEngatados = By.id("quantosVagoesEstavamEngatados");
    private By radioGroupEstavaSendoMovimentadoAMV = By.id("estavaSendoMovimentadoAMV");
    private By radioButtonEstavaSendoMovimentadoAMVSim = By.id("estavaSendoMovimentadoAMV-sim");
    private By inputEstavaSendoMovimentadoAMVSimInput = By.id("estavaSendoMovimentadoAMV-sim-input");
    private By radioButtonEstavaSendoMovimentadoAMVNao = By.id("estavaSendoMovimentadoAMV-nao");
    private By inputEstavaSendoMovimentadoAMVNaoInput = By.id("estavaSendoMovimentadoAMV-nao-input");
    private By radioGroupAlgumVeiculoMovimentado = By.id("algumVeiculoMovimentado");
    private By radioGroupFreioDosVagoesUtilizados = By.id("freioDosVagoesUtilizados");
    private By inputFreioQuantosVagoes = By.id("freioQuantosVagoes");
    private By radioGroupHaviaCondicaoIrregularEstrutura = By.id("haviaCondicaoIrregularEstrutura");
    private By radioGroupHaviaCondicaoIrregularEquipamento = By.id("haviaCondicaoIrregularEquipamento");
    private By textareaExpliqueCondicaoIrregular = By.id("expliqueCondicaoIrregular");
    private By radioGroupDescarrilamentoOcorreuDuranteEngate = By.id("descarrilamentoOcorreuDuranteEngate");
    private By radioGroupEngatesAlinhados = By.id("engatesAlinhados");
    private By radioGroupDescarrilamentoDevidoResultadoColisao = By.id("descarrilamentoDevidoResultadoColisao");
    private By textareaExpliqueDevidoResultadoColisao = By.id("expliqueDevidoResultadoColisao");
    private By radioGroupFoiNotadaAcaoFolgasNaoComum = By.id("foiNotadaAcaoFolgasNaoComum");
    private By textareaExpliqueAcaoFolga = By.id("expliqueAcaoFolga");
    private By textareaOndeEstavamMaquinista = By.id("ondeEstavamMaquinista");
    private By textareaOndeEstavamAjudante = By.id("ondeEstavamAjudante");
    private By textareaOndeEstavamManobrador = By.id("ondeEstavamManobrador");

    public void preencherCampos(QuestionarioManobrador questionarioManobrador) {
        waitTime(400);
        if (questionarioManobrador.isAcidenteDuranteAManobra()) {
            clickWithAction(radioButtonAcidenteDuranteManobraSim);

            questionarioManobrador.setQueTipoDeSinalizacaoEstavaSendoUtilizadaValue(
                    selectOptionAndReturnValue(radioButtonTipoSinalizacao, questionarioManobrador.getQueTipoDeSinalizacaoEstavaSendoUtilizadaIndex())
            );

            sendKeysWithJavaScript(inputQuemEstavaTransmitindo, questionarioManobrador.getQuemEstavaTransmitindoOsSinais());

            sendKeysWithJavaScript(textareaQualUltimaOrdem, questionarioManobrador.getQualFoiAUltimoSinalRecebidoPeloMaquinistaAntesDoDescarrilamento());

            if (questionarioManobrador.isHaviaCoberturaDoTremPorParteDoManobrador()) {
                clickWithAction(radioButtonHaviaCoberturaTremSim);
            } else {
                clickWithAction(radioButtonHaviaCoberturaTremNao);
            }

            sendKeysWithJavaScript(textareaSinaisDistanciaInformadosComo, questionarioManobrador.getOsSinaisDeDistanciaEstavamSendoInformadosComo());

            if (questionarioManobrador.isVoceVisualizouAPosicaoDaChaveDoAMV()) {
                clickWithAction(radioButtonVisualizouPosicaoAMVSim);
            } else {
                clickWithAction(radioButtonVisualizouPosicaoAMVNao);
            }

            if (questionarioManobrador.isVoceUltrapassouOAMVComTodaALocomotiva()) {
                clickWithAction(radioButtonUltrapassouAMVSim);
            } else {
                clickWithAction(radioButtonUltrapassouAMVNao);
                sendKeys(inputQuantoUltrapassadoAMV, questionarioManobrador.getoQuantoFoiUltrapassado());
            }

            sendKeysWithJavaScript(textareaComoImobilizarLocomotiva, questionarioManobrador.getVoceQuandoSaiDeLocomotivaDeManobraOQueFazParaImobilizarALocomotiva());
            sendKeysWithJavaScript(textareaAnomaliasObservadas,
                    questionarioManobrador
                            .getQuaisAsAnomaliasVoceObservouNaOperacaoEQualSuaParticipacaoParaQueOEventoAcidenteOcorrenciaEtcOcorresseOQuePoderiaSerFeitoNaSuaOpiniaoParaEvitarNovaOcorrencia()
            );

            sendKeysWithJavaScript(textareaDescrevaOperacaoTrem, questionarioManobrador.getDescrevaAOperacaoDoTremNoMomentoDoDescarrilamento());

            sendKeys(inputQuantosVagoesEstavamEngatados, questionarioManobrador.getQuantosVagoesEstavamEngatadosNaLocomotivaNoMomentoDoDescarrilamento());

            if (questionarioManobrador.isEstavaSendoMovimentadoUmAMVPorUmMembroDaTripulacaoNoMomentoDoDescarrilamento()) {
                clickWithAction(radioButtonEstavaSendoMovimentadoAMVSim);
            } else {
                clickWithAction(radioButtonEstavaSendoMovimentadoAMVNao);
            }

            questionarioManobrador.setAlgumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoValue(
                    selectOptionAndReturnValue(
                            radioButtonAlgumVeiculoMovimentado,
                            questionarioManobrador.getAlgumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoIndex()
                    )
            );

            questionarioManobrador.setoFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoValue(
                    selectOptionAndReturnValue(
                            radioButtonFreioDosVagoesUtilizados,
                            questionarioManobrador.getoFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoIndex()
                    )
            );

            if (questionarioManobrador.getoFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoIndex() == 0) {
                sendKeys(inputFreioQuantosVagoes, questionarioManobrador.getEmQuantosVagoes());
            }


            questionarioManobrador.setHaviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoValue(
                    selectOptionAndReturnValue(
                            radioButtonHaviaCondicaoIrregularEstrutura,
                            questionarioManobrador.getHaviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoIndex()
                    )
            );

            questionarioManobrador.setHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoValue(
                    selectOptionAndReturnValue(
                            radioButtonHaviaCondicaoIrregularEquipamento,
                            questionarioManobrador.getHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoIndex()
                    )
            );

            if (questionarioManobrador.getHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoIndex() == 0) {
                sendKeysWithJavaScript(textareaExpliqueCondicaoIrregular, questionarioManobrador.getExpliqueHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamento());
            }

            questionarioManobrador.setoDescarrilamentoOcorreuDuranteOEngateValue(
                    selectOptionAndReturnValue(
                            radioButtonDescarrilamentoOcorreuDuranteEngate,
                            questionarioManobrador.getoDescarrilamentoOcorreuDuranteOEngateIndex()
                    )
            );

            if (questionarioManobrador.getoDescarrilamentoOcorreuDuranteOEngateIndex() == 0) {
                questionarioManobrador.setOsEngatesEstavamAlinhadosValue(
                        selectOptionAndReturnValue(
                                radioButtonEngatesAlinhados,
                                questionarioManobrador.getOsEngatesEstavamAlinhadosIndex()
                        )
                );
            }

            questionarioManobrador.setoDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosValue(
                    selectOptionAndReturnValue(
                            radioButtonDescarrilamentoDevidoResultadoColisao,
                            questionarioManobrador.getoDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosIndex()
                    )
            );

            if (questionarioManobrador.getoDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosIndex() == 0) {
                sendKeysWithJavaScript(textareaExpliqueDevidoResultadoColisao, questionarioManobrador.getExpliqueODescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2Veiculos());
            }


            questionarioManobrador.setFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoValue(
                    selectOptionAndReturnValue(
                            radioButtonFoiNotadaAcaoFolgasNaoComum,
                            questionarioManobrador.getFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoIndex()
                    )
            );

            if (questionarioManobrador.getFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoIndex() == 0) {
                sendKeysWithJavaScript(textareaExpliqueAcaoFolga, questionarioManobrador.getExpliqueFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamento());
            }

            sendKeysWithJavaScript(textareaOndeEstavamMaquinista, questionarioManobrador.getAondeEstavaOMaquinistaNoMomentoDoDescarrilamento());
            sendKeysWithJavaScript(textareaOndeEstavamAjudante, questionarioManobrador.getAondeEstavaOAjudanteDeMaquinistaNoMomentoDoDescarrilamento());
            sendKeysWithJavaScript(textareaOndeEstavamManobrador, questionarioManobrador.getAondeEstavaOManobradorAssistenteDePatioNoMomentoDoDescarrilamento());

        } else {
            clickWithAction(radioButtonAcidenteDuranteManobraNao);
        }
    }

    public void validarDados(QuestionarioManobrador questionarioManobrador) {

        if (questionarioManobrador.isAcidenteDuranteAManobra()) {
            Assert.assertTrue(isRadioChecked(radioButtonAcidenteDuranteManobraSim));

            Assert.assertTrue(isRadioChecked(
                    radioButtonTipoSinalizacao.apply(
                            questionarioManobrador.getQueTipoDeSinalizacaoEstavaSendoUtilizadaIndex())
            ));

            Assert.assertEquals(getValue(inputQuemEstavaTransmitindo), questionarioManobrador.getQuemEstavaTransmitindoOsSinais());

            Assert.assertEquals(getValue(textareaQualUltimaOrdem), questionarioManobrador.getQualFoiAUltimoSinalRecebidoPeloMaquinistaAntesDoDescarrilamento());

            if (questionarioManobrador.isHaviaCoberturaDoTremPorParteDoManobrador()) {
                Assert.assertTrue(isRadioChecked(radioButtonHaviaCoberturaTremSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonHaviaCoberturaTremNao));
            }

            Assert.assertEquals(getValue(textareaSinaisDistanciaInformadosComo), questionarioManobrador.getOsSinaisDeDistanciaEstavamSendoInformadosComo());

            if (questionarioManobrador.isVoceVisualizouAPosicaoDaChaveDoAMV()) {
                Assert.assertTrue(isRadioChecked(radioButtonVisualizouPosicaoAMVSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonVisualizouPosicaoAMVNao));
            }

            if (questionarioManobrador.isVoceUltrapassouOAMVComTodaALocomotiva()) {
                Assert.assertTrue(isRadioChecked(radioButtonUltrapassouAMVSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonUltrapassouAMVNao));
                Assert.assertEquals(getValue(inputQuantoUltrapassadoAMV), questionarioManobrador.getoQuantoFoiUltrapassado());
            }

            Assert.assertEquals(getValue(textareaComoImobilizarLocomotiva), questionarioManobrador.getVoceQuandoSaiDeLocomotivaDeManobraOQueFazParaImobilizarALocomotiva());
            Assert.assertEquals(getValue(textareaAnomaliasObservadas),
                    questionarioManobrador.getQuaisAsAnomaliasVoceObservouNaOperacaoEQualSuaParticipacaoParaQueOEventoAcidenteOcorrenciaEtcOcorresseOQuePoderiaSerFeitoNaSuaOpiniaoParaEvitarNovaOcorrencia()
            );

            Assert.assertEquals(getValue(textareaDescrevaOperacaoTrem), questionarioManobrador.getDescrevaAOperacaoDoTremNoMomentoDoDescarrilamento());

            Assert.assertEquals(getValue(inputQuantosVagoesEstavamEngatados), questionarioManobrador.getQuantosVagoesEstavamEngatadosNaLocomotivaNoMomentoDoDescarrilamento());

            if (questionarioManobrador.isEstavaSendoMovimentadoUmAMVPorUmMembroDaTripulacaoNoMomentoDoDescarrilamento()) {
                Assert.assertTrue(isRadioChecked(radioButtonEstavaSendoMovimentadoAMVSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonEstavaSendoMovimentadoAMVNao));
            }

            Assert.assertTrue(isRadioChecked(
                    radioButtonAlgumVeiculoMovimentado.apply(
                            questionarioManobrador.getAlgumDosVeiculosMovimentadosEstavaComOFreioDeMaoAplicadoIndex()
                    ))
            );

            Assert.assertTrue(isRadioChecked(
                    radioButtonFreioDosVagoesUtilizados.apply(
                            questionarioManobrador.getoFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoIndex()
                    ))
            );

            if (questionarioManobrador.getoFreioDosVagoesEstavaSendoUtilizadoParaControlarOMovimentoIndex() == 0) {
                Assert.assertEquals(getValue(inputFreioQuantosVagoes), questionarioManobrador.getEmQuantosVagoes());
            }


            Assert.assertTrue(isRadioChecked(
                    radioButtonHaviaCondicaoIrregularEstrutura.apply(
                            questionarioManobrador.getHaviaAlgumaCondicaoIrregularNaEstruturaDaViaVistaOuDetectadaNaAreaDoDescarrilamentoIndex()
                    ))
            );

            Assert.assertTrue(isRadioChecked(
                    radioButtonHaviaCondicaoIrregularEquipamento.apply(
                            questionarioManobrador.getHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoIndex()
                    ))
            );

            if (questionarioManobrador.getHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamentoIndex() == 0) {
                Assert.assertEquals(getValue(textareaExpliqueCondicaoIrregular), questionarioManobrador.getExpliqueHaviaAlgumaCondicaoIrregularNoEquipamentoOuCargaNotadaAntesDoDescarrilamento());
            }

            Assert.assertTrue(isRadioChecked(
                    radioButtonDescarrilamentoOcorreuDuranteEngate.apply(
                            questionarioManobrador.getoDescarrilamentoOcorreuDuranteOEngateIndex()
                    ))
            );

            if (questionarioManobrador.getoDescarrilamentoOcorreuDuranteOEngateIndex() == 0) {
                Assert.assertTrue(isRadioChecked(
                        radioButtonEngatesAlinhados.apply(
                                questionarioManobrador.getOsEngatesEstavamAlinhadosIndex()
                        ))
                );
            }

            Assert.assertTrue(isRadioChecked(
                    radioButtonDescarrilamentoDevidoResultadoColisao.apply(
                            questionarioManobrador.getoDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosIndex()
                    ))
            );

            if (questionarioManobrador.getoDescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2VeiculosIndex() == 0) {
                Assert.assertEquals(getValue(textareaExpliqueDevidoResultadoColisao), questionarioManobrador.getExpliqueODescarrilamentoOcorreuDevidoAoResultadoDeUmaColisaoDe2Veiculos());
            }


            Assert.assertTrue(isRadioChecked(
                    radioButtonFoiNotadaAcaoFolgasNaoComum.apply(
                            questionarioManobrador.getFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoIndex()
                    ))
            );

            if (questionarioManobrador.getFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamentoIndex() == 0) {
                Assert.assertEquals(getValue(textareaExpliqueAcaoFolga), questionarioManobrador.getExpliqueFoiNotadaUmAcaoNasFolgasNaoComumAntesDoDescarrilamento());
            }

            Assert.assertEquals(getValue(textareaOndeEstavamMaquinista), questionarioManobrador.getAondeEstavaOMaquinistaNoMomentoDoDescarrilamento());
            Assert.assertEquals(getValue(textareaOndeEstavamAjudante), questionarioManobrador.getAondeEstavaOAjudanteDeMaquinistaNoMomentoDoDescarrilamento());
            Assert.assertEquals(getValue(textareaOndeEstavamManobrador), questionarioManobrador.getAondeEstavaOManobradorAssistenteDePatioNoMomentoDoDescarrilamento());

        } else {
            Assert.assertTrue(isRadioChecked(radioButtonAcidenteDuranteManobraNao));
        }
    }
}
