package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.RegistroDoAcidente;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

import static br.com.utils.ReporterUtils.addScreenshotToReport;

public class RegistroDoAcidenteSindicanciaPage extends GeralPage {

    IntFunction<By> observacaoOutros = (index) -> By.id("observacao-outros-" + index);
    private By separatorRegistroFotografico = By.name("REGISTRO FOTOGRÁFICO");
    private By sroTitleLabelTituloBoxRegistroCROQUIACIDENTE = By.id("titulo-box-registro-CROQUIACIDENTE");
    private By divCroquiDoAcidente = By.name("CROQUI DO ACIDENTE");
    private By buttonButtonBoxAddImageCROQUIACIDENTE = By.id("button-box-add-image-CROQUIACIDENTE");
    private By textareaObservacaoCROQUIACIDENTE = By.id("observacao-CROQUIACIDENTE");
    private By sroTitleLabelTituloBoxRegistroFOTOGERALACIDENTE = By.id("titulo-box-registro-FOTOGERALACIDENTE");
    private By divFotoGeralDoAcidente = By.name("FOTO GERAL DO ACIDENTE");
    private By buttonButtonBoxAddImageFOTOGERALACIDENTE = By.id("button-box-add-image-FOTOGERALACIDENTE");
    private By textareaObservacaoFOTOGERALACIDENTE = By.id("observacao-FOTOGERALACIDENTE");
    private By sroTitleLabelTituloBoxRegistroFOTOSENTIDOCIRCULACAOMOTORISTA = By.id("titulo-box-registro-FOTOSENTIDOCIRCULACAOMOTORISTA");
    private By divFotoSentidoDeCirculacaoDoMotorista = By.name("FOTO SENTIDO DE CIRCULAÇÃO DO MOTORISTA");
    private By buttonButtonBoxAddImageFOTOSENTIDOCIRCULACAOMOTORISTA = By.id("button-box-add-image-FOTOSENTIDOCIRCULACAOMOTORISTA");
    private By textareaObservacaoFOTOSENTIDOCIRCULACAOMOTORISTA = By.id("observacao-FOTOSENTIDOCIRCULACAOMOTORISTA");
    private By sroTitleLabelTituloBoxRegistroFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA = By.id("titulo-box-registro-FOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA");
    private By divFotoSentidoContrarioCirculacaoDoMotorista = By.name("FOTO SENTIDO CONTRÁRIO CIRCULAÇÃO DO MOTORISTA");
    private By buttonButtonBoxAddImageFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA = By.id("button-box-add-image-FOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA");
    private By textareaObservacaoFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA = By.id("observacao-FOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA");
    private By sroTitleLabelTituloBoxRegistroFOTOSENTIDOCIRCULACAOMAQUINISTA = By.id("titulo-box-registro-FOTOSENTIDOCIRCULACAOMAQUINISTA");
    private By divFotoSentidoDeCirculacaoDoMaquinista = By.name("FOTO SENTIDO DE CIRCULAÇÃO DO MAQUINISTA");
    private By buttonButtonBoxAddImageFOTOSENTIDOCIRCULACAOMAQUINISTA = By.id("button-box-add-image-FOTOSENTIDOCIRCULACAOMAQUINISTA");
    private By textareaObservacaoFOTOSENTIDOCIRCULACAOMAQUINISTA = By.id("observacao-FOTOSENTIDOCIRCULACAOMAQUINISTA");
    private By sroTitleLabelTituloBoxRegistroFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA = By.id("titulo-box-registro-FOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA");
    private By divFotoSentidoContrarioCirculacaoDoMaquinista = By.name("FOTO SENTIDO CONTRÁRIO CIRCULAÇÃO DO MAQUINISTA");
    private By buttonButtonBoxAddImageFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA = By.id("button-box-add-image-FOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA");
    private By textareaObservacaoFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA = By.id("observacao-FOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA");
    private By sroTitleLabelTituloBoxRegistroDEPOIMENTOCONDUTOR = By.id("titulo-box-registro-DEPOIMENTOCONDUTOR");
    private By divDepoimentoDoCondutorDoVeiculo = By.name("DEPOIMENTO DO CONDUTOR DO VEÍCULO");
    private By buttonButtonBoxAddImageDEPOIMENTOCONDUTOR = By.id("button-box-add-image-DEPOIMENTOCONDUTOR");
    private By textareaObservacaoDEPOIMENTOCONDUTOR = By.id("observacao-DEPOIMENTOCONDUTOR");
    private By sroTitleLabelTituloBoxRegistroDEPOIMENTOTESTEMUNHAS = By.id("titulo-box-registro-DEPOIMENTOTESTEMUNHAS");
    private By divDepoimentoDasTestemunhas = By.name("DEPOIMENTO DAS TESTEMUNHAS");
    private By buttonButtonBoxAddImageDEPOIMENTOTESTEMUNHAS = By.id("button-box-add-image-DEPOIMENTOTESTEMUNHAS");
    private By textareaObservacaoDEPOIMENTOTESTEMUNHAS = By.id("observacao-DEPOIMENTOTESTEMUNHAS");
    private By buttonButtonAddImagemParte1 = By.id("button-add-imagem-parte-1");
    private By separatorDocumentosOficiais = By.name("Documentos oficiais");
    private By sroTitleLabelTituloBoxRegistroBOLETIMOCORRENCIA = By.id("titulo-box-registro-BOLETIMOCORRENCIA");
    private By divBoletimOcorrencia = By.name("BOLETIM OCORRÊNCIA");
    private By buttonButtonBoxAddImageBOLETIMOCORRENCIA = By.id("button-box-add-image-BOLETIMOCORRENCIA");
    private By textareaObservacaoBOLETIMOCORRENCIA = By.id("observacao-BOLETIMOCORRENCIA");
    private By sroTitleLabelTituloBoxRegistroLAUDOIML = By.id("titulo-box-registro-LAUDOIML");
    private By divLaudoDoIml = By.name("LAUDO DO IML");
    private By buttonButtonBoxAddImageLAUDOIML = By.id("button-box-add-image-LAUDOIML");
    private By textareaObservacaoLAUDOIML = By.id("observacao-LAUDOIML");
    private By labelCroquiAcidente = By.cssSelector("#title-label-titulo-box-registro-CROQUIACIDENTE");

    private By imgImgRegistroFotograficoCROQUIACIDENTE = By.id("img-registro-fotografico-CROQUIACIDENTE");
    private By buttonButtonBoxExcluirCROQUIACIDENTE = By.id("button-box-excluir-CROQUIACIDENTE");
    private By imgImgRegistroFotograficoFOTOGERALACIDENTE = By.id("img-registro-fotografico-FOTOGERALACIDENTE");
    private By buttonButtonBoxExcluirFOTOGERALACIDENTE = By.id("button-box-excluir-FOTOGERALACIDENTE");
    private By imgImgRegistroFotograficoFOTOSENTIDOCIRCULACAOMOTORISTA = By.id("img-registro-fotografico-FOTOSENTIDOCIRCULACAOMOTORISTA");
    private By buttonButtonBoxExcluirFOTOSENTIDOCIRCULACAOMOTORISTA = By.id("button-box-excluir-FOTOSENTIDOCIRCULACAOMOTORISTA");
    private By imgImgRegistroFotograficoFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA = By.id("img-registro-fotografico-FOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA");
    private By buttonButtonBoxExcluirFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA = By.id("button-box-excluir-FOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA");
    private By imgImgRegistroFotograficoFOTOSENTIDOCIRCULACAOMAQUINISTA = By.id("img-registro-fotografico-FOTOSENTIDOCIRCULACAOMAQUINISTA");
    private By buttonButtonBoxExcluirFOTOSENTIDOCIRCULACAOMAQUINISTA = By.id("button-box-excluir-FOTOSENTIDOCIRCULACAOMAQUINISTA");
    private By imgImgRegistroFotograficoFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA = By.id("img-registro-fotografico-FOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA");
    private By buttonButtonBoxExcluirFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA = By.id("button-box-excluir-FOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA");
    private By imgImgRegistroFotograficoDEPOIMENTOCONDUTOR = By.id("img-registro-fotografico-DEPOIMENTOCONDUTOR");
    private By buttonButtonBoxExcluirDEPOIMENTOCONDUTOR = By.id("button-box-excluir-DEPOIMENTOCONDUTOR");
    private By imgImgRegistroFotograficoDEPOIMENTOTESTEMUNHAS = By.id("img-registro-fotografico-DEPOIMENTOTESTEMUNHAS");
    private By buttonButtonBoxExcluirDEPOIMENTOTESTEMUNHAS = By.id("button-box-excluir-DEPOIMENTOTESTEMUNHAS");
    private By imgImgRegistroFotograficoBOLETIMOCORRENCIA = By.id("img-registro-fotografico-BOLETIMOCORRENCIA");
    private By buttonButtonBoxExcluirBOLETIMOCORRENCIA = By.id("button-box-excluir-BOLETIMOCORRENCIA");
    private By imgImgRegistroFotograficoLAUDOIML = By.id("img-registro-fotografico-LAUDOIML");
    private By buttonButtonBoxExcluirLAUDOIML = By.id("button-box-excluir-LAUDOIML");

    private By buttonSalvarInformacoesRegistroAcidente = By.id("salvar-informacoes-registro-acidente");
    private By buttonSalvarConcluirInformacoesRegistroAcidente = By.id("salvar-concluir-informacoes-registro-acidente");
    private By buttonCancelarInformacoesRegistroAcidente = By.id("cancelar-informacoes-registro-acidente");

    /*
    Modal upload
     */

    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By inputInputTituloImagem = By.id("input-titulo-imagem");
    private By inputInputFileUpload = By.id("input-file-upload");
    private By buttonBtnInputFileUpload = By.id("btn-input-file-upload");
    private By buttonBtnFileUploadSearch = By.id("btn-file-upload-search");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");

    /*
    Modal delete
     */

    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");

    public void adicionarArquivosEComentarios(RegistroDoAcidente registroDoAcidente) {
        uploadImageWithComentaries(buttonButtonBoxAddImageCROQUIACIDENTE, textareaObservacaoCROQUIACIDENTE,
                registroDoAcidente.getObservacaoCroquiDoAcidente());
        addScreenshotToReport("Adicionou o anexo e comentário do Croqui do Acidente");

        uploadImageWithComentaries(buttonButtonBoxAddImageFOTOGERALACIDENTE, textareaObservacaoFOTOGERALACIDENTE,
                registroDoAcidente.getObservacaoFotoGeralDoAcidente());
        addScreenshotToReport("Adicionou o anexo e comentário da Foto geral do acidente");

        uploadImageWithComentaries(buttonButtonBoxAddImageFOTOSENTIDOCIRCULACAOMOTORISTA, textareaObservacaoFOTOSENTIDOCIRCULACAOMOTORISTA,
                registroDoAcidente.getObservacaoFotoSentidoDeCirculacaoDoMotorista());
        addScreenshotToReport("Adicionou o anexo e comentário da Foto de Sentido de Circulacao do motorista.");

        uploadImageWithComentaries(buttonButtonBoxAddImageFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA, textareaObservacaoFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA,
                registroDoAcidente.getObservacaoFotoSentidoContrarioDaCirculacaoDoMotorista());
        addScreenshotToReport("Adicionou o anexo e comentário da Foto de Sentido Contrário de Circulacao do motorista.");

        uploadImageWithComentaries(buttonButtonBoxAddImageFOTOSENTIDOCIRCULACAOMAQUINISTA, textareaObservacaoFOTOSENTIDOCIRCULACAOMAQUINISTA,
                registroDoAcidente.getObservacaoFotoSentidoContrarioDaCirculacaoDoMotorista());
        addScreenshotToReport("Adicionou o anexo e comentário da Foto de Sentido Contrário de Circulacao do Maquinista.");

        uploadImageWithComentaries(buttonButtonBoxAddImageFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA, textareaObservacaoFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA,
                registroDoAcidente.getObservacaoFotoSentidoDeCirculacaoDoMaquinista());
        addScreenshotToReport("Adicionou o anexo e comentário da Foto de Sentido Contrario de Circulacao do Maquinista.");

        uploadImageWithComentaries(buttonButtonBoxAddImageDEPOIMENTOCONDUTOR, textareaObservacaoDEPOIMENTOCONDUTOR,
                registroDoAcidente.getObservacaoDepoimentoDoCondutorDoVeiculo());
        addScreenshotToReport("Adicionou o anexo e comentário do Depoimento do condutor do veículo.");

        uploadImageWithComentaries(buttonButtonBoxAddImageDEPOIMENTOTESTEMUNHAS, textareaObservacaoDEPOIMENTOTESTEMUNHAS,
                registroDoAcidente.getObservacaoDepoimentoDasTestemunhas());
        addScreenshotToReport("Adicionou o anexo e comentário do Depoimento das testemunhas.");

        uploadImageWithComentaries(buttonButtonBoxAddImageBOLETIMOCORRENCIA, textareaObservacaoBOLETIMOCORRENCIA,
                registroDoAcidente.getNumeroBoletimDeOcorrencia());
        addScreenshotToReport("Adicionou o anexo e comentário do Boletim de Ocorrência.");

        uploadImageWithComentaries(buttonButtonBoxAddImageLAUDOIML, textareaObservacaoLAUDOIML,
                registroDoAcidente.getNumeroLaudoDoIML());
        addScreenshotToReport("Adicionou o anexo e comentário do Laudo do IML.");
    }

    private void uploadImageWithComentaries(By btnUpload, By textAreaComentario, String comentario) {
        scrollToElement(btnUpload);
        clickAndHighlight(btnUpload);
        uploadImagem(inputInputFileUpload);
        addScreenshotToReport("Adicionou o anexo");
        clickAndHighlight(buttonDialogButtonYes);
        validaMensagemSnackBar("Registro do acidente salvo com sucesso");
        sendKeysWithJavaScript(textAreaComentario, comentario);
    }

    public void validaExibicaoDaPagina() {
        expectElementVisible(separatorRegistroFotografico);
        expectElementVisible(separatorDocumentosOficiais);
        expectElementVisible(buttonSalvarInformacoesRegistroAcidente);
        expectElementVisible(buttonSalvarConcluirInformacoesRegistroAcidente);
        expectElementVisible(buttonCancelarInformacoesRegistroAcidente);
    }

    public void validarArquivosAdicionados(RegistroDoAcidente registroDoAcidente) {

        validaImagemEComentario(imgImgRegistroFotograficoCROQUIACIDENTE, textareaObservacaoCROQUIACIDENTE,
                registroDoAcidente.getObservacaoCroquiDoAcidente());
        addScreenshotToReport("Validei o anexo e comentário do Croqui do Acidente");

        validaImagemEComentario(imgImgRegistroFotograficoFOTOGERALACIDENTE, textareaObservacaoFOTOGERALACIDENTE,
                registroDoAcidente.getObservacaoFotoGeralDoAcidente());
        addScreenshotToReport("Validei o anexo e comentário da Foto geral do acidente");

        validaImagemEComentario(imgImgRegistroFotograficoFOTOSENTIDOCIRCULACAOMOTORISTA, textareaObservacaoFOTOSENTIDOCIRCULACAOMOTORISTA,
                registroDoAcidente.getObservacaoFotoSentidoDeCirculacaoDoMotorista());
        addScreenshotToReport("Validei o anexo e comentário da Foto de Sentido de Circulacao do motorista.");

        validaImagemEComentario(imgImgRegistroFotograficoFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA, textareaObservacaoFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA,
                registroDoAcidente.getObservacaoFotoSentidoContrarioDaCirculacaoDoMotorista());
        addScreenshotToReport("Validei o anexo e comentário da Foto de Sentido Contrário de Circulacao do motorista.");

        validaImagemEComentario(imgImgRegistroFotograficoFOTOSENTIDOCIRCULACAOMAQUINISTA, textareaObservacaoFOTOSENTIDOCIRCULACAOMAQUINISTA,
                registroDoAcidente.getObservacaoFotoSentidoContrarioDaCirculacaoDoMotorista());
        addScreenshotToReport("Validei o anexo e comentário da Foto de Sentido Contrário de Circulacao do Maquinista.");

        validaImagemEComentario(imgImgRegistroFotograficoFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA, textareaObservacaoFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA,
                registroDoAcidente.getObservacaoFotoSentidoDeCirculacaoDoMaquinista());
        addScreenshotToReport("Validei o anexo e comentário da Foto de Sentido Contrario de Circulacao do Maquinista.");

        validaImagemEComentario(imgImgRegistroFotograficoDEPOIMENTOCONDUTOR, textareaObservacaoDEPOIMENTOCONDUTOR,
                registroDoAcidente.getObservacaoDepoimentoDoCondutorDoVeiculo());
        addScreenshotToReport("Validei o anexo e comentário do Depoimento do condutor do veículo.");

        validaImagemEComentario(imgImgRegistroFotograficoDEPOIMENTOTESTEMUNHAS, textareaObservacaoDEPOIMENTOTESTEMUNHAS,
                registroDoAcidente.getObservacaoDepoimentoDasTestemunhas());
        addScreenshotToReport("Validei o anexo e comentário do Depoimento das testemunhas.");

        validaImagemEComentario(imgImgRegistroFotograficoBOLETIMOCORRENCIA, textareaObservacaoBOLETIMOCORRENCIA,
                registroDoAcidente.getNumeroBoletimDeOcorrencia());
        addScreenshotToReport("Validei o anexo e comentário do Boletim de Ocorrência.");

        validaImagemEComentario(imgImgRegistroFotograficoLAUDOIML, textareaObservacaoLAUDOIML,
                registroDoAcidente.getNumeroLaudoDoIML());
        addScreenshotToReport("Validei o anexo e comentário do Laudo do IML.");

    }

    private void validaImagemEComentario(By img, By textAreaComentario, String comentario) {
        scrollToElement(img);
        expectElementVisible(img);
        Assert.assertEquals(getValue(textAreaComentario), comentario);
    }

    public void clicarBtnSalvarInformacoes() {
        clickAndHighlight(buttonSalvarInformacoesRegistroAcidente);
    }

    public void editarComentarios(RegistroDoAcidente registroDoAcidenteEditado) {
        editComentario(textareaObservacaoCROQUIACIDENTE,
                registroDoAcidenteEditado.getObservacaoCroquiDoAcidente());
        addScreenshotToReport("Editou o comentário do Croqui do Acidente");

        editComentario(textareaObservacaoFOTOGERALACIDENTE,
                registroDoAcidenteEditado.getObservacaoFotoGeralDoAcidente());
        addScreenshotToReport("Editou o comentário da Foto geral do acidente");

        editComentario(textareaObservacaoFOTOSENTIDOCIRCULACAOMOTORISTA,
                registroDoAcidenteEditado.getObservacaoFotoSentidoDeCirculacaoDoMotorista());
        addScreenshotToReport("Editou o comentário da Foto de Sentido de Circulacao do motorista.");

        editComentario(textareaObservacaoFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA,
                registroDoAcidenteEditado.getObservacaoFotoSentidoContrarioDaCirculacaoDoMotorista());
        addScreenshotToReport("Editou o comentário da Foto de Sentido Contrário de Circulacao do motorista.");

        editComentario(textareaObservacaoFOTOSENTIDOCIRCULACAOMAQUINISTA,
                registroDoAcidenteEditado.getObservacaoFotoSentidoContrarioDaCirculacaoDoMotorista());
        addScreenshotToReport("Editou o comentário da Foto de Sentido Contrário de Circulacao do Maquinista.");

        editComentario(textareaObservacaoFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA,
                registroDoAcidenteEditado.getObservacaoFotoSentidoDeCirculacaoDoMaquinista());
        addScreenshotToReport("Editou o comentário da Foto de Sentido Contrario de Circulacao do Maquinista.");

        editComentario(textareaObservacaoDEPOIMENTOCONDUTOR,
                registroDoAcidenteEditado.getObservacaoDepoimentoDoCondutorDoVeiculo());
        addScreenshotToReport("Editou o comentário do Depoimento do condutor do veículo.");

        editComentario(textareaObservacaoDEPOIMENTOTESTEMUNHAS,
                registroDoAcidenteEditado.getObservacaoDepoimentoDasTestemunhas());
        addScreenshotToReport("Editou o comentário do Depoimento das testemunhas.");

        editComentario(textareaObservacaoBOLETIMOCORRENCIA,
                registroDoAcidenteEditado.getNumeroBoletimDeOcorrencia());
        addScreenshotToReport("Editou o comentário do Boletim de Ocorrência.");

        editComentario(textareaObservacaoLAUDOIML,
                registroDoAcidenteEditado.getNumeroLaudoDoIML());
        addScreenshotToReport("Editou o comentário do Laudo do IML.");
    }

    private void editComentario(By by, String comentario) {
        scrollToElement(by);
        sendKeysWithJavaScript(by, comentario);
    }

    public void excluirImagensEComentarios() {
        excluirImagemEComentario(buttonButtonBoxExcluirCROQUIACIDENTE, textareaObservacaoCROQUIACIDENTE);
        addScreenshotToReport("Excluiu o anexo e comentário do Croqui do Acidente");

        excluirImagemEComentario(buttonButtonBoxExcluirFOTOGERALACIDENTE, textareaObservacaoFOTOGERALACIDENTE);
        addScreenshotToReport("Excluiu o anexo e comentário da Foto geral do acidente");

        excluirImagemEComentario(buttonButtonBoxExcluirFOTOSENTIDOCIRCULACAOMOTORISTA, textareaObservacaoFOTOSENTIDOCIRCULACAOMOTORISTA);
        addScreenshotToReport("Excluiu o anexo e comentário da Foto de Sentido de Circulacao do motorista.");

        excluirImagemEComentario(buttonButtonBoxExcluirFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA, textareaObservacaoFOTOSENTIDOCONTRARIOCIRCULACAOMOTORISTA);
        addScreenshotToReport("Excluiu o anexo e comentário da Foto de Sentido Contrário de Circulacao do motorista.");

        excluirImagemEComentario(buttonButtonBoxExcluirFOTOSENTIDOCIRCULACAOMAQUINISTA, textareaObservacaoFOTOSENTIDOCIRCULACAOMAQUINISTA);
        addScreenshotToReport("Excluiu o anexo e comentário da Foto de Sentido Contrário de Circulacao do Maquinista.");

        excluirImagemEComentario(buttonButtonBoxExcluirFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA, textareaObservacaoFOTOSENTIDOCONTRARIOCIRCULACAOMAQUINISTA);
        addScreenshotToReport("Excluiu o anexo e comentário da Foto de Sentido Contrario de Circulacao do Maquinista.");

        excluirImagemEComentario(buttonButtonBoxExcluirDEPOIMENTOCONDUTOR, textareaObservacaoDEPOIMENTOCONDUTOR);
        addScreenshotToReport("Excluiu o anexo e comentário do Depoimento do condutor do veículo.");

        excluirImagemEComentario(buttonButtonBoxExcluirDEPOIMENTOTESTEMUNHAS, textareaObservacaoDEPOIMENTOTESTEMUNHAS);
        addScreenshotToReport("Excluiu o anexo e comentário do Depoimento das testemunhas.");

        excluirImagemEComentario(buttonButtonBoxExcluirBOLETIMOCORRENCIA, textareaObservacaoBOLETIMOCORRENCIA);
        addScreenshotToReport("Excluiu o anexo e comentário do Boletim de Ocorrência.");

        excluirImagemEComentario(buttonButtonBoxExcluirLAUDOIML, textareaObservacaoLAUDOIML);
        addScreenshotToReport("Excluiu o anexo e comentário do Laudo do IML.");
    }


    private void excluirImagemEComentario(By btnExcluir, By textareaComentario) {
        scrollToElement(btnExcluir);
        clickAndHighlight(btnExcluir);
        expectElementVisible(divDialogConfirmationMessage);
        expectElementVisible(buttonDialogButtonYes);
        addScreenshotToReport("Validou modal de exclusão.");
        clickAndHighlight(buttonDialogButtonYes);
        validaMensagemSnackBar("Registro excluído com sucesso");
        isReadOnly(textareaComentario);
    }

    public void clicarBtnAdicionarNovaImagem() {
        scrollToElement(buttonButtonAddImagemParte1);
        clickAndHighlight(buttonButtonAddImagemParte1);
    }

    public void preencherCamposModalNovaImagem(RegistroDoAcidente registroDoAcidente) {
        sendKeysWithJavaScript(inputInputTituloImagem, registroDoAcidente.getTituloOutros());
        uploadImagem();
    }

    public void clicarBtnSalvarModal() {
        clickAndHighlight(buttonDialogButtonYes);
    }

    public void preencherComentarioOutros(RegistroDoAcidente registroDoAcidente) {
        sendKeysWithJavaScript(observacaoOutros.apply(0), registroDoAcidente.getObservacaoOutros());
    }

    public void validoPreenchimentoDoOutros(RegistroDoAcidente registroDoAcidente) {
        Assert.assertEquals(getValue(observacaoOutros.apply(0)), registroDoAcidente.getObservacaoOutros());
    }

    public void validoQueEstouNoRegistroAcidente() {
        expectElementVisibleWithoutHighlight(labelCroquiAcidente);
    }
}

