package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.RelatorioFinal;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class PlanoDeAcaoPage extends GeralPage {

    IntFunction<By> origem = (index) -> By.id("table-cell-origemPlanoAcao-" + index);
    IntFunction<By> solicitante = (index) -> By.id("table-cell-nomeSolicitante-0" + index);
    private By separatorPlanoDeAcao = By.name("PLANO DE AÇÃO");
    private By sroTitleLabelMecanismoDeDescarrilamento = By.id("mecanismo-de-descarrilamento");
    private By divMecanismoDeDescarrilamento = By.name("Mecanismo de descarrilamento");
    private By divLabelMecanismoDeDescarrilamento = By.id("label-mecanismo-de-descarrilamento");
    private By sroTitleLabelFatoOcorrido = By.id("fato-ocorrido");
    private By divFatoOcorrido = By.name("Fato ocorrido");
    private By divLabelFatoOcorrido = By.id("label-fato-ocorrido");
    private By sroTitleLabelDonoDoAcidente = By.id("dono-do-acidente");
    private By divDonoDoAcidente = By.name("Dono do acidente");
    private By divLabelDonoDoAcidente = By.id("label-dono-do-acidente");
    private By sroExpandableTopicSeparatorCausaPrimaria = By.id("causa-primaria");
    private By divCausaPrimariaContentToggle = By.id("causa-primaria-content-toggle");
    private By labelLabelBandeiraPRIMARIA = By.id("label-bandeira-PRIMARIA");
    private By selectSelectBandeiraPRIMARIA = By.id("select-bandeira-PRIMARIA");
    private By labelLabelCausaPRIMARIA = By.id("label-causa-PRIMARIA");
    private By selectSelectCausaProvavelPRIMARIA = By.id("select-causa-provavel-PRIMARIA");
    private By labelLabelAreaResponsavelPRIMARIA = By.id("label-area-responsavel-PRIMARIA");
    private By selectSelectAreaResponsavelPRIMARIA = By.id("select-area-responsavel-PRIMARIA");
    private By inputInputPorcentagemGravidadePRIMARIA = By.id("input-porcentagem-gravidade-PRIMARIA");
    private By sroExpandableTopicSeparatorTopicElaboracaoAcoes = By.id("topic-elaboracao-acoes");
    private By divTopicElaboracaoAcoesContentToggle = By.id("topic-elaboracao-acoes-content-toggle");
    private By areaHidden = By.cssSelector("#causa-primaria-content-toggle > mat-icon");
    private By status = By.id("table-cell-status-0");
    private By buttonButtonEnviarParaAprovacao = By.id("button-enviar-para-aprovacao");

    /*
    Modal de confirmação de enviar para aprovação
     */

    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");

    public void validarInformacoesPlanoDeAcao(RelatorioFinal relatorioFinal) {
        clickAndHighlight(areaHidden);
        Assert.assertEquals(getText(divLabelMecanismoDeDescarrilamento), relatorioFinal.getMecanismoDescarrilhamentoValue());
        Assert.assertEquals(getText(divLabelFatoOcorrido), relatorioFinal.getFatoOcorrido());
        Assert.assertEquals(getText(divLabelDonoDoAcidente), relatorioFinal.getResponsavel());
        Assert.assertEquals(getText(selectSelectBandeiraPRIMARIA), relatorioFinal.getBandeiraValue());
        Assert.assertEquals(getText(selectSelectCausaProvavelPRIMARIA), relatorioFinal.getCausaValue());
        Assert.assertEquals(getText(selectSelectAreaResponsavelPRIMARIA), relatorioFinal.getAreaResponsavelValue());
    }

    public void clicarBtnEnviarParaAprovacao() {
        clickAndHighlight(buttonButtonEnviarParaAprovacao);
    }

    public void validaModalEnviarParaAprovacao(String mensagemModal) {
        Assert.assertEquals(getText(divDialogConfirmationMessage), mensagemModal);
    }

    public void clicarBtnConfirmarModalEnviarParaAprovacao() {
        clickAndHighlight(buttonDialogButtonYes);
    }
}



