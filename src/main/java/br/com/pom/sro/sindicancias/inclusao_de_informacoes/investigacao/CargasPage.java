package br.com.pom.sro.sindicancias.inclusao_de_informacoes.investigacao;

import br.com.api.model.sindicancia.investigacao.Cargas;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class CargasPage extends GeralPage {

    private By divCargasContentToggle = By.id("cargas-content-toggle");
    private By radioGroupCondicaoVagao = By.id("condicaoVagao");
    private By radioButtonCondicaoVagaoCarregado = By.id("condicaoVagao-CARREGADO");
    private By inputCondicaoVagaoCarregadoInput = By.id("condicaoVagao-CARREGADO-input");
    private By radioButtonCondicaoVagaoVazio = By.id("condicaoVagao-VAZIO");
    private By inputCondicaoVagaoVazioInput = By.id("condicaoVagao-VAZIO-input");
    private By textAreaMetodosCarregamentoCliente = By.id("metodosCarregamentoCliente");
    private By textAreaDescrevaMetodoContribuiuDesbalanceamento = By.id("descrevaMetodoContribuiuDesbalanceamento");
    private By inputDesbalanceamentoLateral = By.id("desbalanceamentoLateral");
    private By inputDesbalanceamentoLongitudinal = By.id("desbalanceamentoLongitudinal");
    private By radioGroupDemaisVagoesCargaDesloca = By.id("demaisVagoesCargaDeslocada");
    private By radioButtonDemaisVagoesCargaDeslocaSim = By.id("demaisVagoesCargaDeslocada-sim");
    private By inputDemaisVagoesCargaDeslocaSimInput = By.id("demaisVagoesCargaDeslocada-sim-input");
    private By radioButtonDemaisVagoesCargaDeslocaNao = By.id("demaisVagoesCargaDeslocada-nao");
    private By inputDemaisVagoesCargaDeslocadaNaoInput = By.id("demaisVagoesCargaDeslocada-nao-input");
    private By textAreaItensObservados = By.id("itensObservados");

    public void preencherDados(Cargas cargas) {
        waitTime(300);
        if (cargas.isCondicaoDoVagao()) {
            clickWithAction(radioButtonCondicaoVagaoCarregado);

            sendKeysWithJavaScript(textAreaMetodosCarregamentoCliente, cargas.getDescrevaQualOMetodoDeCarregamentoDoCliente());
            sendKeysWithJavaScript(textAreaDescrevaMetodoContribuiuDesbalanceamento, cargas.getDescrevaSeOMetodoContribuiuParaODesbalanceamento());
            sendKeys(inputDesbalanceamentoLateral, cargas.getDesbalanceamentoLateral());
            sendKeys(inputDesbalanceamentoLongitudinal, cargas.getDesbalanceamentoLongitudinal());

            if (cargas.isDemaisVagoesCargaDeslocados()) {
                clickWithAction(radioButtonDemaisVagoesCargaDeslocaSim);
                waitTime(100);
                sendKeysWithJavaScript(textAreaItensObservados, cargas.getDescrevaOsItensObservados());

            } else {
                clickWithAction(radioButtonDemaisVagoesCargaDeslocaNao);
            }
        } else {
            clickWithAction(radioButtonCondicaoVagaoVazio);
        }

    }

    public void validarDados(Cargas cargas) {
        scrollToElement(radioGroupCondicaoVagao);

        if (cargas.isCondicaoDoVagao()) {

            Assert.assertTrue(isRadioChecked(radioButtonCondicaoVagaoCarregado));

            Assert.assertEquals(getValue(textAreaMetodosCarregamentoCliente), cargas.getDescrevaQualOMetodoDeCarregamentoDoCliente());
            Assert.assertEquals(getValue(textAreaDescrevaMetodoContribuiuDesbalanceamento), cargas.getDescrevaSeOMetodoContribuiuParaODesbalanceamento());
            Assert.assertEquals(getValue(inputDesbalanceamentoLateral), cargas.getDesbalanceamentoLateral());
            Assert.assertEquals(getValue(inputDesbalanceamentoLongitudinal), cargas.getDesbalanceamentoLongitudinal());

            if (cargas.isDemaisVagoesCargaDeslocados()) {
                Assert.assertTrue(isRadioChecked(radioButtonDemaisVagoesCargaDeslocaSim));
                Assert.assertEquals(getValue(textAreaItensObservados), cargas.getDescrevaOsItensObservados());

            } else {
                Assert.assertTrue(isRadioChecked(radioButtonDemaisVagoesCargaDeslocaNao));
            }
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonCondicaoVagaoVazio));
        }
    }
}
