package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.AnaliseDeCausa;
import br.com.api.model.sindicancia.RelatorioFinal;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class AnaliseDeCausaPage extends GeralPage {

    private By separatorAnaliseDeCausas = By.name("ANÁLISE DE CAUSAS");
    private By sroTitleLabelMecanismoDeDescarrilamento = By.id("mecanismo-de-descarrilamento");
    private By divMecanismoDeDescarrilamento = By.name("Mecanismo de descarrilamento");
    private By divLabelMecanismoDeDescarrilamento = By.id("label-mecanismo-de-descarrilamento");
    private By sroTitleLabelFatoOcorrido = By.id("fato-ocorrido");
    private By divFatoOcorrido = By.name("Fato ocorrido");
    private By divLabelFatoOcorrido = By.id("label-fato-ocorrido");
    private By labelLabelBandeiraPRIMARIA = By.id("label-bandeira-PRIMARIA");
    private By selectSelectBandeiraPRIMARIA = By.id("select-bandeira-PRIMARIA");
    private By labelLabelCausaPRIMARIA = By.id("label-causa-PRIMARIA");
    private By selectSelectCausaPRIMARIA = By.id("select-causa-PRIMARIA");
    private By labelLabelAreaResponsavelPRIMARIA = By.id("label-area-responsavel-PRIMARIA");
    private By selectSelectAreaResponsavelPRIMARIA = By.id("select-area-responsavel-PRIMARIA");
    private By inputInputPorcentagemGravidadePRIMARIA = By.id("input-porcentagem-gravidade-PRIMARIA");
    private By divAnaliseDeCausa1 = By.name("ANÁLISE DE CAUSA 1");
    private By tableTabelaPorquesPrimaria0 = By.id("tabela-porques-primaria-0");
    private By tdTdPrimeiroPorquePrimaria0 = By.id("td-primeiro-porque-primaria-0");
    private By tdTdSegundoPorquePrimaria0 = By.id("td-segundo-porque-primaria-0");
    private By tdTdTerceiroPorquePrimaria0 = By.id("td-terceiro-porque-primaria-0");
    private By tdTdQuartoPorquePrimaria0 = By.id("td-quarto-porque-primaria-0");
    private By tdTdQuintaPorquePrimaria0 = By.id("td-quinta-porque-primaria-0");
    private By tdTdCausaIdentificadaPrimaria0 = By.id("td-causa-identificada-primaria-0");
    private By buttonButtonEditarPrimaria0 = By.id("button-editar-primaria-0");
    private By buttonButtonExcluirPrimaria0 = By.id("button-excluir-primaria-0");
    private By divAnaliseDeCausa2 = By.name("ANÁLISE DE CAUSA 2");
    private By tableTabelaPorquesPrimaria1 = By.id("tabela-porques-primaria-1");
    private By tdTdPrimeiroPorquePrimaria1 = By.id("td-primeiro-porque-primaria-1");
    private By tdTdSegundoPorquePrimaria1 = By.id("td-segundo-porque-primaria-1");
    private By tdTdTerceiroPorquePrimaria1 = By.id("td-terceiro-porque-primaria-1");
    private By tdTdQuartoPorquePrimaria1 = By.id("td-quarto-porque-primaria-1");
    private By tdTdQuintaPorquePrimaria1 = By.id("td-quinta-porque-primaria-1");
    private By tdTdCausaIdentificadaPrimaria1 = By.id("td-causa-identificada-primaria-1");
    private By buttonButtonEditarPrimaria1 = By.id("button-editar-primaria-1");
    private By buttonButtonExcluirPrimaria1 = By.id("button-excluir-primaria-1");
    private By buttonButtonAddPrimaria = By.id("button-add-primaria");
    private By buttonSalvarAnalisesCausa = By.id("salvarAnalisesCausa");
    private By buttonSalvarAvancarAnalisesCausa = By.id("salvarAvancarAnalisesCausa");
    private By buttonCancelarAnalisesCausa = By.id("cancelarAnalisesCausa");
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By textareaPorque1 = By.id("porque-1");
    private By textareaPorque2 = By.id("porque-2");
    private By textareaPorque3 = By.id("porque-3");
    private By textareaPorque4 = By.id("porque-4");
    private By textareaPorque5 = By.id("porque-5");
    private By buttonDialogAnexosSindicanciaCancelarBtn = By.id("dialog-anexos-sindicancia-cancelar-btn");
    private By buttonDialogAnexosSindicanciaSalvarBtn = By.id("dialog-anexos-sindicancia-salvar-btn");
    private By btnConfirmarExclusao = By.id("dialog-button-yes");

    public void validoPreenchimentoAnaliseDeCausa(RelatorioFinal relatorioFinal) {
        Assert.assertEquals(getText(divLabelMecanismoDeDescarrilamento), relatorioFinal.getMecanismoDescarrilhamentoValue());
        Assert.assertEquals(getText(divLabelFatoOcorrido), relatorioFinal.getFatoOcorrido());
        Assert.assertEquals(getText(selectSelectBandeiraPRIMARIA), relatorioFinal.getBandeiraValue());
        Assert.assertEquals(getText(selectSelectCausaPRIMARIA), relatorioFinal.getCausaValue());
        Assert.assertEquals(getValue(selectSelectAreaResponsavelPRIMARIA), relatorioFinal.getAreaResponsavelValue());
    }

    public void preenchoAnaliseDeCausa(AnaliseDeCausa analiseDeCausa) {
        sendKeysWithJavaScript(textareaPorque1, analiseDeCausa.getPrimeiroPorque());
        sendKeysWithJavaScript(textareaPorque2, analiseDeCausa.getSegundoPorque());
        sendKeysWithJavaScript(textareaPorque3, analiseDeCausa.getTerceiroPorque());
        sendKeysWithJavaScript(textareaPorque4, analiseDeCausa.getQuartoPorque());
        sendKeysWithJavaScript(textareaPorque5, analiseDeCausa.getQuintoPorque());
    }

    public void clicarBtnAdicionarPrimaria() {
        clickAndHighlight(buttonButtonAddPrimaria);
    }

    public void cliqueiNoBotaoSalvar() {
        clickAndHighlight(buttonDialogAnexosSindicanciaSalvarBtn);
    }

    public void cliqueiNoBotaoEditarPrimaria() {
        clickAndHighlight(buttonButtonEditarPrimaria0);
    }

    public void validoOPreenchimentoDosPorques(AnaliseDeCausa analiseDeCausa) {
        scrollToElement(tdTdPrimeiroPorquePrimaria0);
        Assert.assertEquals(getText(tdTdPrimeiroPorquePrimaria0), analiseDeCausa.getPrimeiroPorque());
        scrollToElement(tdTdSegundoPorquePrimaria0);
        Assert.assertEquals(getText(tdTdSegundoPorquePrimaria0), analiseDeCausa.getSegundoPorque());
        scrollToElement(tdTdTerceiroPorquePrimaria0);
        Assert.assertEquals(getText(tdTdTerceiroPorquePrimaria0), analiseDeCausa.getTerceiroPorque());
        scrollToElement(tdTdQuartoPorquePrimaria0);
        Assert.assertEquals(getText(tdTdQuartoPorquePrimaria0), analiseDeCausa.getQuartoPorque());
        scrollToElement(tdTdQuintaPorquePrimaria0);
        Assert.assertEquals(getText(tdTdQuintaPorquePrimaria0), analiseDeCausa.getQuintoPorque());
    }


    public void preenchoAnaliseDeCausaDois(AnaliseDeCausa analiseDeCausa) {
        sendKeysWithJavaScript(textareaPorque1, analiseDeCausa.getPrimeiroPorque());
        sendKeysWithJavaScript(textareaPorque2, analiseDeCausa.getSegundoPorque());
        sendKeysWithJavaScript(textareaPorque3, analiseDeCausa.getTerceiroPorque());
        sendKeysWithJavaScript(textareaPorque4, analiseDeCausa.getQuartoPorque());
        sendKeysWithJavaScript(textareaPorque5, analiseDeCausa.getQuintoPorque());
    }

    public void validoOPreenchimentoDosPorquesDois(AnaliseDeCausa analiseDeCausa) {
        scrollToElement(tdTdPrimeiroPorquePrimaria1);
        Assert.assertEquals(getText(tdTdPrimeiroPorquePrimaria1), analiseDeCausa.getPrimeiroPorque());
        scrollToElement(tdTdSegundoPorquePrimaria1);
        Assert.assertEquals(getText(tdTdSegundoPorquePrimaria1), analiseDeCausa.getSegundoPorque());
        scrollToElement(tdTdTerceiroPorquePrimaria1);
        Assert.assertEquals(getText(tdTdTerceiroPorquePrimaria1), analiseDeCausa.getTerceiroPorque());
        scrollToElement(tdTdQuartoPorquePrimaria1);
        Assert.assertEquals(getText(tdTdQuartoPorquePrimaria1), analiseDeCausa.getQuartoPorque());
        scrollToElement(tdTdQuintaPorquePrimaria1);
        Assert.assertEquals(getText(tdTdQuintaPorquePrimaria1), analiseDeCausa.getQuintoPorque());
    }

    public void excluirAnaliseDeCausa() {
        clickAndHighlight(buttonButtonExcluirPrimaria0);
        clickAndHighlight(btnConfirmarExclusao);
    }

    public void validoAExclusaoAnaliseDeCausa() {
        expectElementNotVisible(tableTabelaPorquesPrimaria0);
    }
}