package br.com.pom.sro.sindicancias.inclusao_de_informacoes.vagoes;

import br.com.api.model.sindicancia.MedidaDeRodas;
import br.com.api.model.sindicancia.Vagao;
import br.com.api.model.sinistro.Sinistro;
import br.com.api.model.sinistro.Veiculo;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;
import java.util.function.IntFunction;

public class VagoesSindicanciaPage extends GeralPage {

    IntFunction<By> optionInfovagaoSelectVagoesParametrizacao = (index) -> By.id("infovagao-select-vagoes-parametrizacao-option-" + index);
    IntFunction<By> optionInfovagaoBuscaVagao = (index) -> By.id("infovagao-busca-vagao-option-" + index);
    IntFunction<By> optionInfovagaoBitola = (index) -> By.id("infovagao-bitola-option-" + index);
    private By aLinkOpenModalJustificativa = By.id("link-open-modal-justificativa");
    /*
    Parametrização
     */
    private By separatorVagoes = By.name("Vagões");
    private By selectInfovagaoSelectVagoesParametrizacao = By.id("infovagao-select-vagoes-parametrizacao");

    /*
    Primeiro veículo a descarrilar
     */
    private By inputInfovagaoBuscaVagao = By.id("infovagao-busca-vagao");
    /*
    Escolha o veículo para inserir as informações
     */
    private By radioGroupInfovagaoVagoes = By.id("infovagao-vagoes");
    private By radioButtonInfovagaoVagoesDescarrilou = By.id("infovagao-vagoes-descarrilou");
    private By labelRadioButtonInfovagaoVagoesDescarrilou = By.cssSelector("#infovagao-vagoes-descarrilou > label");
    private By inputInfovagaoVagoesDescarrilouInput = By.id("infovagao-vagoes-descarrilou-input");
    private By buttonInfovagaoVagoesBtnComecarZero = By.id("infovagao-vagoes-btn-comecar-zero");
    /*
    Informações sobre o sinistro
     */
    private By separatorInformacoesSobreOSinistro = By.name("Informações sobre o sinistro");
    private By inputInfovagaoDataAcidente = By.id("infovagao-data-acidente");
    private By inputInfovagaoPrefixoTrem = By.id("infovagao-prefixo-trem");
    private By inputInfovagaoSindicancia = By.id("infovagao-sindicancia");
    private By inputInfovagaoSubdivisao = By.id("infovagao-subdivisao");
    private By inputInfovagaoKmDoPod = By.id("infovagao-km-do-pod");
    private By selectInfovagaoBitola = By.id("infovagao-bitola");
    private By radioGroupInfovagaoEstadoVagao = By.id("infovagao-estado-vagao");
    private Function<String, By> radioButtonInfovagaoEstadoVagao = (index) -> By.id("infovagao-estado-vagao-" + index);
    private By radioButtonInfovagaoEstadoVagaoVazio = By.id("infovagao-estado-vagao-vazio");
    private By inputInfovagaoEstadoVagaoVazioInput = By.id("infovagao-estado-vagao-vazio-input");
    private By radioButtonInfovagaoEstadoVagaoCarregado = By.id("infovagao-estado-vagao-carregado");
    private By inputInfovagaoEstadoVagaoCarregadoInput = By.id("infovagao-estado-vagao-carregado-input");
    private By radioButtonInfovagaoEstadoVagaoInoperante = By.id("infovagao-estado-vagao-inoperante");
    private By inputInfovagaoEstadoVagaoInoperanteInput = By.id("infovagao-estado-vagao-inoperante-input");

    /*
    DDV
    */

    private By separatorDdv = By.name("DDV");
    private By radioGroupInfovagaoHaviaDdv = By.id("infovagao-havia-ddv");
    private By radioButtonInfovagaoHaviaDdvSim = By.id("infovagao-havia-ddv-sim");
    private By inputInfovagaoHaviaDdvSimInput = By.id("infovagao-havia-ddv-sim-input");
    private By radioButtonInfovagaoHaviaDdvNao = By.id("infovagao-havia-ddv-nao");
    private By inputInfovagaoHaviaDdvNaoInput = By.id("infovagao-havia-ddv-nao-input");
    private By radioGroupInfovagaoEstavaOperacional = By.id("infovagao-estava-operacional");
    private By radioButtonInfovagaoEstavaOperacionalSim = By.id("infovagao-estava-operacional-sim");
    private By inputInfovagaoEstavaOperacionalSimInput = By.id("infovagao-estava-operacional-sim-input");
    private By radioButtonInfovagaoEstavaOperacionalNao = By.id("infovagao-estava-operacional-nao");
    private By inputInfovagaoEstavaOperacionalNaoInput = By.id("infovagao-estava-operacional-nao-input");
    private By radioGroupInfovagaoAtuou = By.id("infovagao-atuou");
    private By radioButtonInfovagaoAtuouSim = By.id("infovagao-atuou-sim");
    private By inputInfovagaoAtuouSimInput = By.id("infovagao-atuou-sim-input");
    private By radioButtonInfovagaoAtuouNao = By.id("infovagao-atuou-nao");
    private By inputInfovagaoAtuouNaoInput = By.id("infovagao-atuou-nao-input");
    private By textareaInfovagaoMotivos = By.id("infovagao-motivos");

    /*
    Condição da carga
     */

    private By divCondicaoDaCarga = By.name("Condição da carga");
    private By selectInfovagaoCondicaoCarga = By.id("infovagao-condicao-carga");
    private IntFunction<By> optionComoEstavaACarga = (index) -> By.id("infovagao-condicao-carga-option-" + index);
    private By inputInfovagaoTipoMercadoria = By.id("infovagao-tipo-mercadoria");
    private IntFunction<By> optionMercadoriaTipo = (index) -> By.id("mercadoria-option-" + index);
    private By inputInfovagaoPesoBrutoVeiculo = By.id("infovagao-peso-bruto-veiculo");
    private By radioGroupInfovagaoEtiquetadoRecomendadoOficina = By.id("infovagao-etiquetado-recomendado-oficina");
    private By radioButtonInfovagaoEtiquetadoRecomendadoOficinaSim = By.id("infovagao-etiquetado-recomendado-oficina-sim");
    private By inputInfovagaoEtiquetadoRecomendadoOficinaSimInput = By.id("infovagao-etiquetado-recomendado-oficina-sim-input");
    private By radioButtonInfovagaoEtiquetadoRecomendadoOficinaNao = By.id("infovagao-etiquetado-recomendado-oficina-nao");
    private By inputInfovagaoEtiquetadoRecomendadoOficinaNaoInput = By.id("infovagao-etiquetado-recomendado-oficina-nao-input");

    /*
    Medição
     */

    private By divMedicao = By.name("Medição");
    private By radioGroupInfovagaoPmvOficina = By.id("infovagao-pmv-oficina");
    private Function<String, By> radioButtonInfovagaoPmv = (index) -> By.id("infovagao-pmv-oficina-" + index);
    private By radioButtonInfovagaoPmvOficinaPmv = By.id("infovagao-pmv-oficina-pmv");
    private By inputInfovagaoPmvOficinaPmvInput = By.id("infovagao-pmv-oficina-pmv-input");
    private By radioButtonInfovagaoPmvOficinaPatio = By.id("infovagao-pmv-oficina-patio");
    private By inputInfovagaoPmvOficinaPatioInput = By.id("infovagao-pmv-oficina-patio-input");
    private By inputInfovagaoLocalMedicao = By.id("infovagao-local-medicao");

    /*
    Última manutenção preventiva
     */
    private By divUltimaManutencaoPreventiva = By.name("Última manutenção preventiva");
    private By inputInfovagaoUltimaManutPreventivaData = By.id("infovagao-ultima-manut-preventiva-data");
    private By inputInfovagaoUltimaManutPreventivaTipo = By.id("infovagao-ultima-manut-preventiva-tipo");
    private By inputInfovagaoUltimaManutPreventivaLocal = By.id("infovagao-ultima-manut-preventiva-local");

    /*
    Última manutenção corretiva
     */
    private By divUltimaManutencaoCorretiva = By.name("Última manutenção corretiva");
    private By inputInfovagaoUltimaManutCorretivaData = By.id("infovagao-ultima-manut-corretiva-data");
    private By inputInfovagaoUltimaManutCorretivaTipo = By.id("infovagao-ultima-manut-corretiva-tipo");
    private By inputInfovagaoUltimaManutCorretivaLocal = By.id("infovagao-ultima-manut-corretiva-local");

    /*
    PRIMEIRO RODEIRO - PRIMEIRO TRUQUE A DESCARRILAR
     */
    private By divPrimeiroRodeiroPrimeiroTruqueADescarrilar = By.name("PRIMEIRO RODEIRO - PRIMEIRO TRUQUE A DESCARRILAR");
    private By selectInfovagaoPrimeiroRodeiroDescarrilar = By.id("infovagao-primeiro-rodeiro-descarrilar");
    private IntFunction<By> optionInfovagaoPrimeiroRodeiroDescarrilar = (index) -> By.id("infovagao-primeiro-rodeiro-descarrilar-option-" + index);
    private By inputInfovagaoRodeirosDescarrilados = By.id("infovagao-rodeiros-descarrilados");
    private By checkboxInfoVagaoRodeirosDescarriladosR1 = By.id("info-vagao-rodeiros-descarrilados-r1");
    private By inputInfoVagaoRodeirosDescarriladosR1Input = By.id("info-vagao-rodeiros-descarrilados-r1-input");
    private By checkboxInfoVagaoRodeirosDescarriladosR2 = By.id("info-vagao-rodeiros-descarrilados-r2");
    private By inputInfoVagaoRodeirosDescarriladosR2Input = By.id("info-vagao-rodeiros-descarrilados-r2-input");
    private By checkboxInfoVagaoRodeirosDescarriladosR3 = By.id("info-vagao-rodeiros-descarrilados-r3");
    private By inputInfoVagaoRodeirosDescarriladosR3Input = By.id("info-vagao-rodeiros-descarrilados-r3-input");
    private By checkboxInfoVagaoRodeirosDescarriladosR4 = By.id("info-vagao-rodeiros-descarrilados-r4");
    private By inputInfoVagaoRodeirosDescarriladosR4Input = By.id("info-vagao-rodeiros-descarrilados-r4-input");
    private By selectInfovagaoEstadoRoda = By.id("infovagao-estado-roda");
    private IntFunction<By> optionInfovagaoEstadoRoda = (index) -> By.id("infovagao-estado-roda-option-" + index);
    private By selectInfovagaoFabricacaoRoda = By.id("infovagao-fabricacao-roda");
    private IntFunction<By> optionInfovagaoFabricacaoRoda = (index) -> By.id("infovagao-fabricacao-roda-option-" + index);
    private By selectInfovagaoEixo = By.id("infovagao-eixo");
    private IntFunction<By> optionInfovagaoEixo = (index) -> By.id("infovagao-eixo-option-" + index);
    private By selectInfovagaoTipoTruque = By.id("infovagao-tipo-truque");
    private IntFunction<By> optionInfovagaoTipoTruque = (index) -> By.id("infovagao-tipo-truque-option-" + index);
    private By selectInfovagaoMangaVagao = By.id("infovagao-manga-vagao");
    private IntFunction<By> optionInfovagaoMangaVagao = (index) -> By.id("infovagao-manga-vagao-option-" + index);
    private By radioGroupInfovagaoSinaisSuperAquecimento = By.id("infovagao-sinais-super-aquecimento");
    private By radioButtonInfovagaoSinaisSuperAquecimentoSim = By.id("infovagao-sinais-super-aquecimento-sim");
    private By inputInfovagaoSinaisSuperAquecimentoSimInput = By.id("infovagao-sinais-super-aquecimento-sim-input");
    private By radioButtonInfovagaoSinaisSuperAquecimentoNao = By.id("infovagao-sinais-super-aquecimento-nao");
    private By inputInfovagaoSinaisSuperAquecimentoNaoInput = By.id("infovagao-sinais-super-aquecimento-nao-input");
    private By inputInfovagaoAquecimentoMedida = By.id("infovagao-aquecimento-medida");
    private By selectInfovagaoRolamentoTipo = By.id("infovagao-rolamento-tipo");
    private IntFunction<By> optionInfovagaoRolamentoTipo = (index) -> By.id("infovagao-rolamento-tipo-option-" + index);
    private By selectInfovagaoRolamentoCor = By.id("infovagao-rolamento-cor");
    private IntFunction<By> optionInfovagaoRolamentoCor = (index) -> By.id("infovagao-rolamento-cor-option-" + index);
    private By radioGroupInfovagaoIdentificacaoEixo = By.id("infovagao-identificacao-eixo");
    private By radioButtonInfovagaoIdentificacaoEixoFita = By.id("infovagao-identificacao-eixo-fita");
    private By inputInfovagaoIdentificacaoEixoFitaInput = By.id("infovagao-identificacao-eixo-fita-input");
    private By radioButtonInfovagaoIdentificacaoEixoNumeroEixo = By.id("infovagao-identificacao-eixo-numero-eixo");
    private By inputInfovagaoIdentificacaoEixoNumeroEixoInput = By.id("infovagao-identificacao-eixo-numero-eixo-input");
    private By inputInfovagaoEixoNumeroFita = By.id("infovagao-eixo-numero-fita");
    private By inputInfovagaoEixoNumeroEixo = By.id("infovagao-eixo-numero-eixo");

    /*
    Croquis
     */
    private By divCroquis = By.name("Croquis");
    private By inputInfovagaoCroquiDrRodeirosDescarrilados = By.id("infovagao-croqui-dr-rodeiros-descarrilados");
    private By inputInfovagaoCroquiErRodeirosDescarrilados = By.id("infovagao-croqui-er-rodeiros-descarrilados");
    private By checkboxInfovagaoCroquiErRodeirosDescarriladosR1 = By.id("infovagao-croqui-er-rodeiros-descarrilados-r1");
    private By inputInfovagaoCroquiErRodeirosDescarriladosR1Input = By.id("infovagao-croqui-er-rodeiros-descarrilados-r1-input");
    private By checkboxInfovagaoCroquiErRodeirosDescarriladosR2 = By.id("infovagao-croqui-er-rodeiros-descarrilados-r2");
    private By inputInfovagaoCroquiErRodeirosDescarriladosR2Input = By.id("infovagao-croqui-er-rodeiros-descarrilados-r2-input");
    private By checkboxInfovagaoCroquiErRodeirosDescarriladosR3 = By.id("infovagao-croqui-er-rodeiros-descarrilados-r3");
    private By inputInfovagaoCroquiErRodeirosDescarriladosR3Input = By.id("infovagao-croqui-er-rodeiros-descarrilados-r3-input");
    private By checkboxInfovagaoCroquiErRodeirosDescarriladosR4 = By.id("infovagao-croqui-er-rodeiros-descarrilados-r4");
    private By inputInfovagaoCroquiErRodeirosDescarriladosR4Input = By.id("infovagao-croqui-er-rodeiros-descarrilados-r4-input");
    private By checkboxInfovagaoCroquiCabeceiraA = By.id("infovagao-croqui-cabeceira-a");
    private By inputInfovagaoCroquiCabeceiraAInput = By.id("infovagao-croqui-cabeceira-a-input");
    private By checkboxInfovagaoCroquiCabeceiraB = By.id("infovagao-croqui-cabeceira-b");
    private By inputInfovagaoCroquiCabeceiraBInput = By.id("infovagao-croqui-cabeceira-b-input");
    private By checkboxInfovagaoCroquiDrRodeirosDescarriladosR1 = By.id("infovagao-croqui-dr-rodeiros-descarrilados-r1");
    private By inputInfovagaoCroquiDrRodeirosDescarriladosR1Input = By.id("infovagao-croqui-dr-rodeiros-descarrilados-r1-input");
    private By checkboxInfovagaoCroquiDrRodeirosDescarriladosR2 = By.id("infovagao-croqui-dr-rodeiros-descarrilados-r2");
    private By inputInfovagaoCroquiDrRodeirosDescarriladosR2Input = By.id("infovagao-croqui-dr-rodeiros-descarrilados-r2-input");
    private By checkboxInfovagaoCroquiDrRodeirosDescarriladosR3 = By.id("infovagao-croqui-dr-rodeiros-descarrilados-r3");
    private By inputInfovagaoCroquiDrRodeirosDescarriladosR3Input = By.id("infovagao-croqui-dr-rodeiros-descarrilados-r3-input");
    private By checkboxInfovagaoCroquiDrRodeirosDescarriladosR4 = By.id("infovagao-croqui-dr-rodeiros-descarrilados-r4");
    private By inputInfovagaoCroquiDrRodeirosDescarriladosR4Input = By.id("infovagao-croqui-dr-rodeiros-descarrilados-r4-input");
    private By radioGroupInfovagaoCroquiSentidoDeslocamento = By.id("infovagao-croqui-sentido-deslocamento");
    private Function<String, By> radioButtonInfovagaoCroquiSentidoDeslocamentoLabel = (index) -> By.cssSelector("#infovagao-croqui-sentido-deslocamento-" + index + " > label");
    private Function<String, By> radioButtonInfovagaoCroquiSentidoDeslocamento = (index) -> By.cssSelector("#infovagao-croqui-sentido-deslocamento-" + index);
    private By radioButtonInfovagaoCroquiSentidoDeslocamentoDireita = By.id("infovagao-croqui-sentido-deslocamento-direita");
    private By inputInfovagaoCroquiSentidoDeslocamentoDireitaInput = By.id("infovagao-croqui-sentido-deslocamento-direita-input");
    private By radioButtonInfovagaoCroquiSentidoDeslocamentoEsquerda = By.id("infovagao-croqui-sentido-deslocamento-esquerda");
    private By inputInfovagaoCroquiSentidoDeslocamentoEsquerdaInput = By.id("infovagao-croqui-sentido-deslocamento-esquerda-input");

    /*
    PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO)
     */
    private By separatorPrimeiroTruqueADescarrilarInspecao = By.name("PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO)");
    private By inputInfovagaoTipoPratoPiao = By.id("infovagao-tipo-prato-piao");
    private By inputInfovagaoFolgaDiametro = By.id("infovagao-folga-diametro");
    private By inputInfovagaoFolgaVertical = By.id("infovagao-folga-vertical");
    /*
    PRATO PIÃO INFERIOR
     */
    private By divPratoPiaoInferior = By.name("PRATO PIÃO INFERIOR");
    private By inputInfovagaoPratoPiaoInferiorDiametro = By.id("infovagao-prato-piao-inferior-diametro");
    private By inputInfovagaoPratoPiaoInferiorProfundidade = By.id("infovagao-prato-piao-inferior-profundidade");

    /*
    PRATO PIÃO SUPERIOR
     */
    private By divPratoPiaoSuperior = By.name("PRATO PIÃO SUPERIOR");
    private By inputInfovagaoPratoPiaoSuperiorDiametro = By.id("infovagao-prato-piao-superior-diametro");
    private By inputInfovagaoPratoPiaoSuperiorAltura = By.id("infovagao-prato-piao-superior-altura");
    /*
    Cunha de Fricção
     */
    private By divCunhaDeFriccao = By.name("Cunha de Fricção");
    private By selectInfovagaoEstadoCunhaFriccaoDireita1 = By.id("infovagao-estado-cunha-friccao-direita1");
    private By selectInfovagaoEstadoCunhaFriccaoDireita2 = By.id("infovagao-estado-cunha-friccao-direita2");
    private By selectInfovagaoEstadoCunhaFriccaoDireita3 = By.id("infovagao-estado-cunha-friccao-direita3");
    private By selectInfovagaoEstadoCunhaFriccaoDireita4 = By.id("infovagao-estado-cunha-friccao-direita4");
    private By selectInfovagaoEstadoCunhaFriccaoEsquerdo1 = By.id("infovagao-estado-cunha-friccao-esquerdo1");
    private By selectInfovagaoEstadoCunhaFriccaoEsquerdo2 = By.id("infovagao-estado-cunha-friccao-esquerdo2");
    private By selectInfovagaoEstadoCunhaFriccaoEsquerdo3 = By.id("infovagao-estado-cunha-friccao-esquerdo3");
    private By selectInfovagaoEstadoCunhaFriccaoEsquerdo4 = By.id("infovagao-estado-cunha-friccao-esquerdo4");
    private IntFunction<By> optionInfovagaoEstadoCunhaFriccaoDireita1 = (index) -> By.id("infovagao-estado-cunha-friccao-direita1-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoCunhaFriccaoDireita2 = (index) -> By.id("infovagao-estado-cunha-friccao-direita2-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoCunhaFriccaoDireita3 = (index) -> By.id("infovagao-estado-cunha-friccao-direita3-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoCunhaFriccaoDireita4 = (index) -> By.id("infovagao-estado-cunha-friccao-direita4-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoCunhaFriccaoEsquerdo1 = (index) -> By.id("infovagao-estado-cunha-friccao-esquerdo1-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoCunhaFriccaoEsquerdo2 = (index) -> By.id("infovagao-estado-cunha-friccao-esquerdo2-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoCunhaFriccaoEsquerdo3 = (index) -> By.id("infovagao-estado-cunha-friccao-esquerdo3-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoCunhaFriccaoEsquerdo4 = (index) -> By.id("infovagao-estado-cunha-friccao-esquerdo4-option-" + index);
    /*
    Placa de Desgaste Coluna Lat.
     */
    private By divPlacaDeDesgasteColunaLat = By.name("Placa de Desgaste Coluna Lat.");
    private By selectInfovagaoEstadoPlacaDesgasteLateralDireita1 = By.id("infovagao-estado-placa-desgaste-lateral-direita1");
    private By selectInfovagaoEstadoPlacaDesgasteLateralDireita2 = By.id("infovagao-estado-placa-desgaste-lateral-direita2");
    private By selectInfovagaoEstadoPlacaDesgasteLateralDireita3 = By.id("infovagao-estado-placa-desgaste-lateral-direita3");
    private By selectInfovagaoEstadoPlacaDesgasteLateralDireita4 = By.id("infovagao-estado-placa-desgaste-lateral-direita4");
    private By selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo1 = By.id("infovagao-estado-placa-desgaste-lateral-esquerdo1");
    private By selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo2 = By.id("infovagao-estado-placa-desgaste-lateral-esquerdo2");
    private By selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo3 = By.id("infovagao-estado-placa-desgaste-lateral-esquerdo3");
    private By selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo4 = By.id("infovagao-estado-placa-desgaste-lateral-esquerdo4");
    private IntFunction<By> optionInfovagaoEstadoPlacaDesgasteLateralDireita1 = (index) -> By.id("infovagao-estado-placa-desgaste-lateral-direita1-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoPlacaDesgasteLateralDireita2 = (index) -> By.id("infovagao-estado-placa-desgaste-lateral-direita2-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoPlacaDesgasteLateralDireita3 = (index) -> By.id("infovagao-estado-placa-desgaste-lateral-direita3-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoPlacaDesgasteLateralDireita4 = (index) -> By.id("infovagao-estado-placa-desgaste-lateral-direita4-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoPlacaDesgasteLateralEsquerdo1 = (index) -> By.id("infovagao-estado-placa-desgaste-lateral-esquerdo1-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoPlacaDesgasteLateralEsquerdo2 = (index) -> By.id("infovagao-estado-placa-desgaste-lateral-esquerdo2-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoPlacaDesgasteLateralEsquerdo3 = (index) -> By.id("infovagao-estado-placa-desgaste-lateral-esquerdo3-option-" + index);
    private IntFunction<By> optionInfovagaoEstadoPlacaDesgasteLateralEsquerdo4 = (index) -> By.id("infovagao-estado-placa-desgaste-lateral-esquerdo4-option-" + index);

    /*
    BOTÕES LATERAL DO TRUQUE
     */
    private By divBotoesLateralDoTruque = By.name("BOTÕES LATERAL DO TRUQUE");
    private By inputInfovagaoBotaoLateralTruqueDireitaA = By.id("infovagao-botao-lateral-truque-direita-a");
    private By inputInfovagaoBotaoLateralTruqueEsquerdaA = By.id("infovagao-botao-lateral-truque-esquerda-a");
    private By inputInfovagaoBotaoLateralTruqueDireitaB = By.id("infovagao-botao-lateral-truque-direita-b");
    private By inputInfovagaoBotaoLateralTruqueEsquerdaB = By.id("infovagao-botao-lateral-truque-esquerda-b");
    private By inputInfovagaoBotaoLateralTruqueDiferencaA = By.id("infovagao-botao-lateral-truque-diferenca-a");
    private By inputInfovagaoBotaoLateralTruqueDiferencaB = By.id("infovagao-botao-lateral-truque-diferenca-b");

    private By divLadoDireito = By.name("Lado direito");
    private By inputInfovagaoAlturaCunhaCabALadoDireito1 = By.id("infovagao-altura-cunha-cab-a-lado-direito1");
    private By inputInfovagaoAlturaCunhaCabALadoDireito2 = By.id("infovagao-altura-cunha-cab-a-lado-direito2");
    /*
    Lado esquerdo
     */
    private By divLadoEsquerdo = By.name("Lado esquerdo");
    private By inputInfovagaoAlturaCunhaCabALadoEsquerdo1 = By.id("infovagao-altura-cunha-cab-a-lado-esquerdo1");
    private By inputInfovagaoAlturaCunhaCabALadoEsquerdo2 = By.id("infovagao-altura-cunha-cab-a-lado-esquerdo2");
    /*
    Lado direito
    */
    private By inputInfovagaoAlturaCunhaCabBLadoDireito3 = By.id("infovagao-altura-cunha-cab-b-lado-direito3");
    private By inputInfovagaoAlturaCunhaCabBLadoDireito4 = By.id("infovagao-altura-cunha-cab-b-lado-direito4");
    private By inputInfovagaoAlturaCunhaCabBLadoEsquerdo3 = By.id("infovagao-altura-cunha-cab-b-lado-esquerdo3");
    private By inputInfovagaoAlturaCunhaCabBLadoEsquerdo4 = By.id("infovagao-altura-cunha-cab-b-lado-esquerdo4");
    private By inputInfovagaoAlturaMolaTruqueExterna = By.id("infovagao-altura-mola-truque-externa");
    private By inputInfovagaoAlturaMolaTruqueIntermediaria = By.id("infovagao-altura-mola-truque-intermediaria");
    private By inputInfovagaoAlturaMolaTruqueInterna = By.id("infovagao-altura-mola-truque-interna");
    private By divLadoDireitoLd = By.name("LADO DIREITO - LD");

    private IntFunction<By> inputInfovagaoMolaExternaLadoDireitoV = (index) -> By.id("infovagao-mola-externa-lado-direito-v" + index);

    private IntFunction<By> inputInfovagaoMolaInternaLadoDireitoV = (index) -> By.id("infovagao-mola-interna-lado-direito-v" + index);

    private IntFunction<By> inputInfovagaoMolaIntermediariaLadoDireitoV = (index) -> By.id("infovagao-mola-intermediaria-lado-direito-v" + index);

    private By divLadoEsquerdoLe = By.name("LADO ESQUERDO - LE");

    private IntFunction<By> inputInfovagaoMolaExternaLadoEsquerdoV = (index) -> By.id("infovagao-mola-externa-lado-esquerdo-v" + index);
    private IntFunction<By> inputInfovagaoMolaInternaLadoEsquerdoV = (index) -> By.id("infovagao-mola-interna-lado-esquerdo-v" + index);
    private IntFunction<By> inputInfovagaoMolaIntermediariaLadoEsquerdaV = (index) -> By.id("infovagao-mola-intermediaria-lado-esquerda-v" + index);

    private By selectInfovagaoTipoAlturaMolasCunha = By.id("infovagao-tipo-altura-molas-cunha");
    private IntFunction<By> optionInfovagaoTipoAlturaMolasCunha = (index) -> By.id("infovagao-tipo-altura-molas-cunha-option-" + index);
    private By inputInfovagaoAlturaMolasCunhaDireitoMolaExterna1 = By.id("infovagao-altura-molas-cunha-direito-mola-externa1");
    private By inputInfovagaoAlturaMolasCunhaDireitoMolaExterna2 = By.id("infovagao-altura-molas-cunha-direito-mola-externa2");
    private By inputInfovagaoAlturaMolasCunhaDireitoMolaInterna1 = By.id("infovagao-altura-molas-cunha-direito-mola-interna1");
    private By inputInfovagaoAlturaMolasCunhaDireitoMolaInterna2 = By.id("infovagao-altura-molas-cunha-direito-mola-interna2");
    private By inputInfovagaoAlturaMolasCunhaEsquerdoMolaExterna1 = By.id("infovagao-altura-molas-cunha-esquerdo-mola-externa1");
    private By inputInfovagaoAlturaMolasCunhaEsquerdoMolaExterna2 = By.id("infovagao-altura-molas-cunha-esquerdo-mola-externa2");
    private By inputInfovagaoAlturaMolasCunhaEsquerdoMolaInterna1 = By.id("infovagao-altura-molas-cunha-esquerdo-mola-interna1");
    private By inputInfovagaoAlturaMolasCunhaEsquerdoMolaInterna2 = By.id("infovagao-altura-molas-cunha-esquerdo-mola-interna2");

    private IntFunction<By> inputInfovagaoAlturaMolasTruqueOutraCabExternaDireitaV = (index) -> By.id("infovagao-altura-molas-truque-outra-cab-externa-direita-v" + index);

    private IntFunction<By> inputInfovagaoAlturaMolasTruqueOutraCabInternaDireitaV = (index) -> By.id("infovagao-altura-molas-truque-outra-cab-interna-direita-v" + index);

    private IntFunction<By> inputInfovagaoAlturaMolasTruqueOutraIntermediariaLadoDireitoV = (index) -> By.id("infovagao-altura-molas-truque-outra-intermediaria-lado-direito-v" + index);

    private IntFunction<By> inputInfovagaoAlturaMolasTruqueOutraCabExternaEsquerdaV = (index) -> By.id("infovagao-altura-molas-truque-outra-cab-externa-esquerda-v" + index);

    private IntFunction<By> inputInfovagaoAlturaMolasTruqueOutraCabInternaEsquerdaV = (index) -> By.id("infovagao-altura-molas-truque-outra-cab-interna-esquerda-v" + index);

    private IntFunction<By> inputInfovagaoAlturaMolasTruqueOutraIntermediariaLadoEsquerdaV = (index) -> By.id("infovagao-altura-molas-truque-outra-intermediaria-lado-esquerda-v" + index);

    private By selectInfovagaoTipoAlturaMolasCunhaCab = By.id("infovagao-tipo-altura-molas-cunha-cab");
    private IntFunction<By> optionInfovagaoTipoAlturaMolasCunhaCab = (index) -> By.id("infovagao-tipo-altura-molas-cunha-cab-option-" + index);

    private By inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaExterna1 = By.id("infovagao-altura-molas-barber-ride-master-direito-mola-externa1");
    private By inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaExterna2 = By.id("infovagao-altura-molas-barber-ride-master-direito-mola-externa2");
    private By inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaInterna1 = By.id("infovagao-altura-molas-barber-ride-master-direito-mola-interna1");
    private By inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaInterna2 = By.id("infovagao-altura-molas-barber-ride-master-direito-mola-interna2");
    private By inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaExterna1 = By.id("infovagao-altura-molas-barber-ride-master-esquerdo-mola-externa1");
    private By inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaExterna2 = By.id("infovagao-altura-molas-barber-ride-master-esquerdo-mola-externa2");
    private By inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaInterna1 = By.id("infovagao-altura-molas-barber-ride-master-esquerdo-mola-interna1");
    private By inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaInterna2 = By.id("infovagao-altura-molas-barber-ride-master-esquerdo-mola-interna2");

    /*
    Folgas - Medidas Válidas Antes do Acidente
     */
    private By divFolgasMedidasValidasAntesDoAcidente = By.name("Folgas - Medidas Válidas Antes do Acidente");
    private By inputInfovagaoLocal = By.id("infovagao-local");
    private By inputInfovagaoData = By.id("infovagao-data");
    private By selectInfovagaoTipoManutencao = By.id("infovagao-tipo-manutencao");
    private IntFunction<By> optionInfovagaoTipoManutencao = (index) -> By.id("infovagao-tipo-manutencao-option-" + index);
    private By inputInfovagaoAmparaBalancosEsquerdaA = By.id("infovagao-ampara-balancos-esquerda-a");
    private By inputInfovagaoAmparaBalancosDireitaA = By.id("infovagao-ampara-balancos-direita-a");
    private By inputInfovagaoAmparaBalancosEsquerdaB = By.id("infovagao-ampara-balancos-esquerda-b");
    private By inputInfovagaoAmparaBalancosDireitaB = By.id("infovagao-ampara-balancos-direita-b");
    private By inputInfovagaoAmparaBalancosEsquerdaADireitaB = By.id("infovagao-ampara-balancos-esquerda-a-direita-b");
    private By inputInfovagaoAmparaBalancosDireitaAEsquerdaB = By.id("infovagao-ampara-balancos-direita-a-esquerda-b");
    private By inputInfovagaoAmparaBalancosValorAbsoluto = By.id("infovagao-ampara-balancos-valor-absoluto");

    /*
    Folgas - Medidas Após o Acidente
     */
    private By divFolgasMedidasAposOAcidente = By.name("Folgas - Medidas Após o Acidente");
    private By inputInfovagaoAmparaBalancosPosAcidenteEsquerdaA = By.id("infovagao-ampara-balancos-pos-acidente-esquerda-a");
    private By inputInfovagaoAmparaBalancosPosAcidenteDireitaA = By.id("infovagao-ampara-balancos-pos-acidente-direita-a");
    private By inputInfovagaoAmparaBalancosPosAcidenteEsquerdaB = By.id("infovagao-ampara-balancos-pos-acidente-esquerda-b");
    private By inputInfovagaoAmparaBalancosPosAcidenteDireitaB = By.id("infovagao-ampara-balancos-pos-acidente-direita-b");
    private By inputInfovagaoAmparaBalancosPosAcidenteEsquerdaADireitaB = By.id("infovagao-ampara-balancos-pos-acidente-esquerda-a-direita-b");
    private By inputInfovagaoAmparaBalancosPosAcidenteDireitaAEsquerdaB = By.id("infovagao-ampara-balancos-pos-acidente-direita-a-esquerda-b");
    private By inputInfovagaoAmparaBalancosPosAcidenteValorAbsoluto = By.id("infovagao-ampara-balancos-pos-acidente-valor-absoluto");

    /*
    Outras informações
     */
    private By divOutrasInformacoes = By.name("Outras informações");
    private By inputInfovagaoExtensaoTrafego = By.id("infovagao-extensao-trafego");
    private By selectInfovagaoModeloAmparaBalanco = By.id("infovagao-modelo-ampara-balanco");
    private IntFunction<By> optionInfovagaoModeloAmparaBalanco = (index) -> By.id("infovagao-modelo-ampara-balanco-option-" + index);

    /*
    Castanha/Tampa do Ampara Balanço
     */
    private By divCastanhatampaDoAmparaBalanco = By.name("Castanha/Tampa do Ampara Balanço");
    private By selectInfovagaoEstadoCastanha = By.id("infovagao-estado-castanha");
    private IntFunction<By> optionInfovagaoEstadoCastanha = (index) -> By.id("infovagao-estado-castanha-option-" + index);
    private By divParafusosDeFixacaoCondicao = By.name("Parafusos de fixação (condição)");
    private By inputInfovagaoChapaSuperiorParafusoAEsq = By.id("infovagao-chapa-superior-parafuso-a-esq");
    private By inputInfovagaoChapaSuperiorParafusoBEsq = By.id("infovagao-chapa-superior-parafuso-b-esq");
    private By inputInfovagaoChapaSuperiorParafusoADir = By.id("infovagao-chapa-superior-parafuso-a-dir");
    private By inputInfovagaoChapaSuperiorParafusoBDir = By.id("infovagao-chapa-superior-parafuso-b-dir");
    private By divMedidaDeDesgasteDaChapaSuperiorDoAb = By.name("Medida de desgaste da chapa superior do AB");
    private By inputInfovagaoChapaSuperiorDesgasteAEsq = By.id("infovagao-chapa-superior-desgaste-a-esq");
    private By inputInfovagaoChapaSuperiorDesgasteBEsq = By.id("infovagao-chapa-superior-desgaste-b-esq");
    private By inputInfovagaoChapaSuperiorDesgasteADir = By.id("infovagao-chapa-superior-desgaste-a-dir");
    private By inputInfovagaoChapaSuperiorDesgasteBDir = By.id("infovagao-chapa-superior-desgaste-b-dir");

    private By selectInfovagaoFreioValvula = By.id("infovagao-freio-valvula");
    private By selectInfovagaoFreioManual = By.id("infovagao-freio-manual");
    private By selectInfovagaoTorneiraAngular = By.id("infovagao-torneira-angular");
    private By selectInfovagaoDispositivo = By.id("infovagao-dispositivo");
    private By selectInfovagaoTorneiraIsolamentoFreio = By.id("infovagao-torneira-isolamento-freio");
    private By selectInfovagaoPosicaoValvulaRetentora = By.id("infovagao-posicao-valvula-retentora");

    private IntFunction<By> optionInfovagaoFreioValvula = (index) -> By.id("infovagao-freio-valvula-option-" + index);
    private IntFunction<By> optionInfovagaoFreioManual = (index) -> By.id("infovagao-freio-manual-option-" + index);
    private IntFunction<By> optionInfovagaoTorneiraAngular = (index) -> By.id("infovagao-torneira-angular-option-" + index);
    private IntFunction<By> optionInfovagaoDispositivo = (index) -> By.id("infovagao-dispositivo-option-" + index);
    private IntFunction<By> optionInfovagaoTorneiraIsolamentoFreio = (index) -> By.id("infovagao-torneira-isolamento-freio-option-" + index);
    private IntFunction<By> optionInfovagaoPosicaoValvulaRetentora = (index) -> By.id("infovagao-posicao-valvula-retentora-option-" + index);
    /*
    DDV (Detector de Descarrilamento de Vagão)
     */
    private By divDdvDetectorDeDescarrilamentoDeVagao = By.name("DDV (Detector de Descarrilamento de Vagão)");
    private By selectInfovagaoPreDescarrilamento = By.id("infovagao-pre-descarrilamento");
    private By selectInfovagaoPosDescarrilamentoDdvAcionou1 = By.id("infovagao-pos-descarrilamento-ddv-acionou1");
    private By selectInfovagaoPosDescarrilamentoDdvAcionou2 = By.id("infovagao-pos-descarrilamento-ddv-acionou2");
    private By selectInfovagaoPosDescarrilamentoDdvAcionou3 = By.id("infovagao-pos-descarrilamento-ddv-acionou3");
    private By selectInfovagaoPosDescarrilamentoDdvAcionou4 = By.id("infovagao-pos-descarrilamento-ddv-acionou4");

    private IntFunction<By> optionInfovagaoPreDescarrilamento = (index) -> By.id("infovagao-pre-descarrilamento-option-" + index);
    private IntFunction<By> optionInfovagaoPosDescarrilamentoDdvAcionou1 = (index) -> By.id("infovagao-pos-descarrilamento-ddv-acionou1-option-" + index);
    private IntFunction<By> optionInfovagaoPosDescarrilamentoDdvAcionou2 = (index) -> By.id("infovagao-pos-descarrilamento-ddv-acionou2-option-" + index);
    private IntFunction<By> optionInfovagaoPosDescarrilamentoDdvAcionou3 = (index) -> By.id("infovagao-pos-descarrilamento-ddv-acionou3-option-" + index);
    private IntFunction<By> optionInfovagaoPosDescarrilamentoDdvAcionou4 = (index) -> By.id("infovagao-pos-descarrilamento-ddv-acionou4-option-" + index);

    /*
    Longarina
     */
    private By divLongarina = By.name("Longarina");
    private By selectInfovagaoLongarinaCondicao = By.id("infovagao-longarina-condicao");
    private By selectInfovagaoLongarinaLocalizacao = By.id("infovagao-longarina-localizacao");
    private By divTravessaDoPratoDoPiaoSuperior = By.name("Travessa do Prato do Pião Superior");
    private By selectInfovagaoTravessaPratoPiaoCondicao = By.id("infovagao-travessa-prato-piao-condicao");
    private By selectInfovagaoTravessaPratoPiaoLocalizacao = By.id("infovagao-travessa-prato-piao-localizacao");
    /*
Longarina Lateral
 */
    private By divLongarinaLateral = By.name("Longarina Lateral");
    private By selectInfovagaoLongarinaLateralCondicao = By.id("infovagao-longarina-lateral-condicao");
    private By selectInfovagaoLongarinaLateralLocalizacao = By.id("infovagao-longarina-lateral-localizacao");


    private IntFunction<By> optionInfovagaoLongarinaCondicao = (index) -> By.id("infovagao-longarina-condicao-option-" + index);
    private IntFunction<By> optionInfovagaoLongarinaLocalizacao = (index) -> By.id("infovagao-longarina-localizacao-option-" + index);
    private IntFunction<By> optionInfovagaoTravessaPratoPiaoCondicao = (index) -> By.id("infovagao-travessa-prato-piao-condicao-option-" + index);
    private IntFunction<By> optionInfovagaoTravessaPratoPiaoLocalizacao = (index) -> By.id("infovagao-travessa-prato-piao-localizacao-option-" + index);
    private IntFunction<By> optionInfovagaoLongarinaLateralCondicao = (index) -> By.id("infovagao-longarina-lateral-condicao-option-" + index);
    private IntFunction<By> optionInfovagaoLongarinaLateralLocalizacao = (index) -> By.id("infovagao-longarina-lateral-localizacao-option-" + index);


    /*
    ENGATES E ACTS
     */
    private By selectInfovagaoTipoEngate = By.id("infovagao-tipo-engate");
    private By inputInfovagaoAlturaEngateA = By.id("infovagao-altura-engate-a");
    private By inputInfovagaoAlturaEngateB = By.id("infovagao-altura-engate-b");
    private By selectInfovagaoEngateNivelado = By.id("infovagao-engate-nivelado");
    private By selectInfovagaoExisteMarcaCabecaEngate = By.id("infovagao-existe-marca-cabeca-engate");

    private IntFunction<By> optionInfovagaoTipoEngate = (index) -> By.id("infovagao-tipo-engate-option-" + index);
    private IntFunction<By> optionInfovagaoEngateNivelado = (index) -> By.id("infovagao-engate-nivelado-option-" + index);
    private IntFunction<By> optionInfovagaoExisteMarcaCabecaEngate = (index) -> By.id("infovagao-existe-marca-cabeca-engate-option-" + index);


    /*
    ACT
     */
    private By divAct = By.name("ACT");
    private By selectInfovagaoModeloAct = By.id("infovagao-modelo-act");
    private IntFunction<By> optionInfovagaoModeloAct = (index) -> By.id("infovagao-modelo-act-option-" + index);
    private By radioGroupInfovagaoPossuiFolgaAct = By.id("infovagao-possui-folga-act");
    private By radioButtonInfovagaoPossuiFolgaActOption0 = By.id("infovagao-possui-folga-act-option-0");
    private By inputInfovagaoPossuiFolgaActOption0Input = By.id("infovagao-possui-folga-act-option-0-input");
    private By radioButtonInfovagaoPossuiFolgaActOption1 = By.id("infovagao-possui-folga-act-option-1");
    private By inputInfovagaoPossuiFolgaActOption1Input = By.id("infovagao-possui-folga-act-option-1-input");
    private By inputInfovagaoFolgaAct = By.id("infovagao-folga-act");

    /*
    Componentes do ACT/Engate
     */
    private By divComponentesDoActengate = By.name("Componentes do ACT/Engate");

    private By selectInfovagaoComponentesActEngate = By.id("infovagao-componentes-act-engate");
    private By selectInfovagaoComponentesActChave = By.id("infovagao-componentes-act-chave");
    private By selectInfovagaoComponentesActMandibula = By.id("infovagao-componentes-act-mandibula");
    private By selectInfovagaoComponentesActPino = By.id("infovagao-componentes-act-pino");
    private IntFunction<By> optionInfovagaoComponentesActEngate = (index) -> By.id("infovagao-componentes-act-engate-option-" + index);
    private IntFunction<By> optionInfovagaoComponentesActChave = (index) -> By.id("infovagao-componentes-act-chave-option-" + index);
    private IntFunction<By> optionInfovagaoComponentesActMandibula = (index) -> By.id("infovagao-componentes-act-mandibula-option-" + index);
    private IntFunction<By> optionInfovagaoComponentesActPino = (index) -> By.id("infovagao-componentes-act-pino-option-" + index);


    private By textareaInfovagaoObservacaoFraturas = By.id("infovagao-observacao-fraturas");
    private By radioGroupInfovagaoLocalPodMarca = By.id("infovagao-local-pod-marca");
    private By radioButtonInfovagaoLocalPodMarcaOption0 = By.id("infovagao-local-pod-marca-option-0");
    private By inputInfovagaoLocalPodMarcaOption0Input = By.id("infovagao-local-pod-marca-option-0-input");
    private By radioButtonInfovagaoLocalPodMarcaOption1 = By.id("infovagao-local-pod-marca-option-1");
    private By inputInfovagaoLocalPodMarcaOption1Input = By.id("infovagao-local-pod-marca-option-1-input");
    private By radioGroupInfovagaoLocalPodHistoricoAcidente = By.id("infovagao-local-pod-historico-acidente");
    private By radioButtonInfovagaoLocalPodHistoricoAcidenteOption0 = By.id("infovagao-local-pod-historico-acidente-option-0");
    private By inputInfovagaoLocalPodHistoricoAcidenteOption0Input = By.id("infovagao-local-pod-historico-acidente-option-0-input");
    private By radioButtonInfovagaoLocalPodHistoricoAcidenteOption1 = By.id("infovagao-local-pod-historico-acidente-option-1");
    private By inputInfovagaoLocalPodHistoricoAcidenteOption1Input = By.id("infovagao-local-pod-historico-acidente-option-1-input");
    private By radioGroupInfovagaoHaMarcaPeca = By.id("infovagao-ha-marca-peca");
    private By radioButtonInfovagaoHaMarcaPecaOption0 = By.id("infovagao-ha-marca-peca-option-0");
    private By inputInfovagaoHaMarcaPecaOption0Input = By.id("infovagao-ha-marca-peca-option-0-input");
    private By radioButtonInfovagaoHaMarcaPecaOption1 = By.id("infovagao-ha-marca-peca-option-1");
    private By inputInfovagaoHaMarcaPecaOption1Input = By.id("infovagao-ha-marca-peca-option-1-input");
    private By inputInfovagaoQualPeca = By.id("infovagao-qual-peca");

    /*
    Btn footer
     */

    private By buttonInfovagaoBtnSalvar = By.id("infovagao-btn-salvar");
    private By buttonInfovagaoBtnSalvarRascunho = By.id("infovagao-btn-salvar-rascunho");
    private By buttonInfovagaoBtnSalvarAvancar = By.id("infovagao-btn-salvar-avancar");
    private By buttonInfovagaoBtnCancelar = By.id("infovagao-btn-cancelar");

    /*
    Medida das rodas
     */

    private By btnPreencherTelaCheia = By.id("btn-preencher-medida-rodas");
    private By tableTabelaMedidasRodas = By.id("tabela-medidas-rodas");
    private By inputMedidaRodasEspessuraFrisoCabAEsq1 = By.id("medida-rodas-espessura-friso-cab-a-esq1");
    private By inputMedidaRodasEspessuraFrisoCabADir1 = By.id("medida-rodas-espessura-friso-cab-a-dir1");
    private By inputMedidaRodasEspessuraFrisoCabAEsq2 = By.id("medida-rodas-espessura-friso-cab-a-esq2");
    private By inputMedidaRodasEspessuraFrisoCabADir2 = By.id("medida-rodas-espessura-friso-cab-a-dir2");
    private By inputMedidaRodasEspessuraFrisoCabBEsq1 = By.id("medida-rodas-espessura-friso-cab-b-esq1");
    private By inputMedidaRodasEspessuraFrisoCabBDir1 = By.id("medida-rodas-espessura-friso-cab-b-dir1");
    private By inputMedidaRodasEspessuraFrisoCabBEsq2 = By.id("medida-rodas-espessura-friso-cab-b-esq2");
    private By inputMedidaRodasEspessuraFrisoCabBDir2 = By.id("medida-rodas-espessura-friso-cab-b-dir2");
    private By inputMedidaDiferencaFriso1 = By.id("medida-diferenca-friso1");
    private By inputMedidaDiferencaFriso2 = By.id("medida-diferenca-friso2");
    private By inputMedidaDiferencaFriso3 = By.id("medida-diferenca-friso3");
    private By inputMedidaDiferencaFriso4 = By.id("medida-diferenca-friso4");
    private By inputMedidaRodasAlturaFrisoCabAEsq1 = By.id("medida-rodas-altura-friso-cab-a-esq1");
    private By inputMedidaRodasAlturaFrisoCabADir1 = By.id("medida-rodas-altura-friso-cab-a-dir1");
    private By inputMedidaRodasAlturaFrisoCabAEsq2 = By.id("medida-rodas-altura-friso-cab-a-esq2");
    private By inputMedidaRodasAlturaFrisoCabADir2 = By.id("medida-rodas-altura-friso-cab-a-dir2");
    private By inputMedidaRodasAlturaFrisoCabBEsq1 = By.id("medida-rodas-altura-friso-cab-b-esq1");
    private By inputMedidaRodasAlturaFrisoCabBDir1 = By.id("medida-rodas-altura-friso-cab-b-dir1");
    private By inputMedidaRodasAlturaFrisoCabBEsq2 = By.id("medida-rodas-altura-friso-cab-b-esq2");
    private By inputMedidaRodasAlturaFrisoCabBDir2 = By.id("medida-rodas-altura-friso-cab-b-dir2");
    private By inputMedidaRodasBandagemCabAEsq1 = By.id("medida-rodas-bandagem-cab-a-esq1");
    private By inputMedidaRodasBandagemCabADir1 = By.id("medida-rodas-bandagem-cab-a-dir1");
    private By inputMedidaRodasBandagemCabAEsq2 = By.id("medida-rodas-bandagem-cab-a-esq2");
    private By inputMedidaRodasBandagemCabADir2 = By.id("medida-rodas-bandagem-cab-a-dir2");
    private By inputMedidaRodasBandagemCabBEsq1 = By.id("medida-rodas-bandagem-cab-b-esq1");
    private By inputMedidaRodasBandagemCabBDir1 = By.id("medida-rodas-bandagem-cab-b-dir1");
    private By inputMedidaRodasBandagemCabBEsq2 = By.id("medida-rodas-bandagem-cab-b-esq2");
    private By inputMedidaRodasBandagemCabBDir2 = By.id("medida-rodas-bandagem-cab-b-dir2");
    private By inputMedidaRodasLarguraCabAEsq1 = By.id("medida-rodas-largura-cab-a-esq1");
    private By inputMedidaRodasLarguraCabADir1 = By.id("medida-rodas-largura-cab-a-dir1");
    private By inputMedidaRodasLarguraCabAEsq2 = By.id("medida-rodas-largura-cab-a-esq2");
    private By inputMedidaRodasLarguraCabADir2 = By.id("medida-rodas-largura-cab-a-dir2");
    private By inputMedidaRodasLarguraCabBEsq1 = By.id("medida-rodas-largura-cab-b-esq1");
    private By inputMedidaRodasLarguraCabBDir1 = By.id("medida-rodas-largura-cab-b-dir1");
    private By inputMedidaRodasLarguraCabBEsq2 = By.id("medida-rodas-largura-cab-b-esq2");
    private By inputMedidaRodasLarguraCabBDir2 = By.id("medida-rodas-largura-cab-b-dir2");
    private By inputMedidaRodasConcavidadeCabAEsq1 = By.id("medida-rodas-concavidade-cab-a-esq1");
    private By inputMedidaRodasConcavidadeCabADir1 = By.id("medida-rodas-concavidade-cab-a-dir1");
    private By inputMedidaRodasConcavidadeCabAEsq2 = By.id("medida-rodas-concavidade-cab-a-esq2");
    private By inputMedidaRodasConcavidadeCabADir2 = By.id("medida-rodas-concavidade-cab-a-dir2");
    private By inputMedidaRodasConcavidadeCabBEsq1 = By.id("medida-rodas-concavidade-cab-b-esq1");
    private By inputMedidaRodasConcavidadeCabBDir1 = By.id("medida-rodas-concavidade-cab-b-dir1");
    private By inputMedidaRodasConcavidadeCabBEsq2 = By.id("medida-rodas-concavidade-cab-b-esq2");
    private By inputMedidaRodasConcavidadeCabBDir2 = By.id("medida-rodas-concavidade-cab-b-dir2");
    private By inputBitolaM1a1 = By.id("bitola-m1a1");
    private By inputBitolaM1a2 = By.id("bitola-m1a2");
    private By inputBitolaM1b3 = By.id("bitola-m1b3");
    private By inputBitolaM1b4 = By.id("bitola-m1b4");
    private By inputBitolaM2a1 = By.id("bitola-m2a1");
    private By inputBitolaM2a2 = By.id("bitola-m2a2");
    private By inputBitolaM2b3 = By.id("bitola-m2b3");
    private By inputBitolaM2b4 = By.id("bitola-m2b4");
    private By inputBitolaM3a1 = By.id("bitola-m3a1");
    private By inputBitolaM3a2 = By.id("bitola-m3a2");
    private By inputBitolaM3b3 = By.id("bitola-m3b3");
    private By inputBitolaM3b4 = By.id("bitola-m3b4");
    private By btnSalvarMedidaRodas = By.id("dialog-button-save");
    private By buttonButtonVoltar = By.id("button-voltar");
    private By buttonButtonProximo = By.id("button-proximo");


    public void validaQueEstaNaPaginaDeVagoes() {
        expectElementVisible(separatorVagoes);
        expectElementVisible(aLinkOpenModalJustificativa);
    }

    public void validaBtnSalvarInformacoesDisabled() {
        expectElementDisable(buttonInfovagaoBtnSalvar);
    }

    public void preencherParametrizacao(Vagao vagao) {
        vagao.setParametrizacaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoSelectVagoesParametrizacao,
                        optionInfovagaoSelectVagoesParametrizacao,
                        vagao.getParametrizacaoIndex()
                )
        );
    }

    public void escolherVagaoQueDescarrilhou(Veiculo veiculo) {
        sendKeys(inputInfovagaoBuscaVagao, veiculo.getPosicao());
        clickAndHighlight(optionInfovagaoBuscaVagao.apply(0));
    }

    public void selecionarVagaoDescarrilado() {
        clickWithAction(labelRadioButtonInfovagaoVagoesDescarrilou);
    }

    public void preencherInformacoesSobreOSinistro(Vagao vagao) {
        scrollToElement(separatorInformacoesSobreOSinistro);
        vagao.setBitolaValue(
                selectOptionAndReturnValue(
                        selectInfovagaoBitola,
                        optionInfovagaoBitola,
                        vagao.getBitolaIndex()
                )
        );

        clickWithAction(radioButtonInfovagaoEstadoVagao.apply(vagao.getEstadoDoVagao()));
    }

    public void preencherDDV(Vagao vagao) {
        scrollToElement(separatorDdv);
        if (vagao.isHaviaDDVNoLocal()) {
            clickWithAction(radioButtonInfovagaoHaviaDdvSim);
            if (vagao.isEstavaOperacional()) {
                clickWithAction(radioButtonInfovagaoEstavaOperacionalSim);
                if (vagao.isAtuou()) {
                    clickWithAction(radioButtonInfovagaoAtuouSim);
                } else {
                    clickWithAction(radioButtonInfovagaoAtuouNao);
                }
            } else {
                clickWithAction(radioButtonInfovagaoEstavaOperacionalNao);
            }
        } else {
            clickWithAction(radioButtonInfovagaoHaviaDdvNao);
        }
        sendKeysWithJavaScript(textareaInfovagaoMotivos, vagao.getComentarioDDV());
    }

    public void preencherPrimeiroVeiculoADescarrilar(Vagao vagao) {

        scrollToElement(selectInfovagaoCondicaoCarga);
        vagao.setComoEstavaACargaValue(
                selectOptionAndReturnValue(
                        selectInfovagaoCondicaoCarga,
                        optionComoEstavaACarga,
                        vagao.getComoEstavaACargaIndex()
                )
        );

        sendKeys(inputInfovagaoTipoMercadoria, vagao.getTipoDeMercadoria());
        expectText(optionMercadoriaTipo.apply(0), vagao.getTipoDeMercadoria());
        clickAndHighlight(optionMercadoriaTipo.apply(0));
        sendKeys(inputInfovagaoPesoBrutoVeiculo, vagao.getPesoBrutoDoVeiculo());
        if (vagao.isEtiquetadoOuRecomendadoOficina()) {
            clickWithAction(radioButtonInfovagaoEtiquetadoRecomendadoOficinaSim);
        } else {
            clickWithAction(radioButtonInfovagaoEtiquetadoRecomendadoOficinaNao);
        }

        /*
        Medição
         */
        clickWithAction(radioButtonInfovagaoPmv.apply(vagao.getMedicaoRealizada()));
        sendKeysWithJavaScript(inputInfovagaoLocalMedicao, vagao.getLocalDaMedicao());

        /*
        ÚLTIMA MANUTENÇÃO PREVENTIVA
         */

        clearForce(inputInfovagaoUltimaManutPreventivaData);
        sendKeys(inputInfovagaoUltimaManutPreventivaData, vagao.getDataUltimaManutencaoPreventiva());
        sendKeysWithJavaScript(inputInfovagaoUltimaManutPreventivaTipo, vagao.getTipoUltimaManutencaoPreventiva());
        sendKeysWithJavaScript(inputInfovagaoUltimaManutPreventivaLocal, vagao.getLocalUltimaManutencaoPreventiva());

        /*
        ÚLTIMA MANUTENÇÃO CORRETIVA
         */
        clearForce(inputInfovagaoUltimaManutCorretivaData);
        sendKeys(inputInfovagaoUltimaManutCorretivaData, vagao.getDataUltimaManutencaoCorretiva());
        sendKeysWithJavaScript(inputInfovagaoUltimaManutCorretivaTipo, vagao.getTratativaUltimaManutencaoCorretiva());
        sendKeysWithJavaScript(inputInfovagaoUltimaManutCorretivaLocal, vagao.getLocalUltimaManutencaoPreventiva());

        /*
        PRIMEIRO RODEIRO - PRIMEIRO TRUQUE A DESCARRILAR
         */

        vagao.setPrimeiroRodeiroADescarrilarPosicaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoPrimeiroRodeiroDescarrilar,
                        optionInfovagaoPrimeiroRodeiroDescarrilar,
                        vagao.getPrimeiroRodeiroADescarrilarPosicaoIndex()
                )
        );

        if ((vagao.isRodeirosDescarriladosR1() && !isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR1))
                || (!vagao.isRodeirosDescarriladosR1() && isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR1))) {
            clickAndHighlight(checkboxInfoVagaoRodeirosDescarriladosR1);
        }
        if ((vagao.isRodeirosDescarriladosR2() && !isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR2))
                || (!vagao.isRodeirosDescarriladosR2() && isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR2))) {
            clickAndHighlight(checkboxInfoVagaoRodeirosDescarriladosR2);
        }
        if ((vagao.isRodeirosDescarriladosR3() && !isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR3))
                || (!vagao.isRodeirosDescarriladosR3() && isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR3))) {
            clickAndHighlight(checkboxInfoVagaoRodeirosDescarriladosR3);
        }
        if ((vagao.isRodeirosDescarriladosR4() && !isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR4))
                || (!vagao.isRodeirosDescarriladosR4() && isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR4))) {
            clickAndHighlight(checkboxInfoVagaoRodeirosDescarriladosR4);
        }

        vagao.setSituacaoDaRodaValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoRoda,
                        optionInfovagaoEstadoRoda,
                        vagao.getSituacaoDaRodaIndex()
                )
        );

        vagao.setFabricacaoDaRodaValue(
                selectOptionAndReturnValue(
                        selectInfovagaoFabricacaoRoda,
                        optionInfovagaoFabricacaoRoda,
                        vagao.getFabricacaoDaRodaIndex()
                )
        );

        vagao.setEixoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEixo,
                        optionInfovagaoEixo,
                        vagao.getEixoIndex()
                )
        );

        vagao.setTipoDeTruqueValue(
                selectOptionAndReturnValue(
                        selectInfovagaoTipoTruque,
                        optionInfovagaoTipoTruque,
                        vagao.getTipoDeTruqueIndex()
                )
        );

        vagao.setMangaDoVagaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoMangaVagao,
                        optionInfovagaoMangaVagao,
                        vagao.getMangaDoVagaoIndex()
                )
        );

        if (vagao.isSinaisDeSuperAquecimentoNaRoda()) {
            clickWithAction(radioButtonInfovagaoSinaisSuperAquecimentoSim);
            sendKeys(inputInfovagaoAquecimentoMedida, vagao.getMedidaAquecimento());
        } else {
            clickWithAction(radioButtonInfovagaoSinaisSuperAquecimentoNao);
        }

        vagao.setTipoDoRolamentoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoRolamentoTipo,
                        optionInfovagaoRolamentoTipo,
                        vagao.getTipoDoRolamentoIndex()
                )
        );

        vagao.setCorLubrificacaoDoRolamentoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoRolamentoCor,
                        optionInfovagaoRolamentoCor,
                        vagao.getCorLubrificacaoDoRolamentoIndex()
                )
        );

        if (vagao.getIdentificacaoDoEixo().equalsIgnoreCase("fita")) {
            clickWithAction(radioButtonInfovagaoIdentificacaoEixoFita);
            sendKeys(inputInfovagaoEixoNumeroFita, vagao.getNumeroDaFITA());
        } else {
            clickWithAction(radioButtonInfovagaoIdentificacaoEixoNumeroEixo);
            sendKeys(inputInfovagaoEixoNumeroEixo, vagao.getNumeroDoEixo());
        }

        /*
        CROQUIS
         */

        if ((vagao.isInfovagaoCroquiCabeceiraA() && !isCheckboxChecked(checkboxInfovagaoCroquiCabeceiraA))
                || (!vagao.isInfovagaoCroquiCabeceiraA() && isCheckboxChecked(checkboxInfovagaoCroquiCabeceiraA))) {
            clickAndHighlight(checkboxInfovagaoCroquiCabeceiraA);
        }

        if ((vagao.isInfovagaoCroquiCabeceiraB() && !isCheckboxChecked(checkboxInfovagaoCroquiCabeceiraB))
                || (!vagao.isInfovagaoCroquiCabeceiraB() && isCheckboxChecked(checkboxInfovagaoCroquiCabeceiraB))) {
            clickAndHighlight(checkboxInfovagaoCroquiCabeceiraB);
        }

        /*
        Lado esquerdo
         */

        if ((vagao.isInfovagaoCroquiErRodeirosDescarriladosR1() && !isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR1))
                || (!vagao.isInfovagaoCroquiErRodeirosDescarriladosR1() && isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR1))) {
            clickAndHighlight(checkboxInfovagaoCroquiErRodeirosDescarriladosR1);
        }

        if ((vagao.isInfovagaoCroquiErRodeirosDescarriladosR2() && !isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR2))
                || (!vagao.isInfovagaoCroquiErRodeirosDescarriladosR2() && isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR2))) {
            clickAndHighlight(checkboxInfovagaoCroquiErRodeirosDescarriladosR2);
        }

        if ((vagao.isInfovagaoCroquiErRodeirosDescarriladosR3() && !isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR3))
                || (!vagao.isInfovagaoCroquiErRodeirosDescarriladosR3() && isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR3))) {
            clickAndHighlight(checkboxInfovagaoCroquiErRodeirosDescarriladosR3);
        }

        if ((vagao.isInfovagaoCroquiErRodeirosDescarriladosR4() && !isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR4))
                || (!vagao.isInfovagaoCroquiErRodeirosDescarriladosR4() && isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR4))) {
            clickAndHighlight(checkboxInfovagaoCroquiErRodeirosDescarriladosR4);
        }
        /*
        Lado direito
         */

        if ((vagao.isInfovagaoCroquiDrRodeirosDescarriladosR1() && !isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR1))
                || (!vagao.isInfovagaoCroquiDrRodeirosDescarriladosR1() && isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR1))) {
            clickAndHighlight(checkboxInfovagaoCroquiDrRodeirosDescarriladosR1);
        }

        if ((vagao.isInfovagaoCroquiDrRodeirosDescarriladosR2() && !isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR2))
                || (!vagao.isInfovagaoCroquiDrRodeirosDescarriladosR2() && isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR2))) {
            clickAndHighlight(checkboxInfovagaoCroquiDrRodeirosDescarriladosR2);
        }

        if ((vagao.isInfovagaoCroquiDrRodeirosDescarriladosR3() && !isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR3))
                || (!vagao.isInfovagaoCroquiDrRodeirosDescarriladosR3() && isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR3))) {
            clickAndHighlight(checkboxInfovagaoCroquiDrRodeirosDescarriladosR3);
        }

        if ((vagao.isInfovagaoCroquiDrRodeirosDescarriladosR4() && !isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR4))
                || (!vagao.isInfovagaoCroquiDrRodeirosDescarriladosR4() && isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR4))) {
            clickAndHighlight(checkboxInfovagaoCroquiDrRodeirosDescarriladosR4);
        }

        clickWithAction(radioButtonInfovagaoCroquiSentidoDeslocamentoLabel.apply(vagao.getMarcarOSentidoDoDeslocamentoDoVagao()));
    }

    public void preencherPrimeiroTruqueADescarrilar(Vagao vagao) {
        /*
        PRATO PIÃO
         */
        sendKeys(inputInfovagaoTipoPratoPiao, vagao.getTipoPratoPiao());
        sendKeys(inputInfovagaoFolgaDiametro, vagao.getFolgaNoDiametro());
        sendKeys(inputInfovagaoFolgaVertical, vagao.getFolgaVertical());

        /*
        PRATO PIÃO INFERIOR
         */

        sendKeys(inputInfovagaoPratoPiaoInferiorDiametro, vagao.getDiametroPratoPiaoInferior());
        sendKeys(inputInfovagaoPratoPiaoInferiorProfundidade, vagao.getProfundidadePratoPiaoInferior());

        /*
        PRATO PIÃO SUPERIOR
         */
        scrollToElement(divPratoPiaoSuperior);
        sendKeys(inputInfovagaoPratoPiaoSuperiorDiametro, vagao.getDiametroPratoPiaoSuperior());
        sendKeys(inputInfovagaoPratoPiaoSuperiorAltura, vagao.getDiametroPratoPiaoSuperior());

        /*
        CONDIÇÃO DE CUNHA / CHAPA DE DESGASTE
         */

        vagao.setDireita1CunhaDeFriccaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoCunhaFriccaoDireita1,
                        optionInfovagaoEstadoCunhaFriccaoDireita1,
                        vagao.getDireita1CunhaDeFriccaoIndex()
                )
        );

        vagao.setDireita2CunhaDeFriccaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoCunhaFriccaoDireita2,
                        optionInfovagaoEstadoCunhaFriccaoDireita2,
                        vagao.getDireita2CunhaDeFriccaoIndex()
                )
        );

        vagao.setDireita3CunhaDeFriccaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoCunhaFriccaoDireita3,
                        optionInfovagaoEstadoCunhaFriccaoDireita3,
                        vagao.getDireita3CunhaDeFriccaoIndex()
                )
        );

        vagao.setDireita4CunhaDeFriccaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoCunhaFriccaoDireita4,
                        optionInfovagaoEstadoCunhaFriccaoDireita4,
                        vagao.getDireita4CunhaDeFriccaoIndex()
                )
        );
        /*
        esquerda
         */

        vagao.setEsquerda1CunhaDeFriccaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoCunhaFriccaoEsquerdo1,
                        optionInfovagaoEstadoCunhaFriccaoEsquerdo1,
                        vagao.getEsquerda1CunhaDeFriccaoIndex()
                )
        );

        vagao.setEsquerda2CunhaDeFriccaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoCunhaFriccaoEsquerdo2,
                        optionInfovagaoEstadoCunhaFriccaoEsquerdo2,
                        vagao.getEsquerda2CunhaDeFriccaoIndex()
                )
        );

        vagao.setEsquerda3CunhaDeFriccaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoCunhaFriccaoEsquerdo3,
                        optionInfovagaoEstadoCunhaFriccaoEsquerdo3,
                        vagao.getEsquerda3CunhaDeFriccaoIndex()
                )
        );

        vagao.setEsquerda4CunhaDeFriccaoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoCunhaFriccaoEsquerdo4,
                        optionInfovagaoEstadoCunhaFriccaoEsquerdo4,
                        vagao.getEsquerda4CunhaDeFriccaoIndex()
                )
        );


        /*
        PLACA DE DESGASTE COLUNA LAT.

        Direita
         */

        vagao.setDireita1PlacaDeDesgasteColunaLatValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoPlacaDesgasteLateralDireita1,
                        optionInfovagaoEstadoPlacaDesgasteLateralDireita1,
                        vagao.getDireita1PlacaDeDesgasteColunaLatIndex()
                )
        );

        vagao.setDireita2PlacaDeDesgasteColunaLatValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoPlacaDesgasteLateralDireita2,
                        optionInfovagaoEstadoPlacaDesgasteLateralDireita2,
                        vagao.getDireita2PlacaDeDesgasteColunaLatIndex()
                )
        );

        vagao.setDireita3PlacaDeDesgasteColunaLatValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoPlacaDesgasteLateralDireita3,
                        optionInfovagaoEstadoPlacaDesgasteLateralDireita3,
                        vagao.getDireita3PlacaDeDesgasteColunaLatIndex()
                )
        );

        vagao.setDireita4PlacaDeDesgasteColunaLatValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoPlacaDesgasteLateralDireita4,
                        optionInfovagaoEstadoPlacaDesgasteLateralDireita4,
                        vagao.getDireita4PlacaDeDesgasteColunaLatIndex()
                )
        );

        /*
            PLACA DE DESGASTE COLUNA LAT.
        esquerda
         */
        vagao.setEsquerda1PlacaDeDesgasteColunaLatValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo1,
                        optionInfovagaoEstadoPlacaDesgasteLateralEsquerdo1,
                        vagao.getEsquerda1PlacaDeDesgasteColunaLatIndex()
                )
        );

        vagao.setEsquerda2PlacaDeDesgasteColunaLatValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo2,
                        optionInfovagaoEstadoPlacaDesgasteLateralEsquerdo2,
                        vagao.getEsquerda2PlacaDeDesgasteColunaLatIndex()
                )
        );

        vagao.setEsquerda3PlacaDeDesgasteColunaLatValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo3,
                        optionInfovagaoEstadoPlacaDesgasteLateralEsquerdo3,
                        vagao.getEsquerda3PlacaDeDesgasteColunaLatIndex()
                )
        );

        vagao.setEsquerda4PlacaDeDesgasteColunaLatValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo4,
                        optionInfovagaoEstadoPlacaDesgasteLateralEsquerdo4,
                        vagao.getEsquerda4PlacaDeDesgasteColunaLatIndex()
                )
        );

        /*
        BOTÕES LATERAL DO TRUQUE
         */

        sendKeys(inputInfovagaoBotaoLateralTruqueDireitaA, vagao.getDireitaABotoesLateralDoTruque());
        sendKeys(inputInfovagaoBotaoLateralTruqueEsquerdaA, vagao.getEsquerdaABotoesLateralDoTruque());
        sendKeys(inputInfovagaoBotaoLateralTruqueDireitaB, vagao.getDireitaBBotoesLateralDoTruque());
        sendKeys(inputInfovagaoBotaoLateralTruqueEsquerdaB, vagao.getEsquerdaBBotoesLateralDoTruque());

        /*
        ALTURA DAS CUNHAS CAB. A
         */

        sendKeys(inputInfovagaoAlturaCunhaCabALadoDireito1, vagao.getAlturaDasCunhasCabALadoDiteiro1());
        sendKeys(inputInfovagaoAlturaCunhaCabALadoDireito2, vagao.getAlturaDasCunhasCabALadoDiteiro2());
        sendKeys(inputInfovagaoAlturaCunhaCabALadoEsquerdo1, vagao.getAlturaDasCunhasCabALadoEsquerdo1());
        sendKeys(inputInfovagaoAlturaCunhaCabALadoEsquerdo2, vagao.getAlturaDasCunhasCabALadoEsquerdo2());
        /*
        ALTURA DAS CUNHAS CAB. B
         */

        sendKeys(inputInfovagaoAlturaCunhaCabBLadoDireito3, vagao.getAlturaDasCunhasCabBLadoDiteiro3());
        sendKeys(inputInfovagaoAlturaCunhaCabBLadoDireito4, vagao.getAlturaDasCunhasCabBLadoDiteiro4());
        sendKeys(inputInfovagaoAlturaCunhaCabBLadoEsquerdo3, vagao.getAlturaDasCunhasCabBLadoEsquerdo3());
        sendKeys(inputInfovagaoAlturaCunhaCabBLadoEsquerdo4, vagao.getAlturaDasCunhasCabBLadoEsquerdo4());

        /*
        ALTURA DAS MOLAS (DO TRUQUE DESCARRILADO)
         */

        sendKeys(inputInfovagaoAlturaMolaTruqueExterna, vagao.getExternaAlturaDasMolasDoTruqueDescarrilado());
        sendKeys(inputInfovagaoAlturaMolaTruqueIntermediaria, vagao.getIntermediariaAlturaDasMolasDoTruqueDescarrilado());
        sendKeys(inputInfovagaoAlturaMolaTruqueInterna, vagao.getInternaAlturaDasMolasDoTruqueDescarrilado());

        /*
        LADO DIREITO - LD
         */

        final int NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS = 8;
        final int NUMERO_DE_CAMPOS_INTERMEDIARIAS = 4;

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD",
                inputInfovagaoMolaExternaLadoDireitoV
        );

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD",
                inputInfovagaoMolaInternaLadoDireitoV
        );

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_INTERMEDIARIAS,
                "getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD",
                inputInfovagaoMolaIntermediariaLadoDireitoV
        );

        /*
        LADO ESQUERDO - LE
         */

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE",
                inputInfovagaoMolaExternaLadoEsquerdoV
        );

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE",
                inputInfovagaoMolaInternaLadoEsquerdoV
        );

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_INTERMEDIARIAS,
                "getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE",
                inputInfovagaoMolaIntermediariaLadoEsquerdaV
        );

        /*
        ALTURA DAS MOLAS DAS CUNHAS
         */

        vagao.setSobreAlturaDasMolasCunhasValue(
                selectOptionAndReturnValue(
                        selectInfovagaoTipoAlturaMolasCunha,
                        optionInfovagaoTipoAlturaMolasCunha,
                        vagao.getSobreAlturaDasMolasCunhasIndex()
                )
        );

        sendKeys(inputInfovagaoAlturaMolasCunhaDireitoMolaExterna1, vagao.getMolasExternasAlturaDasMolasCunhasLadoDireitoLD1());
        sendKeys(inputInfovagaoAlturaMolasCunhaDireitoMolaExterna2, vagao.getMolasExternasAlturaDasMolasCunhasLadoDireitoLD2());
        sendKeys(inputInfovagaoAlturaMolasCunhaDireitoMolaInterna1, vagao.getMolasInternasAlturaDasMolasCunhasLadoDireitoLD1());
        sendKeys(inputInfovagaoAlturaMolasCunhaDireitoMolaInterna2, vagao.getMolasInternasAlturaDasMolasCunhasLadoDireitoLD2());
        sendKeys(inputInfovagaoAlturaMolasCunhaEsquerdoMolaExterna1, vagao.getMolasExternasAlturaDasMolasCunhasLadoEsquerdoLE1());
        sendKeys(inputInfovagaoAlturaMolasCunhaEsquerdoMolaExterna2, vagao.getMolasExternasAlturaDasMolasCunhasLadoEsquerdoLE2());
        sendKeys(inputInfovagaoAlturaMolasCunhaEsquerdoMolaInterna1, vagao.getMolasInternasAlturaDasMolasCunhasLadoEsquerdoLE1());
        sendKeys(inputInfovagaoAlturaMolasCunhaEsquerdoMolaInterna2, vagao.getMolasInternasAlturaDasMolasCunhasLadoEsquerdoLE2());

        /*
        ALTURA DAS MOLAS - TRUQUE DA OUTRA CABECEIRA
         */


        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD",
                inputInfovagaoAlturaMolasTruqueOutraCabExternaDireitaV
        );

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD",
                inputInfovagaoAlturaMolasTruqueOutraCabInternaDireitaV
        );

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_INTERMEDIARIAS,
                "getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD",
                inputInfovagaoAlturaMolasTruqueOutraIntermediariaLadoDireitoV
        );

        /*
        LADO ESQUERDO - LE
         */

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE",
                inputInfovagaoAlturaMolasTruqueOutraCabExternaEsquerdaV
        );

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE",
                inputInfovagaoAlturaMolasTruqueOutraCabInternaEsquerdaV
        );

        sendKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_INTERMEDIARIAS,
                "getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE",
                inputInfovagaoAlturaMolasTruqueOutraIntermediariaLadoEsquerdaV
        );

        /*
        ALTURA DAS MOLAS DAS CUNHAS OUTRA CABECEIRA
         */

        vagao.setSobreAlturaDasMolasCunhasOutraCabeceiraValue(
                selectOptionAndReturnValue(
                        selectInfovagaoTipoAlturaMolasCunhaCab,
                        optionInfovagaoTipoAlturaMolasCunhaCab,
                        vagao.getSobreAlturaDasMolasCunhasOutraCabeceiraIndex()
                )
        );

        sendKeys(inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaExterna1, vagao.getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1());
        sendKeys(inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaExterna2, vagao.getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2());
        sendKeys(inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaInterna1, vagao.getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1());
        sendKeys(inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaInterna2, vagao.getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2());
        sendKeys(inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaExterna1, vagao.getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1());
        sendKeys(inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaExterna2, vagao.getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2());
        sendKeys(inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaInterna1, vagao.getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1());
        sendKeys(inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaInterna2, vagao.getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2());
    }

    public void preencherPrimeiroTruqueADescarrilarAmparaBalanco(Vagao vagao) {

        /*
        FOLGAS - MEDIDAS VÁLIDAS ANTES DO ACIDENTE
         */

        sendKeysWithJavaScript(inputInfovagaoLocal, vagao.getLocalFolgasMedidasValidasAntesDoAcidente());
        clearForce(inputInfovagaoData);
        sendKeys(inputInfovagaoData, vagao.getDataFolgasMedidasValidasAntesDoAcidente());

        vagao.setTipoManutencaoFolgasMedidasValidasAntesDoAcidenteValue(
                selectOptionAndReturnValue(
                        selectInfovagaoTipoManutencao,
                        optionInfovagaoTipoManutencao,
                        vagao.getTipoManutencaoFolgasMedidasValidasAntesDoAcidenteIndex()
                )
        );

        sendKeys(inputInfovagaoAmparaBalancosEsquerdaA, vagao.getaEsquerdaFolgasMedidasValidasAntesDoAcidente());
        sendKeys(inputInfovagaoAmparaBalancosDireitaA, vagao.getaDireitaFolgasMedidasValidasAntesDoAcidente());
        sendKeys(inputInfovagaoAmparaBalancosEsquerdaB, vagao.getbEsquerdaFolgasMedidasValidasAntesDoAcidente());
        sendKeys(inputInfovagaoAmparaBalancosDireitaB, vagao.getbDireitaFolgasMedidasValidasAntesDoAcidente());

        /*
        FOLGAS - MEDIDAS APÓS O ACIDENTE
         */

        sendKeys(inputInfovagaoAmparaBalancosPosAcidenteEsquerdaA, vagao.getaEsquerdaFolgasAposOAcidente());
        sendKeys(inputInfovagaoAmparaBalancosPosAcidenteDireitaA, vagao.getaDireitaFolgasAposOAcidente());
        sendKeys(inputInfovagaoAmparaBalancosPosAcidenteEsquerdaB, vagao.getbEsquerdaFolgasAposOAcidente());
        sendKeys(inputInfovagaoAmparaBalancosPosAcidenteDireitaB, vagao.getbDireitaFolgasAposOAcidente());


        /*
        OUTRAS INFORMAÇÕES
         */

        sendKeys(inputInfovagaoExtensaoTrafego, vagao.getExtensaoDoTrafegoDescarrilado());
        vagao.setModeloAmparaBalancoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoModeloAmparaBalanco,
                        optionInfovagaoModeloAmparaBalanco,
                        vagao.getModeloAmparaBalancoIndex()
                )
        );

        /*
        CASTANHA/TAMPA DO AMPARA BALANÇO
         */

        vagao.setCondicaoCastanhaTampaDoAmparaBalancoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEstadoCastanha,
                        optionInfovagaoEstadoCastanha,
                        vagao.getCondicaoCastanhaTampaDoAmparaBalancoIndex()
                )
        );

        /*
        PARAFUSOS DE FIXAÇÃO (CONDIÇÃO)
         */

        sendKeys(inputInfovagaoChapaSuperiorParafusoAEsq, vagao.getaEsquerdaParafusosDeFixacao());
        sendKeys(inputInfovagaoChapaSuperiorParafusoBEsq, vagao.getbEsquerdaParafusosDeFixacao());
        sendKeys(inputInfovagaoChapaSuperiorParafusoADir, vagao.getaDireitaFolgasAposOAcidente());
        sendKeys(inputInfovagaoChapaSuperiorParafusoBDir, vagao.getbDireitaFolgasAposOAcidente());

        /*
        MEDIDA DE DESGASTE DA CHAPA SUPERIOR DO AB
         */

        sendKeys(inputInfovagaoChapaSuperiorDesgasteAEsq, vagao.getaEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB());
        sendKeys(inputInfovagaoChapaSuperiorDesgasteBEsq, vagao.getbEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB());
        sendKeys(inputInfovagaoChapaSuperiorDesgasteADir, vagao.getaDireitaMedidasDeDesgasteDaChapaSuperiorDoAB());
        sendKeys(inputInfovagaoChapaSuperiorDesgasteBDir, vagao.getaEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB());

    }

    public void preencherPrimeiroTruqueADescarrilarFreio(Vagao vagao) {

        vagao.setValvulaDeFreioValue(
                selectOptionAndReturnValue(
                        selectInfovagaoFreioValvula,
                        optionInfovagaoFreioValvula,
                        vagao.getValvulaDeFreioIndex()
                )
        );

        vagao.setFreioManualValue(
                selectOptionAndReturnValue(
                        selectInfovagaoFreioManual,
                        optionInfovagaoFreioManual,
                        vagao.getFreioManualIndex()
                )
        );

        vagao.setTorneiraAngularEGValue(
                selectOptionAndReturnValue(
                        selectInfovagaoTorneiraAngular,
                        optionInfovagaoTorneiraAngular,
                        vagao.getTorneiraAngularEGIndex()
                )
        );

        vagao.setDispositivoVazioCarregadoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoDispositivo,
                        optionInfovagaoDispositivo,
                        vagao.getDispositivoVazioCarregadoIndex()
                )
        );

        vagao.setTorneiraIsolamentoDeFreioValue(
                selectOptionAndReturnValue(
                        selectInfovagaoTorneiraIsolamentoFreio,
                        optionInfovagaoTorneiraIsolamentoFreio,
                        vagao.getTorneiraIsolamentoDeFreioIndex()
                )
        );

        vagao.setPosicaoDaValvulaRetentoraValue(
                selectOptionAndReturnValue(
                        selectInfovagaoPosicaoValvulaRetentora,
                        optionInfovagaoPosicaoValvulaRetentora,
                        vagao.getPosicaoDaValvulaRetentoraIndex()
                )
        );
        /*
        DDV (DETECTOR DE DESCARRILAMENTO DE VAGÃO)
         */

        vagao.setPreDescarrilamentoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoPreDescarrilamento,
                        optionInfovagaoPreDescarrilamento,
                        vagao.getPreDescarrilamentoIndex()
                )
        );

        vagao.setdDVAcionouRodeiro1Value(
                selectOptionAndReturnValue(
                        selectInfovagaoPosDescarrilamentoDdvAcionou1,
                        optionInfovagaoPosDescarrilamentoDdvAcionou1,
                        vagao.getdDVAcionouRodeiro1Index()
                )
        );

        vagao.setdDVAcionouRodeiro2Value(
                selectOptionAndReturnValue(
                        selectInfovagaoPosDescarrilamentoDdvAcionou2,
                        optionInfovagaoPosDescarrilamentoDdvAcionou2,
                        vagao.getdDVAcionouRodeiro2Index()
                )
        );

        vagao.setdDVAcionouRodeiro3Value(
                selectOptionAndReturnValue(
                        selectInfovagaoPosDescarrilamentoDdvAcionou3,
                        optionInfovagaoPosDescarrilamentoDdvAcionou3,
                        vagao.getdDVAcionouRodeiro3Index()
                )
        );

        vagao.setdDVAcionouRodeiro4Value(
                selectOptionAndReturnValue(
                        selectInfovagaoPosDescarrilamentoDdvAcionou4,
                        optionInfovagaoPosDescarrilamentoDdvAcionou4,
                        vagao.getdDVAcionouRodeiro4Index()
                )
        );

    }

    public void preencherPrimeiroTruqueADescarrilarEstruturaDoVagao(Vagao vagao) {

        /*
        LONGARINA
         */

        vagao.setCondicaoLongarinaValue(
                selectOptionAndReturnValue(
                        selectInfovagaoLongarinaCondicao,
                        optionInfovagaoLongarinaCondicao,
                        vagao.getCondicaoLongarinaIndex()
                )
        );

        vagao.setLocalizacaoLongarinaValue(
                selectOptionAndReturnValue(
                        selectInfovagaoLongarinaLocalizacao,
                        optionInfovagaoLongarinaLocalizacao,
                        vagao.getLocalizacaoLongarinaIndex()
                )
        );

        /*
        TRAVESSA DO PRATO DO PIÃO SUPERIOR
         */

        vagao.setCondicaoTravessaDoPratoDoPiaoSuperiorValue(
                selectOptionAndReturnValue(
                        selectInfovagaoTravessaPratoPiaoCondicao,
                        optionInfovagaoTravessaPratoPiaoCondicao,
                        vagao.getCondicaoTravessaDoPratoDoPiaoSuperiorIndex()
                )
        );

        vagao.setLocalizacaoTravessaDoPratoDoPiaoSuperiorValue(
                selectOptionAndReturnValue(
                        selectInfovagaoTravessaPratoPiaoLocalizacao,
                        optionInfovagaoTravessaPratoPiaoLocalizacao,
                        vagao.getLocalizacaoTravessaDoPratoDoPiaoSuperiorIndex()
                )
        );

        /*
        LONGARINA LATERAL
         */

        vagao.setCondicaoLongarinaLateralValue(
                selectOptionAndReturnValue(
                        selectInfovagaoLongarinaLateralCondicao,
                        optionInfovagaoLongarinaLateralCondicao,
                        vagao.getCondicaoLongarinaLateralIndex()
                )
        );

        vagao.setLocalizacaoLongarinaLateralValue(
                selectOptionAndReturnValue(
                        selectInfovagaoLongarinaLateralLocalizacao,
                        optionInfovagaoLongarinaLateralLocalizacao,
                        vagao.getLocalizacaoLongarinaLateralIndex()
                )
        );

    }

    public void preencherPrimeiroTruqueADescarrilarEngatesEACTS(Vagao vagao) {
        vagao.setQualOTipoDoEngateValue(
                selectOptionAndReturnValue(
                        selectInfovagaoTipoEngate,
                        optionInfovagaoTipoEngate,
                        vagao.getQualOTipoDoEngateIndex()
                )
        );

        sendKeys(inputInfovagaoAlturaEngateA, vagao.getAlturaEngateA());
        sendKeys(inputInfovagaoAlturaEngateB, vagao.getAlturaEngateB());

        vagao.setEngateEstaNiveladoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoEngateNivelado,
                        optionInfovagaoEngateNivelado,
                        vagao.getEngateEstaNiveladoIndex()
                )
        );

        vagao.setExisteMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaValue(
                selectOptionAndReturnValue(
                        selectInfovagaoExisteMarcaCabecaEngate,
                        optionInfovagaoExisteMarcaCabecaEngate,
                        vagao.getExisteMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaIndex()
                )
        );

        /*
        ACT
         */
        vagao.setQualOModeloValue(
                selectOptionAndReturnValue(
                        selectInfovagaoModeloAct,
                        optionInfovagaoModeloAct,
                        vagao.getQualOModeloIndex()
                )
        );

        if (vagao.isExisteFolgaNoACT()) {
            clickWithAction(radioButtonInfovagaoPossuiFolgaActOption0);
            sendKeys(inputInfovagaoFolgaAct, vagao.getQualAMedidaDaFolga());
        } else {
            clickWithAction(radioButtonInfovagaoPossuiFolgaActOption1);
        }

        /*
        COMPONENTES DO ACT/ENGATE
         */

        vagao.setEngateComponentesACTEngateValue(
                selectOptionAndReturnValue(
                        selectInfovagaoComponentesActEngate,
                        optionInfovagaoComponentesActEngate,
                        vagao.getEngateComponentesACTEngateIndex()
                )
        );

        vagao.setChavetaEPinoTValue(
                selectOptionAndReturnValue(
                        selectInfovagaoComponentesActChave,
                        optionInfovagaoComponentesActChave,
                        vagao.getChavetaEPinoTIndex()
                )
        );

        vagao.setMandibulaEPinoValue(
                selectOptionAndReturnValue(
                        selectInfovagaoComponentesActMandibula,
                        optionInfovagaoComponentesActMandibula,
                        vagao.getMandibulaEPinoIndex()
                )
        );

        vagao.setPinoY47ETravaValue(
                selectOptionAndReturnValue(
                        selectInfovagaoComponentesActPino,
                        optionInfovagaoComponentesActPino,
                        vagao.getPinoY47ETravaIndex()
                )
        );
    }

    public void preencherPrimeiroTruqueADescarrilarInformacoesAdicionais(Vagao vagao) {
        sendKeysWithJavaScript(textareaInfovagaoObservacaoFraturas, vagao.getParaPecasFraturadasEspecificarTrincaAntigaOuRecente());
        if (vagao.isLocalDoPODTemMarcasDescarrilamentoAnterior()) {
            clickWithAction(radioButtonInfovagaoLocalPodMarcaOption0);
        } else {
            clickWithAction(radioButtonInfovagaoLocalPodMarcaOption1);
        }

        if (vagao.isLocalDoPODTemHistoricoDeAcidentes06Meses()) {
            clickWithAction(radioButtonInfovagaoLocalPodHistoricoAcidenteOption0);
        } else {
            clickWithAction(radioButtonInfovagaoLocalPodHistoricoAcidenteOption1);
        }

        if (vagao.isHaMarcasDePecaDoVagaoArrastandoNosDormentesAntesDoPOD()) {
            clickWithAction(radioButtonInfovagaoHaMarcaPecaOption0);
            sendKeysWithJavaScript(inputInfovagaoQualPeca, vagao.getQualPeca());
        } else {
            clickWithAction(radioButtonInfovagaoHaMarcaPecaOption1);
        }
    }

    public void clicarBtnSalvar() {
        clickAndHighlight(buttonInfovagaoBtnSalvar);
    }

    public void validarParametrizacao(Vagao vagao) {
        scrollToElement(separatorVagoes);
        Assert.assertEquals(getText(selectInfovagaoSelectVagoesParametrizacao), vagao.getParametrizacaoValue());
    }

    public void validarEscolhaVagaoQueDescarrilhou(Veiculo veiculo) {
        expectValue(inputInfovagaoBuscaVagao, String.valueOf(veiculo.getPosicao()));
    }

    public void validarInformacoesSobreOSinistro(Sinistro sinistro, Vagao vagao) {
        scrollToElement(separatorInformacoesSobreOSinistro);
        Assert.assertEquals(getValue(inputInfovagaoDataAcidente), sinistro.getDate());
        Assert.assertEquals(getValue(inputInfovagaoPrefixoTrem), sinistro.getPrefixo());
        Assert.assertEquals(getValue(inputInfovagaoSindicancia), String.valueOf(sinistro.getIdSinistro()));
        Assert.assertEquals(getValue(inputInfovagaoSubdivisao), sinistro.getSubdivisao());
        Assert.assertEquals(getValue(inputInfovagaoKmDoPod), sinistro.getKm());
        Assert.assertEquals(getText(selectInfovagaoBitola), vagao.getBitolaValue());
        Assert.assertTrue(isRadioChecked(radioButtonInfovagaoEstadoVagao.apply(vagao.getEstadoDoVagao())));
    }

    public void validarDDV(Vagao vagao) {
        scrollToElement(separatorDdv);
        if (vagao.isHaviaDDVNoLocal()) {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoHaviaDdvSim));
            if (vagao.isEstavaOperacional()) {
                Assert.assertTrue(isRadioChecked(radioButtonInfovagaoEstavaOperacionalSim));
                if (vagao.isAtuou()) {
                    Assert.assertTrue(isRadioChecked(radioButtonInfovagaoAtuouSim));
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonInfovagaoAtuouNao));
                }
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonInfovagaoEstavaOperacionalNao));
            }
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoHaviaDdvNao));
        }
        Assert.assertEquals(getValue(textareaInfovagaoMotivos), vagao.getComentarioDDV());
    }

    public void validarPrimeiroVeiculoADescarrilar(Vagao vagao) {
        scrollToElement(selectInfovagaoCondicaoCarga);
        Assert.assertEquals(getText(selectInfovagaoCondicaoCarga), vagao.getComoEstavaACargaValue());

        Assert.assertEquals(getValue(inputInfovagaoTipoMercadoria), vagao.getTipoDeMercadoria());
        Assert.assertEquals(getValue(inputInfovagaoPesoBrutoVeiculo), formatToDecimal(vagao.getPesoBrutoDoVeiculo()));
        if (vagao.isEtiquetadoOuRecomendadoOficina()) {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoEtiquetadoRecomendadoOficinaSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoEtiquetadoRecomendadoOficinaNao));
        }

        /*))        Medição
         */
        Assert.assertTrue(isRadioChecked(radioButtonInfovagaoPmv.apply(vagao.getMedicaoRealizada())));
        Assert.assertEquals(getValue(inputInfovagaoLocalMedicao), vagao.getLocalDaMedicao());

        /*
        ÚLTIMA MANUTENÇÃO PREVENTIVA
         */

        Assert.assertEquals(getValue(inputInfovagaoUltimaManutPreventivaData), vagao.getDataUltimaManutencaoPreventiva());
        Assert.assertEquals(getValue(inputInfovagaoUltimaManutPreventivaTipo), vagao.getTipoUltimaManutencaoPreventiva());
        Assert.assertEquals(getValue(inputInfovagaoUltimaManutPreventivaLocal), vagao.getLocalUltimaManutencaoPreventiva());

        /*
        ÚLTIMA MANUTENÇÃO CORRETIVA
         */
        Assert.assertEquals(getValue(inputInfovagaoUltimaManutCorretivaData), vagao.getDataUltimaManutencaoCorretiva());
        Assert.assertEquals(getValue(inputInfovagaoUltimaManutCorretivaTipo), vagao.getTratativaUltimaManutencaoCorretiva());
        Assert.assertEquals(getValue(inputInfovagaoUltimaManutCorretivaLocal), vagao.getLocalUltimaManutencaoPreventiva());

        /*
        PRIMEIRO RODEIRO - PRIMEIRO TRUQUE A DESCARRILAR
         */

        Assert.assertEquals(getText(selectInfovagaoPrimeiroRodeiroDescarrilar), vagao.getPrimeiroRodeiroADescarrilarPosicaoValue());

        if ((vagao.isRodeirosDescarriladosR1() && !isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR1))
                || (!vagao.isRodeirosDescarriladosR1() && isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR1))) {
            Assert.assertTrue(isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR1));
        }
        if ((vagao.isRodeirosDescarriladosR2() && !isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR2))
                || (!vagao.isRodeirosDescarriladosR2() && isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR2))) {
            Assert.assertTrue(isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR2));
        }
        if ((vagao.isRodeirosDescarriladosR3() && !isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR3))
                || (!vagao.isRodeirosDescarriladosR3() && isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR3))) {
            Assert.assertTrue(isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR3));
        }
        if ((vagao.isRodeirosDescarriladosR4() && !isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR4))
                || (!vagao.isRodeirosDescarriladosR4() && isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR4))) {
            Assert.assertTrue(isCheckboxChecked(checkboxInfoVagaoRodeirosDescarriladosR4));
        }

        Assert.assertEquals(getText(selectInfovagaoEstadoRoda), vagao.getSituacaoDaRodaValue());

        Assert.assertEquals(getText(selectInfovagaoFabricacaoRoda), vagao.getFabricacaoDaRodaValue());

        Assert.assertEquals(getText(selectInfovagaoEixo), vagao.getEixoValue());

        Assert.assertEquals(getText(selectInfovagaoTipoTruque), vagao.getTipoDeTruqueValue());

        Assert.assertEquals(getText(selectInfovagaoMangaVagao), vagao.getMangaDoVagaoValue());

        if (vagao.isSinaisDeSuperAquecimentoNaRoda()) {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoSinaisSuperAquecimentoSim));
            Assert.assertEquals(getValue(inputInfovagaoAquecimentoMedida), formatToDecimal(vagao.getMedidaAquecimento()));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoSinaisSuperAquecimentoNao));
        }

        Assert.assertEquals(getText(selectInfovagaoRolamentoTipo), vagao.getTipoDoRolamentoValue());

        Assert.assertEquals(getText(selectInfovagaoRolamentoCor), vagao.getCorLubrificacaoDoRolamentoValue());

        if (vagao.getIdentificacaoDoEixo().equalsIgnoreCase("fita")) {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoIdentificacaoEixoFita));
            Assert.assertEquals(getValue(inputInfovagaoEixoNumeroFita), vagao.getNumeroDaFITA());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoIdentificacaoEixoNumeroEixo));
            Assert.assertEquals(getValue(inputInfovagaoEixoNumeroEixo), vagao.getNumeroDoEixo());
        }

        /*
        CROQUIS
         */

        if ((vagao.isInfovagaoCroquiCabeceiraA() && !isCheckboxChecked(checkboxInfovagaoCroquiCabeceiraA))
                || (!vagao.isInfovagaoCroquiCabeceiraA() && isCheckboxChecked(checkboxInfovagaoCroquiCabeceiraA))) {
            Assert.assertTrue(isRadioChecked(checkboxInfovagaoCroquiCabeceiraA));
        }

        if ((vagao.isInfovagaoCroquiCabeceiraB() && !isCheckboxChecked(checkboxInfovagaoCroquiCabeceiraB))
                || (!vagao.isInfovagaoCroquiCabeceiraB() && isCheckboxChecked(checkboxInfovagaoCroquiCabeceiraB))) {
            Assert.assertTrue(isRadioChecked(checkboxInfovagaoCroquiCabeceiraB));
        }

        /*
        Lado esquerdo
         */

        if ((vagao.isInfovagaoCroquiErRodeirosDescarriladosR1() && !isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR1))
                || (!vagao.isInfovagaoCroquiErRodeirosDescarriladosR1() && isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR1))) {
            Assert.assertTrue(isRadioChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR1));
        }

        if ((vagao.isInfovagaoCroquiErRodeirosDescarriladosR2() && !isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR2))
                || (!vagao.isInfovagaoCroquiErRodeirosDescarriladosR2() && isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR2))) {
            Assert.assertTrue(isRadioChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR2));
        }

        if ((vagao.isInfovagaoCroquiErRodeirosDescarriladosR3() && !isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR3))
                || (!vagao.isInfovagaoCroquiErRodeirosDescarriladosR3() && isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR3))) {
            Assert.assertTrue(isRadioChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR3));
        }

        if ((vagao.isInfovagaoCroquiErRodeirosDescarriladosR4() && !isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR4))
                || (!vagao.isInfovagaoCroquiErRodeirosDescarriladosR4() && isCheckboxChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR4))) {
            Assert.assertTrue(isRadioChecked(checkboxInfovagaoCroquiErRodeirosDescarriladosR4));
        }
        /*
        Lado direito
         */

        if ((vagao.isInfovagaoCroquiDrRodeirosDescarriladosR1() && !isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR1))
                || (!vagao.isInfovagaoCroquiDrRodeirosDescarriladosR1() && isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR1))) {
            Assert.assertTrue(isRadioChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR1));
        }

        if ((vagao.isInfovagaoCroquiDrRodeirosDescarriladosR2() && !isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR2))
                || (!vagao.isInfovagaoCroquiDrRodeirosDescarriladosR2() && isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR2))) {
            Assert.assertTrue(isRadioChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR2));
        }

        if ((vagao.isInfovagaoCroquiDrRodeirosDescarriladosR3() && !isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR3))
                || (!vagao.isInfovagaoCroquiDrRodeirosDescarriladosR3() && isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR3))) {
            Assert.assertTrue(isRadioChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR3));
        }

        if ((vagao.isInfovagaoCroquiDrRodeirosDescarriladosR4() && !isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR4))
                || (!vagao.isInfovagaoCroquiDrRodeirosDescarriladosR4() && isCheckboxChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR4))) {
            Assert.assertTrue(isRadioChecked(checkboxInfovagaoCroquiDrRodeirosDescarriladosR4));
        }

        Assert.assertTrue(isRadioChecked(radioButtonInfovagaoCroquiSentidoDeslocamento.apply(vagao.getMarcarOSentidoDoDeslocamentoDoVagao())));
    }

    public void validarPrimeiroTruqueADescarrilar(Vagao vagao) {
          /*
        PRATO PIÃO
         */
        Assert.assertEquals(getValue(inputInfovagaoTipoPratoPiao), vagao.getTipoPratoPiao());
        Assert.assertEquals(getValue(inputInfovagaoFolgaDiametro), vagao.getFolgaNoDiametro());
        Assert.assertEquals(getValue(inputInfovagaoFolgaVertical), vagao.getFolgaVertical());

        /*
        PRATO PIÃO INFERIOR
         */

        Assert.assertEquals(getValue(inputInfovagaoPratoPiaoInferiorDiametro), vagao.getDiametroPratoPiaoInferior());
        Assert.assertEquals(getValue(inputInfovagaoPratoPiaoInferiorProfundidade), vagao.getProfundidadePratoPiaoInferior());

        /*
        PRATO PIÃO SUPERIOR
         */
        scrollToElement(divPratoPiaoSuperior);
        Assert.assertEquals(getValue(inputInfovagaoPratoPiaoSuperiorDiametro), vagao.getDiametroPratoPiaoSuperior());
        Assert.assertEquals(getValue(inputInfovagaoPratoPiaoSuperiorAltura), vagao.getDiametroPratoPiaoSuperior());

        /*
        CONDIÇÃO DE CUNHA / CHAPA DE DESGASTE
         */

        Assert.assertEquals(getText(selectInfovagaoEstadoCunhaFriccaoDireita1), vagao.getDireita1CunhaDeFriccaoValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoCunhaFriccaoDireita2), vagao.getDireita2CunhaDeFriccaoValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoCunhaFriccaoDireita3), vagao.getDireita3CunhaDeFriccaoValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoCunhaFriccaoDireita4), vagao.getDireita4CunhaDeFriccaoValue());
        /*
        esquerda
         */

        Assert.assertEquals(getText(selectInfovagaoEstadoCunhaFriccaoEsquerdo1), vagao.getEsquerda1CunhaDeFriccaoValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoCunhaFriccaoEsquerdo2), vagao.getEsquerda2CunhaDeFriccaoValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoCunhaFriccaoEsquerdo3), vagao.getEsquerda3CunhaDeFriccaoValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoCunhaFriccaoEsquerdo4), vagao.getEsquerda4CunhaDeFriccaoValue());


        /*
        PLACA DE DESGASTE COLUNA LAT.

        Direita
         */

        Assert.assertEquals(getText(selectInfovagaoEstadoPlacaDesgasteLateralDireita1), vagao.getDireita1PlacaDeDesgasteColunaLatValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoPlacaDesgasteLateralDireita2), vagao.getDireita2PlacaDeDesgasteColunaLatValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoPlacaDesgasteLateralDireita3), vagao.getDireita3PlacaDeDesgasteColunaLatValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoPlacaDesgasteLateralDireita4), vagao.getDireita4PlacaDeDesgasteColunaLatValue());

        /*
            PLACA DE DESGASTE COLUNA LAT.
        esquerda
         */
        Assert.assertEquals(getText(selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo1), vagao.getEsquerda1PlacaDeDesgasteColunaLatValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo2), vagao.getEsquerda2PlacaDeDesgasteColunaLatValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo3), vagao.getEsquerda3PlacaDeDesgasteColunaLatValue());

        Assert.assertEquals(getText(selectInfovagaoEstadoPlacaDesgasteLateralEsquerdo4), vagao.getEsquerda4PlacaDeDesgasteColunaLatValue());

        /*
        BOTÕES LATERAL DO TRUQUE
         */

        Assert.assertEquals(getValue(inputInfovagaoBotaoLateralTruqueDireitaA), formatToDecimal(vagao.getDireitaABotoesLateralDoTruque()));
        Assert.assertEquals(getValue(inputInfovagaoBotaoLateralTruqueEsquerdaA), formatToDecimal(vagao.getEsquerdaABotoesLateralDoTruque()));
        Assert.assertEquals(getValue(inputInfovagaoBotaoLateralTruqueDireitaB), formatToDecimal(vagao.getDireitaBBotoesLateralDoTruque()));
        Assert.assertEquals(getValue(inputInfovagaoBotaoLateralTruqueEsquerdaB), formatToDecimal(vagao.getEsquerdaBBotoesLateralDoTruque()));

        /*
        ALTURA DAS CUNHAS CAB. A
         */

        Assert.assertEquals(getValue(inputInfovagaoAlturaCunhaCabALadoDireito1), formatToDecimal(vagao.getAlturaDasCunhasCabALadoDiteiro1()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaCunhaCabALadoDireito2), formatToDecimal(vagao.getAlturaDasCunhasCabALadoDiteiro2()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaCunhaCabALadoEsquerdo1), formatToDecimal(vagao.getAlturaDasCunhasCabALadoEsquerdo1()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaCunhaCabALadoEsquerdo2), formatToDecimal(vagao.getAlturaDasCunhasCabALadoEsquerdo2()));
        /*
        ALTURA DAS CUNHAS CAB. B
         */

        Assert.assertEquals(getValue(inputInfovagaoAlturaCunhaCabBLadoDireito3), formatToDecimal(vagao.getAlturaDasCunhasCabBLadoDiteiro3()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaCunhaCabBLadoDireito4), formatToDecimal(vagao.getAlturaDasCunhasCabBLadoDiteiro4()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaCunhaCabBLadoEsquerdo3), formatToDecimal(vagao.getAlturaDasCunhasCabBLadoEsquerdo3()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaCunhaCabBLadoEsquerdo4), formatToDecimal(vagao.getAlturaDasCunhasCabBLadoEsquerdo4()));

        /*
        ALTURA DAS MOLAS (DO TRUQUE DESCARRILADO)
         */

        Assert.assertEquals(getValue(inputInfovagaoAlturaMolaTruqueExterna), formatToDecimal(vagao.getExternaAlturaDasMolasDoTruqueDescarrilado()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolaTruqueIntermediaria), formatToDecimal(vagao.getIntermediariaAlturaDasMolasDoTruqueDescarrilado()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolaTruqueInterna), formatToDecimal(vagao.getInternaAlturaDasMolasDoTruqueDescarrilado()));

        /*
        LADO DIREITO - LD
         */

        final int NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS = 8;
        final int NUMERO_DE_CAMPOS_INTERMEDIARIAS = 4;

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD",
                inputInfovagaoMolaExternaLadoDireitoV
        );

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD",
                inputInfovagaoMolaInternaLadoDireitoV
        );

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_INTERMEDIARIAS,
                "getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoDireitoLD",
                inputInfovagaoMolaIntermediariaLadoDireitoV
        );

        /*
        LADO ESQUERDO - LE
         */

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasInternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE",
                inputInfovagaoMolaExternaLadoEsquerdoV
        );

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasExternasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE",
                inputInfovagaoMolaInternaLadoEsquerdoV
        );

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_INTERMEDIARIAS,
                "getMolasIntermediariasAlturaDasMolasDoTruqueDescarriladoLadoEsquerdoLE",
                inputInfovagaoMolaIntermediariaLadoEsquerdaV
        );

        /*
        ALTURA DAS MOLAS DAS CUNHAS
         */

        Assert.assertEquals(getText(selectInfovagaoTipoAlturaMolasCunha), vagao.getSobreAlturaDasMolasCunhasValue());

        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasCunhaDireitoMolaExterna1), formatToDecimal(vagao.getMolasExternasAlturaDasMolasCunhasLadoDireitoLD1()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasCunhaDireitoMolaExterna2), formatToDecimal(vagao.getMolasExternasAlturaDasMolasCunhasLadoDireitoLD2()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasCunhaDireitoMolaInterna1), formatToDecimal(vagao.getMolasInternasAlturaDasMolasCunhasLadoDireitoLD1()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasCunhaDireitoMolaInterna2), formatToDecimal(vagao.getMolasInternasAlturaDasMolasCunhasLadoDireitoLD2()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasCunhaEsquerdoMolaExterna1), formatToDecimal(vagao.getMolasExternasAlturaDasMolasCunhasLadoEsquerdoLE1()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasCunhaEsquerdoMolaExterna2), formatToDecimal(vagao.getMolasExternasAlturaDasMolasCunhasLadoEsquerdoLE2()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasCunhaEsquerdoMolaInterna1), formatToDecimal(vagao.getMolasInternasAlturaDasMolasCunhasLadoEsquerdoLE1()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasCunhaEsquerdoMolaInterna2), formatToDecimal(vagao.getMolasInternasAlturaDasMolasCunhasLadoEsquerdoLE2()));

        /*
        ALTURA DAS MOLAS - TRUQUE DA OUTRA CABECEIRA
         */


        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD",
                inputInfovagaoAlturaMolasTruqueOutraCabExternaDireitaV
        );

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD",
                inputInfovagaoAlturaMolasTruqueOutraCabInternaDireitaV
        );

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_INTERMEDIARIAS,
                "getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoDireitoLD",
                inputInfovagaoAlturaMolasTruqueOutraIntermediariaLadoDireitoV
        );

        /*
        LADO ESQUERDO - LE
         */

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasExternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE",
                inputInfovagaoAlturaMolasTruqueOutraCabExternaEsquerdaV
        );

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_EXTERNAS_INTERNAS,
                "getMolasInternasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE",
                inputInfovagaoAlturaMolasTruqueOutraCabInternaEsquerdaV
        );

        assertKeysForLine(
                vagao,
                NUMERO_DE_CAMPOS_INTERMEDIARIAS,
                "getMolasIntermediariasAlturaDasMolasTruqueDaOutraCabeceiraLadoEsquerdoLE",
                inputInfovagaoAlturaMolasTruqueOutraIntermediariaLadoEsquerdaV
        );

        /*
        ALTURA DAS MOLAS DAS CUNHAS OUTRA CABECEIRA
         */

        Assert.assertEquals(getText(selectInfovagaoTipoAlturaMolasCunhaCab), vagao.getSobreAlturaDasMolasCunhasOutraCabeceiraValue());

        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaExterna1), formatToDecimal(vagao.getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaExterna2), formatToDecimal(vagao.getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaInterna1), formatToDecimal(vagao.getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD1()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasBarberRideMasterDireitoMolaInterna2), formatToDecimal(vagao.getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoDireitoLD2()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaExterna1), formatToDecimal(vagao.getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaExterna2), formatToDecimal(vagao.getMolasExternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaInterna1), formatToDecimal(vagao.getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE1()));
        Assert.assertEquals(getValue(inputInfovagaoAlturaMolasBarberRideMasterEsquerdoMolaInterna2), formatToDecimal(vagao.getMolasInternasAlturaDasMolasCunhasOutraCabeceiraLadoEsquerdoLE2()));
    }

    public void validarPrimeiroTruqueADescarrilarAmparaBalanco(Vagao vagao) {

        /*
        FOLGAS - MEDIDAS VÁLIDAS ANTES DO ACIDENTE
         */

        Assert.assertEquals(getValue(inputInfovagaoLocal), vagao.getLocalFolgasMedidasValidasAntesDoAcidente());
        Assert.assertEquals(getValue(inputInfovagaoData), vagao.getDataFolgasMedidasValidasAntesDoAcidente());

        Assert.assertEquals(getText(selectInfovagaoTipoManutencao), vagao.getTipoManutencaoFolgasMedidasValidasAntesDoAcidenteValue());

        Assert.assertEquals(getValue(inputInfovagaoAmparaBalancosEsquerdaA), formatToDecimal(vagao.getaEsquerdaFolgasMedidasValidasAntesDoAcidente()));
        Assert.assertEquals(getValue(inputInfovagaoAmparaBalancosDireitaA), formatToDecimal(vagao.getaDireitaFolgasMedidasValidasAntesDoAcidente()));
        Assert.assertEquals(getValue(inputInfovagaoAmparaBalancosEsquerdaB), formatToDecimal(vagao.getbEsquerdaFolgasMedidasValidasAntesDoAcidente()));
        Assert.assertEquals(getValue(inputInfovagaoAmparaBalancosDireitaB), formatToDecimal(vagao.getbDireitaFolgasMedidasValidasAntesDoAcidente()));

        /*
        FOLGAS - MEDIDAS APÓS O ACIDENTE
         */

        Assert.assertEquals(getValue(inputInfovagaoAmparaBalancosPosAcidenteEsquerdaA), formatToDecimal(vagao.getaEsquerdaFolgasAposOAcidente()));
        Assert.assertEquals(getValue(inputInfovagaoAmparaBalancosPosAcidenteDireitaA), formatToDecimal(vagao.getaDireitaFolgasAposOAcidente()));
        Assert.assertEquals(getValue(inputInfovagaoAmparaBalancosPosAcidenteEsquerdaB), formatToDecimal(vagao.getbEsquerdaFolgasAposOAcidente()));
        Assert.assertEquals(getValue(inputInfovagaoAmparaBalancosPosAcidenteDireitaB), formatToDecimal(vagao.getbDireitaFolgasAposOAcidente()));


        /*
        OUTRAS INFORMAÇÕES
         */

        Assert.assertEquals(getValue(inputInfovagaoExtensaoTrafego), vagao.getExtensaoDoTrafegoDescarrilado());
        Assert.assertEquals(getText(selectInfovagaoModeloAmparaBalanco), vagao.getModeloAmparaBalancoValue());

        /*
        CASTANHA/TAMPA DO AMPARA BALANÇO
         */

        Assert.assertEquals(getText(selectInfovagaoEstadoCastanha), vagao.getCondicaoCastanhaTampaDoAmparaBalancoValue());

        /*
        PARAFUSOS DE FIXAÇÃO (CONDIÇÃO)
         */

        Assert.assertEquals(getValue(inputInfovagaoChapaSuperiorParafusoAEsq), formatToDecimal(vagao.getaEsquerdaParafusosDeFixacao()));
        Assert.assertEquals(getValue(inputInfovagaoChapaSuperiorParafusoBEsq), formatToDecimal(vagao.getbEsquerdaParafusosDeFixacao()));
        Assert.assertEquals(getValue(inputInfovagaoChapaSuperiorParafusoADir), formatToDecimal(vagao.getaDireitaFolgasAposOAcidente()));
        Assert.assertEquals(getValue(inputInfovagaoChapaSuperiorParafusoBDir), formatToDecimal(vagao.getbDireitaFolgasAposOAcidente()));

        /*
        MEDIDA DE DESGASTE DA CHAPA SUPERIOR DO AB
         */

        Assert.assertEquals(getValue(inputInfovagaoChapaSuperiorDesgasteAEsq), formatToDecimal(vagao.getaEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB()));
        Assert.assertEquals(getValue(inputInfovagaoChapaSuperiorDesgasteBEsq), formatToDecimal(vagao.getbEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB()));
        Assert.assertEquals(getValue(inputInfovagaoChapaSuperiorDesgasteADir), formatToDecimal(vagao.getaDireitaMedidasDeDesgasteDaChapaSuperiorDoAB()));
        Assert.assertEquals(getValue(inputInfovagaoChapaSuperiorDesgasteBDir), formatToDecimal(vagao.getaEsquerdaMedidasDeDesgasteDaChapaSuperiorDoAB()));

    }

    public void validarPrimeiroTruqueADescarrilarFreio(Vagao vagao) {
        Assert.assertEquals(getText(selectInfovagaoFreioValvula), vagao.getValvulaDeFreioValue());

        Assert.assertEquals(getText(selectInfovagaoFreioManual), vagao.getFreioManualValue());

        Assert.assertEquals(getText(selectInfovagaoTorneiraAngular), vagao.getTorneiraAngularEGValue());

        Assert.assertEquals(getText(selectInfovagaoDispositivo), vagao.getDispositivoVazioCarregadoValue());

        Assert.assertEquals(getText(selectInfovagaoTorneiraIsolamentoFreio), vagao.getTorneiraIsolamentoDeFreioValue());

        Assert.assertEquals(getText(selectInfovagaoPosicaoValvulaRetentora), vagao.getPosicaoDaValvulaRetentoraValue());
        /*
        DDV (DETECTOR DE DESCARRILAMENTO DE VAGÃO)
         */

        Assert.assertEquals(getText(selectInfovagaoPreDescarrilamento), vagao.getPreDescarrilamentoValue());

        Assert.assertEquals(getText(selectInfovagaoPosDescarrilamentoDdvAcionou1), vagao.getdDVAcionouRodeiro1Value());

        Assert.assertEquals(getText(selectInfovagaoPosDescarrilamentoDdvAcionou2), vagao.getdDVAcionouRodeiro2Value());

        Assert.assertEquals(getText(selectInfovagaoPosDescarrilamentoDdvAcionou3), vagao.getdDVAcionouRodeiro3Value());

        Assert.assertEquals(getText(selectInfovagaoPosDescarrilamentoDdvAcionou4), vagao.getdDVAcionouRodeiro4Value());
    }

    public void validarPrimeiroTruqueADescarrilarEstruturaDoVagao(Vagao vagao) {
         /*
        LONGARINA
         */

        Assert.assertEquals(getText(selectInfovagaoLongarinaCondicao), vagao.getCondicaoLongarinaValue());

        Assert.assertEquals(getText(selectInfovagaoLongarinaLocalizacao), vagao.getLocalizacaoLongarinaValue());

        /*
        TRAVESSA DO PRATO DO PIÃO SUPERIOR
         */

        Assert.assertEquals(getText(selectInfovagaoTravessaPratoPiaoCondicao), vagao.getCondicaoTravessaDoPratoDoPiaoSuperiorValue());

        Assert.assertEquals(getText(selectInfovagaoTravessaPratoPiaoLocalizacao), vagao.getLocalizacaoTravessaDoPratoDoPiaoSuperiorValue());

        /*
        LONGARINA LATERAL
         */

        Assert.assertEquals(getText(selectInfovagaoLongarinaLateralCondicao), vagao.getCondicaoLongarinaLateralValue());

        Assert.assertEquals(getText(selectInfovagaoLongarinaLateralLocalizacao), vagao.getLocalizacaoLongarinaLateralValue());

    }

    public void validarPrimeiroTruqueADescarrilarEngatesEACTS(Vagao vagao) {
        Assert.assertEquals(getText(selectInfovagaoTipoEngate), vagao.getQualOTipoDoEngateValue());

        Assert.assertEquals(getValue(inputInfovagaoAlturaEngateA), vagao.getAlturaEngateA());
        Assert.assertEquals(getValue(inputInfovagaoAlturaEngateB), vagao.getAlturaEngateB());

        Assert.assertEquals(getText(selectInfovagaoEngateNivelado), vagao.getEngateEstaNiveladoValue());

        Assert.assertEquals(getText(selectInfovagaoExisteMarcaCabecaEngate), vagao.getExisteMarcaDaCabecaDoEngateBatendoNaChapaFrontalDoEspelhoDaLongarinaValue());

        /*
        ACT
         */
        Assert.assertEquals(getText(selectInfovagaoModeloAct), vagao.getQualOModeloValue());

        if (vagao.isExisteFolgaNoACT()) {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoPossuiFolgaActOption0));
            Assert.assertEquals(getValue(inputInfovagaoFolgaAct), formatToDecimal(vagao.getQualAMedidaDaFolga()));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoPossuiFolgaActOption1));
        }

        /*
        COMPONENTES DO ACT/ENGATE
         */

        Assert.assertEquals(getText(selectInfovagaoComponentesActEngate), vagao.getEngateComponentesACTEngateValue());

        Assert.assertEquals(getText(selectInfovagaoComponentesActChave), vagao.getChavetaEPinoTValue());

        Assert.assertEquals(getText(selectInfovagaoComponentesActMandibula), vagao.getMandibulaEPinoValue());

        Assert.assertEquals(getText(selectInfovagaoComponentesActPino), vagao.getPinoY47ETravaValue());

    }

    public void validarPrimeiroTruqueADescarrilarInformacoesAdicionais(Vagao vagao) {
        Assert.assertEquals(getValue(textareaInfovagaoObservacaoFraturas), vagao.getParaPecasFraturadasEspecificarTrincaAntigaOuRecente());
        if (vagao.isLocalDoPODTemMarcasDescarrilamentoAnterior()) {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoLocalPodMarcaOption0));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoLocalPodMarcaOption1));
        }

        if (vagao.isLocalDoPODTemHistoricoDeAcidentes06Meses()) {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoLocalPodHistoricoAcidenteOption0));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoLocalPodHistoricoAcidenteOption1));
        }

        if (vagao.isHaMarcasDePecaDoVagaoArrastandoNosDormentesAntesDoPOD()) {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoHaMarcaPecaOption0));
            Assert.assertEquals(getValue(inputInfovagaoQualPeca), vagao.getQualPeca());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonInfovagaoHaMarcaPecaOption1));
        }
    }

    public void preencherMedidasDasRodas(MedidaDeRodas medidaDeRodas) {
        sendKeys(inputMedidaRodasEspessuraFrisoCabAEsq1, medidaDeRodas.getEspessuraDoFriso().get(0));
        sendKeys(inputMedidaRodasEspessuraFrisoCabADir1, medidaDeRodas.getEspessuraDoFriso().get(1));
        sendKeys(inputMedidaRodasEspessuraFrisoCabAEsq2, medidaDeRodas.getEspessuraDoFriso().get(2));
        sendKeys(inputMedidaRodasEspessuraFrisoCabADir2, medidaDeRodas.getEspessuraDoFriso().get(3));
        sendKeys(inputMedidaRodasEspessuraFrisoCabBEsq1, medidaDeRodas.getEspessuraDoFriso().get(4));
        sendKeys(inputMedidaRodasEspessuraFrisoCabBDir1, medidaDeRodas.getEspessuraDoFriso().get(5));
        sendKeys(inputMedidaRodasEspessuraFrisoCabBEsq2, medidaDeRodas.getEspessuraDoFriso().get(6));
        sendKeys(inputMedidaRodasEspessuraFrisoCabBDir2, medidaDeRodas.getEspessuraDoFriso().get(7));

        sendKeys(inputMedidaRodasAlturaFrisoCabAEsq1, medidaDeRodas.getAlturaDoFriso().get(0));
        sendKeys(inputMedidaRodasAlturaFrisoCabADir1, medidaDeRodas.getAlturaDoFriso().get(1));
        sendKeys(inputMedidaRodasAlturaFrisoCabAEsq2, medidaDeRodas.getAlturaDoFriso().get(2));
        sendKeys(inputMedidaRodasAlturaFrisoCabADir2, medidaDeRodas.getAlturaDoFriso().get(3));
        sendKeys(inputMedidaRodasAlturaFrisoCabBEsq1, medidaDeRodas.getAlturaDoFriso().get(4));
        sendKeys(inputMedidaRodasAlturaFrisoCabBDir1, medidaDeRodas.getAlturaDoFriso().get(5));
        sendKeys(inputMedidaRodasAlturaFrisoCabBEsq2, medidaDeRodas.getAlturaDoFriso().get(6));
        sendKeys(inputMedidaRodasAlturaFrisoCabBDir2, medidaDeRodas.getAlturaDoFriso().get(7));

        sendKeys(inputMedidaRodasBandagemCabAEsq1, medidaDeRodas.getBandagem().get(0));
        sendKeys(inputMedidaRodasBandagemCabADir1, medidaDeRodas.getBandagem().get(1));
        sendKeys(inputMedidaRodasBandagemCabAEsq2, medidaDeRodas.getBandagem().get(2));
        sendKeys(inputMedidaRodasBandagemCabADir2, medidaDeRodas.getBandagem().get(3));
        sendKeys(inputMedidaRodasBandagemCabBEsq1, medidaDeRodas.getBandagem().get(4));
        sendKeys(inputMedidaRodasBandagemCabBDir1, medidaDeRodas.getBandagem().get(5));
        sendKeys(inputMedidaRodasBandagemCabBEsq2, medidaDeRodas.getBandagem().get(6));
        sendKeys(inputMedidaRodasBandagemCabBDir2, medidaDeRodas.getBandagem().get(7));

        sendKeys(inputMedidaRodasLarguraCabAEsq1, medidaDeRodas.getLarguraDaRoda().get(0));
        sendKeys(inputMedidaRodasLarguraCabADir1, medidaDeRodas.getLarguraDaRoda().get(1));
        sendKeys(inputMedidaRodasLarguraCabAEsq2, medidaDeRodas.getLarguraDaRoda().get(2));
        sendKeys(inputMedidaRodasLarguraCabADir2, medidaDeRodas.getLarguraDaRoda().get(3));
        sendKeys(inputMedidaRodasLarguraCabBEsq1, medidaDeRodas.getLarguraDaRoda().get(4));
        sendKeys(inputMedidaRodasLarguraCabBDir1, medidaDeRodas.getLarguraDaRoda().get(5));
        sendKeys(inputMedidaRodasLarguraCabBEsq2, medidaDeRodas.getLarguraDaRoda().get(6));
        sendKeys(inputMedidaRodasLarguraCabBDir2, medidaDeRodas.getLarguraDaRoda().get(7));

        sendKeys(inputMedidaRodasConcavidadeCabAEsq1, medidaDeRodas.getConcavidade().get(0));
        sendKeys(inputMedidaRodasConcavidadeCabADir1, medidaDeRodas.getConcavidade().get(1));
        sendKeys(inputMedidaRodasConcavidadeCabAEsq2, medidaDeRodas.getConcavidade().get(2));
        sendKeys(inputMedidaRodasConcavidadeCabADir2, medidaDeRodas.getConcavidade().get(3));
        sendKeys(inputMedidaRodasConcavidadeCabBEsq1, medidaDeRodas.getConcavidade().get(4));
        sendKeys(inputMedidaRodasConcavidadeCabBDir1, medidaDeRodas.getConcavidade().get(5));
        sendKeys(inputMedidaRodasConcavidadeCabBEsq2, medidaDeRodas.getConcavidade().get(6));
        sendKeys(inputMedidaRodasConcavidadeCabBDir2, medidaDeRodas.getConcavidade().get(7));

        sendKeys(inputBitolaM1a1, medidaDeRodas.getBitolaM1().get(0));
        sendKeys(inputBitolaM1a2, medidaDeRodas.getBitolaM1().get(1));
        sendKeys(inputBitolaM1b3, medidaDeRodas.getBitolaM1().get(2));
        sendKeys(inputBitolaM1b4, medidaDeRodas.getBitolaM1().get(3));

        sendKeys(inputBitolaM2a1, medidaDeRodas.getBitolaM2().get(0));
        sendKeys(inputBitolaM2a2, medidaDeRodas.getBitolaM2().get(1));
        sendKeys(inputBitolaM2b3, medidaDeRodas.getBitolaM2().get(2));
        sendKeys(inputBitolaM2b4, medidaDeRodas.getBitolaM2().get(3));

        sendKeys(inputBitolaM3a1, medidaDeRodas.getBitolaM3().get(0));
        sendKeys(inputBitolaM3a2, medidaDeRodas.getBitolaM3().get(1));
        sendKeys(inputBitolaM3b3, medidaDeRodas.getBitolaM3().get(2));
        sendKeys(inputBitolaM3b4, medidaDeRodas.getBitolaM3().get(3));
    }

    public void validarMedidaDeRodas(MedidaDeRodas medidaDeRodas) {
        Assert.assertEquals(getValue(inputMedidaRodasEspessuraFrisoCabAEsq1), formatToDecimal(medidaDeRodas.getEspessuraDoFriso().get(0)));
        Assert.assertEquals(getValue(inputMedidaRodasEspessuraFrisoCabADir1), formatToDecimal(medidaDeRodas.getEspessuraDoFriso().get(1)));
        Assert.assertEquals(getValue(inputMedidaRodasEspessuraFrisoCabAEsq2), formatToDecimal(medidaDeRodas.getEspessuraDoFriso().get(2)));
        Assert.assertEquals(getValue(inputMedidaRodasEspessuraFrisoCabADir2), formatToDecimal(medidaDeRodas.getEspessuraDoFriso().get(3)));
        Assert.assertEquals(getValue(inputMedidaRodasEspessuraFrisoCabBEsq1), formatToDecimal(medidaDeRodas.getEspessuraDoFriso().get(4)));
        Assert.assertEquals(getValue(inputMedidaRodasEspessuraFrisoCabBDir1), formatToDecimal(medidaDeRodas.getEspessuraDoFriso().get(5)));
        Assert.assertEquals(getValue(inputMedidaRodasEspessuraFrisoCabBEsq2), formatToDecimal(medidaDeRodas.getEspessuraDoFriso().get(6)));
        Assert.assertEquals(getValue(inputMedidaRodasEspessuraFrisoCabBDir2), formatToDecimal(medidaDeRodas.getEspessuraDoFriso().get(7)));

        Assert.assertEquals(getValue(inputMedidaRodasAlturaFrisoCabAEsq1), formatToDecimal(medidaDeRodas.getAlturaDoFriso().get(0)));
        Assert.assertEquals(getValue(inputMedidaRodasAlturaFrisoCabADir1), formatToDecimal(medidaDeRodas.getAlturaDoFriso().get(1)));
        Assert.assertEquals(getValue(inputMedidaRodasAlturaFrisoCabAEsq2), formatToDecimal(medidaDeRodas.getAlturaDoFriso().get(2)));
        Assert.assertEquals(getValue(inputMedidaRodasAlturaFrisoCabADir2), formatToDecimal(medidaDeRodas.getAlturaDoFriso().get(3)));
        Assert.assertEquals(getValue(inputMedidaRodasAlturaFrisoCabBEsq1), formatToDecimal(medidaDeRodas.getAlturaDoFriso().get(4)));
        Assert.assertEquals(getValue(inputMedidaRodasAlturaFrisoCabBDir1), formatToDecimal(medidaDeRodas.getAlturaDoFriso().get(5)));
        Assert.assertEquals(getValue(inputMedidaRodasAlturaFrisoCabBEsq2), formatToDecimal(medidaDeRodas.getAlturaDoFriso().get(6)));
        Assert.assertEquals(getValue(inputMedidaRodasAlturaFrisoCabBDir2), formatToDecimal(medidaDeRodas.getAlturaDoFriso().get(7)));

        Assert.assertEquals(getValue(inputMedidaRodasBandagemCabAEsq1), formatToDecimal(medidaDeRodas.getBandagem().get(0)));
        Assert.assertEquals(getValue(inputMedidaRodasBandagemCabADir1), formatToDecimal(medidaDeRodas.getBandagem().get(1)));
        Assert.assertEquals(getValue(inputMedidaRodasBandagemCabAEsq2), formatToDecimal(medidaDeRodas.getBandagem().get(2)));
        Assert.assertEquals(getValue(inputMedidaRodasBandagemCabADir2), formatToDecimal(medidaDeRodas.getBandagem().get(3)));
        Assert.assertEquals(getValue(inputMedidaRodasBandagemCabBEsq1), formatToDecimal(medidaDeRodas.getBandagem().get(4)));
        Assert.assertEquals(getValue(inputMedidaRodasBandagemCabBDir1), formatToDecimal(medidaDeRodas.getBandagem().get(5)));
        Assert.assertEquals(getValue(inputMedidaRodasBandagemCabBEsq2), formatToDecimal(medidaDeRodas.getBandagem().get(6)));
        Assert.assertEquals(getValue(inputMedidaRodasBandagemCabBDir2), formatToDecimal(medidaDeRodas.getBandagem().get(7)));

        Assert.assertEquals(getValue(inputMedidaRodasLarguraCabAEsq1), formatToDecimal(medidaDeRodas.getLarguraDaRoda().get(0)));
        Assert.assertEquals(getValue(inputMedidaRodasLarguraCabADir1), formatToDecimal(medidaDeRodas.getLarguraDaRoda().get(1)));
        Assert.assertEquals(getValue(inputMedidaRodasLarguraCabAEsq2), formatToDecimal(medidaDeRodas.getLarguraDaRoda().get(2)));
        Assert.assertEquals(getValue(inputMedidaRodasLarguraCabADir2), formatToDecimal(medidaDeRodas.getLarguraDaRoda().get(3)));
        Assert.assertEquals(getValue(inputMedidaRodasLarguraCabBEsq1), formatToDecimal(medidaDeRodas.getLarguraDaRoda().get(4)));
        Assert.assertEquals(getValue(inputMedidaRodasLarguraCabBDir1), formatToDecimal(medidaDeRodas.getLarguraDaRoda().get(5)));
        Assert.assertEquals(getValue(inputMedidaRodasLarguraCabBEsq2), formatToDecimal(medidaDeRodas.getLarguraDaRoda().get(6)));
        Assert.assertEquals(getValue(inputMedidaRodasLarguraCabBDir2), formatToDecimal(medidaDeRodas.getLarguraDaRoda().get(7)));

        Assert.assertEquals(getValue(inputMedidaRodasConcavidadeCabAEsq1), formatToDecimal(medidaDeRodas.getConcavidade().get(0)));
        Assert.assertEquals(getValue(inputMedidaRodasConcavidadeCabADir1), formatToDecimal(medidaDeRodas.getConcavidade().get(1)));
        Assert.assertEquals(getValue(inputMedidaRodasConcavidadeCabAEsq2), formatToDecimal(medidaDeRodas.getConcavidade().get(2)));
        Assert.assertEquals(getValue(inputMedidaRodasConcavidadeCabADir2), formatToDecimal(medidaDeRodas.getConcavidade().get(3)));
        Assert.assertEquals(getValue(inputMedidaRodasConcavidadeCabBEsq1), formatToDecimal(medidaDeRodas.getConcavidade().get(4)));
        Assert.assertEquals(getValue(inputMedidaRodasConcavidadeCabBDir1), formatToDecimal(medidaDeRodas.getConcavidade().get(5)));
        Assert.assertEquals(getValue(inputMedidaRodasConcavidadeCabBEsq2), formatToDecimal(medidaDeRodas.getConcavidade().get(6)));
        Assert.assertEquals(getValue(inputMedidaRodasConcavidadeCabBDir2), formatToDecimal(medidaDeRodas.getConcavidade().get(7)));

        Assert.assertEquals(getValue(inputBitolaM1a1), medidaDeRodas.getBitolaM1().get(0));
        Assert.assertEquals(getValue(inputBitolaM1a2), medidaDeRodas.getBitolaM1().get(1));
        Assert.assertEquals(getValue(inputBitolaM1b3), medidaDeRodas.getBitolaM1().get(2));
        Assert.assertEquals(getValue(inputBitolaM1b4), medidaDeRodas.getBitolaM1().get(3));

        Assert.assertEquals(getValue(inputBitolaM2a1), medidaDeRodas.getBitolaM2().get(0));
        Assert.assertEquals(getValue(inputBitolaM2a2), medidaDeRodas.getBitolaM2().get(1));
        Assert.assertEquals(getValue(inputBitolaM2b3), medidaDeRodas.getBitolaM2().get(2));
        Assert.assertEquals(getValue(inputBitolaM2b4), medidaDeRodas.getBitolaM2().get(3));

        Assert.assertEquals(getValue(inputBitolaM3a1), medidaDeRodas.getBitolaM3().get(0));
        Assert.assertEquals(getValue(inputBitolaM3a2), medidaDeRodas.getBitolaM3().get(1));
        Assert.assertEquals(getValue(inputBitolaM3b3), medidaDeRodas.getBitolaM3().get(2));
        Assert.assertEquals(getValue(inputBitolaM3b4), medidaDeRodas.getBitolaM3().get(3));
    }

    public void clicarBtnPreencherTelaCheia() {
        clickAndHighlight(btnPreencherTelaCheia);
    }

    public void salvarMedidasRodas() {
        clickAndHighlight(btnSalvarMedidaRodas);
    }

    public void validaFormReadOnly() {
        Assert.assertTrue(isAriaDisabled(selectInfovagaoSelectVagoesParametrizacao));
        expectElementVisible(buttonButtonProximo);
        expectElementVisible(buttonButtonVoltar);
    }
}
