package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.GeralPage;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.testng.Assert;

public class ConclusaoSindicanciaPage extends GeralPage {

    private Faker faker = new Faker();
    private By separatorConclusao = By.name("Conclusão");
    private By textAreaConclusao = By.id("observacaoControl");
    private By btnSalvarConclusao = By.id("salvarConclusao");
    private By btnSalvarEConcluir = By.id("salvarAvancarConclusao");
    private By btnVoltar = By.id("back-button");
    private By iconeStatusSindicancia = By.id("icon-status");
    private By btnSalvarEConcluirSegPatrimonial = By.id("salvar-concluir-informacoes-registro-acidente");
    private By labelConclusaoAnaliseTecnica = By.cssSelector("sro-conclusao > form > div > div > label");

    public void validaQueEstaNaPaginaDeConclusao() {
        expectElementVisible(separatorConclusao);
    }

    public void preencheConclusao(String conclusao) {
        sendKeysWithJavaScript(textAreaConclusao, conclusao);
    }

    public void clicarBtnSalvar() {
        clickAndHighlight(btnSalvarConclusao);
    }

    public void validaConclusaoIncluida(String conclusao) {
        expectElementVisible(textAreaConclusao);
        Assert.assertEquals(getValue(textAreaConclusao), conclusao);
    }

    public void clicarBtnSalvarEConcluir() {
        expectElementNotVisible(snackBarContent);
        clickAndHighlight(btnSalvarEConcluir);
    }

    public void validaIconeStatusSindicancia() {
        Assert.assertEquals(getAttribute(iconeStatusSindicancia, "alt"), "Finalizado");
    }

    public void clicarBtnVoltar() {
        clickAndHighlight(btnVoltar);
    }

    public void clicarBtnSalvarEConcluirSegPatrimonial() {
        clickAndHighlight(btnSalvarEConcluirSegPatrimonial);
    }

    public void validoBtnSalvarDisabled() {
        expectElementNotVisible(btnSalvarConclusao);
    }

    public void validaLabelConclusaoAnaliseTecnica(String label) {
        Assert.assertEquals(getText(labelConclusaoAnaliseTecnica), label);
    }
}
