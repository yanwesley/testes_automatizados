package br.com.pom.sro.sindicancias.inclusao_de_informacoes.vagoes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class MedidaDeRodasVagoesSindicanciaPage extends GeralPage {
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonButtonTabelaAnomaliasSair = By.id("button-tabela-anomalias-sair");
    private By tableTabelaMedidasRodas = By.id("tabela-medidas-rodas");
    private By inputMedidaRodasEspessuraFrisoCabAEsq1 = By.id("medida-rodas-espessura-friso-cab-a-esq1");
    private By inputMedidaRodasEspessuraFrisoCabADir1 = By.id("medida-rodas-espessura-friso-cab-a-dir1");
    private By inputMedidaRodasEspessuraFrisoCabAEsq2 = By.id("medida-rodas-espessura-friso-cab-a-esq2");
    private By inputMedidaRodasEspessuraFrisoCabADir2 = By.id("medida-rodas-espessura-friso-cab-a-dir2");
    private By inputMedidaRodasEspessuraFrisoCabBEsq1 = By.id("medida-rodas-espessura-friso-cab-b-esq1");
    private By inputMedidaRodasEspessuraFrisoCabBDir1 = By.id("medida-rodas-espessura-friso-cab-b-dir1");
    private By inputMedidaRodasEspessuraFrisoCabBEsq2 = By.id("medida-rodas-espessura-friso-cab-b-esq2");
    private By inputMedidaRodasEspessuraFrisoCabBDir2 = By.id("medida-rodas-espessura-friso-cab-b-dir2");
    private By inputMedidaDiferencaFriso1 = By.id("medida-diferenca-friso1");
    private By inputMedidaDiferencaFriso2 = By.id("medida-diferenca-friso2");
    private By inputMedidaDiferencaFriso3 = By.id("medida-diferenca-friso3");
    private By inputMedidaDiferencaFriso4 = By.id("medida-diferenca-friso4");
    private By inputMedidaRodasAlturaFrisoCabAEsq1 = By.id("medida-rodas-altura-friso-cab-a-esq1");
    private By inputMedidaRodasAlturaFrisoCabADir1 = By.id("medida-rodas-altura-friso-cab-a-dir1");
    private By inputMedidaRodasAlturaFrisoCabAEsq2 = By.id("medida-rodas-altura-friso-cab-a-esq2");
    private By inputMedidaRodasAlturaFrisoCabADir2 = By.id("medida-rodas-altura-friso-cab-a-dir2");
    private By inputMedidaRodasAlturaFrisoCabBEsq1 = By.id("medida-rodas-altura-friso-cab-b-esq1");
    private By inputMedidaRodasAlturaFrisoCabBDir1 = By.id("medida-rodas-altura-friso-cab-b-dir1");
    private By inputMedidaRodasAlturaFrisoCabBEsq2 = By.id("medida-rodas-altura-friso-cab-b-esq2");
    private By inputMedidaRodasAlturaFrisoCabBDir2 = By.id("medida-rodas-altura-friso-cab-b-dir2");
    private By inputMedidaRodasBandagemCabAEsq1 = By.id("medida-rodas-bandagem-cab-a-esq1");
    private By inputMedidaRodasBandagemCabADir1 = By.id("medida-rodas-bandagem-cab-a-dir1");
    private By inputMedidaRodasBandagemCabAEsq2 = By.id("medida-rodas-bandagem-cab-a-esq2");
    private By inputMedidaRodasBandagemCabADir2 = By.id("medida-rodas-bandagem-cab-a-dir2");
    private By inputMedidaRodasBandagemCabBEsq1 = By.id("medida-rodas-bandagem-cab-b-esq1");
    private By inputMedidaRodasBandagemCabBDir1 = By.id("medida-rodas-bandagem-cab-b-dir1");
    private By inputMedidaRodasBandagemCabBEsq2 = By.id("medida-rodas-bandagem-cab-b-esq2");
    private By inputMedidaRodasBandagemCabBDir2 = By.id("medida-rodas-bandagem-cab-b-dir2");
    private By inputMedidaRodasLarguraCabAEsq1 = By.id("medida-rodas-largura-cab-a-esq1");
    private By inputMedidaRodasLarguraCabADir1 = By.id("medida-rodas-largura-cab-a-dir1");
    private By inputMedidaRodasLarguraCabAEsq2 = By.id("medida-rodas-largura-cab-a-esq2");
    private By inputMedidaRodasLarguraCabADir2 = By.id("medida-rodas-largura-cab-a-dir2");
    private By inputMedidaRodasLarguraCabBEsq1 = By.id("medida-rodas-largura-cab-b-esq1");
    private By inputMedidaRodasLarguraCabBDir1 = By.id("medida-rodas-largura-cab-b-dir1");
    private By inputMedidaRodasLarguraCabBEsq2 = By.id("medida-rodas-largura-cab-b-esq2");
    private By inputMedidaRodasLarguraCabBDir2 = By.id("medida-rodas-largura-cab-b-dir2");
    private By inputMedidaRodasConcavidadeCabAEsq1 = By.id("medida-rodas-concavidade-cab-a-esq1");
    private By inputMedidaRodasConcavidadeCabADir1 = By.id("medida-rodas-concavidade-cab-a-dir1");
    private By inputMedidaRodasConcavidadeCabAEsq2 = By.id("medida-rodas-concavidade-cab-a-esq2");
    private By inputMedidaRodasConcavidadeCabADir2 = By.id("medida-rodas-concavidade-cab-a-dir2");
    private By inputMedidaRodasConcavidadeCabBEsq1 = By.id("medida-rodas-concavidade-cab-b-esq1");
    private By inputMedidaRodasConcavidadeCabBDir1 = By.id("medida-rodas-concavidade-cab-b-dir1");
    private By inputMedidaRodasConcavidadeCabBEsq2 = By.id("medida-rodas-concavidade-cab-b-esq2");
    private By inputMedidaRodasConcavidadeCabBDir2 = By.id("medida-rodas-concavidade-cab-b-dir2");
    private By inputBitolaM1a1 = By.id("bitola-m1a1");
    private By inputBitolaM1a2 = By.id("bitola-m1a2");
    private By inputBitolaM1b3 = By.id("bitola-m1b3");
    private By inputBitolaM1b4 = By.id("bitola-m1b4");
    private By inputBitolaM2a1 = By.id("bitola-m2a1");
    private By inputBitolaM2a2 = By.id("bitola-m2a2");
    private By inputBitolaM2b3 = By.id("bitola-m2b3");
    private By inputBitolaM2b4 = By.id("bitola-m2b4");
    private By inputBitolaM3a1 = By.id("bitola-m3a1");
    private By inputBitolaM3a2 = By.id("bitola-m3a2");
    private By inputBitolaM3b3 = By.id("bitola-m3b3");
    private By inputBitolaM3b4 = By.id("bitola-m3b4");
    private By buttonDialogButtonSave = By.id("dialog-button-save");
    private By buttonDialogButtonSaveAndClose = By.id("dialog-button-save-and-close");
    private By buttonDialogButtonCancel = By.id("dialog-button-cancel");

}
