package br.com.pom.sro.sindicancias.inclusao_de_informacoes.questionario_maquinista;

import br.com.api.model.sindicancia.questionario.QuestionarioMaquinista;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class InformarcoesQuestionariosSindicanciaPage extends GeralPage {
    private By separatorInformacoesDoMaquinista = By.name("Informações do maquinista");
    private By radioGroupMaquinistaPraticante = By.id("maquinistaPraticante");
    private By radioButtonMaquinistaPraticanteSim = By.id("maquinistaPraticante-sim");
    private By inputMaquinistaPraticanteSimInput = By.id("maquinistaPraticante-sim-input");
    private By radioButtonMaquinistaPraticanteNao = By.id("maquinistaPraticante-nao");
    private By inputMaquinistaPraticanteNaoInput = By.id("maquinistaPraticante-nao-input");
    private By sroExpandableTopicSeparatorQuestionarioMaquinista = By.id("questionario-maquinista");
    private By sroExpandableTopicSeparatorQuestionarioFalhaHumana = By.id("questionario-falha-humana");
    private By divQuestionarioMaquinistaContentToggle = By.id("questionario-maquinista-content-toggle");
    private By divQuestionarioFalhaHumanaContentToggle = By.id("questionario-falha-humana-content-toggle");
    private By buttonButtonSalvarRascunho = By.id("button-salvar-rascunho");
    private By buttonButtonSalvar = By.id("button-salvar");
    private By buttonButtonSalvarAvancar = By.id("button-salvar-avancar");
    private By buttonButtonCancelar = By.id("button-cancelar");
    private By separatorInformacoesDoManobrador = By.name("Informações do manobrador");
    private By sroExpandableTopicSeparatorQuestionarioManobrador = By.id("questionario-manobrador");
    private By divQuestionarioManobradorContentToggle = By.id("questionario-manobrador-content-toggle");

    public void validaExibicaoDaPaginaQuestionarioMaquinista() {
        expectElementVisible(separatorInformacoesDoMaquinista);
        expectElementVisible(sroExpandableTopicSeparatorQuestionarioMaquinista);
        expectElementVisible(sroExpandableTopicSeparatorQuestionarioFalhaHumana);
    }

    public void preencherDados(QuestionarioMaquinista questionarioMaquinista) {
        if (questionarioMaquinista.isMaquinistaPraticante()) {
            clickWithAction(radioButtonMaquinistaPraticanteSim);
        } else {
            clickWithAction(radioButtonMaquinistaPraticanteSim);
        }
    }

    public void validarDados(QuestionarioMaquinista questionarioMaquinista) {
        if (questionarioMaquinista.isMaquinistaPraticante()) {
            Assert.assertTrue(isRadioChecked(radioButtonMaquinistaPraticanteSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonMaquinistaPraticanteSim));
        }
    }

    public void clicarExpandirQuestionarioMaquinista() {
        clickAndHighlight(divQuestionarioMaquinistaContentToggle);
    }

    public void clicarExpandirQuestionarioCheckListAcidente() {
        clickAndHighlight(divQuestionarioFalhaHumanaContentToggle);
    }

    public void clicarBtnSalvarRascunho() {
        clickAndHighlight(buttonButtonSalvarRascunho);
    }

    public void clicarBtnSalvar() {
        clickAndHighlight(buttonButtonSalvar);
    }

    public void validaExibicaoDaPaginaQuestionarioManobrador() {
        expectElementVisible(separatorInformacoesDoManobrador);
        expectElementVisible(sroExpandableTopicSeparatorQuestionarioManobrador);
        expectElementVisible(sroExpandableTopicSeparatorQuestionarioFalhaHumana);
    }

    public void clicarExpandirQuestionarioManobrador() {
        clickAndHighlight(divQuestionarioManobradorContentToggle);
    }
}
