package br.com.pom.sro.sindicancias.inclusao_de_informacoes.investigacao;

import br.com.api.model.sindicancia.investigacao.SobreOAMV;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class SobreOAMVInvestigacaoPage extends GeralPage {

    IntFunction<By> radioButtonLocalAMV = (index) -> By.id("localAmv-" + index);
    IntFunction<By> radioButtonLocalAMVValue = (index) -> By.cssSelector("#localAmv-" + index + " > label");
    IntFunction<By> radioButtonIluminacaoLocal = (index) -> By.id("iluminacaoLocal-" + index);
    IntFunction<By> radioButtonIluminacaoLocalValue = (index) -> By.cssSelector("#iluminacaoLocal-" + index + " > label");
    IntFunction<By> radioButtonPosicaoManobradorMomento = (index) -> By.id("posicaoManobradorMomento-" + index);
    IntFunction<By> radioButtonPosicaoManobradorMomentoValue = (index) -> By.cssSelector("#posicaoManobradorMomento-" + index + " > label");
    IntFunction<By> radioButtonQuemEstavaComando = (index) -> By.id("quemEstavaComando-" + index);
    IntFunction<By> radioButtonQuemEstavaComandoValue = (index) -> By.cssSelector("#quemEstavaComando-" + index + " > label");
    IntFunction<By> radioButtonCoberturaCauda = (index) -> By.id("coberturaCauda-" + index);
    IntFunction<By> radioButtonCoberturaCaudaValue = (index) -> By.cssSelector("#coberturaCauda-" + index + " > label");
    IntFunction<By> radioButtonFrenteLocomotiva = (index) -> By.id("frenteLocomotiva-" + index);
    IntFunction<By> radioButtonFrenteLocomotivaValue = (index) -> By.cssSelector("#frenteLocomotiva-" + index + " > label");

    private By radioGroupEnvolvendoAMV = By.id("envolvendoAMV");
    private By radioButtonEnvolvendoAMVSim = By.id("envolvendoAMV-sim");
    private By inputEnvolvendoAMVSimInput = By.id("envolvendoAMV-sim-input");
    private By radioButtonEnvolvendoAMVNao = By.id("envolvendoAMV-nao");
    private By inputEnvolvendoAMVNaoInput = By.id("envolvendoAMV-nao-input");
    private By radioGroupFrenteLocomotiva = By.id("frenteLocomotiva");
    private By radioGroupLocalAMV = By.id("localAmv");
    private By radioGroupIluminacaoLocal = By.id("iluminacaoLocal");
    private By radioGroupPosicaoManobradorMomento = By.id("posicaoManobradorMomento");
    private By radioGroupEpisAdequados = By.id("episAdequados");
    private By radioButtonEpisAdequadosSim = By.id("episAdequados-sim");
    private By inputEpisAdequadosSimInput = By.id("episAdequados-sim-input");
    private By radioButtonEpisAdequadosNao = By.id("episAdequados-nao");
    private By inputEpisAdequadosNaoInput = By.id("episAdequados-nao-input");
    private By radioGroupQuemEstavaComando = By.id("quemEstavaComando");
    private By textAreaUltimaOrdemRecebida = By.id("ultimaOrdemRecebida");
    private By radioGroupCoberturaCauda = By.id("coberturaCauda");

    private By radioGroupMaquinistaVisualizouChave = By.id("maquinistaVisualizouChave");
    private By radioButtonMaquinistaVisualizouChaveSim = By.id("maquinistaVisualizouChave-sim");
    private By inputMaquinistaVisualizouChaveSimInput = By.id("maquinistaVisualizouChave-sim-input");
    private By radioButtonMaquinistaVisualizouChaveNao = By.id("maquinistaVisualizouChave-nao");
    private By inputMaquinistaVisualizouChaveNaoInput = By.id("maquinistaVisualizouChave-nao-input");

    private By radioGroupManobradorVisualizouChave = By.id("manobradorVisualizouChave");
    private By radioButtonManobradorVisualizouChaveSim = By.id("manobradorVisualizouChave-sim");
    private By inputManobradorVisualizouChaveSimInput = By.id("manobradorVisualizouChave-sim-input");
    private By radioButtonManobradorVisualizouChaveNao = By.id("manobradorVisualizouChave-nao");
    private By inputManobradorVisualizouChaveNaoInput = By.id("manobradorVisualizouChave-nao-input");

    public void preencherDados(SobreOAMV sobreOAMV) {
        waitTime(500);
        if (sobreOAMV.isAcidenteEnvolvendoAMV()) {
            clickWithAction(radioButtonEnvolvendoAMVSim);

            waitTime(200);

            clickWithAction(radioButtonLocalAMV.apply(sobreOAMV.getLocalDoAMVIndex()));

            sobreOAMV.setLocalDoAMVValue(
                    getText(radioButtonLocalAMVValue.apply(
                            sobreOAMV.getLocalDoAMVIndex())
                    )
            );

            scrollToElement(radioGroupIluminacaoLocal);
            clickWithAction(radioButtonIluminacaoLocal.apply(sobreOAMV.getIluminacaoDoLocalIndex()));

            sobreOAMV.setIluminacaoDoLocalValue(
                    getText(radioButtonIluminacaoLocalValue.apply(
                            sobreOAMV.getIluminacaoDoLocalIndex())
                    )
            );

            scrollToElement(radioGroupPosicaoManobradorMomento);
            clickWithAction(radioButtonPosicaoManobradorMomento
                    .apply(sobreOAMV.getPosicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVIndex())
            );

            sobreOAMV
                    .setPosicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVValue(
                            getText(
                                    radioButtonPosicaoManobradorMomentoValue
                                            .apply(sobreOAMV.getPosicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVIndex())
                            )
                    );


            scrollToElement(radioGroupEpisAdequados);
            if (sobreOAMV.isTodosOsEPIsEramAdequadosParaAOperacao()) {
                clickWithAction(radioButtonEpisAdequadosSim);
            } else {
                clickWithAction(radioButtonEpisAdequadosNao);
            }

            scrollToElement(radioGroupQuemEstavaComando);
            clickWithAction(radioButtonQuemEstavaComando.apply(sobreOAMV.getQuemEstavaComandandoAManobraIndex()));

            sobreOAMV.setQuemEstavaComandandoAManobraValue(
                    getText(radioButtonQuemEstavaComandoValue
                            .apply(sobreOAMV.getQuemEstavaComandandoAManobraIndex())
                    )
            );

            scrollToElement(textAreaUltimaOrdemRecebida);
            sendKeysWithJavaScript(textAreaUltimaOrdemRecebida, sobreOAMV.getQualFoiAUltimaOrdemRecebidaPeloMaquinistaAntesDoEvento());

            scrollToElement(radioGroupCoberturaCauda);
            clickWithAction(radioButtonCoberturaCauda.apply(sobreOAMV.getHaviaCoberturaNaCaudaIndex()));
            sobreOAMV.setHaviaCoberturaNaCaudaValue(
                    getText(radioButtonCoberturaCaudaValue.apply(
                            sobreOAMV.getHaviaCoberturaNaCaudaIndex())
                    )
            );

            scrollToElement(radioGroupFrenteLocomotiva);
            clickWithAction(radioButtonFrenteLocomotiva.apply(sobreOAMV.getFrenteDaLocomotivaIndex()));
            sobreOAMV.setFrenteDaLocomotivaValue(
                    getText(radioButtonFrenteLocomotivaValue.apply(
                            sobreOAMV.getFrenteDaLocomotivaIndex())
                    )
            );

            scrollToElement(radioGroupMaquinistaVisualizouChave);
            if (sobreOAMV.isMaquinistaVisualizouAPosicaoDaChaveAMV()) {
                clickWithAction(radioButtonMaquinistaVisualizouChaveSim);
            } else {
                clickWithAction(radioButtonMaquinistaVisualizouChaveNao);
            }

            scrollToElement(radioGroupManobradorVisualizouChave);
            if (sobreOAMV.isManobradorVisualizouAPosicaoDaChaveAMV()) {
                clickWithAction(radioButtonManobradorVisualizouChaveSim);
            } else {
                clickWithAction(radioButtonManobradorVisualizouChaveNao);
            }

        } else {

            clickWithAction(radioButtonEnvolvendoAMVNao);
        }

    }

    public void validarDados(SobreOAMV sobreOAMV) {
        scrollToElement(radioGroupEnvolvendoAMV);
        if (sobreOAMV.isAcidenteEnvolvendoAMV()) {

            Assert.assertTrue(isRadioChecked(radioButtonEnvolvendoAMVSim));

            scrollToElement(radioGroupLocalAMV);

            Assert.assertTrue(isRadioChecked(radioButtonLocalAMV.apply(sobreOAMV.getLocalDoAMVIndex())));
            Assert.assertEquals(getText(radioButtonLocalAMVValue.apply(
                    sobreOAMV.getLocalDoAMVIndex())
            ), sobreOAMV.getLocalDoAMVValue());


            Assert.assertTrue(isRadioChecked(radioButtonIluminacaoLocal.apply(sobreOAMV.getIluminacaoDoLocalIndex())));
            Assert.assertEquals(
                    getText(radioButtonIluminacaoLocalValue.apply(
                            sobreOAMV.getIluminacaoDoLocalIndex())
                    ), sobreOAMV.getIluminacaoDoLocalValue()
            );


            Assert.assertTrue(isRadioChecked(radioButtonPosicaoManobradorMomento
                    .apply(sobreOAMV.getPosicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVIndex())
            ));

            Assert.assertEquals(
                    getText(
                            radioButtonPosicaoManobradorMomentoValue
                                    .apply(sobreOAMV.getPosicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVIndex())
                    ), sobreOAMV.getPosicaoDoManobradorNoMomentoDoAcidenteOuQuebraDoAMVValue()
            );


            if (sobreOAMV.isTodosOsEPIsEramAdequadosParaAOperacao()) {
                Assert.assertTrue(isRadioChecked(radioButtonEpisAdequadosSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonEpisAdequadosNao));
            }


            Assert.assertTrue(isRadioChecked(radioButtonQuemEstavaComando.apply(sobreOAMV.getQuemEstavaComandandoAManobraIndex())));

            Assert.assertEquals(getText(radioButtonQuemEstavaComandoValue
                    .apply(sobreOAMV.getQuemEstavaComandandoAManobraIndex())
            ), sobreOAMV.getQuemEstavaComandandoAManobraValue());


            Assert.assertEquals(getValue(textAreaUltimaOrdemRecebida), sobreOAMV.getQualFoiAUltimaOrdemRecebidaPeloMaquinistaAntesDoEvento());


            Assert.assertTrue(isRadioChecked(radioButtonCoberturaCauda.apply(sobreOAMV.getHaviaCoberturaNaCaudaIndex())));
            Assert.assertEquals(getText(radioButtonCoberturaCaudaValue.apply(
                    sobreOAMV.getHaviaCoberturaNaCaudaIndex())
            ), sobreOAMV.getHaviaCoberturaNaCaudaValue());

            Assert.assertTrue(isRadioChecked(radioButtonFrenteLocomotiva.apply(sobreOAMV.getFrenteDaLocomotivaIndex())));

            Assert.assertEquals(getText(radioButtonFrenteLocomotivaValue.apply(
                    sobreOAMV.getFrenteDaLocomotivaIndex())
            ), sobreOAMV.getFrenteDaLocomotivaValue());


            if (sobreOAMV.isMaquinistaVisualizouAPosicaoDaChaveAMV()) {
                Assert.assertTrue(isRadioChecked(radioButtonMaquinistaVisualizouChaveSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonMaquinistaVisualizouChaveNao));
            }

            if (sobreOAMV.isManobradorVisualizouAPosicaoDaChaveAMV()) {
                Assert.assertTrue(isRadioChecked(radioButtonManobradorVisualizouChaveSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonManobradorVisualizouChaveNao));
            }

        } else {
            Assert.assertTrue(isRadioChecked(radioButtonEnvolvendoAMVNao));
        }
    }
}
