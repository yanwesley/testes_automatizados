package br.com.pom.sro.sindicancias.inclusao_de_informacoes.investigacao;

import br.com.api.model.sindicancia.investigacao.DadosDoManobradorAjudante;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class DadosDoManobradorAjudantePage extends GeralPage {

    private By radioGroupManobradorRumo = By.id("manobradorRumo");
    private By radioButtonManobradorRumoSim = By.id("manobradorRumo-sim");
    private By inputManobradorRumoSimInput = By.id("manobradorRumo-sim-input");
    private By radioButtonManobradorRumoNao = By.id("manobradorRumo-nao");
    private By inputManobradorRumoNaoInput = By.id("manobradorRumo-nao-input");
    private By inputNomeManobrador = By.id("nomeManobrador");
    private By buttonButtonSearchManobrador = By.id("button-search-manobrador");
    private By inputMatriculaManobrador = By.id("matriculaManobrador");
    private By inputTeSimuladoAssociadaCausaManobrador = By.id("teSimuladoAssociadaCausaManobrador");
    private By sroTitleLabelValoresDeTestes = By.name("Valores de testes");
    private By inputTesteSofridoAtualManobrador = By.id("testeSofridoAtualManobrador");
    private By inputTesteSofridoM1Manobrador = By.id("testeSofridoM1Manobrador");
    private By inputTesteSofridoM2Manobrador = By.id("testeSofridoM2Manobrador");
    private By inputTesteSofridoRelacionadoAcidenteAtualManobrador = By.id("testeSofridoRelacionadoAcidenteAtualManobrador");
    private By inputTesteSofridoRelacionadoAcidenteM1Manobrador = By.id("testeSofridoRelacionadoAcidenteM1Manobrador");
    private By inputTesteSofridoRelacionadoAcidenteM2Manobrador = By.id("testeSofridoRelacionadoAcidenteM2Manobrador");
    private By inputOutrosTestesInsatisfatoriosAtualManobrador = By.id("outrosTestesInsatisfatoriosAtualManobrador");
    private By inputOutrosTestesInsatisfatoriosM1Manobrador = By.id("outrosTestesInsatisfatoriosM1Manobrador");
    private By inputOutrosTestesInsatisfatoriosM2Manobrador = By.id("outrosTestesInsatisfatoriosM2Manobrador");
    private By inputNotaProvaAtualManobrador = By.id("notaProvaAtualManobrador");
    private By inputNotaProvaM1Manobrador = By.id("notaProvaM1Manobrador");
    private By inputNotaProvaM2Manobrador = By.id("notaProvaM2Manobrador");
    private By inputDataReciclagemROManobrador = By.id("dataReciclagemROManobrador");
    private By inputNotaUltimaReciclagemManobrador = By.id("notaUltimaReciclagemManobrador");
    private By inputDataUltimaReciclagemRelacionadoAcidenteManobrador = By.id("dataUltimaReciclagemRelacionadoAcidenteManobrador");
    private By inputNotaUltimaReciclagemRelacionadoAcidenteManobrador = By.id("notaUltimaReciclagemRelacionadoAcidenteManobrador");
    private By textAreaQuestoesRespondidasIncorretamenteManobrador = By.id("questoesRespondidasIncorretamenteManobrador");
    private By inputHorasServicoMomentoAcidenteManobrador = By.id("horasServicoMomentoAcidenteManobrador");
    private By inputDiasAposUltimoDescansoManobrador = By.id("diasAposUltimoDescansoManobrador");
    private By inputMesesAposFeriasManobrador = By.id("mesesAposFeriasManobrador");

    public void preencherDados(DadosDoManobradorAjudante dadosDoManobradorAjudante) {
        waitTime(300);
        if (dadosDoManobradorAjudante.isoManobradorOuMaquinistaSaoDaRumo()) {
            clickWithAction(radioButtonManobradorRumoSim);
            clickAndHighlight(buttonButtonSearchManobrador);
            selecionarResponsavel(dadosDoManobradorAjudante.getNomeDoManobradorOuMaquinista());
            dadosDoManobradorAjudante.setNomeDoManobradorOuMaquinista(getValue(inputNomeManobrador));
            dadosDoManobradorAjudante.setCS(getValue(inputMatriculaManobrador));
        } else {
            clickWithAction(radioButtonManobradorRumoNao);
            sendKeys(inputNomeManobrador, dadosDoManobradorAjudante.getNomeDoManobradorOuMaquinista());
            sendKeys(inputMatriculaManobrador, dadosDoManobradorAjudante.getDocumento());
        }

        sendKeys(inputTeSimuladoAssociadaCausaManobrador, dadosDoManobradorAjudante.getTESimuladoAssociadaACausa());

        sendKeys(inputTesteSofridoAtualManobrador, dadosDoManobradorAjudante.getTestesSofridosMesAtual());
        sendKeys(inputTesteSofridoM1Manobrador, dadosDoManobradorAjudante.getTestesSofridosMesM1());
        sendKeys(inputTesteSofridoM2Manobrador, dadosDoManobradorAjudante.getTestesSofridosMesM2());

        sendKeys(inputTesteSofridoRelacionadoAcidenteAtualManobrador, dadosDoManobradorAjudante.getTestesSofridosRelacionadoComOAcidenteMesAtual());
        sendKeys(inputTesteSofridoRelacionadoAcidenteM1Manobrador, dadosDoManobradorAjudante.getTestesSofridosRelacionadoComOAcidenteMesM1());
        sendKeys(inputTesteSofridoRelacionadoAcidenteM2Manobrador, dadosDoManobradorAjudante.getTestesSofridosRelacionadoComOAcidenteMesM2());

        sendKeys(inputOutrosTestesInsatisfatoriosAtualManobrador, dadosDoManobradorAjudante.getOutrosTestesInsatisfatoriosMesAtual());
        sendKeys(inputOutrosTestesInsatisfatoriosM1Manobrador, dadosDoManobradorAjudante.getOutrosTestesInsatisfatoriosMesM1());
        sendKeys(inputOutrosTestesInsatisfatoriosM2Manobrador, dadosDoManobradorAjudante.getOutrosTestesInsatisfatoriosMesM2());

        sendKeys(inputNotaProvaAtualManobrador, dadosDoManobradorAjudante.getNotaProvaMesAtual());
        sendKeys(inputNotaProvaM1Manobrador, dadosDoManobradorAjudante.getNotaProvaMesM1());
        sendKeys(inputNotaProvaM2Manobrador, dadosDoManobradorAjudante.getNotaProvaMesM2());

        clearForce(inputDataReciclagemROManobrador);
        sendKeys(inputDataReciclagemROManobrador, dadosDoManobradorAjudante.getDataUltimaReciclagemOuTreinamentoRO());
        sendKeys(inputNotaUltimaReciclagemManobrador, dadosDoManobradorAjudante.getNotaUltimaReciclagemRO());

        clearForce(inputDataUltimaReciclagemRelacionadoAcidenteManobrador);
        sendKeys(inputDataUltimaReciclagemRelacionadoAcidenteManobrador, dadosDoManobradorAjudante.getDataUltimaReciclagemOuTreinamentoPORelacionadoAoAcidente());
        sendKeys(inputNotaUltimaReciclagemRelacionadoAcidenteManobrador, dadosDoManobradorAjudante.getNotaUltimaReciclagemPORelacionadoAoAcidente());

        sendKeysWithJavaScript(textAreaQuestoesRespondidasIncorretamenteManobrador, dadosDoManobradorAjudante.getQuestoesRespondidasIncorretamente());

        sendKeys(inputHorasServicoMomentoAcidenteManobrador, dadosDoManobradorAjudante.getHorasDeServicosNoMomentoDoAcidente());
        sendKeys(inputDiasAposUltimoDescansoManobrador, dadosDoManobradorAjudante.getDiasAposOUltimoDescansoSemanal());
        sendKeys(inputMesesAposFeriasManobrador, dadosDoManobradorAjudante.getMesesAposAsUltimasFerias());
    }

    public void validarDados(DadosDoManobradorAjudante dadosDoManobradorAjudante) {
        scrollToElement(radioGroupManobradorRumo);
        if (dadosDoManobradorAjudante.isoManobradorOuMaquinistaSaoDaRumo()) {
            Assert.assertTrue(isRadioChecked(radioButtonManobradorRumoSim));
            Assert.assertEquals(getValue(inputMatriculaManobrador), dadosDoManobradorAjudante.getCS());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonManobradorRumoNao));
            Assert.assertEquals(getValue(inputMatriculaManobrador), dadosDoManobradorAjudante.getDocumento());
        }
        Assert.assertEquals(getValue(inputNomeManobrador), dadosDoManobradorAjudante.getNomeDoManobradorOuMaquinista());

        Assert.assertEquals(getValue(inputTeSimuladoAssociadaCausaManobrador), dadosDoManobradorAjudante.getTESimuladoAssociadaACausa());

        Assert.assertEquals(getValue(inputTesteSofridoAtualManobrador), dadosDoManobradorAjudante.getTestesSofridosMesAtual());
        Assert.assertEquals(getValue(inputTesteSofridoM1Manobrador), dadosDoManobradorAjudante.getTestesSofridosMesM1());
        Assert.assertEquals(getValue(inputTesteSofridoM2Manobrador), dadosDoManobradorAjudante.getTestesSofridosMesM2());

        Assert.assertEquals(getValue(inputTesteSofridoRelacionadoAcidenteAtualManobrador), dadosDoManobradorAjudante.getTestesSofridosRelacionadoComOAcidenteMesAtual());
        Assert.assertEquals(getValue(inputTesteSofridoRelacionadoAcidenteM1Manobrador), dadosDoManobradorAjudante.getTestesSofridosRelacionadoComOAcidenteMesM1());
        Assert.assertEquals(getValue(inputTesteSofridoRelacionadoAcidenteM2Manobrador), dadosDoManobradorAjudante.getTestesSofridosRelacionadoComOAcidenteMesM2());

        Assert.assertEquals(getValue(inputOutrosTestesInsatisfatoriosAtualManobrador), dadosDoManobradorAjudante.getOutrosTestesInsatisfatoriosMesAtual());
        Assert.assertEquals(getValue(inputOutrosTestesInsatisfatoriosM1Manobrador), dadosDoManobradorAjudante.getOutrosTestesInsatisfatoriosMesM1());
        Assert.assertEquals(getValue(inputOutrosTestesInsatisfatoriosM2Manobrador), dadosDoManobradorAjudante.getOutrosTestesInsatisfatoriosMesM2());

        Assert.assertEquals(getValue(inputNotaProvaAtualManobrador), dadosDoManobradorAjudante.getNotaProvaMesAtual());
        Assert.assertEquals(getValue(inputNotaProvaM1Manobrador), dadosDoManobradorAjudante.getNotaProvaMesM1());
        Assert.assertEquals(getValue(inputNotaProvaM2Manobrador), dadosDoManobradorAjudante.getNotaProvaMesM2());

        Assert.assertEquals(getValue(inputDataReciclagemROManobrador), dadosDoManobradorAjudante.getDataUltimaReciclagemOuTreinamentoRO());
        Assert.assertEquals(getValue(inputNotaUltimaReciclagemManobrador), dadosDoManobradorAjudante.getNotaUltimaReciclagemRO());

        Assert.assertEquals(getValue(inputDataUltimaReciclagemRelacionadoAcidenteManobrador), dadosDoManobradorAjudante.getDataUltimaReciclagemOuTreinamentoPORelacionadoAoAcidente());
        Assert.assertEquals(getValue(inputNotaUltimaReciclagemRelacionadoAcidenteManobrador), dadosDoManobradorAjudante.getNotaUltimaReciclagemPORelacionadoAoAcidente());

        Assert.assertEquals(getValue(textAreaQuestoesRespondidasIncorretamenteManobrador), dadosDoManobradorAjudante.getQuestoesRespondidasIncorretamente());

        Assert.assertEquals(getValue(inputHorasServicoMomentoAcidenteManobrador), dadosDoManobradorAjudante.getHorasDeServicosNoMomentoDoAcidente());
        Assert.assertEquals(getValue(inputDiasAposUltimoDescansoManobrador), dadosDoManobradorAjudante.getDiasAposOUltimoDescansoSemanal());
        Assert.assertEquals(getValue(inputMesesAposFeriasManobrador), dadosDoManobradorAjudante.getMesesAposAsUltimasFerias());
    }
}
