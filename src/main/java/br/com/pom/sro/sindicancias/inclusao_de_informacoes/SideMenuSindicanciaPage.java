package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class SideMenuSindicanciaPage extends GeralPage {

    private By menuDados = By.id("side-menu-dados");
    private By menuAnaliseGrafica = By.id("side-menu-analise");
    private By menuFlambagem = By.id("side-menu-flambagem");
    private By menuAMV = By.id("side-menu-amv");
    private By menuTrilho = By.id("side-menu-trilho");
    private By menuCustos = By.id("side-menu-custos");
    private By menuRegistrosFotograficos = By.id("side-menu-registrofotografico");
    private By menuDepoimentos = By.id("side-menu-depoimento");
    private By menuConclusao = By.id("side-menu-conclusao");
    private By menuAnexos = By.id("side-menu-anexos");
    private By menuTO = By.id("side-menu-to");
    private By menuMeioAmbiente = By.id("side-menu-meioambiente");
    private By menuSST = By.id("side-menu-sst");
    private By menuControleDePerdas = By.id("side-menu-controleperdas");
    private By menuInvestigacao = By.id("side-menu-investigacao");
    private By menuLocomotiva = By.id("side-menu-locomotiva");
    private By menuVagoes = By.id("side-menu-vagoes");
    private By menuQuestionario = By.id("side-menu-questionario");
    private By subMenuQuestionarioMaquinista = By.id("side-sub-menu-questionariomaquinista");
    private By subMenuQuestionarioManobrador = By.id("side-sub-menu-questionariomanobrador");
    private By menuAnaliseDeViagem = By.id("side-menu-analise");
    private By menuCriticaDaViagem = By.id("side-menu-critica");
    private By menuRegistroAcidente = By.id("side-menu-registroacidente");
    private By menuHistorico = By.id("side-menu-historico");
    private By menuPlanoDeAcao = By.id("side-menu-planoacao");

    private By menuDescricao = By.id("side-menu-descricao");
    private By menuSobreLocal = By.id("side-menu-sobrelocal");
    private By menuSobreEnvolvidos = By.id("side-menu-sobreenvolvidos");
    private By menuAnaliseCausa = By.id("side-menu-analisecausas");

    /*
    financeiro
     */

    private By aSideMenuVia = By.id("side-menu-via");
    private By aSideMenuMecanica = By.id("side-menu-mecanica");

    public void clicarMenuCustos() {
        clickWithJavaScript(menuCustos);
    }

    public void clicarMenuFlambagem() {
        clickAndHighlight(menuFlambagem);
    }

    public void clicarMenuAnexos() {
        expectLoading();
        clickAndHighlight(menuAnexos);
    }

    public void clicarMenuConclusao() {
        clickAndHighlight(menuConclusao);
    }

    public void clicarMenuTO() {
        clickAndHighlight(menuTO);
    }

    public void clicarMenuMeioAmbiente() {
        clickAndHighlight(menuMeioAmbiente);
    }

    public void clicarMenuSST() {
        clickAndHighlight(menuSST);
    }

    public void clicarMenuControleDePerdas() {
        clickWithJavaScript(menuControleDePerdas);
    }

    public void clicarMenuRegistroFotografico() {
        clickAndHighlight(menuRegistrosFotograficos);
    }

    public void clicarMenuDepoimento() {
        expectLoading();
        clickAndHighlight(menuDepoimentos);
    }

    public void clicarMenuAMV() {
        clickAndHighlight(menuAMV);
    }

    public void clicarMenuTrilho() {
        clickAndHighlight(menuTrilho);
    }

    public void clicarMenuInvestigacao() {
        clickAndHighlight(menuInvestigacao);
    }

    public void clicarMenuLocomotiva() {
        clickAndHighlight(menuLocomotiva);
    }

    public void clicarMenuVagoes() {
        clickAndHighlight(menuVagoes);
    }

    public void clicarMenuAnaliseDeViagem() {
        clickAndHighlight(menuAnaliseDeViagem);
    }

    public void clicarMenuCriticaDaViagem() {
        clickAndHighlight(menuCriticaDaViagem);
    }

    public void clicarMenuQuestionario() {
        clickAndHighlight(menuQuestionario);
    }

    public void clicarSubMenuQuestionarioMaquinista() {
        clickAndHighlight(subMenuQuestionarioMaquinista);
    }

    public void clicarSubMenuQuestionarioManobrador() {
        clickAndHighlight(subMenuQuestionarioManobrador);
    }

    public void clicarMenuRegistroAcidente() {
        scrollToElement(menuRegistroAcidente);
        clickAndHighlight(menuRegistroAcidente);
    }

    public void clicarSideMenuHistorico() {
        clickAndHighlight(menuHistorico);
    }

    public void clicarSideMenuDescricao() {
        clickAndHighlight(menuDescricao);
    }

    public void clicarSideMenuSobreOsEnvolvidos() {
        clickWithJavaScript(menuSobreEnvolvidos);
    }

    public void clicarSideMenuSobreOLocal() {
        clickAndHighlight(menuSobreLocal);
    }

    public void clicarSideMenuAnaliseCausa() {
        clickAndHighlight(menuAnaliseCausa);
    }

    public void clicarSideMenuPlanoDeAcao() {
        expectLoading();
        clickAndHighlight(menuPlanoDeAcao);
    }

    public void clicarSideMenuVia() {
        expectLoading();
        clickAndHighlight(aSideMenuVia);
    }

    public void clicarSideMenuMecanica() {
        expectLoading();
        clickAndHighlight(aSideMenuMecanica);
    }

}