package br.com.pom.sro.sindicancias.inclusao_de_informacoes.questionario_maquinista;

import br.com.api.model.sindicancia.questionario.QuestionarioCheckListAcidente;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class QuestionarioCheckListAcidenteSindicanciaPage extends GeralPage {

    private By radioGroupHouveFalhaHumana = By.id("houveFalhaHumana");
    private By radioButtonHouveFalhaHumanaSim = By.id("houveFalhaHumana-sim");
    private By radioButtonHouveFalhaHumanaNao = By.id("houveFalhaHumana-nao");
    private By textareaDescricaoBreveAcidente = By.id("descricaoBreveAcidente");
    private By radioGroupCausadoPor = By.id("causadoPor");
    private IntFunction<By> radioButtonCausadoPor = (index) -> By.id("causadoPor-" + index);
    private By radioGroupAtentoDuranteExecucao = By.id("atentoDuranteExecucao");
    private By radioButtonAtentoDuranteExecucaoSim = By.id("atentoDuranteExecucao-sim");
    private By radioButtonAtentoDuranteExecucaoNao = By.id("atentoDuranteExecucao-nao");
    private By radioGroupCansadoMomentoAcidente = By.id("cansadoMomentoAcidente");
    private By radioButtonCansadoMomentoAcidenteSim = By.id("cansadoMomentoAcidente-sim");
    private By radioButtonCansadoMomentoAcidenteNao = By.id("cansadoMomentoAcidente-nao");
    private By radioGroupDescansouCorretamenteDiaAnterior = By.id("descansouCorretamenteDiaAnterior");
    private By radioButtonDescansouCorretamenteDiaAnteriorSim = By.id("descansouCorretamenteDiaAnterior-sim");
    private By radioButtonDescansouCorretamenteDiaAnteriorNao = By.id("descansouCorretamenteDiaAnterior-nao");
    private By radioGroupHaviaProcedimentoAquiloExecutando = By.id("haviaProcedimentoAquiloExecutando");
    private By radioButtonHaviaProcedimentoAquiloExecutandoSim = By.id("haviaProcedimentoAquiloExecutando-sim");
    private By radioButtonHaviaProcedimentoAquiloExecutandoNao = By.id("haviaProcedimentoAquiloExecutando-nao");
    private By radioGroupConheciaProcedimento = By.id("conheciaProcedimento");
    private By radioButtonConheciaProcedimentoSim = By.id("conheciaProcedimento-sim");
    private By radioButtonConheciaProcedimentoNao = By.id("conheciaProcedimento-nao");
    private By textareaDescrevaProcedimento = By.id("descrevaProcedimento");
    private By radioGroupSeConsideraApto = By.id("seConsideraApto");
    private By radioButtonSeConsideraAptoSim = By.id("seConsideraApto-sim");
    private By radioButtonSeConsideraAptoNao = By.id("seConsideraApto-nao");
    private By radioGroupEstavaDuvidaProcedimento = By.id("estavaDuvidaProcedimento");
    private By radioButtonEstavaDuvidaProcedimentoSim = By.id("estavaDuvidaProcedimento-sim");
    private By radioButtonEstavaDuvidaProcedimentoNao = By.id("estavaDuvidaProcedimento-nao");
    private By radioGroupTeveTreinamentoAdequado = By.id("teveTreinamentoAdequado");
    private IntFunction<By> radioButtonTeveTreinamentoAdequado = (index) -> By.id("teveTreinamentoAdequado-" + index);

    private By radioGroupQuemDeuTreinamento = By.id("quemDeuTreinamento");
    private IntFunction<By> radioButtonQuemDeuTreinamento = (index) -> By.id("quemDeuTreinamento-" + index);

    private By radioGroupPediramQueDescumprisseProcedimento = By.id("pediramQueDescumprisseProcedimento");
    private By radioButtonPediramQueDescumprisseProcedimentoSim = By.id("pediramQueDescumprisseProcedimento-sim");
    private By radioButtonPediramQueDescumprisseProcedimentoNao = By.id("pediramQueDescumprisseProcedimento-nao");
    private By radioGroupSentiuPressionadoPorAlguemPara = By.id("sentiuPressionadoPorAlguemPara");
    private IntFunction<By> radioButtonSentiuPressionadoPorAlguemPara = (index) -> By.id("sentiuPressionadoPorAlguemPara-" + index);
    private By sroTitleLabelNaSuaOpiniaoQuaisForamAsCausasParaEsteAcidente = By.name("Na sua opinião, quais foram as causas para este acidente");
    private By radioGroupDistracao = By.id("distracao");
    private By radioButtonDistracaoSim = By.id("distracao-sim");
    private By radioButtonDistracaoNao = By.id("distracao-nao");
    private By radioGroupFatorExterno = By.id("fatorExterno");
    private By radioButtonFatorExternoSim = By.id("fatorExterno-sim");
    private By radioButtonFatorExternoNao = By.id("fatorExterno-nao");
    private By radioGroupFaltaConhecimento = By.id("faltaConhecimento");
    private By radioButtonFaltaConhecimentoSim = By.id("faltaConhecimento-sim");
    private By radioButtonFaltaConhecimentoNao = By.id("faltaConhecimento-nao");
    private By radioGroupExecucaoErradaProcedimento = By.id("execucaoErradaProcedimento");
    private By radioButtonExecucaoErradaProcedimentoSim = By.id("execucaoErradaProcedimento-sim");
    private By radioButtonExecucaoErradaProcedimentoNao = By.id("execucaoErradaProcedimento-nao");
    private By radioGroupFaltaProcedimento = By.id("faltaProcedimento");
    private By radioButtonFaltaProcedimentoSim = By.id("faltaProcedimento-sim");
    private By radioButtonFaltaProcedimentoNao = By.id("faltaProcedimento-nao");
    private By radioGroupMaQualidadeTreinamento = By.id("maQualidadeTreinamento");
    private By radioButtonMaQualidadeTreinamentoSim = By.id("maQualidadeTreinamento-sim");
    private By radioButtonMaQualidadeTreinamentoNao = By.id("maQualidadeTreinamento-nao");
    private By radioGroupNaoTeveTreinamento = By.id("naoTeveTreinamento");
    private By radioButtonNaoTeveTreinamentoSim = By.id("naoTeveTreinamento-sim");
    private By radioButtonNaoTeveTreinamentoNao = By.id("naoTeveTreinamento-nao");
    private By radioGroupFaltaEquipamentoExecutarProcedimento = By.id("faltaEquipamentoExecutarProcedimento");
    private By radioButtonFaltaEquipamentoExecutarProcedimentoSim = By.id("faltaEquipamentoExecutarProcedimento-sim");
    private By radioButtonFaltaEquipamentoExecutarProcedimentoNao = By.id("faltaEquipamentoExecutarProcedimento-nao");
    private By radioGroupDeveriaExecutarSemEquipamentos = By.id("deveriaExecutarSemEquipamentos");
    private By radioButtonDeveriaExecutarSemEquipamentosSim = By.id("deveriaExecutarSemEquipamentos-sim");
    private By radioButtonDeveriaExecutarSemEquipamentosNao = By.id("deveriaExecutarSemEquipamentos-nao");
    private By radioGroupPressaoExecutarTarefa = By.id("pressaoExecutarTarefa");
    private By radioButtonPressaoExecutarTarefaSim = By.id("pressaoExecutarTarefa-sim");
    private By radioButtonPressaoExecutarTarefaNao = By.id("pressaoExecutarTarefa-nao");
    private By textareaQualSuaSugestaoParaEvitar = By.id("qualSuaSugestaoParaEvitar");

    public void preencherDados(QuestionarioCheckListAcidente questionarioCheckListAcidente) {
        expectElementVisible(radioGroupHouveFalhaHumana);
        if (questionarioCheckListAcidente.isHouveFalhaHumana()) {
            clickWithAction(radioButtonHouveFalhaHumanaSim);
            waitTime(200);

            sendKeysWithJavaScript(textareaDescricaoBreveAcidente, questionarioCheckListAcidente.getDescrevaBrevementeOAcidente());

            questionarioCheckListAcidente.setVoceAcreditaQueOAcidenteFoiCausadoPorValue(
                    selectOptionAndReturnValue(radioButtonCausadoPor,
                            questionarioCheckListAcidente.getVoceAcreditaQueOAcidenteFoiCausadoPorIndex())
            );

            if (questionarioCheckListAcidente.isVoceConsideraQueEstavaAtentoDuranteAExecucaoDaTarefa()) {
                clickWithAction(radioButtonAtentoDuranteExecucaoSim);
            } else {
                clickWithAction(radioButtonAtentoDuranteExecucaoNao);
            }

            if (questionarioCheckListAcidente.isVoceEstavaCansadoNoMomentoDoAcidente()) {
                clickWithAction(radioButtonCansadoMomentoAcidenteSim);
            } else {
                clickWithAction(radioButtonCansadoMomentoAcidenteNao);
            }


            if (questionarioCheckListAcidente.isVoceDescansouCorretamenteNoDiaAnterior()) {
                clickWithAction(radioButtonDescansouCorretamenteDiaAnteriorSim);
            } else {
                clickWithAction(radioButtonDescansouCorretamenteDiaAnteriorNao);
            }

            if (questionarioCheckListAcidente.isHaviaUmProcedimentoParaAquiloQueVoceEstavaExecutando()) {
                clickWithAction(radioButtonHaviaProcedimentoAquiloExecutandoSim);


                if (questionarioCheckListAcidente.isVoceConheciaEsteProcedimento()) {
                    clickWithAction(radioButtonConheciaProcedimentoSim);

                    sendKeysWithJavaScript(textareaDescrevaProcedimento, questionarioCheckListAcidente.getDescrevaOProcedimentoAbaixo());
                } else {
                    clickWithAction(radioButtonConheciaProcedimentoNao);
                }


            } else {
                clickWithAction(radioButtonHaviaProcedimentoAquiloExecutandoNao);
            }


            if (questionarioCheckListAcidente.isVoceSeConsideraAptoARealizarEssaTarefa()) {
                clickWithAction(radioButtonSeConsideraAptoSim);
            } else {
                clickWithAction(radioButtonSeConsideraAptoNao);
            }

            if (questionarioCheckListAcidente.isVoceEstavaComDuvidaDoQueEstavaFazendo()) {
                clickWithAction(radioButtonEstavaDuvidaProcedimentoSim);
            } else {
                clickWithAction(radioButtonEstavaDuvidaProcedimentoNao);
            }


            questionarioCheckListAcidente.setVoceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaValue(
                    selectOptionAndReturnValue(radioButtonTeveTreinamentoAdequado,
                            questionarioCheckListAcidente.getVoceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaIndex())
            );

            if (questionarioCheckListAcidente.getVoceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaIndex() == 0) {

                questionarioCheckListAcidente.setQuemTeDeuOTreinamentoValue(
                        selectOptionAndReturnValue(radioButtonQuemDeuTreinamento,
                                questionarioCheckListAcidente.getQuemTeDeuOTreinamentoIndex())
                );
            }


            if (questionarioCheckListAcidente.isAlguemPediuParaVoceDescumprirAlgumProcedimentoParaExecutarEssaTarefa()) {
                clickWithAction(radioButtonPediramQueDescumprisseProcedimentoSim);
            } else {
                clickWithAction(radioButtonPediramQueDescumprisseProcedimentoNao);
            }


            questionarioCheckListAcidente.setVoceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezValue(
                    selectOptionAndReturnValue(radioButtonSentiuPressionadoPorAlguemPara,
                            questionarioCheckListAcidente.getVoceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezIndex())
            );


            if (questionarioCheckListAcidente.isDistracaoDesatencao()) {
                clickWithAction(radioButtonDistracaoSim);
            } else {
                clickWithAction(radioButtonDistracaoNao);
            }

            if (questionarioCheckListAcidente.isAlgumFatorExterno()) {
                clickWithAction(radioButtonFatorExternoSim);
            } else {
                clickWithAction(radioButtonFatorExternoNao);
            }

            if (questionarioCheckListAcidente.isFaltaDeConhecimentoDoProcedimento()) {
                clickWithAction(radioButtonFaltaConhecimentoSim);
            } else {
                clickWithAction(radioButtonFaltaConhecimentoNao);
            }

            if (questionarioCheckListAcidente.isExecucaoErradaDoProcedimento()) {
                clickWithAction(radioButtonExecucaoErradaProcedimentoSim);
            } else {
                clickWithAction(radioButtonExecucaoErradaProcedimentoNao);
            }

            if (questionarioCheckListAcidente.isFaltaDeProcedimento()) {
                clickWithAction(radioButtonFaltaProcedimentoSim);
            } else {
                clickWithAction(radioButtonFaltaProcedimentoNao);
            }

            if (questionarioCheckListAcidente.isMaQualidadeNoTreinamento()) {
                clickWithAction(radioButtonMaQualidadeTreinamentoSim);
            } else {
                clickWithAction(radioButtonMaQualidadeTreinamentoNao);
            }

            if (questionarioCheckListAcidente.isNaoTeveTreinamento()) {
                clickWithAction(radioButtonNaoTeveTreinamentoSim);
            } else {
                clickWithAction(radioButtonNaoTeveTreinamentoNao);
            }

            if (questionarioCheckListAcidente.isFaltaDeEquipamentosParaExecutarATarefaExecucaoErradaDoProcedimento()) {
                clickWithAction(radioButtonFaltaEquipamentoExecutarProcedimentoSim);

                if (questionarioCheckListAcidente.isVoceDeveriaTerExecutadoATarefaSemEquipamentos()) {
                    clickWithAction(radioButtonDeveriaExecutarSemEquipamentosSim);
                } else {
                    clickWithAction(radioButtonDeveriaExecutarSemEquipamentosNao);
                }

            } else {
                clickWithAction(radioButtonFaltaEquipamentoExecutarProcedimentoNao);
            }


            if (questionarioCheckListAcidente.isPressaoParaExecutarATarefa()) {
                clickWithAction(radioButtonPressaoExecutarTarefaSim);
            } else {
                clickWithAction(radioButtonPressaoExecutarTarefaNao);
            }

            sendKeysWithJavaScript(textareaQualSuaSugestaoParaEvitar, questionarioCheckListAcidente.getQualASuaSugestaoParaQueAcidentesComoEsseNaoOcorressemMaisNaNossaCompanhia());

        } else {
            clickWithAction(radioButtonHouveFalhaHumanaNao);
        }
    }

    public void validarDados(QuestionarioCheckListAcidente questionarioCheckListAcidente) {

        if (questionarioCheckListAcidente.isHouveFalhaHumana()) {
            Assert.assertTrue(isRadioChecked(radioButtonHouveFalhaHumanaSim));

            Assert.assertEquals(getValue(textareaDescricaoBreveAcidente), questionarioCheckListAcidente.getDescrevaBrevementeOAcidente());

            Assert.assertTrue(isRadioChecked(radioButtonCausadoPor.apply(
                    questionarioCheckListAcidente.getVoceAcreditaQueOAcidenteFoiCausadoPorIndex())
            ));

            if (questionarioCheckListAcidente.isVoceConsideraQueEstavaAtentoDuranteAExecucaoDaTarefa()) {
                Assert.assertTrue(isRadioChecked(radioButtonAtentoDuranteExecucaoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonAtentoDuranteExecucaoNao));
            }

            if (questionarioCheckListAcidente.isVoceEstavaCansadoNoMomentoDoAcidente()) {
                Assert.assertTrue(isRadioChecked(radioButtonCansadoMomentoAcidenteSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonCansadoMomentoAcidenteNao));
            }


            if (questionarioCheckListAcidente.isVoceDescansouCorretamenteNoDiaAnterior()) {
                Assert.assertTrue(isRadioChecked(radioButtonDescansouCorretamenteDiaAnteriorSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonDescansouCorretamenteDiaAnteriorNao));
            }

            if (questionarioCheckListAcidente.isHaviaUmProcedimentoParaAquiloQueVoceEstavaExecutando()) {
                Assert.assertTrue(isRadioChecked(radioButtonHaviaProcedimentoAquiloExecutandoSim));


                if (questionarioCheckListAcidente.isVoceConheciaEsteProcedimento()) {
                    Assert.assertTrue(isRadioChecked(radioButtonConheciaProcedimentoSim));

                    Assert.assertEquals(getValue(textareaDescrevaProcedimento), questionarioCheckListAcidente.getDescrevaOProcedimentoAbaixo());
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonConheciaProcedimentoNao));
                }


            } else {
                Assert.assertTrue(isRadioChecked(radioButtonHaviaProcedimentoAquiloExecutandoNao));
            }


            if (questionarioCheckListAcidente.isVoceSeConsideraAptoARealizarEssaTarefa()) {
                Assert.assertTrue(isRadioChecked(radioButtonSeConsideraAptoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonSeConsideraAptoNao));
            }

            if (questionarioCheckListAcidente.isVoceEstavaComDuvidaDoQueEstavaFazendo()) {
                Assert.assertTrue(isRadioChecked(radioButtonEstavaDuvidaProcedimentoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonEstavaDuvidaProcedimentoNao));
            }


            Assert.assertTrue(isRadioChecked(radioButtonTeveTreinamentoAdequado.apply(
                    questionarioCheckListAcidente.getVoceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaIndex())
            ));

            if (questionarioCheckListAcidente.getVoceAcreditaQueTeveTreinamentoAdequadoParaRealizarEstaTarefaIndex() == 0) {

                Assert.assertTrue(isRadioChecked(radioButtonQuemDeuTreinamento.apply(
                        questionarioCheckListAcidente.getQuemTeDeuOTreinamentoIndex())
                ));
            }


            if (questionarioCheckListAcidente.isAlguemPediuParaVoceDescumprirAlgumProcedimentoParaExecutarEssaTarefa()) {
                Assert.assertTrue(isRadioChecked(radioButtonPediramQueDescumprisseProcedimentoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonPediramQueDescumprisseProcedimentoNao));
            }


            Assert.assertTrue(isRadioChecked(radioButtonSentiuPressionadoPorAlguemPara.apply(
                    questionarioCheckListAcidente.getVoceSeSentiuPressionadoPorAlguemParaExecutarEssaTarefaComRapidezIndex())
            ));


            if (questionarioCheckListAcidente.isDistracaoDesatencao()) {
                Assert.assertTrue(isRadioChecked(radioButtonDistracaoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonDistracaoNao));
            }

            if (questionarioCheckListAcidente.isAlgumFatorExterno()) {
                Assert.assertTrue(isRadioChecked(radioButtonFatorExternoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonFatorExternoNao));
            }

            if (questionarioCheckListAcidente.isFaltaDeConhecimentoDoProcedimento()) {
                Assert.assertTrue(isRadioChecked(radioButtonFaltaConhecimentoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonFaltaConhecimentoNao));
            }

            if (questionarioCheckListAcidente.isExecucaoErradaDoProcedimento()) {
                Assert.assertTrue(isRadioChecked(radioButtonExecucaoErradaProcedimentoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonExecucaoErradaProcedimentoNao));
            }

            if (questionarioCheckListAcidente.isFaltaDeProcedimento()) {
                Assert.assertTrue(isRadioChecked(radioButtonFaltaProcedimentoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonFaltaProcedimentoNao));
            }

            if (questionarioCheckListAcidente.isMaQualidadeNoTreinamento()) {
                Assert.assertTrue(isRadioChecked(radioButtonMaQualidadeTreinamentoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonMaQualidadeTreinamentoNao));
            }

            if (questionarioCheckListAcidente.isNaoTeveTreinamento()) {
                Assert.assertTrue(isRadioChecked(radioButtonNaoTeveTreinamentoSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonNaoTeveTreinamentoNao));
            }

            if (questionarioCheckListAcidente.isFaltaDeEquipamentosParaExecutarATarefaExecucaoErradaDoProcedimento()) {
                Assert.assertTrue(isRadioChecked(radioButtonFaltaEquipamentoExecutarProcedimentoSim));

                if (questionarioCheckListAcidente.isVoceDeveriaTerExecutadoATarefaSemEquipamentos()) {
                    Assert.assertTrue(isRadioChecked(radioButtonDeveriaExecutarSemEquipamentosSim));
                } else {
                    Assert.assertTrue(isRadioChecked(radioButtonDeveriaExecutarSemEquipamentosNao));
                }

            } else {
                Assert.assertTrue(isRadioChecked(radioButtonFaltaEquipamentoExecutarProcedimentoNao));
            }


            if (questionarioCheckListAcidente.isPressaoParaExecutarATarefa()) {
                Assert.assertTrue(isRadioChecked(radioButtonPressaoExecutarTarefaSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonPressaoExecutarTarefaNao));
            }

            Assert.assertEquals(getValue(textareaQualSuaSugestaoParaEvitar), questionarioCheckListAcidente.getQualASuaSugestaoParaQueAcidentesComoEsseNaoOcorressemMaisNaNossaCompanhia());

        } else {
            Assert.assertTrue(isRadioChecked(radioButtonHouveFalhaHumanaNao));
        }
    }
}
