package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.AnaliseDeViagem;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class AnaliseDaViagemSindicanciaPage extends GeralPage {

    private By separatorAnaliseDaViagem = By.name("ANÁLISE DE VIAGEM");
    private By separatorAnaliseDaViagemComparativaUm = By.name("ANÁLISE DE VIAGEM COMPARATIVA 1");
    private By separatorAnaliseDaViagemComparativaDois = By.name("ANÁLISE DE VIAGEM COMPARATIVA 2");
    private By separatorLogDaLocomotiva = By.name("LOG DA LOCOMOTIVA");
    private By btnSalvarInformacoes = By.id("salvarDadosVia");
    private By inputNumeroDaViagem = By.id("numeroViagem");
    private By formAnaliseViagem = By.id("formAnaliseViagem");
    private By inputAnexarArquivoAnaliseViagem = By.id("input-file-upload-analise-viagem");
    private By inputAnexarArquivoAnaliseViagemComparativaUm = By.id("input-file-upload-analise-comparativa-um");
    private By inputAnexarArquivoAnaliseViagemComparativaDois = By.id("input-file-upload-analise-comparativa-dois");
    private By inputAnexarArquivoLogDaLocomotiva = By.id("input-file-upload-log-locomotiva");
    private By textAreaComentarioAnaliseViagem = By.id("observacaoAnaliseViagem");
    private By textAreaComentarioAnaliseViagemComparativaUm = By.id("observacaoAnaliseComparativa1");
    private By textAreaComentarioAnaliseViagemComparativaDois = By.id("observacaoAnaliseComparativa2");
    private By textAreaComentarioLogDaLocomotiva = By.id("observacaoLogLocomotiva");
    private By tableComentarioAnaliseViagem = By.cssSelector("#table-analise-viagem > tbody");
    private By tableComentarioAnaliseViagemComparativaUm = By.cssSelector("#table-analise-comparativa-um > tbody");
    private By tableComentarioAnaliseViagemComparativaDois = By.cssSelector("#table-analise-comparativa-dois > tbody");
    private By tableComentarioLogDaLocomotiva = By.cssSelector("#table-analise-log-locomotiva > tbody");


    public void validaQueEstaNaPaginaDeAnaliseDaViagem() {
        expectElementVisible(separatorAnaliseDaViagem);
    }

    public void clicarBtnSalvarInformacoes() {
        scrollToElement(separatorLogDaLocomotiva);
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void validaBotaoSalvarInformacoesDisabled() {
        Assert.assertTrue(isDisabled(btnSalvarInformacoes));
    }

    public void preencheAnaliseDeViagem(AnaliseDeViagem analiseDeViagem) {
        scrollToElement(separatorAnaliseDaViagem);
        sendKeys(inputNumeroDaViagem, analiseDeViagem.getNumeroViagem());
        uploadImagem(inputAnexarArquivoAnaliseViagem);
        sendKeysWithJavaScript(textAreaComentarioAnaliseViagem, analiseDeViagem.getComentarioAnaliseDeViagem());
    }

    public void preencheAnaliseDeViagemComparativaUm(AnaliseDeViagem analiseDeViagem) {
        scrollToElement(separatorAnaliseDaViagemComparativaUm);
        uploadImagem(inputAnexarArquivoAnaliseViagemComparativaUm);
        sendKeysWithJavaScript(textAreaComentarioAnaliseViagemComparativaUm, analiseDeViagem.getComentarioAnaliseDeViagemComparativaUm());
    }

    public void preencheAnaliseDeViagemComparativaDois(AnaliseDeViagem analiseDeViagem) {
        scrollToElement(separatorAnaliseDaViagemComparativaDois);
        uploadImagem(inputAnexarArquivoAnaliseViagemComparativaDois);
        sendKeysWithJavaScript(textAreaComentarioAnaliseViagemComparativaDois, analiseDeViagem.getComentarioAnaliseDeViagemComparativaDois());
    }

    public void preencheLogDaLocomotiva(AnaliseDeViagem analiseDeViagem) {
        scrollToElement(separatorLogDaLocomotiva);
        uploadImagem(inputAnexarArquivoLogDaLocomotiva);
        sendKeysWithJavaScript(textAreaComentarioLogDaLocomotiva, analiseDeViagem.getComentarioLogDaLocomotiva());
    }

    public void validoAnaliseDeViagem(AnaliseDeViagem analiseDeViagem) {
        scrollToElement(separatorAnaliseDaViagem);
        Assert.assertEquals(getValue(inputNumeroDaViagem), analiseDeViagem.getNumeroViagem());
        Assert.assertEquals(countChildElement(tableComentarioAnaliseViagem, "tr"), 1);
        Assert.assertEquals(getValue(textAreaComentarioAnaliseViagem), analiseDeViagem.getComentarioAnaliseDeViagem());
    }

    public void validoAnaliseDeViagemComparativaUm(AnaliseDeViagem analiseDeViagem) {
        scrollToElement(separatorAnaliseDaViagemComparativaUm);
        Assert.assertEquals(countChildElement(tableComentarioAnaliseViagemComparativaUm, "tr"), 1);
        Assert.assertEquals(getValue(textAreaComentarioAnaliseViagemComparativaUm), analiseDeViagem.getComentarioAnaliseDeViagemComparativaUm());
    }

    public void validoAnaliseDeViagemComparativaDois(AnaliseDeViagem analiseDeViagem) {
        scrollToElement(separatorAnaliseDaViagemComparativaDois);
        Assert.assertEquals(countChildElement(tableComentarioAnaliseViagemComparativaDois, "tr"), 1);
        Assert.assertEquals(getValue(textAreaComentarioAnaliseViagemComparativaDois), analiseDeViagem.getComentarioAnaliseDeViagemComparativaDois());
    }

    public void validoLogDaLocomotiva(AnaliseDeViagem analiseDeViagem) {
        scrollToElement(separatorLogDaLocomotiva);
        Assert.assertEquals(countChildElement(tableComentarioLogDaLocomotiva, "tr"), 1);
        Assert.assertEquals(getValue(textAreaComentarioLogDaLocomotiva), analiseDeViagem.getComentarioLogDaLocomotiva());
    }

    public void validaFormReadOnly() {
        isReadonlyForm(formAnaliseViagem, "input");
        isReadonlyForm(formAnaliseViagem, "textarea");
    }
}
