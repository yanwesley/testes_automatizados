package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class HeaderInformacoesSindicanciaPage extends GeralPage {

    private By btnFichaDoAcidente = By.id("button-ficha-acidente");

    private By radioGroupClassificacaoSindicancia = By.id("classificacaoSindicancia");
    private By radioButtonClassificacaoSindicanciaACIDENTE = By.id("classificacaoSindicancia-ACIDENTE");
    private By inputClassificacaoSindicanciaACIDENTEInput = By.id("classificacaoSindicancia-ACIDENTE-input");
    private By radioButtonClassificacaoSindicanciaOCORRENCIA = By.id("classificacaoSindicancia-OCORRENCIA");
    private By inputClassificacaoSindicanciaOCORRENCIAInput = By.id("classificacaoSindicancia-OCORRENCIA-input");
    private By btnConfirmar = By.id("dialog-button-save");
    private By btnFichaInvestigacao = By.id("button-ficha-investigacao");

    /*
    Header Classificação
     */
    private By divClassificacaoTag = By.id("classificacao-tag");
    private By buttonButtonEditClassificacaoSindicancia = By.id("button-edit-classificacao-sindicancia");
    private By divImportanciaTag = By.id("importancia-tag");
    private By buttonButtonEditImportanciaSindicancia = By.id("button-edit-importancia-sindicancia");
    private By labelClassificacaoValue = By.cssSelector("#classificacao-tag > span:nth-child(3)");

    /*
    Header Importância
     */
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By radioGroupImportanciaTrecho = By.id("importanciaTrecho");
    private By radioButtonImportanciaTrechoA = By.id("importanciaTrecho-A");
    private By inputImportanciaTrechoAInput = By.id("importanciaTrecho-A-input");
    private By radioButtonImportanciaTrechoB = By.id("importanciaTrecho-B");
    private By inputImportanciaTrechoBInput = By.id("importanciaTrecho-B-input");
    private By radioButtonImportanciaTrechoC = By.id("importanciaTrecho-C");
    private By inputImportanciaTrechoCInput = By.id("importanciaTrecho-C-input");
    private By radioButtonImportanciaTrechoD = By.id("importanciaTrecho-D");
    private By inputImportanciaTrechoDInput = By.id("importanciaTrecho-D-input");
    private By buttonDialogButtonCancel = By.id("dialog-button-cancel");
    private By buttonDialogButtonSave = By.id("dialog-button-save");
    private By labelClassificacaoVia = By.cssSelector("#importancia-tag > span:nth-child(2)");

    public void clicarBtnFichaAcidente() {
        clickAndHighlight(btnFichaDoAcidente);
        waitTime(1000);
    }

    public void alterarClassificacao(String classificacao) {
        clickAndHighlight(buttonButtonEditClassificacaoSindicancia);
        if (classificacao.equals("ACIDENTE")) {
            clickWithAction(radioButtonClassificacaoSindicanciaACIDENTE);
        } else {
            clickWithAction(radioButtonClassificacaoSindicanciaOCORRENCIA);
        }

    }

    public void clicarBtnConfirmarModal() {
        clickWithJavaScript(btnConfirmar);
    }

    public void exibirClassificacao(String classificacao) {
        Assert.assertEquals(getText(labelClassificacaoValue), (classificacao));
    }

    public void selecionarImportancia(String importância) {
        clickAndHighlight(buttonButtonEditImportanciaSindicancia);

        switch (importância) {
            case "A":
                clickWithAction(radioButtonImportanciaTrechoA);
                break;
            case "B":
                clickWithAction(radioButtonImportanciaTrechoB);
                break;
            case "C":
                clickWithAction(radioButtonImportanciaTrechoC);
                break;
            case "D":
                clickWithAction(radioButtonImportanciaTrechoD);
                break;
        }

    }

    public void exibirImportancia(String importância) {
        Assert.assertEquals(getText(labelClassificacaoVia), (importância));
    }

    public void clicarBtnFichaInvestigacao() {
        clickAndHighlight(btnFichaInvestigacao);
        switchToWindow();
    }
}