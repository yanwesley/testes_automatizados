package br.com.pom.sro.sindicancias.inclusao_de_informacoes.investigacao;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class InvestigacaoSindicanciaPage extends GeralPage {

    private By sroExpandableTopicSeparatorSobreOAmv = By.id("sobre-o-amv");
    private By divSobreOAmvContentToggle = By.id("sobre-o-amv-content-toggle");
    private By sroExpandableTopicSeparatorEstacionamento = By.id("estacionamento");
    private By divEstacionamentoContentToggle = By.id("estacionamento-content-toggle");
    private By sroExpandableTopicSeparatorFormacaoDoTrem = By.id("formacao-do-trem");
    private By divFormacaoDoTremContentToggle = By.id("formacao-do-trem-content-toggle");
    private By sroExpandableTopicSeparatorCargas = By.id("cargas");
    private By divCargasContentToggle = By.id("cargas-content-toggle");
    private By sroExpandableTopicSeparatorDadosDoMaquinistaCondutor = By.id("dados-do-maquinista-condutor");
    private By divDadosDoMaquinistaCondutorContentToggle = By.id("dados-do-maquinista-condutor-content-toggle");
    private By sroExpandableTopicSeparatorDadosDoSupervisorResponsavel = By.id("dados-do-supervisor-responsavel");
    private By divDadosDoSupervisorResponsavelContentToggle = By.id("dados-do-supervisor-responsavel-content-toggle");
    private By sroExpandableTopicSeparatorDadosDoManobradorAjudante = By.id("dados-do-manobrador-ajudante");
    private By divDadosDoManobradorAjudanteContentToggle = By.id("dados-do-manobrador-ajudante-content-toggle");
    private By buttonButtonSalvar = By.id("button-salvar");
    private By buttonButtonSalvarAvancar = By.id("button-salvar-avancar");
    private By buttonButtonSalvarRascunho = By.id("button-salvar-rascunho");

    public void clicarExpandirEstacionamento() {
        clickAndHighlight(divEstacionamentoContentToggle);
    }

    public void clicarExpandirFormacaoDoTrem() {
        clickAndHighlight(divFormacaoDoTremContentToggle);
    }

    public void clicarExpandirCargas() {
        clickAndHighlight(divCargasContentToggle);
    }

    public void clicarExpandirMaquinistaCondutor() {
        clickAndHighlight(divDadosDoMaquinistaCondutorContentToggle);
    }

    public void clicarExpandirSupervisorResponsavel() {
        clickAndHighlight(divDadosDoSupervisorResponsavelContentToggle);
    }

    public void clicarExpandirManobradorAjudante() {
        clickAndHighlight(divDadosDoManobradorAjudanteContentToggle);
    }

    public void clicarBtnSalvarRascunho() {
        clickAndHighlight(buttonButtonSalvarRascunho);
    }

    public void clicarExpandirSobreOAMV() {
        waitTime(1000);
        clickAndHighlight(divSobreOAmvContentToggle);
    }
}
