package br.com.pom.sro.sindicancias.inclusao_de_informacoes;

import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.api.model.sindicancia.RelatorioFinal;
import br.com.api.model.sindicancia.ReprovarSindicancia;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.List;
import java.util.function.IntFunction;

public class RelatorioFinalPage extends GeralPage {

    IntFunction<By> tdTableResponsaveis0Operacao = (index) -> By.id("table-responsaveis-0-operacao-" + index);
    IntFunction<By> tdTableResponsaveis0Complexo = (index) -> By.id("table-responsaveis-0-complexo-" + index);
    IntFunction<By> tdTableResponsaveis0Area = (index) -> By.id("table-responsaveis-0-area-" + index);
    IntFunction<By> tdTableResponsaveis0Gerente = (index) -> By.id("table-responsaveis-0-gerente-" + index);
    IntFunction<By> tdTableResponsaveis0Responsavel = (index) -> By.id("table-responsaveis-0-responsavel-" + index);
    IntFunction<By> buttonTableResponsaveis0Acoes = (index) -> By.id("table-responsaveis-0-acoes-" + index);


    IntFunction<By> optionMecanismoDescarrilhamento = (index) -> By.id("option-mecanismo-descarrilamento-" + index);
    IntFunction<By> optionBandeira = (index) -> By.id("option-bandeira-primaria-" + index);
    IntFunction<By> optionBandeiraContributiva1 = (index) -> By.id("option-bandeira-contributiva-01-" + index);
    IntFunction<By> causaContributiva = (index) -> By.id("option-causa-contributiva-01-" + index);
    IntFunction<By> areaResponsavelContributiva = (index) -> By.id("option-area-responsavel-contributiva-01-" + index);
    IntFunction<By> causaPrimaria = (index) -> By.id("option-causa-primaria-" + index);
    IntFunction<By> areaResponsavelPrimaria = (index) -> By.id("option-area-responsavel-primaria-" + index);
    private By conclusaoCipiaVia = By.id("conclusao-cipia-via");
    private By conclusaoCipiaMaterialRodante = By.id("conclusao-cipia-material-rodante");
    private By conclusaoCipiaOperacao = By.id("conclusao-cipia-operacao");
    private By sroExpandableTopicSeparatorConclusaoDasAreas = By.name("CONCLUSÃO DAS ÁREAS");
    private By divContentToggle = By.id("-content-toggle");
    private By sroTitleLabelLabelConclusaoCipiaVia = By.id("label-conclusao-cipia-via");
    private By divCipiaVia = By.name("CIPIA VIA");
    private By pConclusaoCipiaVia = By.id("conclusao-cipia-via");
    private By sroTitleLabelLabelConclusaoCipiaMaterialRodante = By.id("label-conclusao-cipia-material-rodante");
    private By divCipiaMaterialRodante = By.name("CIPIA MATERIAL RODANTE");
    private By pConclusaoCipiaMaterialRodante = By.id("conclusao-cipia-material-rodante");
    private By sroTitleLabelLabelConclusaoCipiaOperacao = By.id("label-conclusao-cipia-operacao");
    private By divCipiaOperacao = By.name("CIPIA OPERAÇÃO");
    private By pConclusaoCipiaOperacao = By.id("conclusao-cipia-operacao");
    private By sroTitleLabelCipiaSegurancaPatrimonial = By.name("CIPIA SEGURANÇA PATRIMONIAL");
    private By divCipiaSegurancaPatrimonial = By.name("CIPIA SEGURANÇA PATRIMONIAL");
    private By sroTopicSeparatorTituloDonoAcidente = By.id("titulo-dono-acidente");
    private By labelLabelResponsavelNome = By.id("label-dono-do-acidente");
    private By inputInputResponsavelNome = By.id("input-responsavelNome");
    private By buttonButtonSearchResponsavel = By.id("button-search-responsavel");
    private By inputInputInputResponsavelSuperior = By.id("input-input-responsavel-superior");
    private By inputInputInputResponsavelArea = By.id("input-input-responsavel-area");
    private By sroTopicSeparatorTituloDefinicaoCausas = By.id("titulo-definicao-causas");
    private By labelLabelFatoOcorrido = By.id("label-fato-ocorrido");
    private By textareaFatoOcorrido = By.id("fatoOcorrido");
    private By labelLabelMecanismoDescarrilamento = By.id("label-mecanismo-de-descarrilamento");
    private By selectSelectMecanismoDescarrilamento = By.id("select-mecanismo-descarrilamento");
    private By radioGroupDistribuicaoGravidade = By.id("distribuicao-gravidade");
    private By radioButtonDistribuicaoGravidadeSim = By.id("distribuicao-gravidade-sim");
    private By inputDistribuicaoGravidadeSimInput = By.id("distribuicao-gravidade-sim-input");
    private By radioButtonDistribuicaoGravidadeNao = By.id("distribuicao-gravidade-nao");
    private By inputDistribuicaoGravidadeNaoInput = By.id("distribuicao-gravidade-nao-input");
    private By labelLabelBandeiraPrimaria = By.id("label-bandeira-primaria");
    private By selectSelectBandeiraPrimaria = By.id("select-bandeira-primaria");
    private By labelLabelCausaPrimaria = By.id("label-causa-primaria");
    private By selectSelectCausaPrimaria = By.id("select-causa-primaria");
    private By labelLabelAreaResponsavelPrimaria = By.id("label-area-responsavel-primaria");
    private By selectSelectAreaResponsavelPrimaria = By.id("select-area-responsavel-primaria");
    private By inputCausaSindicanciaPrimariaPercentual = By.id("causaSindicanciaPrimariaPercentual");
    private By labelLabelBandeiraContributiva1 = By.id("label-bandeira-contributiva-1");
    private By selectSelectBandeiraContributiva1 = By.id("select-bandeira-contributiva-1");
    private By labelLabelCausa = By.id("label-causa");
    private By selectSelectCausaContributiva1 = By.id("select-causa-contributiva-1");
    private By labelLabelAreaResponsavelContributiva1 = By.id("label-area-responsavel-contributiva-1");
    private By selectSelectAreaResponsavelContributiva1 = By.id("select-area-responsavel-contributiva-1");
    private By inputInputPercentualCausaContributiva1 = By.id("input-percentual-causa-contributiva-1");
    private By h1ValorGravidadeCausa1 = By.id("valor-gravidade-causa-1");
    private By btnAdicionarCausaContributiva = By.id("btn-adicionar-causa-contributiva");
    private By labelLabelBandeiraContributiva2 = By.id("label-bandeira-contributiva-2");
    private By selectSelectBandeiraContributiva2 = By.id("select-bandeira-contributiva-2");
    private By selectSelectCausaContributiva2 = By.id("select-causa-contributiva-2");
    private By labelLabelAreaResponsavelContributiva2 = By.id("label-area-responsavel-contributiva-2");
    private By selectSelectAreaResponsavelContributiva2 = By.id("select-area-responsavel-contributiva-2");
    private By inputInputPercentualCausaContributiva2 = By.id("input-percentual-causa-contributiva-2");
    private By h1ValorGravidadeCausa2 = By.id("valor-gravidade-causa-2");
    private By labelLabelBandeiraContributiva3 = By.id("label-bandeira-contributiva-3");
    private By selectSelectBandeiraContributiva3 = By.id("select-bandeira-contributiva-3");
    private By selectSelectCausaContributiva3 = By.id("select-causa-contributiva-3");
    private By labelLabelAreaResponsavelContributiva3 = By.id("label-area-responsavel-contributiva-3");
    private By selectSelectAreaResponsavelContributiva3 = By.id("select-area-responsavel-contributiva-3");
    private By inputInputPercentualCausaContributiva3 = By.id("input-percentual-causa-contributiva-3");
    private By h1ValorGravidadeCausa3 = By.id("valor-gravidade-causa-3");
    private By labelLabelBandeiraContributiva4 = By.id("label-bandeira-contributiva-4");
    private By selectSelectBandeiraContributiva4 = By.id("select-bandeira-contributiva-4");
    private By selectSelectCausaContributiva4 = By.id("select-causa-contributiva-4");
    private By labelLabelAreaResponsavelContributiva4 = By.id("label-area-responsavel-contributiva-4");
    private By selectSelectAreaResponsavelContributiva4 = By.id("select-area-responsavel-contributiva-4");
    private By inputInputPercentualCausaContributiva4 = By.id("input-percentual-causa-contributiva-4");
    private By h1ValorGravidadeCausa4 = By.id("valor-gravidade-causa-4");
    private By buttonButtonSalvarInformacoes = By.id("button-salvar-informacoes");
    private By buttonButtonSalvarAvancar = By.id("button-salvar-avancar");
    private By buttonCancelarFlambagem = By.id("cancelarFlambagem");
    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By tabAnaliseTecnica = By.id("tab-analisetecnica");
    private By sideMenuRelatorioFinal = By.id("side-menu-relatoriofinal");
    private By conclusaoSegurancaPatrimonial = By.id("descricao-seguranca-patrimonial");
    private By bandeira1 = By.id("option-bandeira-primaria-0");
    private By btnExcluirCausaContributiva = By.id("CONTRIBUTIVA01-btn-delete");
    private By causaContributiva1Texto = By.cssSelector("body > sro-root > sro-shell > div > sro-detail-sindicancia > sro-analise-tecnica > div > div.col > div > sro-relatorio-final > form > div:nth-child(7) > div > sro-box-causa.ng-star-inserted > div.causa-box > div.tipo-causa > h1");
    private By btnBuscarResponsavel = By.id("button-search-employee");
    private By btnAprovarSindicancia = By.id("button-aprovar-sindicancia");
    private By checkAprovarInvestigacao = By.id("aprovar-investigacao");
    private By checkAprovarPlanoAcao = By.id("aprovar-plano-acao");
    private By btnProximoPasso = By.id("dialog-proximo-passo");
    private By checkAcaoPublica = By.id("acao-publica-0-input");
    private By checkAcaoPublicaLabel = By.cssSelector("#acao-publica-0 > label");
    private By btnAprovarSindicanciaModel = By.id("dialog-proximo-passo");
    private By btnConfirmarAprovacao = By.id("dialog-button-yes");
    private By expansionPanel = By.id("causa-primaria-content-toggle");
    private By labelBandeira = By.cssSelector("#select-bandeira-PRIMARIA > div > div.mat-select-value > span");
    private By labelCausa = By.cssSelector("#select-causa-PRIMARIA > div > div.mat-select-value > span > span");
    private By labelArea = By.cssSelector("#select-area-responsavel-PRIMARIA");
    private By abaFinalizados = By.id("tab-finalizados");
    private By btnDetalhesSindicanciaAnulada = By.id("button-edit-sindicancia-0");

    /*
    Aprovar Sindicância
     */

    private By divAbaAprovarSindicanciaAprovar = By.id("aba-aprovar-sindicancia-aprovar");
    private By divAbaAprovarSindicanciaPublicas = By.id("aba-aprovar-sindicancia-publicas");
    private By divAbaAprovarSindicanciaDesdobradas = By.id("aba-aprovar-sindicancia-desdobradas");
    private By divAbaAprovarSindicanciaConfirmacacao = By.id("aba-aprovar-sindicancia-confirmacacao");
    private By div1ParaAprovarASindicanciaConfirmeMarcandoOsCamposAbaixo = By.name("1 - Para aprovar a sindicância, confirme marcando os campos abaixo:");
    private By checkboxAprovarInvestigacao = By.id("aprovar-investigacao");
    private By checkboxAprovarInvestigacaoLabel = By.cssSelector("#aprovar-investigacao > label");
    private By inputAprovarInvestigacaoInput = By.id("aprovar-investigacao-input");
    private By checkboxAprovarPlanoAcao = By.id("aprovar-plano-acao");
    private By checkboxAprovarPlanoAcaoLabel = By.cssSelector("#aprovar-plano-acao > label");
    private By inputAprovarPlanoAcaoInput = By.id("aprovar-plano-acao-input");
    private By sroMessageBoxMsgBoxAprovacaoInvestigacao = By.cssSelector("#msg-box-aprovacao-investigacao > div > span");
    private By sroMessageBoxMsgBoxPublicas = By.cssSelector("#msg-box-publicas > div > span");
    private By sroMessageBoxMsgBoxDesdobradas = By.cssSelector("#msg-box-desdobradas > div > span");
    private By checkboxAcaoDesdobrada = By.cssSelector("#acao-toggle-0 > span");
    private By btnAdicionarResponsavel = By.id("button-0-add-responsavel");

    /*
    Reprovar Sindicância
     */
    private By btnReprovar = By.id("button-reprovar-avancar");
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By radioGroupReprovarInvestigacao = By.id("reprovar-investigacao");
    private By radioButtonReprovarInvestigação = By.id("radio-reprovar-investigacao");
    private By inputMatRadio15Input = By.id("mat-radio-15-input");
    private By btnReprovaSindicanciaModal = By.cssSelector("#dialog-button-yes");
    private By radioButtonReprovarPlanoCompleto = By.id("radio-reprovar-plano-completo");
    private By inputMatRadio16Input = By.id("mat-radio-16-input");
    private By radioButtonMatRadio17 = By.id("mat-radio-17");
    private By inputMatRadio17Input = By.id("mat-radio-17-input");
    private By buttonDialogButtonCancelar = By.id("dialog-button-cancelar");
    private By buttonConfirmarReprovacao = By.id("dialog-proximo-passo");
    private By radioButtonReprovarPlanoParcial = By.id("radio-reprovar-plano-parcial");
    private By inputMatRadio22Input = By.id("mat-radio-22-input");
    private By checkboxCipiaVia = By.id("cipia-via");
    private By inputCipiaViaInput = By.id("cipia-via-input");
    private By checkboxCipiaOperacao = By.id("cipia-operacao");
    private By inputCipiaOperacaoInput = By.id("cipia-operacao-input");
    private By checkboxCipiaMaterialRodante = By.id("cipia-material-rodante");
    private By inputCipiaMaterialRodanteInput = By.id("cipia-material-rodante-input");
    private By checkboxCipiaTecninca = By.id("cipia-tecninca");
    private By inputCipiaTecnincaInput = By.id("cipia-tecninca-input");
    private By checkboxSegurancaPatrimonial = By.id("seguranca-patrimonial");
    private By inputSegurancaPatrimonialInput = By.id("seguranca-patrimonial-input");
    private By textAreaJustificativaCompleto = By.id("justificativa-plano-completo");
    private By textAreaJustificativaInvestigacao = By.id("justificativa-investigacao");
    private By sroFileUploadUploadAnexosRecusarInvestigacao = By.id("upload-anexos-recusar-investigacao");
    private By inputInputFileUpload = By.id("input-file-upload");
    private By buttonBtnInputFileUpload = By.id("btn-input-file-upload");
    private By buttonBtnFileUploadSearch = By.id("btn-file-upload-search");
    private By ulFileUploadComponentListFiles = By.id("file-upload-component-list-files");
    private By radioButtonMatRadio23 = By.id("mat-radio-23");
    private By inputMatRadio23Input = By.id("mat-radio-23-input");
    private By radioButtonMatRadio24 = By.id("mat-radio-24");
    private By inputMatRadio24Input = By.id("mat-radio-24-input");
    private By checkBoxReprovarAcao = By.cssSelector("#acao-0");
    private By justificativaAutorizador = By.id("justificativaAutorizador");
    private By justificativaReprovarPlanoParcial = By.id("justificativa-plano-parcial");
    private By btnAnularSindicancia = By.id("button-anular");
    private By btnConfirmarAnulacao = By.id("dialog-button-confirm");
    private By labelAnulacaoDaSindicancia = By.cssSelector("body > sro-root > sro-shell > div > sro-detail-sindicancia > sro-box-anulacao-sindicancia > div > div > div:nth-child(1) > div > span");
    private By JustificativaDaAnulacao = By.id("anulacao-justificativaAutorizador");


    public void validoQueEstouNaPaginaRelatorio() {
        expectLoading();
        expectElementVisible(tabAnaliseTecnica);
        expectElementVisible(sideMenuRelatorioFinal);
    }

    public void validoOPreenchimentoConclusaoArea(String conclusao, String descricao) {
        Assert.assertEquals(getText(conclusaoCipiaVia), conclusao);
        Assert.assertEquals(getText(conclusaoCipiaMaterialRodante), conclusao);
        Assert.assertEquals(getText(conclusaoCipiaOperacao), conclusao);
        Assert.assertEquals(getText(conclusaoSegurancaPatrimonial), descricao);
    }

    public void preenchoODonoDoAcidente(RelatorioFinal relatorioFinal) {
        clickAndHighlight(buttonButtonSearchResponsavel);
        selecionarResponsavel(relatorioFinal.getResponsavel());
        relatorioFinal.setSuperior(getValue(inputInputInputResponsavelSuperior));
        relatorioFinal.setAreaDonoDoAcidente(getValue(inputInputInputResponsavelArea));
    }

    public void preenchoAsDefinicoesDeCausa(RelatorioFinal relatorioFinal) {
        sendKeysWithJavaScript(textareaFatoOcorrido, relatorioFinal.getFatoOcorrido());

        scrollToElement(selectSelectMecanismoDescarrilamento);
        relatorioFinal.setMecanismoDescarrilhamentoValue(selectOptionAndReturnValue(
                selectSelectMecanismoDescarrilamento,
                optionMecanismoDescarrilhamento,
                relatorioFinal.getMecanismoDescarrilhamentoIndex()
        ));

        if (relatorioFinal.isDistribuicaoDeGravidade()) {
            clickWithAction(radioButtonDistribuicaoGravidadeSim);
        } else {
            clickWithAction(radioButtonDistribuicaoGravidadeNao);
        }

        relatorioFinal.setBandeiraValue(selectOptionAndReturnValue(
                selectSelectBandeiraPrimaria,
                optionBandeira,
                relatorioFinal.getBandeiraIndex()
        ));

        scrollToElement(selectSelectCausaPrimaria);

        relatorioFinal.setCausaValue(selectOptionAndReturnValue(
                selectSelectCausaPrimaria,
                causaPrimaria,
                relatorioFinal.getCausaIndex()
        ));

        scrollToElement(selectSelectAreaResponsavelPrimaria);
        relatorioFinal.setAreaResponsavelValue(selectOptionAndReturnValue(
                selectSelectAreaResponsavelPrimaria,
                areaResponsavelPrimaria,
                relatorioFinal.getAreaResponsavelIndex()
        ));

    }

    public void adicionoMaisCausasContributivas(RelatorioFinal relatorioFinal) {
        scrollToElement(btnAdicionarCausaContributiva);
        clickAndHighlight(btnAdicionarCausaContributiva);

        relatorioFinal.setBandeiraValue(selectOptionAndReturnValue(
                selectSelectBandeiraContributiva1,
                optionBandeiraContributiva1,
                relatorioFinal.getBandeiraIndex()
        ));

        scrollToElement(selectSelectCausaContributiva1);
        relatorioFinal.setCausaValue(selectOptionAndReturnValue(
                selectSelectCausaContributiva1,
                causaContributiva,
                relatorioFinal.getCausaIndex()
        ));


        scrollToElement(selectSelectAreaResponsavelContributiva1);
        relatorioFinal.setAreaResponsavelValue(selectOptionAndReturnValue(
                selectSelectAreaResponsavelContributiva1,
                areaResponsavelContributiva,
                relatorioFinal.getAreaResponsavelIndex()
        ));
    }

    public void removoAsCausasContributivas() {
        scrollToElement(btnExcluirCausaContributiva);
        clickAndHighlight(btnExcluirCausaContributiva);
    }

    public void salvoAsInformacoes() {
        scrollToElement(buttonButtonSalvarInformacoes);
        clickAndHighlight(buttonButtonSalvarInformacoes);
    }

    public void validoQueAsInformacoesPersistiram(RelatorioFinal relatorioFinal) {
        Assert.assertEquals(getValue(inputInputResponsavelNome), relatorioFinal.getResponsavel());
        Assert.assertEquals(getValue(inputInputInputResponsavelSuperior), relatorioFinal.getSuperior());
        Assert.assertEquals(getValue(inputInputInputResponsavelArea), relatorioFinal.getAreaDonoDoAcidente());
        Assert.assertEquals(getValue(textareaFatoOcorrido), relatorioFinal.getFatoOcorrido());
        Assert.assertEquals(getText(selectSelectMecanismoDescarrilamento), relatorioFinal.getMecanismoDescarrilhamentoValue());

        Assert.assertEquals(getText(selectSelectBandeiraPrimaria), relatorioFinal.getBandeiraValue());
        Assert.assertEquals(getText(selectSelectCausaPrimaria), relatorioFinal.getCausaValue());
        Assert.assertEquals(getText(selectSelectAreaResponsavelPrimaria), relatorioFinal.getAreaResponsavelValue());
    }

    public void validarCausaContributiva(RelatorioFinal relatorioFinal) {
        scrollToElement(causaContributiva1Texto);
        Assert.assertEquals(getText(selectSelectCausaContributiva1), relatorioFinal.getCausaValue());
    }

    public void validarCausaContributivaRemovida() {
        scrollToElement(btnAdicionarCausaContributiva);
        expectElementNotVisible(causaContributiva1Texto);
    }

    public void clicarbtnAprovar() {
        clickAndHighlight(btnAprovarSindicancia);
    }

    public void aprovarInvestigacaoEPlanoAcao() {
        clickAndHighlight(checkboxAprovarInvestigacaoLabel);
        clickWithAction(checkboxAprovarPlanoAcaoLabel);
        clickAndHighlight(btnProximoPasso);
    }

    public void acaoNaoPublica() {
        clickAndHighlight(checkAcaoPublicaLabel);
        clickAndHighlight(btnProximoPasso);
    }

    public void validoConfirmacaoAprovacaoSindicancia(List<String> statusList) {
        Assert.assertEquals(getText(sroMessageBoxMsgBoxAprovacaoInvestigacao), statusList.get(0));
        Assert.assertEquals(getText(sroMessageBoxMsgBoxPublicas), statusList.get(1));
        Assert.assertEquals(getText(sroMessageBoxMsgBoxDesdobradas), statusList.get(2));
    }

    public void confirmoAprovacaoSindicancia() {
        clickAndHighlight(btnConfirmarAprovacao);
    }

    public void desdobroAAcao() {
        clickWithAction(checkboxAcaoDesdobrada);
    }

    public void clicarBtnAdicionarResponsavel() {
        clickWithAction(btnAdicionarResponsavel);
    }

    public void validoResponsavelAtribuido(ResponsavelAcao responsavelAcao, int linha) {
        linha--;
        Assert.assertEquals(getText(tdTableResponsaveis0Responsavel.apply(linha)), responsavelAcao.getResponsavel());
    }

    public void clicarBtnProximoPasso() {
        clickAndHighlight(btnProximoPasso);
    }

    public void clicarbtnAprovarDoModal() {
        clickAndHighlight(btnAprovarSindicanciaModel);
    }

    public void clicarBtnReprovar() {
        clickAndHighlight(btnReprovar);
    }

    public void clicarRadioButtonReprovarInvestigacao() {
        expectLoading();
        clickWithAction(radioButtonReprovarInvestigação);
    }

    public void escolhoUmaOuMaisAreasParaReprovar(ReprovarSindicancia reprovarSindicancia) {
        expectLoading();
        if (reprovarSindicancia.isCipiaVia()) {
            clickWithAction(checkboxCipiaVia);
        }
        if (reprovarSindicancia.isCipiaOperacao()) {
            clickWithAction(checkboxCipiaOperacao);
        }
        if (reprovarSindicancia.isCipiaMatRodante()) {
            clickWithAction(checkboxCipiaMaterialRodante);
        }
        if (reprovarSindicancia.isCipiaTecnica()) {
            clickWithAction(checkboxCipiaTecninca);
        }
        if (reprovarSindicancia.isSegPatrimonial()) {
            clickWithAction(checkboxSegurancaPatrimonial);
        }
    }

    public void preencherJustificativaReprovacao(ReprovarSindicancia reprovarSindicancia) {
        sendKeysWithJavaScript(textAreaJustificativaCompleto, reprovarSindicancia.getJustificativa());
    }

    public void envioUmArquivo() {
        uploadImagem();
    }

    public void confirmoReprovacao() {
        clickAndHighlight(buttonConfirmarReprovacao);
    }

    public void clicarRadioButtonReprovarPlanoCompleto() {
        expectLoading();
        clickWithAction(radioButtonReprovarPlanoCompleto);
    }

    public void confirmarReprovacaoModal() {
        clickAndHighlight(btnReprovaSindicanciaModal);
    }

    public void preencherJustificativaReprovacaoInvestigacao(ReprovarSindicancia reprovarSindicancia) {
        sendKeysWithJavaScript(textAreaJustificativaInvestigacao, reprovarSindicancia.getJustificativa());
    }

    public void clicarRadioButtonReprovarPlanoParcial() {
        expectLoading();
        clickWithAction(radioButtonReprovarPlanoParcial);
    }

    public void selecionoAcaoParaReprovar() {
        expectLoading();
        clickAndHighlight(checkBoxReprovarAcao);
    }

    public void preencherJustificativaReprovacaoParcial(ReprovarSindicancia reprovarSindicancia) {
        sendKeysWithJavaScript(justificativaReprovarPlanoParcial, reprovarSindicancia.getJustificativa());
    }

    public void validarPlanoDeAcao(RelatorioFinal relatorioFinal) {
        Assert.assertEquals(getText(labelLabelMecanismoDescarrilamento), relatorioFinal.getMecanismoDescarrilhamentoValue());
        Assert.assertEquals(getText(labelLabelFatoOcorrido), relatorioFinal.getFatoOcorrido());
        Assert.assertEquals(getText(labelLabelResponsavelNome), relatorioFinal.getResponsavel());
        clickAndHighlight(expansionPanel);
        Assert.assertEquals(getText(labelBandeira), relatorioFinal.getBandeiraValue());
        Assert.assertEquals(getText(labelCausa), relatorioFinal.getCausaValue());
        Assert.assertEquals(getValue(labelArea), relatorioFinal.getAreaResponsavelValue());
    }

    public void clicoBtnAnularSindicancia() {
        scrollToElement(btnAnularSindicancia);
        clickAndHighlight(btnAnularSindicancia);
    }

    public void preencherJustificativaAnulação(ReprovarSindicancia reprovarSindicancia) {
        sendKeysWithJavaScript(justificativaAutorizador, reprovarSindicancia.getJustificativaAnulacao());
    }

    public void clicarBtnConfirmarAnulacao() {
        scrollToElement(btnConfirmarAnulacao);
        clickAndHighlight(btnConfirmarAnulacao);
    }

    public void clicarNaAbaFinalizados() {
        clickAndHighlight(abaFinalizados);
    }

    public void clicarDetalhesSindicanciaAnulada() {
        clickAndHighlight(btnDetalhesSindicanciaAnulada);
    }

    public void validoAnulacaoSindicancia(ReprovarSindicancia reprovarSindicancia) {
        expectElementVisible(labelAnulacaoDaSindicancia);
        Assert.assertEquals(getText(JustificativaDaAnulacao), reprovarSindicancia.getJustificativaAnulacao());
    }
}




