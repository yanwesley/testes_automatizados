package br.com.pom.sro.informacoes;

import br.com.api.model.informacoes.MembroEquipe;
import br.com.api.model.informacoes.SobreOSetor;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class SobreInformacoesPage extends GeralPage {

    private By separatorSobreOSetor = By.name("SOBRE O SETOR");
    private By divSobreSetor = By.id("sobre-setor");
    private By strongStrongOndeEstamos = By.id("strong-onde-estamos");
    private By strongStrongTelefone = By.id("strong-telefone");
    private By tabSobre = By.id("tab-sobre");
    private By separatorEquipe = By.name("EQUIPE");
    private IntFunction<By> imgMembroEquipeFoto = (index) -> By.id("membro-equipe-foto-" + index);
    private IntFunction<By> spanMembroEquipeNome = (index) -> By.id("membro-equipe-nome-" + index);
    private IntFunction<By> pMembroEquipeCargo = (index) -> By.id("membro-equipe-cargo-" + index);
    private IntFunction<By> spanMembroEquipeEmail = (index) -> By.id("membro-equipe-email-" + index);
    private IntFunction<By> spanMembroEquipeTelefone = (index) -> By.id("membro-equipe-telefone-" + index);

    public void clicarTABSobre() {
        clickAndHighlight(tabSobre);
    }

    public void validarDadosSobreOSetor(SobreOSetor sobreOSetor) {
        Assert.assertEquals(getAttribute(divSobreSetor, "innerHTML"), sobreOSetor.getSobreSetor());
        Assert.assertEquals(getText(strongStrongOndeEstamos), sobreOSetor.getLocalizacaoFisica());
        Assert.assertEquals(getText(strongStrongTelefone), sobreOSetor.getTelefoneDoSetor());
    }

    public void validaDadosPreenchidosNoAdministracao(int linha, MembroEquipe membroEquipe) {
        linha--;
        if (membroEquipe.isExibirDadosEquipe()) {
            scrollToElement(separatorEquipe);
            Assert.assertTrue(getAttribute(imgMembroEquipeFoto.apply(linha), "src").endsWith("download"));
            Assert.assertEquals(getText(spanMembroEquipeNome.apply(linha)), membroEquipe.getNome());
            Assert.assertEquals(getText(pMembroEquipeCargo.apply(linha)), membroEquipe.getCargo());
            Assert.assertEquals(getText(spanMembroEquipeEmail.apply(linha)), membroEquipe.getEmail());
            Assert.assertEquals(getText(spanMembroEquipeTelefone.apply(linha)), membroEquipe.getTelefone());
        } else {
            expectElementNotVisible(separatorEquipe);
        }
    }
}
