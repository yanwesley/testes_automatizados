package br.com.pom.sro.informacoes;

import br.com.api.model.informacoes.AtualizacaoDoSistema;
import br.com.api.model.informacoes.BlocoDeTexto;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class DocumentacaoInformacoesPage extends GeralPage {

    private By tabDocumentacao = By.id("tab-documentacao");
    private By divDocumentacaoModulo = By.id("documentacaoModulo");
    private By separatorDocumentacaoOficialDoProjeto = By.name("Documentação Oficial do Projeto");

    private IntFunction<By> sroTopicSeparatorBlocoTextoTitulo = (index) -> By.id("bloco-texto-titulo-" + index);
    private IntFunction<By> divBlocoTextoDescricao = (index) -> By.id("bloco-texto-descricao-" + index);


    public void clicarTABDocumentacao() {
        clickAndHighlight(tabDocumentacao);
    }

    public void validaInformacoesDeDocumentacaoNoSRO(AtualizacaoDoSistema atualizacaoDoSistema) {
        scrollToElement(separatorDocumentacaoOficialDoProjeto);
        Assert.assertEquals(getAttribute(divDocumentacaoModulo, "innerHTML"), atualizacaoDoSistema.getDocumentacaoOficialDoProjeto());
    }

    public void validaDadosBlocoDeTexto(BlocoDeTexto blocoDeTexto, int linha) {
        linha--;
        scrollToElement(sroTopicSeparatorBlocoTextoTitulo.apply(linha));
        Assert.assertEquals(getText(sroTopicSeparatorBlocoTextoTitulo.apply(linha)), blocoDeTexto.getTitulo().toUpperCase());
        Assert.assertEquals(getAttribute(divBlocoTextoDescricao.apply(linha), "innerHTML"), blocoDeTexto.getDescricao());
    }
}
