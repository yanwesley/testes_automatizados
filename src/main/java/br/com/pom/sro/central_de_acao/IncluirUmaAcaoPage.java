package br.com.pom.sro.central_de_acao;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class IncluirUmaAcaoPage extends GeralPage {

    IntFunction<By> optionFocoCausaDescricao = (index) -> By.id("focoCausaDescricao-" + index);
    IntFunction<By> optionImpacto = (index) -> By.id("impacto-" + index);
    IntFunction<By> optionTipoAcao = (index) -> By.id("tipoAcao-" + index);
    IntFunction<By> optionOperacaoResponsavel = (index) -> By.id("operacao-" + index);
    IntFunction<By> optionComplexoResponsavel = (index) -> By.id("complexo-" + index);
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By sroTitleLabel1InsiraAsInformacoesIniciais = By.name("1 - Insira as informações iniciais");
    private By div1InsiraAsInformacoesIniciais = By.name("1 - Insira as informações iniciais");
    private By inputOrigemAcao = By.id("origemAcao");
    private By inputOperacao = By.id("operacao");
    private By inputInputAmvSolicitante = By.id("input-amv-solicitante");
    private By inputNomeComplexo = By.id("nomeComplexo");
    private By selectFocoCausaDescricao = By.id("focoCausaDescricao");
    private By selectImpacto = By.id("impacto");
    private By selectTipoAcao = By.id("tipoAcao");
    private By inputInputIdPlanoAcao = By.id("input-idPlanoAcao");
    private By inputInputData = By.id("input-data");
    private By radioGroupPublica = By.id("publica");
    private By radioButtonPublica0 = By.id("publica-0");
    private By inputPublica0Input = By.id("publica-0-input");
    private By radioButtonPublica1 = By.id("publica-1");
    private By inputPublica1Input = By.id("publica-1-input");
    private By textAreaDescricaoAcao = By.id("descricaoAcao");
    private By btnQuemEstaSolicitando = By.id("dialog-incluir-plano-solicitante-search-button");
    private By btnConfirmarAcaoAvulsa = By.id("dialog-incluir-plano-confirm");
    private By sroTitleLabel2IndiqueUmOuMaisResponsaveisPelaAcao = By.name("2 - Indique um ou mais responsáveis pela ação:");
    private By div2IndiqueUmOuMaisResponsaveisPelaAcao = By.name("2 - Indique um ou mais responsáveis pela ação:");

    private By tableTableResponsaveis = By.id("table-responsaveis");

    private By thTableResponsaveisHeaderOperacao = By.id("table-responsaveis-header-operacao");
    private By thTableResponsaveisHeaderComplexo = By.id("table-responsaveis-header-complexo");
    private By thTableResponsaveisHeaderArea = By.id("table-responsaveis-header-area");
    private By thTableResponsaveisHeaderGerente = By.id("table-responsaveis-header-gerente");
    private By thTableResponsaveisHeaderResponsavel = By.id("table-responsaveis-header-responsavel");
    private By thTableResponsaveisHeaderRemove = By.id("table-responsaveis-header-remove");
    private By tdTableResponsaveisCellOperacao0 = By.id("table-responsaveis-cell-operacao-0");
    private By tdTableResponsaveisCellComplexo0 = By.id("table-responsaveis-cell-complexo-0");
    private By tdTableResponsaveisCellArea0 = By.id("table-responsaveis-cell-area-0");
    private By tdTableResponsaveisCellGerente0 = By.id("table-responsaveis-cell-gerente-0");
    private By tdTableResponsaveisCellResponsavel0 = By.id("table-responsaveis-cell-responsavel-0");
    private By tdTableResponsaveisCellRemove0 = By.id("table-responsaveis-cell-remove-0");
    private IntFunction<By> buttonButtonRemoveResponsavel = (index) -> By.id("button-remove-responsavel-" + index);

    private By buttonSearchResponsavel = By.id("button-search-responsavel");
    private By buttonDialogButtonCancel = By.id("dialog-button-cancel");
    private By buttonDialogButtonSave = By.id("dialog-button-save");

    /*
    modal adicionar responsavel
     */

    private By selectDialogIncluirResponsavelOperacao = By.id("dialog-incluir-responsavel-operacao");
    private By selectDialogIncluirResponsavelComplexo = By.id("dialog-incluir-responsavel-complexo");
    private By inputDialogIncluirResponsavelResponsavel = By.id("dialog-incluir-responsavel-responsavel");
    private By buttonDialogIncluirResponsavelResponsavelSearchButton = By.id("dialog-incluir-responsavel-responsavel-search-button");
    private By inputDialogIncluirResponsavelGerente = By.id("dialog-incluir-responsavel-gerente");
    private By inputDialogIncluirResponsavelArea = By.id("dialog-incluir-responsavel-area");
    private By buttonDialogIncluirResponsavelButtonCancel = By.id("dialog-incluir-responsavel-button-cancel");
    private By buttonDialogIncluirResponsavelConfirm = By.id("dialog-incluir-responsavel-confirm");

    /*
    modal remover responsável
     */

    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");

    public void validarDadosDoPlanoDeAcao(PlanoDeAcao planoDeAcao, String title) {
        Assert.assertEquals(getText(h4DialogHeader), title);
        Assert.assertEquals(getValue(inputOrigemAcao), planoDeAcao.getOrigem());
        Assert.assertEquals(getValue(inputInputAmvSolicitante), planoDeAcao.getSolicitante());
        Assert.assertEquals(getValue(inputInputIdPlanoAcao), String.valueOf(planoDeAcao.getIdPlanoAcao()));
    }

    public void preencherInformacoesIniciais(Acao acao) {
        acao.setFocoCausaDaAcaoValue(
                selectOptionAndReturnValue(
                        selectFocoCausaDescricao,
                        optionFocoCausaDescricao,
                        acao.getFocoCausaDaAcaoIndex()
                )
        );

        acao.setQualOImpactoDaAcaoValue(
                selectOptionAndReturnValue(
                        selectImpacto,
                        optionImpacto,
                        acao.getQualOImpactoDaAcaoIndex()
                )
        );

        acao.setQualOTipoDaAcaoValue(
                selectOptionAndReturnValue(
                        selectTipoAcao,
                        optionTipoAcao,
                        acao.getQualOTipoDaAcaoIndex()
                )
        );

        clearForce(inputInputData);
        sendKeys(inputInputData, acao.getQualEOPrazoParaExecucao());

        if (acao.isAcaoPublica()) {
            clickWithAction(radioButtonPublica0);
        } else {
            clickWithAction(radioButtonPublica1);
        }

        sendKeysWithJavaScript(textAreaDescricaoAcao, acao.getDescritivoDaAcao());
    }

    public void clicarBtnBuscarResponsavel() {
        clickAndHighlight(buttonSearchResponsavel);
    }

    public String preencherDadosResponsavel(ResponsavelAcao responsavelAcao) {

        responsavelAcao.setQualEAOperacaoValue(
                selectOptionAndReturnValue(
                        selectDialogIncluirResponsavelOperacao,
                        optionOperacaoResponsavel,
                        responsavelAcao.getQualEAOperacaoIndex()
                )
        );

        responsavelAcao.setQualEOComplexoValue(
                selectOptionAndReturnValue(
                        selectDialogIncluirResponsavelComplexo,
                        optionComplexoResponsavel,
                        responsavelAcao.getQualEOComplexoIndex()
                )
        );
        clickAndHighlight(buttonDialogIncluirResponsavelResponsavelSearchButton);
        String matricula = selecionarResponsavelERetornarMatricula(responsavelAcao.getResponsavel());

        responsavelAcao.setResponsavel(getValue(inputDialogIncluirResponsavelResponsavel));
        responsavelAcao.setGerente(getValue(inputDialogIncluirResponsavelGerente));
        responsavelAcao.setArea(getValue(inputDialogIncluirResponsavelArea));
        return matricula;
    }

    public void clicarBtnConfirmarModalAdicionarResponsavel() {
        clickAndHighlight(buttonDialogIncluirResponsavelConfirm);
    }

    public void clicarBtnSalvarIncluirAcao() {
        clickAndHighlight(buttonDialogButtonSave);
    }

    public void clicarBtnRemoverResponsavel() {
        clickAndHighlight(buttonButtonRemoveResponsavel.apply(0));
    }

    public void validaModalDeRemoverResponsavel() {
        Assert.assertEquals(getText(divDialogConfirmationMessage), "Deseja remover o responsável?");
    }

    public void clicarBtnConfirmarModalRemoverResponsavel() {
        clickAndHighlight(buttonDialogButtonYes);
    }

    public void clicarBtnSolicitado() {
        clickAndHighlight(btnQuemEstaSolicitando);
    }

    public void clicarBtnCriarPlanoAvulso() {
        clickAndHighlight(btnConfirmarAcaoAvulsa);
    }
}
