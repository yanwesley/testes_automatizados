package br.com.pom.sro.central_de_acao;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class ElaborarPlanoDeAcaoPage extends GeralPage {

    IntFunction<By> tdPRIMARIATableAcoesDescricaoAcao = (index) -> By.id("PRIMARIA-table-acoes-descricao-acao-" + index);
    IntFunction<By> tdPRIMARIATableAcoesPrazo = (index) -> By.id("PRIMARIA-table-acoes-prazo-" + index);
    IntFunction<By> tdPRIMARIATableAcoesExecutor = (index) -> By.id("PRIMARIA-table-acoes-executor-" + index);
    IntFunction<By> tdPRIMARIATableAcoesStatus = (index) -> By.id("PRIMARIA-table-acoes-status-" + index);
    IntFunction<By> tdPRIMARIATableAcoesActions = (index) -> By.id("PRIMARIA-table-acoes-actions-" + index);
    IntFunction<By> buttonPRIMARIAButtonEditarAcao = (index) -> By.id("PRIMARIA-button-editar-acao-" + index);
    IntFunction<By> buttonPRIMARIAButtonRemoverAcao = (index) -> By.id("PRIMARIA-button-remover-acao-" + index);

    /*
    detalhes ação
     */
    IntFunction<By> spanVisualizarPlanoListaAcoesPrazoAcao = (index) -> By.id("visualizar-plano-lista-acoes-prazo-acao-" + index);
    IntFunction<By> spanVisualizarPlanoListaAcoesResponsavelAcao = (index) -> By.id("visualizar-plano-lista-acoes-responsavel-acao-" + index);
    IntFunction<By> spanVisualizarPlanoListaAcoesDescricaoAcao = (index) -> By.id("visualizar-plano-lista-acoes-descricao-acao-" + index);
    IntFunction<By> spanVisualizarPlanoListaAcoesStatusAcao = (index) -> By.id("visualizar-plano-lista-acoes-status-acao-" + index);
    IntFunction<By> iconAcaoDetalhesExpandIcon = (index) -> By.id("acao-detalhes-expand-icon-" + index);


    private By divHeaderPlanoAcao = By.id("header-plano-acao");
    private By buttonBackButton = By.id("back-button");
    private By aButtonFichaAcidente = By.id("button-ficha-acidente");
    private By aButtonFichaInvestigacao = By.id("button-ficha-investigacao");
    private By separatorResumo = By.name("resumo");
    private By sroTitleLabelResumoCausaPlanoAcaoPRIMARIATituloCausa = By.id("resumo-causa-plano-acao-PRIMARIA-titulo-causa");
    private By divCausaPrimariaCausaPrimaria = By.name("Causa PRIMÁRIA: CAUSA PRIMÁRIA");
    private By labelResumoCausaPlanoAcaoPRIMARIABandeira = By.cssSelector("#resumo-causa-plano-acao-PRIMARIA-bandeira > strong");
    private By labelResumoCausaPlanoAcaoPRIMARIAFatoOcorrido = By.cssSelector("#resumo-causa-plano-acao-PRIMARIA-fato-ocorrido > strong");
    private By labelResumoCausaPlanoAcaoPRIMARIACausaIdentificada = By.cssSelector("#resumo-causa-plano-acao-PRIMARIA-causa-identificada > strong");
    private By labelResumoCausaPlanoAcaoCausaOrigem = By.cssSelector("#resumo-causa-plano-acao-causa-origem > strong");
    private By labelResumoCausaPlanoAcaoCausaSolicitante = By.cssSelector("#resumo-causa-plano-acao-causa-solicitante > strong");
    private By labelResumoCausaPlanoAcaoCausaOperacao = By.cssSelector("#resumo-causa-plano-acao-causa-operacao > strong");
    private By labelResumoCausaPlanoAcaoCausaComplexo = By.cssSelector("#resumo-causa-plano-acao-causa-complexo > strong");
    private By sroExpandableTopicSeparatorElaboracaoDeAcoes = By.name("ELABORAÇÃO DE AÇÕES");
    private By divPRIMARIATopicElaboracaoAcoesContentToggle = By.id("PRIMARIA-topic-elaboracao-acoes-content-toggle");
    private By buttonCreateActionButton0 = By.id("create-action-button-0");
    private By thPRIMARIATableDescricaoAcaoHeader = By.id("PRIMARIA-table-descricao-acao-header");
    private By thPRIMARIATablePrazoHeader = By.id("PRIMARIA-table-prazo-header");
    private By thPRIMARIATableExecutorHeader = By.id("PRIMARIA-table-executor-header");
    private By nomePlanoAvulso = By.id("dialog-incluir-plano-nome-plano");
    private By incluirAcao = By.id("footer-visualizar-plano-incluir-acao");
    private By editarPlano = By.id("footer-visualizar-plano-editar-acao");
    private By expansionPanelAcao = By.id("acao-detalhes-expand-icon-0");
    private By descricaoAcao = By.id("visualizar-acao-aceite-descricao-acao");


    /*
    modal de excluir ação
     */
    private By thPRIMARIATableStatusHeader = By.id("PRIMARIA-table-status-header");
    private By thPRIMARIATableActionsHeader = By.id("PRIMARIA-table-actions-header");
    private By tableAcoes = By.cssSelector("table.lista-acoes-table > tbody");
    private By labelNenhumaAcaoFoiCriada = By.tagName("sro-message-box");
    private By buttonFooterBackButton = By.id("footer-back-button");


    /*
    Visualizar ação
     */
    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");

    /*
    Ação detalhada avulsa
     */
    private By divResumoCausaPlanoAcaoCausaOrigem = By.id("resumo-causa-plano-acao-causa-origem");
    private By divResumoCausaPlanoAcaoCausaSolicitante = By.id("resumo-causa-plano-acao-causa-solicitante");
    private By divResumoCausaPlanoAcaoCausaOperacao = By.id("resumo-causa-plano-acao-causa-operacao");
    private By divResumoCausaPlanoAcaoCausaComplexo = By.id("resumo-causa-plano-acao-causa-complexo");
    private By aHeaderVisualizarPlanoAcaoDetalhesImpacto2 = By.id("header-visualizar-plano-acao-detalhes-impacto-2");
    private By aHeaderVisualizarPlanoAcaoDetalhes = By.id("header-visualizar-plano-acao-detalhes");
    private By divVisualizarPlanoAcaoDetalhesFocoCausaDescricao = By.id("visualizar-plano-acao-detalhes-focoCausaDescricao");
    private By divVisualizarPlanoAcaoDetalhesTipoAcao = By.id("visualizar-plano-acao-detalhes-tipoAcao");
    private By sroTitleLabelQuemIraRealizar = By.name("QUEM IRÁ REALIZAR");
    private By divQuemIraRealizar = By.name("QUEM IRÁ REALIZAR");
    private By divVisualizarAcaoAceiteArea = By.id("visualizar-acao-aceite-area");
    private By divVisualizarAcaoAceiteGerente = By.id("visualizar-acao-aceite-gerente");
    private By divVisualizarAcaoAceiteResponsavel = By.id("visualizar-acao-aceite-responsavel");
    private By sroTitleLabelAcaoASerRealizada = By.name("AÇÃO A SER REALIZADA");
    private By divAcaoASerRealizada = By.name("AÇÃO A SER REALIZADA");
    private By tdVisualizarAcaoAceiteDescricaoAcao = By.id("visualizar-acao-aceite-descricao-acao");
    private By tdVisualizarAcaoAceitePrazo = By.id("visualizar-acao-aceite-prazo");
    private By excluiAcaoAvulsa = By.id("header-visualizar-plano-acao-excluir");


    public void validarInformacoesDoPlano(PlanoDeAcao planoDeAcao) {
        expectElementVisibleWithoutHighlight(separatorResumo);
        expectElementVisibleWithoutHighlight(buttonBackButton);
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoPRIMARIABandeira).trim(), planoDeAcao.getBandeira());
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoPRIMARIAFatoOcorrido).trim(), planoDeAcao.getFatoOcorrido());
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoPRIMARIACausaIdentificada).trim(), planoDeAcao.getCausaProvavelIdentificada());
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoCausaOrigem).trim(), planoDeAcao.getOrigem());
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoCausaOperacao).trim().toUpperCase(), planoDeAcao.getOperacao());
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoCausaSolicitante).trim(), planoDeAcao.getSolicitante());
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoCausaComplexo).trim(), planoDeAcao.getComplexo());
    }

    public void validaExibicaoDaPagina() {
        expectElementVisible(separatorResumo);
        expectElementVisible(buttonCreateActionButton0);
    }

    public void clicarBtnAdicionarAcao() {
        clickAndHighlight(buttonCreateActionButton0);
    }

    public void validarExibicaoDaAcaoInserida(Acao acao, ResponsavelAcao responsavelAcao, int linha) {
        linha--;
        Assert.assertEquals(getText(tdPRIMARIATableAcoesDescricaoAcao.apply(linha)), acao.getDescritivoDaAcao());
        Assert.assertEquals(getText(tdPRIMARIATableAcoesPrazo.apply(linha)), acao.getQualEOPrazoParaExecucao());
        Assert.assertEquals(getText(tdPRIMARIATableAcoesExecutor.apply(linha)), responsavelAcao.getResponsavel());
        Assert.assertEquals(getText(tdPRIMARIATableAcoesStatus.apply(linha)), "Aguardando aceite");
        expectElementVisible(buttonPRIMARIAButtonEditarAcao.apply(linha));
        expectElementVisible(buttonPRIMARIAButtonRemoverAcao.apply(linha));
    }

    public void validarQueTenhaAcoes(int quantidade) {
        Assert.assertTrue(countChildElement(tableAcoes, "tr") >= quantidade);
    }

    public void clicarBtnEditarAcao(int linha) {
        linha--;
        clickAndHighlight(buttonPRIMARIAButtonEditarAcao.apply(linha));
    }

    public void clicarBtnExcluirAcao(int linha) {
        linha--;
        clickAndHighlight(buttonPRIMARIAButtonRemoverAcao.apply(linha));
    }

    public void validaModalDeExclusaoDaAcao() {
        Assert.assertEquals(getText(divDialogConfirmationMessage), "Deseja excluir a ação?");
    }

    public void clicarBtnExcluirModalExclusao() {
        clickAndHighlight(buttonDialogButtonYes);
    }

    public void validaQueAAcaoNaoEExibida() {
        scrollToElement(labelNenhumaAcaoFoiCriada);
        Assert.assertEquals(getAttribute(labelNenhumaAcaoFoiCriada, "text"), "Atenção: nenhuma ação foi criada até o momento.");
    }

    public void validarDadosDaAcaoVisualizacao(Acao acao, ResponsavelAcao responsavelAcao, String status, int linha) {
        linha--;
        Assert.assertEquals(getText(spanVisualizarPlanoListaAcoesPrazoAcao.apply(linha)), acao.getQualEOPrazoParaExecucao());
        Assert.assertEquals(getText(spanVisualizarPlanoListaAcoesResponsavelAcao.apply(linha)), responsavelAcao.getResponsavel());
        String descritivo = acao.getDescritivoDaAcao().length() > 80 ? acao.getDescritivoDaAcao().substring(0, 80).concat("…") : acao.getDescritivoDaAcao();
        Assert.assertEquals(getText(spanVisualizarPlanoListaAcoesDescricaoAcao.apply(linha)), descritivo);
        Assert.assertEquals(getText(spanVisualizarPlanoListaAcoesStatusAcao.apply(linha)), status);
    }

    public void preenchoNomePlano(PlanoDeAcao planoDeAcao) {
        sendKeysWithJavaScript(nomePlanoAvulso, planoDeAcao.getNomePlanoAcaoAvulso());
    }

    public void validoPlanoAvulso(PlanoDeAcao planoDeAcao, ResponsavelAcao responsavelAcao) {
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoCausaOrigem).trim(), "Plano");
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoCausaOperacao).trim(), responsavelAcao.getQualEAOperacaoValue());
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoCausaSolicitante).trim(), responsavelAcao.getResponsavel());
        Assert.assertEquals(getText(labelResumoCausaPlanoAcaoCausaComplexo).trim(), responsavelAcao.getQualEOComplexoValue());
    }

    public void clicarEditarPlano() {
        clickAndHighlight(editarPlano);
    }

    public void editeiNomePlano(PlanoDeAcao planoDeAcao) {
        sendKeysWithJavaScript(nomePlanoAvulso, planoDeAcao.getNomePlanoAcaoAvulso());
    }

    public void clicarBtnIncluirAcao() {
        clickAndHighlight(incluirAcao);
    }

    public void validarAcaoPlanoAvulso(Acao acao, ResponsavelAcao responsavelAcao) {
        expectElementVisibleWithoutHighlight(expansionPanelAcao);
        clickAndHighlight(expansionPanelAcao);
        Assert.assertEquals(getText(divVisualizarPlanoAcaoDetalhesFocoCausaDescricao).trim(), acao.getFocoCausaDaAcaoValue());
        Assert.assertEquals(getText(divVisualizarPlanoAcaoDetalhesTipoAcao).trim(), acao.getQualOTipoDaAcaoValue());
        Assert.assertEquals(getText(divVisualizarAcaoAceiteResponsavel).trim(), responsavelAcao.getResponsavel());
        Assert.assertEquals(getText(descricaoAcao).trim(), acao.getDescritivoDaAcao());
        Assert.assertEquals(getText(tdVisualizarAcaoAceitePrazo).trim(), acao.getQualEOPrazoParaExecucao());
    }

    public void clicarAcaoAvulsa() {
        clickAndHighlight(expansionPanelAcao);
    }

    public void excluirAcaoAvulsa() {
        clickAndHighlight(excluiAcaoAvulsa);
    }

    public void clicarBtnConfirmarExclusao() {
        clickAndHighlight(buttonDialogButtonYes);
    }
}
