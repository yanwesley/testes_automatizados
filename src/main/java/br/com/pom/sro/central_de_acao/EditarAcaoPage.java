package br.com.pom.sro.central_de_acao;

import br.com.api.model.central_de_acoes.Acao;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class EditarAcaoPage extends GeralPage {

    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By radioGroupOpcaoEditarAcao = By.id("opcao-editar-acao");
    private By radioButtonOpcaoEditarAcaoEditar = By.id("opcao-editar-acao-editar");
    private By inputOpcaoEditarAcaoEditarInput = By.id("opcao-editar-acao-editar-input");
    private By radioButtonOpcaoEditarAcaoAnular = By.id("opcao-editar-acao-anular");
    private By inputOpcaoEditarAcaoAnularInput = By.id("opcao-editar-acao-anular-input");
    private By radioButtonOpcaoEditarAcaoAlterarResponsavel = By.id("opcao-editar-acao-alterar-responsavel");
    private By inputOpcaoEditarAcaoAlterarResponsavelInput = By.id("opcao-editar-acao-alterar-responsavel-input");
    private By buttonDialogEditarAcaoButtonCancel = By.id("dialog-editar-acao-button-cancel");
    private By buttonDialogEditarAcaoConfirm = By.id("dialog-editar-acao-confirm");

    /*
    Justificativa
     */
    private By sroTitleLabelDialogEditarAcaoTitleJustificativa = By.id("dialog-editar-acao-title-justificativa");
    private By div1InsiraUmaJustificativa = By.name("1 - Insira uma justificativa:");
    private By textareaDialogEditarAcaoTextJustificativa = By.id("dialog-editar-acao-text-justificativa");
    private By sroFileUploadDialogEditarAcaoUploadComponent = By.id("dialog-editar-acao-upload-component");
    private By inputInputFileUpload = By.id("input-file-upload");
    private By buttonBtnInputFileUpload = By.id("btn-input-file-upload");
    private By buttonBtnFileUploadSearch = By.id("btn-file-upload-search");
    private By ulFileUploadComponentListFiles = By.id("file-upload-component-list-files");

    /*
   Informações iniciais
     */

    private By sroTitleLabel2CasoSejaNecessarioEditeAsInformacoesIniciais = By.name("2 - Caso seja necessário edite as informações iniciais:");
    private By div2CasoSejaNecessarioEditeAsInformacoesIniciais = By.name("2 - Caso seja necessário edite as informações iniciais:");
    private By radioGroupPublica = By.id("publica");
    private By radioButtonPublicaSim = By.id("publica-sim");
    private By inputPublicaSimInput = By.id("publica-sim-input");
    private By radioButtonPublicaNao = By.id("publica-nao");
    private By inputPublicaNaoInput = By.id("publica-nao-input");
    private By inputDialogEditarAcaoTextPrazo = By.id("dialog-editar-acao-text-prazo");
    private By textareaDialogEditarAcaoTextAcao = By.id("dialog-editar-acao-text-acao");


    /*
    Indique o novo responsável
     */

    private By sroTitleLabel3IndiqueONovoResponsavelPelaAcao = By.name("3 - Indique o novo responsável pela ação:");
    private By div3IndiqueONovoResponsavelPelaAcao = By.name("3 - Indique o novo responsável pela ação:");
    private By tdDialogEditarAcaoResponsavelOperacao = By.id("dialog-editar-acao-responsavel-operacao");
    private By tdDialogEditarAcaoResponsavelComplexo = By.id("dialog-editar-acao-responsavel-complexo");
    private By tdDialogEditarAcaoResponsavelNome = By.id("dialog-editar-acao-responsavel-nome");
    private By tdDialogEditarAcaoResponsavelGerente = By.id("dialog-editar-acao-responsavel-gerente");
    private By tdDialogEditarAcaoResponsavelArea = By.id("dialog-editar-acao-responsavel-area");
    private By buttonDialogEditarAcaoResponsavelIncluirButton = By.id("dialog-editar-acao-responsavel-incluir-button");

    public void validaExibicaoDoModal() {
        expectElementVisibleWithoutHighlight(radioButtonOpcaoEditarAcaoEditar);
        expectElementVisibleWithoutHighlight(radioButtonOpcaoEditarAcaoAnular);
        expectElementVisibleWithoutHighlight(radioButtonOpcaoEditarAcaoAlterarResponsavel);
    }

    public void selecionarOpcaoEditarAcao() {
        clickWithAction(radioButtonOpcaoEditarAcaoEditar);
    }

    public void preencherJustificativa(String justificativa) {
        sendKeysWithJavaScript(textareaDialogEditarAcaoTextJustificativa, justificativa);
        uploadArquivo(1);
    }

    public void editarInformacoesIniciais(Acao acaoEditada) {
        if (acaoEditada.isAcaoPublica()) {
            clickWithAction(radioButtonPublicaSim);
        } else {
            clickWithAction(radioButtonPublicaNao);
        }

        clearForce(inputDialogEditarAcaoTextPrazo);
        sendKeys(inputDialogEditarAcaoTextPrazo, acaoEditada.getQualEOPrazoParaExecucao());
        sendKeysWithJavaScript(textareaDialogEditarAcaoTextAcao, acaoEditada.getDescritivoDaAcao());
    }

    public void clicarBtnConfirmarModal() {
        clickAndHighlight(buttonDialogEditarAcaoConfirm);
    }

    public void selecionarOpcaoAnularAcao() {
        clickAndHighlight(radioButtonOpcaoEditarAcaoAnular);
    }

    public void selecionarOpcaoAlterarResponsavel() {
        clickAndHighlight(radioButtonOpcaoEditarAcaoAlterarResponsavel);
    }

    public void clicarBtnBuscarResponsavel() {
        clickAndHighlight(buttonDialogEditarAcaoResponsavelIncluirButton);
    }

    public void preencherAcaoPublica(Acao acao) {
        if (!acao.isAcaoPublica()) {
            clickWithAction(radioButtonPublicaSim);
        } else {
            clickWithAction(radioButtonPublicaNao);
        }
    }
}
