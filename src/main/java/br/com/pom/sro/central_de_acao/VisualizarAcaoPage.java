package br.com.pom.sro.central_de_acao;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.pom.sro.GeralPage;
import com.google.common.base.CaseFormat;
import org.openqa.selenium.By;
import org.testng.Assert;

import static br.com.api.GeradorDeMassa.getDay;

public class VisualizarAcaoPage extends GeralPage {

    private By divHeaderVisualizarAcao = By.id("header-visualizar-acao");
    private By buttonBackButton = By.id("back-button");
    private By aHeaderVisualizarAcaoButtonFichaInvestigacao = By.id("header-visualizar-acao-button-ficha-investigacao");
    private By divHeaderVisualizarAcaoPrazo = By.id("header-visualizar-acao-prazo");
    private By divHeaderVisualizarAcaoPublica = By.id("header-visualizar-acao-publica");
    private By divHeaderVisualizarAcaoImpacto = By.id("header-visualizar-acao-impacto");
    private By aHeaderVisualizarAcaoButtonPlanoAcao = By.id("header-visualizar-acao-button-plano-acao");
    private By separatorResumo = By.name("resumo");
    private By divResumoAcaoBandeira = By.cssSelector("#resumo-acao-bandeira > strong");
    private By divResumoAcaoFatoOcorrido = By.cssSelector("#resumo-acao-fato-ocorrido > strong");
    private By divResumoAcaoCausaIdentificada = By.cssSelector("#resumo-acao-causa-identificada > strong");
    private By divResumoAcaoCausaOrigem = By.cssSelector("#resumo-acao-causa-origem > strong");
    private By divResumoAcaoCausaFoco = By.cssSelector("#resumo-acao-causa-foco > strong");
    private By divResumoAcaoCausaOperacao = By.cssSelector("#resumo-acao-causa-operacao > strong");
    private By divResumoAcaoCausaComplexo = By.cssSelector("#resumo-acao-causa-complexo > strong");
    private By divResumoAcaoCausaSolicitante = By.cssSelector("#resumo-acao-causa-solicitante > strong");
    private By divResumoAcaoCausaTipoAcao = By.cssSelector("#resumo-acao-causa-tipo-acao > strong");
    private By divMatTabLabel00 = By.id("mat-tab-label-0-0");
    private By divMatTabLabel01 = By.id("mat-tab-label-0-1");
    private By tabBodyMatTabContent00 = By.id("mat-tab-content-0-0");
    private By separatorQuemIraRealizar = By.name("Quem irá realizar");
    private By divVisualizarAcaoAceiteArea = By.id("visualizar-acao-aceite-area");
    private By divVisualizarAcaoAceiteGerente = By.id("visualizar-acao-aceite-gerente");
    private By divVisualizarAcaoAceiteResponsavel = By.id("visualizar-acao-aceite-responsavel");
    private By separatorAcaoASerRealizada = By.name("Ação a ser realizada");
    private By tdVisualizarAcaoAceiteDescricaoAcao = By.id("visualizar-acao-aceite-descricao-acao");
    private By tdVisualizarAcaoAceitePrazo = By.id("visualizar-acao-aceite-prazo");
    private By tdVisualizarAcaoAceitePrevisaoConclusao = By.id("visualizar-acao-aceite-previsao-conclusao");
    private By tabBodyMatTabContent01 = By.id("mat-tab-content-0-1");
    private By buttonVisualizarAcaoFooterNextButton = By.id("visualizar-acao-footer-next-button");
    private By buttonVisualizarAcaoFooterAceitarAcaoButton = By.id("visualizar-acao-footer-aceitar-acao-button");
    private By buttonVisualizarAcaoFooterRecusarAcaoButton = By.id("visualizar-acao-footer-recusar-acao-button");
    private By buttonVisualizarAcaoFooterEditarAcaoButton = By.id("visualizar-acao-footer-editar-acao-button");
    private By buttonVisualizarAcaoFooterSalvarExecucaoButton = By.id("visualizar-acao-footer-salvar-execucao-button");
    private By buttonVisualizarAcaoFooterSalvarFinalizarExecucaoButton = By.id("visualizar-acao-footer-salvar-finalizar-execucao-button");
    private By buttonVisualizarAcaoFooterJustificarAtrasoExecucaoButton = By.id("visualizar-acao-footer-justificar-atraso-execucao-button");
    private By buttonVisualizarAcaoFooterAnularExecucaoButton = By.id("visualizar-acao-footer-anular-execucao-button");
    private By buttonVisualizarAcaoFooterAprovarEvidenciaButton = By.id("visualizar-acao-footer-aprovar-evidencia-button");
    private By buttonVisualizarAcaoFooterReprovarEvidenciaButton = By.id("visualizar-acao-footer-reprovar-evidencia-button");
    private By buttonVisualizarAcaoFooterReabrirAcaoExecucaoButton = By.id("visualizar-acao-footer-reabrir-acao-execucao-button");
    private By buttonVisualizarAcaoFooterAceitarAlteracaoButton = By.id("visualizar-acao-footer-aceitar-alteracao-button");
    private By buttonVisualizarAcaoFooterDesdobrarButton = By.id("visualizar-acao-footer-desdobrar-button");
    private By expansionPanelPlanoAcaoDetalhado = By.id("visualizar-plano-acao-card-button-0");
    private By tabAceite = By.xpath("//*[contains(text(),'ACEITE')]");
    private By tabHistorico = By.xpath("//*[contains(text(),'HISTÓRICO')]");
    private By buttonVisualizarAcaoFooterRecusarAlteracaoButton = By.id("visualizar-acao-footer-recusar-alteracao-button");
    private By textAreaJustificativa = By.id("dialog-recusar-alteracao-text-justificativa");
    private By btnConfirmarRecusaAcao = By.id("dialog-recusar-alteracao-confirm");
    private By labelJustificativaRecusada = By.id("visualizar-acao-solicitacao-recusada-justificativa-recusada");

    /*
    Execução
     */
    private By separatorRegistreOsResultados = By.name("Registre os resultados");
    private By formFieldExecutarAcaoDataFinalizacaoInput = By.id("executar-acao-data-finalizacao-input");
    private By inputExecutarAcaoDataFinalizacaoInputData = By.id("executar-acao-data-finalizacao-input-data");
    private By formFieldExecutarAcaoResultadosInput = By.id("executar-acao-resultados-input");
    private By textareaResultadoAlcancado = By.id("resultadoAlcancado");
    private By formFieldExecutarAcaoComentariosInput = By.id("executar-acao-comentarios-input");
    private By textareaComentario = By.id("comentario");
    private By formFieldExecutarAcaoSugestoesInput = By.id("executar-acao-sugestoes-input");
    private By textareaSugestoesAcoesFuturas = By.id("sugestoesAcoesFuturas");
    private By sroFileUploadExecutarAcaoEvidenciaUpload = By.id("executar-acao-evidencia-upload");
    private By inputInputFileUpload = By.id("input-file-upload");
    private By buttonBtnInputFileUpload = By.id("btn-input-file-upload");
    private By buttonBtnFileUploadSearch = By.id("btn-file-upload-search");
    private By ulFileUploadComponentListFiles = By.id("file-upload-component-list-files");
    private By sroDownloadFilesListExecutarAcaoEvidenciasFiles = By.id("executar-acao-evidencias-files");
    private By tableDownloadFiles = By.id("download-files");
    private By thDownloadFilesNameHeader = By.id("download-files-name-header");
    private By tdDownloadFilesName0 = By.id("download-files-name-0");
    private By tdDownloadFilesCellActions0 = By.id("download-files-cell-actions-0");
    private By aDownloadFilesBtnDownload0 = By.id("download-files-btn-download-0");
    private By buttonDownloadFilesBtnExcluir0 = By.id("download-files-btn-excluir-0");
    private By tdDownloadFilesName1 = By.id("download-files-name-1");
    private By tdDownloadFilesCellActions1 = By.id("download-files-cell-actions-1");
    private By aDownloadFilesBtnDownload1 = By.id("download-files-btn-download-1");
    private By buttonDownloadFilesBtnExcluir1 = By.id("download-files-btn-excluir-1");
    private By btnLupaDetalhes = By.id("button-edit-sindicancia-0");

    /*
    Execução readonly
     */

    private By divExecutarAcaoDataFinalizacaoExecutada = By.id("executar-acao-data-finalizacao-executada");
    private By divExecutarAcaoDataNomeExecucao = By.id("executar-acao-data-nome-execucao");
    private By divExecutarAcaoDataMatriculaExecucao = By.id("executar-acao-data-matricula-execucao");
    private By divExecutarAcaoDataResultadosExecucao = By.id("executar-acao-data-resultados-execucao");
    private By divExecutarAcaoDataComentariosExecucao = By.id("executar-acao-data-comentarios-execucao");
    private By divExecutarAcaoDataSugestoesExecucao = By.id("executar-acao-data-sugestoes-execucao");
    private By sroDownloadFilesListExecutarAcaoDataFilesExecucao = By.id("executar-acao-data-files-execucao");
    private By divExecutarAcaoDataFinalizacaoRealExecucao = By.id("executar-acao-data-finalizacao-real-execucao");

    /*
    Modal Aceitar ação
     */

    private By h4DialogHeader = By.id("dialog-header");
    private By h4DialogContent = By.cssSelector("sro-dialog-aceitar-acao > div.mat-dialog-content > div > div");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By buttonDialogAceitarAcaoButtonCancel = By.id("dialog-aceitar-acao-button-cancel");
    private By buttonDialogAceitarAcaoConfirm = By.id("dialog-aceitar-acao-confirm");

    /*
    Modal Ação Atrasada
     */

    private By textareaJustificativaAtraso = By.id("justificativa-atraso");
    private By inputInputData = By.id("input-data");
    private By buttonDialogEditarAcaoButtonCancel = By.id("dialog-editar-acao-button-cancel");
    private By buttonDialogEditarAcaoConfirm = By.id("dialog-editar-acao-confirm");
    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");

    /*
    Modal reprovar evidência
     */

    private By textareaDialogReprovarEvidenciaTextJustificativa = By.id("dialog-reprovar-evidencia-text-justificativa");
    private By sroFileUploadDialogReprovarEvidenciaUploadComponent = By.id("dialog-reprovar-evidencia-upload-component");
    private By buttonDialogAprovarEvidenciaButtonCancel = By.id("dialog-aprovar-evidencia-button-cancel");
    private By buttonDialogAprovarEvidenciaConfirm = By.id("dialog-aprovar-evidencia-confirm");

    /*
    Modal aprovar evidência
     */

    private By radioGroupOpcaoAprovarEvidencia = By.id("opcao-aprovar-evidencia");
    private By radioButtonOpcaoAprovarEvidenciaNao = By.id("opcao-aprovar-evidencia-nao");
    private By inputOpcaoAprovarEvidenciaNaoInput = By.id("opcao-aprovar-evidencia-nao-input");
    private By radioButtonOpcaoAprovarEvidenciaSim = By.id("opcao-aprovar-evidencia-sim");
    private By inputOpcaoAprovarEvidenciaSimInput = By.id("opcao-aprovar-evidencia-sim-input");
    private By textareaDialogAprovarEvidenciaTextComentario = By.id("dialog-aprovar-evidencia-text-comentario");
    private By inputDialogAprovarEvidenciaDataAprovacao = By.id("dialog-aprovar-evidencia-data-aprovacao");
    private By sroFileUploadDialogAprovarEvidenciaUploadComponent = By.id("dialog-aprovar-evidencia-upload-component");

    /*
    Modal reabrir ação
     */

    private By sroTitleLabel1InsiraUmaJustificativa = By.name("1 - Insira uma justificativa:");
    private By div1InsiraUmaJustificativa = By.name("1 - Insira uma justificativa:");
    private By textareaJustificativaReaberturaAcao = By.id("justificativa-reabertura-acao");
    private By sroFileUploadUploadAnexosRecusarInvestigacao = By.id("upload-anexos-recusar-investigacao");
    private By sroTitleLabel2EditeAsInformacoesIniciais = By.name("2 - Edite as informações iniciais:");
    private By div2EditeAsInformacoesIniciais = By.name("2 - Edite as informações iniciais:");
    private By radioGroupPublica = By.id("publica");
    private By radioButtonPublicaSim = By.id("publica-sim");
    private By inputPublicaSimInput = By.id("publica-sim-input");
    private By radioButtonPublicaNao = By.id("publica-nao");
    private By inputPublicaNaoInput = By.id("publica-nao-input");
    private By inputDialogEditarAcaoPrazo = By.id("dialog-editar-acao-prazo");
    private By textAreaDialogEditarAcaoDescricaoAcao = By.id("dialog-editar-acao-descricao-acao");

    /*
    Plano Ação detalhado
     */
    private By labelBandeira = By.cssSelector("#visualizar-plano-acao-detalhes-focoCausaDescricao > strong");
    private By labelTipoAcao = By.id("visualizar-plano-acao-detalhes-tipoAcao");
    private By labelArea = By.id("visualizar-acao-aceite-area");
    private By labelGerente = By.id("visualizar-acao-aceite-gerente");
    private By labelResponsavel = By.id("visualizar-acao-aceite-responsavel");
    private By labelDataFinalizacao = By.id("executar-acao-data-finalizacao-executada");

    /*
    Modal desdobrar ação
     */

    private By sroTitleLabel1CasoSejaNecessarioEditeAsInformacoesIniciais = By.name("1 - Caso seja necessário edite as informações iniciais:");
    private By div1CasoSejaNecessarioEditeAsInformacoesIniciais = By.name("1 - Caso seja necessário edite as informações iniciais:");
    private By radioButtonPublica0 = By.id("publica-0");
    private By inputPublica0Input = By.id("publica-0-input");
    private By radioButtonPublica1 = By.id("publica-1");
    private By inputPublica1Input = By.id("publica-1-input");
    private By inputPrazo = By.id("prazo");
    private By textareaDescricaoAcao = By.id("descricaoAcao");
    private By sroTitleLabel2IndiqueUmOuMaisResponsaveisPelaAcao = By.name("2 - Indique um ou mais responsáveis pela ação:");
    private By div2IndiqueUmOuMaisResponsaveisPelaAcao = By.name("2 - Indique um ou mais responsáveis pela ação:");
    private By buttonButtonAddResponsavel = By.id("button-add-responsavel");
    private By buttonDialogButtonCancel = By.id("dialog-button-cancel");
    private By buttonDialogButtonConfirm = By.id("dialog-button-confirm");

    /*
    Recusar Ação desdobrada
     */
    private By radioGroupOpcaoRecusarAcao = By.id("opcao-recusar-acao");
    private By radioButtonOpcaoRecusarAcaoRotina = By.id("opcao-recusar-acao-rotina");
    private By inputOpcaoRecusarAcaoRotinaInput = By.id("opcao-recusar-acao-rotina-input");
    private By radioButtonOpcaoRecusarAcaoNaoAplica = By.id("opcao-recusar-acao-nao-aplica");
    private By inputOpcaoRecusarAcaoNaoAplicaInput = By.id("opcao-recusar-acao-nao-aplica-input");
    private By textAreaDialogRecusarAcaoTextJustificativa = By.id("dialog-recusar-acao-text-justificativa");
    private By sroFileUploadDialogRecusarDesdobradaUploadComponent = By.id("dialog-recusar-desdobrada-upload-component");
    private By buttonDialogRecusarAcaoButtonCancel = By.id("dialog-recusar-acao-button-cancel");
    private By buttonDialogRecusarAcaoConfirm = By.id("dialog-recusar-acao-confirm");


    /*
    modal recusa ação sindicância
     */
    private By radioButtonOpcaoRecusarAcaoDescricao = By.id("opcao-recusar-acao-descricao");
    private By radioButtonOpcaoRecusarAcaoPrazo = By.id("opcao-recusar-acao-prazo");
    private By radioButtonOpcaoRecusarAcaoResponsavel = By.id("opcao-recusar-acao-responsavel");
    private By textareaDialogRecusarAcaoTextDescription = By.id("dialog-recusar-acao-text-description");
    private By inputDialogRecusarAcaoTextPrazo = By.id("dialog-recusar-acao-text-prazo");
    private By textareaDialogRecusarAcaoTextJustificativa = By.id("dialog-recusar-acao-text-justificativa");
    /*
    Histórico recusa
     */

    private By divVisualizarAcaoSolicitacaoDataRecusa = By.id("visualizar-acao-solicitacao-data-recusa");
    private By divVisualizarAcaoSolicitacaoNomeRecusa = By.id("visualizar-acao-solicitacao-nome-recusa");
    private By divVisualizarAcaoSolicitacaoMatriculaRecusa = By.id("visualizar-acao-solicitacao-matricula-recusa");
    private By divVisualizarAcaoSolicitacaoTipoRecusa = By.id("visualizar-acao-solicitacao-tipo-recusa");
    private By divVisualizarAcaoSolicitacaoJustificativaRecusa = By.id("visualizar-acao-solicitacao-justificativa-recusa");
    private By divVisualizarAcaoSolicitacaoDescricaoAtual = By.id("visualizar-acao-solicitacao-descricao-atual");
    private By divVisualizarAcaoSolicitacaoNovaAcao = By.id("visualizar-acao-solicitacao-nova-acao");
    private By divVisualizarAcaoSolicitacaoPrazoAtual = By.id("visualizar-acao-solicitacao-prazo-atual");
    private By divVisualizarAcaoSolicitacaoNovoPrazo = By.id("visualizar-acao-solicitacao-novo-prazo");

    /*
    Modal editar ação resusada
     */
    private By radioGroupOpcaoEditarAcao = By.id("opcao-editar-acao");
    private By radioButtonOpcaoEditarAcaoEditar = By.id("opcao-editar-acao-editar");
    private By inputOpcaoEditarAcaoEditarInput = By.id("opcao-editar-acao-editar-input");
    private By sroTitleLabelDialogEditarAcaoTitleJustificativa = By.id("dialog-editar-acao-title-justificativa");
    private By textareaDialogEditarAcaoTextJustificativa = By.id("dialog-editar-acao-text-justificativa");
    private By sroFileUploadDialogEditarAcaoUploadComponent = By.id("dialog-editar-acao-upload-component");
    private By sroTitleLabel2CasoSejaNecessarioEditeAsInformacoesIniciais = By.name("2 - Caso seja necessário edite as informações iniciais:");
    private By div2CasoSejaNecessarioEditeAsInformacoesIniciais = By.name("2 - Caso seja necessário edite as informações iniciais:");
    private By inputDialogEditarAcaoTextPrazo = By.id("dialog-editar-acao-text-prazo");
    private By textareaDialogEditarAcaoTextAcao = By.id("dialog-editar-acao-text-acao");

    public void validaExibicaoDaPagina() {
        expectElementVisible(separatorResumo);
        expectElementVisible(separatorQuemIraRealizar);
        expectElementVisible(separatorAcaoASerRealizada);
    }

    public void clicaBtnAceitarAcao() {
        scrollToElement(separatorAcaoASerRealizada);
        clickAndHighlight(buttonVisualizarAcaoFooterAceitarAcaoButton);
    }

    public void validaModalDeAceitarAcao() {
        expectElementVisible(h4DialogHeader);
        Assert.assertEquals(getText(h4DialogContent), "Gostaria de confirmar o aceite desta ação?");
    }

    public void clicarBtnConfirmarModalAceitarAcao() {
        clickAndHighlight(buttonDialogAceitarAcaoConfirm);
    }

    public void validaExibicaoDosDetalhesDaAcao(PlanoDeAcao planoDeAcao, Acao acao, ResponsavelAcao responsavelAcao) {
        Assert.assertEquals(getText(divResumoAcaoBandeira).trim(), planoDeAcao.getBandeira());
        Assert.assertEquals(getText(divResumoAcaoFatoOcorrido).trim(), planoDeAcao.getFatoOcorrido());
        Assert.assertEquals(getText(divResumoAcaoCausaIdentificada).trim(), planoDeAcao.getCausaProvavelIdentificada());
        Assert.assertEquals(getText(divResumoAcaoCausaOrigem).trim(), planoDeAcao.getOrigem());
        Assert.assertEquals(getText(divResumoAcaoCausaFoco).trim(), acao.getFocoCausaDaAcaoValue());
        Assert.assertEquals(getText(divResumoAcaoCausaOperacao).trim(), CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, responsavelAcao.getQualEAOperacaoValue()));
        Assert.assertEquals(getText(divResumoAcaoCausaComplexo).trim(), responsavelAcao.getQualEOComplexoValue());
        Assert.assertEquals(getText(divResumoAcaoCausaSolicitante).trim(), planoDeAcao.getSolicitante());
        Assert.assertEquals(getText(divResumoAcaoCausaTipoAcao).trim(), acao.getQualOTipoDaAcaoValue());

        Assert.assertEquals(getText(divVisualizarAcaoAceiteArea).trim(), responsavelAcao.getArea());
        Assert.assertEquals(getText(divVisualizarAcaoAceiteGerente).trim(), responsavelAcao.getGerente());
        Assert.assertEquals(getText(divVisualizarAcaoAceiteResponsavel).trim(), responsavelAcao.getResponsavel());


        Assert.assertEquals(getText(tdVisualizarAcaoAceiteDescricaoAcao), acao.getDescritivoDaAcao());
        Assert.assertEquals(getText(tdVisualizarAcaoAceitePrazo), acao.getQualEOPrazoParaExecucao());
    }

    public void clicarBtnEditarAcao() {
        clickAndHighlight(buttonVisualizarAcaoFooterEditarAcaoButton);
    }

    public void validaExibicaoDosDetalhesDaAcaoAposEdicao(Acao acaoEditada) {
        if (acaoEditada.isAcaoPublica()) {
            expectElementVisible(divHeaderVisualizarAcaoPublica);
        } else {
            expectElementNotVisible(divHeaderVisualizarAcaoPublica);
        }
        Assert.assertEquals(getText(tdVisualizarAcaoAceiteDescricaoAcao), acaoEditada.getDescritivoDaAcao());
        Assert.assertEquals(getText(tdVisualizarAcaoAceitePrazo), acaoEditada.getQualEOPrazoParaExecucao());
    }

    public void validarDetalhesDaAcaoAnulada() {
        expectElementNotVisible(buttonVisualizarAcaoFooterEditarAcaoButton);
    }

    public void validarDadosDoResponsavel(ResponsavelAcao responsavelAcao) {
        Assert.assertEquals(getText(divVisualizarAcaoAceiteArea).trim(), responsavelAcao.getArea());
        Assert.assertEquals(getText(divVisualizarAcaoAceiteGerente).trim(), responsavelAcao.getGerente());
        Assert.assertEquals(getText(divVisualizarAcaoAceiteResponsavel).trim(), responsavelAcao.getResponsavel());
    }

    public void clicarBtnJustificarAtraso() {
        clickAndHighlight(buttonVisualizarAcaoFooterJustificarAtrasoExecucaoButton);
    }

    public void validaExibicaoDoModalDeJustificarAtraso() {
        expectElementVisible(textareaJustificativaAtraso);
        expectElementVisible(inputInputData);
    }

    public void preencherDadosDoModalJustificarAtraso(String justificativa, Acao acaoEditada) {
        sendKeysWithJavaScript(textareaJustificativaAtraso, justificativa);
        clearForce(inputInputData);
        sendKeys(inputInputData, acaoEditada.getQualEOPrazoParaExecucao());
    }

    public void clicarBtnConfirmarModalAcaoAtrasada() {
        clickAndHighlight(buttonDialogEditarAcaoConfirm);
    }

    public void clicarBtnConfirmarAlteracao() {
        clickAndHighlight(buttonDialogButtonYes);
    }

    public void validaDataPrevisaoEditada(Acao acaoEditada) {
        scrollToElement(tdVisualizarAcaoAceitePrevisaoConclusao);
        Assert.assertEquals(getText(tdVisualizarAcaoAceitePrevisaoConclusao), acaoEditada.getQualEOPrazoParaExecucao());
    }

    public void preencherDadosExecucao(Acao acao) {
        scrollToElement(separatorRegistreOsResultados);
        clearForce(inputExecutarAcaoDataFinalizacaoInputData);
        sendKeys(inputExecutarAcaoDataFinalizacaoInputData, acao.getDataDeFinalizacao());
        sendKeysWithJavaScript(textareaResultadoAlcancado, acao.getResultadosAlcancados());
        sendKeysWithJavaScript(textareaComentario, acao.getComentarios());
        sendKeysWithJavaScript(textareaSugestoesAcoesFuturas, acao.getSugestoesDeAcoesFuturas());
        uploadArquivo(1);
    }

    public void clicarBtnSalvarEFinalizarAcao() {
        clickAndHighlight(buttonVisualizarAcaoFooterSalvarFinalizarExecucaoButton);
    }

    public void validaDadosExecucaoReadOnly(Acao acao) {
        Assert.assertEquals(getText(divExecutarAcaoDataFinalizacaoExecutada), acao.getDataDeFinalizacao());
        Assert.assertEquals(getText(divExecutarAcaoDataResultadosExecucao), acao.getResultadosAlcancados());
        Assert.assertEquals(getText(divExecutarAcaoDataComentariosExecucao), acao.getComentarios());
        Assert.assertEquals(getText(divExecutarAcaoDataFinalizacaoRealExecucao), getDay());
    }

    public void validaAcaoPublica(Acao acao) {
        if (!acao.isAcaoPublica()) {
            scrollToElement(divHeaderVisualizarAcaoPublica);
            expectElementVisible(divHeaderVisualizarAcaoPublica);
        } else {
            expectElementNotVisible(divHeaderVisualizarAcaoPublica);
        }
    }

    public void clicarBtnReprovarEvidencia() {
        clickAndHighlight(buttonVisualizarAcaoFooterReprovarEvidenciaButton);
    }

    public void preencherDadosDoModalReprovarEvidencia(String justificativa) {
        sendKeysWithJavaScript(textareaDialogReprovarEvidenciaTextJustificativa, justificativa);
        uploadArquivo(1);
    }

    public void clicarBtnConfirmarModalReprovarEvidencia() {
        clickAndHighlight(buttonDialogAprovarEvidenciaConfirm);
    }

    public void clicarBtnAprovarEvidencia() {
        clickAndHighlight(buttonVisualizarAcaoFooterAprovarEvidenciaButton);
    }

    public void validarExibicaoDoModalDeAprovarEvidencia() {
        expectElementVisible(radioButtonOpcaoAprovarEvidenciaSim);
        expectElementVisible(radioButtonOpcaoAprovarEvidenciaNao);
        expectElementVisible(buttonDialogAprovarEvidenciaConfirm);
        expectElementVisible(buttonDialogAprovarEvidenciaButtonCancel);
    }

    public void preencherDadosAprovarEvidencia(Acao acao) {
        if (acao.isHouveAVerificacaoDaEficaciaDaAcao()) {
            sendKeysWithJavaScript(textareaDialogAprovarEvidenciaTextComentario, acao.getComentarioComprovacao());
            clearForce(inputDialogAprovarEvidenciaDataAprovacao);
            sendKeys(inputDialogAprovarEvidenciaDataAprovacao, acao.getDataDaComprovacao());
            uploadArquivo(1);
        } else {
            sendKeysWithJavaScript(textareaDialogAprovarEvidenciaTextComentario, acao.getComentarioComprovacao());
        }
    }

    public void selecionarOpcaoHouveVerificacaoEficacia(Acao acao) {
        if (acao.isHouveAVerificacaoDaEficaciaDaAcao()) {
            clickWithAction(radioButtonOpcaoAprovarEvidenciaSim);
        } else {
            clickWithAction(radioButtonOpcaoAprovarEvidenciaNao);
        }
    }

    public void clicarBtnConfirmarModalAprovarEvidencia() {
        clickAndHighlight(buttonDialogAprovarEvidenciaConfirm);
    }

    public void validarQueOsBotoesAprovarReprovarEvidenciasNaoSaoExibidos() {
        scrollToElement(buttonVisualizarAcaoFooterEditarAcaoButton);
        expectElementNotVisible(buttonVisualizarAcaoFooterAprovarEvidenciaButton);
        expectElementNotVisible(buttonVisualizarAcaoFooterReprovarEvidenciaButton);
    }

    public void clicarBtnReabrirAcao() {
        clickAndHighlight(buttonVisualizarAcaoFooterReabrirAcaoExecucaoButton);
    }

    public void clicarBtnConfirmarModalReabrirAcao() {
        clickAndHighlight(buttonDialogEditarAcaoConfirm);
    }

    public void preencherJustificativaReabrir(Acao acaoEditada) {
        sendKeysWithJavaScript(textareaJustificativaReaberturaAcao, acaoEditada.getJustificativaReabertura());
        uploadArquivo(1);
    }

    public void editarInformacoesIniciais(Acao acaoEditada) {
        if (acaoEditada.isAcaoPublica()) {
            clickWithAction(radioButtonPublicaSim);
        } else {
            clickWithAction(radioButtonPublicaNao);
        }

        clearForce(inputDialogEditarAcaoPrazo);
        sendKeys(inputDialogEditarAcaoPrazo, acaoEditada.getQualEOPrazoParaExecucao());
        sendKeysWithJavaScript(textAreaDialogEditarAcaoDescricaoAcao, acaoEditada.getDescritivoDaAcao());
    }

    public void validoPlanoDeAcaoDetalhado(PlanoDeAcao planoDeAcao, Acao acao, ResponsavelAcao responsavelAcao) {
        clickAndHighlight(expansionPanelPlanoAcaoDetalhado);

        Assert.assertEquals(getText(labelBandeira), acao.getFocoCausaDaAcaoValue());
        Assert.assertEquals(getText(labelTipoAcao), acao.getQualOTipoDaAcaoValue());
        Assert.assertEquals(getText(labelArea), responsavelAcao.getArea());
        Assert.assertEquals(getText(labelGerente), responsavelAcao.getGerente());
        Assert.assertEquals(getText(labelResponsavel), responsavelAcao.getResponsavel());
        Assert.assertEquals(getText(tdVisualizarAcaoAceiteDescricaoAcao), acao.getDescritivoDaAcao());

        Assert.assertEquals(getText(labelDataFinalizacao), acao.getDataDeFinalizacao());
        Assert.assertEquals(getText(divExecutarAcaoDataResultadosExecucao), acao.getResultadosAlcancados());
        Assert.assertEquals(getText(divExecutarAcaoDataComentariosExecucao), acao.getComentarios());
        Assert.assertEquals(getText(divExecutarAcaoDataSugestoesExecucao), acao.getSugestoesDeAcoesFuturas());
    }

    public void clicarBtnDesdobraAcao() {
        clickAndHighlight(buttonVisualizarAcaoFooterDesdobrarButton);
    }

    public void preencherDadosIniciasModalDesdobrarAcao(Acao acao) {
        if (acao.isAcaoPublica()) {
            clickWithAction(radioButtonPublica0);
        } else {
            clickWithAction(radioButtonPublica1);
        }

        clearForce(inputPrazo);
        sendKeys(inputPrazo, acao.getQualEOPrazoParaExecucao());
        sendKeysWithJavaScript(textareaDescricaoAcao, acao.getDescritivoDaAcao());
    }

    public void clicarBtnConfirmaModalDesdobrarAcao() {
        clickAndHighlight(buttonDialogButtonConfirm);
    }

    public void clicarBtnAdicionarResponsavelModalDesdobrada() {
        clickAndHighlight(buttonButtonAddResponsavel);
    }

    public void validaExibicaoDosDetalhesDaAcaoDesdobrada(PlanoDeAcao planoDeAcao, Acao acao, ResponsavelAcao responsavelAcao) {
        Assert.assertEquals(getText(divResumoAcaoBandeira).trim(), planoDeAcao.getBandeira());
        Assert.assertEquals(getText(divResumoAcaoFatoOcorrido).trim(), planoDeAcao.getFatoOcorrido());
        Assert.assertEquals(getText(divResumoAcaoCausaIdentificada).trim(), planoDeAcao.getCausaProvavelIdentificada());
        Assert.assertEquals(getText(divResumoAcaoCausaOrigem).trim(), "Desdobrada");
        Assert.assertEquals(getText(divResumoAcaoCausaFoco).trim(), acao.getFocoCausaDaAcaoValue());
        Assert.assertEquals(getText(divResumoAcaoCausaOperacao).trim().toUpperCase(), CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, responsavelAcao.getQualEAOperacaoValue()).toUpperCase());
        Assert.assertEquals(getText(divResumoAcaoCausaComplexo).trim(), responsavelAcao.getQualEOComplexoValue());
        Assert.assertEquals(getText(divResumoAcaoCausaSolicitante).trim(), planoDeAcao.getSolicitante());
        Assert.assertEquals(getText(divResumoAcaoCausaTipoAcao).trim(), acao.getQualOTipoDaAcaoValue());

        Assert.assertEquals(getText(divVisualizarAcaoAceiteArea).trim(), responsavelAcao.getArea());
        Assert.assertEquals(getText(divVisualizarAcaoAceiteGerente).trim(), responsavelAcao.getGerente());
        Assert.assertEquals(getText(divVisualizarAcaoAceiteResponsavel).trim(), responsavelAcao.getResponsavel());


        Assert.assertEquals(getText(tdVisualizarAcaoAceiteDescricaoAcao), acao.getDescritivoDaAcao());
        Assert.assertEquals(getText(tdVisualizarAcaoAceitePrazo), acao.getQualEOPrazoParaExecucao());
    }

    public void clicarBtnRecusarAcao() {
        scrollToElement(separatorAcaoASerRealizada);
        clickAndHighlight(buttonVisualizarAcaoFooterRecusarAcaoButton);
    }

    public void selecionarOpcaoDeRecusaDaAcao(Acao acao) {
        switch (acao.getPorQueVoceEstaRecusandoAAcao()) {
            case "Discorda do prazo":
                clickWithAction(radioButtonOpcaoRecusarAcaoPrazo);
                break;
            case "Discorda da descrição":
                clickWithAction(radioButtonOpcaoRecusarAcaoDescricao);
                break;
            case "Discorda do responsável":
                clickWithAction(radioButtonOpcaoRecusarAcaoResponsavel);
                break;
        }

    }

    public void preencherDadosRecusaAcao(Acao acao) {
        switch (acao.getPorQueVoceEstaRecusandoAAcao()) {
            case "Discorda da descrição":
                sendKeysWithJavaScript(textareaDialogRecusarAcaoTextDescription, acao.getDescricaoDaAcaoSugiraUmaNovaDescricaoDaAcao());
                break;
            case "Discorda do prazo":
                clearForce(inputDialogRecusarAcaoTextPrazo);
                sendKeys(inputDialogRecusarAcaoTextPrazo, acao.getQualEOPrazoParaExecucaoSoliciteUmNovoPrazo());
                break;
            case "Discorda do responsável":
                sendKeysWithJavaScript(textareaDialogRecusarAcaoTextJustificativa, acao.getJustificativaDiscordaDoResponsavel());
                break;
        }
    }

    public void clicarBtnEnviarSolicitacaoRecusaAcao() {
        clickAndHighlight(buttonDialogRecusarAcaoConfirm);
    }

    public void clicarTABHistorico() {
        clickAndHighlight(tabHistorico);
    }

    public void validaRecusaNoHistorico(Acao acao) {
        Assert.assertEquals(getText(divVisualizarAcaoSolicitacaoDataRecusa), getDay());
        Assert.assertEquals(getText(divVisualizarAcaoSolicitacaoTipoRecusa), acao.getPorQueVoceEstaRecusandoAAcao());
        switch (acao.getPorQueVoceEstaRecusandoAAcao()) {
            case "Discorda da descrição":
                Assert.assertEquals(getText(divVisualizarAcaoSolicitacaoDescricaoAtual), acao.getDescritivoDaAcao());
                Assert.assertEquals(getText(divVisualizarAcaoSolicitacaoNovaAcao), acao.getDescricaoDaAcaoSugiraUmaNovaDescricaoDaAcao());
                break;
            case "Discorda do prazo":
                Assert.assertEquals(getText(divVisualizarAcaoSolicitacaoPrazoAtual), acao.getQualEOPrazoParaExecucao());
                Assert.assertEquals(getText(divVisualizarAcaoSolicitacaoNovoPrazo), acao.getQualEOPrazoParaExecucaoSoliciteUmNovoPrazo());
                break;
            case "Discorda do responsável":
                Assert.assertEquals(getText(divVisualizarAcaoSolicitacaoJustificativaRecusa), acao.getJustificativaDiscordaDoResponsavel());
                break;
            default:

        }
    }

    public void selecionarMotivoRecusa(Acao acao) {
        if (acao.isRecusarAcaoDesdobrada()) {
            clickWithAction(radioButtonOpcaoRecusarAcaoRotina);
            acao.setMotivoRecusaDesdobrada(getText(radioButtonOpcaoRecusarAcaoRotina));
        } else {
            clickWithAction(radioButtonOpcaoRecusarAcaoNaoAplica);
            acao.setMotivoRecusaDesdobrada(getText(radioButtonOpcaoRecusarAcaoNaoAplica));
        }
    }

    public void preencherJustificativaRecusarDesdobrar(Acao acao) {
        sendKeysWithJavaScript(textAreaDialogRecusarAcaoTextJustificativa, acao.getJustificativaRecusarAcaoDesdobrada());
    }

    public void clicarEnviarSolicitacao() {
        clickAndHighlight(buttonDialogRecusarAcaoConfirm);
    }

    public void validaRecusaAcaoDesdobradaNoHistorico(Acao acao) {
        Assert.assertEquals(getText(divVisualizarAcaoSolicitacaoTipoRecusa), acao.getMotivoRecusaDesdobrada());
        Assert.assertEquals(getText(divVisualizarAcaoSolicitacaoJustificativaRecusa), acao.getJustificativaRecusarAcaoDesdobrada());
    }

    public void preencherJustificativaRecusarAlteracao(Acao acao) {
        sendKeysWithJavaScript(textAreaJustificativa, acao.getJustificativaAcaoRecusada());
    }

    public void clicarBtnConfirmarRecusaAcao() {
        clickAndHighlight(btnConfirmarRecusaAcao);
    }

    public void validarHistoricoAlteracaoRecusada(Acao acao) {
        Assert.assertEquals(getText(labelJustificativaRecusada), acao.getJustificativaAcaoRecusada());
    }

    public void clicarBtnRecusarAlteracao() {
        clickAndHighlight(buttonVisualizarAcaoFooterRecusarAlteracaoButton);
    }

    public void clicarBtnAceitarAlteracao() {
        clickAndHighlight(buttonVisualizarAcaoFooterAceitarAlteracaoButton);
    }

    public void preencherJustificativaEditarAcaoRecusada(Acao acao) {
        sendKeysWithJavaScript(textareaDialogEditarAcaoTextJustificativa, acao.getJustificativaAceitarAlteracao());
        uploadArquivo(1);
    }

    public void preencherDadosIniciasModalEditarAceitarAlteracaoAcao(Acao acaoEditada) {
        if (acaoEditada.isAcaoPublica()) {
            clickWithAction(radioButtonPublicaSim);
        } else {
            clickWithAction(radioButtonPublicaNao);
        }
        clearForce(inputDialogEditarAcaoTextPrazo);
        sendKeys(inputDialogEditarAcaoTextPrazo, acaoEditada.getQualEOPrazoParaExecucao());
        sendKeysWithJavaScript(textareaDialogEditarAcaoTextAcao, acaoEditada.getDescritivoDaAcao());
    }

    public void clicarTABAceite() {
        clickAndHighlight(tabAceite);
    }

    public void validarDetalhesDaAcaoPublica(Acao acao, ResponsavelAcao responsavelAcao) {
        clickAndHighlight(btnLupaDetalhes);
        Assert.assertEquals(getText(divVisualizarAcaoAceiteArea).trim(), responsavelAcao.getArea());
        Assert.assertEquals(getText(divVisualizarAcaoAceiteGerente).trim(), responsavelAcao.getGerente());
        Assert.assertEquals(getText(divVisualizarAcaoAceiteResponsavel).trim(), responsavelAcao.getResponsavel());
        Assert.assertEquals(getText(tdVisualizarAcaoAceiteDescricaoAcao), acao.getDescritivoDaAcao());
    }
}