package br.com.pom.sro.central_de_acao;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.api.model.incidente.Incidente;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class ListagemPlanoDeAcaoPage extends GeralPage {

    /*
    Container
     */

    IntFunction<By> tdTableCellCheckboxAcao = (index) -> By.cssSelector("#checkbox-selecionar-acao-lote-" + index + " > label");
    IntFunction<By> tdTableCellOrigemPlanoAcao = (index) -> By.id("table-cell-origemPlanoAcao-" + index);
    IntFunction<By> tdTableCellDescricaoOrigem = (index) -> By.id("table-cell-descricaOrigem-" + index);
    IntFunction<By> tdTableCellNomeAreaResponsavel = (index) -> By.id("table-cell-nomeAreaResponsavel-" + index);
    IntFunction<By> tdTableCellOrigemAcao = (index) -> By.id("table-cell-origemAcao-" + index);
    IntFunction<By> tdTableCellDescricaoOrigemAcao = (index) -> By.id("table-cell-descricaOrigem-" + index);
    IntFunction<By> tdTableCellAreaResponsavelAcao = (index) -> By.id("table-cell-nomeAreaResponsavel-" + index);
    IntFunction<By> solicitante = (index) -> By.id("table-cell-nomeSolicitante-" + index);
    IntFunction<By> responsavel = (index) -> By.id("table-cell-nomeSuperior-" + index);
    IntFunction<By> tdTableCellStatus = (index) -> By.id("table-cell-status-" + index);
    IntFunction<By> buttonButtonEditPlanoAcao = (index) -> By.id("button-detail-plano-" + index);
    IntFunction<By> buttonButtonEditAcao = (index) -> By.id("button-edit-sindicancia-" + index);
    IntFunction<By> tdTableCellNomeSuperior = (index) -> By.id("table-cell-nomeSuperior-" + index);
    IntFunction<By> tdTableCellPrazoIndicator = (index) -> By.id("table-cell-prazoIndicator-" + index);
    IntFunction<By> tdTableCellIdAcao = (index) -> By.id("table-cell-idAcao-" + index);
    IntFunction<By> tdTableCellDescricaoAcao = (index) -> By.id("table-cell-descricaoAcao-" + index);
    IntFunction<By> tdTableCellImpacto = (index) -> By.id("table-cell-impacto-" + index);
    IntFunction<By> tdTableCellPrazo = (index) -> By.id("table-cell-prazo-" + index);
    IntFunction<By> tdTableCellDataFinalizacao = (index) -> By.id("table-cell-dataFinalizacao-" + index);
    IntFunction<By> tdTableCellActions = (index) -> By.id("table-cell-actions-" + index);

    private By iconIconSearch = By.id("icon-search");
    private By inputSearchSinistro = By.id("search-sinistro");
    private By aTabPlano = By.id("tab-plano");
    private By aTabAceite = By.id("tab-aceite");
    private By aTabExecucao = By.id("tab-execucao");
    private By aTabEncerradas = By.id("tab-encerradas");
    private By aTabPublica = By.id("tab-publicas");
    private By buttonButtonFilter = By.id("button-filter");
    private By aButtonExport = By.id("button-export");
    private By divFilterChips = By.id("filter-chips");
    private By divFilterChipDateSince = By.id("filter-chip-dateSince");
    private By iconChipButtonRemoveFilterFieldDateSince = By.id("chip-button-remove-filter-field-dateSince");
    private By buttonButtonChipClearFilter = By.id("button-chip-clear-filter");
    private By thTableHeaderPrazoIndicator = By.id("table-header-prazoIndicator");
    private By thTableHeaderIdPlanoAcao = By.id("table-header-idPlanoAcao");
    private By thTableHeaderIdAcao = By.id("table-header-idAcao");
    private By thTableHeaderOrigemPlanoAcao = By.id("table-header-origemPlanoAcao");
    private By thTableHeaderNomeSolicitante = By.id("table-header-nomeSolicitante");
    private By thTableHeaderNumAcoesCriadas = By.id("table-header-numAcoesCriadas");
    private By thTableHeaderPrazo = By.id("table-header-prazo");
    private By thTableHeaderStatus = By.id("table-header-status");
    private By thTableHeaderActions = By.id("table-header-actions");
    private By tdTableCellIdPlanoAcao0 = By.id("table-cell-idPlanoAcao-0");
    private By tdTableCellOrigemPlanoAcao0 = By.id("table-cell-origemPlanoAcao-0");
    private By tdTableCellNomeSolicitante0 = By.id("table-cell-nomeSolicitante-0");
    private By tdTableCellNumAcoesCriadas0 = By.id("table-cell-numAcoesCriadas-0");
    private By paginatorTableSindicanciaPaginator = By.id("table-sindicancia-paginator");
    private By btnFiltrarGerente = By.id("button-advanced-filter-gerente-search");
    private By inputMatricula = By.id("nomeMatricula");
    private By selecionarMatricula = By.id("dialog-button-yes");
    private By buscarMatricula = By.id("button-search-employee");
    private By checkBoxGerente = By.id("tbEmployee-checkbox-0");
    private By aplicoFiltro = By.id("button-advanced-filter-apply");
    private By btnFiltrarResponsavel = By.id("button-advanced-filter-responsavel-search");
    private By listagemDeAcoesLabel = By.cssSelector("body > sro-root > sro-shell > div > sro-tabs-acoes > div:nth-child(1) > div.col-auto > sro-default-title");
    private By btnIncluirAcao = By.id("button-incluir-novo-plano");
    private By btnAceitarAcaoSelecionada = By.id("btn-aceitar-acoes-lote");
    private By btnAlterarResponsavelSelecionado = By.id("btn-alterar-responsavel-lote");

    /*
    Modal aceitar em lote
     */

    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");
    private By btnEditarAcao = By.id("button-detail-plano-0");
    private By inputAreaFiltro = By.id("advanced-filter-nomeAreaResponsavel");

    public void validoListagemPlanoDeAcao(PlanoDeAcao planoDeAcao, int linha) {
        linha--;
        String origemExpect = planoDeAcao.getOrigem() + " #" + planoDeAcao.getIdSindicancia();
        Assert.assertEquals(getText(tdTableCellOrigemPlanoAcao.apply(linha)).toLowerCase(), origemExpect.toLowerCase());
        Assert.assertEquals(getText(solicitante.apply(linha)), planoDeAcao.getSolicitante());
        Assert.assertEquals(getText(tdTableCellStatus.apply(linha)), "Elaborar plano");
    }

    public void ordenarIdDecrescente() {
        clickAndHighlight(thTableHeaderIdPlanoAcao);
        expectLoading();
        clickAndHighlight(thTableHeaderIdPlanoAcao);
        waitTime(500);
    }

    public void clicarBtnEditarListagem(int linha) {
        linha--;
        waitTime(1000);
        clickAndHighlight(buttonButtonEditPlanoAcao.apply(linha));
    }

    public void validoAExibicaoListagemAcoes() {
        expectElementVisible(listagemDeAcoesLabel);
    }

    public void validaResultadoDaBuscaPorTextoListagemAcao(String textoBusca) {
        Assert.assertTrue(expectText(tdTableCellNomeSolicitante0, textoBusca), "Texto deve ser :" + textoBusca);
    }

    public void clicarNaTabParaAceite() {
        clickAndHighlight(aTabAceite);
    }

    public void escolhoOGerenteNoFiltro(String gerente) {
        clickAndHighlight(buttonButtonFilter);
        clickAndHighlight(btnFiltrarGerente);
        selecionarResponsavel(gerente);
        clickAndHighlight(aplicoFiltro);
    }

    public void validoListagemAcaoComFiltroGerente(String gerenteEsperado, int linha) {
        linha--;
        Assert.assertTrue(getText(tdTableCellNomeSuperior.apply(linha)).startsWith(gerenteEsperado), "Esperado que contenha o gerente : " + gerenteEsperado);
    }

    public void limpoOFiltro() {
        clickAndHighlight(buttonButtonChipClearFilter);
    }

    public void clicarNaTabEmExecução() {
        clickAndHighlight(aTabExecucao);
    }

    public void escolhoResponsavelNoFiltro(String responsavel) {
        clickAndHighlight(buttonButtonFilter);
        clickAndHighlight(btnFiltrarResponsavel);
        selecionarResponsavel(responsavel);
        clickAndHighlight(aplicoFiltro);
    }

    public void validoListagemAcaoComFiltroResponsavel(String responsavel, int linha) {
        linha--;
        expectLoading();
        String textoAtual = getText(tdTableCellNomeSuperior.apply(linha));
        Assert.assertTrue(textoAtual.contains(responsavel), "Esperado que contenha o responsável : " + responsavel + " Atual " + textoAtual);
    }

    public void validaExibicaoDaAcaoNaListagemParaAceite(PlanoDeAcao planoDeAcao, Acao acao, ResponsavelAcao responsavelAcao, int linha) {
        linha--;
        Assert.assertEquals(getText(tdTableCellIdAcao.apply(linha)), String.valueOf(acao.getIdAcao()));
        Assert.assertEquals(getText(tdTableCellDescricaoOrigemAcao.apply(linha)), acao.getOrigemAcao());
        Assert.assertEquals(getText(tdTableCellNomeSuperior.apply(linha)), responsavelAcao.getGerente().concat("\n").concat(responsavelAcao.getResponsavel()));
        Assert.assertEquals(getText(tdTableCellImpacto.apply(linha)).toUpperCase(), acao.getQualOImpactoDaAcaoValue().toUpperCase());
        Assert.assertEquals(getText(tdTableCellPrazo.apply(linha)).toUpperCase(), acao.getQualEOPrazoParaExecucao());
    }

    public void ordenarAcaoIdDecrescente() {
        clickAndHighlight(thTableHeaderIdAcao);
        expectLoading();
        clickAndHighlight(thTableHeaderIdAcao);
        waitTime(700);
    }

    public void clicarNaTabEncerradas() {
        expectLoading();
        clickAndHighlight(aTabEncerradas);
    }

    public void validaExibicaoDaAcaoNaListagemEncerradaSRO(PlanoDeAcao planoDeAcao, Acao acao, ResponsavelAcao responsavelAcao, String status, int linha) {
        linha--;
        Assert.assertEquals(getText(tdTableCellIdAcao.apply(linha)), String.valueOf(acao.getIdAcao()));
        Assert.assertEquals(getText(tdTableCellDescricaoOrigemAcao.apply(linha)), acao.getOrigemAcao());
        Assert.assertEquals(getText(tdTableCellAreaResponsavelAcao.apply(linha)), responsavelAcao.getArea());
        Assert.assertEquals(getText(tdTableCellNomeSuperior.apply(linha)), responsavelAcao.getGerente().concat("\n").concat(responsavelAcao.getResponsavel()));
        Assert.assertEquals(getText(tdTableCellImpacto.apply(linha)).toUpperCase(), acao.getQualOImpactoDaAcaoValue().toUpperCase());
//        Assert.assertEquals(getText(tdTableCellDataFinalizacao.apply(linha)).toUpperCase(), acao.getQualEOPrazoParaExecucao());
        Assert.assertEquals(getText(tdTableCellStatus.apply(linha)), status);
    }

    public void clicarBtnEditarAcaoListagem(int linha) {
        linha--;
        expectLoading();
        clickAndHighlight(buttonButtonEditAcao.apply(linha));
    }

    public void validarStatusDaAcao(int linha, String status) {
        linha--;
        Assert.assertEquals(getText(tdTableCellStatus.apply(linha)), status);
    }

    public void validaOrigemAcao(int linha, Acao acao) {
        linha--;
        Assert.assertEquals(getText(tdTableCellDescricaoOrigemAcao.apply(linha)), acao.getOrigemAcao());
    }

    public void validaExibicaoDaAcaoNaListagemPublicaSRO(PlanoDeAcao planoDeAcao, Acao acao, ResponsavelAcao responsavelAcao, int linha) {
        linha--;
        Assert.assertEquals(getText(tdTableCellDescricaoOrigem.apply(linha)), acao.getOrigemAcao());
        Assert.assertEquals(getText(responsavel.apply(linha)), responsavelAcao.getGerente().concat("\n").concat(responsavelAcao.getResponsavel()));
        Assert.assertEquals(getText(tdTableCellStatus.apply(linha)), "Aguardando aceite");
    }

    public void clicarNaTabPublica() {
        clickAndHighlight(aTabPublica);
    }

    public void validaExibicaoDaAcaoNaListagemPublica(PlanoDeAcao planoDeAcao, Acao acao, ResponsavelAcao responsavelAcao, int linha) {
        linha--;
        Assert.assertEquals(getText(tdTableCellDescricaoOrigem.apply(linha)), acao.getOrigemAcao());
        Assert.assertEquals(getText(responsavel.apply(linha)), responsavelAcao.getGerente().concat("\n").concat(responsavelAcao.getResponsavel()));
    }

    public void clicarBtnIncluirNovoPlano() {
        clickAndHighlight(btnIncluirAcao);
    }

    public void clicarBtnEditarAcaoNaListagem() {
        clickAndHighlight(btnEditarAcao);
    }

    public void clicarNaTabEmPublica() {
        clickAndHighlight(aTabPublica);
    }

    public void filtroPelaArea() {
        clickAndHighlight(buttonButtonFilter);
        clickAndHighlight(inputAreaFiltro);
        sendKeys(inputAreaFiltro, "RUMO - Manut. Mecânica Turno");
        clickAndHighlight(aplicoFiltro);
    }

    public void validoListagemAcaoComFiltroArea(String areaEsperada, int linha) {
        linha--;
        Assert.assertTrue(getText(tdTableCellNomeAreaResponsavel.apply(linha)).startsWith(areaEsperada), "Esperado que contenha a área : " + areaEsperada);
    }

    public void selecionarAcoes(int quantidade) {
        for (int i = 0; i < quantidade; i++) {
            expectElementClickable(tdTableCellCheckboxAcao.apply(i));
            clickWithAction(tdTableCellCheckboxAcao.apply(i));
        }
    }

    public void clicarBtnAceitarSelecionadas() {
        clickAndHighlight(btnAceitarAcaoSelecionada);
    }

    public void validaModalAceitarAcaoEmLote() {
        Assert.assertEquals(getText(divDialogConfirmationMessage), "Atenção: você está aceitando múltiplas ações. Gostaria de confirmar o aceite de 10 ações?");
        expectElementVisible(buttonDialogButtonNo);
        expectElementVisible(buttonDialogButtonYes);
    }

    public void clicarBtnOkModalAceitarAcao() {
        clickAndHighlight(buttonDialogButtonYes);
    }

    public void validarAcoesListagemEmExecucao(int quantidade, Acao acao, ResponsavelAcao responsavelAcao, PlanoDeAcao planoDeAcao, String status) {
        for (int i = 0; i < quantidade; i++) {
            Assert.assertEquals(getText(tdTableCellIdAcao.apply(i)), String.valueOf(acao.getIdAcao() - i));

            Assert.assertEquals(
                    getText(tdTableCellDescricaoOrigemAcao.apply(i)),
                    acao.getOrigemAcao().replace(String.valueOf(planoDeAcao.getIdSindicancia()),
                            String.valueOf(planoDeAcao.getIdSindicancia() - i)
                    )
            );
            Assert.assertEquals(getText(tdTableCellNomeSuperior.apply(i)), responsavelAcao.getGerente().concat("\n").concat(responsavelAcao.getResponsavel()));
            Assert.assertEquals(getText(tdTableCellImpacto.apply(i)).toUpperCase(), acao.getQualOImpactoDaAcaoValue().toUpperCase());
            Assert.assertEquals(getText(tdTableCellPrazo.apply(i)).toUpperCase(), acao.getQualEOPrazoParaExecucao());
            Assert.assertEquals(getText(tdTableCellStatus.apply(i)), status);
        }
    }

    public void clicarBtnAlterarResponsavelLote() {
        clickAndHighlight(btnAlterarResponsavelSelecionado);
    }

    public void validarModalConfirmarAlteracoes() {
        Assert.assertEquals(getText(divDialogConfirmationMessage), "Atenção: você está alterando o responsável de múltiplas ações. Gostaria de confirmar a alteração de 10 ações?");
        expectElementVisible(buttonDialogButtonNo);
        expectElementVisible(buttonDialogButtonYes);
    }

    public void clicarBtnOkModalConfirmarAlteracao() {
        clickAndHighlight(buttonDialogButtonYes);
    }

    public void validarPlanoDeAcaoCriadoPeloIncidente(Incidente incidente, Acao acao) {
        Assert.assertEquals(getText(tdTableCellOrigemPlanoAcao0), "Incidente #" + incidente.getIdIncidente());
        acao.setIdPlanoAcao(Integer.parseInt(getText(tdTableCellIdPlanoAcao0)));
    }
}
