package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class SobreOSinistroNovoSinistroPage extends GeralPage {

    Function<Integer, By> optionTipoSinistro = (Integer index) -> By.id("idTipoAcidente-" + index);
    private By separatorSobreOSinistro = By.name("Sobre o Sinistro");
    private By inputDataSinistro = By.id("dataSinistro");
    private By inputHoraSinistro = By.id("horarioSinistro");
    private By selectTipoSinistro = By.id("tipoAcidente");
    private By inputExtensaoDaViaDanificada = By.id("extensaoViaDanificada");

    private By optionInterdicaoCirculacaoSim = By.id("flagInterdicaoCirculacao-sim");
    private By optionInterdicaoCirculacaoNao = By.id("flagInterdicaoCirculacao-nao");

    private By divInterdicaoDaCirculacao = By.cssSelector("#formSinistro > div.sinistro.row.align-items-center.form-group > div.interdicaoCirculacao");
    private By divTipoSinistro = By.cssSelector("#formSinistro > div.sinistro.row.align-items-center.form-group > div.col-3.offset-1.orange-background.ng-untouched.ng-pristine.ng-invalid");

    private By btnEditar = By.id("edit-sinistro");

    public void preencherCampos(Sinistro sinistro) {
        scrollToElement(separatorSobreOSinistro);
        clearForce(inputDataSinistro);
        sendKeys(inputDataSinistro, sinistro.getDate());
        clearForce(inputHoraSinistro);
        sendKeys(inputHoraSinistro, sinistro.getHora());
        clickAndHighlight(selectTipoSinistro);
        try {
            clickWithJavaScript(optionTipoSinistro.apply(sinistro.getTipoIndex()));
        } catch (Exception e) {
            clickAndHighlight(selectTipoSinistro);
            clickAndHighlight(optionTipoSinistro.apply(sinistro.getTipoIndex()));
        }
        sinistro.setTipo(getText(selectTipoSinistro));
        sendKeys(inputExtensaoDaViaDanificada, sinistro.getExtensaoDaViaDanificada());
        if (sinistro.isInterdicaoDaCirculacao()) {
            clickWithAction(optionInterdicaoCirculacaoSim);
        } else {
            clickWithAction(optionInterdicaoCirculacaoNao);
        }
    }

    public void validaCamposVazios() {
        scrollToElement(separatorSobreOSinistro);
        Assert.assertEquals(getValue(inputDataSinistro), "");
        Assert.assertEquals(getValue(inputHoraSinistro), "");
        Assert.assertEquals(getText(selectTipoSinistro), "Escolha abaixo");
        Assert.assertEquals(getValue(inputExtensaoDaViaDanificada), "");
    }

    public void validaCamposPreenchidos(Sinistro sinistro) {
        scrollToElement(separatorSobreOSinistro);
        expectValue(inputDataSinistro, sinistro.getDate());
        Assert.assertEquals(getValue(inputDataSinistro), sinistro.getDate());
        Assert.assertEquals(getValue(inputHoraSinistro), sinistro.getHora());
        Assert.assertEquals(getValue(selectTipoSinistro), sinistro.getTipo());
        Assert.assertEquals(getValue(inputExtensaoDaViaDanificada), String.valueOf(sinistro.getExtensaoDaViaDanificada()));
    }

    public void validaCamposPreenchidosReadOnly(Sinistro sinistro) {
        expectLoading();
        scrollToElement(separatorSobreOSinistro);
        expectValue(inputDataSinistro, sinistro.getDate());
        Assert.assertEquals(getValue(inputDataSinistro), sinistro.getDate());
        expectValue(inputHoraSinistro, sinistro.getHora());
        Assert.assertEquals(getValue(inputHoraSinistro), sinistro.getHora());
        Assert.assertEquals(getValue(selectTipoSinistro), sinistro.getTipo());
        Assert.assertEquals(getValue(inputExtensaoDaViaDanificada), String.valueOf(sinistro.getExtensaoDaViaDanificada()));
    }

    public void clicarBtnEditar() {
        scrollToElement(btnEditar);
        clickAndHighlight(btnEditar);
    }

    public void editarData(String data) {
        scrollToElement(inputDataSinistro);
        clearForce(inputDataSinistro);
        sendKeys(inputDataSinistro, data);
    }

    public void validaCamposSobreOSinistroPreenchidosViaOS(Sinistro sinistro) {
        scrollToElement(separatorSobreOSinistro);
        Assert.assertEquals(getValue(inputDataSinistro), sinistro.getDateOS());
        Assert.assertEquals(getValue(inputHoraSinistro), sinistro.getHoraOS());
    }
}
