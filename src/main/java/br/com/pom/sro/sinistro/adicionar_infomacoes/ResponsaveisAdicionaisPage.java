package br.com.pom.sro.sinistro.adicionar_infomacoes;

import br.com.api.model.sinistro.ResponsavelAcionado;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class ResponsaveisAdicionaisPage extends GeralPage {

    Function<Integer, By> checkBoxColaborador = (Integer index) -> By.id("tbEmployee-nome-" + index);
    Function<Integer, By> cellNomeResponsavel = (Integer index) -> By.id("table-recurso-humano-cell-nome-" + index);
    Function<Integer, By> cellCargo = (Integer index) -> By.id("table-recurso-humano-cell-cargo-" + index);
    Function<Integer, By> cellPrevisaoLocal = (Integer index) -> By.id("table-recurso-humano-cell-previsaoNoLocal-" + index);

    /*
    Campos do modal
     */
    Function<Integer, By> cellTelefone = (Integer index) -> By.id("table-recurso-humano-cell-contato-" + index);
    Function<Integer, By> cellBtnEditar = (Integer index) -> By.id("button-edit-human-resource-" + index);
    Function<Integer, By> cellBtnExcluir = (Integer index) -> By.id("button-remove-human-resource-" + index);
    private By secaoResponsaveisAcionados = By.name("Responsáveis acionados");
    //    private By radioSim = By.id("hasHumanResources-sim");
    private By radioSim = By.cssSelector("#hasHumanResources-sim > label");
    private By btnAdicionarResponsavel = By.id("button-add-human-resource");
    private By lblAcionamentoDeResponsavel = By.id("dialog-header");
    private By lupaNome = By.id("button-search");
    private By inputDataPrevisaoModal = By.id("input-eta-date");
    private By inputHorarioModal = By.id("input-eta-time");
    private By inputTelefoneModal = By.id("input-contact");

    /*
    Lista de responsaveis
     */
    private By lupaColaborador = By.id("button-search-employee");
    private By btnSalvarColaboradorSelecionado = By.id("dialog-button-yes");
    private By btnSalvarResponsavel = By.id("dialog-button-save");
    private By btnCancelarAdicaoResponsavel = By.id("dialog-button-cancel");
    private By inputNomeMatriculaModalInterno = By.id("nomeMatricula");

    private By btnSimDialogExcluir = By.id("dialog-button-yes");

    public void validarSecaoResponsaveisAdicionais() {
        expectElementVisible(secaoResponsaveisAcionados);
    }

    public void selecionarRadioButtonSim() {
        waitTime();
        clickWithAction(radioSim);
    }

    public void clicarBtnAdicionarResponsaveis() {
        clickAndHighlight(btnAdicionarResponsavel);
    }

    public void validarModalAcionamentoDeResponsavel() {
        expectElementVisible(lblAcionamentoDeResponsavel);
    }

    public void preencherModalAcionamentoResponsavel(ResponsavelAcionado responsavelAcionado) {
        clickAndHighlight(lupaNome);
        sendKeys(inputNomeMatriculaModalInterno, responsavelAcionado.getNome());
        clickAndHighlight(lupaColaborador);
        clickAndHighlight(checkBoxColaborador.apply(0));
        clickAndHighlight(btnSalvarColaboradorSelecionado);
        sendKeys(inputDataPrevisaoModal, responsavelAcionado.getData());
        sendKeys(inputHorarioModal, responsavelAcionado.getHora());
        sendKeys(inputTelefoneModal, responsavelAcionado.getTelefone());
    }

    public void clicarBtnSalvar() {
        clickAndHighlight(btnSalvarResponsavel);
    }

    public void validarResponsavelAdicionado(ResponsavelAcionado responsavelAcionado) {
        Assert.assertTrue(getText(cellNomeResponsavel.apply(0)).startsWith(responsavelAcionado.getNome()));
        //            TODO Verificar melhor forma de valida o coordenador
//        Assert.assertEquals(getText(cellCargo.apply(0)), responsavelAcionado.getCargo());
        Assert.assertEquals(getText(cellPrevisaoLocal.apply(0)), responsavelAcionado.getData() + " " + responsavelAcionado.getHora());
        Assert.assertEquals(getText(cellTelefone.apply(0)), responsavelAcionado.getTelefone());
    }

    public void validaSeExisteResponsavel() {
        expectElementVisible(cellNomeResponsavel.apply(0));
    }

    public void clicarBtnEditarResponsavel(int linha) {
        clickAndHighlight(cellBtnEditar.apply(linha - 1));
    }

    public void alterarDadosResponsávelModal(ResponsavelAcionado responsavel) {
        clearForce(inputDataPrevisaoModal);
        sendKeys(inputDataPrevisaoModal, responsavel.getData());
        sendKeys(inputHorarioModal, responsavel.getHora());
        sendKeys(inputTelefoneModal, responsavel.getTelefone());
    }

    public void validaDadosAlteradosDoResponsavel(ResponsavelAcionado responsavelEditado) {
        Assert.assertTrue(getText(cellNomeResponsavel.apply(0)).startsWith(responsavelEditado.getNome()));
        //            TODO Verificar melhor forma de valida o coordenador
//        Assert.assertEquals(getText(cellCargo.apply(0)), responsavelEditado.getCargo());
        Assert.assertEquals(getText(cellPrevisaoLocal.apply(0)), responsavelEditado.getData() + " " + responsavelEditado.getHora());
        Assert.assertEquals(getText(cellTelefone.apply(0)), responsavelEditado.getTelefone());
    }

    public void excluiResponsavel() {
        clickAndHighlight(cellBtnExcluir.apply(0));
    }

    public void validaQueOResponsavelFoiExcluido() {
        expectElementNotVisible(cellNomeResponsavel.apply(0));
    }

    public void confirmaExclusaoDoResponsavel() {
        clickAndHighlight(btnSimDialogExcluir);
    }

    public void cancelaAdicaoDeNovoResponsavel() {
        clickAndHighlight(btnCancelarAdicaoResponsavel);
    }

    public void validarResponsavelAdicionadoReadOnly(ResponsavelAcionado responsavelAcionado) {
        Assert.assertTrue(getText(cellNomeResponsavel.apply(0)).startsWith(responsavelAcionado.getNome()));
//            TODO Verificar melhor forma de valida o coordenador
        //        Assert.assertTrue(responsavelAcionado.getCargo().startsWith(getText(cellCargo.apply(0))));
        Assert.assertEquals(getText(cellPrevisaoLocal.apply(0)), responsavelAcionado.getData() + " " + responsavelAcionado.getHora());
        Assert.assertEquals(getText(cellTelefone.apply(0)), responsavelAcionado.getTelefone());
    }


}
