package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class SobreOTremNovoSinistroPage extends GeralPage {

    IntFunction<By> optionProduto = (index) -> By.id("idTipoProdutoPerigoso-" + index);
    private By separatorSobreOTrem = By.name("Sobre o Trem");
    private By inputPrefixoTrem = By.id("prefixoTrem");
    private By inputNumeroOS = By.id("numeroOS");
    private By inputTaraBruta = By.id("taraBruta");
    private By inputComprimento = By.id("comprimento");
    private By selectProduto = By.id("tipoProdutoPerigoso");
    private By btnEditar = By.id("edit-sobreTrem");

    public void preencherCampos(Sinistro sinistro) {
        scrollToElement(separatorSobreOTrem);
        sendKeys(inputPrefixoTrem, sinistro.getPrefixo());
        sendKeys(inputNumeroOS, sinistro.getNumeroOS());
        sendKeys(inputTaraBruta, sinistro.getTaraBrutaTB());
        sendKeys(inputComprimento, sinistro.getComprimento());

        sinistro.setProduto(
                selectOptionAndReturnValue(
                        selectProduto,
                        optionProduto,
                        sinistro.getProdutoIndex()
                )
        );

    }

    public void validaCamposVazios() {
        scrollToElement(separatorSobreOTrem);
        Assert.assertEquals(getText(inputPrefixoTrem), "");
        Assert.assertEquals(getText(inputNumeroOS), "");
        Assert.assertEquals(getText(inputTaraBruta), "");
        Assert.assertEquals(getText(inputComprimento), "");
        Assert.assertEquals(getText(selectProduto), "Escolha abaixo");
    }

    public void validaCamposPreenchidos(Sinistro sinistro) {
        scrollToElement(separatorSobreOTrem);
        Assert.assertEquals(getValue(inputPrefixoTrem), sinistro.getPrefixo());
        Assert.assertEquals(getValue(inputNumeroOS), sinistro.getNumeroOS());
        Assert.assertEquals(getValue(inputTaraBruta), sinistro.getTaraBrutaTB());
        Assert.assertEquals(getValue(inputComprimento), String.valueOf(sinistro.getComprimento()));
        Assert.assertEquals(getValue(selectProduto), sinistro.getProduto());
    }

    public void validaCamposPreenchidosReadOnly(Sinistro sinistro) {
        scrollToElement(separatorSobreOTrem);
        Assert.assertEquals(getValue(inputPrefixoTrem), sinistro.getPrefixo());
        Assert.assertEquals(getValue(inputNumeroOS), sinistro.getNumeroOS());
        Assert.assertEquals(getValue(inputTaraBruta), sinistro.getTaraBrutaTB());
        Assert.assertEquals(getValue(inputComprimento), String.valueOf(sinistro.getComprimento()));
        Assert.assertEquals(getValue(selectProduto), sinistro.getProduto());
    }

    public void clicarBtnEditar() {
        scrollToElement(btnEditar);
        clickAndHighlight(btnEditar);
    }

    public void editarOS(int os) {
        scrollToElement(inputNumeroOS);
        sendKeys(inputNumeroOS, os);
    }

    public void validaCamposSobreOTremPreenchidosViaOS(Sinistro sinistro) {
        scrollToElement(separatorSobreOTrem);
        Assert.assertEquals(getValue(inputPrefixoTrem), sinistro.getPrefixo());
        Assert.assertEquals(getValue(inputNumeroOS), sinistro.getNumeroOS());
    }
}
