package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.List;
import java.util.function.Function;

public class HistoricoDoSinistro extends GeralPage {

    Function<Integer, By> cellAcaoRealizada = (Integer index) -> By.id("table-cell-tipoEvento-" + index);

    public void validaListaDeAcoes(List<String> listaAcao) {
        for (int i = 0; i < listaAcao.size(); i++) {
            Assert.assertEquals(getText(cellAcaoRealizada.apply(i)), listaAcao.get(i));
        }
    }
}
