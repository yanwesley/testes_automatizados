package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

public class AdicionarVitimasNovoSinistroPage extends GeralPage {


    private By separatorAdicionarVitimas = By.name("Adicionar Vítimas");

    private By divCondutorOuMaquinista = By.cssSelector("#formSinistro > div.vitimas.row.align-items-center > div.col-3.orange-background.margin-left15");
    private By optionExistemVitimasEnvolvidasSim = By.id("existemVitimasEnvolvidas-sim");
    private By optionExistemVitimasEnvolvidasNao = By.id("existemVitimasEnvolvidas-nao");

    private By inputQtdVitimasFatais = By.id("qtdVitimasFatais");
    private By inputQtdVitimasGraves = By.id("qtdVitimasGraves");
    private By inputQtdVitimasLeves = By.id("qtdVitimasLeves");

    private By btnEditar = By.id("edit-addVitimas");

    public void preencherCampos(Sinistro sinistro) {
        scrollToElement(separatorAdicionarVitimas);
        if (sinistro.isExistemVitimasEnvolvidas()) {
            clickWithAction(optionExistemVitimasEnvolvidasSim);
            sendKeys(inputQtdVitimasFatais, sinistro.getVitimasFatais());
            sendKeys(inputQtdVitimasGraves, sinistro.getVitimasGraves());
            sendKeys(inputQtdVitimasLeves, sinistro.getVitimasLeves());
        } else {
            clickWithAction(optionExistemVitimasEnvolvidasNao);
        }
    }

    public void informarQueNaoExisteVitimas() {
        scrollToElement(separatorAdicionarVitimas);
        clickWithAction(optionExistemVitimasEnvolvidasNao);
    }

    public void informarQueExisteVitimas() {
        scrollToElement(separatorAdicionarVitimas);
        clickWithAction(optionExistemVitimasEnvolvidasSim);
    }

    public void validarExibicaoDosCamposParaAdicionarVitimas() {
        expectElementVisible(inputQtdVitimasFatais);
        expectElementVisible(inputQtdVitimasGraves);
        expectElementVisible(inputQtdVitimasLeves);
    }

    public void validarQueNaoExibiuOsCamposParaAdicionarVitimas() {
        expectElementNotVisible(inputQtdVitimasFatais);
        expectElementNotVisible(inputQtdVitimasGraves);
        expectElementNotVisible(inputQtdVitimasLeves);
        assert getAttribute(divCondutorOuMaquinista, "class").endsWith("selected-no");
    }

    public void preencherCamposVazios() {
        sendKeys(inputQtdVitimasFatais, "0");
        sendKeys(inputQtdVitimasGraves, "0");
        sendKeys(inputQtdVitimasLeves, "0" + Keys.TAB);
    }

    public void validaCamposVazios() {
        scrollToElement(separatorAdicionarVitimas);
        clickWithAction(optionExistemVitimasEnvolvidasSim);
        Assert.assertEquals(getValue(inputQtdVitimasFatais), "");
        Assert.assertEquals(getValue(inputQtdVitimasGraves), "");
        Assert.assertEquals(getValue(inputQtdVitimasLeves), "");
    }

    public void validaCamposPreenchidos(Sinistro sinistro) {
        scrollToElement(separatorAdicionarVitimas);
        if (sinistro.isExistemVitimasEnvolvidas()) {
            Assert.assertEquals(getValue(inputQtdVitimasFatais), String.valueOf(sinistro.getVitimasFatais()));
            Assert.assertEquals(getValue(inputQtdVitimasGraves), String.valueOf(sinistro.getVitimasGraves()));
            Assert.assertEquals(getValue(inputQtdVitimasLeves), String.valueOf(sinistro.getVitimasLeves()));
        }
    }

    public void validaCamposPreenchidosReadOnly(Sinistro sinistro) {
        scrollToElement(separatorAdicionarVitimas);
        if (sinistro.isExistemVitimasEnvolvidas()) {
            Assert.assertEquals(getValue(inputQtdVitimasFatais), String.valueOf(sinistro.getVitimasFatais()));
            Assert.assertEquals(getValue(inputQtdVitimasGraves), String.valueOf(sinistro.getVitimasGraves()));
            Assert.assertEquals(getValue(inputQtdVitimasLeves), String.valueOf(sinistro.getVitimasLeves()));
        }
    }

    public void clicarBtnEditar() {
        scrollToElement(btnEditar);
        clickAndHighlight(btnEditar);
    }
}
