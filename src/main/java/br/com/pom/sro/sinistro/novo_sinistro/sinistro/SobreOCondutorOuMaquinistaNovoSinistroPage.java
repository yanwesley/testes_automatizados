package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.api.model.sinistro.ResponsavelAcionado;
import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class SobreOCondutorOuMaquinistaNovoSinistroPage extends GeralPage {

    Function<Integer, By> selectColaborador = (Integer index) -> By.id("tbEmployee-checkbox-" + index);
    private By separatorSobreOCondutorOuMaquinista = By.name("Sobre o Condutor ou Maquinista");
    private By inputRelato = By.id("relato");
    private By divCondutorOuMaquinista = By.cssSelector("#formSinistro > div.condutor.row.align-items-center > div.col-3.orange-background.flagCondutorRumo");
    private By optionFlagCondutorRumoSim = By.id("flagCondutorRumo-sim");
    private By optionFlagCondutorRumoNao = By.id("flagCondutorRumo-nao");
    private By btnEditar = By.id("btn-editar-condutor");
    private By cellNomeCondutor = By.id("nomeCondutor");
    private By cellMatriculaCondutor = By.id("matriculaCondutor");
    private By inputNomeMatricula = By.id("nomeMatricula");
    private By btnSearchEmployee = By.id("button-search-employee");
    private By inputNomeCondutor = By.id("nomeCondutor");
    private By inputDocumentoCondutor = By.id("matriculaCondutor");

    private By btnSalvarModal = By.id("dialog-button-yes");
    private By btnCancelarModal = By.id("dialog-button-no");

    private By btnEditarSRO = By.id("edit-sobreCondutor");

    public void preencherCampos(Sinistro sinistro) {
        if (sinistro.isCondutorOuMaquinistaDaRumo()) {
            clicarNaOptionSim();
            clicarBtnEditarCondutor();
            buscarPorNomeDoColaborador(sinistro.getCondutorDados());
            selecionarColaborador(0);
            clicarBtnSalvarModal();
        } else {
            clicarNaOptionNao();
            sendKeysWithJavaScript(inputNomeCondutor, sinistro.getNomeCondutor());
            sendKeysWithJavaScript(inputDocumentoCondutor, sinistro.getDocumentoCondutor());
        }
        expectLoading();
        expectElementClickable(inputRelato);
        sendKeysWithJavaScript(inputRelato, sinistro.getRelato());
    }

    public void validaCamposVazios() {
        scrollToElement(separatorSobreOCondutorOuMaquinista);
        Assert.assertEquals(getText(inputRelato), "        ");
    }

    public void clicarNaOptionSim() {
        scrollToElement(separatorSobreOCondutorOuMaquinista);
        clickWithAction(optionFlagCondutorRumoSim);
    }

    public void clicarNaOptionNao() {
        scrollToElement(separatorSobreOCondutorOuMaquinista);
        clickWithAction(optionFlagCondutorRumoNao);
    }

    public void clicarBtnEditarCondutor() {
        clickAndHighlight(btnEditar);
    }

    public void buscarPorNomeDoColaborador(ResponsavelAcionado responsavelAcionado) {
        sendKeys(inputNomeMatricula, responsavelAcionado.getNome());
        clickAndHighlight(btnSearchEmployee);
    }

    public void selecionarColaborador(int index) {
        clickAndHighlight(selectColaborador.apply(index));
    }

    public void clicarBtnSalvarModal() {
        clickAndHighlight(btnSalvarModal);
    }

    public void validaExibicaoDoColaboradorSelecionado(ResponsavelAcionado responsavelAcionado) {
        Assert.assertTrue(true);
    }

    public void validaCamposParaAdicionarColaborador() {
        expectElementVisible(btnEditar);
    }

    public void validaOcultacaoDosCamposParaAdicionarColaborador() {
        expectElementNotVisible(btnEditar);
    }

    public void clicarBtnCancelarModal() {
        clickAndHighlight(btnCancelarModal);
    }

    public void validaCamposPreenchidos(Sinistro sinistro) {
        scrollToElement(separatorSobreOCondutorOuMaquinista);
        if (sinistro.isCondutorOuMaquinistaDaRumo()) {
            Assert.assertTrue(getText(cellNomeCondutor).toLowerCase().startsWith(sinistro.getCondutorDados().getNome().toLowerCase()));
        } else
            Assert.assertEquals(getValue(inputNomeCondutor), sinistro.getNomeCondutor());
        Assert.assertEquals(getValue(inputDocumentoCondutor), sinistro.getDocumentoCondutor());
    }

    public void validaCamposPreenchidosReadOnly(Sinistro sinistro) {
        scrollToElement(separatorSobreOCondutorOuMaquinista);
        if (sinistro.isCondutorOuMaquinistaDaRumo()) {
            Assert.assertTrue(getText(cellNomeCondutor).toLowerCase().startsWith(sinistro.getCondutorDados().getNome().toLowerCase()));
        }
        Assert.assertEquals(getValue(inputRelato), sinistro.getRelato());
    }

    public void clicarBtnEditar() {
        scrollToElement(btnEditarSRO);
        clickAndHighlight(btnEditarSRO);
    }

}
