package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class DadosDoSinistroPage extends GeralPage {


    private By tabDadosDoSinistro = By.id("tab-dados");


    public void clicarNaTabDadosDoSinistro() {
        expectElementClickable(tabDadosDoSinistro);
        clickAndHighlight(tabDadosDoSinistro);
    }
}
