package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class OutrasInformacoesNovoSinistroPage extends GeralPage {

    private By separatorOutrasInformacoes = By.name("Outras Informações");

    private By divIncendioOuExplosaoComDestruicaoTotal = By.cssSelector("#formInformacoes > div > div:nth-child(2) > div");
    private By optionIncendioDestruicaoSim = By.id("destruicaoTotal-sim");
    private By optionIncendioDestruicaoNao = By.id("destruicaoTotal-nao");

    private By optionDanoAmbientalSim = By.id("danoAmbiental-sim");
    private By optionDanoAmbientalSimCircle = By.cssSelector("#danoAmbiental-sim > label > div.mat-radio-container");
    private By danoAmbientalVazCombustivelDaLocomotiva = By.id("danoAmbientalVazComBloco");
    private By checkboxDanoAmbientalVazCombustivelDaLocomotiva = By.cssSelector("#danoAmbientalVazComBloco > label > div");
    private By checkboxDanoAmbientalVazProdRioLago = By.cssSelector("#danoAmbientalVazProdRioLago > label > div");
    private By danoAmbientalVazProdRioLago = By.id("danoAmbientalVazProdRioLago");

    private By optionDanoAmbientalNao = By.id("danoAmbiental-nao");
    private By optionDanoAmbientalNaoCircle = By.cssSelector("#danoAmbiental-nao > label > div.mat-radio-container");

    private By optionImpactoPopulacaoSim = By.id("impactoPopulacao-sim");
    private By optionImpactoPopulacaoSimCircle = By.cssSelector("#impactoPopulacao-sim > label > div.mat-radio-container");
    private By checkboxImpactoPopulacaoPNTrancada1hora = By.cssSelector("#impactoPopulacaoPNTrancada1hora > label > div");
    private By impactoPopulacaoPNTrancada1hora = By.cssSelector("#impactoPopulacaoPNTrancada1hora > label > div");
    private By checkboxImpactoPopulacaoTombamentoQueDanificouEdificacoesDeTerceiros = By.cssSelector("#impactoPopulacaoDanificouEdiTerceiros > label > div");
    private By impactoPopulacaoTombamentoQueDanificouEdificacoesDeTerceiros = By.cssSelector("#impactoPopulacaoDanificouEdiTerceiros > label > div");

    private By optionImpactoPopulacaoNao = By.id("impactoPopulacao-nao");
    private By optionImpactoPopulacaoNaoCircle = By.cssSelector("#impactoPopulacao-nao > label > div.mat-radio-container");

    private By btnEditar = By.id("edit-outrasInformacoes");

    public void preencherCampos(Sinistro sinistro) {
        scrollToElement(separatorOutrasInformacoes);
        if (sinistro.isIncendioOuExplosaoComDestruicaoTotal()) {
            clickWithAction(optionIncendioDestruicaoSim);
        } else {
            clickWithAction(optionIncendioDestruicaoNao);
        }

        if (sinistro.isHouveDanoAmbiental()) {
            clickWithAction(optionDanoAmbientalSimCircle);
            if (sinistro.isVazamentoDeCombustivelDaLocomotiva()) {
                clickWithAction(checkboxDanoAmbientalVazCombustivelDaLocomotiva);
            }
            if (sinistro.isVazamentoDeProdutoEmRiosELagos()) {
                clickWithAction(checkboxDanoAmbientalVazProdRioLago);
            }
        } else {
            clickWithAction(optionDanoAmbientalNaoCircle);
        }

        if (sinistro.isHouveOutrosImpactosAPopulacao()) {
            clickWithAction(optionImpactoPopulacaoSimCircle);
            if (sinistro.isPnTrancadaPorMaisDe1HoraEmPerimetroUrbano()) {
                clickWithAction(checkboxImpactoPopulacaoPNTrancada1hora);
            }

            if (sinistro.isTombamentoQueDanificouEdificacoesDeTerceiros()) {
                clickWithAction(checkboxImpactoPopulacaoTombamentoQueDanificouEdificacoesDeTerceiros);
            }
        } else {
            scrollToElement(optionImpactoPopulacaoNaoCircle);
            clickWithAction(optionImpactoPopulacaoNaoCircle);
        }
    }

    public void validaCamposVazios() {
        scrollToElement(separatorOutrasInformacoes);
    }

    public void validaCamposPreenchidos(Sinistro sinistro) {
        scrollToElement(separatorOutrasInformacoes);
        if (sinistro.isIncendioOuExplosaoComDestruicaoTotal()) {
            Assert.assertTrue(isRadioChecked(optionIncendioDestruicaoSim));
        } else {
            Assert.assertTrue(isRadioChecked(optionIncendioDestruicaoNao));
        }

        if (sinistro.isHouveDanoAmbiental()) {
            Assert.assertTrue(isRadioChecked(optionDanoAmbientalSim));
            if (sinistro.isVazamentoDeCombustivelDaLocomotiva()) {
                Assert.assertTrue(isCheckboxChecked(danoAmbientalVazCombustivelDaLocomotiva));
            } else {
                Assert.assertFalse(isCheckboxChecked(danoAmbientalVazCombustivelDaLocomotiva));
            }

            if (sinistro.isVazamentoDeProdutoEmRiosELagos()) {
                Assert.assertTrue(isCheckboxChecked(danoAmbientalVazProdRioLago));
            } else {
                Assert.assertFalse(isCheckboxChecked(danoAmbientalVazProdRioLago));
            }
        } else {
            Assert.assertTrue(isRadioChecked(optionDanoAmbientalNao));
        }

        if (sinistro.isHouveOutrosImpactosAPopulacao()) {
            Assert.assertTrue(isRadioChecked(optionImpactoPopulacaoSim));

            if (sinistro.isPnTrancadaPorMaisDe1HoraEmPerimetroUrbano()) {
                Assert.assertTrue(isCheckboxChecked(impactoPopulacaoPNTrancada1hora));
            } else {
                Assert.assertFalse(isCheckboxChecked(impactoPopulacaoPNTrancada1hora));
            }
            scrollToElement(optionImpactoPopulacaoSimCircle);
            waitTime();
            if (sinistro.isTombamentoQueDanificouEdificacoesDeTerceiros()) {
                Assert.assertTrue(isCheckboxChecked(impactoPopulacaoTombamentoQueDanificouEdificacoesDeTerceiros));
            } else {
                Assert.assertFalse(isCheckboxChecked(impactoPopulacaoTombamentoQueDanificouEdificacoesDeTerceiros));
            }
        } else {
            Assert.assertTrue(isRadioChecked(optionImpactoPopulacaoNao));
        }
    }

    public void validaCamposPreenchidosReadOnly(Sinistro sinistro) {
        scrollToElement(separatorOutrasInformacoes);
        if (sinistro.isIncendioOuExplosaoComDestruicaoTotal()) {
            Assert.assertTrue(isRadioChecked(optionIncendioDestruicaoSim));
        } else {
            Assert.assertTrue(isRadioChecked(optionIncendioDestruicaoNao));
        }
        if (sinistro.isHouveDanoAmbiental()) {

            Assert.assertTrue(isRadioChecked(optionDanoAmbientalSim));
            if (sinistro.isVazamentoDeCombustivelDaLocomotiva()) {
                Assert.assertTrue(isCheckboxChecked(danoAmbientalVazCombustivelDaLocomotiva));
            } else {
                Assert.assertFalse(isCheckboxChecked(danoAmbientalVazCombustivelDaLocomotiva));
            }

            if (sinistro.isVazamentoDeProdutoEmRiosELagos()) {
                Assert.assertTrue(isCheckboxChecked(danoAmbientalVazProdRioLago));
            } else {
                Assert.assertFalse(isCheckboxChecked(danoAmbientalVazProdRioLago));
            }
        } else {
            Assert.assertTrue(isRadioChecked(optionDanoAmbientalNao));
        }

        if (sinistro.isHouveOutrosImpactosAPopulacao()) {
            Assert.assertTrue(isRadioChecked(optionImpactoPopulacaoSim));

            if (sinistro.isPnTrancadaPorMaisDe1HoraEmPerimetroUrbano()) {
                Assert.assertTrue(isCheckboxChecked(impactoPopulacaoPNTrancada1hora));
            } else {
                Assert.assertFalse(isCheckboxChecked(impactoPopulacaoPNTrancada1hora));
            }
            scrollToElement(optionImpactoPopulacaoSimCircle);
            waitTime();
            if (sinistro.isTombamentoQueDanificouEdificacoesDeTerceiros()) {
                Assert.assertTrue(isCheckboxChecked(impactoPopulacaoTombamentoQueDanificouEdificacoesDeTerceiros));
            } else {
                Assert.assertFalse(isCheckboxChecked(impactoPopulacaoTombamentoQueDanificouEdificacoesDeTerceiros));
            }
        } else {
            Assert.assertTrue(isRadioChecked(optionImpactoPopulacaoNao));
        }
    }

    public void clicarBtnEditar() {
        scrollToElement(btnEditar);
        clickAndHighlight(btnEditar);
    }
}
