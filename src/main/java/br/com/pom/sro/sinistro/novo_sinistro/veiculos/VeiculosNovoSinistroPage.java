package br.com.pom.sro.sinistro.novo_sinistro.veiculos;

import br.com.api.model.sinistro.Rodeiros;
import br.com.api.model.sinistro.RodeirosPorTipo;
import br.com.api.model.sinistro.TipoVeiculo;
import br.com.api.model.sinistro.Veiculo;
import br.com.pom.sro.GeralPage;
import br.com.utils.ReporterUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class VeiculosNovoSinistroPage extends GeralPage {


    Function<Integer, By> tableCellPosicao = (Integer index) -> By.id("table-cell-posicao-" + index);
    Function<Integer, By> tableCellNumero = (Integer index) -> By.id("table-cell-numero-" + index);
    Function<Integer, By> tableCellTipo = (Integer index) -> By.id("table-cell-tipo-" + index);
    Function<Integer, By> tableCellModelo = (Integer index) -> By.id("table-cell-modelo-" + index);
    Function<Integer, By> tableCellMercadoria = (Integer index) -> By.id("table-cell-mercadoria-" + index);
    Function<Integer, By> tableCellPerda = (Integer index) -> By.id("table-cell-perdaProduto-" + index);
    Function<Integer, By> tableCellSituacao = (Integer index) -> By.id("table-cell-situacao-veiculo-" + index);
    Function<Integer, By> tableCellRodeiros = (Integer index) -> By.cssSelector("#table-cell-rodeiros-" + index + " > div > h5");


    Function<Integer, By> checkboxSelect = (Integer index) -> By.id("veiculo-checkbox-" + index);
    Function<Integer, By> inputPosicaoCell = (Integer index) -> By.id("input-table-cell-posicao-" + index);
    Function<Integer, By> inputNumeroCell = (Integer index) -> By.id("input-table-cell-numero-" + index);
    Function<Integer, By> selectTipoCell = (Integer index) -> By.id("select-table-cell-tipo-" + index);

    Function<String, By> optionText = (String texto) -> By.xpath("//span[@class='mat-option-text'][contains(text(),'" + texto + "')]");
    Function<Integer, By> inputModeloCell = (Integer index) -> By.id("input-table-cell-modelo-" + index);
    Function<Integer, By> optionModeloCell = (Integer index) -> By.id("modelo-option-" + index);


    Function<Integer, By> inputMercadoriaCell = (Integer index) -> By.id("input-table-cell-mercadoria-" + index);
    Function<Integer, By> optionMercadoriaCell = (Integer index) -> By.id("mercadoria-option-" + index);

    Function<Integer, By> selectPerdaCell = (Integer index) -> By.id("select-table-cell-perdaProduto-" + index);
    Function<Integer, By> inputPerdaCell = (Integer index) -> By.id("input-table-cell-perdaProduto-" + index);
    Function<String, By> optionPerdaCell = (String perda) -> By.id("perdaProduto-option-" + perda.toLowerCase());

    Function<Integer, By> selectSituacaoCell = (Integer index) -> By.id("select-table-cell-situacao-veiculo-" + index);
    Function<Integer, By> optionSituacaoCell = (Integer index) -> By.id("option-situacao-" + index);
    Function<Integer, By> inputRodeirosCell = (Integer index) -> By.id("input-table-cell-rodeiros-" + index);
    Function<Integer, By> btnEditRodeirosCell = (Integer index) -> By.id("button-table-cell-edit-rodeiros-" + index);
    Function<Integer, By> iconTipoCell = (Integer index) -> By.cssSelector("#table-cell-icone-" + index + " > sro-veiculo-icon > div > img");
    Function<Integer, By> checkboxRodeiros = (Integer index) -> By.id("checkbox-rodeiro-" + index);
    private By btnAdicionarCampoVazio = By.id("button-add-veiculo");
    private By btnFiltrarVeiculosMarcados = By.id("button-show-all");
    private By btnSalvarInformacoes = By.id("button-saved-info");
    private By btnFecharModalRodeiros = By.id("dialog-button-close");

    /*
    Modal rodeiros
     */
    private By table = By.cssSelector("sro-veiculos > div > div:nth-child(2) > table > tbody");
    private By contentModalRodeiros = By.className("mat-dialog-content");
    private By btnSalvarModalRodeiro = By.id("dialog-button-save");

    private By labelQuantidadeDeVeiculosSelecionados = By.cssSelector("#card-selected-vehicles > mat-card-content > strong");

    public void clicarBtnAdicionarCampoVazio() {
        expectLoading();
        expectElementVisibleWithoutHighlight(btnAdicionarCampoVazio);
        expectElementClickable(btnAdicionarCampoVazio);
        clickWithJavaScript(btnAdicionarCampoVazio);
    }

    public int getCountLinesOfTheTable() {
        if (getText(labelQuantidadeDeVeiculosSelecionados).equals("0")) {
            clickWithJavaScript(btnAdicionarCampoVazio);
        }
        return countChildElement(table, "tr");
    }

    public void validarQueFoiIncluidaUmaNovaLinha() {

    }

    public void validarCamposVaziosDaLinha() {
        int index = getCountLinesOfTheTable() - 1;
        Assert.assertEquals(getText(inputPosicaoCell.apply(index)), "");
        Assert.assertEquals(getText(inputNumeroCell.apply(index)), "");
        Assert.assertEquals(getText(selectTipoCell.apply(index)), " ");
        Assert.assertEquals(getText(inputModeloCell.apply(index)), "");
        Assert.assertEquals(getText(inputMercadoriaCell.apply(index)), "");
        Assert.assertEquals(getValue(inputPerdaCell.apply(index)), "--");
        Assert.assertEquals(getText(selectSituacaoCell.apply(index)), " ");
        Assert.assertEquals(getValue(inputRodeirosCell.apply(index)), "0");
    }

    public void validarIconesConformeOTipo(Veiculo veiculo) {
        for (int i = 0; i < veiculo.getTipoVeiculosList().size(); i++) {
            TipoVeiculo tipoVeiculo = veiculo.getTipoVeiculosList().get(i);

            clickAndHighlight(selectTipoCell.apply(0));

            clickAndHighlight(optionText.apply(tipoVeiculo.tipo));

            Assert.assertEquals(getText(selectTipoCell.apply(0)), tipoVeiculo.tipo);
            Assert.assertEquals(getAttribute(iconTipoCell.apply(0), "alt"), tipoVeiculo.icone);
            ReporterUtils.addScreenshotToReport("Validado icone " + tipoVeiculo.tipo + " é :" + tipoVeiculo.icone);
        }
    }

    public void selecionarVeiculos(int quantidade) {
        for (int i = 0; i < getCountLinesOfTheTable(); i++) {
            if (i < quantidade) {
                if (!isCheckboxChecked(checkboxSelect.apply(i))) {
                    clickWithAction(checkboxSelect.apply(i));
                }
            } else {
                if (isCheckboxChecked(checkboxSelect.apply(i))) {
                    clickWithAction(checkboxSelect.apply(i));
                }
            }
        }
    }

    public void clicarBtnFiltrarVeiculosMarcados() {
        clickAndHighlight(btnFiltrarVeiculosMarcados);
    }

    public void selecionarTipo(String tipo) {
        if (getText(labelQuantidadeDeVeiculosSelecionados).equals("0")) {
            clickWithJavaScript(btnAdicionarCampoVazio);
        }
        clickAndHighlight(selectTipoCell.apply(0));
        clickAndHighlight(optionText.apply(tipo));
    }

    public void validarSeCamposMercadoriaEPerdaEstaoReadOnly() {
        Assert.assertTrue(isReadOnly(inputMercadoriaCell.apply(0)));
        Assert.assertTrue(isReadOnly(inputPerdaCell.apply(0)));
    }

    public void validarRodeirosConformeTipoESituacao(String tipo) {
        Rodeiros rodeiros = new Rodeiros();
        for (int i = 0; i < 6; i++) {
            expectElementVisible(selectSituacaoCell.apply(0));
            clickAndHighlight(selectSituacaoCell.apply(0));
            clickAndHighlight(optionSituacaoCell.apply(i));
            String situacao = getText(selectSituacaoCell.apply(0));
            RodeirosPorTipo rodeirosPorTipo = rodeiros.getRodeiros(tipo, situacao);
            Assert.assertEquals(getValue(inputRodeirosCell.apply(0)), rodeirosPorTipo.quantidade.toString());
            Assert.assertEquals(isEnabled(btnEditRodeirosCell.apply(0)), rodeirosPorTipo.editavel);

            ReporterUtils.addScreenshotToReport("Validei que para o tipo:  " + tipo + " com situação :"
                    + situacao + "a quantidade de rodeiros é : " + rodeirosPorTipo.quantidade);

        }
    }

    public void selecionarSituacao(String situacao) {
        clickAndHighlight(selectSituacaoCell.apply(0));
        clickAndHighlight(optionText.apply(situacao));
    }

    public void validarBtnEditarRodeirosEnabled(int linhaVeiculo) {
        Assert.assertTrue(isEnabled(btnEditRodeirosCell.apply(linhaVeiculo - 1)));
    }

    public void clicarBtnEditarRodeiros(int linhaVeiculo) {
        clickAndHighlight(btnEditRodeirosCell.apply(linhaVeiculo - 1));
    }

    public void selecionarRodeiros(int quantidadeRodeiros) {
        for (int i = 0; i < countChildElement(contentModalRodeiros, "mat-checkbox"); i++) {
            if (i < quantidadeRodeiros) {
                if (!isCheckboxChecked(checkboxRodeiros.apply(i))) {
                    clickWithAction(checkboxRodeiros.apply(i));
                }
            } else {
                if (isCheckboxChecked(checkboxRodeiros.apply(i))) {
                    clickWithAction(checkboxRodeiros.apply(i));
                }
            }
        }
    }

    public void clicarBtnSalvarModalRodeiros() {
        clickAndHighlight(btnSalvarModalRodeiro);
    }

    public void validarQuantidadeDeRodeirosSelecionados(int linha, int quantidadeRodeiros) {
        Assert.assertEquals(getValue(inputRodeirosCell.apply(linha - 1)), String.valueOf(quantidadeRodeiros));
    }

    public void preencherCampos(int linha, Veiculo veiculo) {
        if (getText(labelQuantidadeDeVeiculosSelecionados) == "0") {
            clickWithJavaScript(btnAdicionarCampoVazio);
        }
        sendKeys(inputPosicaoCell.apply(linha), veiculo.getPosicao());
        sendKeys(inputNumeroCell.apply(linha), veiculo.getNumero());
        clickAndHighlight(selectTipoCell.apply(linha));
        expectLoading();
        clickAndHighlight(optionText.apply("Vagão"));
        veiculo.setTipoValue(getText(selectTipoCell.apply(linha)));
        sendKeys(inputModeloCell.apply(linha), veiculo.getModelo());
        expectLoading();
        clickAndHighlight(optionModeloCell.apply(veiculo.getModeloIndex()));
        veiculo.setModelo(getValue(inputModeloCell.apply(linha)));
        sendKeys(inputMercadoriaCell.apply(linha), veiculo.getMercadoria());
        expectLoading();
        clickAndHighlight(optionMercadoriaCell.apply(veiculo.getMercadoriaIndex()));
        veiculo.setMercadoria(getValue(inputMercadoriaCell.apply(linha)));
        clickAndHighlight(selectPerdaCell.apply(linha));
        clickAndHighlight(optionPerdaCell.apply(veiculo.getPerdaValue()));
        clickAndHighlight(selectSituacaoCell.apply(linha));
        clickAndHighlight(optionSituacaoCell.apply(veiculo.getSituacaoIndex()));
        veiculo.setSituacaoValue(getText(selectSituacaoCell.apply(linha)));
        if (veiculo.getSituacaoValue().equals("Descarrilado")) {
            clicarBtnEditarRodeiros(linha);
            selecionarRodeiros(2);
            clicarBtnSalvarModalRodeiros();
        }
    }

    public void clicarBtnSalvarInformacoes() {
        scrollToElement(btnSalvarInformacoes);
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void validaVeiculoAdicionado(int linha, Veiculo veiculo) {
        Assert.assertEquals(getValue(inputPosicaoCell.apply(linha)), veiculo.getPosicao().toString());
        Assert.assertEquals(getValue(inputNumeroCell.apply(linha)), veiculo.getNumero());
        Assert.assertEquals(getText(selectTipoCell.apply(linha)), veiculo.getTipoValue());
        Assert.assertEquals(getValue(inputModeloCell.apply(linha)), veiculo.getModelo());
        Assert.assertEquals(getValue(inputMercadoriaCell.apply(linha)), veiculo.getMercadoria());
        Assert.assertEquals(getText(selectPerdaCell.apply(linha)), veiculo.getPerdaValue());
        Assert.assertEquals(getText(selectSituacaoCell.apply(linha)), veiculo.getSituacaoValue());
        Assert.assertEquals(getValue(inputRodeirosCell.apply(linha)), String.valueOf(veiculo.getQtdRodeiros()));
    }

    public void validaVeiculoAdicionadoReadOnly(int linha, Veiculo veiculo) {
        Assert.assertEquals(getText(tableCellPosicao.apply(linha)), veiculo.getPosicao().toString());
        Assert.assertEquals(getText(tableCellNumero.apply(linha)), veiculo.getNumero());
        Assert.assertEquals(getText(tableCellTipo.apply(linha)), veiculo.getTipoValue());
        Assert.assertEquals(getText(tableCellModelo.apply(linha)), veiculo.getModelo());
        Assert.assertEquals(getText(tableCellMercadoria.apply(linha)).toLowerCase(), veiculo.getMercadoria().toLowerCase());
        Assert.assertEquals(getText(tableCellPerda.apply(linha)), veiculo.getPerdaValue());
        Assert.assertEquals(getText(tableCellSituacao.apply(linha)), veiculo.getSituacaoValue());
        Assert.assertEquals(getText(tableCellRodeiros.apply(linha)), String.valueOf(veiculo.getQtdRodeiros()));
    }

    public void validarQuantidadeDeRodeirosSelecionadosReadOnly(int linha, Veiculo veiculo) {
        if (veiculo.getQtdRodeiros() != null && veiculo.getQtdRodeiros() > 0) {
            Assert.assertEquals(getText(tableCellRodeiros.apply(linha - 1)), String.valueOf(veiculo.getQtdRodeiros()));
            clickAndHighlight(btnEditRodeirosCell.apply(linha - 1));
            Assert.assertEquals(String.valueOf(countChildElement(contentModalRodeiros, "p")), veiculo.getQtdRodeiros().toString());
            clicarBtnFecharModalRodeiros();
        } else {
            Assert.assertEquals(getText(tableCellRodeiros.apply(linha - 1)), String.valueOf(veiculo.getQtdRodeiros()));
        }
    }

    public void clicarBtnFecharModalRodeiros() {
        clickAndHighlight(btnFecharModalRodeiros);
    }
}
