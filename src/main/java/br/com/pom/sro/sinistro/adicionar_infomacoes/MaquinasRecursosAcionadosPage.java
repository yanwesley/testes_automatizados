package br.com.pom.sro.sinistro.adicionar_infomacoes;

import br.com.api.model.sinistro.MaquinaAcionada;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class MaquinasRecursosAcionadosPage extends GeralPage {

    Function<Integer, By> cellBtnEditar = (Integer index) -> By.id("button-edit-material-resource-" + index);
    Function<Integer, By> cellBtnExcluir = (Integer index) -> By.id("button-remove-material-resource-" + index);

    Function<Integer, By> cellRecurso_Tamanho = (Integer index) -> By.id("table-recurso-material-cell-recurso_tamanho-" + index);
    Function<Integer, By> cellOrigem = (Integer index) -> By.id("table-recurso-material-cell-origem-" + index);
    Function<Integer, By> cellHoraPrevista_HoraLocal = (Integer index) -> By.id("table-recurso-material-cell-horaPrevista_horaLocal-" + index);
    Function<Integer, By> cellEmpresa = (Integer index) -> By.id("table-recurso-material-cell-empresa-" + index);
    Function<Integer, By> cellResponsavel_Telefone = (Integer index) -> By.id("table-recurso-material-cell-responsavel_telefone-" + index);
    Function<Integer, By> cellObservacao = (Integer index) -> By.id("table-recurso-material-cell-observacao-" + index);

    private By secaoRecursosMaquinas = By.name("Recursos acionados - máquinas");
    //    private By radioSim = By.id("hasMaterialResource-sim");
    private By radioSim = By.cssSelector("#hasMaterialResource-sim > label");
    private By btnAdicionarMaquina = By.id("button-add-material-resource");
    private By lblAcionamentoDeMaquina = By.id("dialog-header");

    private By inputRecursoModal = By.id("input-recurso");
    private By inputTamanhoModal = By.id("input-tamanho");
    private By inputOrigemModal = By.id("input-origem");

    private By inputHoraPrevistaModal = By.id("input-horaPrevista");
    private By inputHoraLocalModal = By.id("input-horaLocal");
    private By inputEmpresaModal = By.id("input-empresa");
    private By inputResponsavelModal = By.id("input-responsavel");
    private By inputTelefoneModal = By.id("input-telefone");
    private By inputObservacaoModal = By.id("input-observacao");

    private By btnConfirmarExclusao = By.id("dialog-button-yes");

    public void validarSecaoRecursosAcionadosMaquinas() {
        scrollToElement(secaoRecursosMaquinas);
        expectElementVisible(radioSim);
    }

    public void selecionarRadioButtonMaquinasForamAcionadasSim() {
        waitTime();
        scrollToElement(secaoRecursosMaquinas);
        clickWithAction(radioSim);
    }

    public void clicarBtnAdicionarMaquina() {
        clickWithAction(btnAdicionarMaquina);
    }

    public void validarModalAcionamentoDeMaquina() {
        expectElementVisible(lblAcionamentoDeMaquina);
        expectText(lblAcionamentoDeMaquina, "ACIONAMENTO DE MÁQUINA");
    }

    public void preencherModalAcionamentoDeMaquina(MaquinaAcionada maquinaAcionada) {
        sendKeys(inputRecursoModal, maquinaAcionada.getRecurso());
        sendKeys(inputTamanhoModal, maquinaAcionada.getTamanho());
        sendKeys(inputOrigemModal, maquinaAcionada.getOrigem());
        sendKeys(inputHoraPrevistaModal, maquinaAcionada.getHoraPrevista());
        sendKeys(inputHoraLocalModal, maquinaAcionada.getHoraLocal());
        sendKeys(inputEmpresaModal, maquinaAcionada.getEmpresa());
        sendKeys(inputResponsavelModal, maquinaAcionada.getResponsavel());
        sendKeys(inputTelefoneModal, maquinaAcionada.getTelefoneDoResponsavel());
        sendKeysWithJavaScript(inputObservacaoModal, maquinaAcionada.getObservacao());
    }

    public void validaQueAMaquinaFoiAdicionada(MaquinaAcionada maquinaAcionada) {
        String recursoTamanho = getText(cellRecurso_Tamanho.apply(0));
        Assert.assertTrue(recursoTamanho.contains(maquinaAcionada.getRecurso())
                && recursoTamanho.contains(String.valueOf(maquinaAcionada.getTamanho()))
        );

        Assert.assertEquals(getText(cellOrigem.apply(0)), maquinaAcionada.getOrigem());

        String horaPrevista_Local = getText(cellHoraPrevista_HoraLocal.apply(0));
        Assert.assertTrue(horaPrevista_Local.contains(maquinaAcionada.getHoraLocal())
                && horaPrevista_Local.contains(maquinaAcionada.getHoraPrevista())
        );

        Assert.assertEquals(getText(cellEmpresa.apply(0)), maquinaAcionada.getEmpresa());

        String responsavel_telefone = getText(cellResponsavel_Telefone.apply(0));
        Assert.assertTrue(responsavel_telefone.contains(maquinaAcionada.getResponsavel())
                && responsavel_telefone.contains(maquinaAcionada.getTelefoneDoResponsavel())
        );

        Assert.assertEquals(getText(cellObservacao.apply(0)), maquinaAcionada.getObservacao());
    }

    public void validaSeExisteMaquina() {
        expectElementVisible(cellRecurso_Tamanho.apply(0));
    }

    public void clicarBtnEditarMaquina(int indexMaquina) {
        clickAndHighlight(cellBtnEditar.apply(indexMaquina - 1));
    }

    public void clicarBtnExcluirMaquina(int indexMaquina) {
        clickAndHighlight(cellBtnExcluir.apply(indexMaquina - 1));
    }

    public void confirmarExclusaoDaMaquina() {
        clickAndHighlight(btnConfirmarExclusao);
    }

    public void validaSeAMaquinaFoiExcluida() {
        expectElementNotVisible(cellBtnExcluir.apply(0));
    }

    public void validaQueAMaquinaFoiAdicionadaReadOnly(MaquinaAcionada maquinaAcionada) {
        String recursoTamanho = getText(cellRecurso_Tamanho.apply(0));
        Assert.assertTrue(recursoTamanho.contains(maquinaAcionada.getRecurso())
                && recursoTamanho.contains(String.valueOf(maquinaAcionada.getTamanho()))
        );

        Assert.assertEquals(getText(cellOrigem.apply(0)), maquinaAcionada.getOrigem());

        String horaPrevista_Local = getText(cellHoraPrevista_HoraLocal.apply(0));
        Assert.assertTrue(horaPrevista_Local.contains(maquinaAcionada.getHoraLocal())
                && horaPrevista_Local.contains(maquinaAcionada.getHoraPrevista())
        );

        Assert.assertEquals(getText(cellEmpresa.apply(0)), maquinaAcionada.getEmpresa());

        String responsavel_telefone = getText(cellResponsavel_Telefone.apply(0));
        Assert.assertTrue(responsavel_telefone.contains(maquinaAcionada.getResponsavel())
                && responsavel_telefone.contains(maquinaAcionada.getTelefoneDoResponsavel())
        );

        Assert.assertEquals(getText(cellObservacao.apply(0)), maquinaAcionada.getObservacao());

    }
}
