package br.com.pom.sro.sinistro.novo_sinistro.liberacao;

import br.com.api.model.sinistro.InformarANTT;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

public class InformarANTTNovoSinistroPage extends GeralPage {

    Function<Integer, By> optionNatureza = (Integer index) -> By.id("option-natureza-" + index);
    Function<Integer, By> optionMotivo = (Integer index) -> By.id("option-motivo-" + index);
    Function<Integer, By> optionCausaProvavel = (Integer index) -> By.id("option-causa-" + index);
    private By formEmailAntt = By.id("formEmailAntt");
    private By radioButtonRMP = By.id("ferrovia-RMP");
    private By radioButtonRMN = By.id("ferrovia-RMN");
    private By radioButtonRMO = By.id("ferrovia-RMO");
    private By radioButtonRMS = By.id("ferrovia-RMS");
    private By radioButtonRMC = By.id("ferrovia-RMC");
    private By radioButtonAcidentePNSim = By.cssSelector("#acidentePn-sim > label > div.mat-radio-container > div.mat-radio-outer-circle");
    private By radioButtonAcidentePNNao = By.cssSelector("#acidentePn-nao > label > div.mat-radio-container > div.mat-radio-outer-circle");
    private By radioButtonPerimetroUrbSim = By.cssSelector("#perimetroUrbano-sim > label > div.mat-radio-container > div.mat-radio-inner-circle");
    private By radioButtonPerimetroUrbNao = By.cssSelector("#perimetroUrbano-nao > label > div.mat-radio-container > div.mat-radio-outer-circle");
    private By txtAreaTrecho = By.id("trecho");
    private By selectNatureza = By.id("natureza");
    private By selectMotivo = By.id("motivo");
    private By selectCausaProvavel = By.id("causa");
    private By txtAreaRelato = By.id("relato");

    private By txtInstrucao = By.className("texto-de-instru");

    private By btnCancelar = By.id("dialog-button-cancel");
    private By btnConfirmarEnvio = By.id("dialog-button-save");

    private By btnSobreescreverSim = By.id("dialog-button-yes");
    private By btnSobreescreverNao = By.id("dialog-button-no");

    private By txtEmailEnviadoANTT = By.id("conteudoEmail");
    private By dtEmailEnviadoModal = By.id("dataEnviado");

    private By btnFecharModalEmailEnviado = By.id("dialog-button-fechar");

    private By msgAvisoParaEnvioDoEmailANTT = By.id("message-email-antt-warning");

    private By msgEmailANTTEnviado = By.id("message-email-antt-sent");
    private By btnVisualizarEmailANTT = By.id("visualizarEmailAntt");
    private By textEnviadoPor = By.id("enviadoPor");

    private String dataEnvioEmailANTT;
    private String enviadoPor = "00000001 - Usuário DEV";

    public void validarModalParaEnvioDeInformacaoANTT() {
        expectElementVisible(formEmailAntt);
    }

    public void validarCamposDaModalEnviarEmailANTT() {
        expectElementVisible(radioButtonRMP);
        expectElementVisible(radioButtonRMN);
        expectElementVisible(radioButtonRMO);
        expectElementVisible(radioButtonRMS);
        expectElementVisible(radioButtonRMC);
        expectElementVisible(radioButtonAcidentePNSim);
        expectElementVisible(radioButtonAcidentePNNao);
        expectElementVisible(radioButtonPerimetroUrbSim);
        expectElementVisible(radioButtonPerimetroUrbNao);
        expectElementVisible(txtAreaTrecho);
        expectElementVisible(selectNatureza);
        expectElementVisible(selectMotivo);
        expectElementVisible(selectCausaProvavel);
        expectElementVisible(txtAreaRelato);
        expectElementVisible(txtInstrucao);
        expectElementVisible(btnCancelar);
        expectElementVisible(btnConfirmarEnvio);
    }

    public void validarMotivoDesabilitado() {
        Assert.assertTrue(isAriaDisabled(selectMotivo));
    }

    public void selecionaCampoNatureza() {
        clickAndHighlight(selectNatureza);
        clickAndHighlight(optionNatureza.apply(0));
    }

    public void validarMotivoHabilitado() {

        Assert.assertFalse(isAriaDisabled(selectMotivo));
    }

    public void preencheFerrovia(InformarANTT informarANTT) {
        clickAndHighlight(radioButtonRMN);
        informarANTT.setFerrovia("RMN");
    }

    public void preencheAcidentePN(InformarANTT informarANTT) {
        clickWithAction(radioButtonAcidentePNNao);
        informarANTT.setAcidentePN("Não");
    }

    public void preenchePerimetroUrbano(InformarANTT informarANTT) {
        clickWithAction(radioButtonPerimetroUrbSim);
        informarANTT.setPerimetroUrb("Sim");
    }

    public void preencheTrecho(InformarANTT informarANTT) {
        sendKeys(txtAreaTrecho, informarANTT.getTrecho());
    }

    public void preencheNatureza(InformarANTT informarANTT) {
        clickWithAction(selectNatureza);
        clickWithAction(optionNatureza.apply(0));
        informarANTT.setNatureza(getText(selectNatureza));
    }

    public void preencheMotivo(InformarANTT informarANTT) {
        clickWithAction(selectMotivo);
        expectElementVisible(optionMotivo.apply(0));
        clickWithAction(optionMotivo.apply(0));
        clickWithAction(btnSobreescreverSim);
        informarANTT.setMotivo(getText(selectMotivo));
    }

    public void preencheCausaProvavel(InformarANTT informarANTT) {
        clickWithAction(selectCausaProvavel);
        clickWithAction(optionCausaProvavel.apply(0));
        informarANTT.setCausaProvavel(getText(selectCausaProvavel));
    }

    public void validaRelato(InformarANTT informarANTT) {
        expectLoading();
        expectElementClickable(selectNatureza);
        waitTime(1000);
        String txtRelato = getValue(txtAreaRelato).toLowerCase();
        Assert.assertTrue(txtRelato.contains(getText(selectNatureza).toLowerCase()));
        informarANTT.setRelato(txtRelato);
    }

    public void alteraNatureza(InformarANTT informarANTT) {
        clickWithAction(selectNatureza);
        clickWithAction(optionNatureza.apply(1));
        clickWithAction(btnSobreescreverSim);
        informarANTT.setNatureza(getText(selectNatureza));
    }

    public void clicaConfirmarEnvio() {
        expectElementClickable(btnConfirmarEnvio);
        clickAndHighlight(btnConfirmarEnvio);
        dataEnvioEmailANTT = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public void clicaCancelarEnvio() {
        clickAndHighlight(btnCancelar);
    }

    public void validaEmailEnviadoANTT(InformarANTT informarANTT) {
        expectElementVisible(txtEmailEnviadoANTT);
        waitTime(1000);
        Assert.assertTrue(getText(txtEmailEnviadoANTT).contains(informarANTT.getNatureza()));
        Assert.assertTrue(getText(dtEmailEnviadoModal).contains(dataEnvioEmailANTT));
        Assert.assertTrue(getText(textEnviadoPor).contains(enviadoPor));
    }

    public void clicarBtnFecharModalEmailEnviado() {
        clickAndHighlight(btnFecharModalEmailEnviado);
    }

    public void validaMsgDeEmailANTTEnviado() {
        expectElementVisible(msgEmailANTTEnviado);
        Assert.assertTrue(getText(msgEmailANTTEnviado).contains("Email enviado para a ANTT no dia"));
        Assert.assertTrue(getText(msgEmailANTTEnviado).contains(dataEnvioEmailANTT));
    }

    public void clicarBtnVisualizarEmail() {
        clickAndHighlight(btnVisualizarEmailANTT);
    }

    public void validarMsgDeAvisoParaOEnvioDeEmailANTT() {
        expectElementVisible(msgAvisoParaEnvioDoEmailANTT);
        Assert.assertEquals(getText(msgAvisoParaEnvioDoEmailANTT), "warning\n" +
                "Um email deve ser enviado a ANTT no prazo máximo de 2 horas após o sinistro.");
    }

    public void validaQueMotivoNaoEstaPreenchido() {
        Assert.assertEquals(getText(selectMotivo), "Escolha abaixo");
    }
}
