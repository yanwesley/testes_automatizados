package br.com.pom.sro.sinistro.novo_sinistro.tipo_entrada;

import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class EntradaMacroEOsPage extends GeralPage {

    Function<Integer, By> cellNumeroDaMacro = (Integer index) -> By.id("tbMacro-idMacro-" + index);
    Function<Integer, By> cellCheckBoxMacro = (Integer index) -> By.id("tb-checkbox-" + index);
    Function<Integer, By> cellNumeroOs = (Integer index) -> By.id("tb-codigo-" + index);
    Function<Integer, By> cellPrefixo = (Integer index) -> By.id("tb-prefixoTrem-" + index);
    Function<Integer, By> cellDataHora = (Integer index) -> By.id("tb-dataLiberacao-" + index);

    private By btnAlterarEntradaDeDados = By.id("btn-alterar-entrada");
    private By inputNumeroDaMacro = By.id("searchMacro");
    private By btnSelecionar = By.id("selecionarMacrosOS");
    private By btnCancelar = By.id("cancelarSelecaoMacroOS");
    private By labelTipoDeEntrada = By.cssSelector("body > sro-root > sro-shell > div > sro-select-macro > div.row.header-macro > div > div:nth-child(2) > mat-card > span.font-bold");
    private By inputDataAcidente = By.id("dataAcidente");
    private By inputHoraAcidente = By.id("horaAcidente");
    private By inputSearchMacro = By.id("searchMacro");
    private By buttonButtonSearchMacro = By.id("button-searchMacro");

    public void validaQueAListaDeMacroEApresentada() {
        expectElementVisible(cellNumeroDaMacro.apply(0));
    }

    public void clicarNoBtnAlterarEntradaDeDados() {
        clickAndHighlight(btnAlterarEntradaDeDados);
    }

    public void clicarNoBtnCancelar() {
        waitTime();
        clickAndHighlight(btnCancelar);
    }

    public void validarBtnSelecionarDesabilitado() {
        expectElementDisable(btnSelecionar);
    }

    public void selecionarMacro(int linha) {
        clickAndHighlight(cellCheckBoxMacro.apply(linha - 1));
    }

    public void validarBtnSelecionarHabilitado() {
        expectElementClickable(btnSelecionar);
    }

    public void clicarNoBtnSelecionar() {
        clickAndHighlight(btnSelecionar);
    }

    public void validaQueEstaNaTelaDeMacros() {
        Assert.assertEquals(getText(labelTipoDeEntrada), "número da macro");
    }

    public void validaQueEstaNaTelaDeOs() {
        Assert.assertEquals(getText(labelTipoDeEntrada), "número da OS ou prefixo");
    }

    public void validaQueALinhaNaoEstaSelecionada(int linha) {
        Assert.assertFalse(getAttribute(cellCheckBoxMacro.apply(linha - 1), "class").contains("mat-checkbox-checked"));
    }

    public void validarSeAsLinhasSelecionadasSaoDoMesmoPrefixo() {

    }

    public void selecionarOSSetandoOsDados(int linha, Sinistro sinistro) {
        clickAndHighlight(cellCheckBoxMacro.apply(linha - 1));
        sinistro.setNumeroOS(getText(cellNumeroOs.apply(linha - 1)));
        sinistro.setPrefixo(getText(cellPrefixo.apply(linha - 1)));
    }

    public void preencherCamposDataEHoraOS(Sinistro sinistro) {
        clearForce(inputDataAcidente);
        sendKeys(inputDataAcidente, sinistro.getDateOS());
        sendKeys(inputHoraAcidente, sinistro.getHoraOS());
    }
}