package br.com.pom.sro.sinistro.adicionar_infomacoes;

import br.com.api.model.sinistro.TurmaAcionada;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class TurmasRecursosAcionadosPage extends GeralPage {

    Function<Integer, By> cellBtnEditar = (Integer index) -> By.id("button-edit-cre-resource-" + index);
    Function<Integer, By> cellBtnExcluir = (Integer index) -> By.id("button-remove-cre-resource-" + index);

    Function<Integer, By> cellRecurso = (Integer index) -> By.id("table-recurso-turmas-cell-recurso-" + index);
    Function<Integer, By> cellQuantidade = (Integer index) -> By.id("table-recurso-turmas-cell-quantidade-" + index);
    Function<Integer, By> cellOrigem = (Integer index) -> By.id("table-recurso-turmas-cell-origem-" + index);
    Function<Integer, By> cellHoraPrevista_HoraLocal = (Integer index) -> By.id("table-recurso-turmas-cell-horaPrevista_horaLocal-" + index);
    Function<Integer, By> cellLadoEntrada = (Integer index) -> By.id("table-recurso-turmas-cell-lado_entrada-" + index);
    Function<Integer, By> cellEmpresa_Responsavel_Telefone = (Integer index) -> By.id("table-recurso-turmas-cell-empresa_responsaveltelefone-" + index);
    Function<Integer, By> cellObservacao = (Integer index) -> By.id("table-recurso-turmas-cell-observacao-" + index);

    private By secaoRecursosTurmas = By.name("Recursos acionados - turmas");
    //    private By radioSim = By.id("hasCrewResources-sim");
    private By radioSim = By.cssSelector("#hasCrewResources-sim > label");
    private By btnAdicionarTurma = By.id("button-add-crew-resource");
    private By lblAcionamentoDeTurmas = By.id("dialog-header");

    private By inputRecursoModal = By.id("input-recurso");
    private By inputQuantidadeModal = By.id("input-quantidade");
    private By inputOrigemModal = By.id("input-origem");
    private By inputHoraPrevistaModal = By.id("input-horaPrevista");
    private By inputHoraLocalModal = By.id("input-horaLocal");
    private By inputLadoEntradaModal = By.id("input-lado-entrada");
    private By inputEmpresaModal = By.id("input-empresa");
    private By inputResponsavelModal = By.id("input-responsavel");
    private By inputTelefoneModal = By.id("input-telefone");
    private By inputObservacaoModal = By.id("input-observacao");

    private By btnConfirmarExclusao = By.id("dialog-button-yes");

    public void validarModalAcionamentoDeTurma() {
        expectText(lblAcionamentoDeTurmas, "ACIONAMENTO DE TURMAS");
    }

    public void validaSeATurmaFoiExcluida() {
        expectElementNotVisible(cellRecurso.apply(0));
    }

    public void validaQueATurmaFoiAdicionada(TurmaAcionada turmaAcionada) {
        Assert.assertEquals(getText(cellRecurso.apply(0)), turmaAcionada.getRecurso());
        Assert.assertEquals(getText(cellQuantidade.apply(0)), String.valueOf(turmaAcionada.getQuantidade()));
        Assert.assertEquals(getText(cellOrigem.apply(0)), turmaAcionada.getOrigem());

        String horaPrevista_Local = getText(cellHoraPrevista_HoraLocal.apply(0));
        Assert.assertTrue(horaPrevista_Local.contains(turmaAcionada.getHoraLocal())
                && horaPrevista_Local.contains(turmaAcionada.getHoraPrevista())
        );

        Assert.assertEquals(getText(cellLadoEntrada.apply(0)), turmaAcionada.getLadoEntrada());

        String empresa_responsavel_telefone = getText(cellEmpresa_Responsavel_Telefone.apply(0));
        Assert.assertTrue(

                empresa_responsavel_telefone.contains(turmaAcionada.getEmpresa()) &&
                        empresa_responsavel_telefone.contains(turmaAcionada.getResponsavel()) &&
                        empresa_responsavel_telefone.contains(turmaAcionada.getTelefoneDoResponsavel())
        );

        Assert.assertEquals(getText(cellObservacao.apply(0)), turmaAcionada.getObservacao());

    }


    public void preencherModalAcionamentoDeTurmas(TurmaAcionada turmaAcionada) {
        sendKeys(inputRecursoModal, turmaAcionada.getRecurso());
        sendKeys(inputQuantidadeModal, turmaAcionada.getQuantidade());
        sendKeys(inputOrigemModal, turmaAcionada.getOrigem());
        sendKeys(inputHoraPrevistaModal, turmaAcionada.getHoraPrevista());
        sendKeys(inputHoraLocalModal, turmaAcionada.getHoraLocal());
        sendKeys(inputLadoEntradaModal, turmaAcionada.getLadoEntrada());
        sendKeys(inputEmpresaModal, turmaAcionada.getEmpresa());
        sendKeys(inputResponsavelModal, turmaAcionada.getResponsavel());
        sendKeys(inputTelefoneModal, turmaAcionada.getTelefoneDoResponsavel());
        sendKeysWithJavaScript(inputObservacaoModal, turmaAcionada.getObservacao());
    }

    public void confirmarExclusaoDaTurma() {
        clickAndHighlight(btnConfirmarExclusao);
    }

    public void validaSeExisteTurma() {
        expectElementVisible(cellRecurso.apply(0));
    }

    public void clicarBtnEditarTurma(int indexLinha) {
        clickAndHighlight(cellBtnEditar.apply(indexLinha - 1));
    }

    public void clicarBtnExcluirTurma(int indexLinha) {
        clickAndHighlight(cellBtnExcluir.apply(indexLinha - 1));
    }

    public void clicarBtnAdicionarTurma() {
        scrollToElement(secaoRecursosTurmas);
        clickAndHighlight(btnAdicionarTurma);
    }

    public void selecionarRadioButtonTurmasForamAcionadasSim() {
        waitTime();
        scrollToElement(secaoRecursosTurmas);
        clickWithAction(radioSim);
    }

    public void validarSecaoRecursosAcionadosTurmas() {
        scrollToElement(secaoRecursosTurmas);
        expectElementVisible(secaoRecursosTurmas);
    }

    public void validaQueATurmaFoiAdicionadaReadOnly(TurmaAcionada turmaAcionada) {
        scrollToElement(secaoRecursosTurmas);
        Assert.assertEquals(getText(cellRecurso.apply(0)), turmaAcionada.getRecurso());
        Assert.assertEquals(getText(cellQuantidade.apply(0)), String.valueOf(turmaAcionada.getQuantidade()));
        Assert.assertEquals(getText(cellOrigem.apply(0)), turmaAcionada.getOrigem());

        String horaPrevista_Local = getText(cellHoraPrevista_HoraLocal.apply(0));
        Assert.assertTrue(horaPrevista_Local.contains(turmaAcionada.getHoraLocal())
                && horaPrevista_Local.contains(turmaAcionada.getHoraPrevista())
        );

        Assert.assertEquals(getText(cellLadoEntrada.apply(0)), turmaAcionada.getLadoEntrada());

        String empresa_responsavel_telefone = getText(cellEmpresa_Responsavel_Telefone.apply(0));
        Assert.assertTrue(

                empresa_responsavel_telefone.contains(turmaAcionada.getEmpresa()) &&
                        empresa_responsavel_telefone.contains(turmaAcionada.getResponsavel()) &&
                        empresa_responsavel_telefone.contains(turmaAcionada.getTelefoneDoResponsavel())
        );

        Assert.assertEquals(getText(cellObservacao.apply(0)), turmaAcionada.getObservacao());

    }
}
