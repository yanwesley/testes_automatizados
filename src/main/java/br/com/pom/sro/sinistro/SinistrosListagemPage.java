package br.com.pom.sro.sinistro;

import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.io.File;
import java.util.function.Function;

public class SinistrosListagemPage extends GeralPage {

    Function<Integer, By> labelCellDataSinistro = (Integer index) -> By.id("table-cell-dataHoraAcidente-" + index);
    Function<Integer, By> labelCellOrigemSinistro = (Integer index) -> By.id("table-cell-origem-" + index);
    Function<Integer, By> labelCellPrefixoTremSinistro = (Integer index) -> By.id("table-cell-prefixoTrem-" + index);
    Function<Integer, By> labelCellTipoSinistro = (Integer index) -> By.id("table-cell-tipoAcidente-" + index);
    Function<Integer, By> labelCellSubdivisaoSinistro = (Integer index) -> By.id("table-cell-subDivisao-" + index);
    Function<Integer, By> labelCellSecaoBloqueioSBSinistro = (Integer index) -> By.id("table-cell-secaoBloqueio-" + index);
    Function<Integer, By> labelCellEmailANTTEnviadoSinistro = (Integer index) -> By.id("table-cell-flagEmailEnviadoANTT-" + index);
    Function<Integer, By> labelCellInterdicaoCirculacaoSinistro = (Integer index) -> By.id("table-cell-interdicaoCirculacao-" + index);
    Function<Integer, By> labelCellStatusSinistro = (Integer index) -> By.id("table-cell-status-" + index);
    Function<Integer, By> btnCellEdit = (Integer index) -> By.id("table-cell-edit-" + index);
    Function<Integer, By> cellSinistroAceito = (Integer index) -> By.id("table-cell-aceito-" + index);

    private By labelTitulo = By.tagName("sro-default-title");
    private By inputSearchSinistros = By.id("search-sinistro");
    private By btnExportar = By.id("button-export");
    private By btnFilter = By.id("button-filter");
    private By tabSinistrosAbertos = By.id("tab-abertos");
    private By tabSinistrosEncerrados = By.id("tab-encerrados");
    /*
    Header tabela abertos
     */
    @Deprecated
    private By tabSinistrosRecusados = By.id("tab-recusados");
    private By btnIncluirNovoSinistro = By.id("button-add-sinistro");
    private By headerId = By.id("table-header-idSinistro");
    private By headerDataHoraSinistro = By.id("table-header-dataHoraAcidente");
    private By headerDataHoraRecusa = By.id("table-header-dataHoraRecusa");
    private By headerOrigem = By.id("table-header-origem");
    private By headerPrefixo = By.id("table-header-prefixoTrem");
    private By headerTipo = By.id("table-header-tipoAcidente");
    private By headerSubdivisao = By.id("table-header-subDivisao");
    private By headerSecaoBloqueioSB = By.id("table-header-secaoBloqueio");
    private By headerCirculacaoInterditada = By.id("table-header-interdicaoCirculacao");
    private By headerMotivoRecusa = By.id("table-header-motivoRecusaSinistro");
    private By headerResponsavelRecusa = By.id("table-header-responsavelRecusaSinistro");
    private By headerResponsavel = By.id("table-header-usuarioUltimaAlteracao");
    private By headerGravidadeDoAcidente = By.id("table-header-gravidade");
    private By headerEmailEnviadoANTT = By.id("table-header-flagEmailEnviadoANTT");
    private By headerDataHoraLiberacaoFinal = By.id("table-header-dataHoraLiberacaoFinal");
    private By headerStatus = By.id("table-header-status");
    private By headerEdit = By.id("table-header-edit");
    private By headerShow = By.id("table-header-show");
    private By labelCellSecaoBloqueio0 = By.id("table-cell-secaoBloqueio-0");
    /*
    Legendas
     */
    private By legendaTempoViaEstaInterditada = By.xpath("//*[contains(text(), 'Tempo que a via está interditada')]");
    private By legendaTempoQueFicouInterditada = By.xpath("//*[contains(text(), 'Tempo que a via ficou interditada')]");

    /*
    mensagem sem resultado
     */

    private By msgNenhumResultadoEncontrado = By.className("empty-label");

    /*
    Campos do filtro de data
     */

    private By inputDataInicio = By.id("input-advanced-filter-date-since");
    private By inputDataFim = By.id("input-advanced-filter-date-until");
    private By btnAplicarFiltro = By.id("button-advanced-filter-apply");


    private By btnRemoverFiltro = By.id("button-chip-clear-filter");


    public void selecionarTABSinistrosAbertos() {
        clickAndHighlight(tabSinistrosAbertos);
    }

    @Deprecated
    public void selecionarTABSinistrosRecusados() {
        clickAndHighlight(tabSinistrosRecusados);
    }

    public void selecionarTABSinistrosEncerrados() {
        clickAndHighlight(tabSinistrosEncerrados);
    }

    public void validaCamposParaPerfilSRO() {
        expectElementVisible(tabSinistrosAbertos);
        expectElementVisible(tabSinistrosEncerrados);
        expectElementVisible(inputSearchSinistros);
        expectElementVisible(btnExportar);
        expectElementVisible(btnFilter);
        expectElementNotVisible(btnIncluirNovoSinistro);
    }

    public void filtrarPorTexto(String textoBusca) {
        clearForce(inputSearchSinistros);
        sendKeys(inputSearchSinistros, textoBusca);
    }

    public void validaResultadoDaBuscaPorTexto(String textoBusca) {
        Assert.assertTrue(expectText(labelCellSecaoBloqueio0, textoBusca), "Texto deve ser :" + textoBusca);
    }

    public void validaExibicaoDaMensagemSemResultadoDaBusca(String msgSemResultado) {
        Assert.assertEquals(getText(msgNenhumResultadoEncontrado), msgSemResultado);
    }

    public void filtrarPorDataInicial(String dataInicial) {
        expectElementVisibleWithoutHighlight(inputDataInicio);
        clearForce(inputDataInicio);
        sendKeys(inputDataInicio, dataInicial);
    }

    public void validaResultadoDaBuscaPorDataInicial(String dataInicial) {
        expectLoading();
        Assert.assertTrue(
                expectText(labelCellDataSinistro.apply(0), dataInicial),
                dataInicial + "not equal " + getText(labelCellDataSinistro.apply(0)));
    }

    public void filtrarPorDataFinal(String dataFinal) {
        expectElementVisibleWithoutHighlight(inputDataFim);
        clearForce(inputDataFim);
        sendKeys(inputDataFim, dataFinal);
    }

    public void validaResultadoDaBuscaPorDataFinal(String dataFinal) {
        expectLoading();
        clickAndHighlight(headerDataHoraSinistro);
        Assert.assertTrue(
                expectText(labelCellDataSinistro.apply(0), dataFinal),
                dataFinal + "not equal " + getText(labelCellDataSinistro.apply(0)));
    }

    public void clicarBtnExportar() {
        clickAndHighlight(btnExportar);
    }

    public void clicarBtnFiltrar() {
        clearForce(inputSearchSinistros);
        clickAndHighlight(btnFilter);
    }

    public File validaDownloadDoArquivoXLS(String partialFileName) {
        File file = verifyFileExist(partialFileName);
        Assert.assertNotNull(file, "Deveria ter baixado o arquivo sinistro...xls");
        return file;
    }

    public void validaQueEstaExibindoBtnNovoSinistro() {
        expectElementVisible(btnIncluirNovoSinistro);
    }

    public void validaQueNaoEstaExibindoBtnNovoSinistro() {
        expectElementNotVisible(btnIncluirNovoSinistro);
    }

    public void clicarBtnIncluirNovoSinistro() {
        clickAndHighlight(btnIncluirNovoSinistro);
    }

    public void validaCamposSinistrosAbertosPerfilCCO() {
        expectElementVisible(headerId);
        expectElementVisible(headerDataHoraSinistro);
        expectElementVisible(headerOrigem);
        expectElementVisible(headerPrefixo);
        expectElementVisible(headerTipo);
        expectElementVisible(headerSubdivisao);
        expectElementVisible(headerSecaoBloqueioSB);
        expectElementVisible(headerEmailEnviadoANTT);
        expectElementVisible(headerCirculacaoInterditada);
        expectElementVisible(headerStatus);
        expectElementVisible(headerEdit);
        expectElementVisible(legendaTempoViaEstaInterditada);
        expectElementVisible(legendaTempoQueFicouInterditada);
    }

    public void validaExibicaoDaPaginaListagemDeSinistros() {
        Assert.assertEquals(getText(labelTitulo), "LISTAGEM DE SINISTROS");
    }

    public void validaExibicaoDosElementosPermitidosParaPerfilCCO() {
        expectElementVisible(tabSinistrosAbertos);
        expectElementVisible(tabSinistrosEncerrados);
        expectElementVisible(inputSearchSinistros);
        expectElementVisible(btnIncluirNovoSinistro);
    }

    public void validaCamposSinistrosAbertosPerfilCCOVia() {
        expectElementVisible(headerId);
        expectElementVisible(headerDataHoraSinistro);
        expectElementVisible(headerOrigem);
        expectElementVisible(headerPrefixo);
        expectElementVisible(headerTipo);
        expectElementVisible(headerSubdivisao);
        expectElementVisible(headerSecaoBloqueioSB);
        expectElementVisible(headerEmailEnviadoANTT);
        expectElementVisible(headerCirculacaoInterditada);
        expectElementVisible(headerStatus);
        expectElementVisible(headerEdit);
        expectElementVisible(legendaTempoViaEstaInterditada);
        expectElementVisible(legendaTempoQueFicouInterditada);
    }

    public void validaExibicaoDosElementosPermitidosParaPerfilCCOVia() {
        expectElementVisible(tabSinistrosAbertos);
        expectElementVisible(inputSearchSinistros);

        expectElementNotVisible(tabSinistrosEncerrados);
        expectElementNotVisible(btnIncluirNovoSinistro);
    }

    public void validaCamposSinistrosAbertosPerfilSRO() {
        expectElementVisible(headerId);
        expectElementVisible(headerDataHoraSinistro);
        expectElementVisible(headerPrefixo);
        expectElementVisible(headerTipo);
        expectElementVisible(headerSubdivisao);
        expectElementVisible(headerSecaoBloqueioSB);
        expectElementVisible(headerCirculacaoInterditada);
        expectElementVisible(headerGravidadeDoAcidente);
        expectElementVisible(headerStatus);
        expectElementVisible(headerEdit);
        expectElementVisible(legendaTempoViaEstaInterditada);
        expectElementVisible(legendaTempoQueFicouInterditada);
        expectElementNotVisible(headerEmailEnviadoANTT);
    }

    public void validaCamposSinistrosEncerradosPerfilCCO() {
        expectElementVisible(headerId);
        expectElementVisible(headerDataHoraSinistro);
        expectElementVisible(headerDataHoraLiberacaoFinal);
        expectElementVisible(headerPrefixo);
        expectElementVisible(headerTipo);
        expectElementVisible(headerSubdivisao);
        expectElementVisible(headerSecaoBloqueioSB);
        expectElementVisible(headerCirculacaoInterditada);
        expectElementVisible(headerStatus);
        expectElementVisible(headerResponsavel);
        expectElementVisible(headerShow);

        expectElementNotVisible(headerOrigem);
        expectElementNotVisible(headerEmailEnviadoANTT);
        expectElementNotVisible(headerEdit);
    }

    public void validaCamposSinistrosEncerradosPerfilSRO() {
        expectElementVisible(headerId);
        expectElementVisible(headerDataHoraSinistro);
        expectElementVisible(headerDataHoraLiberacaoFinal);
        expectElementVisible(headerPrefixo);
        expectElementVisible(headerTipo);
        expectElementVisible(headerSecaoBloqueioSB);
        expectElementVisible(headerCirculacaoInterditada);
        expectElementVisible(headerStatus);
        expectElementVisible(headerResponsavel);
        expectElementVisible(headerEdit);
        expectElementVisible(legendaTempoViaEstaInterditada);
        expectElementVisible(legendaTempoQueFicouInterditada);

        expectElementNotVisible(headerSubdivisao);
        expectElementNotVisible(headerOrigem);
        expectElementNotVisible(headerEmailEnviadoANTT);
        expectElementNotVisible(headerShow);
    }

    public void clicarBtnAplicarFiltro() {
        clickAndHighlight(btnAplicarFiltro);
    }

    public void limparCampoDataInicial() {
        clearForce(inputDataInicio);
    }

    public void validaCorBtnFiltrar(String cor) {
        String classe = getAttribute(btnFilter, "class");
        Assert.assertFalse(classe.contains(cor), "A classe atual é : " + classe);
    }

    public void clicarBtnRemoverFiltro() {
        clickAndHighlight(btnRemoverFiltro);
    }

    public void validaResultadoAoRemoverFiltro() {
        expectLoading();
        expectElementVisibleWithoutHighlight(labelCellDataSinistro.apply(0));
        expectElementVisibleWithoutHighlight(labelCellDataSinistro.apply(1));
        expectElementVisibleWithoutHighlight(labelCellDataSinistro.apply(2));
    }

    public void validarExibicaoDeUmRegistroNaLista() {
        expectElementVisible(btnCellEdit.apply(0));
    }

    public void clicarBtnEditarSinistro(int linha) {
        clickAndHighlight(headerId);
        waitTime();
        expectLoading();
        clickAndHighlight(btnCellEdit.apply(linha));
    }

    public void validarDadosDoRegistroAbertos(Sinistro sinistro) {
        expectText(labelCellDataSinistro.apply(0), sinistro.getDate());
        expectText(labelCellDataSinistro.apply(0), sinistro.getHora());
        expectText(labelCellOrigemSinistro.apply(0), "Manual");
        expectText(labelCellPrefixoTremSinistro.apply(0), sinistro.getPrefixo());
        expectText(labelCellTipoSinistro.apply(0), sinistro.getTipo());
        expectText(labelCellSubdivisaoSinistro.apply(0), sinistro.getSubdivisao());
        expectText(labelCellSecaoBloqueioSBSinistro.apply(0), sinistro.getSecaoBloqueioSB());
        expectText(labelCellEmailANTTEnviadoSinistro.apply(0), "Não");
        expectText(labelCellInterdicaoCirculacaoSinistro.apply(0), "Não");
        expectText(labelCellStatusSinistro.apply(0), sinistro.getStatus());
    }

    public void clicarBtnEditarSinistroSemOrdenar(int linha) {
        clickAndHighlight(btnCellEdit.apply(linha));
    }

    public void ordenarIdDecrescente() {
        clickAndHighlight(headerId);
        expectLoading();
        clickAndHighlight(headerId);
        waitTime();
    }

    public void ordenarIdCrescente() {
        clickAndHighlight(headerId);
        waitTime();
    }

    public void clicarBntEditarSinistroOrdenadoDecrescente() {
        ordenarIdDecrescente();
        clickAndHighlight(btnCellEdit.apply(0));
    }

    public void validaQueSinistroFoiEncerradoENãoAceito() {
        ordenarIdDecrescente();
        Assert.assertEquals(getText(cellSinistroAceito.apply(0)), "Não");
    }

    public void validaQueSinistroFoiEncerradoEAceito() {
        ordenarIdDecrescente();
        Assert.assertEquals(getText(cellSinistroAceito.apply(0)), "Sim");
    }

    public void validarDadosDoRegistroAbertosSRO(Sinistro sinistro) {
        expectText(labelCellDataSinistro.apply(0), sinistro.getDate());
        expectText(labelCellDataSinistro.apply(0), sinistro.getHora());
        expectText(labelCellPrefixoTremSinistro.apply(0), sinistro.getPrefixo());
        expectText(labelCellTipoSinistro.apply(0), sinistro.getTipo());
        expectText(labelCellSubdivisaoSinistro.apply(0), sinistro.getSubdivisao());
        expectText(labelCellSecaoBloqueioSBSinistro.apply(0), sinistro.getSecaoBloqueioSB());
        expectText(labelCellStatusSinistro.apply(0), sinistro.getStatus());
    }
}
