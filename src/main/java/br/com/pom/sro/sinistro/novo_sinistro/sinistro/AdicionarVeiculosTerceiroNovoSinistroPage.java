package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.api.model.sinistro.Sinistro;
import br.com.api.model.sinistro.VeiculoDeTerceiro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class AdicionarVeiculosTerceiroNovoSinistroPage extends GeralPage {

    Function<Integer, By> labelCellMarca = (Integer index) -> By.id("tbMarca-" + index);
    Function<Integer, By> labelCellModelo = (Integer index) -> By.id("tbModelo-" + index);
    Function<Integer, By> labelCellPlaca = (Integer index) -> By.id("tbPlaca-" + index);
    Function<Integer, By> labelCellRegistradoEm = (Integer index) -> By.id("tbDataCadastro-" + index);
    Function<Integer, By> btnEdit = (Integer index) -> By.id("tbEdit-" + index);
    Function<Integer, By> btnRemove = (Integer index) -> By.id("tbRemove-" + index);

    private By separatorAdicionarVeiculosTerceiro = By.name("Adicionar Veículos de Terceiros");
    private By btnAdicionarMaisUmVeiculo = By.id("addVeiculoTerceiro");
    private By optionExistemVeiculosTerceirosSim = By.cssSelector("#existemVeiculosEnvolvidos-sim");
    private By optionExistemVeiculosTerceirosNao = By.cssSelector("#existemVeiculosEnvolvidos-nao");

    private By inputMarca = By.id("marca");
    private By inputModelo = By.id("modelo");
    private By inputPlaca = By.id("placa");
    private By tableHeaderMarca = By.className("mat-column-marca");
    private By tableHeaderModelo = By.className("mat-column-modelo");
    private By tableHeaderPlaca = By.className("mat-column-placa");
    private By tableHeaderRegistradoEm = By.className("mat-column-dataCadastro");
    private By tableHeaderAcoes = By.className("mat-column-actions");
    private By btnSalvaModal = By.id("dialog-button-yes");
    private By btnCancelarModal = By.id("dialog-button-no");
    private By btnFecharModal = By.id("dialog-button-close");

    private By btnEditar = By.id("edit-addVeiculos");

    public void informarQueExisteVeiculosDeTerceiro() {
        scrollToElement(separatorAdicionarVeiculosTerceiro);
        expectElementClickable(optionExistemVeiculosTerceirosSim);
        clickWithAction(optionExistemVeiculosTerceirosSim);
    }

    public void validaExibicaoDosCamposQuandoExisteVeiculosTerceiro() {
        expectElementVisible(btnAdicionarMaisUmVeiculo);
        expectElementNotVisible(tableHeaderMarca);
        expectElementNotVisible(tableHeaderModelo);
        expectElementNotVisible(tableHeaderPlaca);
        expectElementNotVisible(tableHeaderRegistradoEm);
        expectElementNotVisible(tableHeaderAcoes);
    }

    public void informarQueNaoExisteVeiculosDeTerceiro() {
        scrollToElement(separatorAdicionarVeiculosTerceiro);
        expectElementClickable(optionExistemVeiculosTerceirosNao);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickWithAction(optionExistemVeiculosTerceirosNao);
    }

    public void validaQueNaoExibiuCamposParaEditarVeiculosTerceiro() {
        expectElementNotVisible(btnAdicionarMaisUmVeiculo);
        expectElementNotVisible(tableHeaderMarca);
        expectElementNotVisible(tableHeaderModelo);
        expectElementNotVisible(tableHeaderPlaca);
        expectElementNotVisible(tableHeaderRegistradoEm);
        expectElementNotVisible(tableHeaderAcoes);
    }

    public void clicarBtnAdicionarMaisUmVeículo() {
        clickAndHighlight(btnAdicionarMaisUmVeiculo);
    }

    public void validaModalAdicionarMaisUmVeículo() {
        expectElementVisible(btnFecharModal);
        expectElementVisible(inputMarca);
        expectElementVisible(inputModelo);
        expectElementVisible(inputPlaca);
        expectElementVisible(btnFecharModal);
        expectElementVisible(btnSalvaModal);
    }

    public void preencherCamposDaModalDeInclusaoDeNovoVeiculo(VeiculoDeTerceiro veiculoDeTerceiro) {
        sendKeys(inputMarca, veiculoDeTerceiro.getMarca());
        sendKeys(inputModelo, veiculoDeTerceiro.getModelo());
        sendKeys(inputPlaca, veiculoDeTerceiro.getPlaca());
    }

    public void clicarBtnSalvarModalInclusao() {
        clickAndHighlight(btnSalvaModal);
    }

    public void validarTabelaComVeiculo(int linha, VeiculoDeTerceiro veiculoDeTerceiro) {
        expectText(labelCellMarca.apply(linha - 1), veiculoDeTerceiro.getMarca());
        expectText(labelCellModelo.apply(linha - 1), veiculoDeTerceiro.getModelo());
        expectText(labelCellPlaca.apply(linha - 1), veiculoDeTerceiro.getPlaca());
    }

    public void clicarBtnEditarVeiculo(int index) {
        clickAndHighlight(btnEdit.apply(index));
    }

    public void clicarBtnExcluirVeiculo(int index) {
        clickAndHighlight(btnRemove.apply(index));
    }

    public void validaTextoModalDeExclusao(String msg) {
        String textoModal = getTextModal();
        Assert.assertEquals(textoModal, msg);
    }

    public void validarTabelaComVeiculoRemovido() {
        expectElementNotVisible(labelCellMarca.apply(0));
        expectElementNotVisible(labelCellModelo.apply(0));
        expectElementNotVisible(labelCellPlaca.apply(0));
        expectElementNotVisible(labelCellRegistradoEm.apply(0));
    }

    public void preencherCampos(Sinistro sinistro) {
        scrollToElement(separatorAdicionarVeiculosTerceiro);
        if (sinistro.isExistemVeiculosDeTerceiros()) {
            clickWithAction(optionExistemVeiculosTerceirosSim);
            clickAndHighlight(btnAdicionarMaisUmVeiculo);
            preencherCamposDaModalDeInclusaoDeNovoVeiculo(sinistro.getVeiculoDeTerceiro());
            clickAndHighlight(btnSalvaModal);
        } else {
            clickWithAction(optionExistemVeiculosTerceirosNao);
        }
    }

    public void validaCamposVazios() {
        scrollToElement(separatorAdicionarVeiculosTerceiro);
    }

    public void validaCamposPreenchidos(int linha, Sinistro sinistro) {
        scrollToElement(separatorAdicionarVeiculosTerceiro);
        if (sinistro.isExistemVeiculosDeTerceiros()) {
            validarTabelaComVeiculo(linha, sinistro.getVeiculoDeTerceiro());
        }
    }

    public void validaCamposPreenchidosReadOnly(int linha, Sinistro sinistro) {
        scrollToElement(separatorAdicionarVeiculosTerceiro);
        if (sinistro.isExistemVeiculosDeTerceiros()) {
            validarTabelaComVeiculo(linha, sinistro.getVeiculoDeTerceiro());
        }
    }

    public void clicarBtnEditar() {
        scrollToElement(btnEditar);
        clickAndHighlight(btnEditar);
    }

}
