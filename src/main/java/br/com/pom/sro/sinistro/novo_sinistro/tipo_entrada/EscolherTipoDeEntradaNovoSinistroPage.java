package br.com.pom.sro.sinistro.novo_sinistro.tipo_entrada;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class EscolherTipoDeEntradaNovoSinistroPage extends GeralPage {

    private By labelTitlePage = By.cssSelector("body > sro-root > sro-shell > div > sro-new-options > sro-default-title > div > h1");
    private By btnNumeroDaMacro = By.id("btn-macro");
    private By btnNumeroDaOS = By.id("btn-os");
    private By btnDadosManuais = By.id("btn-manual");


    public void validaExibicaoDaPagina() {
        Assert.assertEquals(getText(labelTitlePage), "NOVO SINISTRO");
        expectElementVisible(btnNumeroDaMacro);
        expectElementVisible(btnNumeroDaOS);
        expectElementVisible(btnDadosManuais);
    }

    public void clicarBtnDadosManuais() {
        clickAndHighlight(btnDadosManuais);
    }

    public void clicarBtnNumeroDaMacro() {
        clickAndHighlight(btnNumeroDaMacro);
    }

    public void clicarBtnNumedoDaOS() {
        clickAndHighlight(btnNumeroDaOS);
    }
}
