package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;
import java.util.function.IntFunction;

public class SobreOLocalNovoSinistroPage extends GeralPage {

    Function<Integer, By> optionSubdivisao = (Integer index) -> By.id("option-subdivisao-" + index);
    Function<Integer, By> optionUnidadeProducao = (Integer index) -> By.id("option-unidadeProducao-" + index);
    IntFunction<By> optionOperacaoSinistro = (index) -> By.id("operacao-sinistro-" + index);
    IntFunction<By> optionComplexoSinistro = (index) -> By.id("complexo-sinistro-" + index);
    private By separatorSobreOLocal = By.name("Sobre o Local");
    private By inputSecaoBloqueioSB = By.id("secaoBloqueio");
    private By inputKmLocalAcidente = By.id("kmLocalAcidente");
    private By selectSubdivisao = By.id("subDivisao");
    private By selectUnidadeProducao = By.id("unidadeProducao");
    private By selectOperacaoSinistro = By.id("operacao-sinistro");
    private By selectComplexoSinistro = By.id("complexo-sinistro");
    private By btnEditar = By.id("edit-sobreLocal");

    public void preencherCampos(Sinistro sinistro) {
        scrollToElement(separatorSobreOLocal);
        sendKeys(inputSecaoBloqueioSB, sinistro.getSecaoBloqueioSB());
        sendKeys(inputKmLocalAcidente, sinistro.getKm());
        clickAndHighlight(selectSubdivisao);
        clickAndHighlight(optionSubdivisao.apply(sinistro.getSubdivisaoIndex()));
        sinistro.setSubdivisao(getText(selectSubdivisao));
        clickAndHighlight(selectUnidadeProducao);
        clickAndHighlight(optionUnidadeProducao.apply(sinistro.getUnidadeProducaoIndex()));
        sinistro.setUnidadeProducao(getText(selectUnidadeProducao));


        sinistro.setQualEAOperacaoValue(
                selectOptionAndReturnValue(
                        selectOperacaoSinistro,
                        optionOperacaoSinistro,
                        sinistro.getQualEAOperacaoIndex()
                )
        );

        sinistro.setQualEOComplexoValue(
                selectOptionAndReturnValue(
                        selectComplexoSinistro,
                        optionComplexoSinistro,
                        sinistro.getQualEOComplexoIndex()
                )
        );
    }

    public void validaCamposVazios() {
        scrollToElement(separatorSobreOLocal);
        Assert.assertEquals(getText(inputSecaoBloqueioSB), "");
        Assert.assertEquals(getText(inputKmLocalAcidente), "");
        Assert.assertEquals(getText(selectSubdivisao), "Escolha abaixo");
        Assert.assertEquals(getText(selectUnidadeProducao), "Escolha abaixo");
        Assert.assertEquals(getText(selectOperacaoSinistro), "Escolha abaixo");
        Assert.assertEquals(getText(selectComplexoSinistro), "Escolha abaixo");
    }

    public void validaCamposPreenchidos(Sinistro sinistro) {
        scrollToElement(separatorSobreOLocal);
        Assert.assertEquals(getValue(inputSecaoBloqueioSB), String.valueOf(sinistro.getSecaoBloqueioSB()));
        Assert.assertEquals(getValue(inputKmLocalAcidente), sinistro.getKm());
        Assert.assertEquals(getValue(selectSubdivisao), sinistro.getSubdivisao());
        Assert.assertEquals(getValue(selectUnidadeProducao), sinistro.getUnidadeProducao());
        Assert.assertEquals(getValue(selectOperacaoSinistro), sinistro.getQualEAOperacaoValue());
        Assert.assertEquals(getValue(selectComplexoSinistro), sinistro.getQualEOComplexoValue());
    }

    public void validaCamposPreenchidosReadOnly(Sinistro sinistro) {
        scrollToElement(separatorSobreOLocal);
        Assert.assertEquals(getValue(inputSecaoBloqueioSB), String.valueOf(sinistro.getSecaoBloqueioSB()));
        Assert.assertEquals(getValue(inputKmLocalAcidente), sinistro.getKm());
        Assert.assertEquals(getValue(selectSubdivisao), sinistro.getSubdivisao());
        Assert.assertEquals(getValue(selectUnidadeProducao), sinistro.getUnidadeProducao());
        Assert.assertEquals(getValue(selectOperacaoSinistro), sinistro.getQualEAOperacaoValue());
        Assert.assertEquals(getValue(selectComplexoSinistro), sinistro.getQualEOComplexoValue());
    }

    public void clicarBtnEditar() {
        scrollToElement(btnEditar);
        clickAndHighlight(btnEditar);
    }

}
