package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.pom.sro.GeralPage;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.Locale;
import java.util.function.Function;

import static br.com.api.GeradorDeMassa.getRandomCharacters;

public class AceiteDoSinistroPage extends GeralPage {

    Faker faker = new Faker(new Locale("pt-BR"));
    Function<Integer, By> optionBandeira = (Integer index) -> By.id("option-bandeira-" + index);
    Function<Integer, By> optionCausaProvavel = (Integer index) -> By.id("option-causa-" + index);
    Function<Integer, By> linhaArquivo = (Integer index) -> By.id("file-upload-component-list-files-item-" + index);
    Function<Integer, By> btnExcluirArquivo = (Integer index) -> By.id("file-upload-component-list-files-btn-remove-" + index);
    Function<Integer, By> cellDataHoraReabertura = (Integer index) -> By.id("tbListaReabertura-data-hora-" + index);
    Function<Integer, By> cellUsuarioReabertura = (Integer index) -> By.id("tbListaReabertura-usuario-" + index);
    Function<Integer, By> cellMatriculaReabertura = (Integer index) -> By.id("tbListaReabertura-matricula-" + index);
    Function<Integer, By> cellComentarioReabertura = (Integer index) -> By.id("tbListaReabertura-comentario-" + index);
    Function<Integer, By> cellBtnDownloadReabertura = (Integer index) -> By.id("btnListaReabertura-anexo-" + index);
    Function<Integer, By> cellDataHoraRecusa = (Integer index) -> By.id("tbListaRecusaSinistro-data-hora-" + index);
    Function<Integer, By> cellUsuarioRecusa = (Integer index) -> By.id("tbListaRecusaSinistro-usuario-" + index);
    Function<Integer, By> cellMatriculaRecusa = (Integer index) -> By.id("tbListaRecusaSinistro-matricula-" + index);
    Function<Integer, By> cellComentarioRecusa = (Integer index) -> By.id("tbListaRecusaSinistro-comentario-" + index);
    Function<Integer, By> cellBtnDownloadRecusa = (Integer index) -> By.id("btnListaRecusaSinistro-anexo-" + index);

    private By msgInfoAceiteSinistro = By.className("texto");
    private By secaoEscolhaDoAceite = By.name("ESCOLHA DE ACEITE");
    private By secaoClassificacao = By.name("CLASSIFICAÇÃO");
    private By secaoCausaProvavel = By.name("CAUSA PROVÁVEL");
    private By radioButtonEscolhaSim = By.cssSelector("#aceitaSinistro-sim > label");
    private By radioButtonEscolhaNao = By.cssSelector("#aceitaSinistro-nao > label");
    private By radioButtonClassificacaoSim = By.cssSelector("#classificacaoSinistro-acidente > label");
    private By radioButtonClassificacaoNao = By.cssSelector("#classificacaoSinistro-ocorrencia > label");
    private By selectBandeira = By.id("idBandeiraControl");
    private By selectCausaProvavel = By.id("idCausaControl");

    /*
    Histórico
     */
    private By btnConfirmarAceite = By.id("button-saved-info");
    private By btnConfirmarRecusa = By.id("button-saved-info");
    private By btnCancelar = By.id("button-cancel");
    private By inputMotivoDaRecusa = By.id("motivoRecusa");
    private By btnEscolherArquivo = By.id("input-file-upload");
    private By tbArquivos = By.id("file-upload-component-list-files");
    private By bntConfirmarReabertura = By.id("button-saved-info");
    private By comentarioReabertura = By.id("comentarioControl");
    private By bntReabrirSinistro = By.id("button-reabrir-sinistro");
    private By secaoReaberturaHistorico = By.name("REABERTURA DE SINISTRO");
    private By listaReaberturaHistorico = By.cssSelector("#listaRecusaSinistro > tbody");

    private By secaoRecusaHistorico = By.name("RECUSA DE SINISTRO");
    private By listaRecusaHistorico = By.cssSelector("#listaRecusaSinistro > tbody");

    public void validaMsgInformativaParaProsseguirAceite(String msg) {
        Assert.assertEquals(getText(msgInfoAceiteSinistro), msg);
    }

    public void preencheEscolhaDoAceiteSim() {
        waitTime(200);
        clickWithAction(radioButtonEscolhaSim);
    }

    public void preencheClassificacaoAcidente() {

        scrollToElement(radioButtonClassificacaoSim);
        scrollToElement(radioButtonClassificacaoSim);
        waitTime(200);
        clickWithAction(radioButtonClassificacaoSim);
    }

    public void preencheBandeiraECausaProvavel(int opcao) {
        clickAndHighlight(selectBandeira);
        clickAndHighlight(optionBandeira.apply(opcao - 1));
        waitTime(500);
        clickAndHighlight(selectCausaProvavel);
        clickAndHighlight(optionCausaProvavel.apply(opcao - 1));
    }

    public void clicarBtnConfirmarAceite() {
        clickAndHighlight(btnConfirmarAceite);
    }

    public void preenchoAEscolhaDoAceiteComNao() {
        waitTime(1000);
        clickWithAction(radioButtonEscolhaNao);
    }

    public void preencheMotivoDaRecusa() {
        sendKeysWithJavaScript(inputMotivoDaRecusa, getRandomCharacters(4000));
    }

    public void insereArquivo(int qtdArquivo) {
        uploadArquivo(qtdArquivo);
        scrollToElement(tbArquivos);
    }

    public void validaQueOsArquivosForamAdicionados(int qtdArquivosAdicionados) {
        scrollToElement(tbArquivos);
        Assert.assertEquals(qtdArquivosAdicionados, countChildElement(tbArquivos, "li"));
    }

    public void excluirArquivoDaRecusa(int linha) {
        scrollToElement(tbArquivos);
        clickAndHighlight(btnExcluirArquivo.apply(linha - 1));
    }

    public void clicarBtnConfirmarRecusa() {
        clickAndHighlight(btnConfirmarRecusa);
    }

    public void clicarBtnConfirmarReabertura() {
        clickAndHighlight(bntConfirmarReabertura);
    }

    public void preencheComentarReabertura() {
        sendKeysWithJavaScript(comentarioReabertura, "Comentário reabertura");
    }

    public void clicarBtnReabrirSinistro() {
        clickAndHighlight(bntReabrirSinistro);
    }

    public void validaHistoricoReaberturaComQuantidade(int qtdReabertura) {
        expectElementVisibleWithoutHighlight(secaoReaberturaHistorico);
        scrollToElement(secaoReaberturaHistorico);
        int qtdRegistros = countChildElement(listaReaberturaHistorico, "tr");
        Assert.assertEquals(qtdRegistros, qtdReabertura, "A quantidade de linhas deveria ser :" + qtdReabertura + "mas é " + qtdRegistros);
    }

    public void validaHistoricoRecusaComQuantidade(int qtdRecusa) {
        expectElementVisibleWithoutHighlight(secaoRecusaHistorico);
        scrollToElement(secaoRecusaHistorico);
        int qtdRegistros = countChildElement(listaRecusaHistorico, "tr");
        Assert.assertEquals(qtdRegistros, qtdRecusa, "A quantidade de linhas deveria ser :" + qtdRecusa + "mas é " + qtdRegistros);
    }
}
