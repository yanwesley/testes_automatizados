package br.com.pom.sro.sinistro.novo_sinistro.liberacao;

import br.com.api.model.sinistro.AbaLiberacao;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class AbaLiberacaoPage extends GeralPage {


    Function<Integer, By> cellDataPrevisaoDeLiberacao = (Integer index) -> By.id("table-cell-data-" + index);
    Function<Integer, By> cellHorarioPrevisaoDeLiberacao = (Integer index) -> By.id("table-cell-hora-" + index);
    Function<Integer, By> cellBtnExcluirPrevisaoLiberacao = (Integer index) -> By.id("button-remove-sinistro-" + index);
    private By separatorDataPrevisaoLiberacao = By.name("Previsão de liberação");
    private By inputDataLiberacao = By.id("input-previsao-date");
    private By inputHorarioLiberacao = By.id("input-previsao-time");
    private By btnAddPrevisao = By.id("button-add-previsao");
    private By separatorLiberacaoParcial = By.name("Liberação parcial");
    private By inputDataLiberacaoParcial = By.id("input-parcial-date");
    private By inputHorarioLiberacaoParcial = By.id("input-parcial-time");

    private By separatorLiberacaoMecanica = By.name("Liberação mecânica");
    private By inputDataLiberacaoMecanica = By.id("input-mecanica-date");
    private By inputHorarioLiberacaoMecanica = By.id("input-mecanica-time");

    private By separatorLiberacaoVia = By.name("Liberação via");
    private By inputDataLiberacaoVia = By.id("input-via-date");
    private By inputHorarioLiberacaoVia = By.id("input-via-time");

    private By separatorLiberacaoFinal = By.name("Liberação final");
    private By inputDataLiberacaoFinal = By.id("input-final-date");
    private By inputHorarioLiberacaoFinal = By.id("input-final-time");

    private By btnSalvarInformacoes = By.id("button-saved-info");
    private By btnSalvarEEncerrar = By.id("button-save-close");
    private By btnCancelar = By.id("button-cancel");
    private By btnVoltar = By.id("button-back");

    private By msgLiberacaoFinal = By.id("message-liberacao-final");
    private By msgModalConfirmacao = By.id("dialog-confirmation-message");
    private By btnConfirmarModalConfirmacao = By.id("dialog-button-yes");
    private By btnCancelarModalConfirmacao = By.id("dialog-button-no");

    private By separatorInformarANTT = By.name("Informar ANTT");
    private By btnInformarAntt = By.id("informarAntt");

    private By btnEditarLiberacaoPrevisao = By.id("button-edit-previsao");
    private By btnEditarLiberacaoParcial = By.id("button-edit-parcial");
    private By btnEditarLiberacaoMecanica = By.id("button-edit-mecanica");
    private By btnEditarLiberacaoVia = By.id("button-edit-via");

    public void preencheSecaoPrevisaoDeLiberacao(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorDataPrevisaoLiberacao);
        clearForce(inputDataLiberacao);
        sendKeys(inputDataLiberacao, abaLiberacao.getInputDataPrevisaoLiberacao());
        clearForce(inputHorarioLiberacao);
        sendKeys(inputHorarioLiberacao, abaLiberacao.getInputHorarioPrevisaoLiberacao());
        clickAndHighlight(btnAddPrevisao);
    }

    public void preencheSecaoDeLiberacaoParcial(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoParcial);
        clearForce(inputDataLiberacaoParcial);
        sendKeys(inputDataLiberacaoParcial, abaLiberacao.getInputDataLiberacaoParcial());
        clearForce(inputHorarioLiberacaoParcial);
        sendKeys(inputHorarioLiberacaoParcial, abaLiberacao.getInputHorarioLiberacaoParcial());
    }

    public void preencheSecaoDeLiberacaoMecanica(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoMecanica);
        clearForce(inputDataLiberacaoMecanica);
        sendKeys(inputDataLiberacaoMecanica, abaLiberacao.getInputDataLiberacaoMecanica());
        clearForce(inputHorarioLiberacaoMecanica);
        sendKeys(inputHorarioLiberacaoMecanica, abaLiberacao.getInputHorarioLiberacaoMecanica());
    }

    public void preencheSecaoDeLiberacaoVia(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoVia);
        clearForce(inputDataLiberacaoVia);
        sendKeys(inputDataLiberacaoVia, abaLiberacao.getInputDataLiberacaoVia());
        clearForce(inputHorarioLiberacaoVia);
        sendKeys(inputHorarioLiberacaoVia, abaLiberacao.getInputHorarioLiberacaoVia());
    }

    public void preencheSecaoDeLiberacaoFinal(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoFinal);
        clearForce(inputDataLiberacaoFinal);
        sendKeys(inputDataLiberacaoFinal, abaLiberacao.getInputDataLiberacaoFinal());
        clearForce(inputHorarioLiberacaoFinal);
        sendKeys(inputHorarioLiberacaoFinal, abaLiberacao.getInputHorarioLiberacaoFinal());
    }

    public void validaMensagemLiberacaoFinal(String msg) {
        scrollToElement(separatorLiberacaoFinal);
        Assert.assertEquals(msg, getText(msgLiberacaoFinal));
    }

    public void clicarBtnSalvarEEncerrar() {
        scrollToElement(btnSalvarEEncerrar);
        clickAndHighlight(btnSalvarEEncerrar);
    }

    public void validaMsgModalConfirmacao(String msg) {
        Assert.assertEquals(getText(msgModalConfirmacao), msg);
    }

    public void clicarBtnCancelarModalConfirmacao() {
        clickAndHighlight(btnCancelarModalConfirmacao);
    }

    public void validarTabLiberacao() {
        expectElementVisible(separatorDataPrevisaoLiberacao);
    }

    public void clicarBtnConfirmarModalConfirmacao() {
        clickAndHighlight(btnConfirmarModalConfirmacao);
    }

    public void validaPrevisaoDeLiberacaoPreenchida(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorDataPrevisaoLiberacao);
        expectElementVisible(cellDataPrevisaoDeLiberacao.apply(0));
        Assert.assertEquals(getText(cellDataPrevisaoDeLiberacao.apply(0)), abaLiberacao.getInputDataPrevisaoLiberacao());
        expectText(cellHorarioPrevisaoDeLiberacao.apply(0), abaLiberacao.getInputHorarioPrevisaoLiberacao());
        Assert.assertEquals(getText(cellHorarioPrevisaoDeLiberacao.apply(0)), abaLiberacao.getInputHorarioPrevisaoLiberacao());
    }

    public void validaLiberacaoParcialPreenchida(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoParcial);
        expectValue(inputDataLiberacaoParcial, abaLiberacao.getInputDataLiberacaoParcial());
        Assert.assertEquals(getValue(inputDataLiberacaoParcial), abaLiberacao.getInputDataLiberacaoParcial());
        expectValue(inputHorarioLiberacaoParcial, abaLiberacao.getInputHorarioLiberacaoParcial());
        Assert.assertEquals(getValue(inputHorarioLiberacaoParcial), abaLiberacao.getInputHorarioLiberacaoParcial());
    }

    public void validaLiberacaoMecanicaPreenchida(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoMecanica);
        Assert.assertEquals(getValue(inputDataLiberacaoMecanica), abaLiberacao.getInputDataLiberacaoMecanica());
        Assert.assertEquals(getValue(inputHorarioLiberacaoMecanica), abaLiberacao.getInputHorarioLiberacaoMecanica());
    }

    public void validaLiberacaoViaPreenchida(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoVia);
        Assert.assertEquals(getValue(inputDataLiberacaoVia), abaLiberacao.getInputDataLiberacaoVia());
        Assert.assertEquals(getValue(inputHorarioLiberacaoVia), abaLiberacao.getInputHorarioLiberacaoVia());
    }

    public void validaLiberacaoFinalPreenchida(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoFinal);
        Assert.assertEquals(getValue(inputDataLiberacaoFinal), abaLiberacao.getInputDataLiberacaoFinal());
        Assert.assertEquals(getValue(inputHorarioLiberacaoFinal), abaLiberacao.getInputHorarioLiberacaoFinal());
    }

    //Métodos para form apenas leitura
    public void validaPrevisaoDeLiberacaoPreenchidaReadOnly(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorDataPrevisaoLiberacao);
        expectElementVisible(cellDataPrevisaoDeLiberacao.apply(0));
        Assert.assertEquals(getText(cellDataPrevisaoDeLiberacao.apply(0)), abaLiberacao.getInputDataPrevisaoLiberacao());
        Assert.assertEquals(getText(cellHorarioPrevisaoDeLiberacao.apply(0)), abaLiberacao.getInputHorarioPrevisaoLiberacao());
    }

    public void validaLiberacaoParcialPreenchidaReadOnly(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoParcial);
        Assert.assertEquals(getValue(inputDataLiberacaoParcial), abaLiberacao.getInputDataLiberacaoParcial());
        Assert.assertEquals(getValue(inputHorarioLiberacaoParcial), abaLiberacao.getInputHorarioLiberacaoParcial());
    }

    public void validaLiberacaoMecanicaPreenchidaReadOnly(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoMecanica);
        Assert.assertEquals(getValue(inputDataLiberacaoMecanica), abaLiberacao.getInputDataLiberacaoMecanica());
        Assert.assertEquals(getValue(inputHorarioLiberacaoMecanica), abaLiberacao.getInputHorarioLiberacaoMecanica());
    }

    public void validaLiberacaoViaPreenchidaReadOnly(AbaLiberacao abaLiberacao) {
        scrollToElement(separatorLiberacaoVia);
        Assert.assertEquals(getValue(inputDataLiberacaoVia), abaLiberacao.getInputDataLiberacaoVia());
        Assert.assertEquals(getValue(inputHorarioLiberacaoVia), abaLiberacao.getInputHorarioLiberacaoVia());
    }

    public void excluirUmaPrevisaoDeLiberacao(int linha) {
        scrollToElement(separatorDataPrevisaoLiberacao);
        clickAndHighlight(cellBtnExcluirPrevisaoLiberacao.apply(linha - 1));
    }

    public void validaQueAPrevisaoFoiExcluida() {
        expectElementNotVisible(cellDataPrevisaoDeLiberacao.apply(0));
    }

    public void validaQueADataLiberacaoFinalNaoEstaPreenchida() {
        scrollToElement(separatorLiberacaoFinal);
        Assert.assertTrue(isEmpty(inputDataLiberacaoFinal) && isEmpty(inputHorarioLiberacaoFinal));
    }

    public void validaBotaoSalvarInformacoes() {
        scrollToElement(separatorLiberacaoFinal);
        expectElementVisible(btnSalvarInformacoes);
    }

    public void validaBotaoSalvarEEncerrarNaoApresentado() {
        scrollToElement(separatorLiberacaoFinal);
        expectElementNotVisible(btnSalvarEEncerrar);
    }

    public void clicarBtnSalvarInformacoes() {
        scrollToElement(separatorLiberacaoFinal);
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void clicarEmVoltar() {
        scrollToElement(separatorLiberacaoFinal);
        clickAndHighlight(btnVoltar);
    }

    public void clicarBtnEnviarEmailANTT() {
        expectElementVisibleWithoutHighlight(separatorInformarANTT);
        scrollToElement(separatorInformarANTT);
        clickAndHighlight(btnInformarAntt);
    }

    public void naoEPossivelPreencherLiberacaoFinal() {
        scrollToElement(separatorLiberacaoFinal);
        Assert.assertEquals(getAttribute(inputDataLiberacaoFinal, "readonly"), "true");
        Assert.assertEquals(getAttribute(inputHorarioLiberacaoFinal, "readonly"), "true");
    }

    public void clicarBtnCancelar() {
        scrollToElement(separatorLiberacaoFinal);
        clickAndHighlight(btnCancelar);
    }

    public void clicarBtnEditarLiberacaoPrevisao() {
        scrollToElement(btnEditarLiberacaoPrevisao);
        clickAndHighlight(btnEditarLiberacaoPrevisao);
    }

    public void clicarBtnEditarLiberacaoParcial() {
        scrollToElement(btnEditarLiberacaoParcial);
        clickAndHighlight(btnEditarLiberacaoParcial);
    }

    public void clicarBtnEditarLiberacaoMecanica() {
        scrollToElement(btnEditarLiberacaoMecanica);
        clickAndHighlight(btnEditarLiberacaoMecanica);
    }

    public void clicarBtnEditarLiberacaoVia() {
        scrollToElement(btnEditarLiberacaoVia);
        clickAndHighlight(btnEditarLiberacaoVia);
    }
}
