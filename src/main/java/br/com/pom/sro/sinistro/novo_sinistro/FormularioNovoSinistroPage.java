package br.com.pom.sro.sinistro.novo_sinistro;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class FormularioNovoSinistroPage extends GeralPage {

    private By btnAlterar = By.cssSelector("#header-sinistro > div:nth-child(2) > mat-card > button");
    private By btnEnviarEmail = By.id("btn-send-email");
    private By btnEnderecoDoSinistro = By.id("btn-maps-location");
    private By btnFichaDoTrem = By.id("btn-ficha-trem");
    private By btnConfirmarEnvioEmail = By.id("dialog-button-yes");

    private By btnSalvarInformacoes = By.id("salvarSinistro");
    private By btnSalvarEAvancar = By.id("salvarAvancarSinistro");
    private By btnCancelar = By.id("cancelarSinistro");

    private By tabSinistro = By.id("tab-sinistro");
    private By tabVeiculos = By.id("tab-veiculos");
    private By tabLiberacao = By.id("tab-liberacao");

    public FormularioNovoSinistroPage() {
        super();
    }

    public void validaExibicaoPaginaDePreenchimento() {
        expectElementVisible(btnAlterar);
        expectElementVisible(btnEnviarEmail);
        expectElementNotVisible(btnEnderecoDoSinistro);
        expectElementNotVisible(btnFichaDoTrem);
    }

    public void validaQueOBotaoEnviarEMailEstaDesabilitado() {
        Assert.assertEquals(getAttribute(btnEnviarEmail, "disabled"), "true");
    }

    public void validarBtnEnderecoDoSinistroNaoEExibido() {
        expectElementNotVisible(btnEnderecoDoSinistro);
    }

    public void validoQueOBotaoFichaDoTremNaoEExibido() {
        expectElementNotVisible(btnFichaDoTrem);
    }

    public void clicarNoBotaoEnderecoDoSinistro() {
        clickAndHighlight(btnEnderecoDoSinistro);
    }

    public void validaExibicaoPaginaDePreenchimentoComCamposCarregados() {
        expectElementNotVisible(btnAlterar);
        expectElementVisible(btnEnderecoDoSinistro);
        expectElementVisible(btnEnviarEmail);
        expectElementDisable(btnFichaDoTrem);
    }

    public void clicarNoBotaoSalvarInformacoes() {
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void clicarNoBotaoSalvarEAvancar() {
        scrollToElement(btnSalvarEAvancar);
        clickWithJavaScript(btnSalvarEAvancar);
    }

    public void clicarNoBotaoAlterar() {
        clickAndHighlight(btnAlterar);
    }

    public void validarQueOBotaoAlterarNaoEstaVisivel() {
        expectElementNotVisible(btnAlterar);
    }

    public void clicarNoBotaoEnviarEmail() {
        clickAndHighlight(btnEnviarEmail);
    }

    public void clicarNoBotaoConfirmarDoModalDeEmail() {
        clickAndHighlight(btnConfirmarEnvioEmail);
    }

    public void clicaNaTabLiberacao() {
        clickAndHighlight(tabLiberacao);
    }

    public void clicaNaTabSinistro() {
        clickAndHighlight(tabSinistro);
    }

    public void clicarNaTabVeiculos() {
        clickAndHighlight(tabVeiculos);
    }
}
