package br.com.pom.sro.sinistro.novo_sinistro.sinistro;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class SinistroSROPage extends GeralPage {

    private By expansionPanelHeaderMatExpansionPanelHeader0 = By.id("mat-expansion-panel-header-0");
    private By divCdkAccordionChild0 = By.id("cdk-accordion-child-0");
    private By aTabCco = By.id("tab-cco");
    private By aTabHistorico = By.id("tab-historico");
    private By expansionPanelHeaderMatExpansionPanelHeader1 = By.id("mat-expansion-panel-header-1");
    private By divCdkAccordionChild1 = By.id("cdk-accordion-child-1");

    private By tabDadosCCOCirculacao = By.id("tab-cco");
    private By tabDadosCCOVia = By.id("tab-ccovia");
    private By tabAceiteDoSinistro = By.id("tab-aceitesinistro");
    private By tabHistorico = By.id("tab-historico");

    private By btnSalvarInformacoes = By.id("button-save-info");
    private By btnVoltar = By.id("back-button");

    public void clicaTabDadosCCOCirculacao() {
        clickAndHighlight(tabDadosCCOCirculacao);
    }

    public void clicaTabDadosCCOVia() {
        clickAndHighlight(tabDadosCCOVia);
    }

    public void clicaTabAceiteDoSinistro() {
        clickAndHighlight(tabAceiteDoSinistro);
    }

    public void clicaTabHistorico() {
        clickAndHighlight(tabHistorico);
    }

    public void clicarBtnSalvarInformacoes() {
        scrollToElement(btnSalvarInformacoes);
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void clicarBtnVoltar() {
        expectLoading();
        clickWithJavaScript(btnVoltar);
    }

    public void clicarExpandirSinistro() {
        expectLoading();
        clickAndHighlight(expansionPanelHeaderMatExpansionPanelHeader0);
    }
}
