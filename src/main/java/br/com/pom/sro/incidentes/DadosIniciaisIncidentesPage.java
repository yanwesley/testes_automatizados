package br.com.pom.sro.incidentes;

import br.com.api.model.incidente.Incidente;
import br.com.api.model.sinistro.Veiculo;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class DadosIniciaisIncidentesPage extends GeralPage {
    private By separatorSobreOIncidente = By.name("SOBRE O INCIDENTE");
    private By inputDataIncidente = By.id("dataIncidente");
    private By inputHorarioIncidente = By.id("horario-incidente");
    private By selectIdAreaResponsavelInicial = By.id("id-area-responsavel-inicial");
    private IntFunction<By> optionIdAreaResponsavelInicial = (index) -> By.id("id-area-responsavel-inicial-" + index);
    private By selectIdTipoIncidente = By.id("id-tipo-incidente");
    private IntFunction<By> optionTipoIncidente = (index) -> By.id("id-tipo-incidente-" + index);
    private By selectIdSubtipoIncidente = By.id("id-subtipo-incidente");
    private IntFunction<By> optionSubtipoIncidente = (index) -> By.id("id-subtipo-incidente-" + index);
    private By inputCriticidade = By.id("criticidade");
    private By selectIdFonteIncidente = By.id("id-fonte-incidente");
    private IntFunction<By> optionIdFonteIncidente = (index) -> By.id("id-fonte-incidente-" + index);
    private By textareaDescricaoIncidente = By.id("descricao-incidente");
    private By separatorSobreOLocal = By.name("SOBRE O LOCAL");
    private By separatorAtivosEnvolvidos = By.name("ATIVOS ENVOLVIDOS");
    private By inputSb = By.id("sb");
    private By inputKm = By.id("km");
    private By selectSubDivisao = By.id("sub-divisao");
    private IntFunction<By> optionSubDivisao = (index) -> By.id("option-subdivisao-" + index);
    private By selectComplexo = By.id("complexo");
    private IntFunction<By> optionComplexo = (index) -> By.id("option-complexo-" + index);
    private By selectCorredor = By.id("corredor");
    private IntFunction<By> optionCorredor = (index) -> By.id("option-corredor-" + index);
    private By selectOperacao = By.id("operacao");
    private IntFunction<By> optionOperacao = (index) -> By.id("operacao-" + index);
    private By separatorSobreOTrem = By.name("SOBRE O TREM");
    private By radioGroupTremEnvolvido = By.id("trem-envolvido");
    private By radioButtonTremEnvolvidoSim = By.id("trem-envolvido-sim");
    private By inputTremEnvolvidoSimInput = By.id("trem-envolvido-sim-input");
    private By radioButtonTremEnvolvidoNao = By.id("trem-envolvido-nao");
    private By inputTremEnvolvidoNaoInput = By.id("trem-envolvido-nao-input");
    private By inputPrefixoTrem = By.id("prefixo-trem");
    private By selectIdComposicaoTrem = By.id("id-composicao-trem");
    private By inputNumeroOs = By.id("numero-os");
    private By btnVoltar = By.id("back-button");
    private By tabEncerrados = By.id("tab-encerrados");
    private By radioButtonAtivosEnvolvidosSim = By.id("form-ativos-envolvidos-sim");
    private By radioButtonAtivosEnvolvidosNao = By.id("form-ativos-envolvidos-nao");
    private By btnAddVeiculo = By.id("button-add-veiculo");
    private By btnEditarSobreIncidente = By.id("edit-sobre-incidente");
    private By btnEditarSobreLocal = By.id("edit-sobre-local");
    private By btnEditarSobreTrem = By.id("edit-sobre-trem");
    private By btnEditarAtivosEnvolvidos = By.id("edit-ativos-envolvidos");

    private By inputAtivoEnvolvidoPosicao = By.id("input-table-cell-posicao-0");
    private By inputAtivoEnvolvidoNumero = By.id("input-table-cell-numero-0");
    private By selectAtivoEnvolvidoTipo = By.id("select-table-cell-tipo-0");
    private IntFunction<By> optionAtivoEnvolvidoTipo = (index) -> By.id("tipo-option-" + index);
    private By inputAtivoEnvolvidoModelo = By.id("input-table-cell-modelo-0");
    private By optionAtivoEnvolvidoModelo = By.id("modelo-option-0");
    private By inputAtivoEnvolvidoMercadoria = By.id("input-table-cell-mercadoria-0");
    private By optionAtivoEnvolvidoMercadoria = By.id("mercadoria-option-0");


    private By buttonDadosIniciaisFooterSalvarAbrirIncidente = By.id("dados-iniciais-footer-salvar-abrir-incidente");
    private By btnSalvarInvestigacao = By.id("dados-iniciais-footer-salvar");
    private By buttonDadosIniciaisFooterCancelar = By.id("dados-iniciais-footer-cancelar");
    private By btnAprovarInvestigacaoModal = By.id("dialog-button-yes");
    private By btnReabrirInvestigacao = By.id("dados-iniciais-footer-reabrir");
    private By buttonDadosIniciaisFooterAprovarInvestigacao = By.id("dados-iniciais-footer-aprovar-investigacao");
    private By btnReprovarInvestigação = By.id("dados-iniciais-footer-reprovar-investigacao");
    private By inputJustificativa = By.id("dialog-reprovar-investigacao-text-justificativa");
    private By inputJustificativaReabertura = By.id("dialog-reabrir-incidente-text-justificativa");
    private By btnConfirmarReprovaçãoModal = By.id("dialog-reprovar-investigacao-confirm");
    private By btnAnular = By.id("dados-iniciais-footer-anular");
    private By btnConfirmarReabertura = By.id("dialog-reabrir-incidente-confirm");
    private By buttonDadosIniciaisFooterSalvar = By.id("dados-iniciais-footer-salvar");
    private By buttonDadosIniciaisFooterSalvarAvancar = By.id("dados-iniciais-footer-salvar-avancar");
    private By buttonDadosIniciaisFooterSolicitarAnulacao = By.id("dados-iniciais-footer-solicitar-anulacao");
    private IntFunction<By> optionTipoComposicao = (index) -> By.id("option-id-composicao-trem-" + index);
    private By tbArquivos = By.id("download-files");

    /*
    Modal solicitar anulação
     */

    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By textareaDialogSolicitarAnulacaoIncidenteTextJustificativa = By.id("dialog-solicitar-anulacao-incidente-text-justificativa");
    private By sroFileUploadDialogSolicitarAnulacaoIncidenteUploadComponent = By.id("dialog-solicitar-anulacao-incidente-upload-component");
    private By inputInputFileUpload = By.id("input-file-upload");
    private By buttonBtnInputFileUpload = By.id("btn-input-file-upload");
    private By buttonBtnFileUploadSearch = By.id("btn-file-upload-search");
    private By ulFileUploadComponentListFiles = By.id("file-upload-component-list-files");
    private By buttonDialogSolicitarAnulacaoIncidenteButtonCancel = By.id("dialog-solicitar-anulacao-incidente-button-cancel");
    private By buttonDialogSolicitarAnulacaoIncidenteConfirm = By.id("dialog-solicitar-anulacao-incidente-confirm");

    public void validaQueEstaNaPaginaDeDadosIniciais() {
        expectElementVisible(separatorSobreOIncidente);
        expectElementVisible(separatorSobreOLocal);
    }

    public void preencherSobreOIncidente(Incidente incidente, boolean isEdicao) {
        scrollToElement(separatorSobreOIncidente);
        clearForce(inputDataIncidente);
        sendKeys(inputDataIncidente, incidente.getData());
        sendKeys(inputHorarioIncidente, incidente.getHora());

        if (!isEdicao) {
            incidente.setAreaResponsavelInicialValue(
                    selectOptionAndReturnValue(
                            selectIdAreaResponsavelInicial,
                            optionIdAreaResponsavelInicial,
                            incidente.getAreaResponsavelInicialIndex()
                    )
            );
        }

        incidente.setTipoValue(
                selectOptionAndReturnValue(
                        selectIdTipoIncidente,
                        optionTipoIncidente,
                        incidente.getTipoIndex()
                )
        );

        incidente.setSubTipoValue(
                selectOptionAndReturnValue(
                        selectIdSubtipoIncidente,
                        optionSubtipoIncidente,
                        incidente.getSubTipoIndex()
                )
        );

        incidente.setCriticidade(getValue(inputCriticidade));

        incidente.setFonteDoIncidenteValue(
                selectOptionAndReturnValue(
                        selectIdFonteIncidente,
                        optionIdFonteIncidente,
                        incidente.getFonteDoIncidenteIndex()
                )
        );

        sendKeysWithJavaScript(textareaDescricaoIncidente, incidente.getDescricaoDoIncidente());
        //insere aquivo
        uploadArquivo(2);
    }

    public void preencherSobreOLocal(Incidente incidente) {
        expectLoading();
        scrollToElement(separatorSobreOLocal);
        sendKeys(inputSb, incidente.getSecaoBloqueioSB());
        sendKeys(inputKm, incidente.getKm());

        incidente.setSubdivisaoValue(
                selectOptionAndReturnValue(
                        selectSubDivisao,
                        optionSubDivisao,
                        incidente.getSubdivisaoIndex()
                )
        );

        incidente.setComplexoValue(
                selectOptionAndReturnValue(
                        selectComplexo,
                        optionComplexo,
                        incidente.getComplexoIndex()
                )
        );

        incidente.setCorredorValue(
                selectOptionAndReturnValue(
                        selectCorredor,
                        optionCorredor,
                        incidente.getCorredorIndex()
                )
        );

        incidente.setOperacaoValue(
                selectOptionAndReturnValue(
                        selectOperacao,
                        optionOperacao,
                        incidente.getOperacaoIndex()
                )
        );
    }

    public void clicarBtnSalvarEAbrirIncidente() {
        clickAndHighlight(buttonDadosIniciaisFooterSalvarAbrirIncidente);
    }

    public void preencherSobreOTrem(Incidente incidente) {
        scrollToElement(separatorSobreOTrem);
        if (incidente.isExistePrefixoDoTrem()) {
            clickWithAction(radioButtonTremEnvolvidoSim);
            sendKeys(inputPrefixoTrem, incidente.getPrefixo());

            incidente.setTipoDeComposicaoValue(
                    selectOptionAndReturnValue(
                            selectIdComposicaoTrem,
                            optionTipoComposicao,
                            incidente.getTipoDeComposicaoIndex()
                    ));

            sendKeys(inputNumeroOs, incidente.getOS());
        } else {
            clickWithAction(radioButtonTremEnvolvidoNao);
        }
    }

    public void clicarBtnSalvarInvestigacao() {
        clickAndHighlight(btnSalvarInvestigacao);
    }

    public void clicarBtnAprovarModal() {
        clickAndHighlight(btnAprovarInvestigacaoModal);
    }

    public void validarQueInvestigacaoAprovada() {
        scrollToElement(btnReabrirInvestigacao);
        expectElementNotVisible(btnSalvarInvestigacao);
        expectElementVisibleWithoutHighlight(btnReabrirInvestigacao);
    }

    public void clicarBtnReprovarInvestigação() {
        clickAndHighlight(btnReprovarInvestigação);
    }

    public void preencherModalReprovação(String justificativa) {
        sendKeysWithJavaScript(inputJustificativa, justificativa);
    }

    public void clicarEmConfirmarModalReprovacao() {
        clickAndHighlight(btnConfirmarReprovaçãoModal);
    }

    public void validarQueInvestigacaoReprovada() {
        scrollToElement(btnAnular);
        expectElementNotVisible(btnSalvarInvestigacao);
        expectElementVisibleWithoutHighlight(btnAnular);
    }

    public void clicarBtnSolicitarAnulacao() {
        clickAndHighlight(buttonDadosIniciaisFooterSolicitarAnulacao);
    }

    public void preencherModalSolicitarAnulacao(String justificativa) {
        sendKeysWithJavaScript(textareaDialogSolicitarAnulacaoIncidenteTextJustificativa, justificativa);
        uploadArquivo(1);
    }

    public void clicarBtnConfirmarModalSolicitarAnulacao() {
        clickAndHighlight(buttonDialogSolicitarAnulacaoIncidenteConfirm);
    }

    public void clicarBtnAnularInvestigacao() {
        scrollToElement(btnAnular);
        clickAndHighlight(btnAnular);
    }

    public void clicarBtnAnularModal() {
        clickAndHighlight(btnAprovarInvestigacaoModal);
    }

    public void validarQueInvestigacaoFoiAnulada() {
        scrollToElement(btnReabrirInvestigacao);
        expectElementNotVisible(btnSalvarInvestigacao);
        expectElementVisibleWithoutHighlight(btnReabrirInvestigacao);
    }

    public void clicarBtnReabrirIncidente() {
        scrollToElement(btnReabrirInvestigacao);
        clickAndHighlight(btnReabrirInvestigacao);
    }

    public void preencherModalReabertura(String justificativa) {
        sendKeysWithJavaScript(inputJustificativaReabertura, justificativa);
    }

    public void clicarBtnConfirmarReabertura() {
        clickAndHighlight(btnConfirmarReabertura);
    }

    public void preencherSobreAtivosEnvolvidos(Incidente incidente, Veiculo veiculo, boolean isEdicao) {
        if (incidente.isAtivosForamEnvolvidos()) {
            clickWithAction(radioButtonAtivosEnvolvidosSim);

            if (!isEdicao) {
                clickAndHighlight(btnAddVeiculo);
            }

            waitTime(1000);
            sendKeys(inputAtivoEnvolvidoPosicao, veiculo.getPosicao());
            sendKeys(inputAtivoEnvolvidoNumero, veiculo.getNumero());

            veiculo.setTipoValue(
                    selectOptionAndReturnValue(
                            selectAtivoEnvolvidoTipo,
                            optionAtivoEnvolvidoTipo,
                            1
                    ));

            sendKeys(inputAtivoEnvolvidoModelo, veiculo.getModelo());
            expectElementVisible(optionAtivoEnvolvidoModelo);
            clickAndHighlight(optionAtivoEnvolvidoModelo);
            veiculo.setModelo(getValue(inputAtivoEnvolvidoModelo));

            waitTime(1000);
            sendKeys(inputAtivoEnvolvidoMercadoria, veiculo.getMercadoria());
            expectElementVisible(optionAtivoEnvolvidoMercadoria);
            clickAndHighlight(optionAtivoEnvolvidoMercadoria);
            veiculo.setMercadoria(getValue(inputAtivoEnvolvidoMercadoria));


        } else {
            clickWithAction(radioButtonAtivosEnvolvidosNao);
        }
    }

    public void validaSobreOIncidente(Incidente incidente, boolean isEdicao) {
        scrollToElement(separatorSobreOIncidente);
        Assert.assertEquals(getValue(inputDataIncidente), incidente.getData());
        Assert.assertEquals(getValue(inputHorarioIncidente), incidente.getHora());
        if (!isEdicao) {
            Assert.assertEquals(getText(selectIdAreaResponsavelInicial), incidente.getAreaResponsavelInicialValue());
        }
        Assert.assertEquals(getText(selectIdTipoIncidente), incidente.getTipoValue());
        Assert.assertEquals(getText(selectIdSubtipoIncidente), incidente.getSubTipoValue());
        Assert.assertEquals(getValue(inputCriticidade), incidente.getCriticidade());
        Assert.assertEquals(getText(selectIdFonteIncidente), incidente.getFonteDoIncidenteValue());
        Assert.assertEquals(getValue(textareaDescricaoIncidente), incidente.getDescricaoDoIncidente());
        if (isEdicao) {
            Assert.assertEquals(5, countChildElement(tbArquivos, "tr"));
        } else {
            Assert.assertEquals(3, countChildElement(tbArquivos, "tr"));
        }
    }

    public void validaSobreOLocal(Incidente incidente) {
        scrollToElement(separatorSobreOLocal);
        Assert.assertEquals(getValue(inputSb), String.valueOf(incidente.getSecaoBloqueioSB()));
        Assert.assertEquals(getValue(inputKm), incidente.getKm());
        Assert.assertEquals(getText(selectSubDivisao), incidente.getSubdivisaoValue());
        Assert.assertEquals(getText(selectComplexo), incidente.getComplexoValue());
        Assert.assertEquals(getText(selectCorredor), incidente.getCorredorValue());
        Assert.assertEquals(getText(selectOperacao), incidente.getOperacaoValue());
    }

    public void validaSobreOTrem(Incidente incidente) {
        scrollToElement(separatorSobreOTrem);
        if (incidente.isExistePrefixoDoTrem()) {
            Assert.assertTrue(isRadioChecked(radioButtonTremEnvolvidoSim));
            Assert.assertEquals(getValue(inputPrefixoTrem), incidente.getPrefixo());
            Assert.assertEquals(getText(selectIdComposicaoTrem), incidente.getTipoDeComposicaoValue());
            Assert.assertEquals(getValue(inputNumeroOs), incidente.getOS());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonTremEnvolvidoNao));
        }
    }

    public void validaSobreAtivosEnvolvidos(Incidente incidente, Veiculo veiculo) {
        if (incidente.isAtivosForamEnvolvidos()) {
            Assert.assertTrue(isRadioChecked(radioButtonAtivosEnvolvidosSim));
            Assert.assertEquals(getValue(inputAtivoEnvolvidoPosicao), String.valueOf(veiculo.getPosicao()));
            Assert.assertEquals(getValue(inputAtivoEnvolvidoNumero), String.valueOf(veiculo.getNumero()));
            Assert.assertEquals(getText(selectAtivoEnvolvidoTipo), String.valueOf(veiculo.getTipoValue()));
            Assert.assertEquals(getValue(inputAtivoEnvolvidoModelo), veiculo.getModelo());
            Assert.assertEquals(getValue(inputAtivoEnvolvidoMercadoria), veiculo.getMercadoria());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonAtivosEnvolvidosNao));
        }
    }

    public void validaSobreOIncidenteReadOnly(Incidente incidente) {
        scrollToElement(separatorSobreOIncidente);
        expectElementVisible(btnEditarSobreIncidente);
        Assert.assertEquals(getValue(inputDataIncidente), incidente.getData());
        Assert.assertEquals(getValue(inputHorarioIncidente), incidente.getHora());
        Assert.assertEquals(getText(selectIdAreaResponsavelInicial), incidente.getAreaResponsavelInicialValue());
        Assert.assertEquals(getText(selectIdTipoIncidente), incidente.getTipoValue());
        Assert.assertEquals(getText(selectIdSubtipoIncidente), incidente.getSubTipoValue());
        Assert.assertEquals(getValue(inputCriticidade), incidente.getCriticidade());
        Assert.assertEquals(getText(selectIdFonteIncidente), incidente.getFonteDoIncidenteValue());
        Assert.assertEquals(getValue(textareaDescricaoIncidente), incidente.getDescricaoDoIncidente());
        Assert.assertEquals(3, countChildElement(tbArquivos, "tr"));
    }

    public void validaSobreOLocalReadOnly(Incidente incidente) {
        scrollToElement(separatorSobreOLocal);
        expectElementVisible(btnEditarSobreLocal);
        Assert.assertEquals(getValue(inputSb), String.valueOf(incidente.getSecaoBloqueioSB()));
        Assert.assertEquals(getValue(inputKm), incidente.getKm());
        Assert.assertEquals(getText(selectSubDivisao), incidente.getSubdivisaoValue());
        Assert.assertEquals(getText(selectComplexo), incidente.getComplexoValue());
        Assert.assertEquals(getText(selectCorredor), incidente.getCorredorValue());
        Assert.assertEquals(getText(selectOperacao).toUpperCase(), incidente.getOperacaoValue().toUpperCase());
    }

    public void validaSobreOTremReadOnly(Incidente incidente) {
        scrollToElement(separatorSobreOTrem);
        expectElementVisible(btnEditarSobreTrem);
        if (incidente.isExistePrefixoDoTrem()) {
            Assert.assertTrue(isRadioChecked(radioButtonTremEnvolvidoSim));
            Assert.assertEquals(getValue(inputPrefixoTrem), incidente.getPrefixo());
            Assert.assertEquals(getText(selectIdComposicaoTrem), incidente.getTipoDeComposicaoValue());
            Assert.assertEquals(getValue(inputNumeroOs), incidente.getOS());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonTremEnvolvidoNao));
        }
    }

    public void validaSobreAtivosEnvolvidosReadOnly(Incidente incidente, Veiculo veiculo) {
        expectElementVisible(btnEditarAtivosEnvolvidos);
        if (incidente.isAtivosForamEnvolvidos()) {
            Assert.assertTrue(isRadioChecked(radioButtonAtivosEnvolvidosSim));
            Assert.assertEquals(getText(inputAtivoEnvolvidoPosicao), String.valueOf(veiculo.getPosicao()));
            Assert.assertEquals(getText(inputAtivoEnvolvidoNumero), String.valueOf(veiculo.getNumero()));
            Assert.assertEquals(getText(selectAtivoEnvolvidoTipo), String.valueOf(veiculo.getTipoValue()));
            Assert.assertEquals(getText(inputAtivoEnvolvidoModelo), veiculo.getModelo());
            Assert.assertEquals(getText(inputAtivoEnvolvidoMercadoria).toUpperCase(), veiculo.getMercadoria());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonAtivosEnvolvidosNao));
        }
    }

    public void habilitarEdicaoDaPaginaDeIncidente() {
        scrollToElement(separatorSobreOIncidente);
        clickAndHighlight(btnEditarSobreIncidente);

        scrollToElement(separatorSobreOLocal);
        clickAndHighlight(btnEditarSobreLocal);

        scrollToElement(separatorSobreOTrem);
        clickAndHighlight(btnEditarSobreTrem);

        scrollToElement(separatorAtivosEnvolvidos);
        clickAndHighlight(btnEditarAtivosEnvolvidos);
    }

    public void clicarBtnAprovarInvestigacao() {
        clickAndHighlight(buttonDadosIniciaisFooterAprovarInvestigacao);
    }

    public void validaSobreOIncidenteReadOnlyFicha(Incidente incidente) {
        scrollToElement(separatorSobreOIncidente);
        Assert.assertEquals(getValue(inputDataIncidente), incidente.getData());
        Assert.assertEquals(getValue(inputHorarioIncidente), incidente.getHora());
        Assert.assertEquals(getText(selectIdAreaResponsavelInicial), incidente.getAreaResponsavelInicialValue());
        Assert.assertEquals(getText(selectIdTipoIncidente), incidente.getTipoValue());
        Assert.assertEquals(getText(selectIdSubtipoIncidente), incidente.getSubTipoValue());
        Assert.assertEquals(getValue(inputCriticidade), incidente.getCriticidade());
        Assert.assertEquals(getText(selectIdFonteIncidente), incidente.getFonteDoIncidenteValue());
        Assert.assertEquals(getValue(textareaDescricaoIncidente), incidente.getDescricaoDoIncidente());
        Assert.assertEquals(2, countChildElement(tbArquivos, "tr"));
    }

    public void validaSobreOLocalReadOnlyFicha(Incidente incidente) {
        scrollToElement(separatorSobreOLocal);
        Assert.assertEquals(getValue(inputSb), String.valueOf(incidente.getSecaoBloqueioSB()));
        Assert.assertEquals(getValue(inputKm), incidente.getKm());
        Assert.assertEquals(getText(selectSubDivisao), incidente.getSubdivisaoValue());
        Assert.assertEquals(getText(selectComplexo), incidente.getComplexoValue());
        Assert.assertEquals(getText(selectCorredor), incidente.getCorredorValue());
        Assert.assertEquals(getText(selectOperacao).toUpperCase(), incidente.getOperacaoValue());
    }

    public void validaSobreOTremReadOnlyFicha(Incidente incidente) {
        scrollToElement(separatorSobreOTrem);
        if (incidente.isExistePrefixoDoTrem()) {
            Assert.assertTrue(isRadioChecked(radioButtonTremEnvolvidoSim));
            Assert.assertEquals(getValue(inputPrefixoTrem), incidente.getPrefixo());
            Assert.assertEquals(getText(selectIdComposicaoTrem), incidente.getTipoDeComposicaoValue());
            Assert.assertEquals(getValue(inputNumeroOs), incidente.getOS());
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonTremEnvolvidoNao));
        }
    }

    public void validaSobreAtivosEnvolvidosReadOnlyFicha(Incidente incidente, Veiculo veiculo) {
        if (incidente.isAtivosForamEnvolvidos()) {
            Assert.assertTrue(isRadioChecked(radioButtonAtivosEnvolvidosSim));
            Assert.assertEquals(getText(inputAtivoEnvolvidoPosicao), String.valueOf(veiculo.getPosicao()));
            Assert.assertEquals(getText(inputAtivoEnvolvidoNumero), String.valueOf(veiculo.getNumero()));
            Assert.assertEquals(getText(selectAtivoEnvolvidoTipo), String.valueOf(veiculo.getTipoValue()));
            Assert.assertEquals(getText(inputAtivoEnvolvidoModelo), veiculo.getModelo());
            Assert.assertEquals(getText(inputAtivoEnvolvidoMercadoria), "--");
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonAtivosEnvolvidosNao));
        }
    }
}
