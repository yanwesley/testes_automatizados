package br.com.pom.sro.incidentes;

import br.com.api.model.incidente.Incidente;
import br.com.api.model.sindicancia.AnaliseDeCausa;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class AnaliseDeCausaIncidentePage extends GeralPage {

    private By tdTdPrimeiroPorquePrimaria0 = By.id("td-primeiro-porque-0");
    private By tdTdSegundoPorquePrimaria0 = By.id("td-segundo-porque-0");
    private By tdTdTerceiroPorquePrimaria0 = By.id("td-terceiro-porque-0");
    private By tdTdQuartoPorquePrimaria0 = By.id("td-quarto-porque-0");
    private By tdTdQuintaPorquePrimaria0 = By.id("td-quinta-porque-0");
    private By textareaPorque1 = By.id("porque-1");
    private By textareaPorque2 = By.id("porque-2");
    private By textareaPorque3 = By.id("porque-3");
    private By textareaPorque4 = By.id("porque-4");
    private By textareaPorque5 = By.id("porque-5");
    private By buttonDialogCancelarBtn = By.id("dialog-incluir-analise-causa-incidente-cancelar-btn");
    private By buttonDialogSalvarBtn = By.id("dialog-incluir-analise-causa-incidente-salvar-btn");
    private By btnConfirmarExclusao = By.id("dialog-button-yes");
    private By btnEditarAnaliseDeCausa = By.id("button-editar-primaria-0");
    private By btnSalvar = By.id("button-salvar");

    //Incidente
    private By textareaFatosConstatados = By.id("fatosConstatados");
    private By inputFatoOcorrido = By.id("fatoOcorrido");
    private By btnAddAnaliseDeCausas = By.id("button-add-human-resource");
    private By btnSalvarECriarPlanoAcao = By.id("button-salvar-criar-plano-acao");
    private By btnConfirmarSalvarAnaliseCausa = By.id("dialog-button-yes");
    private By textareaCausaIdentificada = By.id("causa-identificada");

    //Plano de ação
    private By tipoDeIncidentePlanoAcao = By.id("resumo-causa-plano-acao--tipo-incidente");
    private By fatoOcorridoPlanoAcao = By.id("resumo-causa-plano-acao--fato-ocorrido");
    private By causaIdentificadaPlanoAcao = By.id("resumo-causa-plano-acao--causa-identificada");
    private By origemPlanoAcao = By.id("resumo-causa-plano-acao-causa-origem");
    private By solicitantePlanoAcao = By.id("resumo-causa-plano-acao-causa-solicitante");
    private By operacaoPlanoAcao = By.id("resumo-causa-plano-acao-causa-operacao");
    private By complexoPlanoAcao = By.id("resumo-causa-plano-acao-causa-complexo");

    public void preenchoAnaliseDeCausa(AnaliseDeCausa analiseDeCausa) {
        sendKeysWithJavaScript(textareaPorque1, analiseDeCausa.getPrimeiroPorque());
        sendKeysWithJavaScript(textareaPorque2, analiseDeCausa.getSegundoPorque());
        sendKeysWithJavaScript(textareaPorque3, analiseDeCausa.getTerceiroPorque());
        sendKeysWithJavaScript(textareaPorque4, analiseDeCausa.getQuartoPorque());
        sendKeysWithJavaScript(textareaPorque5, analiseDeCausa.getQuintoPorque());
    }

    public void cliqueiNoBotaoSalvar() {
        clickAndHighlight(buttonDialogSalvarBtn);
    }

    public void validoOPreenchimentoDosPorques(AnaliseDeCausa analiseDeCausa) {
        scrollToElement(tdTdPrimeiroPorquePrimaria0);
        Assert.assertEquals(getText(tdTdPrimeiroPorquePrimaria0), analiseDeCausa.getPrimeiroPorque());
        scrollToElement(tdTdSegundoPorquePrimaria0);
        Assert.assertEquals(getText(tdTdSegundoPorquePrimaria0), analiseDeCausa.getSegundoPorque());
        scrollToElement(tdTdTerceiroPorquePrimaria0);
        Assert.assertEquals(getText(tdTdTerceiroPorquePrimaria0), analiseDeCausa.getTerceiroPorque());
        scrollToElement(tdTdQuartoPorquePrimaria0);
        Assert.assertEquals(getText(tdTdQuartoPorquePrimaria0), analiseDeCausa.getQuartoPorque());
        scrollToElement(tdTdQuintaPorquePrimaria0);
        Assert.assertEquals(getText(tdTdQuintaPorquePrimaria0), analiseDeCausa.getQuintoPorque());
    }

    public void preencheFatosConstatados(AnaliseDeCausa analiseDeCausa) {
        waitTime(1000);
        sendKeysWithJavaScript(textareaFatosConstatados, analiseDeCausa.getFatosConstatados());
    }

    public void preencheFatoOcorrido(AnaliseDeCausa analiseDeCausa) {
        sendKeysWithJavaScript(inputFatoOcorrido, analiseDeCausa.getFatoOcorrido());
    }

    public void clicarBtnAddAnaliseDeCausa() {
        clickAndHighlight(btnAddAnaliseDeCausas);
    }

    public void clicarBtnSalvarCriarPlanoAcao() {
        scrollToElement(btnSalvarECriarPlanoAcao);
        clickAndHighlight(btnSalvarECriarPlanoAcao);
    }

    public void clicarBtnConfirmarSalvarCriarPlanoAcao() {
        clickAndHighlight(btnConfirmarSalvarAnaliseCausa);
    }

    public void preenchoACausaIdentificada(AnaliseDeCausa analiseDeCausa) {
        sendKeysWithJavaScript(textareaCausaIdentificada, analiseDeCausa.getCausaIdentificada());
    }

    public void validoPlanoAvulso(Incidente incidente, AnaliseDeCausa analiseDeCausaIncidente) {
        Assert.assertTrue(getText(tipoDeIncidentePlanoAcao).contains(incidente.getTipoValue()));
        Assert.assertTrue(getText(fatoOcorridoPlanoAcao).contains(analiseDeCausaIncidente.getFatoOcorrido()));
        Assert.assertTrue(getText(causaIdentificadaPlanoAcao).contains(analiseDeCausaIncidente.getCausaIdentificada()));
        Assert.assertTrue(getText(origemPlanoAcao).contains("Incidente"));
        Assert.assertTrue(getText(solicitantePlanoAcao).contains(incidente.getNomeUsuarioCriacao()));
        Assert.assertTrue(getText(operacaoPlanoAcao).toUpperCase().contains(incidente.getOperacaoValue()));
        Assert.assertTrue(getText(complexoPlanoAcao).contains(incidente.getComplexoValue()));
    }

    public void clicarBtnEditarAnaliseDeCausa() {
        clickAndHighlight(btnEditarAnaliseDeCausa);
    }

    public void clicarBtnSalvarDaPagina() {
        clickAndHighlight(btnSalvar);
    }
}