package br.com.pom.sro.incidentes;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.api.model.incidente.Incidente;
import br.com.api.model.sindicancia.AnaliseDeCausa;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

public class PlanoDeAcaoIncidentePage extends GeralPage {

    private By fatoOcorrido = By.id("plano-acao--fato-ocorrido");
    private By descricaoAcao = By.id("-table-acoes-descricao-acao-0");
    private By prazoAcao = By.id("-table-acoes-prazo-0");
    private By labelResponsavelAcao = By.id("-table-acoes-executor-0");
    private By statusAcao = By.id("-table-acoes-status-0");
    private By causaIdentificadaPlanoAcao = By.id("lista-acoes-incidente--titulo-causa");
    private By btnVisualizarAcao = By.id("-button-visualizar-acao-0");

    private By origemDetalheAcao = By.id("origemAcao");
    private By solicitanteDetalheAcao = By.id("input-amv-solicitante");
    private By causaDetalheAcao = By.id("focoCausaDescricao");
    private By impactoDetalheAcao = By.id("impacto");
    private By tipoDetalheAcao = By.id("tipoAcao");
    private By numeroPlanoDetalheAcao = By.id("input-idPlanoAcao");
    private By prazoDetalheAcao = By.id("input-data");
    private By acaoPublicaSim = By.id("publica-0");
    private By acaoPublicaNao = By.id("publica-1");
    private By descricaoDetalheAcao = By.id("descricaoAcao");

    private By operacaoDetalheResponsavel = By.id("table-responsaveis-cell-operacao-0");
    private By complexoDetalheResponsavel = By.id("table-responsaveis-cell-complexo-0");
    private By areaDetalheResponsavel = By.id("table-responsaveis-cell-area-0");
    private By gerenteDetalheResponsavel = By.id("table-responsaveis-cell-gerente-0");
    private By detalheResponsavel = By.id("table-responsaveis-cell-responsavel-0");
    private By buttonPlanoAcaoFooterSalvarEncerrar = By.id("plano-acao-footer-salvar-encerrar");

    public void validaPlanoDeAcaoCriadoNaAnaliseDeCausa(AnaliseDeCausa analiseDeCausa, Acao acao, ResponsavelAcao responsavelAcao) {
        Assert.assertTrue(getText(causaIdentificadaPlanoAcao).contains(analiseDeCausa.getCausaIdentificada().toUpperCase()));
        Assert.assertTrue(getText(fatoOcorrido).contains(analiseDeCausa.getFatoOcorrido()));
        Assert.assertTrue(getText(descricaoAcao).contains(acao.getDescritivoDaAcao()));
        Assert.assertTrue(getText(prazoAcao).contains(acao.getQualEOPrazoParaExecucao()));
        Assert.assertTrue(getText(labelResponsavelAcao).contains(responsavelAcao.getResponsavel()));
        Assert.assertTrue(getText(labelResponsavelAcao).contains(responsavelAcao.getGerente()));
        Assert.assertTrue(getText(labelResponsavelAcao).contains(responsavelAcao.getArea()));
        Assert.assertTrue(getText(statusAcao).contains("Aguardando aceite"));
    }

    public void clicarNaLupaDaAcao() {
        clickAndHighlight(btnVisualizarAcao);
    }


    public void validaAAcaoInseridaNoIncidenteNoDialog(Incidente incidente, Acao acao, ResponsavelAcao responsavelAcao) {
        Assert.assertEquals(getValue(origemDetalheAcao), "Incidente");
        Assert.assertEquals(getValue(solicitanteDetalheAcao), incidente.getNomeUsuarioCriacao());
        Assert.assertEquals(getValue(causaDetalheAcao), acao.getFocoCausaDaAcaoValue());
        Assert.assertEquals(getValue(impactoDetalheAcao), acao.getQualOImpactoDaAcaoValue());
        Assert.assertEquals(getValue(tipoDetalheAcao), acao.getQualOTipoDaAcaoValue());
        Assert.assertEquals(getValue(numeroPlanoDetalheAcao), String.valueOf(acao.getIdPlanoAcao()));
        Assert.assertEquals(getValue(prazoDetalheAcao), acao.getQualEOPrazoParaExecucao());
        if (acao.isAcaoPublica()) {
            Assert.assertTrue(isRadioChecked(acaoPublicaSim));
        } else {
            Assert.assertTrue(isRadioChecked(acaoPublicaNao));
        }
        Assert.assertEquals(getValue(descricaoDetalheAcao), acao.getDescritivoDaAcao());
        Assert.assertEquals(getText(operacaoDetalheResponsavel), responsavelAcao.getQualEAOperacaoValue());
        Assert.assertEquals(getText(complexoDetalheResponsavel), responsavelAcao.getQualEOComplexoValue());
        Assert.assertEquals(getText(areaDetalheResponsavel), responsavelAcao.getArea());
        Assert.assertEquals(getText(gerenteDetalheResponsavel), responsavelAcao.getGerente());
        Assert.assertEquals(getText(detalheResponsavel), responsavelAcao.getResponsavel());
    }

    public void clicarBtnSalvarEEncerrar() {
        clickAndHighlight(buttonPlanoAcaoFooterSalvarEncerrar);
    }
}