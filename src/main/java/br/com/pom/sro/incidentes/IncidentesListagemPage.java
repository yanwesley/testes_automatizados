package br.com.pom.sro.incidentes;

import br.com.api.model.incidente.Incidente;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class IncidentesListagemPage extends GeralPage {

    private IntFunction<By> btnEditarIncidente = (index) -> By.id("button-edit-incidente-" + index);
    private IntFunction<By> cellID = (index) -> By.id("table-cell-id-incidente-" + index);
    private IntFunction<By> cellStatus = (index) -> By.id("table-cell-status-incidente-" + index);
    private IntFunction<By> btnVisualizarIncidente = (index) -> By.id("button-visualizar-incidente-" + index);
    private IntFunction<By> labelCellTipoAcidente = (index) -> By.id("table-cell-descricao-tipo-incidente-" + index);
    private IntFunction<By> labelCellDataSinistro = (index) -> By.id("table-header-data-criacao-incidente-" + index);
    private IntFunction<By> tdTableCellDescricaoAreaResponsavelIncidente = (index) -> By.id("table-cell-descricao-area-responsavel-incidente-" + index);

    private By tdTableCellDaysIndicator0 = By.id("table-cell-daysIndicator-0");
    private By tdTableCellIdIncidente0 = By.id("table-cell-id-incidente-0");
    private By tdTableHeaderDataCriacaoIncidente0 = By.id("table-header-data-criacao-incidente-0");
    private By tdTableCellDescricaoAreaResponsavelIncidente0 = By.id("table-cell-descricao-area-responsavel-incidente-0");
    private By tdTableCellLocal0 = By.id("table-cell-local-0");
    private By tdTableCellDescricaoTipoIncidente0 = By.id("table-cell-descricao-tipo-incidente-0");
    private By tdTableCellCriticidadeIncidente0 = By.id("table-cell-criticidade-incidente-0");
    private By tdTableCellDiasEmAbertoIncidente0 = By.id("table-cell-dias-em-aberto-incidente-0");
    private By tdTableCellStatusIncidente0 = By.id("table-cell-status-incidente-0");
    private By tdTableCellActions0 = By.id("table-cell-actions-0");
    private By labelTitulo = By.tagName("sro-default-title");
    private By tableListIncidentes = By.cssSelector("body > sro-root > sro-shell > div > sro-tabs-incidentes > sro-table-incidente > div > table");
    private By btnIncluirNovoIncidente = By.id("incluir-novo-incidente");
    private By btnVoltar = By.id("back-button");

    private By tabIncidentesCriados = By.id("tab-criados");
    private By tabInvestigacao = By.id("tab-investigacao");
    private By tabEncerrados = By.id("tab-encerrados");
    private By inputSearchIncidente = By.id("input-buscar-texto-incidentes");
    private By btnCleanSearch = By.id("button-clear-search-incidente");
    private By btnFiltrar = By.id("button-filter-incidentes");
    private By btnExportar = By.id("button-export-incidentes");

    private By headerId = By.id("table-header-id-incidente");
    private By headerDataHora = By.id("table-header-daysIndicator");
    private By headerSB = By.id("table-header-local");
    private By headerTipoIncidente = By.id("table-header-descricao-tipo-incidente");
    private By headerOrigem = By.id("table-header-descricao-fonte-incidente");
    private By headerAreaResponsavel = By.id("table-header-descricao-area-responsavel-incidente");
    private By headerCriticidade = By.id("table-header-criticidade-incidente");
    private By headerDiasAbertos = By.id("table-header-dias-em-aberto-incidente");
    private By headerStatus = By.id("table-header-status-incidente");
    private By msgNenhumResultadoEncontrado = By.id("nenhum-incidente-encontrado");
    private By inputDataInicio = By.id("input-advanced-filter-date-since");
    private By inputDataFim = By.id("advanced-filter-date-until");
    private By btnAplicarFiltroData = By.id("button-advanced-filter-apply");
    private By btnClearFiltrosData = By.id("button-chip-clear-filter");


    public void validaExibicaoDaPagina() {
        expectElementVisible(labelTitulo);
        expectElementVisible(tableListIncidentes);
        expectElementVisible(btnIncluirNovoIncidente);
    }

    public void ordenarIdDecrescente() {
        clickAndHighlight(headerId);
        expectLoading();
        clickAndHighlight(headerId);
        waitTime();
    }

    public void clicarBtnEditarListagem(int linha) {
        clickAndHighlight(btnEditarIncidente.apply(linha - 1));
    }

    public void clicarBtnIncluirNovoIncidente() {
        clickAndHighlight(btnIncluirNovoIncidente);
    }

    public void clicarBtnVoltarParaListagem() {
        clickAndHighlight(btnVoltar);
    }

    public void clicarTabEncerrados() {
        clickAndHighlight(tabEncerrados);
    }

    public void valideiStatusEncerrado(Incidente incidente, int linha) {
        linha--;
        Assert.assertEquals(getText(cellID.apply(linha)), incidente.getIdIncidente().toString());
        Assert.assertEquals(getText(cellStatus.apply(linha)), "Encerrado");
    }

    public void valideiStatusReprovado(Incidente incidente, int linha) {
        linha--;
        Assert.assertEquals(getText(cellID.apply(linha)), incidente.getIdIncidente().toString());
        Assert.assertEquals(getText(cellStatus.apply(linha)), "Reprovado");
    }

    public void validaExibicaoDoBotaoNovoIncidente() {
        expectElementVisible(btnIncluirNovoIncidente);
    }

    public void validaExibicaoDosCamposDoHeaderDaLista() {
        expectElementVisible(btnIncluirNovoIncidente);
        expectElementVisible(tabIncidentesCriados);
        expectElementVisible(tabInvestigacao);
        expectElementVisible(tabEncerrados);
        expectElementVisible(inputSearchIncidente);
        expectElementVisible(btnFiltrar);
        expectElementVisible(btnExportar);
    }

    public void validaExibicaoDosCamposDaTabelaDeIncidentes() {
        expectElementVisible(headerId);
        expectElementVisible(headerDataHora);
        expectElementVisible(headerAreaResponsavel);
        expectElementVisible(headerSB);
        expectElementVisible(headerTipoIncidente);
        expectElementVisible(headerCriticidade);
        expectElementVisible(headerDiasAbertos);
        expectElementVisible(headerStatus);
    }

    public void filtrarPorTexto(String textoBusca) {
        clearForce(inputSearchIncidente);
        sendKeys(inputSearchIncidente, textoBusca);
    }

    public void validaResultadoDaBuscaPorTexto(String textoBusca) {
        Assert.assertTrue(expectText(labelCellTipoAcidente.apply(0), textoBusca), "Texto deve ser :" + textoBusca);
    }

    public void validaExibicaoDaMensagemSemResultadoDaBusca(String msgSemResultado) {
        Assert.assertEquals(getText(msgNenhumResultadoEncontrado), msgSemResultado);
    }

    public void clicarBtnFiltrar() {
        clickAndHighlight(btnFiltrar);
    }

    public void filtrarPorDataInicial(String dataInicial) {
        expectElementVisibleWithoutHighlight(inputDataInicio);
        clearForce(inputDataInicio);
        sendKeys(inputDataInicio, dataInicial);
    }

    public void clicarBtnAplicarFiltro() {
        clickAndHighlight(btnAplicarFiltroData);
    }

    public void validaResultadoDaBuscaPorDataInicial(String dataInicial) {
        clickAndHighlight(btnCleanSearch);
        expectLoading();
        Assert.assertTrue(data1MenorQueData2(dataInicial, getText(labelCellDataSinistro.apply(0))),
                dataInicial + "not equal " + getText(labelCellDataSinistro.apply(0)));
    }

    public void limparCampoDataInicial() {
        clearForce(inputDataInicio);
    }

    public void filtrarPorDataFinal(String dataFinal) {
        expectElementVisibleWithoutHighlight(inputDataFim);
        clearForce(inputDataFim);
        sendKeys(inputDataFim, dataFinal);
    }

    public void validaResultadoDaBuscaPorDataFinal(String dataFinal) {
        clickAndHighlight(headerDataHora);
        expectLoading();
        Assert.assertFalse(data1MenorQueData2(dataFinal, getText(labelCellDataSinistro.apply(0))),
                dataFinal + "not equal " + getText(labelCellDataSinistro.apply(0)));
    }

    public void clicarTabInvestigacao() {
        clickAndHighlight(tabInvestigacao);
    }

    public void clicarTabIncidentesCriados() {
        clickAndHighlight(tabIncidentesCriados);
    }

    public void limparFiltrosDeData() {
        clickAndHighlight(btnClearFiltrosData);
    }

    public void validaStatusDoIncidente(Incidente incidente, String status, int linha) {
        linha--;
        Assert.assertEquals(getText(cellID.apply(linha)), incidente.getIdIncidente().toString());
        Assert.assertEquals(getText(cellStatus.apply(linha)), status);
    }


    public void valideiStatusAnulado(Incidente incidente, int linha) {
        linha--;
        Assert.assertEquals(getText(cellID.apply(linha)), incidente.getIdIncidente().toString());
        Assert.assertEquals(getText(cellStatus.apply(linha)), "Anulado");
    }

    public void clicarNaLupaListagem(int linha) {
        linha--;
        clickWithAction(btnVisualizarIncidente.apply(linha));
    }

    public void valideiStatusReaberto(Incidente incidente, int linha) {
        linha--;
        Assert.assertEquals(getText(cellID.apply(linha)), incidente.getIdIncidente().toString());
        Assert.assertEquals(getText(cellStatus.apply(linha)), "Aberto");
    }

    public void validarAreaResponsavelAlterada(Incidente incidente) {
        Assert.assertEquals(getText(tdTableCellDescricaoAreaResponsavelIncidente.apply(0)), incidente.getAreaResponsavelValue());
    }

    public void clicarBtnVisualizarListagem(int linha) {

    }
}

