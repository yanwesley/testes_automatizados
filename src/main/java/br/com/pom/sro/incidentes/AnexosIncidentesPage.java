package br.com.pom.sro.incidentes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.Function;

public class AnexosIncidentesPage extends GeralPage {

    Function<Integer, By> cellBtnEditarArquivo = (Integer index) -> By.id("table-anexos-incidentes-btn-editar-" + index);
    Function<Integer, By> cellBtnExcluirArquivo = (Integer index) -> By.id("table-anexos-incidentes-btn-excluir-" + index);
    Function<Integer, By> cellComentarioAnexo = (Integer index) -> By.id("table-anexos-incidentes-observacao-" + index);
    private By tableArquivosInseridos = By.cssSelector("sro-anexos-incidente > div > div > table > tbody");
    private By separatorAnexosDaArea = By.name("ANEXOS DA ÁREA");
    //    private By btnIncluirAnexoModal = By.id("btn-input-file-upload");
    private By btnIncluirAnexo = By.id("anexos-incidente-btn");
    private By btnConfirmarExclusaoArquivo = By.id("dialog-button-yes");
    private By textAreaComentario = By.id("dialog-anexos-incidente-observacao-textarea");
    private By btnSalvar = By.id("dialog-anexos-incidente-salvar-btn");


    public void clicarBtnIncluirAnexos() {
        clickAndHighlight(btnIncluirAnexo);
    }

//    public void clicarBtnIncluirAnexosModal() {
//        clickAndHighlight(btnIncluirAnexoModal);
//    }

    public void validaQueEstaNaPaginaDeAnexos() {
        expectElementVisible(separatorAnexosDaArea);
    }

    public void clicarBtnEditarAnexo() {
        clickAndHighlight(cellBtnEditarArquivo.apply(0));
    }

    public void excluirArquivos(int qtdArquivosExcluidos) {
        if (countChildElement(tableArquivosInseridos, "tr") >= qtdArquivosExcluidos) {
            for (int i = 0; i < qtdArquivosExcluidos; i++) {
                clickAndHighlight(cellBtnExcluirArquivo.apply(0));
                clickAndHighlight(btnConfirmarExclusaoArquivo);
                validaMensagemSnackBar("Anexo excluído com sucesso");
            }
        }
    }

    public void validaComentarioEditado(String comentarioEditado) {
        Assert.assertEquals(getText(cellComentarioAnexo.apply(0)), comentarioEditado);

    }

    public void editaComentarioDoAnexo(String comentarioEditado) {
        sendKeysWithJavaScript(textAreaComentario, comentarioEditado);
    }

    public void validaArquivoFoiInserido(int qtdArquivosAdicionados) {
        waitTime(1000);
        Assert.assertEquals(qtdArquivosAdicionados, countChildElement(tableArquivosInseridos, "tr"));
    }

    public void insereArquivosComComentario(int qtdArquivo, String comentario) {
        for (int i = 0; i < qtdArquivo; i++) {
            if (i > 0) {
                expectElementClickable(btnIncluirAnexo);
                clicarBtnIncluirAnexos();
            }
            insereArquivo(1);
            insereComentario(comentario);
            clicarBtnSalvar();
            validaMensagemSnackBar("A informação foi registrada com sucesso!");
        }
    }

    public void insereArquivo(int qtdArquivo) {
        uploadArquivo(qtdArquivo);
    }

    public void insereComentario(String comentario) {
        sendKeysWithJavaScript(textAreaComentario, comentario);
    }

    public void clicarBtnSalvar() {
        clickAndHighlight(btnSalvar);
    }
}
