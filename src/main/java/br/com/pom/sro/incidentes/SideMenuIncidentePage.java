package br.com.pom.sro.incidentes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class SideMenuIncidentePage extends GeralPage {

    private By aSideMenuDadosiniciais = By.id("side-menu-dadosiniciais");
    private By aSideMenuAnalisecausas = By.id("side-menu-analisecausas");
    private By aSideMenuArearesponsavel = By.id("side-menu-arearesponsavel");
    private By aSideMenuPlanoacao = By.id("side-menu-planoacao");
    private By aSideMenuAnexos = By.id("side-menu-anexos");
    private By aSideMenuHistorico = By.id("side-menu-historico");

    public void clicarMenuAnexos() {
        clickAndHighlight(aSideMenuAnexos);
    }

    public void clicarMenuDadosIniciais() {
        clickAndHighlight(aSideMenuDadosiniciais);
    }

    public void clicarMenuHistorico() {
        clickAndHighlight(aSideMenuHistorico);
    }

    public void clicarMenuAreaResponsável() {
        clickAndHighlight(aSideMenuArearesponsavel);
    }

    public void clicarMenuPlanoAcao() {
        clickAndHighlight(aSideMenuPlanoacao);
    }

    public void clicarMenuAnaliseCausas() {
        clickAndHighlight(aSideMenuAnalisecausas);
    }
}