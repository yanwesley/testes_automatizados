package br.com.pom.sro.incidentes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class HistoricoIncidentePage extends GeralPage {


    /*
    Box solicitação anulação
     */
    private By divSolicitacaoAnulacao0SolicitacaoDataSolicitacao = By.id("solicitacao-anulacao-0-solicitacao-data-solicitacao");
    private By divSolicitacaoAnulacao0SolicitacaoNomeSolicitacao = By.id("solicitacao-anulacao-0-solicitacao-nome-solicitacao");
    private By divSolicitacaoAnulacao0SolicitacaoMatriculaSolicitacao = By.id("solicitacao-anulacao-0-solicitacao-matricula-solicitacao");
    private By divSolicitacaoAnulacao0SolicitacaoJustificativa = By.id("solicitacao-anulacao-0-solicitacao-justificativa");
    private By divSolicitacaoAnulacao0SolicitacaoFiles = By.id("solicitacao-anulacao-0-solicitacao-files");
    private By tableDownloadFiles = By.id("download-files");
    private By thDownloadFilesNameHeader = By.id("download-files-name-header");
    private IntFunction<By> tdDownloadFilesName = (index) -> By.id("download-files-name-" + index);
    private IntFunction<By> tdDownloadFilesCellActions = (index) -> By.id("download-files-cell-actions-" + index);
    private IntFunction<By> aDownloadFilesBtnDownload = (index) -> By.id("download-files-btn-download-" + index);

    private By buttonHistoricoIncidenteFooterAprovarAnulacao = By.id("button-aprovar-anulacao");
    private By buttonHistoricoIncidenteFooterRecusarAnulacao = By.id("button-reprovar-anulacao");

    /*
    modal aprovar anulação
     */

    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By textareaDialogReabrirIncidenteTextJustificativa = By.id("dialog-reabrir-incidente-text-justificativa");
    private By sroFileUploadDialogReabrirIncidenteUploadComponent = By.id("dialog-reabrir-incidente-upload-component");
    private By inputInputFileUpload = By.id("input-file-upload");
    private By buttonBtnInputFileUpload = By.id("btn-input-file-upload");
    private By buttonBtnFileUploadSearch = By.id("btn-file-upload-search");
    private By ulFileUploadComponentListFiles = By.id("file-upload-component-list-files");
    private By buttonDialogReabrirIncidenteButtonCancel = By.id("dialog-reabrir-incidente-button-cancel");
    private By buttonDialogReabrirIncidenteConfirm = By.id("dialog-reabrir-incidente-confirm");

    /*
    Box solicitação aprovada
     */

    private By divSolicitacaoAnulacao0SolicitacaoDataAutorizador = By.id("solicitacao-anulacao-0-solicitacao-data-autorizador");
    private By titleSolicitacaoAnulacaoAprovado = By.cssSelector("div.card-solicitacao.aprovado.ng-star-inserted > div > div:nth-child(1) > div > strong");
    private By divSolicitacaoAnulacao0SolicitacaoAPROVADODataAutorizador = By.id("solicitacao-anulacao-0-solicitacao-APROVADO-data-autorizador");
    private By divSolicitacaoAnulacao0SolicitacaoAPROVADONomeAutorizador = By.id("solicitacao-anulacao-0-solicitacao-APROVADO-nome-autorizador");
    private By divSolicitacaoAnulacao0SolicitacaoAPROVADOMatriculaAutorizador = By.id("solicitacao-anulacao-0-solicitacao-APROVADO-matricula-autorizador");
    private By divSolicitacaoAnulacao0SolicitacaoAPROVADOJustificativa = By.id("solicitacao-anulacao-0-solicitacao-APROVADO-justificativa");
    private By divSolicitacaoAnulacao0SolicitacaoAPROVADOFiles = By.id("solicitacao-anulacao-0-solicitacao-APROVADO-files");
    private By tdDownloadFilesName0 = By.id("download-files-name-0");
    private By tdDownloadFilesCellActions0 = By.id("download-files-cell-actions-0");
    private By aDownloadFilesBtnDownload0 = By.id("download-files-btn-download-0");

    /*
    Histórico reprovar pedido de anulação
     */

    private By titleSolicitacaoAnulacao0SolicitacaoREPROVADO = By.cssSelector(" div.card-solicitacao.reprovado.ng-star-inserted > div > div:nth-child(1) > div > strong");
    private By divSolicitacaoAnulacao0SolicitacaoREPROVADODataAutorizador = By.id("solicitacao-anulacao-0-solicitacao-REPROVADO-data-autorizador");
    private By divSolicitacaoAnulacao0SolicitacaoREPROVADONomeAutorizador = By.id("solicitacao-anulacao-0-solicitacao-REPROVADO-nome-autorizador");
    private By divSolicitacaoAnulacao0SolicitacaoREPROVADOMatriculaAutorizador = By.id("solicitacao-anulacao-0-solicitacao-REPROVADO-matricula-autorizador");
    private By divSolicitacaoAnulacao0SolicitacaoREPROVADOJustificativa = By.id("solicitacao-anulacao-0-solicitacao-REPROVADO-justificativa");
    private By divSolicitacaoAnulacao0SolicitacaoREPROVADOFiles = By.id("solicitacao-anulacao-0-solicitacao-REPROVADO-files");


    public void validaHistoricoDeSolicitacaoDaAnulacao(String justificativa) {
        Assert.assertEquals(getText(divSolicitacaoAnulacao0SolicitacaoJustificativa), justificativa);
    }

    public void clicarBrnAprovarAnulacao() {
        scrollToElement(buttonHistoricoIncidenteFooterAprovarAnulacao);
        clickAndHighlight(buttonHistoricoIncidenteFooterAprovarAnulacao);
    }

    public void preencherModalAprovarAnulacao(String justificativa) {
        sendKeysWithJavaScript(textareaDialogReabrirIncidenteTextJustificativa, justificativa);
        uploadArquivo(1);
    }

    public void clicarBtnConfirmarModalAprovarAnulacao() {
        clickAndHighlight(buttonDialogReabrirIncidenteConfirm);
    }

    public void validarHistoricoPedidoAnulacaoAprovado(String justificativa) {
        Assert.assertEquals(getText(divSolicitacaoAnulacao0SolicitacaoAPROVADOJustificativa), justificativa);
        Assert.assertEquals(getText(titleSolicitacaoAnulacaoAprovado), "Solicitação de Anulação Aprovada");
    }

    public void clicarBtnRecusarAnulacao() {
        scrollToElement(buttonHistoricoIncidenteFooterRecusarAnulacao);
        clickAndHighlight(buttonHistoricoIncidenteFooterRecusarAnulacao);
    }

    public void preencherModalRecusarAnulacao(String justificativa) {
        sendKeysWithJavaScript(textareaDialogReabrirIncidenteTextJustificativa, justificativa);
        uploadArquivo(1);
    }

    public void clicarBtnConfirmarModalReprovarAnulacao() {
        clickAndHighlight(buttonDialogReabrirIncidenteConfirm);
    }

    public void validarHistoricoPedidoAnulacaoRecusado(String justificativa) {
        Assert.assertEquals(getText(divSolicitacaoAnulacao0SolicitacaoREPROVADOJustificativa), justificativa);
        Assert.assertEquals(getText(titleSolicitacaoAnulacao0SolicitacaoREPROVADO), "Solicitação de Anulação Reprovada");
    }
}