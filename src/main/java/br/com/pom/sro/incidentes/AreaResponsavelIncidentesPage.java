package br.com.pom.sro.incidentes;

import br.com.api.model.incidente.Incidente;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class AreaResponsavelIncidentesPage extends GeralPage {
    private By separatorAreaResponsavel = By.name("Área responsável");
    private By selectIdAreaResponsavel = By.id("idAreaResponsavel");
    private IntFunction<By> optionIdAreaResponsavel = (index) -> By.id("idAreaResponsavel-" + index);
    private By inputNomeUsuarioCoordenador = By.id("nomeUsuarioCoordenador");
    private By buttonButtonSearchCoordenador = By.id("button-search-coordenador");
    private By inputMatriculaUsuarioCoordenador = By.id("matriculaUsuarioCoordenador");
    private By inputNomeUsuarioGerente = By.id("nomeUsuarioGerente");
    private By buttonButtonSearchGerente = By.id("button-search-gerente");
    private By inputMatriculaUsuarioGerente = By.id("matriculaUsuarioGerente");
    private By inputNomeUsuarioResponsavel = By.id("nomeUsuarioResponsavel");
    private By buttonButtonSearchUsuario = By.id("button-search-usuario");
    private By inputMatriculaUsuarioResponsavel = By.id("matriculaUsuarioResponsavel");
    private By buttonButtonSalvar = By.id("button-salvar");
    private By buttonButtonSalvarAvancar = By.id("button-salvar-avancar");

    public void preencherCampos(Incidente incidente) {

        incidente.setAreaResponsavelValue(
                selectOptionAndReturnValue(
                        selectIdAreaResponsavel,
                        optionIdAreaResponsavel,
                        incidente.getAreaResponsavelIndex()
                )
        );

        clickAndHighlight(buttonButtonSearchCoordenador);
        selecionarResponsavel(incidente.getCoordenadorNomeAreaResponsavel());
        incidente.setCoordenadorMatriculaAreaResponsavel(getValue(inputMatriculaUsuarioCoordenador));

        clickAndHighlight(buttonButtonSearchGerente);
        selecionarResponsavel(incidente.getGerenteNomeAreaResponsavel());
        incidente.setGerenteMatriculaAreaResponsavel(getValue(inputMatriculaUsuarioGerente));

        clickAndHighlight(buttonButtonSearchUsuario);
        selecionarResponsavel(incidente.getResponsavelNomeAreaResponsavel());
        incidente.setResponsavelMatriculaAreaResponsavel(getValue(inputMatriculaUsuarioResponsavel));

    }

    public void clicarBtnSalvarInformacoes() {
        clickAndHighlight(buttonButtonSalvar);
    }

    public void validarDadosPreenchidos(Incidente incidente) {

        waitTime(1000);
        Assert.assertEquals(getText(selectIdAreaResponsavel), incidente.getAreaResponsavelValue());

        Assert.assertEquals(getValue(inputNomeUsuarioCoordenador), incidente.getCoordenadorNomeAreaResponsavel());
        Assert.assertEquals(getValue(inputMatriculaUsuarioCoordenador), incidente.getCoordenadorMatriculaAreaResponsavel());

        Assert.assertEquals(getValue(inputNomeUsuarioGerente), incidente.getGerenteNomeAreaResponsavel());
        Assert.assertEquals(getValue(inputMatriculaUsuarioGerente), incidente.getGerenteMatriculaAreaResponsavel());

        Assert.assertEquals(getValue(inputNomeUsuarioResponsavel), incidente.getResponsavelNomeAreaResponsavel());
        Assert.assertEquals(getValue(inputMatriculaUsuarioResponsavel), incidente.getResponsavelMatriculaAreaResponsavel());
    }

    public void validarDadosPreenchidosFicha(Incidente incidente) {
        Assert.assertEquals(getText(selectIdAreaResponsavel), incidente.getAreaResponsavelValue());

        Assert.assertEquals(getValue(inputNomeUsuarioCoordenador), incidente.getCoordenadorNomeAreaResponsavel());
        Assert.assertEquals(getValue(inputMatriculaUsuarioCoordenador), incidente.getCoordenadorMatriculaAreaResponsavel());

        Assert.assertEquals(getValue(inputNomeUsuarioGerente), incidente.getGerenteNomeAreaResponsavel());
        Assert.assertEquals(getValue(inputMatriculaUsuarioGerente), incidente.getGerenteMatriculaAreaResponsavel());

        Assert.assertEquals(getValue(inputNomeUsuarioResponsavel), incidente.getResponsavelNomeAreaResponsavel());
        Assert.assertEquals(getValue(inputMatriculaUsuarioResponsavel), incidente.getResponsavelMatriculaAreaResponsavel());
    }
}
