package br.com.pom.sro.configuracoes;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class ParametrizacaoSROPage extends GeralPage {

    private By tabDashboard = By.id("tab-configuracao-parametrizacao");


    public void clicarTabParametrizacao() {
        clickAndHighlight(tabDashboard);
    }
}
