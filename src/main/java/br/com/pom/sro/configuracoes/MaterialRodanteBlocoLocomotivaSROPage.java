package br.com.pom.sro.configuracoes;

import br.com.api.model.configuracoes.ConfiguracaoMaterialRodanteLocomotiva;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class MaterialRodanteBlocoLocomotivaSROPage extends GeralPage {

    IntFunction<By> tituloBlocoConfiguracao = (index) -> By.id("title-label-titulo-bloco-configuracao-material-rodante-" + index);
    IntFunction<By> statusBlocoConfiguracao = (index) -> By.id("configuracao-locomotiva-bloco-" + index + "-status");
    IntFunction<By> btnEditarBloco = (index) -> By.id("bloco-configuracao-material-rodante-" + index + "-editar-bloco");
    IntFunction<By> btnAdicionarConfiguracao = (index) -> By.id("bloco-configuracao-material-rodante-" + index + "-adicionar-configucarao");
    IntFunction<By> optionConfiguracaoCopiada = (index) -> By.id("copiar-configuracao-configuracaoExcecao-" + index);


    private By tabMaterialRodante = By.id("tab-configuracao-materialrodante");
    private By btnAdicionarBlocoConfiguracao = By.id("button-add-material-rodante");
    private By inputDialogNomeDoBloco = By.id("dialog-adicionar-bloco-nome");
    private By btnConfirmarDialogBlocoDeConfiguracao = By.id("dialog-adicionar-bloco-confirm");
    private By formConfiguracaoDeMaterial = By.id("info-material-rodante-configuracao-locomotiva");
    private By radioButtonConfiguracaoAtivaSim = By.id("configuracao-ativa-sim");
    private By radioButtonConfiguracaoAtivaNao = By.id("configuracao-ativa-nao");
    private By saparatorConfiguracaoMaterialRodante = By.id("info-material-rodante-configuracao-locomotiva-content-toggle");
    private By selectConfiguracaoCopiada = By.id("copiar-configuracao-configuracaoExcecao");

    /*
    Modal Adicionar Bloco
     */

    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By radioGroupCopiarConfiguracaoCheck = By.id("copiar-configuracao-check");
    private By radioButtonCopiarConfiguracaoSim = By.id("copiar-configuracao-sim");
    private By inputCopiarConfiguracaoSimInput = By.id("copiar-configuracao-sim-input");
    private By radioButtonCopiarConfiguracaoNao = By.id("copiar-configuracao-nao");
    private By inputCopiarConfiguracaoNaoInput = By.id("copiar-configuracao-nao-input");
    private By selectCopiarConfiguracaoConfiguracaoExcecao = By.id("copiar-configuracao-configuracaoExcecao");
    private By buttonDialogCopiarConfiguracaoButtonCancel = By.id("dialog-copiar-configuracao-button-cancel");
    private By buttonDialogCopiarConfiguracaoBlocoConfirm = By.id("dialog-copiar-configuracao-bloco-confirm");

    private int getPositionBlocoConfig() {
        return countChildElement(formConfiguracaoDeMaterial, "sro-title-label") - 1;
    }

    private int getPositionAdicionarConfiguracao() {
        return countChildElement(formConfiguracaoDeMaterial, "sro-title-label") - 1;
    }

    public void clicarTabMaterialRodante() {
        clickAndHighlight(tabMaterialRodante);
    }

    public void clicarBtnAddConfigMaterialRodante() {
        scrollToElement(btnAdicionarBlocoConfiguracao);
        clickAndHighlight(btnAdicionarBlocoConfiguracao);
    }

    public void adicionarBlocoDeConfiguracaoMaterialRodante(ConfiguracaoMaterialRodanteLocomotiva configuracaoMaterialRodanteLocomotiva) {
        sendKeysWithJavaScript(inputDialogNomeDoBloco, configuracaoMaterialRodanteLocomotiva.getNomeDoBloco().toUpperCase());
    }

    public void clicarBtnConfirmarBlocoDeConfiguracao() {
        scrollToElement(btnConfirmarDialogBlocoDeConfiguracao);
        clickAndHighlight(btnConfirmarDialogBlocoDeConfiguracao);
    }

    public void validaBlocoDeConfiguracaoInserido(ConfiguracaoMaterialRodanteLocomotiva configuracaoMaterialRodanteLocomotiva) {
        scrollToElement(tituloBlocoConfiguracao.apply(getPositionBlocoConfig()));
        Assert.assertEquals(getText(tituloBlocoConfiguracao.apply(getPositionBlocoConfig())), configuracaoMaterialRodanteLocomotiva.getNomeDoBloco().toUpperCase());
    }

    public void validaStatusBloco(String status) {
        scrollToElement(statusBlocoConfiguracao.apply(getPositionBlocoConfig()));
        Assert.assertEquals(getText(statusBlocoConfiguracao.apply(getPositionBlocoConfig())), status);
    }

    public void clicarBtnEditarBloco() {
        scrollToElement(btnEditarBloco.apply(getPositionBlocoConfig()));
        clickAndHighlight(btnEditarBloco.apply(getPositionBlocoConfig()));
    }

    public void editaDadosBlocoDeConfiguracao(ConfiguracaoMaterialRodanteLocomotiva configuracaoMaterialRodanteLocomotiva) {
        sendKeys(inputDialogNomeDoBloco, configuracaoMaterialRodanteLocomotiva.getNomeDoBloco().toUpperCase());
        clickWithAction(radioButtonConfiguracaoAtivaNao);
    }

    public void deixaBlocoDeConfiguracaoAtivo() {
        clickWithAction(radioButtonConfiguracaoAtivaSim);
    }

    public void clicarSeparatorConfiguracaoMaterialRodante() {
        clickAndHighlight(saparatorConfiguracaoMaterialRodante);
    }

    public void clicarBtnAdicionarConfiguracaoLocomotiva() {
        scrollToElement(btnAdicionarConfiguracao.apply(getPositionAdicionarConfiguracao()));
        clickAndHighlight(btnAdicionarConfiguracao.apply(getPositionAdicionarConfiguracao()));
    }

    public void preencherModalAdicionarBloco() {
        clickWithAction(radioButtonCopiarConfiguracaoNao);
        clickAndHighlight(buttonDialogCopiarConfiguracaoBlocoConfirm);
    }

    public void adicionarConfiguracaoCopiada() {

        clickWithAction(radioButtonCopiarConfiguracaoSim);

        selectOptionAndReturnValue(
                selectConfiguracaoCopiada,
                optionConfiguracaoCopiada,
                0
        );

        clickAndHighlight(buttonDialogCopiarConfiguracaoBlocoConfirm);
    }
}
