package br.com.pom.sro.configuracoes;

import br.com.api.model.configuracoes.ConfiguracaoMaterialRodanteLocomotiva;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.BiFunction;
import java.util.function.IntFunction;

public class MaterialRodanteConfiguracaoLocomotivaPage extends GeralPage {

    IntFunction<By> btnEditarConfiguracao = (index) -> By.id("bloco-configuracao-material-rodante-" + index + "-edit-valor-padrao");
    BiFunction<Integer, Integer, By> btnEditarConfiguracaoAdicional = (bloco, linha) -> By.id("bloco-configuracao-material-rodante-" + bloco + "-edit-" + linha);
    BiFunction<Integer, Integer, By> btnExcluirConfiguracao = (bloco, linha) -> By.id("bloco-configuracao-material-rodante-" + bloco + "-delete-" + linha);

    private By selectModeloLocomotiva = By.id("modeloLocomotiva");
    private IntFunction<By> optionModeloLocomotiva = (index) -> By.id("modeloLocomotiva-" + index);
    private By radioGroupConfiguracaoAtiva = By.id("configuracao-ativa");
    private By radioButtonConfiguracaoAtivaSim = By.id("configuracao-ativa-sim");
    private By inputConfiguracaoAtivaSimInput = By.id("configuracao-ativa-sim-input");
    private By radioButtonConfiguracaoAtivaNao = By.id("configuracao-ativa-nao");
    private By inputConfiguracaoAtivaNaoInput = By.id("configuracao-ativa-nao-input");
    private By inputValorMinFolgaVertical = By.id("valor-min-folga-vertical");
    private By inputValorMaxFolgaVertical = By.id("valor-max-folga-vertical");
    private By inputValorMinBitolaInternaRodeiro = By.id("valor-min-bitola-interna-rodeiro");
    private By inputValorMaxBitolaInternaRodeiro = By.id("valor-max-bitola-interna-rodeiro");
    private By inputValorMinEspessuraFriso = By.id("valor-min-espessura-friso");
    private By inputValorMaxEspessuraFriso = By.id("valor-max-espessura-friso");
    private By inputValorMinAlturaFriso = By.id("valor-min-altura-friso");
    private By inputValorMaxAlturaFriso = By.id("valor-max-altura-friso");
    private By inputValorMinEspessuraBandagem = By.id("valor-min-espessura-bandagem");
    private By inputValorMaxConcavidade = By.id("valor-max-concavidade");
    private By radioGroupFrisoVertical = By.id("friso-vertical");
    private By radioButtonFrisoVerticalSim = By.id("friso-vertical-sim");
    private By inputFrisoVerticalSimInput = By.id("friso-vertical-sim-input");
    private By radioButtonFrisoVerticalNao = By.id("friso-vertical-nao");
    private By inputFrisoVerticalNaoInput = By.id("friso-vertical-nao-input");
    private By inputValorMaxDiferencaBandagem = By.id("valor-max-diferenca-bandagem");
    private By inputValorMaxRodasMesmoTruque = By.id("valor-max-rodas-mesmo-truque");
    private By inputValorMaxRodasMesmaLocomotiva = By.id("valor-max-rodas-mesma-locomotiva");
    private By inputValorMinAlturaLimpaTrilhos = By.id("valor-min-altura-limpa-trilhos");
    private By inputValorMaxAlturaLimpaTrilhos = By.id("valor-max-altura-limpa-trilhos");
    private By inputValorMinAlturaEngate = By.id("valor-min-altura-engate");
    private By inputValorMaxAlturaEngate = By.id("valor-max-altura-engate");
    private By btnSalvarInformacoes = By.id("configuracao-locomotiva-footer-salvar");
    private By btnCancelar = By.id("configuracao-locomotiva-footer-cancelar");
    private By formConfiguracaoDeMaterial = By.id("info-material-rodante-configuracao-locomotiva");
    private By btnConfirmarExclusao = By.id("dialog-button-yes");

    public void preencherTelaCriarConfiguracaoLocomotiva(ConfiguracaoMaterialRodanteLocomotiva configuracaoMaterialRodanteLocomotiva, boolean isAdicional) {

        if (isAdicional) {
            configuracaoMaterialRodanteLocomotiva.setModeloValue(
                    selectOptionAndReturnValue(
                            selectModeloLocomotiva,
                            optionModeloLocomotiva,
                            configuracaoMaterialRodanteLocomotiva.getModeloIndex()
                    )
            );

            if (configuracaoMaterialRodanteLocomotiva.getConfiguracaoAtiva()) {
                clickWithAction(radioButtonConfiguracaoAtivaSim);
            } else {
                clickWithAction(radioButtonConfiguracaoAtivaNao);
            }
        }

        sendKeys(inputValorMinFolgaVertical, configuracaoMaterialRodanteLocomotiva.getFolgaVerticalValorMinimo());
        sendKeys(inputValorMaxFolgaVertical, configuracaoMaterialRodanteLocomotiva.getFolgaVerticalValorMaximo());
        sendKeys(inputValorMinBitolaInternaRodeiro, configuracaoMaterialRodanteLocomotiva.getBitolaInternaDoRodeiroValorMinimo());
        sendKeys(inputValorMaxBitolaInternaRodeiro, configuracaoMaterialRodanteLocomotiva.getBitolaInternaDoRodeiroValorMaximo());
        sendKeys(inputValorMinEspessuraFriso, configuracaoMaterialRodanteLocomotiva.getEspessuraDeFrisoValorMinimo());
        sendKeys(inputValorMaxEspessuraFriso, configuracaoMaterialRodanteLocomotiva.getEspessuraDeFrisoValorMaximo());
        sendKeys(inputValorMinAlturaFriso, configuracaoMaterialRodanteLocomotiva.getAlturaDeFrisoValorMinimo());
        sendKeys(inputValorMaxAlturaFriso, configuracaoMaterialRodanteLocomotiva.getAlturaDeFrisoValorMaximo());
        sendKeys(inputValorMinEspessuraBandagem, configuracaoMaterialRodanteLocomotiva.getEspessuraDeBandagemValorMinimo());
        sendKeys(inputValorMaxConcavidade, configuracaoMaterialRodanteLocomotiva.getConcavidadeValorMaximo());

        if (configuracaoMaterialRodanteLocomotiva.getFrisoVertical()) {
            clickWithAction(radioButtonFrisoVerticalSim);
        } else {
            clickWithAction(radioButtonFrisoVerticalNao);
        }

        sendKeys(inputValorMaxDiferencaBandagem, configuracaoMaterialRodanteLocomotiva.getRodasNoMesmoEixoValorMaximo());
        sendKeys(inputValorMaxRodasMesmoTruque, configuracaoMaterialRodanteLocomotiva.getRodasNoMesmoTruqueValorMaximo());
        sendKeys(inputValorMaxRodasMesmaLocomotiva, configuracaoMaterialRodanteLocomotiva.getRodasNaMesmaLocomotivaValorMaximo());
        sendKeys(inputValorMinAlturaLimpaTrilhos, configuracaoMaterialRodanteLocomotiva.getDistanciaVerticalEntreBoletoLimpaTrilhoValorMinimo());
        sendKeys(inputValorMaxAlturaLimpaTrilhos, configuracaoMaterialRodanteLocomotiva.getDistanciaVerticalEntreBoletoLimpaTrilhoValorMaximo());
        sendKeys(inputValorMinAlturaEngate, configuracaoMaterialRodanteLocomotiva.getAlturaEntreCentroDaMandibulaTopoBoletoValorMinimo());
        sendKeys(inputValorMaxAlturaEngate, configuracaoMaterialRodanteLocomotiva.getAlturaEntreCentroDaMandibulaTopoBoletoValorMaximo());
    }

    public void validarQueAsInformacoesLocomotivaPersistiram(ConfiguracaoMaterialRodanteLocomotiva configuracaoMaterialRodanteLocomotiva, boolean isAdicional) {
        if (isAdicional) {
            expectText(selectModeloLocomotiva, configuracaoMaterialRodanteLocomotiva.getModeloValue());
            Assert.assertEquals(getText(selectModeloLocomotiva), configuracaoMaterialRodanteLocomotiva.getModeloValue());

            if (configuracaoMaterialRodanteLocomotiva.getConfiguracaoAtiva()) {
                Assert.assertTrue(isRadioChecked(radioButtonConfiguracaoAtivaSim));
            } else {
                Assert.assertTrue(isRadioChecked(radioButtonConfiguracaoAtivaNao));
            }
        }

        Assert.assertEquals(getValue(inputValorMinFolgaVertical), configuracaoMaterialRodanteLocomotiva.getFolgaVerticalValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxFolgaVertical), configuracaoMaterialRodanteLocomotiva.getFolgaVerticalValorMaximo());
        Assert.assertEquals(getValue(inputValorMinBitolaInternaRodeiro), configuracaoMaterialRodanteLocomotiva.getBitolaInternaDoRodeiroValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxBitolaInternaRodeiro), configuracaoMaterialRodanteLocomotiva.getBitolaInternaDoRodeiroValorMaximo());
        Assert.assertEquals(getValue(inputValorMinEspessuraFriso), configuracaoMaterialRodanteLocomotiva.getEspessuraDeFrisoValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxEspessuraFriso), configuracaoMaterialRodanteLocomotiva.getEspessuraDeFrisoValorMaximo());
        Assert.assertEquals(getValue(inputValorMinAlturaFriso), configuracaoMaterialRodanteLocomotiva.getAlturaDeFrisoValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxAlturaFriso), configuracaoMaterialRodanteLocomotiva.getAlturaDeFrisoValorMaximo());
        Assert.assertEquals(getValue(inputValorMinEspessuraBandagem), configuracaoMaterialRodanteLocomotiva.getEspessuraDeBandagemValorMinimo());
        scrollToElement(inputValorMaxConcavidade);
        Assert.assertEquals(getValue(inputValorMaxConcavidade), configuracaoMaterialRodanteLocomotiva.getConcavidadeValorMaximo());

        if (configuracaoMaterialRodanteLocomotiva.getFrisoVertical()) {
            Assert.assertTrue(isRadioChecked(radioButtonFrisoVerticalSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonFrisoVerticalNao));
        }

        Assert.assertEquals(getValue(inputValorMaxDiferencaBandagem), configuracaoMaterialRodanteLocomotiva.getRodasNoMesmoEixoValorMaximo());
        Assert.assertEquals(getValue(inputValorMaxRodasMesmoTruque), configuracaoMaterialRodanteLocomotiva.getRodasNoMesmoTruqueValorMaximo());
        Assert.assertEquals(getValue(inputValorMaxRodasMesmaLocomotiva), configuracaoMaterialRodanteLocomotiva.getRodasNaMesmaLocomotivaValorMaximo());
        Assert.assertEquals(getValue(inputValorMinAlturaLimpaTrilhos), configuracaoMaterialRodanteLocomotiva.getDistanciaVerticalEntreBoletoLimpaTrilhoValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxAlturaLimpaTrilhos), configuracaoMaterialRodanteLocomotiva.getDistanciaVerticalEntreBoletoLimpaTrilhoValorMaximo());
        Assert.assertEquals(getValue(inputValorMinAlturaEngate), configuracaoMaterialRodanteLocomotiva.getAlturaEntreCentroDaMandibulaTopoBoletoValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxAlturaEngate), configuracaoMaterialRodanteLocomotiva.getAlturaEntreCentroDaMandibulaTopoBoletoValorMaximo());
    }

    public void clicarBtnSalvarConfiguracao() {
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void clicarBtnEditarConfiguracao() {
        clickAndHighlight(btnEditarConfiguracao.apply(getPositionAdicionarConfiguracao()));
    }

    public void clicarBtnEditarConfiguracaoAdicional(int linha) {
        linha--;
        clickAndHighlight(btnEditarConfiguracaoAdicional.apply(getPositionAdicionarConfiguracao(), linha));
    }

    private int getPositionAdicionarConfiguracao() {
        return countChildElement(formConfiguracaoDeMaterial, "sro-title-label") - 1;
    }

    public void clicarBtnExcluirConfiguracao(int linha) {
        linha--;
        clickAndHighlight(btnExcluirConfiguracao.apply(getPositionAdicionarConfiguracao(), linha));
    }

    public void confirmarExclusaoConfiguracao() {
        clickAndHighlight(btnConfirmarExclusao);
    }

    public void preenchoModeloEConfiguracaoLocomotiva(ConfiguracaoMaterialRodanteLocomotiva configuracaoMaterialRodanteLocomotiva) {
        configuracaoMaterialRodanteLocomotiva.setModeloValue(
                selectOptionAndReturnValue(
                        selectModeloLocomotiva,
                        optionModeloLocomotiva,
                        configuracaoMaterialRodanteLocomotiva.getModeloIndex()
                )
        );

        if (configuracaoMaterialRodanteLocomotiva.getConfiguracaoAtiva()) {
            clickWithAction(radioButtonConfiguracaoAtivaSim);
        } else {
            clickWithAction(radioButtonConfiguracaoAtivaNao);
        }
    }

    public void validarValoresConfiguracaoCopiada(ConfiguracaoMaterialRodanteLocomotiva configuracaoMaterialRodanteLocomotiva) {
        Assert.assertEquals(getValue(inputValorMinFolgaVertical), configuracaoMaterialRodanteLocomotiva.getFolgaVerticalValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxFolgaVertical), configuracaoMaterialRodanteLocomotiva.getFolgaVerticalValorMaximo());
        Assert.assertEquals(getValue(inputValorMinBitolaInternaRodeiro), configuracaoMaterialRodanteLocomotiva.getBitolaInternaDoRodeiroValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxBitolaInternaRodeiro), configuracaoMaterialRodanteLocomotiva.getBitolaInternaDoRodeiroValorMaximo());
        Assert.assertEquals(getValue(inputValorMinEspessuraFriso), configuracaoMaterialRodanteLocomotiva.getEspessuraDeFrisoValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxEspessuraFriso), configuracaoMaterialRodanteLocomotiva.getEspessuraDeFrisoValorMaximo());
        Assert.assertEquals(getValue(inputValorMinAlturaFriso), configuracaoMaterialRodanteLocomotiva.getAlturaDeFrisoValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxAlturaFriso), configuracaoMaterialRodanteLocomotiva.getAlturaDeFrisoValorMaximo());
        Assert.assertEquals(getValue(inputValorMinEspessuraBandagem), configuracaoMaterialRodanteLocomotiva.getEspessuraDeBandagemValorMinimo());
        scrollToElement(inputValorMaxConcavidade);
        Assert.assertEquals(getValue(inputValorMaxConcavidade), configuracaoMaterialRodanteLocomotiva.getConcavidadeValorMaximo());

        if (configuracaoMaterialRodanteLocomotiva.getFrisoVertical()) {
            Assert.assertTrue(isRadioChecked(radioButtonFrisoVerticalSim));
        } else {
            Assert.assertTrue(isRadioChecked(radioButtonFrisoVerticalNao));
        }

        Assert.assertEquals(getValue(inputValorMaxDiferencaBandagem), configuracaoMaterialRodanteLocomotiva.getRodasNoMesmoEixoValorMaximo());
        Assert.assertEquals(getValue(inputValorMaxRodasMesmoTruque), configuracaoMaterialRodanteLocomotiva.getRodasNoMesmoTruqueValorMaximo());
        Assert.assertEquals(getValue(inputValorMaxRodasMesmaLocomotiva), configuracaoMaterialRodanteLocomotiva.getRodasNaMesmaLocomotivaValorMaximo());
        Assert.assertEquals(getValue(inputValorMinAlturaLimpaTrilhos), configuracaoMaterialRodanteLocomotiva.getDistanciaVerticalEntreBoletoLimpaTrilhoValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxAlturaLimpaTrilhos), configuracaoMaterialRodanteLocomotiva.getDistanciaVerticalEntreBoletoLimpaTrilhoValorMaximo());
        Assert.assertEquals(getValue(inputValorMinAlturaEngate), configuracaoMaterialRodanteLocomotiva.getAlturaEntreCentroDaMandibulaTopoBoletoValorMinimo());
        Assert.assertEquals(getValue(inputValorMaxAlturaEngate), configuracaoMaterialRodanteLocomotiva.getAlturaEntreCentroDaMandibulaTopoBoletoValorMaximo());
    }
}


