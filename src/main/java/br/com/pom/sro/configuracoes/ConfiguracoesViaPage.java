package br.com.pom.sro.configuracoes;

import br.com.api.model.configuracoes.ConfiguracaoVia;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.BiFunction;
import java.util.function.IntFunction;

public class ConfiguracoesViaPage extends GeralPage {

    private By tabConfiguracaoVia = By.id("tab-configuracao-via");
    private By buttonButtonAdicionarConfiguracao = By.id("button-adicionar-configuracao");
    private By buttonButtonSalvarConfiguracoes = By.id("button-salvar-configuracoes");
    private By sroBlocoConfiguracao = By.tagName("sro-bloco-configuracao");

    /*
    Modal add configuração
     */

    private By h4DialogHeader = By.id("dialog-header");
    private By buttonDialogButtonClose = By.id("dialog-button-close");
    private By divDialogConfiguracaoVia = By.id("dialog-configuracao-via");
    private By inputDialogNomeConfiguracaoVia = By.id("dialog-nome-configuracao-via");
    private By radioGroupDialogAtivoConfiguracaoVia = By.id("dialog-ativo-configuracao-via");
    private By radioButtonAtivoSim = By.id("ativo-sim");
    private By inputAtivoSimInput = By.id("ativo-sim-input");
    private By radioButtonAtivoNao = By.id("ativo-nao");
    private By inputAtivoNaoInput = By.id("ativo-nao-input");
    private By buttonDialogButtonNo = By.id("dialog-button-no");
    private By buttonDialogButtonYes = By.id("dialog-button-yes");

    /*
        Configuração
     */

    private IntFunction<By> divTitleLabelConfiguracaoViaTitle = (index) -> By.id("title-label-configuracao-via-title-" + index);
    private IntFunction<By> divConfiguracaoViaAtivo = (index) -> By.id("configuracao-via-ativo-" + index);
    private IntFunction<By> buttonDialogButtonAddFaixa = (index) -> By.id("dialog-button-add-faixa-" + index);
    private IntFunction<By> buttonDialogButtonEditConfiguracao = (index) -> By.id("dialog-button-edit-configuracao-" + index);
    private IntFunction<By> buttonDialogButtonDeleteConfiguracao = (index) -> By.id("dialog-button-delete-configuracao-" + index);


    /*
    Modal faixa
     */

    private By divDialogConfiguracaoViaFaixa = By.id("dialog-configuracao-via-faixa");
    private By divTitleLabel23 = By.id("title-label-configuracao-faixa");
    private By inputDialogMenorValorConfiguracaoFaixa = By.id("dialog-menor-valor-configuracao-faixa");
    private By inputDialogMaiorValorConfiguracaoFaixa = By.id("dialog-maior-valor-configuracao-faixa");
    private By inputDialogTangenteTorcaoConfiguracaoFaixa = By.id("dialog-tangente-torcao-configuracao-faixa");
    private By inputDialogTangenteVariacaoConfiguracaoFaixa = By.id("dialog-tangente-variacao-configuracao-faixa");
    private By inputDialogCurvaTorcaoConfiguracaoFaixa = By.id("dialog-curva-torcao-configuracao-faixa");
    private By inputDialogCurvaVariacaoConfiguracaoFaixa = By.id("dialog-curva-variacao-configuracao-faixa");
    private By inputDialogTangenteTorcao2ConfiguracaoFaixa = By.id("dialog-tangente-torcao2-configuracao-faixa");
    private By inputDialogTangenteVariacao2ConfiguracaoFaixa = By.id("dialog-tangente-variacao2-configuracao-faixa");
    private By inputDialogCurvaTorcao2ConfiguracaoFaixa = By.id("dialog-curva-torcao2-configuracao-faixa");
    private By inputDialogCurvaVariacao2ConfiguracaoFaixa = By.id("dialog-curva-variacao2-configuracao-faixa");
    private By inputDialogTorcaoTwistConfiguracaoFaixa = By.id("dialog-torcao-twist-configuracao-faixa");
    private By inputDialogBitolaMinConfiguracaoFaixa = By.id("dialog-bitola-min-configuracao-faixa");
    private By inputDialogBitolaMaxConfiguracaoFaixa = By.id("dialog-bitola-max-configuracao-faixa");
    private By inputDialogVariacaoBitolaConfiguracaoFaixa = By.id("dialog-variacao-bitola-configuracao-faixa");
    private By inputDialogFlechaConfiguracaoFaixa = By.id("dialog-flecha-configuracao-faixa");
    private By inputDialogDefeitoAlinhamentoTGCVConfiguracaoFaixa = By.id("dialog-defeitoAlinhamentoTGCVconfiguracao-faixa");
    private By inputDialogDefeitoAlinhamentoTransicaoConfiguracaoFaixa = By.id("dialog-defeitoAlinhamentoTransicaoconfiguracao-faixa");
    private By inputDialogClasseViaConfiguracaoFaixa = By.id("dialog-classe-via-configuracao-faixa");
    private By inputDialogSuperiorAdmAgConfiguracaoFaixa = By.id("dialog-superior-adm-ag-configuracao-faixa");
    private By buttonDialogButtonNoAddFaixa = By.id("dialog-button-no");
    private By buttonDialogButtonYesAddFaixa = By.id("dialog-button-yes");

    /*
    Table faixas
     */

    private BiFunction<Integer, Integer, By> thTableFaixasCellNomeFaixa = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-nome-faixa-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellTangenteTorcao = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-tangente-torcao-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellTangenteVariacao = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-tangente-variacao-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellCurvaTorcao = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-curva-torcao-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellCurvaVariacao = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-curva-variacao-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellTangenteTorcao2 = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-tangente-torcao2-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellTangenteVariacao2 = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-tangente-variacao2-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellCurvaTorcao2 = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-curva-torcao2-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellCurvaVariacao2 = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-curva-variacao2-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellTorcaoTwist = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-torcao-twist-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellBitolaMin = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-bitola-min-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellBitolaMax = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-bitola-max-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellVariacaoBitola = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-variacao-bitola-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellFlecha = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-flecha-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellDefeito_alinhamento_tgcv = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-defeito_alinhamento_tgcv-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellDefeito_alinhamento_transicao = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-defeito_alinhamento_transicao-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellClasseVia = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-classe-via-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellSuperiorAdmAg = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-superior-adm-ag-" + linha);
    private BiFunction<Integer, Integer, By> tdTableFaixasCellAcoes = (tableFaixa, linha) -> By.id("table-faixas-" + tableFaixa + "-cell-acoes-" + linha);
    private BiFunction<Integer, Integer, By> buttonButtonEditFaixa = (tableFaixa, linha) -> By.id("button-edit-" + tableFaixa + "-faixa-" + linha);
    private BiFunction<Integer, Integer, By> buttonButtonDeleteFaixa = (tableFaixa, linha) -> By.id("button-delete-" + tableFaixa + "-faixa-" + linha);

    /*
    Modal delete faixa
     */

    private By divDialogConfirmationMessage = By.id("dialog-confirmation-message");
    private By buttonDialogButtonNoDelete = By.id("dialog-button-no");
    private By buttonDialogButtonYesDelete = By.id("dialog-button-yes");

    private int getCountConfiguracoes() {
        return getWebElements(sroBlocoConfiguracao).size();
    }

    public void acessoATABViaDasConfiguracoes() {
        clickAndHighlight(tabConfiguracaoVia);
    }

    public void clicarBtnAdicionarConfiguracao() {
        clickAndHighlight(buttonButtonAdicionarConfiguracao);
    }

    public void preencherModalAddConfiguracao(ConfiguracaoVia configuracaoVia) {
        sendKeys(inputDialogNomeConfiguracaoVia, configuracaoVia.getNomeDaConfiguracao());
        if (configuracaoVia.isStatus()) {
            clickWithAction(radioButtonAtivoSim);
        } else {
            clickWithAction(radioButtonAtivoNao);
        }
    }

    public void clicarBtnSalvarModalAddConfiguracao() {
        clickAndHighlight(buttonDialogButtonYes);
    }

    public void validarDadosConfiguracao(ConfiguracaoVia configuracaoVia) {
        Assert.assertEquals(getText(divTitleLabelConfiguracaoViaTitle.apply(getCountConfiguracoes() - 1)), configuracaoVia.getNomeDaConfiguracao().toUpperCase());
        if (configuracaoVia.isStatus()) {
            Assert.assertEquals(getText(divConfiguracaoViaAtivo.apply(getCountConfiguracoes() - 1)), "ativo");
            expectElementVisible(buttonDialogButtonDeleteConfiguracao.apply(getCountConfiguracoes() - 1));
        } else {
            Assert.assertEquals(getText(divConfiguracaoViaAtivo.apply(getCountConfiguracoes() - 1)), "inativo");
            expectElementNotVisible(buttonDialogButtonDeleteConfiguracao.apply(getCountConfiguracoes() - 1));
        }
    }

    public void clicarBtnEditarConfiguracao() {
        clickAndHighlight(buttonDialogButtonEditConfiguracao.apply(getCountConfiguracoes() - 1));
    }

    public void clicarBtnAdicionarFaixa() {
        clickAndHighlight(buttonDialogButtonAddFaixa.apply(getCountConfiguracoes() - 1));
    }

    public void preencherDadosFaixa(ConfiguracaoVia configuracaoVia) {
        sendKeys(inputDialogMenorValorConfiguracaoFaixa, configuracaoVia.getMenorVelocidade());
        sendKeys(inputDialogMaiorValorConfiguracaoFaixa, configuracaoVia.getMaiorVelocidade());
        sendKeys(inputDialogTangenteTorcaoConfiguracaoFaixa, configuracaoVia.getTangenteTorcao());
        sendKeys(inputDialogTangenteVariacaoConfiguracaoFaixa, configuracaoVia.getTangenteVariacao());
        sendKeys(inputDialogCurvaTorcaoConfiguracaoFaixa, configuracaoVia.getCurvaTorcao());
        sendKeys(inputDialogCurvaVariacaoConfiguracaoFaixa, configuracaoVia.getCurvaVariacao());
        sendKeys(inputDialogTangenteTorcao2ConfiguracaoFaixa, configuracaoVia.getTangenteTorcao2());
        sendKeys(inputDialogTangenteVariacao2ConfiguracaoFaixa, configuracaoVia.getTangenteVariacao2());
        sendKeys(inputDialogCurvaTorcao2ConfiguracaoFaixa, configuracaoVia.getCurvaTorcao2());
        sendKeys(inputDialogCurvaVariacao2ConfiguracaoFaixa, configuracaoVia.getCurvaVariacao2());
        sendKeys(inputDialogTorcaoTwistConfiguracaoFaixa, configuracaoVia.getTorcaoTwist());
        sendKeys(inputDialogBitolaMinConfiguracaoFaixa, configuracaoVia.getBitolaMinima());
        sendKeys(inputDialogBitolaMaxConfiguracaoFaixa, configuracaoVia.getBitolaMaxima());
        sendKeys(inputDialogVariacaoBitolaConfiguracaoFaixa, configuracaoVia.getVariacaoBitola());
        sendKeys(inputDialogFlechaConfiguracaoFaixa, configuracaoVia.getFlecha());
        sendKeys(inputDialogDefeitoAlinhamentoTGCVConfiguracaoFaixa, configuracaoVia.getDefeitoAlinhamentoTGECV());
        sendKeys(inputDialogDefeitoAlinhamentoTransicaoConfiguracaoFaixa, configuracaoVia.getDefeitoAlinhamentoTransicao());
        sendKeys(inputDialogClasseViaConfiguracaoFaixa, configuracaoVia.getClasseVia());
        sendKeys(inputDialogSuperiorAdmAgConfiguracaoFaixa, configuracaoVia.getSuperiorADMAG());
    }

    public void clicarBtnSalvarModalFaixa() {
        clickAndHighlight(buttonDialogButtonYesAddFaixa);
    }

    public void clicarBtnSalvarInformacoes() {
        clickAndHighlight(buttonButtonSalvarConfiguracoes);
    }

    public void validaFaixaAdicionada(ConfiguracaoVia configuracaoVia, int linha) {
        linha--;
        int configuracao = getCountConfiguracoes() - 1;

//        Assert.assertEquals(getText(thTableFaixasCellNomeFaixa.apply(configuracao, linha)), configuracaoVia.getTangenteTorcao());

        Assert.assertEquals(getText(tdTableFaixasCellTangenteTorcao.apply(configuracao, linha)), configuracaoVia.getTangenteTorcao());
        Assert.assertEquals(getText(tdTableFaixasCellTangenteVariacao.apply(configuracao, linha)), configuracaoVia.getTangenteVariacao());
        Assert.assertEquals(getText(tdTableFaixasCellCurvaTorcao.apply(configuracao, linha)), configuracaoVia.getCurvaTorcao());
        Assert.assertEquals(getText(tdTableFaixasCellCurvaVariacao.apply(configuracao, linha)), configuracaoVia.getCurvaVariacao());
        Assert.assertEquals(getText(tdTableFaixasCellTangenteTorcao2.apply(configuracao, linha)), configuracaoVia.getTangenteTorcao2());
        Assert.assertEquals(getText(tdTableFaixasCellTangenteVariacao2.apply(configuracao, linha)), configuracaoVia.getTangenteVariacao2());
        Assert.assertEquals(getText(tdTableFaixasCellCurvaTorcao2.apply(configuracao, linha)), configuracaoVia.getCurvaTorcao2());
        Assert.assertEquals(getText(tdTableFaixasCellCurvaVariacao2.apply(configuracao, linha)), configuracaoVia.getCurvaVariacao2());
        Assert.assertEquals(getText(tdTableFaixasCellTorcaoTwist.apply(configuracao, linha)), configuracaoVia.getTorcaoTwist());
        Assert.assertEquals(getText(tdTableFaixasCellBitolaMin.apply(configuracao, linha)), configuracaoVia.getBitolaMinima());
        Assert.assertEquals(getText(tdTableFaixasCellBitolaMax.apply(configuracao, linha)), configuracaoVia.getBitolaMaxima());
        Assert.assertEquals(getText(tdTableFaixasCellVariacaoBitola.apply(configuracao, linha)), configuracaoVia.getVariacaoBitola());
        Assert.assertEquals(getText(tdTableFaixasCellFlecha.apply(configuracao, linha)), configuracaoVia.getFlecha());
        Assert.assertEquals(getText(tdTableFaixasCellDefeito_alinhamento_tgcv.apply(configuracao, linha)), configuracaoVia.getDefeitoAlinhamentoTGECV());
        Assert.assertEquals(getText(tdTableFaixasCellDefeito_alinhamento_transicao.apply(configuracao, linha)), configuracaoVia.getDefeitoAlinhamentoTransicao());
        Assert.assertEquals(getText(tdTableFaixasCellClasseVia.apply(configuracao, linha)), configuracaoVia.getClasseVia());
        Assert.assertEquals(getText(tdTableFaixasCellSuperiorAdmAg.apply(configuracao, linha)), configuracaoVia.getSuperiorADMAG());
    }

    public void clicarBtnEditarFaixa(int linha) {
        linha--;
        clickAndHighlight(buttonButtonEditFaixa.apply(getCountConfiguracoes() - 1, linha));
    }

    public void clicarBtnExcluirFaixa(int linha) {
        linha--;
        clickAndHighlight(buttonButtonDeleteFaixa.apply(getCountConfiguracoes() - 1, linha));
    }

    public void validaModalExclusaoFaixa() {
        Assert.assertEquals(getText(divDialogConfirmationMessage), "Deseja mesmo excluir a faixa?");
    }

    public void clicarBtnConfirmaExclusao() {
        clickAndHighlight(buttonDialogButtonYesAddFaixa);
    }

    public void validaFaixaExcluida(int linha) {
        linha--;
        expectElementNotVisible(tdTableFaixasCellTangenteTorcao.apply(getCountConfiguracoes() - 1, linha));
    }
}
