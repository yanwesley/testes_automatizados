package br.com.pom.sro.configuracoes;

import br.com.api.model.configuracoes.ConfiguracaoMaterialRodanteVagao;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class MaterialRodanteSROVagaoPage extends GeralPage {


    private IntFunction<By> nomeBlocoConfiguracaoVagao = (index) -> By.id("title-label-titulo-bloco-configuracao-vagao-" + index);
    private IntFunction<By> statusBlocoConfiguracaoVagao = (index) -> By.id("configuracao-vagao-" + index + "-status");
    private IntFunction<By> btnEditarBlocoConfigVagao = (index) -> By.id("bloco-configuracao-vagao-" + index + "-editar-bloco");
    private By formConfiguracaoDeVagao = By.id("info-material-rodante-configuracao-vagao");
    private By btnSalvarInformacoes = By.id("button-salvar");
    private By btnAddConfiguracaoVagao = By.id("button-add-configuracao-vagao");
    private By saparatorConfiguracaoMaterialRodanteVagao = By.id("info-material-rodante-configuracao-vagao-content-toggle");

    //Configuração vagão
    private By inputNomeConfiguracaoVagao = By.id("descricao");
    private By raddioButtonConfiguracaoVagaoAtivaSim = By.id("ativo-0");
    private By raddioButtonConfiguracaoVagaoAtivaNao = By.id("ativo-1");

    //Limites do rodeiro
    private By separatorLimitesDoRodeiro = By.id("limites-do-rodeiro-content-toggle");
    private By inputBitolaMinimaLarga = By.id("bitola-0-bitolaMinima");
    private By inputBitolaMinimaMetrica = By.id("bitola-1-bitolaMinima");
    private By inputBitolaMaximaLarga = By.id("bitola-0-bitolaMaxima");
    private By inputBitolaMaximaMetrica = By.id("bitola-1-bitolaMaxima");
    private By inputEspessuraFrisoLarga = By.id("bitola-0-espessuraFriso");
    private By inputEspessuraFrisoMetrica = By.id("bitola-1-espessuraFriso");
    private By inputAlturaFrisoLarga = By.id("bitola-0-alturaFriso");
    private By inputAlturaFrisoMetrica = By.id("bitola-1-alturaFriso");
    private By inputBandagemLarga = By.id("bitola-0-bandagem");
    private By inputBandagemMetrica = By.id("bitola-1-bandagem");
    private By inputHollowLarga = By.id("bitola-0-hollow");
    private By inputHollowMetrica = By.id("bitola-1-hollow");
    private By inputDiferencaMaximaEspessuraDeFriso = By.id("difMaximaEspessuraFriso");

    //Informação sobre a manga de eixo
    private By separatorInformacaoSobreMangaDeEixo = By.id("informacao-sobre-a-manga-de-eixo-content-toggle");
    private IntFunction<By> inputManga = (index) -> By.id("manga-" + index + "-nomeManga");
    private IntFunction<By> inputLetraNaBitolaMetrica = (index) -> By.id("manga-" + index + "-letraBitolaMetrica");
    private IntFunction<By> inputLetraNaBitolaLarga = (index) -> By.id("manga-" + index + "-letraBitolaLarga");
    private By btnAddManga = By.id("button-add-manga");
    private By msgNomeDuplicadoManga = By.cssSelector("sro-informacao-sobre-manga-de-eixo > div > div.col-8.ng-star-inserted > table > tbody > tr:nth-child(2) > td.nomeManga > sro-input-error-msg");

    //Tipo de manutenção
    private By separatorTipoDeManutencao = By.id("tipo-de-manutencao-content-toggle");
    private IntFunction<By> inputTiposManutencao = (index) -> By.id("tipoManutencao-" + index + "-descricaoManutencao");
    private IntFunction<By> selectBitolaTipoManutencao = (index) -> By.id("tipoManutencao-" + index + "-bitola");
    private IntFunction<By> optionBitolaTipoManutencaoLarga = (index) -> By.id("tipoManutencao-" + index + "-bitola-LARGA");
    private IntFunction<By> optionBitolaTipoManutencaoMetrica = (index) -> By.id("tipoManutencao-" + index + "-bitola-METRICA");
    private By btnAddTipoDeManutencao = By.id("button-add-tipoManutencao");
    private By msgTipoDuplicadoTipoManutencao = By.cssSelector("sro-tipo-de-manutencao > div > div.col-6.ng-star-inserted > table > tbody > tr:nth-child(2) > td:nth-child(1) > sro-input-error-msg");

    //Limite de cunhas de fricção
    private By separatorLimiteDeCunhasDeFriccao = By.id("limite-de-cunhas-de-friccao-content-toggle");
    private IntFunction<By> inputTruqueCunhaFriccao = (index) -> By.id("limiteCunha-" + index + "-descricaoTruque");
    private IntFunction<By> inputLimiteAlturaCunhaFriccao = (index) -> By.id("limiteCunha-" + index + "-limiteCunha");
    private By btnAddLimiteCunhaFriccao = By.id("button-add-limiteCunha");
    private By msgNomeDuplicadoLimiteDeCunhasDeFriccao = By.cssSelector("sro-limite-de-cunhas-de-fricao > div > div.col-7.ng-star-inserted > table > tbody > tr:nth-child(2) > td:nth-child(1) > sro-input-error-msg");

    //Limite de diferença de botões
    private By separatorLimiteDiferencaDeBotoes = By.id("limite-de-diferencas-de-botoes-content-toggle");
    private By inputLimiteDiferencaBotoesMaximo = By.id("diferencaBotoes");

    //Limites de prato de pião
    private By separatorLimiteDePratoDePiao = By.id("limite-de-prato-de-piao-content-toggle");
    private IntFunction<By> inputPratoPiao = (index) -> By.id("limitePratoPiao-" + index + "-pratoPiao");
    private IntFunction<By> selectBitolaLimitePratoDePiao = (index) -> By.id("limitePratoPiao-" + index + "-bitola");
    private IntFunction<By> optionBitolaLimitePratoDePiaoLarga = (index) -> By.id("limitePratoPiao-" + index + "-bitola-LARGA");
    private IntFunction<By> optionBitolaLimitePratoDePiaoMetrica = (index) -> By.id("limitePratoPiao-" + index + "-bitola-METRICA");
    private IntFunction<By> inputSupMin = (index) -> By.id("limitePratoPiao-" + index + "-supMin");
    private IntFunction<By> inputSupMax = (index) -> By.id("limitePratoPiao-" + index + "-supMax");
    private IntFunction<By> inputAltMin = (index) -> By.id("limitePratoPiao-" + index + "-altMin");
    private IntFunction<By> inputAltMax = (index) -> By.id("limitePratoPiao-" + index + "-altMax");
    private IntFunction<By> inputInfMin = (index) -> By.id("limitePratoPiao-" + index + "-infMin");
    private IntFunction<By> inputInfMax = (index) -> By.id("limitePratoPiao-" + index + "-infMax");
    private IntFunction<By> inputProfMin = (index) -> By.id("limitePratoPiao-" + index + "-profMin");
    private IntFunction<By> inputProfMax = (index) -> By.id("limitePratoPiao-" + index + "-profMax");
    private IntFunction<By> btnExcluirLimitePratoDePiao = (index) -> By.id("limitePratoPiao-" + index + "-button-remove-" + index);
    private By btnAddLimitePratoDePiao = By.id("button-add-limitePratoPiao");
    private By msgNomeDuplicadoLimiteDePratoDePiao = By.cssSelector("sro-limites-de-prato-piao > div > div.col-12.ng-star-inserted > table > tbody > tr:nth-child(2) > td:nth-child(1) > sro-input-error-msg");


    //Limites de folga de prato de pião
    private By separatorLimiteDeFolgaPratoDePiao = By.id("limite-de-folga-de-prato-de-piao-content-toggle");
    private By inputFolgaMinimaPrato = By.id("folgaMinimaPratoPiao");
    private By inputFolgaMaximaPrato = By.id("folgaMaximaPratoPiao");
    private By inputFolgaVerticalMinimaPrato = By.id("folgaVerticalMinPratoPiao");
    private By inputFolgaVerticalMaximaPrato = By.id("folgaVerticalMaxPratoPiao");

    //Limites de amparo balanço
    private By separatorLimiteDeAmparoBalanco = By.id("limite-de-amparo-balanco-content-toggle");
    private IntFunction<By> inputTipoDeAmparoBalanco = (index) -> By.id("limiteAmparoBalanco-" + index + "-descricaoTipoAmparaBalanco");
    private By inputAmparoBalancoMinimo = By.id("limiteAmparoBalanco-0-alturaMinima");
    private By inputAmparoBalancoMaximo = By.id("limiteAmparoBalanco-0-alturaMaxima");
    private By inputAmparoBalancoFolgaMinima = By.id("limiteMinimoAmparoBalanco");
    private By inputAmparoBalancoFolgaMaxima = By.id("limiteMaximoAmparoBalanco");
    private By inputAmparoBalancoDiferencaMaximaSomaCruzada = By.id("difMaxCruzAmparoBalanco");
    private By inputAmparoBalancoDesgasteConcavoDaChapa = By.id("desgasteConcavoChapa");
    private By btnAddLimiteAmparoBalanço = By.id("button-add-limiteAmparoBalanco");
    private By msgNomeDuplicadoLimiteAmparoBalanco = By.cssSelector("sro-limites-de-ampara-balanco > div > div.col-10.ng-star-inserted > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > sro-input-error-msg");

    //Limites de altura de engate
    private By separatorLimiteDeAlturaDeEngate = By.id("limite-de-altura-de-engate-content-toggle");
    private By inputAlturaEngateMetricaVazioAltMin = By.id("engate-0-alturaMinima");
    private By inputAlturaEngateMetricaVazioAltMax = By.id("engate-0-alturaMaxima");
    private By inputAlturaEngateMetricaCarregadoAltMin = By.id("engate-1-alturaMinima");
    private By inputAlturaEngateMetricaCarregadoAltMax = By.id("engate-1-alturaMaxima");
    private By inputAlturaEngateLargaVazioAltMin = By.id("engate-2-alturaMinima");
    private By inputAlturaEngateLargaVazioAltMax = By.id("engate-2-alturaMaxima");
    private By inputAlturaEngateLargaCarregadoAltMin = By.id("engate-3-alturaMinima");
    private By inputAlturaEngateLargaCarregadoAltMax = By.id("engate-3-alturaMaxima");

    //Limite de molas
    private By separatorLimiteDeMolas = By.id("limite-de-molas-content-toggle");
    private IntFunction<By> linputLimiteDeMolasMola = (index) -> By.id("mola-" + index + "-descricaoMola");
    private By linputLimiteDeMolasExtMax = By.id("mola-0-extMax");
    private By linputLimiteDeMolasExtMin = By.id("mola-0-extMin");
    private By linputLimiteDeMolasIntMax = By.id("mola-0-intMax");
    private By linputLimiteDeMolasIntMin = By.id("mola-0-intMin");
    private By linputLimiteDeMolasIntermMax = By.id("mola-0-intermMax");
    private By linputLimiteDeMolasIntermMin = By.id("mola-0-intermMin");
    private By btnAddLimiteDeMolas = By.id("button-add-mola");
    private By msgNomeDuplicadoLimiteDeMolas = By.cssSelector("sro-limites-de-molas > div > div.col-12.ng-star-inserted > table > tbody > tr:nth-child(2) > td.descricaoMola > sro-input-error-msg");

    //Mola cunha
    private By separatorLimiteDeMolaCunha = By.id("mola-cunha-content-toggle");
    private IntFunction<By> linputMolaCunhaTipoDeMola = (index) -> By.id("molaCunha-" + index + "-descricaoMolaCunha");
    private By linputMolaCunhaExtMax = By.id("molaCunha-0-extMax");
    private By linputMolaCunhaExtMin = By.id("molaCunha-0-extMin");
    private By linputMolaCunhaIntMax = By.id("molaCunha-0-intMax");
    private By linputMolaCunhaIntMin = By.id("molaCunha-0-intMin");
    private By btnAddMolaCunha = By.id("button-add-mola-cunha");
    private By msgNomeDuplicadoLimiteDeMolaCunha = By.cssSelector("sro-mola-cunha > div > div.col-10.ng-star-inserted > table > tbody > tr:nth-child(2) > td.descricaoMolaCunha > sro-input-error-msg");

    //Condição do vagão quanto a carga
    private By separatorCondicaoDoVagaoQuantoACarga = By.id("condicao-do-vagao-quanto-a-carga-content-toggle");

    //Informação do rolamento
    private By separatorInformacaoDoRolamento = By.id("informacao-do-rolamento-content-toggle");

    //Cor e ano de substituição ou lubrificação do rolamento
    private By separatorCorEAnoDeSubstituicao = By.id("cor-e-ano-de-substitiucao-ou-lubrificacao-do-rolamento-content-toggle");

    private int getPositionBlocoConfigVagao() {
        return countChildElement(formConfiguracaoDeVagao, "sro-title-label") - 1;
    }

    public void clicarSeparatorConfiguracaoVagao() {
        clickAndHighlight(saparatorConfiguracaoMaterialRodanteVagao);
    }

    public void clicarBtnAdicionarNovaConfiguracaoVagao() {
        scrollToElement(btnAddConfiguracaoVagao);
        clickAndHighlight(btnAddConfiguracaoVagao);
    }

    public void preencheConfiguracaoVagao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        sendKeys(inputNomeConfiguracaoVagao, configuracaoMaterialRodanteVagao.getNomeConfiguracaoVagao());
        if (configuracaoMaterialRodanteVagao.isConfiguracaoVagaoAtiva()) {
            clickWithAction(raddioButtonConfiguracaoVagaoAtivaSim);
        } else {
            clickWithAction(raddioButtonConfiguracaoVagaoAtivaNao);
        }
    }

    public void preencheLimitesDoRodeiro(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimitesDoRodeiro);
        clickAndHighlight(separatorLimitesDoRodeiro);
        expectElementVisible(inputBitolaMinimaLarga);
        sendKeys(inputBitolaMinimaLarga, configuracaoMaterialRodanteVagao.getBitolaMinimaLarga());
        sendKeys(inputBitolaMinimaMetrica, configuracaoMaterialRodanteVagao.getBitolaMinimaMetrica());
        sendKeys(inputBitolaMaximaLarga, configuracaoMaterialRodanteVagao.getBitolaMaximaLarga());
        sendKeys(inputBitolaMaximaMetrica, configuracaoMaterialRodanteVagao.getBitolaMaximaMetrica());
        sendKeys(inputEspessuraFrisoLarga, configuracaoMaterialRodanteVagao.getEspessuraFrisoLarga());
        sendKeys(inputEspessuraFrisoMetrica, configuracaoMaterialRodanteVagao.getEspessuraFrisoMetrica());
        sendKeys(inputAlturaFrisoLarga, configuracaoMaterialRodanteVagao.getAlturaFrisoLarga());
        sendKeys(inputAlturaFrisoMetrica, configuracaoMaterialRodanteVagao.getAlturaFrisoMetrica());
        sendKeys(inputBandagemLarga, configuracaoMaterialRodanteVagao.getBandagemLarga());
        sendKeys(inputBandagemMetrica, configuracaoMaterialRodanteVagao.getBandagemMetrica());
        sendKeys(inputHollowLarga, configuracaoMaterialRodanteVagao.getHollowLarga());
        sendKeys(inputHollowMetrica, configuracaoMaterialRodanteVagao.getHollowMetrica());
        sendKeys(inputDiferencaMaximaEspessuraDeFriso, configuracaoMaterialRodanteVagao.getDiferencaMaximaEspessuraDeFriso());
    }

    public void preencheInformacaoSobreMangaDeEixo(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao, int linha, boolean isEdicao) {
        scrollToElement(separatorInformacaoSobreMangaDeEixo);
        clickAndHighlight(separatorInformacaoSobreMangaDeEixo);
        expectElementVisible(btnAddManga);
        if (!isEdicao) {
            clickAndHighlight(btnAddManga);
        }
        sendKeys(inputManga.apply(linha - 1), configuracaoMaterialRodanteVagao.getManga());
        sendKeys(inputLetraNaBitolaMetrica.apply(linha - 1), configuracaoMaterialRodanteVagao.getLetraNaBitolaMetrica());
        sendKeys(inputLetraNaBitolaLarga.apply(linha - 1), configuracaoMaterialRodanteVagao.getLetraNaBitolaLarga());
    }

    public void preencheTipoDeManutencao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao, int linha, boolean isEdicao) {
        scrollToElement(separatorTipoDeManutencao);
        clickAndHighlight(separatorTipoDeManutencao);
        expectElementVisible(btnAddTipoDeManutencao);
        if (!isEdicao) {
            clickAndHighlight(btnAddTipoDeManutencao);
        }
        sendKeys(inputTiposManutencao.apply(linha - 1), configuracaoMaterialRodanteVagao.getTiposManutencao());
        clickAndHighlight(selectBitolaTipoManutencao.apply(linha - 1));
        if (configuracaoMaterialRodanteVagao.isBitolaTipoManutencao()) {
            clickAndHighlight(optionBitolaTipoManutencaoLarga.apply(linha - 1));
        } else {
            clickAndHighlight(optionBitolaTipoManutencaoMetrica.apply(linha - 1));
        }
    }

    public void preencheLimiteDeDiferencaDeBotoes(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDiferencaDeBotoes);
        clickAndHighlight(separatorLimiteDiferencaDeBotoes);
        expectElementVisible(inputLimiteDiferencaBotoesMaximo);
        sendKeys(inputLimiteDiferencaBotoesMaximo, configuracaoMaterialRodanteVagao.getLimiteDiferencaBotoesMaximo());
    }

    public void preencheLimiteDePratoDePiao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao, int linha, boolean isEdicao) {
        scrollToElement(separatorLimiteDePratoDePiao);
        clickAndHighlight(separatorLimiteDePratoDePiao);
        expectElementVisible(btnAddLimitePratoDePiao);
        if (!isEdicao) {
            clickAndHighlight(btnAddLimitePratoDePiao);
        }
        sendKeys(inputPratoPiao.apply(linha - 1), configuracaoMaterialRodanteVagao.getPratoPiao());

        clickAndHighlight(selectBitolaLimitePratoDePiao.apply(linha - 1));
        if (configuracaoMaterialRodanteVagao.isBitolaLimitePratoDePiao()) {
            clickAndHighlight(optionBitolaLimitePratoDePiaoLarga.apply(linha - 1));
        } else {
            clickAndHighlight(optionBitolaLimitePratoDePiaoMetrica.apply(linha - 1));
        }

        sendKeys(inputSupMin.apply(0), configuracaoMaterialRodanteVagao.getSupMin());
        sendKeys(inputSupMax.apply(0), configuracaoMaterialRodanteVagao.getSupMax());
        sendKeys(inputAltMin.apply(0), configuracaoMaterialRodanteVagao.getAltMin());
        sendKeys(inputAltMax.apply(0), configuracaoMaterialRodanteVagao.getAltMax());
        sendKeys(inputInfMin.apply(0), configuracaoMaterialRodanteVagao.getInfMin());
        sendKeys(inputInfMax.apply(0), configuracaoMaterialRodanteVagao.getInfMax());
        sendKeys(inputProfMin.apply(0), configuracaoMaterialRodanteVagao.getProfMin());
        sendKeys(inputProfMax.apply(0), configuracaoMaterialRodanteVagao.getProfMax());
    }

    public void preencheLimiteDeFolgaDePratoPiao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDeFolgaPratoDePiao);
        clickAndHighlight(separatorLimiteDeFolgaPratoDePiao);
        expectElementVisible(inputFolgaMinimaPrato);
        sendKeys(inputFolgaMinimaPrato, configuracaoMaterialRodanteVagao.getFolgaMinimaPrato());
        sendKeys(inputFolgaMaximaPrato, configuracaoMaterialRodanteVagao.getFolgaMaximaPrato());
        sendKeys(inputFolgaVerticalMinimaPrato, configuracaoMaterialRodanteVagao.getFolgaVerticalMinimaPrato());
        sendKeys(inputFolgaVerticalMaximaPrato, configuracaoMaterialRodanteVagao.getFolgaVerticalMaximaPrato());
    }

    public void preencheLimiteCunhaDeFriccao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao, int linha, boolean isEdicao) {
        scrollToElement(separatorLimiteDeCunhasDeFriccao);
        clickAndHighlight(separatorLimiteDeCunhasDeFriccao);
        expectElementVisible(btnAddLimiteCunhaFriccao);
        if (!isEdicao) {
            clickAndHighlight(btnAddLimiteCunhaFriccao);
        }
        sendKeys(inputTruqueCunhaFriccao.apply(linha - 1), configuracaoMaterialRodanteVagao.getTruqueCunhaFriccao());
        sendKeys(inputLimiteAlturaCunhaFriccao.apply(linha - 1), configuracaoMaterialRodanteVagao.getLimiteAlturaCunhaFriccao());
    }

    public void preencheLimitesAmparadoBalanco(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao, int linha, boolean isEdicao) {
        scrollToElement(separatorLimiteDeAmparoBalanco);
        clickAndHighlight(separatorLimiteDeAmparoBalanco);
        expectElementVisible(btnAddLimiteAmparoBalanço);
        if (!isEdicao) {
            clickAndHighlight(btnAddLimiteAmparoBalanço);
        }
        sendKeys(inputTipoDeAmparoBalanco.apply(linha - 1), configuracaoMaterialRodanteVagao.getTipoDeAmpartoBalanco());
        sendKeys(inputAmparoBalancoMinimo, configuracaoMaterialRodanteVagao.getAmpartoBalancoMinimo());
        sendKeys(inputAmparoBalancoMaximo, configuracaoMaterialRodanteVagao.getAmpartoBalancoMaximo());
        sendKeys(inputAmparoBalancoFolgaMinima, configuracaoMaterialRodanteVagao.getAmpartoBalancoFolgaMinima());
        sendKeys(inputAmparoBalancoFolgaMaxima, configuracaoMaterialRodanteVagao.getAmpartoBalancoFolgaMaxima());
        sendKeys(inputAmparoBalancoDiferencaMaximaSomaCruzada, configuracaoMaterialRodanteVagao.getAmpartoBalancoDiferencaMaximaSomaCruzada());
        sendKeys(inputAmparoBalancoDesgasteConcavoDaChapa, configuracaoMaterialRodanteVagao.getAmpartoBalancoDesgasteConcavoDaChapa());
    }

    public void preencheLimitesDeAlturaDeEngate(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDeAlturaDeEngate);
        clickAndHighlight(separatorLimiteDeAlturaDeEngate);
        expectElementVisible(inputAlturaEngateMetricaVazioAltMin);
        sendKeys(inputAlturaEngateMetricaVazioAltMin, configuracaoMaterialRodanteVagao.getAlturaEngateMetricaVazioAltMin());
        sendKeys(inputAlturaEngateMetricaVazioAltMax, configuracaoMaterialRodanteVagao.getAlturaEngateMetricaVazioAltMax());
        sendKeys(inputAlturaEngateMetricaCarregadoAltMin, configuracaoMaterialRodanteVagao.getAlturaEngateMetricaCarregadoAltMin());
        sendKeys(inputAlturaEngateMetricaCarregadoAltMax, configuracaoMaterialRodanteVagao.getAlturaEngateMetricaCarregadoAltMax());
        sendKeys(inputAlturaEngateLargaVazioAltMin, configuracaoMaterialRodanteVagao.getAlturaEngateLargaVazioAltMin());
        sendKeys(inputAlturaEngateLargaVazioAltMax, configuracaoMaterialRodanteVagao.getAlturaEngateLargaVazioAltMax());
        sendKeys(inputAlturaEngateLargaCarregadoAltMin, configuracaoMaterialRodanteVagao.getAlturaEngateLargaCarregadoAltMin());
        sendKeys(inputAlturaEngateLargaCarregadoAltMax, configuracaoMaterialRodanteVagao.getAlturaEngateLargaCarregadoAltMax());
    }

    public void preencheLimitesDeMolas(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao, int linha, boolean isEdicao) {
        scrollToElement(separatorLimiteDeMolas);
        clickAndHighlight(separatorLimiteDeMolas);
        expectElementVisible(btnAddLimiteDeMolas);
        if (!isEdicao) {
            clickAndHighlight(btnAddLimiteDeMolas);
        }
        sendKeys(linputLimiteDeMolasMola.apply(linha - 1), configuracaoMaterialRodanteVagao.getLimiteDeMolasMola());
        sendKeys(linputLimiteDeMolasExtMax, configuracaoMaterialRodanteVagao.getLimiteDeMolasExtMax());
        sendKeys(linputLimiteDeMolasExtMin, configuracaoMaterialRodanteVagao.getLimiteDeMolasExtMin());
        sendKeys(linputLimiteDeMolasIntMax, configuracaoMaterialRodanteVagao.getLimiteDeMolasIntMax());
        sendKeys(linputLimiteDeMolasIntMin, configuracaoMaterialRodanteVagao.getLimiteDeMolasIntMin());
        sendKeys(linputLimiteDeMolasIntermMax, configuracaoMaterialRodanteVagao.getLimiteDeMolasIntermMax());
        sendKeys(linputLimiteDeMolasIntermMin, configuracaoMaterialRodanteVagao.getLimiteDeMolasIntermMin());
    }

    public void preencheMolaCunha(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao, int linha, boolean isEdicao) {
        scrollToElement(separatorLimiteDeMolaCunha);
        clickAndHighlight(separatorLimiteDeMolaCunha);
        expectElementVisible(btnAddMolaCunha);
        if (!isEdicao) {
            clickAndHighlight(btnAddMolaCunha);
        }
        sendKeys(linputMolaCunhaTipoDeMola.apply(linha - 1), configuracaoMaterialRodanteVagao.getMolaCunhaTipoDeMola());
        sendKeys(linputMolaCunhaExtMax, configuracaoMaterialRodanteVagao.getMolaCunhaExtMax());
        sendKeys(linputMolaCunhaExtMin, configuracaoMaterialRodanteVagao.getMolaCunhaExtMin());
        sendKeys(linputMolaCunhaIntMax, configuracaoMaterialRodanteVagao.getMolaCunhaIntMax());
        sendKeys(linputMolaCunhaIntMin, configuracaoMaterialRodanteVagao.getMolaCunhaIntMin());
    }

    public void clicarBtnSalvarInformacoes() {
        scrollToElement(btnSalvarInformacoes);
        clickAndHighlight(btnSalvarInformacoes);
    }

    public void validaConfiguracaoVagao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(inputNomeConfiguracaoVagao);
        Assert.assertEquals(getValue(inputNomeConfiguracaoVagao), configuracaoMaterialRodanteVagao.getNomeConfiguracaoVagao());
        if (configuracaoMaterialRodanteVagao.isConfiguracaoVagaoAtiva()) {
            Assert.assertTrue(isRadioChecked(raddioButtonConfiguracaoVagaoAtivaSim));
        } else {
            Assert.assertTrue(isRadioChecked(raddioButtonConfiguracaoVagaoAtivaNao));
        }
    }

    public void validaLimitesDoRodeiro(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimitesDoRodeiro);
        clickAndHighlight(separatorLimitesDoRodeiro);
        expectElementVisible(inputBitolaMinimaLarga);
        Assert.assertEquals(getValue(inputBitolaMinimaLarga), configuracaoMaterialRodanteVagao.getBitolaMinimaLarga());
        Assert.assertEquals(getValue(inputBitolaMinimaMetrica), configuracaoMaterialRodanteVagao.getBitolaMinimaMetrica());
        Assert.assertEquals(getValue(inputBitolaMaximaLarga), configuracaoMaterialRodanteVagao.getBitolaMaximaLarga());
        Assert.assertEquals(getValue(inputBitolaMaximaMetrica), configuracaoMaterialRodanteVagao.getBitolaMaximaMetrica());
        Assert.assertEquals(getValue(inputEspessuraFrisoLarga), configuracaoMaterialRodanteVagao.getEspessuraFrisoLarga());
        Assert.assertEquals(getValue(inputEspessuraFrisoMetrica), configuracaoMaterialRodanteVagao.getEspessuraFrisoMetrica());
        Assert.assertEquals(getValue(inputAlturaFrisoLarga), configuracaoMaterialRodanteVagao.getAlturaFrisoLarga());
        Assert.assertEquals(getValue(inputAlturaFrisoMetrica), configuracaoMaterialRodanteVagao.getAlturaFrisoMetrica());
        Assert.assertEquals(getValue(inputBandagemLarga), configuracaoMaterialRodanteVagao.getBandagemLarga());
        Assert.assertEquals(getValue(inputBandagemMetrica), configuracaoMaterialRodanteVagao.getBandagemMetrica());
        Assert.assertEquals(getValue(inputHollowLarga), configuracaoMaterialRodanteVagao.getHollowLarga());
        Assert.assertEquals(getValue(inputHollowMetrica), configuracaoMaterialRodanteVagao.getHollowMetrica());
        Assert.assertEquals(getValue(inputDiferencaMaximaEspessuraDeFriso), configuracaoMaterialRodanteVagao.getDiferencaMaximaEspessuraDeFriso());
    }

    public void validaInformacaoSobreMangaDeEixo(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorInformacaoSobreMangaDeEixo);
        clickAndHighlight(separatorInformacaoSobreMangaDeEixo);
        expectElementVisible(inputManga.apply(0));
        Assert.assertEquals(getValue(inputManga.apply(0)), configuracaoMaterialRodanteVagao.getManga());
        Assert.assertEquals(getValue(inputLetraNaBitolaMetrica.apply(0)), configuracaoMaterialRodanteVagao.getLetraNaBitolaMetrica());
        Assert.assertEquals(getValue(inputLetraNaBitolaLarga.apply(0)), configuracaoMaterialRodanteVagao.getLetraNaBitolaLarga());
    }

    public void validaTipoDeManutencao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorTipoDeManutencao);
        clickAndHighlight(separatorTipoDeManutencao);
        expectElementVisible(btnAddTipoDeManutencao);
        Assert.assertEquals(getValue(inputTiposManutencao.apply(0)), configuracaoMaterialRodanteVagao.getTiposManutencao());

        if (configuracaoMaterialRodanteVagao.isBitolaTipoManutencao()) {
            Assert.assertEquals(getText(selectBitolaTipoManutencao.apply(0)), "Larga");
        } else {
            Assert.assertEquals(getText(selectBitolaTipoManutencao.apply(0)), "Métrica");
        }
    }

    public void validaLimiteCunhaDeFriccao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDeCunhasDeFriccao);
        clickAndHighlight(separatorLimiteDeCunhasDeFriccao);
        expectElementVisible(inputTruqueCunhaFriccao.apply(0));
        Assert.assertEquals(getValue(inputTruqueCunhaFriccao.apply(0)), configuracaoMaterialRodanteVagao.getTruqueCunhaFriccao());
        Assert.assertEquals(getValue(inputLimiteAlturaCunhaFriccao.apply(0)), String.valueOf(configuracaoMaterialRodanteVagao.getLimiteAlturaCunhaFriccao()));
    }

    public void validaLimiteDeDiferencaDeBotoes(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDiferencaDeBotoes);
        clickAndHighlight(separatorLimiteDiferencaDeBotoes);
        expectElementVisible(inputLimiteDiferencaBotoesMaximo);
        Assert.assertEquals(getValue(inputLimiteDiferencaBotoesMaximo), String.valueOf(configuracaoMaterialRodanteVagao.getLimiteDiferencaBotoesMaximo()));
    }

    public void validaLimiteDePratoDePiao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDePratoDePiao);
        clickAndHighlight(separatorLimiteDePratoDePiao);
        expectElementVisible(inputPratoPiao.apply(0));
        Assert.assertEquals(getValue(inputPratoPiao.apply(0)), configuracaoMaterialRodanteVagao.getPratoPiao());

        if (configuracaoMaterialRodanteVagao.isBitolaLimitePratoDePiao()) {
            Assert.assertEquals(getText(selectBitolaLimitePratoDePiao.apply(0)), "Larga");
        } else {
            Assert.assertEquals(getText(selectBitolaLimitePratoDePiao.apply(0)), "Métrica");
        }

        Assert.assertEquals(getValue(inputSupMin.apply(0)), configuracaoMaterialRodanteVagao.getSupMin());
        Assert.assertEquals(getValue(inputSupMax.apply(0)), configuracaoMaterialRodanteVagao.getSupMax());
        Assert.assertEquals(getValue(inputAltMin.apply(0)), configuracaoMaterialRodanteVagao.getAltMin());
        Assert.assertEquals(getValue(inputAltMax.apply(0)), configuracaoMaterialRodanteVagao.getAltMax());
        Assert.assertEquals(getValue(inputInfMin.apply(0)), configuracaoMaterialRodanteVagao.getInfMin());
        Assert.assertEquals(getValue(inputInfMax.apply(0)), configuracaoMaterialRodanteVagao.getInfMax());
        Assert.assertEquals(getValue(inputProfMin.apply(0)), configuracaoMaterialRodanteVagao.getProfMin());
        Assert.assertEquals(getValue(inputProfMax.apply(0)), configuracaoMaterialRodanteVagao.getProfMax());
    }

    public void validaLimiteDeFolgaDePratoPiao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDeFolgaPratoDePiao);
        clickAndHighlight(separatorLimiteDeFolgaPratoDePiao);
        expectElementVisible(inputFolgaMinimaPrato);
        Assert.assertEquals(getValue(inputFolgaMinimaPrato), String.valueOf(configuracaoMaterialRodanteVagao.getFolgaMinimaPrato()));
        Assert.assertEquals(getValue(inputFolgaMaximaPrato), String.valueOf(configuracaoMaterialRodanteVagao.getFolgaMaximaPrato()));
        Assert.assertEquals(getValue(inputFolgaVerticalMinimaPrato), String.valueOf(configuracaoMaterialRodanteVagao.getFolgaVerticalMinimaPrato()));
        Assert.assertEquals(getValue(inputFolgaVerticalMaximaPrato), String.valueOf(configuracaoMaterialRodanteVagao.getFolgaVerticalMaximaPrato()));
    }


    public void validaLimitesAmparadoBalanco(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDeAmparoBalanco);
        clickAndHighlight(separatorLimiteDeAmparoBalanco);
        expectElementVisible(btnAddLimiteAmparoBalanço);
        Assert.assertEquals(getValue(inputTipoDeAmparoBalanco.apply(0)), configuracaoMaterialRodanteVagao.getTipoDeAmpartoBalanco());
        Assert.assertEquals(getValue(inputAmparoBalancoMinimo), String.valueOf(configuracaoMaterialRodanteVagao.getAmpartoBalancoMinimo()));
        Assert.assertEquals(getValue(inputAmparoBalancoMaximo), String.valueOf(configuracaoMaterialRodanteVagao.getAmpartoBalancoMaximo()));
        Assert.assertEquals(getValue(inputAmparoBalancoFolgaMinima), String.valueOf(configuracaoMaterialRodanteVagao.getAmpartoBalancoFolgaMinima()));
        Assert.assertEquals(getValue(inputAmparoBalancoFolgaMaxima), String.valueOf(configuracaoMaterialRodanteVagao.getAmpartoBalancoFolgaMaxima()));
        Assert.assertEquals(getValue(inputAmparoBalancoDiferencaMaximaSomaCruzada), String.valueOf(configuracaoMaterialRodanteVagao.getAmpartoBalancoDiferencaMaximaSomaCruzada()));
        Assert.assertEquals(getValue(inputAmparoBalancoDesgasteConcavoDaChapa), String.valueOf(configuracaoMaterialRodanteVagao.getAmpartoBalancoDesgasteConcavoDaChapa()));
    }

    public void validaLimitesDeAlturaDeEngate(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDeAlturaDeEngate);
        clickAndHighlight(separatorLimiteDeAlturaDeEngate);
        expectElementVisible(inputAlturaEngateMetricaVazioAltMin);
        Assert.assertEquals(getValue(inputAlturaEngateMetricaVazioAltMin), String.valueOf(configuracaoMaterialRodanteVagao.getAlturaEngateMetricaVazioAltMin()));
        Assert.assertEquals(getValue(inputAlturaEngateMetricaVazioAltMax), String.valueOf(configuracaoMaterialRodanteVagao.getAlturaEngateMetricaVazioAltMax()));
        Assert.assertEquals(getValue(inputAlturaEngateMetricaCarregadoAltMin), String.valueOf(configuracaoMaterialRodanteVagao.getAlturaEngateMetricaCarregadoAltMin()));
        Assert.assertEquals(getValue(inputAlturaEngateMetricaCarregadoAltMax), String.valueOf(configuracaoMaterialRodanteVagao.getAlturaEngateMetricaCarregadoAltMax()));
        Assert.assertEquals(getValue(inputAlturaEngateLargaVazioAltMin), String.valueOf(configuracaoMaterialRodanteVagao.getAlturaEngateLargaVazioAltMin()));
        Assert.assertEquals(getValue(inputAlturaEngateLargaVazioAltMax), String.valueOf(configuracaoMaterialRodanteVagao.getAlturaEngateLargaVazioAltMax()));
        Assert.assertEquals(getValue(inputAlturaEngateLargaCarregadoAltMin), String.valueOf(configuracaoMaterialRodanteVagao.getAlturaEngateLargaCarregadoAltMin()));
        Assert.assertEquals(getValue(inputAlturaEngateLargaCarregadoAltMax), String.valueOf(configuracaoMaterialRodanteVagao.getAlturaEngateLargaCarregadoAltMax()));
    }

    public void validaLimitesDeMolas(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDeMolas);
        clickAndHighlight(separatorLimiteDeMolas);
        expectElementVisible(btnAddLimiteDeMolas);
        Assert.assertEquals(getValue(linputLimiteDeMolasMola.apply(0)), configuracaoMaterialRodanteVagao.getLimiteDeMolasMola());
        Assert.assertEquals(getValue(linputLimiteDeMolasExtMax), String.valueOf(configuracaoMaterialRodanteVagao.getLimiteDeMolasExtMax()));
        Assert.assertEquals(getValue(linputLimiteDeMolasExtMin), String.valueOf(configuracaoMaterialRodanteVagao.getLimiteDeMolasExtMin()));
        Assert.assertEquals(getValue(linputLimiteDeMolasIntMax), String.valueOf(configuracaoMaterialRodanteVagao.getLimiteDeMolasIntMax()));
        Assert.assertEquals(getValue(linputLimiteDeMolasIntMin), String.valueOf(configuracaoMaterialRodanteVagao.getLimiteDeMolasIntMin()));
        Assert.assertEquals(getValue(linputLimiteDeMolasIntermMax), String.valueOf(configuracaoMaterialRodanteVagao.getLimiteDeMolasIntermMax()));
        Assert.assertEquals(getValue(linputLimiteDeMolasIntermMin), String.valueOf(configuracaoMaterialRodanteVagao.getLimiteDeMolasIntermMin()));
    }

    public void validaMolaCunha(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(separatorLimiteDeMolaCunha);
        clickAndHighlight(separatorLimiteDeMolaCunha);
        expectElementVisible(btnAddMolaCunha);
        Assert.assertEquals(getValue(linputMolaCunhaTipoDeMola.apply(0)), configuracaoMaterialRodanteVagao.getMolaCunhaTipoDeMola());
        Assert.assertEquals(getValue(linputMolaCunhaExtMax), String.valueOf(configuracaoMaterialRodanteVagao.getMolaCunhaExtMax()));
        Assert.assertEquals(getValue(linputMolaCunhaExtMin), String.valueOf(configuracaoMaterialRodanteVagao.getMolaCunhaExtMin()));
        Assert.assertEquals(getValue(linputMolaCunhaIntMax), String.valueOf(configuracaoMaterialRodanteVagao.getMolaCunhaIntMax()));
        Assert.assertEquals(getValue(linputMolaCunhaIntMin), String.valueOf(configuracaoMaterialRodanteVagao.getMolaCunhaIntMin()));
    }

    public void validaStatusBlocoDeConfiguracaoVagao(ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao) {
        scrollToElement(statusBlocoConfiguracaoVagao.apply(getPositionBlocoConfigVagao()));
        if (configuracaoMaterialRodanteVagao.isConfiguracaoVagaoAtiva()) {
            Assert.assertEquals(getText(statusBlocoConfiguracaoVagao.apply(getPositionBlocoConfigVagao())), "Ativo");
        } else {
            Assert.assertEquals(getText(statusBlocoConfiguracaoVagao.apply(getPositionBlocoConfigVagao())), "Inativo");
        }
        Assert.assertEquals(getText(nomeBlocoConfiguracaoVagao.apply(getPositionBlocoConfigVagao())), configuracaoMaterialRodanteVagao.getNomeConfiguracaoVagao().toUpperCase());
    }

    public void clicarBtnEditarDoBlocoDeConfigVagao() {
        clickWithAction(btnEditarBlocoConfigVagao.apply(getPositionBlocoConfigVagao()));
    }

    public void validaMsgNomeDuplicadoInformacaoSobreMangaDeEixo() {
        expectText(msgNomeDuplicadoManga, "*Existe mais de um registro com esse mesmo nome.");
        Assert.assertEquals(getText(msgNomeDuplicadoManga), "*Existe mais de um registro com esse mesmo nome.");
    }

    public void validaMsgTipoDuplicadoTipoDeManutencao() {
        expectText(msgTipoDuplicadoTipoManutencao, "*Existe mais de um registro com esse mesmo nome.");
        Assert.assertEquals(getText(msgTipoDuplicadoTipoManutencao), "*Existe mais de um registro com esse mesmo nome.");
    }

    public void validaMsgNomeDuplicadoLimiteCunhaFriccao() {
        expectText(msgNomeDuplicadoLimiteDeCunhasDeFriccao, "*Existe mais de um registro com esse mesmo nome.");
        Assert.assertEquals(getText(msgNomeDuplicadoLimiteDeCunhasDeFriccao), "*Existe mais de um registro com esse mesmo nome.");
    }

    public void validaMsgNomeDuplicadoLimiteDePratoDePiao() {
        expectText(msgNomeDuplicadoLimiteDePratoDePiao, "*Existe mais de um registro com esse mesmo nome.");
        Assert.assertEquals(getText(msgNomeDuplicadoLimiteDePratoDePiao), "*Existe mais de um registro com esse mesmo nome.");
    }

    public void validaMsgNomeDuplicadoLimitesAmparadoBalanco() {
        expectText(msgNomeDuplicadoLimiteAmparoBalanco, "*Existe mais de um registro com esse mesmo nome.");
        Assert.assertEquals(getText(msgNomeDuplicadoLimiteAmparoBalanco), "*Existe mais de um registro com esse mesmo nome.");
    }

    public void validaMsgNomeDuplicadoLimitesDeMolas() {
        expectText(msgNomeDuplicadoLimiteDeMolas, "*Existe mais de um registro com esse mesmo nome.");
        Assert.assertEquals(getText(msgNomeDuplicadoLimiteDeMolas), "*Existe mais de um registro com esse mesmo nome.");
    }

    public void validaMsgNomeDuplicadoMolaCunha() {
        expectText(msgNomeDuplicadoLimiteDeMolaCunha, "*Existe mais de um registro com esse mesmo nome.");
        Assert.assertEquals(getText(msgNomeDuplicadoLimiteDeMolaCunha), "*Existe mais de um registro com esse mesmo nome.");
    }
}
