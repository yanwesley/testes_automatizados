package br.com.pom.sro.configuracoes;

import br.com.api.model.configuracoes.ConfiguracoesDashboard;
import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.function.IntFunction;

public class DashboardSROPage extends GeralPage {

    private By tabDashboard = By.id("tab-configuracao-dashboard");
    private By btnSalvarConfiguracoes = By.id("salvar-tolerancias");
    private IntFunction<By> inputToleranciaAcidenteNorte = (index) -> By.id("input-tolerancia-acidente-NORTE-" + index);
    private IntFunction<By> inputToleranciaAcidenteSul = (index) -> By.id("input-tolerancia-acidente-SUL-" + index);
    private IntFunction<By> inputToleranciaAcidenteCentral = (index) -> By.id("input-tolerancia-acidente-CENTRAL-" + index);
    private IntFunction<By> inputToleranciaGravidadeNorte = (index) -> By.id("input-tolerancia-gravidade-NORTE-" + index);
    private IntFunction<By> inputToleranciaGravidadeSul = (index) -> By.id("input-tolerancia-gravidade-SUL-" + index);
    private IntFunction<By> inputToleranciaGravidadeCentral = (index) -> By.id("input-tolerancia-gravidade-CENTRAL-" + index);

    public void clicarTabDashboard() {
        clickAndHighlight(tabDashboard);
    }

    public void clicarBtnSalvarConfiguraçõesDashboard() {
        clickAndHighlight(btnSalvarConfiguracoes);
    }

    public void preencheToleranciaAcidenteSul(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            sendKeys(inputToleranciaAcidenteSul.apply(i), configuracoesDashboard.getToleranciaAcidenteSul().get(i - 1));
        }
    }

    public void preencheToleranciaAcidenteNorte(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            sendKeys(inputToleranciaAcidenteNorte.apply(i), configuracoesDashboard.getToleranciaAcidenteNorte().get(i - 1));
        }
    }

    public void preencheToleranciaAcidenteCentral(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            sendKeys(inputToleranciaAcidenteCentral.apply(i), configuracoesDashboard.getToleranciaAcidenteCentral().get(i - 1));
        }
    }

    public void preencheToleranciaGravidadeSul(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            sendKeys(inputToleranciaGravidadeSul.apply(i), configuracoesDashboard.getToleranciaGravidadeSul().get(i - 1));
        }
    }

    public void preencheToleranciaGravidadeNorte(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            sendKeys(inputToleranciaGravidadeNorte.apply(i), configuracoesDashboard.getToleranciaGravidadeNorte().get(i - 1));
        }
    }

    public void preencheToleranciaGravidadeCentral(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            sendKeys(inputToleranciaGravidadeCentral.apply(i), configuracoesDashboard.getToleranciaGravidadeCentral().get(i - 1));
        }
    }

    public void validaToleranciaAcidenteNorte(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            Assert.assertEquals(getValue(inputToleranciaAcidenteNorte.apply(i)), String.valueOf(configuracoesDashboard.getToleranciaAcidenteNorte().get(i - 1)));
        }
    }

    public void validaToleranciaAcidenteSul(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            Assert.assertEquals(getValue(inputToleranciaAcidenteSul.apply(i)), String.valueOf(configuracoesDashboard.getToleranciaAcidenteSul().get(i - 1)));
        }
    }

    public void validaToleranciaAcidenteCentral(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            Assert.assertEquals(getValue(inputToleranciaAcidenteCentral.apply(i)), String.valueOf(configuracoesDashboard.getToleranciaAcidenteCentral().get(i - 1)));
        }
    }

    public void validaToleranciaGravidadeNorte(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            Assert.assertEquals(getValue(inputToleranciaGravidadeNorte.apply(i)), formatToDecimal(configuracoesDashboard.getToleranciaGravidadeNorte().get(i - 1)));
        }
    }

    public void validaToleranciaGravidadeSul(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            Assert.assertEquals(getValue(inputToleranciaGravidadeSul.apply(i)), formatToDecimal(configuracoesDashboard.getToleranciaGravidadeSul().get(i - 1)));
        }
    }

    public void validaToleranciaGravidadeCentral(ConfiguracoesDashboard configuracoesDashboard) {
        for (int i = 1; i <= 12; i++) {
            Assert.assertEquals(getValue(inputToleranciaGravidadeCentral.apply(i)), formatToDecimal(configuracoesDashboard.getToleranciaGravidadeCentral().get(i - 1)));
        }
    }
}
