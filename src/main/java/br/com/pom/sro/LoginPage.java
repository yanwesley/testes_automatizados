package br.com.pom.sro;

import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.incidente.Incidente;
import org.openqa.selenium.By;

import static br.com.pom.api.UtilsAPI.setTypeUser;
import static br.com.utils.WebDriverHelper.resetDriver;

public class LoginPage extends GeralPage {

    private String CCO = "TLG.CCO";
    private String CCO_VIA = "TLG.CCO_VIA";
    private String CIPIA_MAT_RODANTE = "TLG.CIPIA_MATERIAL_RODANTE";
    private String CIPIA_OPERACAO = "TLG.CIPIA_OPERACAO";
    private String CIPIA_VIA = "TLG.CIPIA_VIA";
    private String CONTROLE_PERDAS = "TLG.CONTROLE_PERDAS";
    private String CIPIA_OUTROS = "CIPIA_OUTROS";
    private String CIPIA_TECNICA = "TLG.CIPIA_TECNICA";
    private String CIPIA_PRESIDENTE = "TLG.CIPIA_PRESIDENTE";
    private String SEGURANCA_PATRIMONIAL = "TLG.SEGURANCA_PATRIMONIAL";
    private String MEIO_AMBIENTE = "TLG.MEIO_AMBIENTE";
    private String SRO = "TLG.SRO";
    private String SST = "TLG.SST";
    private String TO = "TLG.TO";
    private String FINANCEIRO = "TLG.FINANCEIRO";

    private By selectUser = By.id("select-user-type");
    private By optionUserCCO = By.id("option-user-TLG.CCO");
    private By optionUserCCOVia = By.id("option-user-TLG.CCO_VIA");
    private By optionUserSRO = By.id("option-user-TLG.SRO");
    private By optionUserCipiaVia = By.id("option-user-TLG.CIPIA_VIA");
    private By optionUserCipiaOperacao = By.id("option-user-TLG.CIPIA_OPERACAO");
    private By optionUserTO = By.id("option-user-TLG.TO");
    private By optionUserMeioAmbiente = By.id("option-user-TLG.MEIO_AMBIENTE");
    private By optionUserSST = By.id("option-user-TLG.SST");
    private By optionUserControleDePerdas = By.id("option-user-TLG.CONTROLE_PERDAS");
    private By optionUserCipiaMaterialRodante = By.id("option-user-TLG.CIPIA_MATERIAL_RODANTE");
    private By optionUserCipiaSegurancaPatrimonial = By.id("option-user-TLG.SEGURANCA_PATRIMONIAL");
    private By optionUserCipiaTecnica = By.id("option-user-TLG.CIPIA_TECNICA");
    private By optionUserCipiaPresidente = By.id("option-user-TLG.CIPIA_PRESIDENTE");
    private By optionUserFinanceiro = By.id("option-user-TLG.FINANCEIRO");

    public LoginPage() {
        super();
    }

    public void selecionarUsuarioSRO() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserSRO);
        setTypeUser(SRO);
    }

    public void selecionarUsuarioCCO() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserCCO);
        setTypeUser(CCO);
    }

    public void selecionarUsuarioCCOVia() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserCCOVia);
        setTypeUser(CCO_VIA);
    }

    public void selecionarUsuarioCipiaVia() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserCipiaVia);
        setTypeUser(CIPIA_VIA);
    }

    public void selecionarUsuarioCipiaOperacao() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserCipiaOperacao);
        setTypeUser(CIPIA_OPERACAO);
    }

    public void selecionarUsuarioTO() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserTO);
        setTypeUser(TO);
    }

    public void selecionarUsuarioMeioAmbiente() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserMeioAmbiente);
        setTypeUser(MEIO_AMBIENTE);
    }

    public void selecionarUsuarioSST() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserSST);
        setTypeUser(SST);
    }

    public void selecionarUsuarioControleDePerdas() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserControleDePerdas);
        setTypeUser(CONTROLE_PERDAS);
    }

    public void selecionarUsuarioCipiaMaterialRodante() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserCipiaMaterialRodante);
        setTypeUser(CIPIA_MAT_RODANTE);
    }

    public void selecionarUsuarioCipiaSegurancaPatrimonial() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserCipiaSegurancaPatrimonial);
        setTypeUser(SEGURANCA_PATRIMONIAL);
    }

    public void selecionarUsuarioCIPIATecnica() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserCipiaTecnica);
        setTypeUser(CIPIA_TECNICA);
    }

    public void preencherUsuarioDonoDoPlano(PlanoDeAcao planoDeAcao) {
        resetDriver(planoDeAcao.getSolicitanteMatricula(), planoDeAcao.getSolicitante());
    }

    public void preencherUsuarioCriadorDoIncidente(Incidente incidente) {
        resetDriver(incidente.getMatriculaUsuarioCriacao(), incidente.getNomeUsuarioCriacao());
    }

    public void selecionarUsuarioCIPIAPresidente() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserCipiaPresidente);
        setTypeUser(CIPIA_PRESIDENTE);
    }

    public void selecionarUsuarioFinanceiro() {
        resetDriver();
        clickAndHighlight(selectUser);
        clickAndHighlight(optionUserFinanceiro);
        setTypeUser(FINANCEIRO);
    }
}
