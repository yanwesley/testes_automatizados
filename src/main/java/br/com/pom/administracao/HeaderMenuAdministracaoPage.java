package br.com.pom.administracao;

import br.com.pom.sro.GeralPage;
import org.openqa.selenium.By;

public class HeaderMenuAdministracaoPage extends GeralPage {

    private By aMenuSro = By.id("menu-sro");
    private By aMenuInfo = By.id("menu-info");

    public void acessarTABSRO() {
        clickAndHighlight(aMenuSro);
    }
}
