package br.com.utils;

import br.com.core.takescreenshot.TakeScreenShot;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.logging.Logger;

import static br.com.utils.FileUtils.readCsv;

public class ReporterUtils extends WebDriverHelper {

    private static Logger LOGGER = Logger.getLogger("InfoLogging");
    private String complementNameEvidence;

    public ReporterUtils() {
        super();
    }

    /**
     * Add json to the cucumber extent-report listener for generating ExtentReports and Klov
     *
     * @param json Text to be added to the report
     */
    public static void addJsonToReport(String json) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(json);
        String prettyJsonString = gson.toJson(je);
        Markup m = MarkupHelper.createCodeBlock(prettyJsonString);
        testScenario.get().write(m.getMarkup());
    }

    /**
     * Add comment to the cucumber-extent report listener for generating ExtentReports and Klov
     *
     * @param sMsg Text to be added to the report
     */
    public static synchronized void addLogToReport(String sMsg) {
        testScenario.get().write(sMsg);
    }

    /**
     * Add comment and screenshot to the cucumber-extent report listener for generating ExtentReports and Klov
     *
     * @param sMsg Text to be added to the report
     */
    public static synchronized void addFileToReport(String sMsg, String filePath) {
        Markup m = MarkupHelper.createLabel(readCsv(filePath).toString(), ExtentColor.TRANSPARENT);
        testScenario.get().write(sMsg);
        testScenario.get().write(m.getMarkup());
    }

    /**
     * Add comment and screenshot to the cucumber-extent report listener for generating ExtentReports and Klov
     *
     * @param sMsg Text to be added to the report
     */
    public static synchronized void addScreenshotToReport(String sMsg) {
        testScenario.get().write(sMsg);
        testScenario.get().embed(TakeScreenShot.getImageBytes(getDriverWeb()), "image/png");
    }

    /**
     * Support for function appendToReport, add a screenshot to the Cucumber Extent-Report
     */
    private synchronized static void appendPrintCucumber() {
        if (globalEvidence.equalsIgnoreCase("true")) {
            testScenario.get().embed(TakeScreenShot.getImageBytes(getDriverWeb()), "image/png");
        }
    }

    public static void logReport() {
        if (getDriverWeb() != null) {
            if (testScenario.get().isFailed()) {
                addScreenshotToReport("");
            }
        }
    }

}
