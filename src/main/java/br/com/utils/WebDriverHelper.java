package br.com.utils;

import br.com.core.properties.PropertiesManager;
import gherkin.ast.Feature;
import io.cucumber.core.api.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import static br.com.utils.FileUtils.createFolder;

public class WebDriverHelper {
    public static ThreadLocal<Scenario> testScenario = new ThreadLocal<>();
    public static ThreadLocal<Feature> testFeature = new ThreadLocal<>();
    public static String pathDownload;
    public static String highlightElementShow;
    public static String urlSinistro;
    public static String urlSindicancia;
    public static String urlSegurancaPatrimonial;
    public static String urlPlanoAcao;
    public static String urlIncidente;
    public static String globalEvidence;
    public static String urlSRO;
    public static String urlAdministracao;
    private static PropertiesManager setupProperties;
    private static WebDriver driver;
    private static String urlSeleniumGrid;
    private static String os;
    private static String browserName;

    public static WebDriver getDriverWeb() {
        if (driver == null) {
            initChromeDriver();
        }
        return driver;
    }

    public static void closeDriverWeb() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    public static void resetDriver() {
        if (driver != null) {
            driver.get(urlSRO);
        }
    }

    public static void resetDriver(String url) {
        if (driver != null) {
            driver.get(url);
        }
    }


    public static void resetDriver(String matricula, String nome) {
        if (driver != null) {
            String urlWithParams = urlSRO + "?matricula=" + matricula + "&nome=" + nome;
            driver.get(urlWithParams);
        }
    }

    private static void initializeProps() {
        setupProperties = new PropertiesManager("Setup.properties");
        os = System.getProperty("os.name").toLowerCase();
        pathDownload = setupProperties.getProps().getProperty("pathDownload");
        urlSRO = setupProperties.getProps().getProperty("urlSRO");
        urlAdministracao = setupProperties.getProps().getProperty("urlAdministracao");
        browserName = System.getProperty("browser");
        urlSeleniumGrid = setupProperties.getProps().getProperty("url.selenium.grid");
        globalEvidence = setupProperties.getProps().getProperty("globalEvidence");
        highlightElementShow = setupProperties.getProps().getProperty("highlightElementShow");
        urlSinistro = setupProperties.getProps().getProperty("urlSinistro");
        urlPlanoAcao = setupProperties.getProps().getProperty("urlPlanoAcao");
        urlIncidente = setupProperties.getProps().getProperty("urlIncidente");
        urlSindicancia = setupProperties.getProps().getProperty("urlSindicancia");
        urlSegurancaPatrimonial = setupProperties.getProps().getProperty("urlSegurancaPatrimonial");
    }

    private static WebDriver initChromeDriver() {
        initializeProps();
        String path = "./src/test/resources/drivers";
        WebDriverManager.chromedriver().config().setTargetPath(path);
        if (urlSeleniumGrid != null && urlSeleniumGrid.length() != 0) {
            try {
                driver = new RemoteWebDriver(new URL(urlSeleniumGrid),
                        chromeCapabilities());
            } catch (MalformedURLException e) {
                System.out.println(e.getMessage());
            }
        } else {
            WebDriverManager.chromedriver().setup();
            HashMap<String, Object> chromePrefs = new HashMap<>();
            ChromeOptions options = new ChromeOptions();
            options.addArguments("start-maximized", "--ignore-ssl-errors", "–-no-sandbox", "--disable-infobars", "ignore-certicate-errors", "--disable-web-security");
            if (pathDownload != null && pathDownload.length() == 0) {
                String pathForDownload = System
                        .getProperty("user.dir")
                        .concat(File.separator)
                        .concat("src")
                        .concat(File.separator)
                        .concat("test")
                        .concat(File.separator)
                        .concat("resources")
                        .concat(File.separator)
                        .concat("downloads");

                createFolder(pathForDownload);
                chromePrefs.put("download.default_directory", pathForDownload);
            } else {
                createFolder(pathDownload);
                chromePrefs.put("download.default_directory", System
                        .getProperty("user.dir").concat(File.separator).concat(pathDownload));
            }
            chromePrefs.put("profile.default_content_settings.popups", 0);
            chromePrefs.put("download.prompt_for_download", "false");
            options.setExperimentalOption("prefs", chromePrefs);
//            options.addArguments("headless");
            driver = new ChromeDriver(options);
        }

        if (urlSRO != null) {
            driver.get(urlSRO);
        }

        return driver;
    }

    private static Capabilities chromeCapabilities() {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.prompt_for_download", "false");
        chromePrefs.put("plugins.plugins_disabled", new String[]{
                "Adobe Flash Player", "Chrome PDF Viewer"});
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("start-maximized", "--ignore-ssl-errors", "–-no-sandbox", "--disable-infobars", "ignore-certicate-errors", "--disable-web-security");
        desiredCapabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        desiredCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
        desiredCapabilities.setCapability("name", testScenario.get().getName());
        return desiredCapabilities;
    }

}
