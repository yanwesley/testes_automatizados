package gherkin.hook;


import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;

import java.net.URLDecoder;

import static br.com.api.GeradorDeMassa.featureName;
import static br.com.api.GeradorDeMassa.updateValuesMassas;
import static br.com.utils.ReporterUtils.logReport;
import static br.com.utils.WebDriverHelper.testScenario;

public class Hook {


    @Before("@WEB")
    public void init(Scenario scenario) {
        if (featureName == null || !featureName.equals(URLDecoder.decode(scenario.getUri()))) {
            updateValuesMassas();
            featureName = URLDecoder.decode(scenario.getUri());
        }
        testScenario.set(scenario);
    }

    @After("@WEB")
    public void cleanUp() {
        logReport();
    }

}









