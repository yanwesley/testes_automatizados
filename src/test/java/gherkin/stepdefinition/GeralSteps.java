package gherkin.stepdefinition;

import br.com.pom.sro.GeralPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import org.testng.Assert;

import static br.com.utils.ClipboardUtils.getClipboard;


public class GeralSteps extends ReporterUtils {
    GeralPage geralPage = new GeralPage();


    @E("deve copiar a mensagem {string} para a area de transferência")
    public void deveCopiarAMensagemParaAAreaDeTransferência(String urlEsperada) {
        String clipboard = getClipboard();
        Assert.assertEquals(clipboard, urlEsperada);
        addLogToReport("Endereço copiado corretamente: " + clipboard);
    }

    @Entao("deve exibir a snackBar com a mensagem {string}")
    public void deveExibirASnackBarComAMensagem(String msg) {
        geralPage.validaMensagemSnackBar(msg);
    }

    @E("deve exibir o box com a mensagem {string}")
    public void deveExibirOBoxComAMensagem(String mensagemBox) {
        geralPage.validaMensagemBox(mensagemBox);
    }

    @E("clico no expansion da sindicância")
    public void clicoNoExpansionDaSindicância() {
        geralPage.clicarExpansionSindicancia();
        addLogToReport("Cliquei no expansion da sindicância");
    }
}
