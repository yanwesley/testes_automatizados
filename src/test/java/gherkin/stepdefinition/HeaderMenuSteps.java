package gherkin.stepdefinition;

import br.com.pom.sro.HeaderMenuPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

import java.util.List;

public class HeaderMenuSteps extends ReporterUtils {

    HeaderMenuPage headerMenuPage = new HeaderMenuPage();

    @Então("deve exibir o menu com permissões do SRO")
    public void deveExibirOMenuComPermissoesDoSRO() {
        headerMenuPage.validaLayoutMenuUsuarioSRO();
        addScreenshotToReport("Validando menu");
    }

    @Então("deve exibir o menu com permissões do CCO")
    public void deveExibirOMenuComPermissoesDoCCO() {
        headerMenuPage.validaLayoutMenuUsuarioCCO();
        addScreenshotToReport("Validando menu");
    }

    @Então("^deve exibir o menu com permissões do CCO VIA$")
    public void deveExibirOMenuComPermissoesDoCCOVIA() {
        headerMenuPage.validaLayoutMenuUsuarioCCO_VIA();
        addScreenshotToReport("Validando menu");
    }

    @E("acesso a TAB Sinistros")
    public void acessoATABSinistros() {
        headerMenuPage.clicarTABSinistros();
        addLogToReport("Cliquei na TAB Sinistros");
    }

    @Entao("valido se ao clicar no menu a página é atualizada")
    public void validoSeAoClicarNoMenuAPaginaEAtualizada(List<String> rotas) {
        headerMenuPage.validaNavegacaoDoMenu(rotas);
    }

    @E("acesso a TAB Sindicâncias")
    public void acessoATABSindicâncias() {
        headerMenuPage.clicarTABSindicancias();
        addLogToReport("Cliquei na TAB Sindicâncias");
    }

    @E("acesso a TAB Central de Ações")
    public void acessoATABCentralDeAções() {
        headerMenuPage.clicarTABCentralAcoes();
        addLogToReport("Cliquei na TAB Central de Ações");
    }

    @Quando("acesso a TAB Dashboard")
    public void acessoATABDashboard() {
        headerMenuPage.clicarTABDashboard();
        addLogToReport("Cliquei na TAB Dashboard");
    }

    @Então("deve exibir o menu com permissões do CIPIA VIA")
    public void deveExibirOMenuComPermissõesDoCIPIAVIA() {
        headerMenuPage.validaLayoutMenuUsuarioCIPIAVIA();
        addScreenshotToReport("Validando menu");
    }

    @E("acesso a TAB Incidentes")
    public void acessoATABIncidentes() {
        headerMenuPage.clicarTABIncidentes();
        addLogToReport("Cliquei na TAB Incidentes");

    }

    @E("acesso a TAB Informações")
    public void acessoATABInformações() {
        headerMenuPage.clicarTABInformacoes();
        addLogToReport("Cliquei na TAB Informações");
    }

    @E("acesso a TAB Configurações")
    public void acessoATABConfigurações() {
        headerMenuPage.clicarTABConfiguracoes();
        addLogToReport("Cliquei na TAB Configurações");
    }
}
