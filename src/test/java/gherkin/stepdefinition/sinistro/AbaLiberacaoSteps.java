package gherkin.stepdefinition.sinistro;

import br.com.api.model.sinistro.AbaLiberacao;
import br.com.api.model.sinistro.InformarANTT;
import br.com.pom.sro.sinistro.novo_sinistro.liberacao.AbaLiberacaoPage;
import br.com.pom.sro.sinistro.novo_sinistro.liberacao.InformarANTTNovoSinistroPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getAbaLiberacao;


public class AbaLiberacaoSteps extends ReporterUtils {

    AbaLiberacaoPage abaLiberacaoPage = new AbaLiberacaoPage();
    InformarANTTNovoSinistroPage informarANTTNovoSinistroPage = new InformarANTTNovoSinistroPage();
    InformarANTT informarANTT = new InformarANTT();
    AbaLiberacao abaLiberacao = getAbaLiberacao();

    @E("na seção Liberação Final está sendo exibida a mensagem {string}")
    public void naSeçãoLiberaçãoFinalEstáSendoExibidaAMensagem(String msg) {
        abaLiberacaoPage.validaMensagemLiberacaoFinal(msg);
        addScreenshotToReport("Validou a mensagem na seção de Liberação Final");
    }

    @E("clico no botão Salvar e Encerrar")
    public void clicoNoBotãoSalvarEEncerrar() {
        abaLiberacaoPage.clicarBtnSalvarEEncerrar();
        addLogToReport("Clicou no botão Salvar e Encerrar");
    }

    @Entao("deve ser apresentado o modal de confirmação com a mensagem {string}")
    public void deveSerApresentadoOModalDeConfirmaçãoComAMensagem(String msg) {
        abaLiberacaoPage.validaMsgModalConfirmacao(msg);
        addScreenshotToReport("Validação de que modal de confirmação está sendo apresentado");
    }

    @Dado("que clico em cancelar")
    public void queClicoEmCancelar() {
        abaLiberacaoPage.clicarBtnCancelarModalConfirmacao();
        addLogToReport("Clicou no botão Cancelar");
    }

    @Entao("a operação é cancelada e o usuário permanece na tab Liberação")
    public void aOperaçãoÉCanceladaEOUsuárioPermaneceNaTabLiberação() {
        abaLiberacaoPage.validarTabLiberacao();
        addScreenshotToReport("Validação de que a operação é cancelada e o usuário permanece na tab Liberação");
    }

    @E("clico em confirmar")
    public void clicoEmConfirmar() {
        abaLiberacaoPage.clicarBtnConfirmarModalConfirmacao();
        addLogToReport("Clicou no botão confirmar");
    }

    @E("valido que todos os campos estão preenchido como somente leitura")
    public void validoQueTodosOsCamposEstãoPreenchidoComoSomenteLeitura() {
        abaLiberacaoPage.validaPrevisaoDeLiberacaoPreenchida(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Previsão da Liberação");
        abaLiberacaoPage.validaLiberacaoParcialPreenchida(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Parcial");
        abaLiberacaoPage.validaLiberacaoMecanicaPreenchida(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Mecânica");
        abaLiberacaoPage.validaLiberacaoViaPreenchida(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Via");
        abaLiberacaoPage.validaLiberacaoFinalPreenchida(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Final");
    }

    @Dado("insiro uma data de previsão")
    public void insiroUmaDataDePrevisão() {
        abaLiberacaoPage.preencheSecaoPrevisaoDeLiberacao(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Previsão da Liberação");
    }

    @Entao("consigo excluir a data de previsão inserida na linha {int}")
    public void consigoExcluirADataDePrevisãoInseridaNaLinhaInt(int linha) {
        abaLiberacaoPage.excluirUmaPrevisaoDeLiberacao(linha);
        addLogToReport("Excluiu uma previsão de liberação");
    }

    @E("a data de previsão não deve mais aparecer na tela")
    public void aDataDePrevisãoNãoDeveMaisAparecerNaTela() {
        abaLiberacaoPage.validaQueAPrevisaoFoiExcluida();
        addScreenshotToReport("Validação que a previsão de liberação foi excluída");
    }

    @Dado("que a previsão final não está preenchida")
    public void queAPrevisãoFinalNãoEstáPreenchida() {
        abaLiberacaoPage.validaQueADataLiberacaoFinalNaoEstaPreenchida();
        addScreenshotToReport("Validação que a previsão final não está preenchida");
    }

    @Entao("o botão Salvar informações está sendo apresentado")
    public void oBotãoSalvarInformaçõesEstáSendoApresentado() {
        abaLiberacaoPage.validaBotaoSalvarInformacoes();
        addScreenshotToReport("Validação de que o botão Salvar informações está sendo apresentado");
    }

    @E("o botão Salvar e Encerrar não está sendo apresentado")
    public void eOBotãoSalvarEEncerrarNãoEstáSendoApresentado() {
        abaLiberacaoPage.validaBotaoSalvarEEncerrarNaoApresentado();
        addScreenshotToReport("Validação de que o botão Salvar e Encerrar não está sendo apresentado");
    }

    @Dado("que preencho a Aba liberação parcialmente")
    public void quePreenchoAAbaLiberaçãoParcialmente() {
        abaLiberacaoPage.preencheSecaoPrevisaoDeLiberacao(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Previsão da Liberação");
        abaLiberacaoPage.preencheSecaoDeLiberacaoParcial(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Parcial");
        abaLiberacaoPage.preencheSecaoDeLiberacaoMecanica(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Mecânica");
        abaLiberacaoPage.preencheSecaoDeLiberacaoVia(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Via");
    }

    @E("clico no botão Salvar Informações")
    public void clicoNoBotãoSalvarInformações() {
        abaLiberacaoPage.clicarBtnSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar Informações");
    }

    @Entao("valido que os dados parcialmente preenchidos estão na tela")
    public void validoQueOsDadosParcialmentePreenchidosEstãoNaTela() {
        abaLiberacaoPage.validaPrevisaoDeLiberacaoPreenchida(abaLiberacao);
        addScreenshotToReport("Valida o campo Previsão de liberação preenchido");
        abaLiberacaoPage.validaLiberacaoParcialPreenchida(abaLiberacao);
        addScreenshotToReport("Valida o campo Liberação Parcial preenchido");
        abaLiberacaoPage.validaLiberacaoMecanicaPreenchida(abaLiberacao);
        addScreenshotToReport("Valida o campo Liberação Mecânica preenchido");
        abaLiberacaoPage.validaLiberacaoViaPreenchida(abaLiberacao);
        addScreenshotToReport("Valida o campo Liberação Via preenchido");
    }

    @Dado("que preencho as a Liberação final")
    public void quePreenchoAsALiberaçãoFinal() {
        abaLiberacaoPage.preencheSecaoDeLiberacaoFinal(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Previsão da Liberação");
    }

    @E("preencho todos os campos da tab Liberação")
    public void preenchoTodosOsCamposDaTabLiberação() {
        abaLiberacaoPage.preencheSecaoPrevisaoDeLiberacao(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Previsão da Liberação");
        abaLiberacaoPage.preencheSecaoDeLiberacaoParcial(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Parcial");
        abaLiberacaoPage.preencheSecaoDeLiberacaoMecanica(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Mecânica");
        abaLiberacaoPage.preencheSecaoDeLiberacaoVia(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Via");
        abaLiberacaoPage.preencheSecaoDeLiberacaoFinal(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Final");
    }

    @Dado("que clico no botão Voltar")
    public void queClicoNoBotãoVoltar() {
        abaLiberacaoPage.clicarEmVoltar();
        addLogToReport("Clicou no botão Voltar");
    }

    @E("clico no botão de Enviar email para a ANTT")
    public void clicoNoBotãoDeEnviarEmailParaAANTT() {
        abaLiberacaoPage.clicarBtnEnviarEmailANTT();
        addLogToReport("Clicou no botão para enviar Email para a ANTT");
    }

    @Entao("deve ser exibido a modal de Informações para e-mail da ANTT")
    public void deveSerExibidoAModalDeInformaçõesParaEMailDaANTT() {
        informarANTTNovoSinistroPage.validarModalParaEnvioDeInformacaoANTT();
        addLogToReport("Validou que a modal para o envio de informações da ANTT apareceu");
    }

    @E("todos os campos da modal para envio da informação da ANTT devem ser apresentados")
    public void todosOsCamposDaModalParaEnvioDaInformaçãoDaANTTDevemSerApresentados() {
        informarANTTNovoSinistroPage.validarCamposDaModalEnviarEmailANTT();
        addScreenshotToReport("Validação que os campos esperados do modal estão sendo apresentados");
    }

    @E("o campo Motivo deve estar desabilitado")
    public void oCampoMotivoDeveEstarDesabilitado() {
        informarANTTNovoSinistroPage.validarMotivoDesabilitado();
        addScreenshotToReport("Validação do campo Motivo desabilitado");
    }

    @E("seleciono o campo Natureza")
    public void preenchoOCampoNatureza() {
        informarANTTNovoSinistroPage.selecionaCampoNatureza();
        addScreenshotToReport("Selecionou o campo Natureza");
    }

    @Entao("o campo Motivo deve estar habilitado")
    public void oCampoMotivoDeveEstarHabilitado() {
        informarANTTNovoSinistroPage.validarMotivoHabilitado();
        addScreenshotToReport("Validação do campo Motivo habilitado");
    }

    @E("preencho todos os campos da modal de envio de email para a ANTT")
    public void preenchoTodosOsCamposDaModalDeEnvioDeEmailParaAANTT() {
        informarANTTNovoSinistroPage.preencheFerrovia(informarANTT);
        addScreenshotToReport("Preencheu Ferrovia");
        informarANTTNovoSinistroPage.preencheAcidentePN(informarANTT);
        addScreenshotToReport("Preencheu Acidente em PN");
        informarANTTNovoSinistroPage.preenchePerimetroUrbano(informarANTT);
        addScreenshotToReport("Preencheu Perímetro Urbano");
        informarANTTNovoSinistroPage.preencheTrecho(informarANTT);
        addScreenshotToReport("Preencheu Trecho");
        informarANTTNovoSinistroPage.preencheNatureza(informarANTT);
        addScreenshotToReport("Preencheu Natureza");
        informarANTTNovoSinistroPage.preencheMotivo(informarANTT);
        addScreenshotToReport("Preencheu Motivo");
        informarANTTNovoSinistroPage.preencheCausaProvavel(informarANTT);
        addScreenshotToReport("Preencheu Causa Provável");
        informarANTTNovoSinistroPage.validaRelato(informarANTT);
        addScreenshotToReport("Valida Relato preenchido");
    }

    @Quando("alterar a Natureza")
    public void alterarANatureza() {
        informarANTTNovoSinistroPage.alteraNatureza(informarANTT);
        addScreenshotToReport("Alterou Natureza");
    }

    @Quando("clico em Cancelar o envio")
    public void clicoEmCancelarOEnvio() {
        informarANTTNovoSinistroPage.clicaCancelarEnvio();
    }

    @Quando("clico em Confirmar o envio")
    public void clicoEmConfirmarOEnvio() {
        informarANTTNovoSinistroPage.clicaConfirmarEnvio();
        addLogToReport("Clicou em confirmar Envio.");
    }


    @E("deve exibir a modal com o E-mail enviado para a ANTT")
    public void deveExibirAModalComOEMailEnviadoParaAANTT() {
        informarANTTNovoSinistroPage.validaEmailEnviadoANTT(informarANTT);
        addScreenshotToReport("Modal com o email enviado para a ANTT");
    }

    @Quando("clico no botão Fechar da modal de email enviado")
    public void clicoNoBotãoFecharDaModalDeEmailEnviado() {
        informarANTTNovoSinistroPage.clicarBtnFecharModalEmailEnviado();
        addLogToReport("Clicou no botão fechar da modal.");
    }

    @Entao("deve ser exibida mensagem de envio para a ANTT")
    public void deveSerExibidaMensagemDeEnvioParaAANTT() {
        informarANTTNovoSinistroPage.validaMsgDeEmailANTTEnviado();
        addScreenshotToReport("Mensagem informativa do email enviado para a ANTT");
    }

    @Quando("clico no botão Visualizar email enviado")
    public void clicoNoBotãoVisualizarEmailEnviado() {
        informarANTTNovoSinistroPage.clicarBtnVisualizarEmail();
        addLogToReport("Clicou no botão Visualizar email");
    }

    @Entao("deve exibir o aviso para que o email para a ANTT seja enviado")
    public void deveExibirOAvisoParaQueOEmailParaAANTTSejaEnviado() {
        informarANTTNovoSinistroPage.validarMsgDeAvisoParaOEnvioDeEmailANTT();
        addScreenshotToReport("Modal com o email enviado para a ANTT");
    }

    @Entao("valida que o campo Relato foi alterado")
    public void validaQueOCampoRelatoFoiAlterado() {
        informarANTTNovoSinistroPage.validaRelato(informarANTT);
        addScreenshotToReport("Campo relato preenchido");
    }

    @E("o campo Motivo não deve estar preenchido")
    public void oCampoMotivoNãoDeveEstarPreenchido() {
        informarANTTNovoSinistroPage.validaQueMotivoNaoEstaPreenchido();
        addScreenshotToReport("Valida o campo Motivo não preenchido");
    }

    @Entao("não é possível preencher a seção de Liberação final")
    public void nãoÉPossívelPreencherASeçãoDeLiberaçãoFinal() {
        abaLiberacaoPage.naoEPossivelPreencherLiberacaoFinal();
        addScreenshotToReport("Liberação final não preenchida");
    }

    @Dado("que clico no botão Cancelar da Aba Liberação")
    public void queClicoNoBotãoCancelarDaAbaLiberação() {
        abaLiberacaoPage.clicarBtnCancelar();
        addLogToReport("Clicou no botão cancelar");
    }
}
