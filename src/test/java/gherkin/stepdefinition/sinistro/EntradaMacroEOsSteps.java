package gherkin.stepdefinition.sinistro;

import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.sinistro.novo_sinistro.tipo_entrada.EntradaMacroEOsPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.List;

import static br.com.api.GeradorDeMassa.getSinistro;

public class EntradaMacroEOsSteps extends ReporterUtils {

    EntradaMacroEOsPage entradaMacroEOsPage = new EntradaMacroEOsPage();
    Sinistro sinistro = getSinistro();

    @Dado("que a lista de macro é apresentada")
    public void queAListaDeMacroÉApresentada() {
        entradaMacroEOsPage.validaQueAListaDeMacroEApresentada();
        addScreenshotToReport("Apresentação da Lista de Macros");
    }

    @E("que clico no botão Alterar a entrada de dados")
    public void queClicoNoBotãoAlterarAEntradaDeDados() {
        entradaMacroEOsPage.clicarNoBtnAlterarEntradaDeDados();
        addLogToReport(" Clicou no botão Alterar a entrada de dados");
    }

    @Dado("que clico no botão Cancelar")
    public void queClicoNoBotãoCancelar() {
        entradaMacroEOsPage.clicarNoBtnCancelar();
        addLogToReport("Clicou no botão Cancelar");
    }

    @Entao("o botão Selecionar deve estar desabilitado")
    public void oBotãoSelecionarDeveEstarDesabilitado() {
        entradaMacroEOsPage.validarBtnSelecionarDesabilitado();
        addScreenshotToReport("Valida que o botão Selecionar deve estar desabilitado");
    }

    @Quando("selecionar a linha {int} de macro e OS")
    public void selecionarUmaMacroDaLinha(int linha) {
        entradaMacroEOsPage.selecionarMacro(linha);
        addScreenshotToReport("Selecionou a linha " + linha + "de macro e OS");
    }

    @Quando("selecionar a linha {int} da OS e setar os dados da linha selecionada")
    public void selecionarALinhaDaOSESetarOsDadosDaLinhaSelecionada(int linha) {
        entradaMacroEOsPage.selecionarOSSetandoOsDados(linha, sinistro);
        addScreenshotToReport("Selecionou a linha " + linha + "de macro e OS");
    }

    @Entao("o botão Selecionar deve estar habilitado")
    public void oBotãoSelecionarDeveEstarHabilitado() {
        entradaMacroEOsPage.validarBtnSelecionarHabilitado();
        addScreenshotToReport("Validação de que o botão Selecionar deve estar habilitado");
    }

    @E("clicar no botão Selecionar")
    public void clicarNoBotãoSelecionar() {
        entradaMacroEOsPage.clicarNoBtnSelecionar();
        addLogToReport("Clicou no botão Selecionar");
    }

    @Dado("que estou na tela de listagem de Macros")
    public void queEstouNaTelaDeListagemDeMacros() {
        entradaMacroEOsPage.validaQueEstaNaTelaDeMacros();
        addScreenshotToReport("Validação de que estou na tela de listagem de Macros");
    }

    @Dado("que estou na tela de listagem de OS ou prefixo")
    public void queEstouNaTelaDeListagemDeOSOuPrefixo() {
        entradaMacroEOsPage.validaQueEstaNaTelaDeOs();
        addScreenshotToReport("Validação de que estou na tela de listagem de OS ou prefixo");
    }

    @Entao("validar que a OS da linha {int} não está selecionada")
    public void validarQueAOSDaLinhaNãoEstáSeleciona(int linha) {
        entradaMacroEOsPage.validaQueALinhaNaoEstaSelecionada(linha);
        addScreenshotToReport("Validação de que a OS da linha" + linha + " não está selecionada");
    }

    @Entao("validar que as duas linhas selecionadas são do mesmo prefixo e estão selecionadas")
    public void validarQueAsDuasLinhasSelecionadasSãoDoMesmoPrefixoEEstãoSelecionadas() {
        entradaMacroEOsPage.validarSeAsLinhasSelecionadasSaoDoMesmoPrefixo();
        addScreenshotToReport("Valida que as linhas selecionadas são do mesmo prefixo e estão selecionadas");
    }

    @Quando("eu preencher os campos de data e hora da busca de OS")
    public void euPreencherOsCamposDeDataEHoraDaBuscaDeOS(List<String> list) {
        sinistro.setDateOS(list.get(0));
        sinistro.setHoraOS(list.get(1));
        entradaMacroEOsPage.preencherCamposDataEHoraOS(sinistro);
        addScreenshotToReport("Preenchi os campos de data e hora da busca de OS.");
    }
}