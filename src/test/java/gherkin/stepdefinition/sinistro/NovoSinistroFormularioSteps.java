package gherkin.stepdefinition.sinistro;

import br.com.api.model.sinistro.AbaLiberacao;
import br.com.api.model.sinistro.Sinistro;
import br.com.api.model.sinistro.Veiculo;
import br.com.pom.sro.HeaderMenuPage;
import br.com.pom.sro.sindicancias.SindicanciaListagemPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.DadosViaPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.HeaderInformacoesSindicanciaPage;
import br.com.pom.sro.sinistro.novo_sinistro.FormularioNovoSinistroPage;
import br.com.pom.sro.sinistro.novo_sinistro.liberacao.AbaLiberacaoPage;
import br.com.pom.sro.sinistro.novo_sinistro.sinistro.*;
import br.com.pom.sro.sinistro.novo_sinistro.veiculos.VeiculosNovoSinistroPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

import static br.com.api.GeradorDeMassa.*;

public class NovoSinistroFormularioSteps extends ReporterUtils {

    FormularioNovoSinistroPage formularioNovoSinistroPage = new FormularioNovoSinistroPage();
    SobreOSinistroNovoSinistroPage sobreOSinistroNovoSinistroPage = new SobreOSinistroNovoSinistroPage();
    SobreOLocalNovoSinistroPage sobreOLocalNovoSinistroPage = new SobreOLocalNovoSinistroPage();
    SobreOTremNovoSinistroPage sobreOTremNovoSinistroPage = new SobreOTremNovoSinistroPage();
    SobreOCondutorOuMaquinistaNovoSinistroPage sobreOCondutorOuMaquinistaNovoSinistroPage = new SobreOCondutorOuMaquinistaNovoSinistroPage();
    AdicionarVitimasNovoSinistroPage adicionarVitimasNovoSinistroPage = new AdicionarVitimasNovoSinistroPage();
    AdicionarVeiculosTerceiroNovoSinistroPage adicionarVeiculosTerceiroNovoSinistroPage = new AdicionarVeiculosTerceiroNovoSinistroPage();
    OutrasInformacoesNovoSinistroPage outrasInformacoesNovoSinistroPage = new OutrasInformacoesNovoSinistroPage();
    VeiculosNovoSinistroPage veiculosNovoSinistroPage = new VeiculosNovoSinistroPage();
    AbaLiberacaoPage abaLiberacaoPage = new AbaLiberacaoPage();
    Sinistro sinistro = getSinistro();
    Veiculo veiculo = getVeiculo();
    AbaLiberacao abaLiberacao = getAbaLiberacao();
    AbaLiberacao abaLiberacaoEditado = getNewAbaLiberacao();
    Sinistro sinistroEditado = getNewSinistro();
    HeaderMenuPage headerMenuPage = new HeaderMenuPage();
    SindicanciaListagemPage sindicanciaListagemPage = new SindicanciaListagemPage();
    DadosViaPage dadosViaPage = new DadosViaPage();
    HeaderInformacoesSindicanciaPage headerInformacoesSindicanciaPage = new HeaderInformacoesSindicanciaPage();
    SinistroSROPage sinistroSROPage = new SinistroSROPage();

    @Entao("deve exibir a página de inserção de dados do sinistro manual")
    public void deveExibirAPaginaDeInsercaoDeDadosDoSinistroManual() {
        formularioNovoSinistroPage.validaExibicaoPaginaDePreenchimento();
        addScreenshotToReport("Validando exibição da tela de preenchimento do Novo sinistro.");
    }

    @Entao("deve exibir a página de inserção de dados do sinistro via macro")
    public void deveExibirAPaginaDeInsercaoDeDadosDoSinistroViaMacro() {
        formularioNovoSinistroPage.validaExibicaoPaginaDePreenchimento();
        addScreenshotToReport("Validando exibição da tela de preenchimento do Novo sinistro.");
    }

    @Entao("deve exibir a página de inserção de dados do sinistro via OS")
    public void deveExibirAPaginaDeInsercaoDeDadosDoSinistroViaOS() {
        formularioNovoSinistroPage.validaExibicaoPaginaDePreenchimento();
        addScreenshotToReport("Validando exibição da tela de preenchimento do Novo sinistro.");
        sobreOSinistroNovoSinistroPage.validaCamposSobreOSinistroPreenchidosViaOS(sinistro);
        addScreenshotToReport("Validando os campos da seção de Sobre o Sinistro preenchido");
        sobreOTremNovoSinistroPage.validaCamposSobreOTremPreenchidosViaOS(sinistro);
        addScreenshotToReport("Validando os campos da seção de Sobre o Trem preenchido");
    }

    @Entao("deve exibir o formulario de novo sinistro com os campos vazios")
    public void deveExibirOFormularioDeNovoSinistroComOsCamposVazios() {
        sobreOSinistroNovoSinistroPage.validaCamposVazios();
        addScreenshotToReport("Validei os campos vazios da seção sobre o sinistro");
        sobreOLocalNovoSinistroPage.validaCamposVazios();
        addScreenshotToReport("Validei os campos vazios da seção sobre o trem");
        sobreOTremNovoSinistroPage.validaCamposVazios();
        addScreenshotToReport("Validei os campos vazios da seção sobre o trem");
        sobreOCondutorOuMaquinistaNovoSinistroPage.validaCamposVazios();
        addScreenshotToReport("Validei os campos vazios da seção sobre O CONDUTOR OU MAQUINISTA");
        adicionarVitimasNovoSinistroPage.validaCamposVazios();
        addScreenshotToReport("Validei os campos vazios da seção ADICIONAR VÍTIMAS");
        adicionarVeiculosTerceiroNovoSinistroPage.validaCamposVazios();
        addScreenshotToReport("Validei os campos vazios da seção ADICIONAR VEÍCULOS DE TERCEIROS");
        outrasInformacoesNovoSinistroPage.validaCamposVazios();
        addScreenshotToReport("Validei os campos vazios da seção OUTRAS INFORMAÇÕES");
    }

    @Entao("valido que o botão Enviar e-mail está desabilitado")
    public void validoQueOBotaoEnviarEMailEstaDesabilitado() {
        formularioNovoSinistroPage.validaQueOBotaoEnviarEMailEstaDesabilitado();
        addScreenshotToReport("Validando que o botão Enviar EMail está desabilitado.");
    }

    @Entao("valido que o botão Enviar e-mail está habilitado")
    public void validoQueOBotaoEnviarEMailEstaHabilitado() {
    }

    @Entao("clico em Enviar e-mail")
    public void clicoEmEnviarEMail() {
        formularioNovoSinistroPage.clicarNoBotaoEnviarEmail();
        addLogToReport("Cliquei no botão 'Envia email'.");
    }

    @E("confirmo o envio do e-mail")
    public void confirmoOEnvioDoEMail() {
        formularioNovoSinistroPage.clicarNoBotaoConfirmarDoModalDeEmail();
        addScreenshotToReport("Cliquei no botão confirmar.");
    }

    @E("clico em Salvar Sinistro")
    public void clicoEmSalvarSinistro() {
        formularioNovoSinistroPage.clicarNoBotaoSalvarInformacoes();
        addLogToReport("Cliquei no botão 'Salvar Informações'");
    }

    @E("clico em Salvar E Avançar")
    public void clicoEmSalvarEAvancar() {
        formularioNovoSinistroPage.clicarNoBotaoSalvarEAvancar();
        addLogToReport("Cliquei no botão 'Salvar Informações'");
    }

    @Entao("valido que o botão Ficha do Trem não é exibido")
    public void validoQueOBotaoFichaDoTremNaoEExibido() {
        formularioNovoSinistroPage.validoQueOBotaoFichaDoTremNaoEExibido();
        addScreenshotToReport("Validando que o o botão 'Ficha do Trem' não é exibido");
    }

    @Entao("valido que o botão Endereço do Sinistro não é exibido")
    public void validoQueOBotaoEnderecoDoSinistroNaoEExibido() {
        formularioNovoSinistroPage.validarBtnEnderecoDoSinistroNaoEExibido();
        addScreenshotToReport("Validando que o o botão 'Endereço do Sinistro' não é exibido");
    }

    @Entao("preencho todos os campos do sinistro manualmente")
    public void preenchoTodosOsCamposDoSinistroManualmente() {
        sobreOSinistroNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o sinistro");
        sobreOLocalNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o trem");
        sobreOTremNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o trem");
        sobreOCondutorOuMaquinistaNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre O CONDUTOR OU MAQUINISTA");
        adicionarVitimasNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados ADICIONAR VÍTIMAS");
        adicionarVeiculosTerceiroNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados ADICIONAR VEÍCULOS DE TERCEIROS");
        outrasInformacoesNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados OUTRAS INFORMAÇÕES");
    }

    @E("ao clicar no botão Endereço do sinistro")
    public void aoClicarNoBotãoEndereçoDoSinistro() {
        formularioNovoSinistroPage.clicarNoBotaoEnderecoDoSinistro();
        addLogToReport("Cliquei no botão 'Endereço do sinistro'.");
    }

    @Entao("deve exibir o formulario de novo sinistro com os campos carregados")
    public void deveExibirOFormularioDeNovoSinistroComOsCamposCarregados() {
        formularioNovoSinistroPage.validaExibicaoPaginaDePreenchimentoComCamposCarregados();
        addScreenshotToReport("Exibindo página do sinistro.");
    }

    @E("clico no botão Alterar")
    public void clicoNoBotãoAlterar() {
        formularioNovoSinistroPage.clicarNoBotaoAlterar();
        addLogToReport("Cliquei no botão alterar");
    }

    @Entao("o botão Alterar não deve mais estar visivel")
    public void oBotãoAlterarNãoDeveMaisEstarVisivel() {
        formularioNovoSinistroPage.validarQueOBotaoAlterarNaoEstaVisivel();
        addScreenshotToReport("Validando que o botão 'Alterar' não está visivel após salvar o sinistro.");
    }

    @Entao("clico na tab Liberação")
    public void clicoNaTabLiberação() {
        formularioNovoSinistroPage.clicaNaTabLiberacao();
    }

    @E("valido que os dados do sinistro foram carregados no formulário")
    public void validoQueOsDadosDoSinistroForamCarregadosNoFormulário() {
        sobreOSinistroNovoSinistroPage.validaCamposPreenchidos(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o sinistro");
        sobreOLocalNovoSinistroPage.validaCamposPreenchidos(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o trem");
        sobreOTremNovoSinistroPage.validaCamposPreenchidos(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o trem");
        sobreOCondutorOuMaquinistaNovoSinistroPage.validaCamposPreenchidos(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre O CONDUTOR OU MAQUINISTA");
        adicionarVitimasNovoSinistroPage.validaCamposPreenchidos(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção ADICIONAR VÍTIMAS");
        adicionarVeiculosTerceiroNovoSinistroPage.validaCamposPreenchidos(1, sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção ADICIONAR VEÍCULOS DE TERCEIROS");
        outrasInformacoesNovoSinistroPage.validaCamposPreenchidos(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção OUTRAS INFORMAÇÕES");
    }

    @Entao("clico na tab Sinistro na página de novo sinistro")
    public void clicoNaTabSinistroNaPáginaDeNovoSinistro() {
        formularioNovoSinistroPage.clicaNaTabSinistro();
        addLogToReport("Clicou na tab Sinistros na página de novo sinistro");
    }

    @Entao("clico na tab Veículos na página de novo sinistro")
    public void clicoNaTabVeiculosNaPaginaDeNovoSinistro() {
        formularioNovoSinistroPage.clicarNaTabVeiculos();
        addLogToReport("Clicou na tab Veículos na página de novo sinistro");
    }

    @Entao("valido que os dados do sinistro foram carregados no formulário como readonly")
    public void validoQueOsDadosDoSinistroForamCarregadosNoFormulárioComoReadonly() {
        sobreOSinistroNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o sinistro");
        sobreOLocalNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o trem");
        sobreOTremNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o trem");
        sobreOCondutorOuMaquinistaNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre O CONDUTOR OU MAQUINISTA");
        veiculosNovoSinistroPage.validaVeiculoAdicionadoReadOnly(0, veiculo);
        addScreenshotToReport("Validei composição do veículo.");
        veiculosNovoSinistroPage.validarQuantidadeDeRodeirosSelecionadosReadOnly(1, veiculo);
        addScreenshotToReport("Validei que apenas os rodeiros selecionados estão sendo contabilizado.");
    }

    @Entao("valido que os dados do sinistro foram carregados no formulário como readonly na visao do SRO")
    public void validoQueOsDadosDoSinistroForamCarregadosNoFormulárioComoReadonlyNaVisaoDoSRO() {
        sobreOSinistroNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o sinistro");
        sobreOLocalNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o trem");
        sobreOTremNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o trem");
        sobreOCondutorOuMaquinistaNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre O CONDUTOR OU MAQUINISTA");

        adicionarVitimasNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção ADICIONAR VÍTIMAS");

        adicionarVeiculosTerceiroNovoSinistroPage.validaCamposPreenchidosReadOnly(1, sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção VEÍCULOS DE TERCEIROS");

        outrasInformacoesNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistro);
        addScreenshotToReport("Validei os campos preenchidos da seção OUTRAS INFORMAÇÕES");

        //COMPOSIÇÃO DO TREM
        veiculosNovoSinistroPage.validaVeiculoAdicionadoReadOnly(0, veiculo);
        addScreenshotToReport("Validei composição do veículo.");
        veiculosNovoSinistroPage.validarQuantidadeDeRodeirosSelecionadosReadOnly(1, veiculo);
        addScreenshotToReport("Validei que apenas os rodeiros selecionados estão sendo contabilizado.");


        //PREVISÃO
        abaLiberacaoPage.validaPrevisaoDeLiberacaoPreenchidaReadOnly(abaLiberacao);
        addScreenshotToReport("Validei a seção de Previsão da Liberação");
        abaLiberacaoPage.validaLiberacaoParcialPreenchidaReadOnly(abaLiberacao);
        addScreenshotToReport("Validei a seção de Liberação Parcial");
        abaLiberacaoPage.validaLiberacaoMecanicaPreenchidaReadOnly(abaLiberacao);
        addScreenshotToReport("Validei a seção de Liberação Mecânica");
        abaLiberacaoPage.validaLiberacaoViaPreenchidaReadOnly(abaLiberacao);
        addScreenshotToReport("Validei a seção de Liberação Via");

    }

    @E("valido que os dados do sinistro foram carregados no formulário como readonly na visao do SRO com os dados editado")
    public void validoQueOsDadosDoSinistroForamCarregadosNoFormulárioComoReadonlyNaVisaoDoSROComDadosEditado() {
        sobreOSinistroNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistroEditado);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o sinistro");
        sobreOLocalNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistroEditado);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o trem");
        sobreOTremNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistroEditado);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre o trem");
        sobreOCondutorOuMaquinistaNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistroEditado);
        addScreenshotToReport("Validei os campos preenchidos da seção sobre O CONDUTOR OU MAQUINISTA");

        adicionarVitimasNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistroEditado);
        addScreenshotToReport("Validei os campos preenchidos da seção ADICIONAR VÍTIMAS");

        adicionarVeiculosTerceiroNovoSinistroPage.validaCamposPreenchidosReadOnly(1, sinistro);
        adicionarVeiculosTerceiroNovoSinistroPage.validaCamposPreenchidosReadOnly(2, sinistroEditado);
        addScreenshotToReport("Validei os campos preenchidos da seção VEÍCULOS DE TERCEIROS");

//        outrasInformacoesNovoSinistroPage.validaCamposPreenchidosReadOnly(sinistroEditado);
//        addScreenshotToReport("Validei os campos preenchidos da seção OUTRAS INFORMAÇÕES");

        //PREVISÃO
        abaLiberacaoPage.validaLiberacaoParcialPreenchidaReadOnly(abaLiberacaoEditado);
        addScreenshotToReport("Validei a seção de Liberação Parcial");
        abaLiberacaoPage.validaLiberacaoMecanicaPreenchidaReadOnly(abaLiberacaoEditado);
        addScreenshotToReport("Validei a seção de Liberação Mecânica");
        abaLiberacaoPage.validaLiberacaoViaPreenchidaReadOnly(abaLiberacaoEditado);
        addScreenshotToReport("Validei a seção de Liberação Via");
    }

    @Entao("edito os dados do sinistro na visão do SRO")
    public void editoOsDadosDoSinistroNaVisãoDoSRO() {
        sobreOSinistroNovoSinistroPage.clicarBtnEditar();
        sobreOSinistroNovoSinistroPage.preencherCampos(sinistroEditado);
        addScreenshotToReport("Editei a seção de Sobre o Sinistro");

        sobreOLocalNovoSinistroPage.clicarBtnEditar();
        sobreOLocalNovoSinistroPage.preencherCampos(sinistroEditado);
        addScreenshotToReport("Editei a seção de Sobre o Local");

        sobreOTremNovoSinistroPage.clicarBtnEditar();
        sobreOTremNovoSinistroPage.preencherCampos(sinistroEditado);
        addScreenshotToReport("Editei a seção de Sobre o Trem");

        sobreOCondutorOuMaquinistaNovoSinistroPage.clicarBtnEditar();
        sobreOCondutorOuMaquinistaNovoSinistroPage.preencherCampos(sinistroEditado);
        addScreenshotToReport("Editei a seção de Sobre o Condutor ou Maquinista");

        adicionarVitimasNovoSinistroPage.clicarBtnEditar();
        adicionarVitimasNovoSinistroPage.preencherCampos(sinistroEditado);
        addScreenshotToReport("Editei a seção de Sobre Adicionar Vítimas");

        adicionarVeiculosTerceiroNovoSinistroPage.clicarBtnEditar();
        adicionarVeiculosTerceiroNovoSinistroPage.preencherCampos(sinistroEditado);
        addScreenshotToReport("Editei a seção de Sobre Adicionar Veículos terceiros.");

        outrasInformacoesNovoSinistroPage.clicarBtnEditar();
        outrasInformacoesNovoSinistroPage.preencherCampos(sinistroEditado);
        addScreenshotToReport("Editei a seção de Sobre Outras Informações");

        abaLiberacaoPage.clicarBtnEditarLiberacaoPrevisao();
        abaLiberacaoPage.preencheSecaoPrevisaoDeLiberacao(abaLiberacaoEditado);
        addScreenshotToReport("Editei a seção de Sobre Previsão Liberação");

        abaLiberacaoPage.clicarBtnEditarLiberacaoParcial();
        abaLiberacaoPage.preencheSecaoDeLiberacaoParcial(abaLiberacaoEditado);
        addScreenshotToReport("Editei a seção de Sobre Liberação Parcial");

        abaLiberacaoPage.clicarBtnEditarLiberacaoMecanica();
        abaLiberacaoPage.preencheSecaoDeLiberacaoMecanica(abaLiberacaoEditado);
        addScreenshotToReport("Editei a seção de Sobre Liberação Mecânica");

        abaLiberacaoPage.clicarBtnEditarLiberacaoVia();
        abaLiberacaoPage.preencheSecaoDeLiberacaoVia(abaLiberacaoEditado);
        addScreenshotToReport("Editei a seção de Sobre Liberação via");

    }

    @E("altero a data para {string} OS para {int}")
    public void alteroADataParaOSPara(String data, int os) {
        sobreOSinistroNovoSinistroPage.editarData(data);
        addScreenshotToReport("Editei a data para: " + data);
        sobreOTremNovoSinistroPage.editarOS(os);
        addScreenshotToReport("Editei a OS para: " + os);
    }

    @E("navego até a ficha do trem")
    public void navegoAtéAFichaDoTrem() {
        headerMenuPage.clicarTABSindicancias();
        addLogToReport("Cliquei na TAB Sindicâncias");
        sindicanciaListagemPage.validaExibicaoDaPagina();
        addScreenshotToReport("valida exibição da listagem");
        sindicanciaListagemPage.ordenarIdDecrescente();
        sindicanciaListagemPage.clicarBtnEditarListagem(1);
        addLogToReport("Cliquei no botão Editar da sindicancia");
        dadosViaPage.validaExibisPaginaInclusaoDeInformacoes();
        addScreenshotToReport("Valida que a página para inclusão de informações carregou com sucesso.");
        headerInformacoesSindicanciaPage.clicarBtnFichaAcidente();
        addLogToReport("Cliquei no botão 'Ficha do Acidente'");
        headerInformacoesSindicanciaPage.switchToWindow();
        sinistroSROPage.clicarExpandirSinistro();
        addLogToReport("Clicou na tab Dados do CCO Circulação.");
        sinistroSROPage.clicaTabDadosCCOCirculacao();
        addLogToReport("Clicou na tab Dados do CCO Circulação.");
    }

    @Entao("valido que os dados do veículo como readonly na ficha do trem")
    public void validoQueOsDadosDoVeículoComoReadonlyNaFichaDoTrem() {
        //COMPOSIÇÃO DO TREM
        veiculosNovoSinistroPage.validaVeiculoAdicionadoReadOnly(0, veiculo);
        addScreenshotToReport("Validei composição do veículo.");
        veiculosNovoSinistroPage.validarQuantidadeDeRodeirosSelecionadosReadOnly(1, veiculo);
        addScreenshotToReport("Validei que apenas os rodeiros selecionados estão sendo contabilizado.");
    }

    @E("valido veículo no formulário como readonly")
    public void validoVeículoNoFormulárioComoReadonly() {
        veiculosNovoSinistroPage.validaVeiculoAdicionadoReadOnly(0, veiculo);
        addScreenshotToReport("Validei composição do veículo.");
        veiculosNovoSinistroPage.validarQuantidadeDeRodeirosSelecionadosReadOnly(1, veiculo);
        addScreenshotToReport("Validei que apenas os rodeiros selecionados estão sendo contabilizado.");
    }
}
