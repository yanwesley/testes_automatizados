package gherkin.stepdefinition.sinistro;

import br.com.pom.sro.sinistro.novo_sinistro.tipo_entrada.EscolherTipoDeEntradaNovoSinistroPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class NovoSinistroTipoDeEntradaSteps extends ReporterUtils {

    EscolherTipoDeEntradaNovoSinistroPage escolherTipoDeEntradaNovoSinistroPage = new EscolherTipoDeEntradaNovoSinistroPage();

    @Entao("deve exibir a página para selecionar o tipo de entrada do novo sinistro")
    public void deveExibirAPaginaParaSelecionarOTipoDeEntradaDoNovoSinistro() {
        escolherTipoDeEntradaNovoSinistroPage.validaExibicaoDaPagina();
        addScreenshotToReport("Validou exibição da Página de novo sinistro.");
    }

    @Quando("clico no botão Dados Manuais")
    public void clicoNoBotaoDadosManuais() {
        escolherTipoDeEntradaNovoSinistroPage.clicarBtnDadosManuais();
        addLogToReport("Cliquei no botão:  'Dados Manuais'");
    }

    @E("clico no botão Número da Macro")
    public void clicoNoBotãoNúmedoDaMacro() {
        escolherTipoDeEntradaNovoSinistroPage.clicarBtnNumeroDaMacro();
        addLogToReport("Cliquei no botão:  'Número da Macro'");
    }

    @E("clico no botão Número da OS ou prefixo")
    public void clicoNoBotãoNúmeroDaOSOuPrefixo() {
        escolherTipoDeEntradaNovoSinistroPage.clicarBtnNumedoDaOS();
        addLogToReport("Cliquei no botão:  'Número da OS ou prefixo'");
    }
}
