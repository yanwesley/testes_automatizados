package gherkin.stepdefinition.sinistro;

import br.com.api.model.sinistro.Veiculo;
import br.com.pom.sro.sinistro.novo_sinistro.veiculos.VeiculosNovoSinistroPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.testng.Assert;

import static br.com.api.GeradorDeMassa.getVeiculo;

public class AbaVeiculosSteps extends ReporterUtils {
    VeiculosNovoSinistroPage veiculosNovoSinistroPage = new VeiculosNovoSinistroPage();
    Veiculo veiculo = getVeiculo();

    @Quando("clico no botão Adicionar campo vazio")
    public void clicoNoBotãoAdicionarCampoVazio() {
        veiculosNovoSinistroPage.clicarBtnAdicionarCampoVazio();
        addLogToReport("Cliquei no botão para adicionar campo vazio.");
    }

    @Entao("deve adicionar mais uma linha na tabela de veículos")
    public void deveAdicionarMaisUmaLinhaNaTabelaDeVeículos() {
        Assert.assertTrue(veiculosNovoSinistroPage.getCountLinesOfTheTable() >= 1);
        veiculosNovoSinistroPage.validarQueFoiIncluidaUmaNovaLinha();
        addScreenshotToReport("Valido que foi incluído uma nova linha.");
    }

    @E("os dados da nova linha da tabela devem estar vazios")
    public void osDadosDaNovaLinhaDaTabelaDevemEstarVazios() {
        veiculosNovoSinistroPage.validarCamposVaziosDaLinha();
        addScreenshotToReport("Validou que os campos estão vazios.");
    }

    @Então("valido se ao alterar o tipo do veículo o ícone é alterado")
    public void validoSeAoAlterarOTipoDoVeículoOÍconeÉAlterado() {
        veiculosNovoSinistroPage.validarIconesConformeOTipo(veiculo);
    }

    @Quando("eu adicionar {int} campos vazios")
    public void euAdicionarCamposVazios(int quantidade) {
        for (int i = 0; i < quantidade; i++) {
            veiculosNovoSinistroPage.clicarBtnAdicionarCampoVazio();
        }
        addScreenshotToReport("Adicionei os novos veículos.");
    }

    @Entao("deve conter {int} novo(s) veiculo(s)")
    public void deveConterNovosVeiculos(int quantidade) {
        Assert.assertEquals(veiculosNovoSinistroPage.getCountLinesOfTheTable(), quantidade);
        addScreenshotToReport("Validei que foi adicionado os novos veículos.");
    }

    @E("ao selecionar {int} veículo(s)")
    public void aoSelecionarApenasVeículo(int quantidade) {
        veiculosNovoSinistroPage.selecionarVeiculos(quantidade);
        addScreenshotToReport("Selecionei os veículos.");
    }

    @E("clicar em Filtrar veículos marcados")
    public void clicarEmFiltrarVeículosMarcados() {
        veiculosNovoSinistroPage.clicarBtnFiltrarVeiculosMarcados();
    }

    @Entao("deve exibir {int} veículo(s)")
    public void deveExibirApenasVeículo(int quantidade) {
        Assert.assertEquals(veiculosNovoSinistroPage.getCountLinesOfTheTable(), quantidade);
        addScreenshotToReport("Validado exibição dos veículos");
    }

    @E("clicar em Exibir todos os veículos")
    public void clicarEmExibirTodosOsVeículos() {
        veiculosNovoSinistroPage.clicarBtnFiltrarVeiculosMarcados();
        addLogToReport("Cliquei no botão 'Exibir todos os veículos'");
    }

    @E("ao selecionar o tipo de veículo {string}")
    public void aoSelecionarOTipoDeVeículo(String tipo) {
        veiculosNovoSinistroPage.selecionarTipo(tipo);
    }

    @Entao("deve manter os campos Mercadoria e Perda readOnly")
    public void deveManterOsCamposMercadoriaEPerdaReadOnly() {
        veiculosNovoSinistroPage.validarSeCamposMercadoriaEPerdaEstaoReadOnly();
        addScreenshotToReport("Validado que campos estão apenas readOnly.");
    }

    @Então("valido se ao alterar o situação do veículo a quantidade de rodeiros é preenchida corretamente {string}")
    public void validoSeAoAlterarOSituaçãoDoVeículoAQuantidadeDeRodeirosÉPreenchidaCorretamenteTombadoPerdaTotalIncendiado(String tipo) {
        veiculosNovoSinistroPage.validarRodeirosConformeTipoESituacao(tipo);
    }

    @E("ao selecionar a situação {string}")
    public void aoSelecionarASituação(String situacao) {
        veiculosNovoSinistroPage.selecionarSituacao(situacao);
        addScreenshotToReport("Selecionei a situação : " + situacao);
    }

    @Então("deve permitir a edição dos rodeiros na linha {int}")
    public void devePermitirAEdiçãoDosRodeiros(int linhaVeiculo) {
        veiculosNovoSinistroPage.validarBtnEditarRodeirosEnabled(linhaVeiculo);
        addScreenshotToReport("Validei que o botão de editar rodeiros está ativo.");
    }

    @E("ao clicar para editar os rodeiros do veículo {int}")
    public void aoClicarParaEditarOsRodeiros(int linhaVeiculo) {
        veiculosNovoSinistroPage.clicarBtnEditarRodeiros(linhaVeiculo);
        addLogToReport("Cliquei para editar quantidade de rodeiros.");
    }

    @E("ao marcar apenas {int} rodeiros")
    public void aoMarcarApenasRodeiros(int quantidadeRodeiros) {
        veiculosNovoSinistroPage.selecionarRodeiros(quantidadeRodeiros);
        addScreenshotToReport("Selecionei rodeiros.");
    }

    @E("salvar a alteração de rodeiros")
    public void salvarAAlteraçãoDeRodeiros() {
        veiculosNovoSinistroPage.clicarBtnSalvarModalRodeiros();
        addLogToReport("Cliquei no botão Salvar no modal de rodeiros.");
    }

    @Então("deve atualizar a quantidade de rodeiros do veículo da linha {int} para {int}")
    public void deveAtualizarAQuantidadeDeRodeirosDoVeículoPara(int linha, int quantidadeRodeiros) {
        veiculosNovoSinistroPage.validarQuantidadeDeRodeirosSelecionados(linha, quantidadeRodeiros);
        addScreenshotToReport("Validei que apenas os rodeiros selecionados estão sendo contabilizado.");
    }

    @Entao("preencho todos os campos do veiculos {int}")
    public void preenchoTodosOsCamposDoVeiculos(int linha) {
        veiculosNovoSinistroPage.preencherCampos(linha - 1, veiculo);
        addScreenshotToReport("Preenchido campos do veículo.");
    }

    @E("clico em Salvar Informações dos Veículos")
    public void clicoEmSalvarInformaçõesDosVeículos() {
        veiculosNovoSinistroPage.clicarBtnSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar Informações.");
    }

    @E("o veículo deve ser exibido na lista de veículos na linha {int}")
    public void oVeículoDeveSerExibidoNaListaDeVeículosNaLinha(int linha) {
        veiculosNovoSinistroPage.validaVeiculoAdicionado(linha - 1, veiculo);
        addScreenshotToReport("Valida veículo adicionado");
    }

    @Entao("deve exibir a composição do trem carregada pela OS")
    public void deveExibirAComposiçãoDoTremCarregadaPelaOS() {
        Assert.assertTrue(veiculosNovoSinistroPage.getCountLinesOfTheTable() > 1);
        addScreenshotToReport("Carregou composição.");
    }

}
