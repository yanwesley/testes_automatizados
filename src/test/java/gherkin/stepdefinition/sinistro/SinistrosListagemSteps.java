package gherkin.stepdefinition.sinistro;

import br.com.pom.sro.HeaderMenuPage;
import br.com.pom.sro.sinistro.SinistrosListagemPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.io.File;

import static br.com.api.GeradorDeMassa.getSinistro;

public class SinistrosListagemSteps extends ReporterUtils {

    SinistrosListagemPage sinistrosListagemPage = new SinistrosListagemPage();
    HeaderMenuPage headerMenuPage = new HeaderMenuPage();

    @E("^seleciono a TAB de sinistros Abertos$")
    public void selecionoTABDeSinistrosAbertos() {
        sinistrosListagemPage.selecionarTABSinistrosAbertos();
        addScreenshotToReport("Selecionei a tab de Sinistros Abertos.");
    }

    @Deprecated()
    @E("seleciono a TAB de sinistros Recusados")
    public void selecionoATABDeSinistrosRecusados() {
        sinistrosListagemPage.selecionarTABSinistrosRecusados();
        addScreenshotToReport("Selecionei a tab de Sinistros Recusados.");
    }

    @E("seleciono a TAB de sinistros Encerrados")
    public void selecionoATABDeSinistrosEncerrados() {
        sinistrosListagemPage.selecionarTABSinistrosEncerrados();
        addScreenshotToReport("Selecionei a tab de Sinistros Encerrados.");
    }

    @Entao("deve exibir os campos permitidos para o perfil SRO")
    public void deveExibirOsCamposPermitidosParaOPerfilSRO() {
        sinistrosListagemPage.validaCamposParaPerfilSRO();
        addScreenshotToReport("Validando exibição dos campos permitidos para o perfil SRO");
    }

    @Quando("eu busco pelo texto {string}")
    public void euBuscoPeloTexto(String textoBusca) {
        sinistrosListagemPage.filtrarPorTexto(textoBusca);
        addScreenshotToReport("Digitado texto para busca : " + textoBusca);
    }

    @Entao("deve ser exibido os registros que contenham o texto {string}")
    public void deveSerExibidoOsRegistrosQueContenhamOTexto(String textoBusca) {
        sinistrosListagemPage.validaResultadoDaBuscaPorTexto(textoBusca);
        addScreenshotToReport("Validando resultado da busca");
    }

    @Entao("deve exibir a mensagem informando {string}")
    public void deveExibirAMensagemInformando(String msgSemResultado) {
        sinistrosListagemPage.validaExibicaoDaMensagemSemResultadoDaBusca(msgSemResultado);
        addScreenshotToReport("Validando exibição da mensagem de :  " + msgSemResultado);
    }

    @Quando("filtro por data inicial {string}")
    public void filtroPorDataInicial(String dataInicial) {
        sinistrosListagemPage.clicarBtnFiltrar();
        addScreenshotToReport("Cliquei no botão 'Filtrar'.");
        sinistrosListagemPage.filtrarPorDataInicial(dataInicial);
        addScreenshotToReport("Preenchi a data inicial: " + dataInicial);
        sinistrosListagemPage.clicarBtnAplicarFiltro();
        addLogToReport("Cliquei em 'Aplicar'.");
    }

    @Entao("deve exibir apenas os registros abertos a partir de {string}")
    public void deveExibirApenasOsRegistrosAbertosAPartirDe(String dataInicial) {
        sinistrosListagemPage.validaResultadoDaBuscaPorDataInicial(dataInicial);
        addScreenshotToReport("Validando resultado.");
    }

    @Quando("filtro por data final {string}")
    public void filtroPorDataFinal(String dataFinal) {
        sinistrosListagemPage.clicarBtnFiltrar();
        addScreenshotToReport("Cliquei no botão 'Filtrar'.");
        sinistrosListagemPage.limparCampoDataInicial();
        sinistrosListagemPage.filtrarPorDataFinal(dataFinal);
        addScreenshotToReport("Preenchi a data final: " + dataFinal);
        sinistrosListagemPage.clicarBtnAplicarFiltro();
        addLogToReport("Cliquei em 'Aplicar'.");
    }

    @Entao("deve exibir apenas os registros abertos até {string}")
    public void deveExibirApenasOsRegistrosAbertosAté(String dataFinal) {
        sinistrosListagemPage.validaResultadoDaBuscaPorDataFinal(dataFinal);
        addScreenshotToReport("Validando resultado.");
    }

    @Quando("clico no botão Exportar")
    public void clicoNoBotaoExportar() {
        sinistrosListagemPage.clicarBtnExportar();
        addLogToReport("Cliquei para exportar o arquivo XLS.");
    }

    @Entao("deve efetuar o download do arquivo XLS {string}")
    public void deveEfetuarODownloadDoArquivoXLS(String partialFileName) {
        File file = sinistrosListagemPage.validaDownloadDoArquivoXLS(partialFileName);
        addFileToReport("Efetuei o download do arquivo.", file.getAbsolutePath());
    }

    @Entao("deve exibir os campos permitidos em registros encerrados para o perfil SRO")
    public void deveExibirOsCamposPermitidosEmRegistrosEncerradosParaOPerfilSRO() {
        sinistrosListagemPage.validaCamposSinistrosEncerradosPerfilSRO();
        addScreenshotToReport("Validando exibição dos campos da tabela de Sinistros encerrados para o perfil SRO.");
    }

    @Entao("valido que o botão Novo Sinistro não deve ser exibido")
    public void validoQueOBotaoNovoSinistroNaoDeveSerExibido() {
        sinistrosListagemPage.validaQueNaoEstaExibindoBtnNovoSinistro();
        addScreenshotToReport("Validando que o botão:'Incluir novo sinistro' não é exibido.");
    }

    @Entao("valido que o botão Novo Sinistro é exibido")
    public void validoQueOBotaoNovoSinistroÉExibido() {
        sinistrosListagemPage.validaQueEstaExibindoBtnNovoSinistro();
        addScreenshotToReport("Validando a ocultação do botão:'Incluir novo sinistro'.");
    }

    @Entao("deve exibir os campos permitidos para o perfil CCO")
    public void deveExibirOsCamposPermitidosParaOPerfilCCO() {
        sinistrosListagemPage.validaExibicaoDosElementosPermitidosParaPerfilCCO();
        addScreenshotToReport("Validando exibição dos campos permitidos para o perfil CCO.");
    }

    @Entao("deve exibir os campos permitidos para o perfil CCO VIA")
    public void deveExibirOsCamposPermitidosParaOPerfilCCOVIA() {
        sinistrosListagemPage.validaExibicaoDosElementosPermitidosParaPerfilCCOVia();
        addScreenshotToReport("Validando exibição dos campos permitidos para o perfil CCO Via.");
    }

    @Entao("deve exibir os campos permitidos em registros Encerrados para o perfil CCO")
    public void deveExibirOsCamposPermitidosEmRegistrosEncerradosParaOPerfilCCO() {
        sinistrosListagemPage.validaCamposSinistrosEncerradosPerfilCCO();
        addScreenshotToReport("Validando exibição dos campos da tabela de Sinistros Encerrados para o perfil CCO.");
    }

    @Quando("clico no botão Incluir novo sinistro")
    public void clicoNoBotaoIncluirNovoSinistro() {
        sinistrosListagemPage.clicarBtnIncluirNovoSinistro();
        addLogToReport("Clicou no botão 'Incluir novo sinistros'.");
    }

    @Entao("deve exibir a pagina de listagem de sinistros")
    public void deveExibirAPaginaDeListagemDeSinistros() {
        sinistrosListagemPage.validaExibicaoDaPaginaListagemDeSinistros();
        addScreenshotToReport("Estou na página de Listagem de Sinistros.");
    }

    @Entao("deve exibir os campos da tabela de registros abertos permitidos para o perfil CCO")
    public void deveExibirOsCamposDaTabelaDeRegistrosAbertosPermitidosParaOPerfilCCO() {
        sinistrosListagemPage.validaCamposSinistrosAbertosPerfilCCO();
        addScreenshotToReport("Validando exibição dos campos da tabela para o perfil CCO.");
    }

    @Entao("deve exibir os campos da tabela de registros abertos permitidos para o perfil CCO VIA")
    public void deveExibirOsCamposDaTabelaDeRegistrosAbertosPermitidosParaOPerfilCCOVIA() {
        sinistrosListagemPage.validaCamposSinistrosAbertosPerfilCCOVia();
        addScreenshotToReport("Validando exibição dos campos da tabela para o perfil CCO Via.");
    }

    @Entao("deve exibir os campos da tabela de registros abertos permitidos para o perfil SRO")
    public void deveExibirOsCamposDaTabelaDeRegistrosAbertosPermitidosParaOPerfilSRO() {
        sinistrosListagemPage.validaCamposSinistrosAbertosPerfilSRO();
        addScreenshotToReport("Validando exibição dos campos da tabela para o perfil CCO Via.");
    }

    @Quando("filtro por data final {string} e data final {string}")
    public void filtroPorDataFinalEDataFinal(String dataInicial, String dataFinal) {
        sinistrosListagemPage.clicarBtnFiltrar();
        addScreenshotToReport("Cliquei no botão 'Filtrar'.");
        sinistrosListagemPage.filtrarPorDataInicial(dataInicial);
        sinistrosListagemPage.filtrarPorDataFinal(dataFinal);
        addScreenshotToReport("Preenchi as datas: " + dataInicial + " e " + dataFinal);
        sinistrosListagemPage.clicarBtnAplicarFiltro();
        addLogToReport("Cliquei em 'Aplicar'.");
    }

    @Quando("eu remover o filtro selecionado")
    public void euRemoverOFiltroSelecionado() {
        sinistrosListagemPage.clicarBtnRemoverFiltro();
        addLogToReport("Cliquei no botão 'Remover filtro'.");
    }

    @Entao("deve exibir os resultados dos últimos três meses")
    public void deveExibirOsResultadosDosUltimosMeses() {
        sinistrosListagemPage.validaResultadoAoRemoverFiltro();
        addScreenshotToReport("Validando resultado.");
    }

    @E("o botão filtrar deve estar com a cor {string}")
    public void oBotaoFiltrarDeveEstarComACor(String cor) {
        sinistrosListagemPage.validaCorBtnFiltrar(cor);
        addScreenshotToReport("Validando cor " + cor + " do botão 'Filtrar'.");
    }

    @Entao("valido que a lista de sinistros não está vazia")
    public void validoQueAListaDeSinistrosNãoEstáVazia() {
        sinistrosListagemPage.validarExibicaoDeUmRegistroNaLista();
        addScreenshotToReport("Validação de que a lista de sinistros não está vazia");
    }

    @E("clico no botão Editar do sinistro da linha {int}")
    public void clicoNoIconeDeEditar(int linha) {
        sinistrosListagemPage.clicarBtnEditarSinistro(linha - 1);
        addLogToReport("Clicou no botão Editar Sinistro");
    }

    @Quando("eu clico para editar o sinistro aberto via macro da linha {int}")
    public void euClicoParaEditarOSinistroAbertoViaMacroDaLinha(int linhaDoRegisto) {
        sinistrosListagemPage.clicarBtnEditarSinistro(linhaDoRegisto - 1);
        addLogToReport("Cliquei para editar o registro da linha: " + linhaDoRegisto);
    }

    @Quando("clicar no botão Editar do sinistro da linha {int}")
    public void clicarNoBotãoEditarDoSinistroDaLinha(int linhaDoRegisto) {
        sinistrosListagemPage.clicarBtnEditarSinistroSemOrdenar(linhaDoRegisto - 1);
        addLogToReport("Cliquei para editar o registro da linha: " + linhaDoRegisto);
    }

    @E("o sinistro deve ser exibido na Listagem de Sinistros abertos com o status de {string}")
    public void oSinistroDeveSerExibidoNaListagemDeSinistrosAbertos(String status) {
        headerMenuPage.clicarTABSinistros();
        sinistrosListagemPage.ordenarIdDecrescente();
        getSinistro().setStatus(status);
        sinistrosListagemPage.validarDadosDoRegistroAbertos(getSinistro());
        addScreenshotToReport("Validação da Listagem de Sinistros abertos  com o status: " + status);
    }

    @E("o sinistro deve ser exibido na Listagem de Sinistros abertos do SRO com o status de {string}")
    public void oSinistroDeveSerExibidoNaListagemDeSinistrosDoSROAbertos(String status) {
        headerMenuPage.clicarTABSinistros();
        sinistrosListagemPage.ordenarIdDecrescente();
        getSinistro().setStatus(status);
        sinistrosListagemPage.validarDadosDoRegistroAbertosSRO(getSinistro());
        addScreenshotToReport("Validação da Listagem de Sinistros abertos com o status: " + status);
    }

    @Quando("clicar no botão Editar do sinistro ordenado por ordem decrescente")
    public void clicarNoBotãoEditarDoSinistroOrdenadoPorOrdemDecrescente() {
        sinistrosListagemPage.clicarBntEditarSinistroOrdenadoDecrescente();
        addLogToReport("no botão Editar do sinistro");
    }
}
