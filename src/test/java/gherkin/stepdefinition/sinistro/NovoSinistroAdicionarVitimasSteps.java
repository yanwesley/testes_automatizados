package gherkin.stepdefinition.sinistro;

import br.com.pom.sro.sinistro.novo_sinistro.sinistro.AdicionarVitimasNovoSinistroPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class NovoSinistroAdicionarVitimasSteps extends ReporterUtils {

    AdicionarVitimasNovoSinistroPage adicionarVitimasNovoSinistroPage = new AdicionarVitimasNovoSinistroPage();

    @Quando("informo que existem vítimas")
    public void informoQueExistemVítimas() {
        adicionarVitimasNovoSinistroPage.informarQueExisteVitimas();
        addScreenshotToReport("Informei que existem Vitimas.");
    }

    @Entao("deve exibir os campos para preencher a quantidade de vítimas")
    public void deveExibirOsCamposParaPreencherAQuantidadeDeVítimas() {
        adicionarVitimasNovoSinistroPage.validarExibicaoDosCamposParaAdicionarVitimas();
        addScreenshotToReport("Validado exibição dos campos para preencher a quantidade de vítimas.");
    }

    @Quando("informo que não existem vítimas")
    public void informoQueNaoExistemVitimas() {
        adicionarVitimasNovoSinistroPage.informarQueNaoExisteVitimas();
        addScreenshotToReport("Informei que não existem Vitimas");
    }

    @Entao("não deve exibir os campos preencher a quantidade de vítimas")
    public void naoDeveExibirOsCamposPreencherAQuantidadeDeVítimas() {
        adicionarVitimasNovoSinistroPage.validarQueNaoExibiuOsCamposParaAdicionarVitimas();
        addScreenshotToReport("Validado exibição dos campos para preencher a quantidade de vítimas.");
    }

    @E("ao preencher com campos vazios a quantidade de vítimas")
    public void aoPreencherComCamposVaziosAQuantidadeDeVítimas() {
        adicionarVitimasNovoSinistroPage.preencherCamposVazios();
        addScreenshotToReport("Preencheu com campos vazios a quantidade de vítimas");
    }
}
