package gherkin.stepdefinition.sinistro;

import br.com.pom.sro.sinistro.SinistrosListagemPage;
import br.com.pom.sro.sinistro.novo_sinistro.sinistro.AceiteDoSinistroPage;
import br.com.pom.sro.sinistro.novo_sinistro.sinistro.HistoricoDoSinistro;
import br.com.pom.sro.sinistro.novo_sinistro.sinistro.SinistroSROPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

import java.util.List;

public class SinistroSROSteps extends ReporterUtils {

    SinistroSROPage sinistroSROPage = new SinistroSROPage();
    AceiteDoSinistroPage aceiteDoSinistroPage = new AceiteDoSinistroPage();
    HistoricoDoSinistro historicoDoSinistro = new HistoricoDoSinistro();
    SinistrosListagemPage sinistrosListagemPage = new SinistrosListagemPage();

    @Entao("clico na tab Dados do CCO Circulação")
    public void clicoNaTabDadosCCOCirculação() {
        sinistroSROPage.clicaTabDadosCCOCirculacao();
        addLogToReport("Clicou na tab Dados do CCO Circulação.");
    }

    @Entao("clico na tab Dados CCO VIA")
    public void clicoNaTabDadosCCOVIA() {
        sinistroSROPage.clicaTabDadosCCOVia();
        addLogToReport("Clicou na tab Dados CCO VIA.");
    }

    @Entao("clico na tab Aceite do Sinistro")
    public void clicoNaTabAceiteDoSinistro() {
        sinistroSROPage.clicaTabAceiteDoSinistro();
        addLogToReport("Clicou na tab Aceite do Sinistro.");
    }

    @Entao("clico na tab Histórico")
    public void clicoNaTabHistórico() {
        sinistroSROPage.clicaTabHistorico();
        addLogToReport("Clicou na tab Histórico.");
    }

    @E("clico em Salvar Informações na Edicao do SRO")
    public void clicoEmSalvarInformaçõesNaEdicaoDoSRO() {
        sinistroSROPage.clicarBtnSalvarInformacoes();
        addLogToReport("Cliquei no botão Salvar informações.");
    }

    @Entao("valido a mensagem informativa para prosseguir com o Aceite {string}")
    public void validoAMensagemInformativaParaProsseguirComOAceite(String msg) {
        aceiteDoSinistroPage.validaMsgInformativaParaProsseguirAceite(msg);
        addScreenshotToReport("Valida a mensagem informativa para prosseguir com o Aceite");
    }

    @Entao("preenche a Escolha do Aceite com Sim")
    public void preenchoAEscolhaDoAceiteComSim() {
        aceiteDoSinistroPage.preencheEscolhaDoAceiteSim();
        addScreenshotToReport("Preencheu Escolha do Aceite com Sim");
    }

    @E("preenche Classificação como Acidente")
    public void preencheClassificaçãoComoAcidente() {
        aceiteDoSinistroPage.preencheClassificacaoAcidente();
        addScreenshotToReport("Preenche Classificação como Acidente");
    }

    @E("preenche a Bandeira e a Causa Provável com a opção {int}")
    public void preencheBandeiraECausaProvávelComAOpção(int opcao) {
        aceiteDoSinistroPage.preencheBandeiraECausaProvavel(opcao);
        addScreenshotToReport("Preenche a Bandeira e a Causa Provável");
    }

    @Quando("clico em Confirmar Aceite")
    public void clicarEmConfirmarAceite() {
        aceiteDoSinistroPage.clicarBtnConfirmarAceite();
        addLogToReport("Clicou no botão Confirmar Aceite");
    }

    @Entao("preenche a Escolha do Aceite com Não")
    public void preencheAEscolhaDoAceiteComNão() {
        aceiteDoSinistroPage.preenchoAEscolhaDoAceiteComNao();
        addScreenshotToReport("Preencheu a Escolha do Aceite com Não");
    }

    @E("preenche o motivo da recusa")
    public void preencheOMotivoDaRecusa() {
        aceiteDoSinistroPage.preencheMotivoDaRecusa();
        addScreenshotToReport("Preencheu o motivo da recusa");
    }

    @E("insere {int} arquivo na recusa do sinistro")
    public void insereUmArquivo(int qtdArquivo) {
        aceiteDoSinistroPage.insereArquivo(qtdArquivo);
        addScreenshotToReport("Inseriu " + qtdArquivo + "arquivo(s) na recusa do sinistro");
    }

    @E("insere {int} arquivo na reabertura do sinistro")
    public void insereUmArquivoReabertura(int qtdArquivo) {
        aceiteDoSinistroPage.insereArquivo(qtdArquivo);
        addScreenshotToReport("Inseriu " + qtdArquivo + "arquivo(s) na reabertura do sinistro");
    }

    @Entao("valida que os {int} arquivos da recusa foram adicionados")
    public void validaQueOsArquivosDaRecusaForamAdicionados(int qtdArquivosAdicionados) {
        aceiteDoSinistroPage.validaQueOsArquivosForamAdicionados(qtdArquivosAdicionados);
        addScreenshotToReport("Adicionou " + qtdArquivosAdicionados + "arquivo(s)");
    }

    @Quando("clico no botão excluir arquivo da linha {int} da Recusa")
    public void clicoNoBotãoExcluirArquivoDaLinhaDaRecusa(int linha) {
        aceiteDoSinistroPage.excluirArquivoDaRecusa(linha);
        addScreenshotToReport("Excluiu um arquivo da lista de arquivos");
    }

    @Entao("clico em Confirmar Recusa")
    public void clicoEmConfirmarRecusa() {
        aceiteDoSinistroPage.clicarBtnConfirmarRecusa();
        addLogToReport("Clicou no botão Confirmar Recusa");
    }

    @E("volta para a lista de sinistros")
    public void voltaParaAListaDeSinistros() {
        sinistroSROPage.clicarBtnVoltar();
        addLogToReport("Clicou no botão voltar");
    }

    @Entao("valida que o Sinistro foi encerrado e não aceito")
    public void validaQueOSinistroFoiEncerradoENãoAceito() {
        sinistrosListagemPage.validaQueSinistroFoiEncerradoENãoAceito();
        addScreenshotToReport("Valida o sinistro na lista de 'Encerrados' e com o status de 'Aceito' = 'Não'");
    }

    @E("clico em confirmar reabertura")
    public void clicoEmConfirmarReabertura() {
        aceiteDoSinistroPage.clicarBtnConfirmarReabertura();
    }

    @E("insiro um comentário para reabertura do sinistro")
    public void insiroUmComentárioParaReaberturaDoSinistro() {
        aceiteDoSinistroPage.preencheComentarReabertura();
    }

    @Quando("clico no botão Reabrir Sinistro")
    public void clicoNoBotãoReabrirSinistro() {
        aceiteDoSinistroPage.clicarBtnReabrirSinistro();
    }

    @Entao("valida que o Sinistro foi encerrado e aceito")
    public void validaQueOSinistroFoiEncerradoEAceito() {
        sinistrosListagemPage.validaQueSinistroFoiEncerradoEAceito();
        addScreenshotToReport("Valida o sinistro na lista de 'Encerrados' e com o status de 'Aceito' = 'Sim'");
    }

    @Entao("deve exibir o histórico de aceite com {int} recusa e {int} reabertura")
    public void deveExibirOHistóricoDeAceiteComRecusaEReabertura(int qtdRecusa, int qtdReabertura) {
        aceiteDoSinistroPage.validaHistoricoReaberturaComQuantidade(qtdReabertura);
        addScreenshotToReport("Validei que está exibindo o registro de Reabertura");
        aceiteDoSinistroPage.validaHistoricoRecusaComQuantidade(qtdRecusa);
        addScreenshotToReport("Validei que está exibindo o registro de Recusa");
    }

    @Então("deve exibir a lista com as ações realizadas")
    public void deveExibirAAListaComAsAçõesRealizadas(List<String> listaAcao) {
        historicoDoSinistro.validaListaDeAcoes(listaAcao);
        addScreenshotToReport("Validou lista de ações.");
    }

    @E("clico para expandir os dados do sinistro")
    public void clicoParaExpandirOsDadosDoSinistro() {
        sinistroSROPage.clicarExpandirSinistro();
        addLogToReport("Clicou na tab Dados do CCO Circulação.");
    }
}
