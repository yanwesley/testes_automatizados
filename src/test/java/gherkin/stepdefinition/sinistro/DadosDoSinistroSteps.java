package gherkin.stepdefinition.sinistro;

import br.com.pom.sro.sinistro.novo_sinistro.sinistro.DadosDoSinistroPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;

public class DadosDoSinistroSteps extends ReporterUtils {

    DadosDoSinistroPage dadosDoSinistroPage = new DadosDoSinistroPage();

    @E("clico na tab Dados do sinistro")
    public void clicoNaTabDadosDoSinistro() {
        dadosDoSinistroPage.clicarNaTabDadosDoSinistro();
    }
}
