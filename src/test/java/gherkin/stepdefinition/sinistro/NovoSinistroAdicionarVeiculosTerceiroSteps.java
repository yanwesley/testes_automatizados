package gherkin.stepdefinition.sinistro;

import br.com.api.model.sinistro.VeiculoDeTerceiro;
import br.com.pom.sro.sinistro.novo_sinistro.sinistro.AdicionarVeiculosTerceiroNovoSinistroPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getNewVeiculoDeTerceiro;
import static br.com.api.GeradorDeMassa.getVeiculoDeTerceiro;

public class NovoSinistroAdicionarVeiculosTerceiroSteps extends ReporterUtils {
    VeiculoDeTerceiro veiculoDeTerceiro = getVeiculoDeTerceiro();
    VeiculoDeTerceiro veiculoDeTerceiroEdicao = getNewVeiculoDeTerceiro();

    AdicionarVeiculosTerceiroNovoSinistroPage adicionarVeiculosTerceiroNovoSinistroPage = new AdicionarVeiculosTerceiroNovoSinistroPage();

    @Quando("informo que existem veículos de terceiro")
    public void informoQueExistemVeiculosDeTerceiro() {
        adicionarVeiculosTerceiroNovoSinistroPage.informarQueExisteVeiculosDeTerceiro();
        addScreenshotToReport("Informei que existem veículos de terceiros.");
    }

    @Entao("deve exibir os campos para adicionar, remove ou editar")
    public void deveExibirOsCamposParaAdicionarRemoveOuEditar() {
        adicionarVeiculosTerceiroNovoSinistroPage.validaExibicaoDosCamposQuandoExisteVeiculosTerceiro();
        addScreenshotToReport("Validando campos.");
    }

    @Quando("informo que não existem veículos de terceiro")
    public void informoQueNaoExistemVeiculosDeTerceiro() {
        adicionarVeiculosTerceiroNovoSinistroPage.informarQueNaoExisteVeiculosDeTerceiro();
        addScreenshotToReport("Informei que existem veículos de terceiros.");
    }

    @Entao("não deve exibir os campos para adicionar, remove ou editar")
    public void naoDeveExibirOsCamposParaAdicionarRemoveOuEditar() {
        adicionarVeiculosTerceiroNovoSinistroPage.validaQueNaoExibiuCamposParaEditarVeiculosTerceiro();
        addScreenshotToReport("Validando campos.");
    }

    @E("clico no botão adicionar mais um veículo")
    public void clicoNoBotaoAdicionarMaisUmVeículo() {
        adicionarVeiculosTerceiroNovoSinistroPage.clicarBtnAdicionarMaisUmVeículo();
        addLogToReport("Cliquei para adicionar um novo veículo.");
    }

    @Entao("deve exibir a modal para adicionar ou editar veiculo")
    public void deveExibirAModalParaAdicionarEditarVeiculo() {
        adicionarVeiculosTerceiroNovoSinistroPage.validaModalAdicionarMaisUmVeículo();
        addScreenshotToReport("Validando modal.");
    }

    @E("preencho os dados para adicionar um novo veículo")
    public void preenchoOsDadosParaAdicionarUmNovoVeículo() {
        adicionarVeiculosTerceiroNovoSinistroPage.preencherCamposDaModalDeInclusaoDeNovoVeiculo(veiculoDeTerceiro);
        addScreenshotToReport("Preenchi dados do modal.");
    }

    @E("clico em salvar modal de adicionar adicionar ou editar veículo")
    public void clicoEmSalvarModalDeAdicionarNovoVeículo() {
        adicionarVeiculosTerceiroNovoSinistroPage.clicarBtnSalvarModalInclusao();
        addLogToReport("Salvei novo veiculo.");
    }

    @E("exibir o veiculo na tabela de veiculos de terceiro")
    public void exibirOVeiculoNaTabelaDeVeiculosDeTerceiro() {
        adicionarVeiculosTerceiroNovoSinistroPage.validarTabelaComVeiculo(1, veiculoDeTerceiro);
        addScreenshotToReport("Validando campos da tabela.");
    }

    @Entao("clico para editar o veiculo adicionado")
    public void clicoParaEditarOVeiculoAdicionado() {
        adicionarVeiculosTerceiroNovoSinistroPage.clicarBtnEditarVeiculo(0);
        addLogToReport("Cliquei para editar o veiculo.");
    }

    @E("altero os dados do veiculos")
    public void alteroOsDadosDoVeiculos() {
        adicionarVeiculosTerceiroNovoSinistroPage.preencherCamposDaModalDeInclusaoDeNovoVeiculo(veiculoDeTerceiroEdicao);
        addScreenshotToReport("Alterei dados do veículo");
    }

    @E("exibir o veiculo na tabela de veiculos de terceiro editado")
    public void exibirOVeiculoNaTabelaDeVeiculosDeTerceiroEditado() {
        adicionarVeiculosTerceiroNovoSinistroPage.validarTabelaComVeiculo(1, veiculoDeTerceiroEdicao);
        addScreenshotToReport("Validando campos da tabela.");
    }

    @Entao("clico para excluir o veículo adicionado")
    public void clicoParaExcluirOVeículoAdicionado() {
        adicionarVeiculosTerceiroNovoSinistroPage.clicarBtnExcluirVeiculo(0);
        addLogToReport("Cliquei no botão para excluir.");
    }

    @E("deve exibir o modal para excluir com a mensagem {string}")
    public void deveExibirOModalParaExcluirComAMensagem(String msg) {
        adicionarVeiculosTerceiroNovoSinistroPage.validaTextoModalDeExclusao(msg);
        addScreenshotToReport("Validando texto da modal: " + msg);
    }

    @Quando("clico em confirmar modal de excluir adicionar ou editar veículo")
    public void clicoEmConfirmarModalDeExcluirAdicionarOuEditarVeículo() {
        adicionarVeiculosTerceiroNovoSinistroPage.clicarBtnSalvarModalInclusao();
        addLogToReport("Cliquei para confirmar a exclusão.");
    }

    @E("remover o veiculo na tabela de veículos de terceiro")
    public void removerOVeiculoNaTabelaDeVeículosDeTerceiro() {
        adicionarVeiculosTerceiroNovoSinistroPage.validarTabelaComVeiculoRemovido();
        addScreenshotToReport("Validado tabela com veículo removido");
    }
}
