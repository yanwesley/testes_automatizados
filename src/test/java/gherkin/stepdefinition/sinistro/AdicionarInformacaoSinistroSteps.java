package gherkin.stepdefinition.sinistro;

import br.com.api.model.sinistro.MaquinaAcionada;
import br.com.api.model.sinistro.ResponsavelAcionado;
import br.com.api.model.sinistro.TurmaAcionada;
import br.com.pom.sro.sinistro.adicionar_infomacoes.MaquinasRecursosAcionadosPage;
import br.com.pom.sro.sinistro.adicionar_infomacoes.ResponsaveisAdicionaisPage;
import br.com.pom.sro.sinistro.adicionar_infomacoes.TurmasRecursosAcionadosPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.*;

import static br.com.api.GeradorDeMassa.*;


public class AdicionarInformacaoSinistroSteps extends ReporterUtils {

    ResponsaveisAdicionaisPage responsaveisAdicionaisPage = new ResponsaveisAdicionaisPage();
    MaquinasRecursosAcionadosPage maquinasRecursosAcionadosPage = new MaquinasRecursosAcionadosPage();
    TurmasRecursosAcionadosPage turmasRecursosAcionadosPage = new TurmasRecursosAcionadosPage();
    ResponsavelAcionado responsavelAcionado = getResponsavelAcionado();
    ResponsavelAcionado responsavelEditado = getNewResponsavelAcionado(true);
    MaquinaAcionada maquinaAcionada = getMaquinaAcionada();
    MaquinaAcionada maquinaAcionadaEditada = getNewMaquinaAcionada();
    TurmaAcionada turmaAcionada = getTurmaAcionada();
    TurmaAcionada turmaAcionadaEditada = getNewTurmaAcionada();

    @Entao("deve exibir a seção de Responsáveis adicionais")
    public void deveExibirASeçãoDeResponsáveisAdicionais() {
        responsaveisAdicionaisPage.validarSecaoResponsaveisAdicionais();
        addScreenshotToReport("Validou a aparição da seção Responsáveis Adicionais");
    }

    @Quando("seleciono a opção Sim para a pergunta Foram adicionados responsáveis?")
    public void selecionoAOpçãoSimParaAPerguntaForamAdicionadosResponsáveis() {
        responsaveisAdicionaisPage.selecionarRadioButtonSim();
        addScreenshotToReport("Selecionou Radio Button Sim");
    }

    @Quando("clico no botão Adicionar responsável")
    public void clicoNoBotãoAdicionarResponsável() {
        responsaveisAdicionaisPage.clicarBtnAdicionarResponsaveis();
        addLogToReport("Clicou no botão Adicionar Responsável");
    }

    @Entao("deve exibir o modal de Acionamento de responsáveis")
    public void deveExibirOModalDeAcionamentoDeResponsáveis() {
        responsaveisAdicionaisPage.validarModalAcionamentoDeResponsavel();
        addScreenshotToReport("Selecionou Radio Button Sim");
    }

    @Quando("preencher todos os campos do modal de Acionamento de responsáveis")
    public void preencherTodosOsCamposDoModalDeAcionamentoDeResponsáveis() {
        responsaveisAdicionaisPage.preencherModalAcionamentoResponsavel(responsavelAcionado);
        addScreenshotToReport("Preencheu o modal de Acionamento de Responsável");
    }

    @E("clico no botão Salvar")
    public void clicoNoBotãoSalvar() {
        responsaveisAdicionaisPage.clicarBtnSalvar();
        addLogToReport("Clicou no botão Salvar");
    }

    @Entao("o responsável deve ser inserido na seção de Responsáveis adicionados")
    public void oResponsávelDeveSerInseridoNaSeçãoDeResponsáveisAdicionados() {
        responsaveisAdicionaisPage.validarResponsavelAdicionado(responsavelAcionado);
        addLogToReport("O responsável foi adicionado");
    }

    @Dado("que existe um Responsável")
    public void queExisteUmResponsável() {
        responsaveisAdicionaisPage.validaSeExisteResponsavel();
        addScreenshotToReport("Responsável adicionado");
    }

    @Quando("clicar no botão Editar do responsável {int}")
    public void clicarNoBotãoEditarResponsavel(int linha) {
        responsaveisAdicionaisPage.clicarBtnEditarResponsavel(linha);
        addLogToReport("Clicou no botão editar do responsável");
    }

    @Então("deve ser alterado os dados do Responsável")
    public void deveSerAlteradoOsDadosDoResponsável() {
        responsaveisAdicionaisPage.alterarDadosResponsávelModal(responsavelEditado);
        addLogToReport("Dados do responsável alterado");
    }

    @E("os dados do responsável foram editado com sucesso")
    public void osDadosDoResponsávelForamEditadosComSucesso() {
        responsaveisAdicionaisPage.validaDadosAlteradosDoResponsavel(responsavelEditado);
        addScreenshotToReport("Os dados do Responsável foi alterado");
    }

    @Quando("clicar no botão Excluir")
    public void clicarNoBotãoExcluir() {
        responsaveisAdicionaisPage.excluiResponsavel();
        addLogToReport("Clicou no botão excluir");
    }

    @Entao("o Responsável deve ser excluído")
    public void oResponsávelDeveSerExcluído() {
        responsaveisAdicionaisPage.validaQueOResponsavelFoiExcluido();
        addScreenshotToReport("O Responsável foi excluído");
    }

    @Quando("confirmo a exclusão do Responsável")
    public void confirmoAExclusãoDoResponsável() {
        responsaveisAdicionaisPage.confirmaExclusaoDoResponsavel();
        addLogToReport("Clicou em confirmar exclusão do Responsável");
    }

    @Quando("clico no botão cancelar")
    public void clicoNoBotãoCancelar() {
        responsaveisAdicionaisPage.cancelaAdicaoDeNovoResponsavel();
        addLogToReport("Clicou em cancelar");
    }

    @Entao("deve exibir a seção de Recursos acionados - Máquinas")
    public void deveExibirASeçãoDeRecursosAcionadosMáquinas() {
        maquinasRecursosAcionadosPage.validarSecaoRecursosAcionadosMaquinas();
        addScreenshotToReport("Validado seção de Recursos acionados - Máquinas");
    }

    @Quando("seleciono a opção Sim para a pergunta Máquinas foram acionadas?")
    public void selecionoAOpçãoSimParaAPerguntaMáquinasForamAcionadas() {
        maquinasRecursosAcionadosPage.selecionarRadioButtonMaquinasForamAcionadasSim();
        addScreenshotToReport("Selecionei que máquinas foram acionadas.");
    }

    @Quando("clico no botão Adicionar máquina")
    public void clicoNoBotãoAdicionarMáquina() {
        maquinasRecursosAcionadosPage.clicarBtnAdicionarMaquina();
        addLogToReport("Cliquei no botão 'Adicionar Máquina.'");

    }

    @Entao("deve exibir o modal de Acionamento de máquina")
    public void deveExibirOModalDeAcionamentoDeMáquina() {
        maquinasRecursosAcionadosPage.validarModalAcionamentoDeMaquina();
        addScreenshotToReport("Validado modal de Acionamento de Máquinas.");
    }

    @Quando("preencher todos os campos do modal de Acionamento de máquina")
    public void preencherTodosOsCamposDoModalDeAcionamentoDeMáquina() {
        maquinasRecursosAcionadosPage.preencherModalAcionamentoDeMaquina(maquinaAcionada);
        addScreenshotToReport("Preenchi a modal de adicionar máquina");
    }

    @E("a máquina deve ser inserido na seção de Acionamento de máquina")
    public void aMáquinaDeveSerInseridoNaSeçãoDeAcionamentoDeMáquina() {
        maquinasRecursosAcionadosPage.validaQueAMaquinaFoiAdicionada(maquinaAcionada);
        addScreenshotToReport("Validado que a máquina foi acionada.");
    }

    @E("exibir a máquina na tabela de máquinas")
    public void exibirAMáquinaNaTabelaDeMáquinas() {
        maquinasRecursosAcionadosPage.validaQueAMaquinaFoiAdicionada(maquinaAcionadaEditada);
        addScreenshotToReport("Validado exibição da máquina apos edição.");
    }

    @Dado("que existe uma Máquina")
    public void queExisteUmaMáquina() {
        maquinasRecursosAcionadosPage.validaSeExisteMaquina();
        addScreenshotToReport("Validado que existe uma máquina adicionada");
    }

    @Quando("clicar no botão Editar máquina {int}")
    public void clicarNoBotãoEditarMáquina(int indexMaquina) {
        maquinasRecursosAcionadosPage.clicarBtnEditarMaquina(indexMaquina);
        addLogToReport("Cliquei no botão para editar a máquina: " + indexMaquina);
    }

    @E("ao alterar os dados da Máquina")
    public void aoAlterarOsDadosDaMáquina() {
        maquinasRecursosAcionadosPage.preencherModalAcionamentoDeMaquina(maquinaAcionadaEditada);
        addScreenshotToReport("Preenchi os campos alterados.");
    }

    @Quando("clicar no botão Excluir Máquina {int}")
    public void clicarNoBotãoExcluirMáquina(int indexMaquina) {
        maquinasRecursosAcionadosPage.clicarBtnExcluirMaquina(indexMaquina);
        addLogToReport("Cliquei no botão para editar a máquina: " + indexMaquina);
    }

    @Quando("confirmo a exclusão da Máquina")
    public void confirmoAExclusãoDaMáquina() {
        maquinasRecursosAcionadosPage.confirmarExclusaoDaMaquina();
        addLogToReport("Confirmei a exclusão.");
    }

    @E("a Máquina deve ser excluída")
    public void aMáquinaDeveSerExcluída() {
        maquinasRecursosAcionadosPage.validaSeAMaquinaFoiExcluida();
        addScreenshotToReport("Validado que a máquina foi excluida.");
    }

    @Entao("deve exibir a seção de Recursos acionados - Turmas")
    public void deveExibirASeçãoDeRecursosAcionadosTurmas() {
        turmasRecursosAcionadosPage.validarSecaoRecursosAcionadosTurmas();
    }

    @Quando("seleciono a opção Sim para a pergunta Turmas foram acionadas?")
    public void selecionoAOpçãoSimParaAPerguntaTurmasForamAcionadas() {
        turmasRecursosAcionadosPage.selecionarRadioButtonTurmasForamAcionadasSim();
    }

    @Quando("clico no botão Adicionar turma")
    public void clicoNoBotãoAdicionarTurma() {
        turmasRecursosAcionadosPage.clicarBtnAdicionarTurma();
    }

    @Quando("preencher todos os campos do modal de Acionamento de turmas")
    public void preencherTodosOsCamposDoModalDeAcionamentoDeTurmas() {
        turmasRecursosAcionadosPage.preencherModalAcionamentoDeTurmas(turmaAcionada);
    }

    @E("a turma deve ser inserida na seção de Acionamento de Turmas")
    public void aTurmaDeveSerInseridaNaSeçãoDeAcionamentoDeTurmas() {
        turmasRecursosAcionadosPage.validaQueATurmaFoiAdicionada(turmaAcionada);
        addScreenshotToReport("Validando que a turma foi adicionada.");
    }

    @Dado("que existe uma Turma")
    public void queExisteUmaTurma() {
        turmasRecursosAcionadosPage.validaSeExisteTurma();
        addScreenshotToReport("Validando que existe uma turma.");
    }

    @Quando("clicar no botão Editar turma {int}")
    public void clicarNoBotãoEditarTurma(int indexLinha) {
        turmasRecursosAcionadosPage.clicarBtnEditarTurma(indexLinha);
        addLogToReport("Cliquei para editar a turma da linha : " + indexLinha);
    }

    @E("ao alterar os dados da Turma")
    public void aoAlterarOsDadosDaTurma() {
        turmasRecursosAcionadosPage.preencherModalAcionamentoDeTurmas(turmaAcionadaEditada);
        addScreenshotToReport("Alterei os dados da turma.");
    }

    @E("exibir a turma na tabela de turmas")
    public void exibirATurmaNaTabelaDeTurmas() {
        turmasRecursosAcionadosPage.validaQueATurmaFoiAdicionada(turmaAcionadaEditada);
        addScreenshotToReport("Validado exibição da turma recém adicionada");
    }

    @Quando("clicar no botão Excluir Turma {int}")
    public void clicarNoBotãoExcluirTurma(int indexLinha) {
        turmasRecursosAcionadosPage.clicarBtnExcluirTurma(indexLinha);
        addLogToReport("Cliquei no botão de excluir turma da linha : " + indexLinha);
    }

    @Quando("confirmo a exclusão da Turma")
    public void confirmoAExclusãoDaTurma() {
        turmasRecursosAcionadosPage.confirmarExclusaoDaTurma();
        addLogToReport("Cliquei para confirmar a exclusão da turma.");
    }

    @E("a Turma deve ser excluída")
    public void aTurmaDeveSerExcluída() {
        turmasRecursosAcionadosPage.validaSeATurmaFoiExcluida();
        addScreenshotToReport("Validado que a turma foi excluida.");
    }

    @Entao("deve exibir o modal de Acionamento de turmas")
    public void deveExibirOModalDeAcionamentoDeTurmas() {
        turmasRecursosAcionadosPage.validarModalAcionamentoDeTurma();
        addScreenshotToReport("Validado exibição do modal.");
    }

    @Entao("o responsável deve ser inserido na seção de Responsáveis adicionados com os dados ReadOnly")
    public void oResponsávelDeveSerInseridoNaSeçãoDeResponsáveisAdicionadosComOsDadosReadOnly() {
        responsaveisAdicionaisPage.validarResponsavelAdicionadoReadOnly(responsavelAcionado);
        addLogToReport("Valida o responsável foi adicionado");
    }

    @E("exibir a máquina na tabela de máquinas com os dados ReadOnly")
    public void exibirAMáquinaNaTabelaDeMáquinasComOsDadosReadOnly() {
        maquinasRecursosAcionadosPage.validaQueAMaquinaFoiAdicionadaReadOnly(maquinaAcionada);
        addScreenshotToReport("Validado exibição da máquina.");
    }

    @E("a turma deve ser inserida na seção de Acionamento de Turmas com os dados ReadOnly")
    public void aTurmaDeveSerInseridaNaSeçãoDeAcionamentoDeTurmasComOsDadosReadOnly() {
        turmasRecursosAcionadosPage.validaQueATurmaFoiAdicionadaReadOnly(turmaAcionada);
        addScreenshotToReport("Validando que a turma foi adicionada.");
    }
}
