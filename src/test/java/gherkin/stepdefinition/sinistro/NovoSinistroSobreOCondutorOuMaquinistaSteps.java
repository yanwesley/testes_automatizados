package gherkin.stepdefinition.sinistro;

import br.com.api.model.sinistro.ResponsavelAcionado;
import br.com.pom.sro.sinistro.novo_sinistro.sinistro.SobreOCondutorOuMaquinistaNovoSinistroPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getNewResponsavelAcionado;

public class NovoSinistroSobreOCondutorOuMaquinistaSteps extends ReporterUtils {

    SobreOCondutorOuMaquinistaNovoSinistroPage sobreOCondutorOuMaquinistaNovoSinistroPage = new SobreOCondutorOuMaquinistaNovoSinistroPage();
    ResponsavelAcionado responsavelAcionado = getNewResponsavelAcionado(false);


    @Quando("informo que é condutor ou maquinista da RUMO")
    public void informoQueECondutorOuMaquinistaDaRUMO() {
        sobreOCondutorOuMaquinistaNovoSinistroPage.clicarNaOptionSim();
        addScreenshotToReport("Cliquei na opção sim.");
    }

    @Entao("deve exibir os campos para adicionar novos condutores ou maquinistas")
    public void deveExibirOsCamposParaAdicionarNovosCondutoresOuMaquinistas() {
        sobreOCondutorOuMaquinistaNovoSinistroPage.validaCamposParaAdicionarColaborador();
        addScreenshotToReport("Validando exibição do campos para adicionar colaborador.");
    }

    @Quando("informo que não é condutor ou maquinista da RUMO")
    public void informoQueNãoÉCondutorOuMaquinistaDaRUMO() {
        sobreOCondutorOuMaquinistaNovoSinistroPage.clicarNaOptionNao();
        addScreenshotToReport("Cliquei na opção nao.");
    }

    @E("ao editar o condutor ou maquinista")
    public void aoEditarOCondutorOuMaquinista() {
        sobreOCondutorOuMaquinistaNovoSinistroPage.clicarBtnEditarCondutor();
        addLogToReport("Cliquei no botão Editar.");
        sobreOCondutorOuMaquinistaNovoSinistroPage.buscarPorNomeDoColaborador(responsavelAcionado);
        addScreenshotToReport("Busquei pelo nome do Colaborador");
        sobreOCondutorOuMaquinistaNovoSinistroPage.selecionarColaborador(0);
        addScreenshotToReport("Selecionei o colaborador");

    }

    @E("deve exibir o colaborador selecionado")
    public void deveExibirOColaboradorSelecionado() {
        sobreOCondutorOuMaquinistaNovoSinistroPage.validaExibicaoDoColaboradorSelecionado(responsavelAcionado);
        addScreenshotToReport("Validando exibição do colaborador selecionado.");
    }

    @Entao("não deve exibir os campos para adicionar novos condutores ou maquinistas")
    public void nãoDeveExibirOsCamposParaAdicionarNovosCondutoresOuMaquinistas() {
        sobreOCondutorOuMaquinistaNovoSinistroPage.validaOcultacaoDosCamposParaAdicionarColaborador();
        addScreenshotToReport("Validando ocultação dos campos para adicionar colaborador.");
    }

    @E("clicar em Salvar adição de Condutor")
    public void clicarEmSalvarAdiçãoDeCondutor() {
        sobreOCondutorOuMaquinistaNovoSinistroPage.clicarBtnSalvarModal();
        addLogToReport("Cliquei no botão para Salvar");
    }

    @E("clicar em Cancelar adição de Condutor")
    public void clicarEmCancelarAdiçãoDeCondutor() {
        sobreOCondutorOuMaquinistaNovoSinistroPage.clicarBtnCancelarModal();
        addLogToReport("Cliquei no botão para Cancelar");
    }
}
