package gherkin.stepdefinition.administracao;

import br.com.api.model.informacoes.AtualizacaoDoSistema;
import br.com.api.model.informacoes.BlocoDeTexto;
import br.com.pom.administracao.DocumentacaoInformacoesSROAdministracaoPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.*;

public class DocumentacaoInformacoesDaSROAdministracaoSteps extends ReporterUtils {

    DocumentacaoInformacoesSROAdministracaoPage documentacaoInformacoesSROAdministracaoPage = new DocumentacaoInformacoesSROAdministracaoPage();
    AtualizacaoDoSistema atualizacaoDoSistema = getAtualizacaoDoSistema();
    AtualizacaoDoSistema atualizacaoDoSistemaEditado = getNewAtualizacaoDoSistema();
    BlocoDeTexto blocoDeTexto = getBlocoDeTexto();

    @E("acesso a TAB Documentação no Administração")
    public void acessoATABDocumentaçãoNoAdministração() {
        documentacaoInformacoesSROAdministracaoPage.acessarTABDocumentacao();
        addLogToReport("Acessei a TAB documentação.");
    }

    @Então("edito a nova versão no Administração")
    public void editoANovaVersãoNoAdministração() {
        documentacaoInformacoesSROAdministracaoPage.editarModalVersao(atualizacaoDoSistemaEditado);
        addScreenshotToReport("Editei a versão");
    }

    @E("clico no botão Salvar no modal de Adicionar Versão")
    public void clicoNoBotãoSalvarNoModalDeAdicionarVersão() {
        documentacaoInformacoesSROAdministracaoPage.clicarBtnSalvarModalVersao();
        addLogToReport("Cliquei no botão Salvar");
    }

    @E("valido que as informações editadas da atualização do sistema persistiram no Administração")
    public void validoQueAsInformaçõesEditadasDaAtualizaçãoDoSistemaPersistiramNoAdministração() {
        documentacaoInformacoesSROAdministracaoPage.validarDadosDaTabelaDeVersao(atualizacaoDoSistemaEditado, 1, true);
        addScreenshotToReport("Validei os dados editados");
    }

    @E("valido que as informações da atualização do sistema persistiram no Administração")
    public void validoQueAsInformaçõesDaAtualizaçãoDoSistemaPersistiramNoAdministração() {
        documentacaoInformacoesSROAdministracaoPage.validarDadosDaTabelaDeVersao(atualizacaoDoSistema, 1, false);
        addScreenshotToReport("Validei os dados inseridos");
    }

    @Então("adiciono uma nova versão no Administração")
    public void adicionoUmaNovaVersãoNoAdministração() {
        documentacaoInformacoesSROAdministracaoPage.preencherModalVersao(atualizacaoDoSistema);
        addScreenshotToReport("Preenchi os dados.");
    }

    @Então("clico no botão para excluir a versão da linha {int} no Administração")
    public void clicoNoBotãoParaExcluirAVersãoDaLinhaNoAdministração(int linha) {
        documentacaoInformacoesSROAdministracaoPage.clicarBtnExcluirVersao(linha);
        addLogToReport("Cliquei no botão de excluir versão.");
    }

    @E("clico no botão Confirmar no modal de Adicionar Versão")
    public void clicoNoBotãoConfirmarNoModalDeAdicionarVersão() {
        documentacaoInformacoesSROAdministracaoPage.clicarBtnConfirmarExclusaoModalVersao();
        addLogToReport("Confirmei a exclusão.");
    }

    @E("clico para adicionar uma nova versão no Administração")
    public void clicoParaAdicionarUmaNovaVersãoNoAdministração() {
        documentacaoInformacoesSROAdministracaoPage.clicarBtnAdicionarVersao();
        addLogToReport("Cliquei no botão adicionar nova versão.");
    }

    @E("clico para editar a versão da linha {int} no Administração")
    public void clicoParaEditarAVersãoDaLinhaNoAdministração(int linha) {
        documentacaoInformacoesSROAdministracaoPage.clicarBtnEditarVersao(linha);
        addLogToReport("Cliquei no botão editar versão.");
    }

    @Então("preencho a Documentação oficial do projeto")
    public void preenchoADocumentaçãoOficialDoProjeto() {
        documentacaoInformacoesSROAdministracaoPage.preencherDocumentacaoOficialDoProjeto(atualizacaoDoSistema);
        addScreenshotToReport("Preencho a Documentação oficial do projeto");
    }

    @E("valido que as informações de Documentação oficial do projeto no Administração persistiram")
    public void validoQueAsInformaçõesDeDocumentaçãoOficialDoProjetoNoAdministraçãoPersistiram() {
        documentacaoInformacoesSROAdministracaoPage.validarDocumentacaoOficialDoProjeto(atualizacaoDoSistema);
        addScreenshotToReport("Valido que as informações de Documentação oficial do projeto no Administração persistiram");
    }

    @E("clico em Adicionar bloco de texto na Documentação no Administração")
    public void clicoEmAdicionarBlocoDeTextoNaDocumentaçãoNoAdministração() {
        documentacaoInformacoesSROAdministracaoPage.clicarBtnAdicionarBlocoDeTexto();
        addLogToReport("Cliquei no botão");
    }

    @Entao("preencho os dados do bloco de texto Documentação no Administração")
    public void preenchoOsDadosDoBlocoDeTextoDocumentaçãoNoAdministração() {
        documentacaoInformacoesSROAdministracaoPage.preencherBlocoDeTexto(blocoDeTexto, 1);
        addScreenshotToReport("Preenchi os dados do bloco de texto.");
    }

    @E("valido que o bloco de texto Documentação do projeto no Administração persistiram")
    public void validoQueOBlocoDeTextoDocumentaçãoDoProjetoNoAdministraçãoPersistiram() {
        documentacaoInformacoesSROAdministracaoPage.validarBlocoDeTexto(blocoDeTexto, 1);
        addScreenshotToReport("Validei os dados do bloco de texto.");
    }

    @Entao("clico em Excluir bloco de texto na Documentação no Administração")
    public void clicoEmExcluirBlocoDeTextoNaDocumentaçãoNoAdministração() {
        documentacaoInformacoesSROAdministracaoPage.clicarBtnExcluirBlocoDeTexto(1);
        addScreenshotToReport("Cliquei no botão excluir bloco de texto");
    }

    @E("confirmo a exclusão do bloco de texto no modal")
    public void confirmoAExclusãoDoBlocoDeTextoNoModal() {
        documentacaoInformacoesSROAdministracaoPage.clicarBtnConfirmarExcluirBlocoDeTexto();
        addScreenshotToReport("Cliquei no botão excluir bloco de texto");
    }
}
