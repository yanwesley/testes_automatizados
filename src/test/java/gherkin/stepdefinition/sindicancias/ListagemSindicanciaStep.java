package gherkin.stepdefinition.sindicancias;

import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.HeaderMenuPage;
import br.com.pom.sro.sindicancias.SindicanciaListagemPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.*;

import static br.com.api.GeradorDeMassa.getSinistro;

public class ListagemSindicanciaStep extends ReporterUtils {

    SindicanciaListagemPage sindicanciaListagemPage = new SindicanciaListagemPage();
    HeaderMenuPage headerMenuPage = new HeaderMenuPage();
    Sinistro sinistro = getSinistro();

    @Entao("deve exibir a pagina de listagem de Sindicâncias")
    public void deveExibirAPaginaDeListagemDeSindicâncias() {
        sindicanciaListagemPage.validaExibicaoDaPagina();
        addScreenshotToReport("valida exibição da listagem");
    }

    @Dado("que tenha {int} registro na tabela de registros")
    public void queTenhaRegistroNaTabelaDeRegistros(int linhas) {
        sindicanciaListagemPage.validaRegistro(linhas);
        addScreenshotToReport("Validei que existem registros.");
    }

    @Então("eu valido que os Dias em aberto da linha {int} está calculado certo")
    public void euValidoQueOsDiasEmAbertoEstáCalculandoCerto(int linha) {
        sindicanciaListagemPage.validarDiasEmAberto(linha);
        addScreenshotToReport("Validei o valor de dias em aberto");
    }

    @E("clicar no botão Editar da sindicancia ordenado por ordem decrescente")
    public void clicarNoBotãoEditarDaSindicanciaOrdenadoPorOrdemDecrescente() {
        sindicanciaListagemPage.ordenarIdDecrescente();
        sindicanciaListagemPage.clicarBtnEditarListagem(1);
        addLogToReport("Cliquei no botão Editar do sinistro");
    }

    @E("ordeno a sindicancia por ordem decrescente")
    public void ordenarSindicanciaPorOrdemDecrescente() {
        sindicanciaListagemPage.ordenarIdDecrescente();
        addLogToReport("Editei por ordem decrescente");
    }

    @Quando("lista de sindicancia ordenado por ordem decrescente")
    public void listaDeSindicanciaOrdenadoPorOrdemDecrescente() {
        sindicanciaListagemPage.ordenarIdDecrescente();
        addLogToReport("Cliquei no botão Editar do sinistro");
    }

    @Entao("eu valido o status conclusão não iniciado")
    public void euValidoOStatusConclusãoNãoIniciado() {
        sindicanciaListagemPage.validarStatusConclusaoNaoIniciado();
        addScreenshotToReport("Validei os status de conclusão - Não iniciados");
    }

    @Entao("eu valido o status conclusão Finalizado")
    public void euValidoOStatusConclusãoFinalizado() {
        sindicanciaListagemPage.validarStatusConclusaoFinalizado();
        addScreenshotToReport("Validei os status de conclusão - Finalizados");
    }

    @E("eu valido se o status conclusão da area está Finalizado")
    public void euValidoSeOStatusConclusãoDaAreaEstáFinalizado() {
        sindicanciaListagemPage.validarStatusConclusaoDaAreaFinalizado();
        addScreenshotToReport("Validei os status de conclusão - Finalizados");
    }

    @Entao("valido que a classificação é {string} na listagem sindicância {int}")
    public void validoQueAClassificaçãoÉNaListagemSindicância(String classificacao, int linha) {
        sindicanciaListagemPage.validaStatusClassificacao(linha, classificacao);
        addScreenshotToReport("Validado que a classificação da sindicância é :" + classificacao);
    }

    @E("valido que o status da sindicância é {string}")
    public void validoQueOStatusDaSindicânciaÉ(String status) {
        sindicanciaListagemPage.validarStatusSindicancia(status, 1);
        addScreenshotToReport("Validei o status da sindicância é : " + status);
    }

    @E("clicar no botão Editar da sindicancia")
    public void clicarNoBotãoEditarDaSindicancia() {
        sindicanciaListagemPage.clicarBtnEditarListagem(1);
        addLogToReport("Cliquei no botão editar.");
    }

    @Entao("eu valido o status iniciado")
    public void euValidoOStatusIniciado() {
        sindicanciaListagemPage.validarStatusConclusaoIniciado();
        addScreenshotToReport("Validei os status de iniciado");
    }

    @Então("valido que a sindicancia está finalizada")
    public void validoQueASindicanciaEstáFinalizada() {
        sindicanciaListagemPage.validaSindicanciaFinalizada(sinistro, 1);
    }
}
