package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.*;
import br.com.api.model.sinistro.Veiculo;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.CustosSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.*;

import static br.com.api.GeradorDeMassa.*;


public class CustosAreaSindicanciaSteps extends ReporterUtils {
    CustosSindicanciaPage custosSindicanciaPage = new CustosSindicanciaPage();
    CustoMaterial custoMaterial = getCustoMaterial();
    CustoMaterial custoMaterialEditado = getNewCustoMaterial();
    CustoMaquina custoMaquina = getCustoMaquina();
    CustoMaquina custoMaquinaEditado = getNewCustoMaquina();
    CustoOutros custoOutros = getCustoOutros();
    CustoOutros custoOutrosEditado = getNewCustoOutros();
    CustoTransporte custoTransporte = getCustoTransporte();
    CustoTransporte custoTransporteEditado = getNewCustoTransporte();
    CustoColaborador custoColaborador = getCustoColaborador();
    CustoColaborador custoColaboradorEditado = getNewCustoColaborador();
    CustoVagoes custoVagoes = getCustoVagoes();
    CustoVagoes custoVagoesEditado = getNewCustoVagoes();
    CustoLocomotiva custoLocomotiva = getCustoLocomotiva();
    CustoLocomotiva custoLocomotivaEditado = getNewCustoLocomotiva();
    CustoVeiculosVia custoVeiculosVia = getCustoVeiculosVia();
    CustoVeiculosVia custoVeiculosViaEditado = getNewCustoVeiculosVia();
    Veiculo veiculo = getVeiculo();

    @Entao("deve exibir a opção de selecionar se houve custos")
    public void deveExibirAOpcaoDeSelecionarSeHouveCustos() {
        custosSindicanciaPage.validaExibicaoDaOpcaoDeHouveCustos();
        addScreenshotToReport("Validou exibição da opção 'Houve custos na área'.");
    }

    @E("ao optar por adicionar Custos")
    public void aoOptarPorAdicionarCustos() {
        custosSindicanciaPage.clicarOptionHouveCustosSim();
        addScreenshotToReport("Selecionei que há custos.");
    }

    @Entao("deve exibir o botão para Incluir custo")
    public void deveExibirOBotaoParaIncluirCusto() {
        custosSindicanciaPage.validaExibicaoDoBtnIncluirCusto();
        addScreenshotToReport("Validado que exibiu o botão para 'Incluir custo'");
    }

    @E("ao adicionar um novo custo de Materiais")
    public void aoAdicionarUmNovoCustoDeMateriais() {
        custosSindicanciaPage.clicarBtnIncluirCusto();
        addLogToReport("Cliquei no botão para incluir novo custo.");
        custosSindicanciaPage.adicionarNovoCustoMaterial(custoMaterial);
        addScreenshotToReport("Preenchido dados do modal de custos.");
    }

    @E("o custo deve ser exibido na tabela de Materiais na linha {int}")
    public void oCustoDeveSerExibidoNaTabelaDeMateriais(int linha) {
        custosSindicanciaPage.validaCustoMaterial(custoMaterial, linha);
        addScreenshotToReport("Validado custo inserido.");
    }

    @E("ao editar o custo de material da linha {int}")
    public void aoEditarOCustoDeMaterialDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnEditarMaterial(linha);
        addLogToReport("cliquei no botão de edição do custo");
        custosSindicanciaPage.editarCustoMaterial(custoMaterialEditado);
        addScreenshotToReport("Editei as informações do custo.");
    }

    @E("clicar no botão Salvar Modal de custos")
    public void clicarNoBotãoSalvarModalDeCustos() {
        custosSindicanciaPage.clicarBtnSalvarModal();
        addScreenshotToReport("Editei o custo.");
    }

    @E("o custo editado deve ser exibido na tabela de Materiais na linha {int}")
    public void oCustoEditadoDeveSerExibidoNaTabelaDeMateriaisNaLinha(int linha) {
        custosSindicanciaPage.validaCustoMaterial(custoMaterialEditado, linha);
        addScreenshotToReport("Validado edição do custo.");
    }

    @E("clicar para excluir o custo material da linha {int}")
    public void clicarParaExcluirOCustoMaterialDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnExcluirCustoMaterial(linha);
    }

    @Então("deve exibir o modal de confirmação de exclusão")
    public void deveExibirOModalDeConfirmaçãoDeExclusão() {
        custosSindicanciaPage.validaModalDeExclusaoDoCusto();
        addScreenshotToReport("Validado exibição do modal de confirmação de exclusão.");
    }

    @E("confirmar a exclusao do custo")
    public void confirmarAExclusaoDoCusto() {
        custosSindicanciaPage.clicarBtnSimModalExclusao();
        addLogToReport("Cliquei em 'Sim' no modal de confirmação de exclusão.");
    }

    @E("não deve exibir a tabela de custo material")
    public void nãoDeveExibirATabelaDeCustoMaterial() {
        custosSindicanciaPage.validaQueOCustoMaterialFoiExcluido();
        addScreenshotToReport("Validado que o custo foi removido");
    }

    @E("ao adicionar um novo custo de Máquinas")
    public void aoAdicionarUmNovoCustoDeMáquinas() {
        custosSindicanciaPage.clicarBtnIncluirCusto();
        addLogToReport("Cliquei no botão para incluir novo custo.");
        custosSindicanciaPage.adicionarNovoCustoMaquinas(custoMaquina);
        addScreenshotToReport("Preenchido dados do modal de custos.");
    }

    @E("o custo deve ser exibido na tabela de Máquinas na linha {int}")
    public void oCustoDeveSerExibidoNaTabelaDeMáquinasNaLinha(int linha) {
        custosSindicanciaPage.validaCustoMaquina(custoMaquina, linha);
        addScreenshotToReport("Validado dados do custo inserido.");
    }

    @E("ao editar o custo de Máquinas da linha {int}")
    public void aoEditarOCustoDeMáquinasDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnEditarMaquina(linha);
        addLogToReport("Cliquei no botão para editar novo custo.");
        custosSindicanciaPage.editarCustoMaquina(custoMaquinaEditado);
        addScreenshotToReport("Editei dados do modal de custos.");
    }

    @E("o custo editado deve ser exibido na tabela de Máquinas na linha {int}")
    public void oCustoEditadoDeveSerExibidoNaTabelaDeMáquinasNaLinha(int linha) {
        custosSindicanciaPage.validaCustoMaquina(custoMaquinaEditado, linha);
        addScreenshotToReport("Validado dados do custo editado.");
    }

    @E("clicar para excluir o custo Máquinas da linha {int}")
    public void clicarParaExcluirOCustoMáquinasDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnExcluirCustoMaquina(linha);
        addLogToReport("Cliquei no botão de excluir máquina.");
    }

    @E("não deve exibir a tabela de custo Máquinas")
    public void nãoDeveExibirATabelaDeCustoMáquinas() {
        custosSindicanciaPage.validaQueOCustoMaquinaFoiExcluido();
        addScreenshotToReport("Validado que o custo foi removido");
    }

    @E("ao adicionar um novo custo")
    public void aoAdicionarUmNovoCusto() {
        custosSindicanciaPage.clicarBtnIncluirCusto();
        addLogToReport("Cliquei no botão para incluir novo custo.");
        custosSindicanciaPage.adicionarNovoCustoOutros(custoOutros);
        addScreenshotToReport("Preenchido dados do modal de custos.");
    }

    @E("o custo deve ser exibido na tabela de custos na linha {int}")
    public void oCustoDeveSerExibidoNaTabelaDeCustosNaLinha(int linha) {
        custosSindicanciaPage.validaCustoOutros(custoOutros, linha);
        addScreenshotToReport("Validado dados do custo inserido.");
    }

    @E("ao editar o custo da linha {int}")
    public void aoEditarOCustoDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnEditarCustoOutros(linha);
        addLogToReport("Cliquei no botão para editar o custo.");
        custosSindicanciaPage.editarCustoOutros(custoOutrosEditado);
        addScreenshotToReport("Alterei dados do modal de custos.");
    }

    @E("o custo editado deve ser exibido na tabela de custos na linha {int}")
    public void oCustoEditadoDeveSerExibidoNaTabelaDeCustosNaLinha(int linha) {
        custosSindicanciaPage.validaCustoOutros(custoOutrosEditado, linha);
        addScreenshotToReport("Validado dados do custo editado.");
    }

    @E("clicar para excluir o custo da linha {int}")
    public void clicarParaExcluirOCustoDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnExcluirCustoOutros(linha);
        addLogToReport("Cliquei no botão para editar o custo.");
    }

    @E("não deve exibir a tabela de custo")
    public void nãoDeveExibirATabelaDeCusto() {
        custosSindicanciaPage.validaQueOCustoOutrosFoiExcluido();
        addScreenshotToReport("Validado que o custo foi removido");
    }

    @E("ao adicionar um novo custo de Transporte")
    public void aoAdicionarUmNovoCustoDeTransporte() {
        custosSindicanciaPage.clicarBtnIncluirCusto();
        addLogToReport("Cliquei no botão para incluir novo custo.");
        custosSindicanciaPage.adicionarNovoCustoTransporte(custoTransporte);
        addScreenshotToReport("Preenchido dados do modal de custos.");
    }

    @E("o custo deve ser exibido na tabela de Transporte na linha {int}")
    public void oCustoDeveSerExibidoNaTabelaDeTransporteNaLinha(int linha) {
        custosSindicanciaPage.validaCustoTransporte(custoTransporte, linha);
        addScreenshotToReport("Validado dados do custo inserido.");
    }

    @Quando("clicar para excluir o custo Transporte da linha {int}")
    public void clicarParaExcluirOCustoTransporteDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnExcluirTransporte(linha);
        addLogToReport("Cliquei no botão para editar o custo.");
    }

    @E("não deve exibir a tabela de custo Transporte")
    public void nãoDeveExibirATabelaDeCustoTransporte() {
        custosSindicanciaPage.validaQueOCustoTransporteFoiExcluido();
        addScreenshotToReport("Validado que o custo foi removido");
    }

    @E("ao editar o custo de Transporte da linha {int}")
    public void aoEditarOCustoDeTransporteDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnEditarCustoTransporte(linha);
        addLogToReport("Cliquei no botão para editar o custo.");
        custosSindicanciaPage.editarCustoTransporte(custoTransporteEditado);
        addScreenshotToReport("Alterei dados do modal de custos.");
    }

    @E("o custo editado deve ser exibido na tabela de Transporte na linha {int}")
    public void oCustoEditadoDeveSerExibidoNaTabelaDeTransporteNaLinha(int linha) {
        custosSindicanciaPage.validaCustoTransporte(custoTransporteEditado, linha);
        addScreenshotToReport("Validado dados do custo editado.");
    }

    @E("ao adicionar um novo custo de Colaborador")
    public void aoAdicionarUmNovoCustoDeColaborador() {
        custosSindicanciaPage.clicarBtnIncluirCusto();
        addLogToReport("Cliquei no botão para incluir novo custo.");
        custosSindicanciaPage.adicionarNovoCustoColaborador(custoColaborador);
        addScreenshotToReport("Preenchido dados do modal de custos.");
    }

    @E("o custo deve ser exibido na tabela de Colaborador na linha {int}")
    public void oCustoDeveSerExibidoNaTabelaDeColaboradorNaLinha(int linha) {
        custosSindicanciaPage.validaCustoColaborador(custoColaborador, linha);
        addScreenshotToReport("Validado dados do custo inserido.");
    }

    @E("ao editar o custo de Colaborador da linha {int}")
    public void aoEditarOCustoDeColaboradorDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnEditarCustoColaborador(linha);
        addLogToReport("Cliquei no botão para editar o custo.");
        custosSindicanciaPage.editarCustoColaborador(custoColaboradorEditado);
        addScreenshotToReport("Alterei dados do modal de custos.");
    }

    @E("o custo editado deve ser exibido na tabela de Colaborador na linha {int}")
    public void oCustoEditadoDeveSerExibidoNaTabelaDeColaboradorNaLinha(int linha) {
        custosSindicanciaPage.validaCustoColaborador(custoColaboradorEditado, linha);
        addScreenshotToReport("Validado dados do custo editado.");
    }

    @Quando("clicar para excluir o custo Colaborador da linha {int}")
    public void clicarParaExcluirOCustoColaboradorDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnExcluirColaborador(linha);
        addLogToReport("Cliquei no botão para editar o custo.");
    }

    @E("não deve exibir a tabela de custo Colaborador")
    public void nãoDeveExibirATabelaDeCustoColaborador() {
        custosSindicanciaPage.validaQueOCustoColaboradorFoiExcluido();
        addScreenshotToReport("Validado que o custo foi removido");

    }

    @Entao("valido o valor dos custos")
    public void validoOValorDosCustos() {
        custosSindicanciaPage.validoValorTotalCustos(custoOutros, custoMaterial, custoLocomotiva);
        addScreenshotToReport("Custo total apresentado");

    }

    @Entao("valido o valor dos custos de cada área")
    public void validoOValorDosCustosDeCadaÁrea() {
        custosSindicanciaPage.validoValorCustosDeCadaArea(custoOutros, custoMaterial, custoLocomotiva);
        addScreenshotToReport("Custo individual apresentado");
    }

    @E("ao adicionar um novo custo de Outros")
    public void aoAdicionarUmNovoCustoDeOutros() {
        custosSindicanciaPage.clicarBtnIncluirCusto();
        addLogToReport("Cliquei no botão para incluir novo custo.");
        custosSindicanciaPage.clicarOpcaoOutros();
        addLogToReport("Cliquei no botão para incluir novo custo do tipo Outros.");
        custosSindicanciaPage.adicionarNovoCustoOutros(custoOutros);
        addScreenshotToReport("Preenchido dados do modal de custos.");
    }

    @E("o custo deve ser exibido na tabela de Outros na linha {int}")
    public void oCustoDeveSerExibidoNaTabelaDeOutrosNaLinha(int linha) {
        custosSindicanciaPage.validaCustoOutrosEditadoDadosVia(custoOutros, linha);
        addScreenshotToReport("Validado dados do custo inserido.");
    }

    @E("ao editar o custo de Outros da linha {int}")
    public void aoEditarOCustoDeOutrosDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnEditarOutros(linha);
        addLogToReport("Cliquei no botão para editar novo custo.");
        custosSindicanciaPage.editarCustoOutros(custoOutrosEditado);
        addScreenshotToReport("Editei dados do modal de custos.");
    }

    @E("o custo editado deve ser exibido na tabela de Outros na linha {int}")
    public void oCustoEditadoDeveSerExibidoNaTabelaDeOutrosNaLinha(int linha) {
        custosSindicanciaPage.validaCustoOutrosEditadoDadosVia(custoOutrosEditado, linha);
        addScreenshotToReport("Validado dados do custo inserido.");
    }

    @Quando("clicar para excluir o custo Outros da linha {int}")
    public void clicarParaExcluirOCustoOutrosDaLinha(int linha) {
        custosSindicanciaPage.clicarBtnExcluirCustoOutrosDadosVia(linha);
        addLogToReport("Cliquei no botão de excluir outros.");
    }

    @E("não deve exibir a tabela de custo Outros")
    public void nãoDeveExibirATabelaDeCustoOutros() {
        custosSindicanciaPage.validaQueOCustoOutrosDadosViaFoiExcluido();
        addScreenshotToReport("Validado que o custo foi removido");
    }

    @Entao("valida que existe a tabela de custos do Veículo de Via")
    public void validaQueExisteATabelaDeCustosDoVeículoDeVia() {
        custosSindicanciaPage.validaQueATableaDeCustosDoVeiculoDeViaEstaNaTela();
        addScreenshotToReport("Validando tabela de custos do Veículo de Via");
    }

    @E("clica no botão para adicionar um novo custo do Veículo de Via")
    public void clicaNoBotãoParaAdicionarUmNovoCustoDoVeículoDeVia() {
        custosSindicanciaPage.clicarBtnAddCustoVeiculoDeVia();
        addLogToReport("Clicou no botão Adicionar ");
    }

    @E("preenche o modal de Custo do Veículo de via")
    public void preencheOModalDeCustoDoVeículoDeVia() {
        custosSindicanciaPage.preencheModalCustoVeiculoDeVia(custoVeiculosVia);
        addScreenshotToReport("Validado modal de custo do Veículo de Via preenchido");
    }

    @E("clicar no botão salvar do modal Custo do Veículo de via")
    public void clicarNoBotãoSalvarDoModalCustoDoVeículoDeVia() {
        custosSindicanciaPage.clicarBtnSalvarModalCustoDeVia();
        addLogToReport("Clicou no botão Salvar do modal de custo Veículo de Via");
    }

    @Entao("valida que o custo foi adicionado na tabela de custos do Veículo de Via")
    public void validaQueOCustoFoiAdicionadoNaTabelaDeCustosDoVeículoDeVia() {
        custosSindicanciaPage.validaCustoAdicionadoNaTabelaDeCustoVeiculoDeVia(custoVeiculosVia);
        addScreenshotToReport("Valida custo adicionado na tabela de custos do Veículo de Via");

    }

    @E("clico no botão Salvar Informações da tela de custos Material Rodante")
    public void clicoNoBotãoSalvarInformaçõesDaTelaDeCustosMaterialRodante() {
        custosSindicanciaPage.clicarBtnSalvarCustos();
        addLogToReport("Clicou no botão Salvar da tela de custos material rodante");
    }

    @Entao("clico no botão editar o custo do Veículo de Via")
    public void clicoNoBotãoEditarOCustoDoVeículoDeVia() {
        custosSindicanciaPage.clicarBtnEditarCustosVeiculoDeVia();
        addLogToReport("Clicou no botão Editar do custo Veículo de Via");
    }

    @E("edito os dados do modal de custo do Veículo de Via")
    public void editoOsDadosDoModalDeCustoDoVeículoDeVia() {
        custosSindicanciaPage.preencheModalCustoVeiculoDeVia(custoVeiculosViaEditado);
        addScreenshotToReport("Validado modal de custo do Veículo de Via preenchido");
    }

    @Entao("valida que o custo foi adicionado na tabela de custos do Veículo de Via Editado")
    public void validaQueOCustoFoiAdicionadoNaTabelaDeCustosDoVeículoDeViaEditado() {
        custosSindicanciaPage.validaCustoAdicionadoNaTabelaDeCustoVeiculoDeVia(custoVeiculosViaEditado);
        addScreenshotToReport("Valida custo editado na tabela de custos do Veículo de Via");
    }

    @Entao("seleciono o check box Sem custos de Veículos de Via")
    public void selecionoOCheckBoxSemCustos() {
        custosSindicanciaPage.selecionarCheckBoxSemCustosVeiculosVia();
        addScreenshotToReport("Preencheu o checkBox de 'Sem valor'");
    }

    @Entao("valida o custo zerado na tabela de custos do Veículo de Via")
    public void validaOCustoZeradoNaTabelaDeCustosDoVeículoDeVia() {
        custosSindicanciaPage.validaCustoZeradoNaTabelaDeCustosDoVeiculoDeVia();
        addScreenshotToReport("Validou que foi zerado o custo");
    }

    @Entao("valida que existe a tabela de custos de Locomotiva")
    public void validaQueExisteATabelaDeCustosDeLocomotiva() {
        custosSindicanciaPage.validaQueATableaDeCustosDaLocomotivaEstaNaTela();
        addScreenshotToReport("Validando tabela de custos da Locomotiva");
    }

    @E("clica no botão para adicionar um novo custo de Locomotiva")
    public void clicaNoBotãoParaAdicionarUmNovoCustoDeLocomotiva() {
        custosSindicanciaPage.clicarBtnAddCustoLocomotiva();
        addLogToReport("Clicou no botão Adicionar custo Locomotiva");
    }

    @E("preenche o modal de Custo de Locomotiva")
    public void preencheOModalDeCustoDeLocomotiva() {
        custosSindicanciaPage.preencheModalCustoLocomotiva(custoLocomotiva);
        addScreenshotToReport("Validado modal de custo do Veículo de Via preenchido");
    }

    @E("clicar no botão salvar do modal Custo de Locomotiva")
    public void clicarNoBotãoSalvarDoModalCustoDeLocomotiva() {
        custosSindicanciaPage.clicarBtnSalvarModalCustoLocomotiva();
        addLogToReport("Clicou no botão Salvar do modal de custo de Locomotiva");
    }

    @Entao("valida que o custo foi adicionado na tabela de custos de Locomotiva")
    public void validaQueOCustoFoiAdicionadoNaTabelaDeCustosDeLocomotiva() {
        custosSindicanciaPage.validaCustoAdicionadoNaTabelaDeCustoLocomotiva(custoLocomotiva);
        addScreenshotToReport("Valida custo editado na tabela de custos de Locomotiva");
    }

    @Entao("clico no botão editar o custo de Locomotiva")
    public void clicoNoBotãoEditarOCustoDeLocomotiva() {
        custosSindicanciaPage.clicarBtnEditarCustosLocomotiva();
        addLogToReport("Clicou no botão Editar do custo de Locomotiva");
    }

    @E("edito os dados do modal de custo de Locomotiva")
    public void editoOsDadosDoModalDeCustoDeLocomotiva() {
        custosSindicanciaPage.preencheModalCustoLocomotiva(custoLocomotivaEditado);
        addScreenshotToReport("Validado modal de custo do Locomotiva preenchido");
    }

    @Entao("seleciono o check box Sem custos de Locomotiva")
    public void selecionoOCheckBoxSemCustosDeLocomotiva() {
        custosSindicanciaPage.selecionarCheckBoxSemCustosLocomotiva();
        addScreenshotToReport("Preencheu o checkBox de 'Sem valor'");
    }

    @Entao("valida que o custo foi adicionado na tabela de custos de Locomotiva Editado")
    public void validaQueOCustoFoiAdicionadoNaTabelaDeCustosDeLocomotivaEditado() {
        custosSindicanciaPage.validaCustoAdicionadoNaTabelaDeCustoLocomotiva(custoLocomotivaEditado);
        addScreenshotToReport("Valida custo editado na tabela de custos do Locomotiva");
    }

    @Entao("valida o custo zerado na tabela de custos do Locomotiva")
    public void validaOCustoZeradoNaTabelaDeCustosDoLocomotiva() {
        custosSindicanciaPage.validaCustoZeradoNaTabelaDeCustosDaLocomotiva();
        addScreenshotToReport("Validou que foi zerado o custo");
    }

    @E("seleciono custos de atendimento")
    public void selecionoCustosDeAtendimento() {
        custosSindicanciaPage.selecionarCustosAtendimento();
        addLogToReport("selecionei custos de atendimento");
    }

    @E("clico no botão incluir custo")
    public void clicoNoBotãoIncluirCusto() {
        custosSindicanciaPage.clicarBtnIncluirCustoAtendimento();
        addLogToReport("cliquei no botão incluir custo");
    }

    @Entao("ao adicionar um novo custo de atendimento")
    public void aoAdicionarUmNovoCustoDeAtendimento() {
        custosSindicanciaPage.adicionarCustoAtendimento(custoVeiculosVia);
        addScreenshotToReport("adicionei um custo de atendimento");
    }

    @E("adiciono mais um custo de atendimento")
    public void adicionoMaisUmCustoDeAtendimento() {
        custosSindicanciaPage.clicarBtnIncluirCustoAtendimento();
        addLogToReport("cliquei no botão incluir custo");
        custosSindicanciaPage.adicionarCustoAtendimento(custoVeiculosVia);
        addScreenshotToReport("adicionei um custo de atendimento");
    }

    @Quando("removo um custo")
    public void removoUmCusto() {
        custosSindicanciaPage.removerCustoAtendimento();
        addLogToReport("cliquei em remover custo atendimento");
    }

    @E("valido que os valores do custo de atendimento persistiram")
    public void validoQueOsValoresDoCustoDeAtendimentoPersistiram() {
        custosSindicanciaPage.validoQueValoresPersistiram(custoVeiculosVia);
        addScreenshotToReport("validei que os valores persistiram");
    }

    @Entao("edito este novo custo")
    public void editoEsteNovoCusto() {
        custosSindicanciaPage.editarCustoAtendimento(custoVeiculosViaEditado);
        addScreenshotToReport("editei o custo de atendimento");
    }

    @E("valido que os valores do custo de atendimento editado persistiram")
    public void validoQueOsValoresDoCustoDeAtendimentoEditadoPersistiram() {
        custosSindicanciaPage.validoQueValoresEditadosPersistiram(custoVeiculosViaEditado);
        addScreenshotToReport("Valido que os valores editados persistiram");
    }

    @Entao("valido que o custo editado foi removido")
    public void validoQueOCustoEditadoFoiRemovido() {
        custosSindicanciaPage.validarCustoAtendimentoRemovido();
        addScreenshotToReport("validei que o custo editado foi removido");
    }

    @Entao("salvos o custo de atendimento")
    public void salvosOCustoDeAtendimento() {
        custosSindicanciaPage.salvarCustosAtendimento();
        addScreenshotToReport("Salvei o custo de atendimento");
    }

    @E("clico em adicionar um custo vagão")
    public void clicoEmAdicionarUmCustoVagão() {
        custosSindicanciaPage.clicarBtnAdicionarCustosVagao();
        addLogToReport("Cliquei sobre o botão para adicionar custo");
    }

    @Entao("preencho os valores para o custo vagão")
    public void preenchoOsValoresParaOCustoVagão() {
        custosSindicanciaPage.adicionarCustosVagao(custoVagoes);
        addScreenshotToReport("adicionei custos vagão");
    }

    @E("salvo o custo")
    public void salvoOCustoDeVagão() {
        custosSindicanciaPage.clicarBtnSalvarCusto();
        addLogToReport("cliquei sobre o botão salvar");
    }

    @E("clico em salvar o custo vagão")
    public void clicoEmSalvarOCustoVagão() {
        custosSindicanciaPage.clicarBtnSalvar();
        addLogToReport("Cliquei em salvar");
    }

    @E("valido os valores preenchidos para o custo vagão")
    public void validoOsValoresPreenchidosParaOCustoVagão() {
        custosSindicanciaPage.validarCustosVagao(veiculo, custoVagoes);
        addScreenshotToReport("validei os valores preenchidos");
    }

    @Dado("que clique em editar o custo vagão")
    public void queCliqueEmEditarOCustoVagão() {
        custosSindicanciaPage.clicarBtnEditarCustosVagao();
        addLogToReport("cliquei em editar custo vagão");
    }

    @E("editos os valores para o custo vagão")
    public void editosOsValoresParaOCustoVagão() {
        custosSindicanciaPage.adicionarCustosVagao(custoVagoesEditado);
        addScreenshotToReport("editei os valores para os custos vagão");
    }

    @E("valido os valores editados para o custo vagão")
    public void validoOsValoresEditadosParaOCustoVagão() {
        custosSindicanciaPage.validarCustosVagaoEditado(custoVagoesEditado);
        addScreenshotToReport("validei que os valores editados persistiram");
    }

    @E("clico no botão Ok do modal de Remover Custo")
    public void clicoNoBotãoOkDoModalDeRemoverCusto() {
        custosSindicanciaPage.clicarBtnOkModalRemoverCusto();
        addLogToReport("Cliquei no botão 'Ok', do modal de remover custo.");
    }

    @E("valido que o Custo Outros está no modo readonly")
    public void validoQueOCustoOutrosEstáNoModoReadonly() {
        custosSindicanciaPage.validaFormReadOnlyCustoOutros();
        addScreenshotToReport("Validei que está no modo readonly");
    }
}
