package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.AMV;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.AMVSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getAMV;

public class AMVSindicanciaSteps extends ReporterUtils {

    AMVSindicanciaPage amvSindicanciaPage = new AMVSindicanciaPage();
    AMV amv = getAMV();

    @E("estou na página de AMV")
    public void estouNaPáginaDeAMV() {
        amvSindicanciaPage.validaQueEstaNaPaginaDeAMV();
    }

    @Entao("preencho a página de AMV-Larga")
    public void preenchoAPáginaDeAMVLarga() {
        //principal
        amvSindicanciaPage.preencheDadosGeraisLargaPrincipal(amv);
        addScreenshotToReport("Preencheu Dados gerais - Larga");
        amvSindicanciaPage.preencheCotasDeSalvaguardaLargaPrincipal(amv);
        addScreenshotToReport("Preencheu Salva Guarda Larga Principal");
        amvSindicanciaPage.preencheFuncionamentoPrincipal(amv);
        addScreenshotToReport("Preencheu Funcionamento Principal");
        amvSindicanciaPage.preencheSegurancaPrincipal(amv);
        addScreenshotToReport("Preencheu Segurança Principal");
        amvSindicanciaPage.preencheBarraDeConjugacaoPrincipal(amv);
        addScreenshotToReport("Preencheu Barra de Conjugação Principal");
        amvSindicanciaPage.preencheAgulhasPrincipal(amv);
        addScreenshotToReport("Preencheu Agulhas Principal");
        //desviada
        amvSindicanciaPage.preencheCotasDeSalvaguardaLargaDesviada(amv);
        addScreenshotToReport("Preencheu Salva Guarda Desviada");
        amvSindicanciaPage.preencheFuncionamentoDesviada(amv);
        addScreenshotToReport("Preencheu Funcionamento Desviada");
        amvSindicanciaPage.preencheSegurancaDesviada(amv);
        addScreenshotToReport("Preencheu Segurança Desviada");
        amvSindicanciaPage.preencheTREncostoDesviada(amv);
        addScreenshotToReport("Preencheu TR Encosto");
        amvSindicanciaPage.preencheChanfroDesviada(amv);
        addScreenshotToReport("Preencheu Chanfro");
        amvSindicanciaPage.preencheAberturaAMVDesviada(amv);
        addScreenshotToReport("Preencheu Abertura AMV");
        amvSindicanciaPage.preencheTrilhoDesviada(amv);
        addScreenshotToReport("Preencheu Trilho");
    }

    @Entao("preencho a página de AMV-Métrica")
    public void preenchoAPáginaDeAMVMétrica() {
        //principal
        amvSindicanciaPage.preencheDadosGeraisMetricaPrincipal(amv);
        addScreenshotToReport("Preencheu Dados gerais - Métrica");
        amvSindicanciaPage.preencheCotasDeSalvaguardaMetricaPrincipal(amv);
        addScreenshotToReport("Preencheu Salva Guarda Printipal");
        amvSindicanciaPage.preencheFuncionamentoPrincipal(amv);
        addScreenshotToReport("Preencheu Funcionamento Principal");
        amvSindicanciaPage.preencheSegurancaPrincipal(amv);
        addScreenshotToReport("Preencheu Segurança Principal");
        amvSindicanciaPage.preencheBarraDeConjugacaoPrincipal(amv);
        addScreenshotToReport("Preencheu Barra de Conjugação Principal");
        amvSindicanciaPage.preencheAgulhasPrincipal(amv);
        addScreenshotToReport("Preencheu Agulhas Principal");
        //desviada
        amvSindicanciaPage.preencheCotasDeSalvaguardaMetricaDesviada(amv);
        addScreenshotToReport("Preencheu Salva Guarda Desviada");
        amvSindicanciaPage.preencheFuncionamentoDesviada(amv);
        addScreenshotToReport("Preencheu Funcionamento Desviada");
        amvSindicanciaPage.preencheSegurancaDesviada(amv);
        addScreenshotToReport("Preencheu Segurança Desviada");
        amvSindicanciaPage.preencheTREncostoDesviada(amv);
        addScreenshotToReport("Preencheu TR Encosto");
        amvSindicanciaPage.preencheChanfroDesviada(amv);
        addScreenshotToReport("Preencheu Chanfro");
        amvSindicanciaPage.preencheAberturaAMVDesviada(amv);
        addScreenshotToReport("Preencheu Abertura AMV");
        amvSindicanciaPage.preencheTrilhoDesviada(amv);
        addScreenshotToReport("Preencheu Trilho");
    }

    @Entao("preencho a página de AMV-Mista")
    public void preenchoAPáginaDeAMVMista() {
        //principal
        amvSindicanciaPage.preencheDadosGeraisMistaPrincipal(amv);
        addScreenshotToReport("Preencheu Dados gerais - Mista");
        amvSindicanciaPage.preencheCotasDeSalvaguardaMistaPrincipal(amv);
        addScreenshotToReport("Preencheu Salva Guarda Principal");
        amvSindicanciaPage.preencheFuncionamentoPrincipal(amv);
        addScreenshotToReport("Preencheu Funcionamento Principal");
        amvSindicanciaPage.preencheSegurancaPrincipal(amv);
        addScreenshotToReport("Preencheu Segurança Principal");
        amvSindicanciaPage.preencheBarraDeConjugacaoPrincipal(amv);
        addScreenshotToReport("Preencheu Barra de Conjugação Principal");
        amvSindicanciaPage.preencheAgulhasPrincipal(amv);
        addScreenshotToReport("Preencheu Agulhas Principal");
        //desviada
        amvSindicanciaPage.preencheCotasDeSalvaguardaMistaDesviada(amv);
        addScreenshotToReport("Preencheu Salva Guarda Desviada");
        amvSindicanciaPage.preencheFuncionamentoDesviada(amv);
        addScreenshotToReport("Preencheu Funcionamento Desviada");
        amvSindicanciaPage.preencheSegurancaDesviada(amv);
        addScreenshotToReport("Preencheu Segurança Desviada");
        amvSindicanciaPage.preencheTREncostoDesviada(amv);
        addScreenshotToReport("Preencheu TR Encosto");
        amvSindicanciaPage.preencheChanfroDesviada(amv);
        addScreenshotToReport("Preencheu Chanfro");
        amvSindicanciaPage.preencheAberturaAMVDesviada(amv);
        addScreenshotToReport("Preencheu Abertura AMV");
        amvSindicanciaPage.preencheTrilhoDesviada(amv);
        addScreenshotToReport("Preencheu Trilho");
    }

    @Quando("clico no botão Salvar Informações AMV")
    public void clicoNoBotãoSalvarInformaçõesAMV() {
        amvSindicanciaPage.clicarBtnSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar informações");
    }

    @E("valida o preenchimento da página de AMV-Larga")
    public void validaOPreenchimentoDaPáginaDeAMVLarga() {
        //principal
        amvSindicanciaPage.validaDadosGeraisLargaPrincipal(amv);
        addScreenshotToReport("Validou Dados gerais - Larga");
        amvSindicanciaPage.validaCotasDeSalvaguardaLargaPrincipal(amv);
        addScreenshotToReport("Validou Salva Guarda Principal");
        amvSindicanciaPage.validaFuncionamentoPrincipal(amv);
        addScreenshotToReport("Validou Funcionamento Principal");
        amvSindicanciaPage.validaSegurancaPrincipal(amv);
        addScreenshotToReport("Validou Segurança Principal");
        amvSindicanciaPage.validaBarraDeConjugacaoPrincipal(amv);
        addScreenshotToReport("Validou Barra de Conjugação Principal");
        amvSindicanciaPage.validaAgulhasPrincipal(amv);
        addScreenshotToReport("Validou Agulhas Principal");
        //desviada
        amvSindicanciaPage.validaCotasDeSalvaguardaLargaDesviada(amv);
        addScreenshotToReport("Validou Salva Guarda Desviada");
        amvSindicanciaPage.validaFuncionamentoDesviada(amv);
        addScreenshotToReport("Validou Funcionamento Desviada");
        amvSindicanciaPage.validaSegurancaDesviada(amv);
        addScreenshotToReport("Validou Segurança Desviada");
        amvSindicanciaPage.validaTREncontroDesviada(amv);
        addScreenshotToReport("Validou TR Encosto");
        amvSindicanciaPage.validaChanfroDesviada(amv);
        addScreenshotToReport("Validou Chanfro");
        amvSindicanciaPage.validaAberturaAMVDesviada(amv);
        addScreenshotToReport("Validou Abertura AMV");
        amvSindicanciaPage.validaTrilhoDesviada(amv);
        addScreenshotToReport("Validou Trilho");
    }

    @E("valida o preenchimento da página de AMV-Métrica")
    public void validaOPreenchimentoDaPáginaDeAMVMétrica() {
        //principal
        amvSindicanciaPage.validaDadosGeraisMetricaPrincipal(amv);
        addScreenshotToReport("Validou Dados gerais - Métrica");
        amvSindicanciaPage.validaCotasDeSalvaguardaMetricaPrincipal(amv);
        addScreenshotToReport("Validou Salva Guarda Principal");
        amvSindicanciaPage.validaFuncionamentoPrincipal(amv);
        addScreenshotToReport("Validou Funcionamento Principal");
        amvSindicanciaPage.validaSegurancaPrincipal(amv);
        addScreenshotToReport("Validou Segurança Principal");
        amvSindicanciaPage.validaBarraDeConjugacaoPrincipal(amv);
        addScreenshotToReport("Validou Barra de Conjugação Principal");
        amvSindicanciaPage.validaAgulhasPrincipal(amv);
        addScreenshotToReport("Validou Agulhas Principal");
        //desviada
        amvSindicanciaPage.validaCotasDeSalvaguardaMetricaDesviada(amv);
        addScreenshotToReport("Validou Salva Guarda Desviada");
        amvSindicanciaPage.validaFuncionamentoDesviada(amv);
        addScreenshotToReport("Validou Funcionamento Desviada");
        amvSindicanciaPage.validaSegurancaDesviada(amv);
        addScreenshotToReport("Validou Segurança Desviada");
        amvSindicanciaPage.validaTREncontroDesviada(amv);
        addScreenshotToReport("Validou TR Encosto");
        amvSindicanciaPage.validaChanfroDesviada(amv);
        addScreenshotToReport("Validou Chanfro");
        amvSindicanciaPage.validaAberturaAMVDesviada(amv);
        addScreenshotToReport("Validou Abertura AMV");
        amvSindicanciaPage.validaTrilhoDesviada(amv);
        addScreenshotToReport("Validou Trilho");
    }

    @E("valida o preenchimento da página de AMV-Mista")
    public void validaOPreenchimentoDaPáginaDeAMVMista() {
        //principal
        amvSindicanciaPage.validaDadosGeraisMistaPrincipal(amv);
        addScreenshotToReport("Validou Dados gerais - Mista");
        amvSindicanciaPage.validaCotasDeSalvaguardaMistaPrincipal(amv);
        addScreenshotToReport("Validou Salva Guarda Principal");
        amvSindicanciaPage.validaFuncionamentoPrincipal(amv);
        addScreenshotToReport("Validou Funcionamento Principal");
        amvSindicanciaPage.validaSegurancaPrincipal(amv);
        addScreenshotToReport("Validou Segurança Principal");
        amvSindicanciaPage.validaBarraDeConjugacaoPrincipal(amv);
        addScreenshotToReport("Validou Barra de Conjugação Principal");
        amvSindicanciaPage.validaAgulhasPrincipal(amv);
        addScreenshotToReport("Validou Barra de Agulhas Principal");
        //desviada
        amvSindicanciaPage.validaCotasDeSalvaguardaMistaDesviada(amv);
        addScreenshotToReport("Validou Salva Guarda Desviada");
        amvSindicanciaPage.validaFuncionamentoDesviada(amv);
        addScreenshotToReport("Validou Funcionamento Desviada");
        amvSindicanciaPage.validaSegurancaDesviada(amv);
        addScreenshotToReport("Validou Segurança Desviada");
        amvSindicanciaPage.validaTREncontroDesviada(amv);
        addScreenshotToReport("Validou TR Encosto");
        amvSindicanciaPage.validaChanfroDesviada(amv);
        addScreenshotToReport("Validou Chanfro");
        amvSindicanciaPage.validaAberturaAMVDesviada(amv);
        addScreenshotToReport("Validou Abertura AMV");
        amvSindicanciaPage.validaTrilhoDesviada(amv);
        addScreenshotToReport("Validou Trilho");
    }

    @Entao("preencho a página de AMV-Larga parcialmente para validar os campos obrigatórios")
    public void preenchoAPáginaDeAMVLargaParcialmenteParaValidarOsCamposObrigatórios() {
        //principal
        amvSindicanciaPage.preencheDadosGeraisLargaPrincipalParcial(amv);
        addScreenshotToReport("Preencheu Dados gerais Parcialemente - Larga");
        amvSindicanciaPage.preencheCotasDeSalvaguardaLargaPrincipal(amv);
        addScreenshotToReport("Preencheu Salva Guarda Principal");
        amvSindicanciaPage.preencheFuncionamentoPrincipal(amv);
        addScreenshotToReport("Preencheu Funcionamento Principal");
        amvSindicanciaPage.preencheSegurancaPrincipal(amv);
        addScreenshotToReport("Preencheu Segurança Principal");
        //desviada
        amvSindicanciaPage.preencheCotasDeSalvaguardaLargaDesviada(amv);
        addScreenshotToReport("Preencheu Salva Guarda Desviada");
        amvSindicanciaPage.preencheFuncionamentoDesviada(amv);
        addScreenshotToReport("Preencheu Funcionamento Desviada");
        amvSindicanciaPage.preencheSegurancaDesviada(amv);
        addScreenshotToReport("Preencheu Segurança Desviada");
    }

    @Entao("preencho a página de AMV-Métrica parcialmente para validar os campos obrigatórios")
    public void preenchoAPáginaDeAMVMétricaParcialmenteParaValidarOsCamposObrigatórios() {
        //principal
        amvSindicanciaPage.preencheDadosGeraisMetricaPrincipalParcial(amv);
        addScreenshotToReport("Preencheu Dados gerais Parcialemente - Métrica");
        amvSindicanciaPage.preencheCotasDeSalvaguardaMetricaPrincipal(amv);
        addScreenshotToReport("Preencheu Salva Guarda Principal");
        amvSindicanciaPage.preencheFuncionamentoPrincipal(amv);
        addScreenshotToReport("Preencheu Funcionamento Principal");
        amvSindicanciaPage.preencheSegurancaPrincipal(amv);
        addScreenshotToReport("Preencheu Segurança Principal");
        //desviada
        amvSindicanciaPage.preencheCotasDeSalvaguardaMetricaDesviada(amv);
        addScreenshotToReport("Preencheu Salva Guarda Desviada");
        amvSindicanciaPage.preencheFuncionamentoDesviada(amv);
        addScreenshotToReport("Preencheu Funcionamento Desviada");
        amvSindicanciaPage.preencheSegurancaDesviada(amv);
        addScreenshotToReport("Preencheu Segurança Desviada");
    }

    @Entao("preencho a página de AMV-Mista parcialmente para validar os campos obrigatórios")
    public void preenchoAPáginaDeAMVMistaParcialmenteParaValidarOsCamposObrigatórios() {
        //principal
        amvSindicanciaPage.preencheDadosGeraisMistaPrincipalParcial(amv);
        addScreenshotToReport("Preencheu Dados gerais Parcialemente - Mista");
        amvSindicanciaPage.preencheCotasDeSalvaguardaMistaPrincipal(amv);
        addScreenshotToReport("Preencheu Salva Guarda Principal");
        amvSindicanciaPage.preencheFuncionamentoPrincipal(amv);
        addScreenshotToReport("Preencheu Funcionamento Principal");
        amvSindicanciaPage.preencheSegurancaPrincipal(amv);
        addScreenshotToReport("Preencheu Segurança Principal");
        //desviada
        amvSindicanciaPage.preencheCotasDeSalvaguardaMistaDesviada(amv);
        addScreenshotToReport("Preencheu Salva Guarda Desviada");
        amvSindicanciaPage.preencheFuncionamentoDesviada(amv);
        addScreenshotToReport("Preencheu Funcionamento Desviada");
        amvSindicanciaPage.preencheSegurancaDesviada(amv);
        addScreenshotToReport("Preencheu Segurança Desviada");
    }
}
