package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.HistoricoSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Entao;

public class HistoricoAnaliseTecnicaSindicanciaSteps extends ReporterUtils {

    HistoricoSindicanciaPage historicoSindicanciaPage = new HistoricoSindicanciaPage();

    @Entao("deve exibir a página de histórico para a Análise Técnica")
    public void deveExibirAPáginaDeHistóricoParaAAnáliseTécnica() {
        historicoSindicanciaPage.validaExibicaoDaPagina();
        addScreenshotToReport("Validado exibição da página de histórico.");
    }
}
