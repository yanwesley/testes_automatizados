package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.AnaliseDeCausa;
import br.com.api.model.sindicancia.RelatorioFinal;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.AnaliseDeCausaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.*;

public class AnaliseDeCausaSteps extends ReporterUtils {
    AnaliseDeCausaPage analiseDeCausaPage = new AnaliseDeCausaPage();
    RelatorioFinal relatorioFinal = getRelatorioFinal();
    AnaliseDeCausa analiseDeCausa = getAnaliseDeCausa();
    AnaliseDeCausa analiseDeCausaEditada = getNewAnaliseDeCausa();

    @E("valido o preenchimento da análise de causa")
    public void validoOPreenchimentoDaAnáliseDeCausa() {
        analiseDeCausaPage.validoPreenchimentoAnaliseDeCausa(relatorioFinal);
        addScreenshotToReport("Validei que as informações são as mesmas preenchidas no relatório final");
    }

    @E("adiciono análise de causas")
    public void adicionoAnáliseDeCausas() {
        analiseDeCausaPage.clicarBtnAdicionarPrimaria();
        analiseDeCausaPage.preenchoAnaliseDeCausa(analiseDeCausa);
        addScreenshotToReport("Preenchi os 5 porquês");
    }

    @Quando("clico no botão salvar da análise de causa")
    public void clicoNoBotãoSalvarDaAnáliseDeCausa() {
        analiseDeCausaPage.cliqueiNoBotaoSalvar();
        addLogToReport("Cliquei sobre o botão salvar ");
    }

    @Entao("valido os porquês da análise de causa")
    public void validoOsPorquêsDaAnáliseDeCausa() {
        analiseDeCausaPage.validoOPreenchimentoDosPorques(analiseDeCausa);
        addScreenshotToReport("validei o preenchimento dos 5 porquês");
    }

    @Quando("que edito a análise de causa")
    public void editoAAnáliseDeCausa() {
        analiseDeCausaPage.cliqueiNoBotaoEditarPrimaria();
        analiseDeCausaPage.preenchoAnaliseDeCausa(analiseDeCausaEditada);
        addScreenshotToReport("Editei a análise de causa");
    }

    @Entao("valido que as informações editadas foram salvas na análise de causa")
    public void validoQueAsInformaçõesEditadasForamSalvasNaAnáliseDeCausa() {
        analiseDeCausaPage.validoOPreenchimentoDosPorques(analiseDeCausaEditada);
        addScreenshotToReport("Validei a edição dos porquês.");
    }

    @E("adiciono análise de causa dois")
    public void adicionoAnáliseDeCausaDois() {
        analiseDeCausaPage.clicarBtnAdicionarPrimaria();
        analiseDeCausaPage.preenchoAnaliseDeCausaDois(analiseDeCausa);
        addScreenshotToReport("Adicionei mais uma análise de causa");
    }

    @E("valido os porquês da análise de causa dois")
    public void validoOsPorquêsDaAnáliseDeCausaDois() {
        analiseDeCausaPage.validoOPreenchimentoDosPorquesDois(analiseDeCausa);
        addScreenshotToReport("validei o preenchimento dos 5 porquês");
    }

    @Dado("que excluo a análise de causa")
    public void queExcluoAAnáliseDeCausa() {
        analiseDeCausaPage.excluirAnaliseDeCausa();
        addScreenshotToReport("Exclui a análise de causa");
    }

    @E("valido que a análise de causa foi excluida")
    public void validoQueAAnáliseDeCausaFoiExcluida() {
        analiseDeCausaPage.validoAExclusaoAnaliseDeCausa();
    }
}
