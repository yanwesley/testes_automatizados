package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.api.model.sindicancia.RelatorioFinal;
import br.com.api.model.sindicancia.ReprovarSindicancia;
import br.com.pom.sro.central_de_acao.IncluirUmaAcaoPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.RelatorioFinalPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

import java.util.List;

import static br.com.api.GeradorDeMassa.*;

public class RelatorioFinalSteps extends ReporterUtils {

    ReprovarSindicancia reprovarSindicancia = new ReprovarSindicancia();
    RelatorioFinalPage relatorioFinalPage = new RelatorioFinalPage();
    RelatorioFinal relatorioFinal = getRelatorioFinal();
    String conclusao = getConclusao();
    String descricao = getDescricao();
    IncluirUmaAcaoPage incluirUmaAcaoPage = new IncluirUmaAcaoPage();
    ResponsavelAcao responsavelAcao = getResponsavelAcao();

    @E("valido que estou na página relatório final")
    public void validoQueEstouNaPáginaRelatórioFinal() {
        relatorioFinalPage.validoQueEstouNaPaginaRelatorio();
        addScreenshotToReport("Validei que estou na página relatório final");
    }

    @Entao("valido o preenchimento das conclusões de cada área")
    public void validoOPreenchimentoDasConclusõesDeCadaÁrea() {
        relatorioFinalPage.validoOPreenchimentoConclusaoArea(conclusao, descricao);
        addScreenshotToReport("Validei que cada área preencheu a conclusão");
    }

    @E("preencho o dono do acidente")
    public void preenchoODonoDoAcidente() {
        relatorioFinalPage.preenchoODonoDoAcidente(relatorioFinal);
        addScreenshotToReport("Preenchi o dono do acidente");
    }

    @Entao("preencho as definições de causa")
    public void preenchoAsDefiniçõesDeCausa() {
        relatorioFinalPage.preenchoAsDefinicoesDeCausa(relatorioFinal);
        addScreenshotToReport("preenchi as definições de causa");
    }

    @E("adiciono mais causas contributivas")
    public void adicionoMaisCausasContributivas() {
        relatorioFinalPage.adicionoMaisCausasContributivas(relatorioFinal);
        addScreenshotToReport("Adcionei mais causas contributivas");
    }

    @Entao("removo as causas contributivas")
    public void removoAsCausasContributivas() {
        relatorioFinalPage.removoAsCausasContributivas();
        addScreenshotToReport("Removi as causas contributivas");
    }

    @E("salvo as informações")
    public void salvoAsInformações() {
        relatorioFinalPage.salvoAsInformacoes();
        addScreenshotToReport("Salvei as informações");
    }

    @Entao("deve persistir as informações do Relatorio final")
    public void devePersistirAsInformaçõesDoRelatorioFinal() {
        relatorioFinalPage.validoQueAsInformacoesPersistiram(relatorioFinal);
        addScreenshotToReport("Valido que as informações persistiram");
    }

    @E("deve persistir as informações com mais causas contributivas")
    public void devePersistirAsInformaçõesComMaisCausasContributivas() {
        relatorioFinalPage.validarCausaContributiva(relatorioFinal);
        addScreenshotToReport("Valido que a causa contributiva foi salva");
    }

    @E("deve persistir as informações com a remoção das causas contributivas")
    public void devePersistirAsInformaçõesComARemoçãoDasCausasContributivas() {
        relatorioFinalPage.validarCausaContributivaRemovida();
        addScreenshotToReport("Valido que a causa contributiva foi removida");
    }

    @Dado("que clique no botão aprovar sindicância")
    public void queCliqueNoBotãoAprovarSindicância() {
        relatorioFinalPage.clicarbtnAprovar();
        addLogToReport("cliquei no botão aprovar sindicância");
    }

    @E("aprovo a investigação e o plano de ação")
    public void aprovoAInvestigaçãoEOPlanoDeAção() {
        relatorioFinalPage.aprovarInvestigacaoEPlanoAcao();
        addScreenshotToReport("Marco os dois checkboxs da investigação e plano de ação");
    }

    @E("escolho que a ação não será pública")
    public void escolhoQueAAçãoNãoSeráPública() {
        relatorioFinalPage.acaoNaoPublica();
        addScreenshotToReport("desmarco o check box da ação pública");
    }

    @E("não desdobro a ação")
    public void nãoDesdobroAAção() {
        addScreenshotToReport("não marco o checkbox para desmembrar a ação");
        relatorioFinalPage.clicarBtnProximoPasso();
    }

    @Entao("valido as informações de confirmação")
    public void validoAsInformaçõesDeConfirmação(List<String> statusList) {
        relatorioFinalPage.validoConfirmacaoAprovacaoSindicancia(statusList);
        addScreenshotToReport("valido as informações da aprovação");
        relatorioFinalPage.clicarbtnAprovarDoModal();
    }

    @E("confirmo a aprovação da sindicância")
    public void confirmoAAprovaçãoDaSindicância() {
        relatorioFinalPage.confirmoAprovacaoSindicancia();
        addScreenshotToReport("confirmo a aprovação da sindicância");
    }

    @E("escolho que a ação será pública")
    public void escolhoQueAAçãoSeráPública() {
        addScreenshotToReport("Validei que a ação está marcada como pública.");
        relatorioFinalPage.clicarBtnProximoPasso();
    }

    @E("desdobro a ação")
    public void desdobroAAção() {
        relatorioFinalPage.desdobroAAcao();
        addScreenshotToReport("Selecionei a ação para desdobrar.");
        relatorioFinalPage.clicarBtnAdicionarResponsavel();
        addLogToReport("Cliquei no botão adicionar responsável");
        incluirUmaAcaoPage.preencherDadosResponsavel(responsavelAcao);
        addScreenshotToReport("Preenchi os dados do responsável.");
        incluirUmaAcaoPage.clicarBtnConfirmarModalAdicionarResponsavel();
        addScreenshotToReport("desdobrei a ação e atribui um responsável");
    }

    @E("valido que o responsável foi atribuido")
    public void validoQueOResponsávelFoiAtribuido() {
        relatorioFinalPage.validoResponsavelAtribuido(responsavelAcao, 1);
        addScreenshotToReport("validei que o responsável foi atribuido");
        relatorioFinalPage.clicarBtnProximoPasso();
    }

    @Dado("que clique no botão reprovar sindicância")
    public void queCliqueNoBotãoReprovarSindicância() {
        relatorioFinalPage.clicarBtnReprovar();
        addLogToReport("cliquei no botão reprovar");
    }

    @E("seleciono reprovar investigação")
    public void selecionoReprovarInvestigação() {
        relatorioFinalPage.clicarRadioButtonReprovarInvestigacao();
        addScreenshotToReport("selecionei o radio button reprovar investigação");
    }

    @Entao("escolho uma ou mais áreas a reprovar")
    public void escolhoUmaOuMaisÁreasAReprovar() {
        relatorioFinalPage.escolhoUmaOuMaisAreasParaReprovar(reprovarSindicancia);
        addScreenshotToReport("escolho as áreas a reprovar");
    }

    @E("preencho a justificativa de reprovação completa")
    public void preenchoAJustificativaDeReprovaçãoCompleta() {
        relatorioFinalPage.preencherJustificativaReprovacao(reprovarSindicancia);
        addScreenshotToReport("preencho a justificativa da reprovação");
    }

    @E("insiro um arquivo para reprovação")
    public void insiroUmArquivoParaReprovação() {
        relatorioFinalPage.envioUmArquivo();
        addScreenshotToReport("envio uma imagem");
    }

    @Entao("Confirmo a reprovação")
    public void confirmoAReprovação() {
        relatorioFinalPage.confirmoReprovacao();
        relatorioFinalPage.confirmarReprovacaoModal();
        addLogToReport("cliquei em confirmar reprovação");
    }

    @E("seleciono reprovar plano de ação completo")
    public void selecionoReprovarPlanoDeAçãoCompleto() {
        relatorioFinalPage.clicarRadioButtonReprovarPlanoCompleto();
        addLogToReport("cliquei em reprovar plano de ação completo");
    }

    @E("preencho a justificativa de reprovação da investigação")
    public void preenchoAJustificativaDeReprovaçãoDaInvestigação() {
        relatorioFinalPage.preencherJustificativaReprovacaoInvestigacao(reprovarSindicancia);
        addScreenshotToReport("preencho a justificativa da reprovação");
    }

    @E("seleciono reprovar plano de ação parcialmente")
    public void selecionoReprovarPlanoDeAçãoParcialmente() {
        relatorioFinalPage.clicarRadioButtonReprovarPlanoParcial();
        addLogToReport("cliquei em reprovar plano de ação parcialmente");
    }

    @Entao("seleciono a ação a reprovar")
    public void selecionoAAçãoAReprovar() {
        relatorioFinalPage.selecionoAcaoParaReprovar();
        addScreenshotToReport("Selecionei a ação a reprovar");
    }

    @E("preencho a justificativa de reprovação parcial")
    public void preenchoAJustificativaDeReprovaçãoParcial() {
        relatorioFinalPage.preencherJustificativaReprovacaoParcial(reprovarSindicancia);
        addScreenshotToReport("preencho a justificativa da reprovação");
    }

    @Entao("valido as informações preenchidas do plano de ação")
    public void validoAsInformaçõesPreenchidasDoPlanoDeAção() {
        relatorioFinalPage.validarPlanoDeAcao(relatorioFinal);
        addScreenshotToReport("validei o preenchimento do plano de ação");
    }

    @Dado("que clique no botão anular sindicância")
    public void queCliqueNoBotãoAnularSindicância() {
        relatorioFinalPage.clicoBtnAnularSindicancia();
        addLogToReport("cliquei no botão anular do relatório final");
    }

    @E("preencho a justificativa da anulação")
    public void preenchoAJustificativaDaAnulação() {
        relatorioFinalPage.preencherJustificativaAnulação(reprovarSindicancia);
        addScreenshotToReport("preencho a justificativa da anulação");
    }

    @Entao("clico no botão confirmar anulação")
    public void clicoNoBotãoConfirmarAnulacao() {
        relatorioFinalPage.clicarBtnConfirmarAnulacao();
        addLogToReport("cliquei sobre o botão confirmar anulação");
    }

    @E("vou para aba finalizados")
    public void vouParaAbaFinalizados() {
        relatorioFinalPage.clicarNaAbaFinalizados();
        addScreenshotToReport("cliquei sobre a aba Finalizados");
    }

    @E("vou em detalhes da sindicância anulada")
    public void vouEmDetalhesDaSindicânciaAnulada() {
        relatorioFinalPage.clicarDetalhesSindicanciaAnulada();
        addLogToReport("cliquei na lupa para mais detalhes");
    }

    @E("valido que a sindicancia está anulada")
    public void validoQueASindicanciaEstáAnulada() {
        relatorioFinalPage.validoAnulacaoSindicancia(reprovarSindicancia);
        addScreenshotToReport("Validei que a sindicância foi anulada");
    }
}
