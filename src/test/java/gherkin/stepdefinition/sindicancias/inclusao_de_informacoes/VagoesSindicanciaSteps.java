package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.MedidaDeRodas;
import br.com.api.model.sindicancia.Vagao;
import br.com.api.model.sinistro.Sinistro;
import br.com.api.model.sinistro.Veiculo;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.vagoes.VagoesSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.*;

public class VagoesSindicanciaSteps extends ReporterUtils {

    VagoesSindicanciaPage vagoesSindicanciaPage = new VagoesSindicanciaPage();
    Vagao vagao = getVagao();
    Vagao vagaoEditado = getNewVagao();
    Veiculo veiculo = getVeiculo();
    Sinistro sinistro = getSinistro();
    MedidaDeRodas medidaDeRodas = getNewMedidaDeRodas();

    @E("estou na página de VAGÕES")
    public void estouNaPáginaDeVAGÕES() {
        vagoesSindicanciaPage.validaQueEstaNaPaginaDeVagoes();
        addScreenshotToReport("Valida que o usuário está na página de Vagões");
    }

    @E("o botão Salvar Informações dos Vagões deve estar disabled")
    public void oBotãoSalvarInformaçõesDosVagõesDeveEstarDisabled() {
        vagoesSindicanciaPage.validaBtnSalvarInformacoesDisabled();
        addScreenshotToReport("Validei que o botão de salvar informações esta desabilitado.");
    }

    @Então("preencho o campos da tela do vagão descarrilado")
    public void preenchoOCamposDaTelaDoVagãoDescarrilado() {
        vagoesSindicanciaPage.preencherParametrizacao(vagao);
        addScreenshotToReport("Preenchi a parametrização.");
        vagoesSindicanciaPage.escolherVagaoQueDescarrilhou(veiculo);
        addScreenshotToReport("Escolhi o primeiro veículo a descarrilar.");
        vagoesSindicanciaPage.selecionarVagaoDescarrilado();
        addScreenshotToReport("Escolhi o veículo para inserir as informações.");
        vagoesSindicanciaPage.preencherInformacoesSobreOSinistro(vagao);
        addScreenshotToReport("Preenchi as 'INFORMAÇÕES SOBRE O SINISTRO'");
        vagoesSindicanciaPage.preencherDDV(vagao);
        addScreenshotToReport("Preenchi os dados do 'DDV'");
        vagoesSindicanciaPage.preencherPrimeiroVeiculoADescarrilar(vagao);
        addScreenshotToReport("Preenchi os dados do 'PRIMEIRO VEÍCULO A DESCARRILAR'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilar(vagao);
        addScreenshotToReport("Preenchi os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO)'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilarAmparaBalanco(vagao);
        addScreenshotToReport("Preenchi os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - AMPARA-BALANÇOS'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilarFreio(vagao);
        addScreenshotToReport("Preenchi os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - FREIO'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilarEstruturaDoVagao(vagao);
        addScreenshotToReport("Preenchi os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - ESTRUTURA DO VAGÃO'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilarEngatesEACTS(vagao);
        addScreenshotToReport("Preenchi os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - ENGATES E ACTS'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilarInformacoesAdicionais(vagao);
        addScreenshotToReport("Preenchi os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - INFORMAÇÕES ADICIONAIS'");
    }

    @E("clico no botão Salvar da página de Vagões")
    public void clicoNoBotãoSalvarDaPáginaDeVagões() {
        vagoesSindicanciaPage.clicarBtnSalvar();
        addLogToReport("Cliquei no botão Salvar");
    }

    @E("valido que os dados do vagão descarrilado foram persistido corretamente")
    public void validoQueOsDadosDoVagãoDescarriladoForamPersistidoCorretamente() {
        vagoesSindicanciaPage.validarParametrizacao(vagao);
        addScreenshotToReport("Validei a parametrização.");
        vagoesSindicanciaPage.validarEscolhaVagaoQueDescarrilhou(veiculo);
        addScreenshotToReport("Validei o primeiro veículo a descarrilar.");
//        vagoesSindicanciaPage.validarSelecaoVagaoDescarrilado();
//        addScreenshotToReport("Validei o veículo para inserir as informações.");
        vagoesSindicanciaPage.validarInformacoesSobreOSinistro(sinistro, vagao);
        addScreenshotToReport("Validei as 'INFORMAÇÕES SOBRE O SINISTRO'");
        vagoesSindicanciaPage.validarDDV(vagao);
        addScreenshotToReport("Validei os dados do 'DDV'");
        vagoesSindicanciaPage.validarPrimeiroVeiculoADescarrilar(vagao);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO VEÍCULO A DESCARRILAR'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilar(vagao);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO)'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilarAmparaBalanco(vagao);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - AMPARA-BALANÇOS'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilarFreio(vagao);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - FREIO'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilarEstruturaDoVagao(vagao);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - ESTRUTURA DO VAGÃO'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilarEngatesEACTS(vagao);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - ENGATES E ACTS'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilarInformacoesAdicionais(vagao);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - INFORMAÇÕES ADICIONAIS'");
    }

    @Então("edito os campos da tela do vagão descarrilado")
    public void editoOsCamposDaTelaDoVagãoDescarrilado() {

        //Já deve estar selecionado
//        vagoesSindicanciaPage.preencherParametrizacao(vagaoEditado);
//        addScreenshotToReport("Preenchi a parametrização.");
//        vagoesSindicanciaPage.escolherVagaoQueDescarrilhou(veiculo);
//        addScreenshotToReport("Escolhi o primeiro veículo a descarrilar.");
//        vagoesSindicanciaPage.selecionarVagaoDescarrilado();
//        addScreenshotToReport("Escolhi o veículo para inserir as informações.");
        vagoesSindicanciaPage.preencherInformacoesSobreOSinistro(vagaoEditado);
        addScreenshotToReport("Editei as 'INFORMAÇÕES SOBRE O SINISTRO'");
        vagoesSindicanciaPage.preencherDDV(vagaoEditado);
        addScreenshotToReport("Editei os dados do 'DDV'");
        vagoesSindicanciaPage.preencherPrimeiroVeiculoADescarrilar(vagaoEditado);
        addScreenshotToReport("Editei os dados do 'PRIMEIRO VEÍCULO A DESCARRILAR'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilar(vagaoEditado);
        addScreenshotToReport("Editei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO)'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilarAmparaBalanco(vagaoEditado);
        addScreenshotToReport("Editei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - AMPARA-BALANÇOS'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilarFreio(vagaoEditado);
        addScreenshotToReport("Editei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - FREIO'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilarEstruturaDoVagao(vagaoEditado);
        addScreenshotToReport("Editei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - ESTRUTURA DO VAGÃO'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilarEngatesEACTS(vagaoEditado);
        addScreenshotToReport("Editei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - ENGATES E ACTS'");
        vagoesSindicanciaPage.preencherPrimeiroTruqueADescarrilarInformacoesAdicionais(vagaoEditado);
        addScreenshotToReport("Editei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - INFORMAÇÕES ADICIONAIS'");
    }

    @E("valido que os dados editado do vagão descarrilado foram persistido corretamente")
    public void validoQueOsDadosEditadoDoVagãoDescarriladoForamPersistidoCorretamente() {
        vagoesSindicanciaPage.validarParametrizacao(vagao);
        addScreenshotToReport("Validei a parametrização.");
        vagoesSindicanciaPage.validarEscolhaVagaoQueDescarrilhou(veiculo);
        addScreenshotToReport("Validei o primeiro veículo a descarrilar.");
//        vagoesSindicanciaPage.validarSelecaoVagaoDescarrilado();
//        addScreenshotToReport("Validei o veículo para inserir as informações.");
        vagoesSindicanciaPage.validarInformacoesSobreOSinistro(sinistro, vagaoEditado);
        addScreenshotToReport("Validei as 'INFORMAÇÕES SOBRE O SINISTRO'");
        vagoesSindicanciaPage.validarDDV(vagaoEditado);
        addScreenshotToReport("Validei os dados do 'DDV'");
        vagoesSindicanciaPage.validarPrimeiroVeiculoADescarrilar(vagaoEditado);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO VEÍCULO A DESCARRILAR'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilar(vagaoEditado);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO)'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilarAmparaBalanco(vagaoEditado);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - AMPARA-BALANÇOS'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilarFreio(vagaoEditado);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - FREIO'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilarEstruturaDoVagao(vagaoEditado);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - ESTRUTURA DO VAGÃO'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilarEngatesEACTS(vagaoEditado);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - ENGATES E ACTS'");
        vagoesSindicanciaPage.validarPrimeiroTruqueADescarrilarInformacoesAdicionais(vagaoEditado);
        addScreenshotToReport("Validei os dados do 'PRIMEIRO TRUQUE A DESCARRILAR (INSPEÇÃO) - INFORMAÇÕES ADICIONAIS'");
    }

    @E("preencha as medidas das rodas")
    public void preenchaAsMedidasDasRodas() {
        vagoesSindicanciaPage.preencherMedidasDasRodas(medidaDeRodas);
        addScreenshotToReport("Preenchi as medidas das rodas");
    }

    @Entao("valido que o valor preenchido das medidas das rodas persistiram")
    public void validoQueOValorPreenchidoDasMedidasDasRodasPersistiram() {
        vagoesSindicanciaPage.validarMedidaDeRodas(medidaDeRodas);
        addScreenshotToReport("Validei que as medidas das rodas foram preenchidas");
    }

    @Dado("que eu clique em preencher em tela cheia")
    public void queEuCliqueEmPreencherEmTelaCheia() {
        vagoesSindicanciaPage.clicarBtnPreencherTelaCheia();
        addLogToReport("Cliquei no botão Preencher tela cheia");
    }

    @E("salvo as medidas")
    public void salvoAsMedidas() {
        vagoesSindicanciaPage.salvarMedidasRodas();
        addLogToReport("cliquei sobre o botão salvar");
    }

    @Então("valido que Vagoes está no modo readonly")
    public void validoQueVagoesEstáNoModoReadonly() {
        vagoesSindicanciaPage.validaFormReadOnly();
        addScreenshotToReport("Validei que está no modo readonly");
    }
}
