package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.RegistroDoAcidente;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.RegistroDoAcidenteSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

import static br.com.api.GeradorDeMassa.getNewRegistroDoAcidente;
import static br.com.api.GeradorDeMassa.getRegistroDoAcidente;

public class RegistroDoAcidenteSegurancaPatrimonialSindicanciaSteps extends ReporterUtils {
    RegistroDoAcidente registroDoAcidente = getRegistroDoAcidente();
    RegistroDoAcidente registroDoAcidenteEditado = getNewRegistroDoAcidente();
    RegistroDoAcidenteSindicanciaPage registroDoAcidenteSindicanciaPage = new RegistroDoAcidenteSindicanciaPage();

    @Entao("deve exibir a página de Registro do Acidente")
    public void deveExibirAPáginaDeRegistroDoAcidente() {
        registroDoAcidenteSindicanciaPage.validaExibicaoDaPagina();
        addScreenshotToReport("Validou exibição da página de Registro Do Acidente.");
    }

    @E("ao adicionar todas as imagens e comentários do Registro do Acidente")
    public void aoAdicionarTodasAsImagensEComentáriosDoRegistroDoAcidente() {
        registroDoAcidenteSindicanciaPage.adicionarArquivosEComentarios(registroDoAcidente);
    }

    @E("clico no botão Salvar Informações do Registro do Acidente")
    public void clicoNoBotãoSalvarInformaçõesDoRegistroDoAcidente() {
        registroDoAcidenteSindicanciaPage.clicarBtnSalvarInformacoes();
        addLogToReport("Cliquei no botão Salvar Informações.");
    }

    @E("valido se os dados do registro fotográfico foram persistido")
    public void validoSeOsDadosDoRegistroFotográficoForamPersistido() {
        registroDoAcidenteSindicanciaPage.validarArquivosAdicionados(registroDoAcidente);
    }

    @E("valido se os comentários editado do registro fotográfico")
    public void validoSeOsComentáriosEditadoDoRegistroFotográfico() {
        registroDoAcidenteSindicanciaPage.validarArquivosAdicionados(registroDoAcidenteEditado);
        addScreenshotToReport("Validei que os arquivos foram adicionados");
    }

    @Dado("que eu edite os comentários das imagens do registro de acidentes")
    public void queEuEditeOsComentáriosDasImagensDoRegistroDeAcidentes() {
        registroDoAcidenteSindicanciaPage.editarComentarios(registroDoAcidenteEditado);
    }

    @Dado("que eu exclua os comentários e as imagens do registro de acidentes")
    public void queEuExcluaOsComentáriosEAsImagensDoRegistroDeAcidentes() {
        registroDoAcidenteSindicanciaPage.excluirImagensEComentarios();
    }

    @Dado("que eu insira uma nova imagem no registro de acidentes")
    public void queEuInsiraUmaNovaImagemNoRegistroDeAcidentes() {
        registroDoAcidenteSindicanciaPage.clicarBtnAdicionarNovaImagem();
        addLogToReport("Cliquei no botão 'Adicionar nova imagem'.");
        registroDoAcidenteSindicanciaPage.preencherCamposModalNovaImagem(registroDoAcidente);
        addScreenshotToReport("Preenchi o título e a imagem.");
        registroDoAcidenteSindicanciaPage.clicarBtnSalvarModal();
        addLogToReport("Cliquei no botão salvar do modal");
    }

    @E("adiciono um comentário para a nova imagem")
    public void adicionoUmComentárioParaANovaImagem() {
        registroDoAcidenteSindicanciaPage.preencherComentarioOutros(registroDoAcidente);
    }

    @E("valido se o comentário e a imagem está sendo exibida do registro fotográfico")
    public void validoSeOComentárioEAImagemEstáSendoExibidaDoRegistroFotográfico() {
        registroDoAcidenteSindicanciaPage.validoPreenchimentoDoOutros(registroDoAcidente);
        addScreenshotToReport("Validado preenchimento de outra imagem.");
    }

    @E("valido que as informações registro do acidente foram exibidas no modo readonly")
    public void validoQueAsInformaçõesRegistroDoAcidenteForamExibidasNoModoReadonly() {
        registroDoAcidenteSindicanciaPage.validarArquivosAdicionados(registroDoAcidente);
        addScreenshotToReport("Validei que os arquivos foram adicionados");
    }

    @E("valido que estou na página registro do acidente")
    public void validoQueEstouNaPáginaRegistroDoAcidente() {
        registroDoAcidenteSindicanciaPage.validoQueEstouNoRegistroAcidente();
        addScreenshotToReport("Validei que estou no registro do acidente");
    }

    @E("ao adicionar todas as imagens e comentários do Registro do Acidente via SRO")
    public void aoAdicionarTodasAsImagensEComentáriosDoRegistroDoAcidenteViaSRO() {
        registroDoAcidenteSindicanciaPage.waitTime(2000);
        registroDoAcidenteSindicanciaPage.adicionarArquivosEComentarios(registroDoAcidente);
    }
}
