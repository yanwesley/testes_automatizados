package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.questionario.QuestionarioCheckListAcidente;
import br.com.api.model.sindicancia.questionario.QuestionarioMaquinista;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.questionario_maquinista.InformarcoesQuestionariosSindicanciaPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.questionario_maquinista.QuestionarioCheckListAcidenteSindicanciaPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.questionario_maquinista.QuestionarioMaquinistaSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.*;

public class QuestionarioMaquinistaSindicanciaSteps extends ReporterUtils {

    InformarcoesQuestionariosSindicanciaPage informarcoesQuestionariosSindicanciaPage = new InformarcoesQuestionariosSindicanciaPage();
    QuestionarioMaquinistaSindicanciaPage questionarioMaquinistaSindicanciaPage = new QuestionarioMaquinistaSindicanciaPage();
    QuestionarioCheckListAcidenteSindicanciaPage questionarioCheckListAcidenteSindicanciaPage = new QuestionarioCheckListAcidenteSindicanciaPage();
    QuestionarioMaquinista questionarioMaquinista = getQuestionarioMaquinista();
    QuestionarioMaquinista questionarioMaquinistaEditado = getNewQuestionarioMaquinista();
    QuestionarioCheckListAcidente questionarioCheckListAcidente = getQuestionarioCheckListAcidente();
    QuestionarioCheckListAcidente questionarioCheckListAcidenteEditado = getNewQuestionarioCheckListAcidente();

    @Entao("deve exibir a página de Informações do Maquinista")
    public void deveExibirAPáginaDeInformaçõesDoMaquinista() {
        informarcoesQuestionariosSindicanciaPage.validaExibicaoDaPaginaQuestionarioMaquinista();
        addScreenshotToReport("Validado exibição da página de Questionário.");
    }

    @E("ao preencher os dados de Informações do Maquinista")
    public void aoPreencherOsDadosDeInformaçõesDoMaquinista() {
        informarcoesQuestionariosSindicanciaPage.preencherDados(questionarioMaquinista);
        addScreenshotToReport("Preenchi os dados de Informações do Maquinista.");
    }

    @E("ao preencher os dados do QUESTIONÁRIO MAQUINISTA")
    public void aoPreencherOsDadosDoQUESTIONÁRIOMAQUINISTA() {
        questionarioMaquinistaSindicanciaPage.preencherDados(questionarioMaquinista);
        addScreenshotToReport("Preenchi os dados de Questionário do Maquinista.");
    }

    @E("ao preencher os dados do QUESTIONÁRIO CHECK-LIST ACIDENTE")
    public void aoPreencherOsDadosDoQUESTIONÁRIOCHECKLISTACIDENTE() {
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioCheckListAcidente();
        questionarioCheckListAcidenteSindicanciaPage.preencherDados(questionarioCheckListAcidente);
        addScreenshotToReport("Preenchi os dados do Check List do acidente.");
    }

    @E("clico no botão Salvar Rascunho do Questionário do Maquinista")
    public void clicoNoBotãoSalvarRascunhoDoQuestionárioDoMaquinista() {
        informarcoesQuestionariosSindicanciaPage.clicarBtnSalvarRascunho();
        addLogToReport("Cliquei no botão 'Salvar Rascunho'.");
    }

    @E("valido que os dados do Questionario do Maquinista estão conforme o preenchido")
    public void validoQueOsDadosDoQuestionarioDoMaquinistaEstãoConformeOPreenchido() {
        informarcoesQuestionariosSindicanciaPage.validarDados(questionarioMaquinista);
        addScreenshotToReport("Validei os dados de Informações do Maquinista.");
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioMaquinista();
        questionarioMaquinistaSindicanciaPage.validarDados(questionarioMaquinista);
        addScreenshotToReport("Validei os dados de Questionário do Maquinista.");
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioCheckListAcidente();
        questionarioCheckListAcidenteSindicanciaPage.validarDados(questionarioCheckListAcidente);
        addScreenshotToReport("Validei os dados do Check List do acidente.");
    }

    @Quando("eu editar os dados de Informações do Maquinista")
    public void aoEditarOsDadosDeInformaçõesDoMaquinista() {
        informarcoesQuestionariosSindicanciaPage.preencherDados(questionarioMaquinistaEditado);
        addScreenshotToReport("Editei os dados de Informações do Maquinista.");
    }

    @E("ao editar os dados do QUESTIONÁRIO MAQUINISTA")
    public void aoEditarOsDadosDoQUESTIONÁRIOMAQUINISTA() {
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioMaquinista();
        questionarioMaquinistaSindicanciaPage.preencherDados(questionarioMaquinistaEditado);
        addScreenshotToReport("Editei os dados de Questionário do Maquinista.");
    }

    @E("ao editar os dados do QUESTIONÁRIO CHECK-LIST ACIDENTE")
    public void aoEditarOsDadosDoQUESTIONÁRIOCHECKLISTACIDENTE() {
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioCheckListAcidente();
        questionarioCheckListAcidenteSindicanciaPage.preencherDados(questionarioCheckListAcidenteEditado);
        addScreenshotToReport("Editei os dados do Check List do acidente.");
    }

    @E("clico no botão Salvar do Questionário do Maquinista")
    public void clicoNoBotãoSalvarDoQuestionárioDoMaquinista() {
        informarcoesQuestionariosSindicanciaPage.clicarBtnSalvar();
        addLogToReport("Cliquei no Botão Salvar");
    }

    @E("valido que os dados editar do Questionario do Maquinista estão conforme o preenchido")
    public void validoQueOsDadosEditarDoQuestionarioDoMaquinistaEstãoConformeOPreenchido() {
        informarcoesQuestionariosSindicanciaPage.validarDados(questionarioMaquinistaEditado);
        addScreenshotToReport("Validei os dados de Informações do Maquinista.");
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioMaquinista();
        questionarioMaquinistaSindicanciaPage.validarDados(questionarioMaquinistaEditado);
        addScreenshotToReport("Validei os dados de Questionário do Maquinista.");
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioCheckListAcidente();
        questionarioCheckListAcidenteSindicanciaPage.validarDados(questionarioCheckListAcidenteEditado);
        addScreenshotToReport("Validei os dados do Check List do acidente.");
    }

    @E("valido que os dados do Questionario do Maquinista estão conforme o preenchido no modo readonly")
    public void validoQueOsDadosDoQuestionarioDoMaquinistaEstãoConformeOPreenchidoNoModoReadonly() {
        informarcoesQuestionariosSindicanciaPage.validarDados(questionarioMaquinista);
        addScreenshotToReport("Validei os dados de Informações do Maquinista.");
        questionarioMaquinistaSindicanciaPage.validarDados(questionarioMaquinista);
        addScreenshotToReport("Validei os dados de Questionário do Maquinista.");
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioCheckListAcidente();
        questionarioCheckListAcidenteSindicanciaPage.validarDados(questionarioCheckListAcidente);
        addScreenshotToReport("Validei os dados do Check List do acidente.");
    }
}
