package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.Flambagem;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.FlambagemSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

import static br.com.api.GeradorDeMassa.getFlambagem;

public class FlambagemSindicanciaSteps extends ReporterUtils {

    FlambagemSindicanciaPage flambagemSindicanciaPage = new FlambagemSindicanciaPage();
    Flambagem flambagem = getFlambagem();

    @Entao("deve exibir a página de preenchimento da Flambagem")
    public void deveExibirAPáginaDePreenchimentoDaFlambagem() {
        flambagemSindicanciaPage.validaExibicaoDaPageDeFlambagem();
    }

    @E("valida a regra da seção LOCAL DA FLAMBAGEM")
    public void validaARegraDaSeçãoLOCALDAFLAMBAGEM() {
        flambagemSindicanciaPage.validaRegraSecaoLocalDaFlambagem();
    }

    @E("valida a regra da seção HISTÓRICO DA FLAMBAGEM")
    public void validaARegraDaSeçãoHISTÓRICODAFLAMBAGEM() {
        flambagemSindicanciaPage.validaRegraSecaoHistoricoDaFlambagem();
    }

    @E("valida a regra da seção HISTÓRICO DO ACIDENTE")
    public void validaARegraDaSeçãoHISTÓRICODOACIDENTE() {
        flambagemSindicanciaPage.validaRegraSecaoHistoricoDoAcidente();
    }

    @E("clico no botão Salvar Informações Flambagem")
    public void clicoNoBotãoSalvarInformaçõesFlambagem() {
        flambagemSindicanciaPage.clicarBtnSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar informações");
    }

    @E("clico no botão Salvar e Avançar Flambagem")
    public void clicoNoBotãoSalvarEAvançarFlambagem() {
        flambagemSindicanciaPage.clicarBtnSalvarEAvancar();
        addLogToReport("Clicou no botão Salvar e Avançar");
    }

    @Dado("que preencho a Aba Flambagem")
    public void quePreenchoAAbaFlambagem() {
        flambagemSindicanciaPage.preencheSecaoLocalDaFlambagem(flambagem);
        addScreenshotToReport("Preenche seção Local da Flambagem");
        flambagemSindicanciaPage.preencheSecaoCaracteristicasDoTrecho(flambagem);
        addScreenshotToReport("Preenche seção Característica do trecho");
        flambagemSindicanciaPage.preencheSecaoHistoricoDaFlambagem(flambagem);
        addScreenshotToReport("Preenche seção Histórico da Flambagem");
        flambagemSindicanciaPage.preencheSecaoHistoricoDoAcidente(flambagem);
        addScreenshotToReport("Preenche seção Histórico do acidente");
        flambagemSindicanciaPage.preencheSecaoSolucaoDefinitiva(flambagem);
        addScreenshotToReport("Preenche seção Solução definitiva");

    }

    @Entao("valido os dados preenchido na tela de Flambagem")
    public void validoOsDadosPreenchidoNaTelaDeFlambagem() {
        flambagemSindicanciaPage.validoSecaoLocalDaFlambagem(flambagem);
        addScreenshotToReport("Valida seção Local da Flambagem");
        flambagemSindicanciaPage.validoSecaoCaracteristicasDoTrecho(flambagem);
        addScreenshotToReport("Valida seção Característica do trecho");
        flambagemSindicanciaPage.validoSecaoHistoricoDaFlambagem(flambagem);
        addScreenshotToReport("Valida seção Histórico da Flambagem");
        flambagemSindicanciaPage.validoSecaoHistoricoDoAcidente(flambagem);
        addScreenshotToReport("Valida seção Histórico do acidente");
        flambagemSindicanciaPage.validoSecaoSolucaoDefinitiva(flambagem);
        addScreenshotToReport("Valida seção Solução definitiva");
    }

    @E("deve exibir a página de AMV")
    public void deveExibirAPáginaDeAMV() {
        flambagemSindicanciaPage.validaExibicaoDaPageAMV();

    }
}
