package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.RegistroFotograficoSindicanciaCipiaViaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;

public class RegistroFotograficoSindicanciaCipiaViaSteps extends ReporterUtils {

    RegistroFotograficoSindicanciaCipiaViaPage registroFotograficoSindicanciaCipiaViaPage = new RegistroFotograficoSindicanciaCipiaViaPage();

    @E("estou na página de REGISTRO FOTOGRÁFICO")
    public void estouNaPáginaDeREGISTROFOTOGRÁFICO() {
        registroFotograficoSindicanciaCipiaViaPage.validaQueEstaNaPaginaDeRegistroFotografico();
    }

    @Entao("valida os itens da tela de Registro fotográfico")
    public void validaOsItensDaTelaDeRegistroFotográfico() {
        registroFotograficoSindicanciaCipiaViaPage.validaTelaRegistroFotografico();
    }

    @Então("insiro as imagens na tela de registro fotográfico")
    public void insiroAsImagensNaTelaDeRegistroFotográfico() {
        registroFotograficoSindicanciaCipiaViaPage.insereImgVisaoGeralDoAcidente();
        addLogToReport("Imagem de Visão geral do Acidente adicionada");
        registroFotograficoSindicanciaCipiaViaPage.insereImgCroquiDoLocal();
        addLogToReport("Imagem Croqui do Local adicionada");
        registroFotograficoSindicanciaCipiaViaPage.insereNovaImagemParteUm();
        addLogToReport("Nova Imagem Parte 1 adicionada");
        registroFotograficoSindicanciaCipiaViaPage.insereImgVisaoGeralDoPod();
        addLogToReport("Imagem de Visão geral do Pod adicionada");
        registroFotograficoSindicanciaCipiaViaPage.insereImgCloseDoPod();
        addLogToReport("Imagem Close do Pod adicionada");
        registroFotograficoSindicanciaCipiaViaPage.insereImgDetalheMarcaUm();
        addLogToReport("Imagem Detalhe Marca I adicionada");
        registroFotograficoSindicanciaCipiaViaPage.insereImgDetalheMarcaDois();
        addLogToReport("Imagem Detalhe Marca II adicionada");
        registroFotograficoSindicanciaCipiaViaPage.insereNovaImagemParteDois();
        addLogToReport("Nova Imagem Parte 2 adicionada");
    }

    @E("valido que as imagens foram inseridas na tela de registro fotográfico")
    public void validoQueAsImagensForamInseridasNaTelaDeRegistroFotográfico() {
        registroFotograficoSindicanciaCipiaViaPage.validaImagensInseridasNaTelaRegistroFotografico();
    }

    @Dado("excluo as imagens na tela de registro fotográfico")
    public void excluoAsImagensNaTelaDeRegistroFotográfico() {
        registroFotograficoSindicanciaCipiaViaPage.excluiImagensTelaRegistroFotografico();
        addLogToReport("Todas as imagens foram excluídas");
    }

    @E("clica no botão Avançar")
    public void clicaNoBotãoAvançar() {
        registroFotograficoSindicanciaCipiaViaPage.clicarBtnAvancar();
        addLogToReport("Clicou no botão Avançar");
    }
}
