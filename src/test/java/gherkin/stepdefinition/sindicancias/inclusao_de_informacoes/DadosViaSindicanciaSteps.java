package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.DadosVia;
import br.com.api.model.sindicancia.TabelaAnomalias;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.DadosViaPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.TabelaAnomaliasPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getDadosVia;
import static br.com.api.GeradorDeMassa.getTabelaAnomalias;


public class DadosViaSindicanciaSteps extends ReporterUtils {

    DadosViaPage dadosViaPage = new DadosViaPage();
    TabelaAnomaliasPage tabelaAnomaliasPage = new TabelaAnomaliasPage();
    DadosVia dadosVia = getDadosVia();
    TabelaAnomalias tabelaAnomalias = getTabelaAnomalias();

    @Entao("deve exibir a página de inclusão de informações do acidente")
    public void deveExibirAPáginaDeInclusãoDeInformaçõesDoAcidente() {
        dadosViaPage.validaExibisPaginaInclusaoDeInformacoes();
        addScreenshotToReport("Valida que a página para inclusão de informações carregou com sucesso.");
    }

    @E("preenche a aba Dados Via da sindicância")
    public void preencheAAbaDadosViaDaSindicância() {
        dadosViaPage.preencheSecaoDadosVia(dadosVia);
        addScreenshotToReport("Preencheu a seção Dados Via");
        dadosViaPage.preencheSecaoPlanimetria(dadosVia);
        addScreenshotToReport("Preencheu a seção Tangente");
        dadosViaPage.preencheSecaoPontoDeDescarrilamento(dadosVia);
        addScreenshotToReport("Preencheu a seção Descarrilamento");
        dadosViaPage.preencheSecaoOutrasMedidas(dadosVia);
        addScreenshotToReport("Preencheu a seção Outras Medidas");
        dadosViaPage.preencheSecaoDormentes(dadosVia);
        addScreenshotToReport("Preencheu a seção Dormentes");
        dadosViaPage.preencheSecaoInformacoesDoDDC(dadosVia);
        addScreenshotToReport("Preencheu a seção Informações do DDC");
        dadosViaPage.preencheSecaoInformacoesDoDQB(dadosVia);
        addScreenshotToReport("Preencheu a seção Informações do DQB");
    }

    @Quando("clico no botão Salvar Informações Dados Via")
    public void clicoNoBotãoSalvarInformaçõesDadosVia() {
        dadosViaPage.clicarBtnSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar informações");
    }

    @Entao("valida que os valores preenchidos na opção Dados Via persistiram")
    public void validaQueOsValoresPreenchidosNaOpçãoDadosVia() {
        dadosViaPage.validaSecaoDadosVia(dadosVia);
        addScreenshotToReport("Validou a seção Dados Via");
        dadosViaPage.validaSecaoPlanimetria(dadosVia);
        addScreenshotToReport("Validou a seção Tangente");
        dadosViaPage.validaSecaoPontoDeDescarrilamento(dadosVia);
        addScreenshotToReport("Validou a seção Descarrilamento");
        dadosViaPage.validaSecaoOutrasMedidas(dadosVia);
        addScreenshotToReport("Validou a seção Outras Medidas");
        dadosViaPage.validaSecaoDormentes(dadosVia);
        addScreenshotToReport("Validou a seção Dormentes");
        dadosViaPage.validaSecaoInformacoesDoDDC(dadosVia);
        addScreenshotToReport("Validou a seção Informações do DDC");
        dadosViaPage.validaSecaoInformacoesDoDQB(dadosVia);
        addScreenshotToReport("Validou a seção Informações do DQB");
    }

    @E("clico no link para preencher a JUSTIFICATIVA")
    public void clicoNoLinkParaPreencherJUSTIFICATIVA() {
        dadosViaPage.clicarLinkAbrirJustificativa();
        addLogToReport("Cliquei no link para abrir o modal.");
    }

    @E("preencho os dados do modal de JUSTIFICATIVA")
    public void preenchoOsDadosDoModaDeJustificativa() {
        dadosViaPage.preencherModalImpossivelMedir(dadosVia);
        addScreenshotToReport("Preenche os dados do modal de 'JUSTIFICATIVA DE IMPOSSIBILIDADE DE REGISTRO'");
    }

    @E("clico em salvar modal de JUSTIFICATIVA")
    public void clicoEmSalvarModalDeJUSTIFICATIVA() {
        dadosViaPage.clicarBtnSalvarModalJustificativa();
        addScreenshotToReport("Salvei a justificativa.");
    }

    @E("valida que a justificativa esta preenchida")
    public void validaQueAJustificativaEstaPreenchida() {
        dadosViaPage.validarJustificativaInserida(dadosVia);
        addScreenshotToReport("Validado informações preenchidas no modal de 'Não foi possível fazer o registro das medições'.");
    }

    @E("clico no botão Excluir Justificativa")
    public void clicoNoBotãoExcluirJustificativa() {
        dadosViaPage.clicarBtnExcluirJustificativa();
        addLogToReport("Cliquei no botão Excluir Justificativa");
    }

    @Então("o botão Salvar Informações Dados Via deve estar disabled")
    public void oBotãoSalvarInformaçõesDadosViaDeveEstarDisabled() {
        dadosViaPage.validaBotaoSalvarInformacoesDisabled();
        addScreenshotToReport("Validado que o botão está disable e os campos do formulário estão obrigatórios.");
    }

    @E("clico no botão Preencher em tela cheia")
    public void clicoNoBotãoPreencherEmTelaCheia() {
        dadosViaPage.clicarBtnAbrirModalTabelaAnomalias();
        addLogToReport("Cliquei no botão Excluir Justificativa");
    }

    @Então("deve exibir o modal de preenchimento de tabela de anomalias")
    public void deveExibirOModalDePreenchimentoDeTabelaDeAnomalias() {
        tabelaAnomaliasPage.validaExibicao();
        addScreenshotToReport("Validei que exibiu a tabela de anomalias.");
    }

    @E("ao preencher todo o modal de anomalias")
    public void aoPreencherTodoOModalDeAnomalias() {
        tabelaAnomaliasPage.preencherDadosClasse(tabelaAnomalias);
        addScreenshotToReport("Preenchi os dados de Superelevação mínima, Superelevação máxima e  Velocidade de circulação ");
        tabelaAnomaliasPage.preencherSuperel(tabelaAnomalias);
        addScreenshotToReport("Preenchi os dados de Superel.");
        tabelaAnomaliasPage.preencherEfeitoDinamico(tabelaAnomalias);
        addScreenshotToReport("Preenchi os dados de Efeito Dinâmico.");
        tabelaAnomaliasPage.preencherBitola(tabelaAnomalias);
        addScreenshotToReport("Preenchi os dados de Bitola.");
        tabelaAnomaliasPage.preencherFlechaCorda(tabelaAnomalias);
        addScreenshotToReport("Preenchi os dados de Flecha Corda.");
    }

    @Então("deve preencher automaticamente toda a tabela de anomalias")
    public void devePreencherAutomaticamenteTodaATabela() {
        tabelaAnomaliasPage.validaValoresDaTabela();
    }

    @E("salvar as tabela de anomalias")
    public void salvarAsTabelaDeAnomalias() {
        tabelaAnomaliasPage.clicarBtnSalvar();
        addLogToReport("Cliquei no botão de Salvar");
    }

    @E("clico na tab VIA")
    public void clicoNaTabVIA() {
        dadosViaPage.clicoNaTabVia();
        addLogToReport("cliquei sobre a tab Via");
    }

    @E("clico no botão Todas as colunas")
    public void clicoNoBotãoTodasAsColunas() {
        tabelaAnomaliasPage.clicarBtnTodasAsColunas();
        addLogToReport("clicou no botão 'Todas as colunas'");
    }

    @E("valido que a Dados Via está no modo readonly")
    public void validoQueADadosViaEstáNoModoReadonly() {
        dadosViaPage.validaFormReadOnly();
        addScreenshotToReport("Validei que está no modo readonly");
    }
}
