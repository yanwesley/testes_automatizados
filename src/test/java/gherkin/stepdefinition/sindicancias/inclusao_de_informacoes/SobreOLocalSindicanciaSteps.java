package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.SobreOLocal;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.SobreOLocalSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.getNewSobreOLocal;
import static br.com.api.GeradorDeMassa.getSobreOLocal;

public class SobreOLocalSindicanciaSteps extends ReporterUtils {

    SobreOLocalSindicanciaPage sobreOLocalSindicanciaPage = new SobreOLocalSindicanciaPage();
    SobreOLocal sobreOLocal = getSobreOLocal();
    SobreOLocal sobreOLocalEditado = getNewSobreOLocal();

    @E("valido que estou na página sobre o local")
    public void validoQueEstouNaPáginaSobreOLocal() {
        sobreOLocalSindicanciaPage.validoQueEstouSobreOLocal();
        addScreenshotToReport("Validei que estou na tela Sobre o local");
    }

    @Então("preencho sobre o local")
    public void preenchoSobreOLocal() {
        sobreOLocalSindicanciaPage.preencherInformacoesSobreOLocal(sobreOLocal);
        addScreenshotToReport("Preenchi dados sobre o local");
        sobreOLocalSindicanciaPage.preencherUnidadeDeAtendimento(sobreOLocal);
        addScreenshotToReport("Preenchi todos as unidades de atendimento");
        sobreOLocalSindicanciaPage.preencherAtivosDaCompanhia(sobreOLocal);
        addScreenshotToReport("Preenchi ativos da companhia");
        sobreOLocalSindicanciaPage.preencherTipoPerimetro(sobreOLocal);
        addScreenshotToReport("Preenchi o tipo do perímetro");
        sobreOLocalSindicanciaPage.preencherLocalAcidente(sobreOLocal);
        addScreenshotToReport("preenchi o local do acidente");
    }

    @E("valido que as informações foram salvas e persistem")
    public void validoQueAsInformaçõesForamSalvasEPersistem() {
        sobreOLocalSindicanciaPage.validarInformacoesSobreOLocal(sobreOLocal);
        addScreenshotToReport("Validei dados sobre o local");
        sobreOLocalSindicanciaPage.validarUnidadeDeAtendimento(sobreOLocal);
        addScreenshotToReport("Validei todos as unidades de atendimento");
        sobreOLocalSindicanciaPage.validarAtivosDaCompanhia(sobreOLocal);
        addScreenshotToReport("Validei ativos da companhia");
        sobreOLocalSindicanciaPage.validarTipoPerimetro(sobreOLocal);
        addScreenshotToReport("Validei o tipo do perímetro");
        sobreOLocalSindicanciaPage.validarLocalAcidente(sobreOLocal);
        addScreenshotToReport("Validei o local do acidente");
    }

    @E("clico no botão Salvar do Sobre o Local")
    public void clicoNoBotãoSalvarDoSobreOLocal() {
        sobreOLocalSindicanciaPage.clicarEmSalvarSobreOLocal();
        addLogToReport("Cliquei no botão salvar");
    }

    @Então("Edito as informações")
    public void editoAsInformações() {
        sobreOLocalSindicanciaPage.preencherInformacoesSobreOLocal(sobreOLocalEditado);
        addScreenshotToReport("Editei dados sobre o local");
        sobreOLocalSindicanciaPage.preencherUnidadeDeAtendimento(sobreOLocalEditado);
        addScreenshotToReport("Editei todos as unidades de atendimento");
        sobreOLocalSindicanciaPage.preencherAtivosDaCompanhia(sobreOLocalEditado);
        addScreenshotToReport("Editei ativos da companhia");
        sobreOLocalSindicanciaPage.preencherTipoPerimetro(sobreOLocalEditado);
        addScreenshotToReport("Editei o tipo do perímetro");
        sobreOLocalSindicanciaPage.preencherLocalAcidente(sobreOLocalEditado);
        addScreenshotToReport("Editei o local do acidente");
    }

    @E("valido que as informações editadas foram salvas e persistem")
    public void validoQueAsInformaçõesEditadasForamSalvasEPersistem() {
        sobreOLocalSindicanciaPage.validarInformacoesSobreOLocal(sobreOLocalEditado);
        addScreenshotToReport("Validei dados sobre o local");
        sobreOLocalSindicanciaPage.validarUnidadeDeAtendimento(sobreOLocalEditado);
        addScreenshotToReport("Validei todos as unidades de atendimento");
        sobreOLocalSindicanciaPage.validarAtivosDaCompanhia(sobreOLocalEditado);
        addScreenshotToReport("Validei ativos da companhia");
        sobreOLocalSindicanciaPage.validarTipoPerimetro(sobreOLocalEditado);
        addScreenshotToReport("Validei o tipo do perímetro");
        sobreOLocalSindicanciaPage.validarLocalAcidente(sobreOLocalEditado);
        addScreenshotToReport("Validei o local do acidente");

    }

    @E("valido que as informações foram exibidas no modo readonly")
    public void validoQueAsInformaçõesForamExibidasNoModoReadonly() {
        sobreOLocalSindicanciaPage.validarInformacoesSobreOLocal(sobreOLocal);
        addScreenshotToReport("Validei dados sobre o local");
        sobreOLocalSindicanciaPage.validarUnidadeDeAtendimento(sobreOLocal);
        addScreenshotToReport("Validei todos as unidades de atendimento");
        sobreOLocalSindicanciaPage.validarAtivosDaCompanhia(sobreOLocal);
        addScreenshotToReport("Validei ativos da companhia");
        sobreOLocalSindicanciaPage.validarTipoPerimetro(sobreOLocal);
        addScreenshotToReport("Validei o tipo do perímetro");
        sobreOLocalSindicanciaPage.validarLocalAcidente(sobreOLocal);
        addScreenshotToReport("Validei o local do acidente");
        sobreOLocalSindicanciaPage.validarBotoesDisabled();
        addScreenshotToReport("Validei a exibição dos botões.");
    }

    @E("valido que Sobre o Local está no modo readonly")
    public void validoQueSobreOLocalEstáNoModoReadonly() {
        sobreOLocalSindicanciaPage.validaFormReadOnly();
        addScreenshotToReport("Validei que está no modo readonly");
    }
}

