package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.DadosVia;
import br.com.api.model.sindicancia.investigacao.CriticaDeViagem;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.CriticaDaViagemSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;

import static br.com.api.GeradorDeMassa.getCriticaDeViagem;
import static br.com.api.GeradorDeMassa.getDadosVia;

public class CriticaDaViagemSindicanciaSteps extends ReporterUtils {
    DadosVia dadosVia = getDadosVia();
    CriticaDeViagem criticaDeViagem = getCriticaDeViagem();
    CriticaDaViagemSindicanciaPage criticaDaViagemSindicanciaPage = new CriticaDaViagemSindicanciaPage();


    @E("estou na página de CRÍTICA DA VIAGEM")
    public void estouNaPáginaDeCRÍTICADAVIAGEM() {
        criticaDaViagemSindicanciaPage.validaQueEstaNaPaginaDeCriticaDaViagem();
        addScreenshotToReport("Valida que e estou na página de Crítica da viagem");
    }

    @E("valida as informações preenchidas na Análise da viagem apresentadas na tela de Crítica da viagem")
    public void validaAsInformaçõesPreenchidasNaAnáliseDaViagemApresentadasNaTelaDeCríticaDaViagem() {
        criticaDaViagemSindicanciaPage.validaInformacoesPreenchidasNaTelaAnaliseDaViagem(dadosVia);
    }

    @Dado("que incluo uma nova Crítica de viagem")
    public void queIncluoUmaNovaCríticaDeViagem() {
        criticaDaViagemSindicanciaPage.preencheCriticaDeViagem(criticaDeViagem);
        addScreenshotToReport("Preencheu Crítica da viagem");
    }

    @E("clico no botão Salvar Informações do Crítica da viagem")
    public void clicoNoBotãoSalvarInformaçõesDoCríticaDaViagem() {
        criticaDaViagemSindicanciaPage.clicarBtnSalvarInformacoesCriticaDaViagem();
        addLogToReport("Clicou no botão salvar informações");
    }

    @E("valida as informações preenchidas na Crítica da viagem")
    public void validaAsInformaçõesPreenchidasNaCríticaDaViagem() {
        criticaDaViagemSindicanciaPage.validaCriticaDeViagem(criticaDeViagem);
        addScreenshotToReport("Valida informações inseridas na Crítica da viagem");
    }

    @E("o botão Salvar Informações Crítica da viagem deve estar disabled")
    public void oBotãoSalvarInformaçõesCríticaDaViagemDeveEstarDisabled() {
        criticaDaViagemSindicanciaPage.validaBotaoSalvarInformacoesDisabled();
        addScreenshotToReport("Validado que o botão Salvar informações está disable");
    }

    @E("valida as informações preenchidas na Análise da viagem apresentadas na tela de Crítica da viagem Read Only")
    public void validaAsInformaçõesPreenchidasNaAnáliseDaViagemApresentadasNaTelaDeCríticaDaViagemReadOnly() {
        criticaDaViagemSindicanciaPage.validaCriticaDeViagem(criticaDeViagem);
        addScreenshotToReport("Valida informações inseridas na Crítica da viagem");
    }
}
