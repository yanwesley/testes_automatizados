package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.HeaderInformacoesSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;


public class HeaderInformacoesSindicanciaSteps extends ReporterUtils {
    HeaderInformacoesSindicanciaPage headerInformacoesSindicanciaPage = new HeaderInformacoesSindicanciaPage();

    @E("clico no botão Ficha do Acidente")
    public void clicoNoBotãoFichaDoAcidente() {
        headerInformacoesSindicanciaPage.clicarBtnFichaAcidente();
        addLogToReport("Cliquei no botão 'Ficha do Acidente'");
    }

    @E("clico no botão Ficha investigação")
    public void clicoNoBotãoFichaInvestigação() {
        headerInformacoesSindicanciaPage.clicarBtnFichaInvestigacao();
        addLogToReport("Cliquei no botão 'Ficha Investigação'");
    }

    @Então("deve abrir uma nova guia com a ficha do acidente")
    public void deveAbrirUmaNovaGuiaComAFichaDoAcidente() {
        headerInformacoesSindicanciaPage.switchToWindow();
    }

    @E("altero a classificação da sindicância para {string}")
    public void alteroAClassificaçãoDaSindicânciaPara(String classificacao) {
        headerInformacoesSindicanciaPage.alterarClassificacao(classificacao);
        addScreenshotToReport("selecionei a classificação: " + classificacao);
        headerInformacoesSindicanciaPage.clicarBtnConfirmarModal();
    }

    @E("deve exibir a classificação {string} no header da sindicância")
    public void deveExibirAClassificaçãoNoHeaderDaSindicância(String classificacao) {
        headerInformacoesSindicanciaPage.exibirClassificacao(classificacao);
        addScreenshotToReport("Exibe o tipo da classe no Header");
    }

    @E("altero a classe para {string}")
    public void alteroAImportânciaPara(String importância) {
        headerInformacoesSindicanciaPage.selecionarImportancia(importância);
        addScreenshotToReport("Selecionei o tipo da classe");
        headerInformacoesSindicanciaPage.clicarBtnConfirmarModal();
    }

    @E("deve exibir a classe {string} no header da sindicância")
    public void deveExibirAImportânciaNoHeaderDaSindicância(String importância) {
        headerInformacoesSindicanciaPage.exibirImportancia(importância);
        addScreenshotToReport("Valida o tipo da classe no Header");
    }
}