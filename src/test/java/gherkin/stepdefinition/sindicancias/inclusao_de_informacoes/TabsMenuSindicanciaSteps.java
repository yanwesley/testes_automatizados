package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.TabsMenuSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Quando;


public class TabsMenuSindicanciaSteps extends ReporterUtils {
    TabsMenuSindicanciaPage tabsMenuSindicanciaPage = new TabsMenuSindicanciaPage();

    @Quando("clico na TAB Operação")
    public void clicoNaTABOperação() {
        tabsMenuSindicanciaPage.clicarTabOperacao();
        addLogToReport("Clicou na tab Operação");
    }

    @Quando("clico na TAB Outros")
    public void clicoNaTABOutros() {
        tabsMenuSindicanciaPage.clicarTabOutros();
        addLogToReport("Clicou na tab Outros");
    }

    @Quando("clico na TAB Material Rodante")
    public void clicoNaTABMaterialRodante() {
        tabsMenuSindicanciaPage.clicarTabMaterialRodante();
        addLogToReport("Clicou na tab Outros");
    }

    @Quando("clico na TAB Segurança Patrimonial")
    public void clicoNaTABSegurançaPatrimonial() {
        tabsMenuSindicanciaPage.clicarTabSegurancaPatrimonial();
        addLogToReport("Clicou na tab Segurança Patrimonial");
    }

    @Quando("clico na TAB Análise Técnica")
    public void clicoNaTABAnáliseTécnica() {
        tabsMenuSindicanciaPage.clicarTabAnaliseTecnica();
        addLogToReport("Clicou na tab Segurança Patrimonial");
    }

    @E("valido que o menu Financeiro está sendo exibido")
    public void validoQueOMenuFinanceiroEstáSendoExibido() {
        tabsMenuSindicanciaPage.validaExibicaoMenuFinanceiro();
        addScreenshotToReport("validou que o menu Financeiro está sendo exibido");
    }

    @E("valido que o menu Financeiro não está sendo exibido")
    public void validoQueOMenuFinanceiroNãoEstáSendoExibido() {
        tabsMenuSindicanciaPage.validaQueOMenuFinanceiroNaoEstaAparecendo();
        addScreenshotToReport("validou que o menu Financeiro não está sendo exibido");
    }

    @Quando("clico na TAB Via")
    public void clicoNaTABVia() {
        tabsMenuSindicanciaPage.clicarTabVia();
        addLogToReport("Clicou na tab Via");
    }
}