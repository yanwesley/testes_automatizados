package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.Trilho;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.TrilhoPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getNewTrilho;
import static br.com.api.GeradorDeMassa.getTrilho;


public class TrilhoSindicanciaSteps extends ReporterUtils {
    TrilhoPage trilhoPage = new TrilhoPage();
    Trilho trilho = getTrilho();
    Trilho trilhoEditado = getNewTrilho();

    @E("preenche os dados do Trilho")
    public void preencheOsDadosDoTrilho() {
        trilhoPage.preencherDadosGerais(trilho);
        addScreenshotToReport("Preenchi dados gerais.");
        trilhoPage.preencherDadosDoTrilho(trilho);
        addScreenshotToReport("Preenchi dados do trilho.");
        trilhoPage.preencherInformacoesDoDTQ(trilho);
        addScreenshotToReport("Preenchi informações do DTQ.");
        trilhoPage.preencherDadosDaTemperatura(trilho);
        addScreenshotToReport("Preenchi dados da temperatura.");
    }

    @Quando("clico no botão Salvar Informações Trilho")
    public void clicoNoBotãoSalvarInformaçõesTrilho() {
        trilhoPage.clicarBtnSalvarInformacoes();
        addLogToReport("Cliquei no botão 'Salvar informações'.");
    }

    @E("valida que os valores preenchidos sobre o trilho persistiram")
    public void validaQueOsValoresPreenchidosSobreOTrilhoPersistiram() {
        trilhoPage.validarDadosGerais(trilho);
        addScreenshotToReport("Validei a persistência do dados gerais.");
        trilhoPage.validarDadosDoTrilho(trilho);
        addScreenshotToReport("Validei a persistência do dados do trilho.");
        trilhoPage.validarInformacoesDoDTQ(trilho);
        addScreenshotToReport("Validei a persistência do informações do DTQ.");
        trilhoPage.validarDadosDaTemperatura(trilho);
        addScreenshotToReport("Validei a persistência do dados da temperatura.");
    }

    @E("edito os dados do Trilho")
    public void editoOsDadosDoTrilho() {
        trilhoPage.preencherDadosGerais(trilhoEditado);
        addScreenshotToReport("Editei os dados gerais.");
        trilhoPage.preencherDadosDoTrilho(trilhoEditado);
        addScreenshotToReport("Editei os dados do trilho.");
        trilhoPage.preencherInformacoesDoDTQ(trilhoEditado);
        addScreenshotToReport("Editei as informações do DTQ.");
        trilhoPage.preencherDadosDaTemperatura(trilhoEditado);
        addScreenshotToReport("Editei os dados da temperatura.");
    }

    @E("valida que os valores editado sobre o trilho persistiram")
    public void validaQueOsValoresEditadoSobreOTrilhoPersistiram() {
        trilhoPage.validarDadosGerais(trilhoEditado);
        addScreenshotToReport("Validei a persistência do dados gerais.");
        trilhoPage.validarDadosDoTrilho(trilhoEditado);
        addScreenshotToReport("Validei a persistência do dados do trilho.");
        trilhoPage.validarInformacoesDoDTQ(trilhoEditado);
        addScreenshotToReport("Validei a persistência do informações do DTQ.");
        trilhoPage.validarDadosDaTemperatura(trilhoEditado);
        addScreenshotToReport("Validei a persistência do dados da temperatura.");
    }
}
