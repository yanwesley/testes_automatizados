package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.AnaliseDeViagem;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.AnaliseDaViagemSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getAnaliseDeViagem;

public class AnaliseDaViagemSindicanciaSteps extends ReporterUtils {

    AnaliseDeViagem analiseDeViagem = getAnaliseDeViagem();
    AnaliseDaViagemSindicanciaPage analiseDaViagemSindicanciaPage = new AnaliseDaViagemSindicanciaPage();

    @E("estou na página de ANÁLISE DE VIAGEM")
    public void estouNaPáginaDeANÁLISEDEVIAGEM() {
        analiseDaViagemSindicanciaPage.validaQueEstaNaPaginaDeAnaliseDaViagem();
        addScreenshotToReport("Valida que o usuário está na página de Análise da viagem.");
    }

    @Quando("clico no botão Salvar Informações do Análise da viagem")
    public void clicoNoBotãoSalvarInformaçõesDoAnáliseDaViagem() {
        analiseDaViagemSindicanciaPage.clicarBtnSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar informações");
    }

    @E("o botão Salvar Informações Análise da viagem deve estar disabled")
    public void oBotãoSalvarInformaçõesAnáliseDaViagemDeveEstarDisabled() {
        analiseDaViagemSindicanciaPage.validaBotaoSalvarInformacoesDisabled();
        addScreenshotToReport("Validado que o botão Salvar informações está disable");
    }

    @Dado("que incluo uma nova Análise de viagem")
    public void queIncluoUmaNovaAnáliseDeViagem() {
        analiseDaViagemSindicanciaPage.preencheAnaliseDeViagem(analiseDeViagem);
        addScreenshotToReport("Preencheu a seção de Análise de viagem");
        analiseDaViagemSindicanciaPage.preencheAnaliseDeViagemComparativaUm(analiseDeViagem);
        addScreenshotToReport("Preencheu a seção de Análise de viagem comparativa 1");
        analiseDaViagemSindicanciaPage.preencheAnaliseDeViagemComparativaDois(analiseDeViagem);
        addScreenshotToReport("Preencheu a seção de Análise de viagem comparativa 2");
        analiseDaViagemSindicanciaPage.preencheLogDaLocomotiva(analiseDeViagem);
        addScreenshotToReport("Preencheu a seção de Log da Locomotiva");
    }

    @Dado("validando Análise de Viagem preenchida")
    public void validandoAnáliseDeViagemPreenchida() {
        analiseDaViagemSindicanciaPage.validoAnaliseDeViagem(analiseDeViagem);
        addScreenshotToReport("Validou a seção de Análise da viagem");
        analiseDaViagemSindicanciaPage.validoAnaliseDeViagemComparativaUm(analiseDeViagem);
        addScreenshotToReport("Validou a seção de Análise da viagem  comparativa 1");
        analiseDaViagemSindicanciaPage.validoAnaliseDeViagemComparativaDois(analiseDeViagem);
        addScreenshotToReport("Validou a seção de Análise da viagem  comparativa 2");
        analiseDaViagemSindicanciaPage.validoLogDaLocomotiva(analiseDeViagem);
        addScreenshotToReport("Validou a seção de Log da Locomotiva");
    }

    @E("valido a exibição da análise de viagem em modo Read Only")
    public void validoAExibiçãoDaAnáliseDeViagemEmModoReadOnly() {
        analiseDaViagemSindicanciaPage.validoAnaliseDeViagem(analiseDeViagem);
        addScreenshotToReport("Validou a seção de Análise da viagem");
        analiseDaViagemSindicanciaPage.validoAnaliseDeViagemComparativaUm(analiseDeViagem);
        addScreenshotToReport("Validou a seção de Análise da viagem  comparativa 1");
        analiseDaViagemSindicanciaPage.validoAnaliseDeViagemComparativaDois(analiseDeViagem);
        addScreenshotToReport("Validou a seção de Análise da viagem  comparativa 2");
        analiseDaViagemSindicanciaPage.validoLogDaLocomotiva(analiseDeViagem);
        addScreenshotToReport("Validou a seção de Log da Locomotiva");
    }

    @E("valido que a Analise de Viagem está no modo readonly")
    public void validoQueAAnaliseDeViagemEstáNoModoReadonly() {
        analiseDaViagemSindicanciaPage.validaFormReadOnly();
        addScreenshotToReport("Validei que está no modo readonly");
    }
}
