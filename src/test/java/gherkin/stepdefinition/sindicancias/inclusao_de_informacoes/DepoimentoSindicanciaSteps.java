package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.DepoimentoSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class DepoimentoSindicanciaSteps extends ReporterUtils {

    DepoimentoSindicanciaPage depoimentoSindicanciaPage = new DepoimentoSindicanciaPage();

    @E("estou na página de DEPOIMENTO")
    public void estouNaPáginaDeDEPOIMENTO() {
        depoimentoSindicanciaPage.validaQueEstaNaPaginaDeDepoimento();
        addScreenshotToReport("Estou na página de DEPOIMENTO");
    }

    @Dado("insere {int} arquivo na tela de depoimento")
    public void insereArquivoNaTelaDeDepoimento(int qtdArquivo) {
        depoimentoSindicanciaPage.insereArquivo(qtdArquivo);
        addScreenshotToReport("Inseriu " + qtdArquivo + "arquivo(s) na tela de anexos");
    }

    @E("insiro uma observação na tela de depoimento")
    public void insiroUmaObservaçãoNaTelaDeDepoimento() {
        depoimentoSindicanciaPage.insereObservacao();
        addScreenshotToReport("Inseriu uma observação");
    }

    @E("clico em Salvar Informações da tela de depoimento")
    public void clicoEmSalvarInformaçõesDaTelaDeDepoimento() {
        depoimentoSindicanciaPage.clicarBtnSalvarInformacao();
        addLogToReport("Clicou no botão Salvar informação");
    }

    @Entao("valida que os {int} arquivos na tela de depoimento foram adicionados")
    public void validaQueOsArquivosNaTelaDeDepoimentoForamAdicionados(int qtdArquivosAdicionados) {
        depoimentoSindicanciaPage.validaQueOsArquivosForamAdicionados(qtdArquivosAdicionados);
        addScreenshotToReport("Valida que os arquivos foram adicionados");
    }

    @E("valida a observação inserida na tela de depoimento")
    public void validaAObservaçãoInseridaNaTelaDeDepoimento() {
        depoimentoSindicanciaPage.validaObservacaoInserida();
        addScreenshotToReport("Valida a observação inserida");
    }

    @E("clico no botão Salvar e Avançar da tela de Depoimento")
    public void clicoNoBotãoSalvarEAvançarDaTelaDeDepoimento() {
        depoimentoSindicanciaPage.clicarBtnSalvarEAvancar();
        addLogToReport("Clicou no botão Salvar e Avançar");
    }

    @Quando("excluo {int} arquivos da tela de depoimento")
    public void excluoArquivosDaTelaDeDepoimento(int qtdArquivosExcluidos) {
        depoimentoSindicanciaPage.excluirArquivos(qtdArquivosExcluidos);
        addScreenshotToReport("Valida que o(s)" + qtdArquivosExcluidos + " foram excluídos");
    }
}
