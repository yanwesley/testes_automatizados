package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.ConclusaoSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getConclusao;

public class ConclusaoSindicanciaSteps extends ReporterUtils {

    ConclusaoSindicanciaPage conclusaoSindicanciaPage = new ConclusaoSindicanciaPage();
    String conclusao = getConclusao();

    @Quando("preencho o campo de Conclusão")
    public void preenchoOCampoDeConclusão() {
        conclusaoSindicanciaPage.preencheConclusao(conclusao);
        addScreenshotToReport("Conclusão preenchida");
    }

    @E("clico no botão Salvar da Conclusão")
    public void clicoNoBotãoSalvarDaConclusão() {
        conclusaoSindicanciaPage.clicarBtnSalvar();
        addLogToReport("Clicou no botão Salvar");
    }

    @Quando("que clico no botão Voltar da tela de Conclusão")
    public void queClicoNoBotãoVoltarDaTelaDeConclusão() {
        conclusaoSindicanciaPage.clickBackButton();
        addLogToReport("Clicou no botão voltar");
    }

    @Entao("valido que a conclusão inserida está sendo apresentada")
    public void validoQueAConclusãoInseridaEstáSendoApresentada() {
        conclusaoSindicanciaPage.validaConclusaoIncluida(conclusao);
        addScreenshotToReport("Valida que conclusão inserida está sendo apresentada");
    }

    @Quando("clico no botão Salvar e Concluir")
    public void clicoNoBotãoSalvarEConcluir() {
        conclusaoSindicanciaPage.clicarBtnSalvarEConcluir();
        addLogToReport("Clicou no botão Salvar e Concluir");
    }

    @Entao("estou na página de CONCLUSÃO")
    public void estouNaPáginaDeCONCLUSÃO() {
        conclusaoSindicanciaPage.validaQueEstaNaPaginaDeConclusao();
        addScreenshotToReport("Valida que o usuário está na página de Conclusão");
    }

    @Entao("valida o ícone atualizado do status da sindicancia")
    public void validaOÍconeAtualizado() {
        conclusaoSindicanciaPage.validaIconeStatusSindicancia();
        addScreenshotToReport("Valida o ícone atualizado na lista de sindicância");
    }

    @E("clico no botão Salvar e Concluir Segurança Patrimonial")
    public void clicoNoBotãoSalvarEConcluirSegurancaPatrimonial() {
        conclusaoSindicanciaPage.clicarBtnSalvarEConcluirSegPatrimonial();
        addScreenshotToReport("Cliquei no botão salvar e concluir");
    }

    @Entao("valido que a conclusão inserida está sendo apresentada no modo readonly")
    public void validoQueAConclusãoInseridaEstáSendoApresentadaNoModoReadonly() {
        conclusaoSindicanciaPage.validaConclusaoIncluida(conclusao);
        addScreenshotToReport("Valida que conclusão inserida está sendo apresentada");
    }

    @E("valido a label {string} da conclusão da Análise Técnica")
    public void validoALabelDaConclusãoDaAnáliseTécnica(String label) {
        conclusaoSindicanciaPage.validaLabelConclusaoAnaliseTecnica(label);
        addScreenshotToReport("Valida label da conclusão da Análise Técnica ");
    }
}
