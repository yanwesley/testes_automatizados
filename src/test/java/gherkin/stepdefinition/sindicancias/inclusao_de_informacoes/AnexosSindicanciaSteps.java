package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.AnexosSindicanciaPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.SideMenuSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

import static br.com.api.GeradorDeMassa.getComentario4000Char;

public class AnexosSindicanciaSteps extends ReporterUtils {

    SideMenuSindicanciaPage sideMenuSindicanciaPage = new SideMenuSindicanciaPage();
    AnexosSindicanciaPage anexosSindicanciaPage = new AnexosSindicanciaPage();
    String comentario = getComentario4000Char();
    String comentarioEditado = getComentario4000Char();


    @E("clico no side-menu ANEXO")
    public void clicoNoSideMenuANEXO() {
        sideMenuSindicanciaPage.clicarMenuAnexos();
        addLogToReport("Clicou no side-menu ANEXO");
    }

    @E("estou na página de ANEXOS")
    public void estouNaPáginaDeANEXOS() {
        anexosSindicanciaPage.validaQueEstaNaPaginaDeAnexos();
        addScreenshotToReport("Valida que o usuário está na página de ANEXOS");
    }

    @Dado("clico no botão Incluir Anexos")
    public void clicoNoBotãoIncluirAnexos() {
        anexosSindicanciaPage.clicarBtnIncluirAnexos();
        addLogToReport("Clicou no botão Incluir Anexos");
    }

    @Entao("insere {int} arquivo na tela de anexos")
    public void insereArquivoNaTelaDeAnexos(int qtdArquivo) {
        anexosSindicanciaPage.insereArquivo(qtdArquivo);
        addScreenshotToReport("Inseriu " + qtdArquivo + "arquivo(s) na tela de anexos");
    }

    @E("insere um comentário na inclusão do anexo")
    public void insereUmComentárioNaInclusãoDoAnexo() {
        anexosSindicanciaPage.insereComentario(comentario);
        addScreenshotToReport("Inseriu um comentário");
    }

    @E("clico no botão Salvar da tela de anexos")
    public void clicoNoBotãoSalvarDaTelaDeAnexos() {
        anexosSindicanciaPage.clicarBtnSalvar();
        addLogToReport("Clicou no botão Salvar");
    }

    @E("valida que o\\(s) {int} arquivo\\(s) foram adicionados")
    public void validaQueOSArquivoForamAdicionados(int qtdArquivosAdicionados) {
        anexosSindicanciaPage.validaArquivoFoiInserido(qtdArquivosAdicionados);
        addScreenshotToReport("Valida que o(s)" + qtdArquivosAdicionados + " foram adicionados");
    }

    @Dado("excluo {int} arquivos dos Anexos")
    public void excluoArquivosDosAnexos(int qtdArquivosExcluidos) {
        anexosSindicanciaPage.excluirArquivos(qtdArquivosExcluidos);
        addScreenshotToReport("Valida que o(s)" + qtdArquivosExcluidos + " foram excluídos");
    }

    @Entao("insere {int} arquivo na tela de anexos com comentário")
    public void insereArquivoNaTelaDeAnexosComComentário(int qtdArquivo) {
        anexosSindicanciaPage.insereArquivosComComentario(qtdArquivo, comentario);
        addScreenshotToReport("Valida que o(s)" + qtdArquivo + " foram adicionados");
    }

    @Dado("clico no botão Editar do anexo")
    public void clicoNoBotãoEditarDoAnexo() {
        anexosSindicanciaPage.clicarBtnEditarAnexo();
        addLogToReport("Clicou no botão Editar Anexo");
    }

    @E("edito o comentário do anexo")
    public void editoOComentárioDoAnexo() {
        anexosSindicanciaPage.editaComentarioDoAnexo(comentarioEditado);
        addScreenshotToReport("Editou o comentário");
    }

    @E("valida que o comentário do anexo foi editado")
    public void validaQueOComentárioDoAnexoFoiEditado() {
        anexosSindicanciaPage.validaComentarioEditado(comentarioEditado);
        addScreenshotToReport("Valida o comentário editado");
    }
}
