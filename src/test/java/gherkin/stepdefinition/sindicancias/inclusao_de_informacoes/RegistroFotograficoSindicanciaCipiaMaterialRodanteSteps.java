package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.RegistroFotograficoSindicanciaCipiaMaterialRodantePage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

public class RegistroFotograficoSindicanciaCipiaMaterialRodanteSteps extends ReporterUtils {

    RegistroFotograficoSindicanciaCipiaMaterialRodantePage registroFotograficoSindicanciaCipiaMaterialRodantePage = new RegistroFotograficoSindicanciaCipiaMaterialRodantePage();

    @Entao("valida os itens da tela de Registro fotográfico do perfil CIPIA Material Rodante")
    public void validaOsItensDaTelaDeRegistroFotográficoDoPerfilCIPIAMaterialRodante() {
        registroFotograficoSindicanciaCipiaMaterialRodantePage.validarItemsDaTela();
        addScreenshotToReport("Validei os campos da tela de 'Registros fotográficos'.");
    }

    @Dado("insiro as imagens na tela de registro fotográfico para CIPIA Material Rodante")
    public void insiroAsImagensNaTelaDeRegistroFotográficoParaCIPIAMaterialRodante() {
        registroFotograficoSindicanciaCipiaMaterialRodantePage.adicionarImagensComTituloDefinido();
    }

    @Entao("valido que as imagens foram inseridas na tela de registro fotográfico para CIPIA Material Rodante")
    public void validoQueAsImagensForamInseridasNaTelaDeRegistroFotográficoParaCIPIAMaterialRodante() {
        registroFotograficoSindicanciaCipiaMaterialRodantePage.validarImagensComTituloDefinido();
    }

    @Dado("excluo as imagens na tela de registro fotográfico do perfil CIPIA Material Rodante")
    public void excluoAsImagensNaTelaDeRegistroFotográficoDoPerfilCIPIAMaterialRodante() {
        registroFotograficoSindicanciaCipiaMaterialRodantePage.excluirImagensComTituloDefinido();
    }

    @Entao("valida os itens da tela de Registro fotográfico do perfil CIPIA Material Rodante foram excluídos")
    public void validaOsItensDaTelaDeRegistroFotográficoDoPerfilCIPIAMaterialRodanteForamExcluídos() {
        registroFotograficoSindicanciaCipiaMaterialRodantePage.validaExclusaoImagensComTituloDefinido();
    }

    @Dado("que insiro {int} imagem\\(s) adicional na tela de registro fotográfico para CIPIA Material Rodante")
    public void queInsiroImagemSAdicionalNaTelaDeRegistroFotográficoParaCIPIAMaterialRodante(int quantidade) {
        registroFotograficoSindicanciaCipiaMaterialRodantePage.inserirImagemAdicional(quantidade);
    }

    @Entao("valida que a {int} imagem\\(s) adicional do perfil CIPIA Material Rodante foi adicionadas")
    public void validaQueAImagemSAdicionalDoPerfilCIPIAMaterialRodanteFoiAdicionadas(int quantidade) {
        registroFotograficoSindicanciaCipiaMaterialRodantePage.validarImagemAdicional(quantidade);
    }


    @E("clica no botão Avançar do Registro fotográfico do perfil CIPIA Material Rodante")
    public void clicaNoBotãoAvançarDoRegistroFotográficoDoPerfilCIPIAMaterialRodante() {
    }

    @E("estou na página de DEPOIMENTO do perfil CIPIA Material Rodante")
    public void estouNaPáginaDeDEPOIMENTODoPerfilCIPIAMaterialRodante() {
    }

}
