package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.RelatorioFinal;
import br.com.api.model.sinistro.Sinistro;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.PlanoDeAcaoPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getRelatorioFinal;
import static br.com.api.GeradorDeMassa.getSinistro;

public class PlanoDeAcaoSteps extends ReporterUtils {
    PlanoDeAcaoPage planoDeAcaoPage = new PlanoDeAcaoPage();
    RelatorioFinal relatorioFinal = getRelatorioFinal();
    Sinistro sinistro = getSinistro();

    @Quando("eu clicar no botão ENVIAR PARA APROVAÇÃO")
    public void euClicarNoBotãoENVIARPARAAPROVAÇÃO() {
        planoDeAcaoPage.clicarBtnEnviarParaAprovacao();
        addLogToReport("Cliquei no botão 'ENVIAR PARA APROVAÇÃO'");
    }

    @Entao("deve exibir o modal de confirmação com a mensagem {string}")
    public void deveExibirOModalDeConfirmaçãoComAMensagem(String mensagemModal) {
        planoDeAcaoPage.validaModalEnviarParaAprovacao(mensagemModal);
        addScreenshotToReport("Validei o modal de confirmação");
    }

    @E("clico em confirmar no modal de Enviar para aprovação")
    public void clicoEmConfirmarNoModalDeEnviarParaAprovação() {
        planoDeAcaoPage.clicarBtnConfirmarModalEnviarParaAprovacao();
    }
}
