package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.Financeiro;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.FinanceiroPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.getFinanceiro;
import static br.com.api.GeradorDeMassa.getNewFinanceiro;

public class FinanceiroSindicanciaSteps extends ReporterUtils {

    FinanceiroPage financeiroPage = new FinanceiroPage();
    Financeiro financeiro = getFinanceiro();
    Financeiro financeiroEditado = getNewFinanceiro();

    @Então("preencho o numero da ordem de serviço")
    public void preenchoONumeroDaOrdemDeServiço() {
        financeiroPage.preencherOrdemDeServico(financeiro);
        addScreenshotToReport("Preenchi os dados da ordem de serviço.");
    }

    @E("adicionar RC")
    public void adicionarRC() {
        financeiroPage.adicionarRC(financeiro, 1);
        addScreenshotToReport("Adicionei uma RC");
    }

    @E("adicionar PEDIDO")
    public void adicionarPEDIDO() {
        financeiroPage.adicionarPedido(financeiro, 1);
        addScreenshotToReport("Adicionei um Pedido");
    }

    @E("clico no botão salvar informações do Financeiro")
    public void clicoNoBotãoSalvarInformaçõesDoFinanceiro() {
        financeiroPage.clicarBtnSalvarInformacoes();
        addLogToReport("Cliquei no botão salvar informações.");
    }

    @E("valido se os dados do financeiro persistiram")
    public void validoSeOsDadosDoFinanceiroPersistiram() {
        financeiroPage.validarDados(financeiro, 1);
        addScreenshotToReport("Validei os dados preenchidos.");
    }

    @Então("edito o numero da ordem de serviço")
    public void editoONumeroDaOrdemDeServiço() {
        financeiroPage.preencherOrdemDeServico(financeiroEditado);
        addScreenshotToReport("Editei os dados da ordem de serviço.");
    }

    @E("edito a RC")
    public void editoARC() {
        financeiroPage.editarRC(financeiroEditado, 1);
        addScreenshotToReport("Editei uma RC");
    }

    @E("edito o PEDIDO")
    public void editoOPEDIDO() {
        financeiroPage.editarPedido(financeiroEditado, 1);
        addScreenshotToReport("Editei um Pedido");
    }

    @E("valido se os dados editado do financeiro persistiram")
    public void validoSeOsDadosEditadoDoFinanceiroPersistiram() {
        financeiroPage.validarDados(financeiroEditado, 1);
        addScreenshotToReport("Validei os dados preenchidos.");
    }

    @E("excluo a RC")
    public void excluoARC() {
        financeiroPage.excluirRC(1);
        addScreenshotToReport("Exclui a RC");
    }

    @E("excluo o PEDIDO")
    public void excluoOPEDIDO() {
        financeiroPage.excluirPedido(1);
        addScreenshotToReport("Exclui o Pedido");
    }

    @E("valido se os dados do financeiro foram excluidos")
    public void validoSeOsDadosDoFinanceiroForamExcluidos() {
        financeiroPage.validarDadosExcluidos(1);
        addScreenshotToReport("Validei os dados excluídos.");
    }
}
