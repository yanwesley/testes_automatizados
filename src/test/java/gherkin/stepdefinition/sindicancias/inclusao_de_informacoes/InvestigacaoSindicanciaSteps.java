package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.investigacao.*;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.investigacao.*;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.*;

public class InvestigacaoSindicanciaSteps extends ReporterUtils {

    InvestigacaoSindicanciaPage investigacaoSindicanciaPage = new InvestigacaoSindicanciaPage();
    SobreOAMVInvestigacaoPage sobreOAMVInvestigacaoPage = new SobreOAMVInvestigacaoPage();
    EstacionamentoSindicanciaPage estacionamentoSindicanciaPage = new EstacionamentoSindicanciaPage();
    FormacaoDoTremPage formacaoDoTremPage = new FormacaoDoTremPage();
    CargasPage cargasPage = new CargasPage();
    DadosDoMaquinistaCondutorPage dadosDoMaquinistaCondutorPage = new DadosDoMaquinistaCondutorPage();
    DadosDoSupervisorResponsavelPage dadosDoSupervisorResponsavelPage = new DadosDoSupervisorResponsavelPage();
    DadosDoManobradorAjudantePage dadosDoManobradorAjudantePage = new DadosDoManobradorAjudantePage();

    SobreOAMV sobreOAMV = getSobreOAMV();
    SobreOAMV sobreOAMVEditado = getNewSobreOAMV();

    Estacionamento estacionamento = getEstacionamento();
    Estacionamento estacionamentoEditado = getNewEstacionamento();

    FormacaoDoTrem formacaoDoTrem = getFormacaoDoTrem();
    FormacaoDoTrem formacaoDoTremEditado = getNewFormacaoDoTrem();

    Cargas cargas = getCargas();
    Cargas cargasEditado = getNewCargas();

    DadosDoMaquinistaCondutor dadosDoMaquinistaCondutor = getDadosDoMaquinistaCondutor();
    DadosDoMaquinistaCondutor dadosDoMaquinistaCondutorEditado = getNewDadosDoMaquinistaCondutor();

    DadosDoSupervisorResponsavel dadosDoSupervisorResponsavel = getDadosDoSupervisorResponsavel();
    DadosDoSupervisorResponsavel dadosDoSupervisorResponsavelEditado = getNewDadosDoSupervisorResponsavel();

    DadosDoManobradorAjudante dadosDoManobradorAjudante = getDadosDoManobradorAjudante();
    DadosDoManobradorAjudante dadosDoManobradorAjudanteEditado = getDadosDoManobradorAjudante();

    @Então("preencho todo o formulário de Investigação")
    public void preenchoTodoOFormularioDeInvestigacao() {
        sobreOAMVInvestigacaoPage.preencherDados(sobreOAMV);
        addScreenshotToReport("Preenchi os dados sobre o AMV.");

        investigacaoSindicanciaPage.clicarExpandirEstacionamento();

        estacionamentoSindicanciaPage.preencherDados(estacionamento);
        addScreenshotToReport("Preenchi os dados sobre o Estacionamento.");

        investigacaoSindicanciaPage.clicarExpandirFormacaoDoTrem();
        formacaoDoTremPage.preencherDados(formacaoDoTrem);
        addScreenshotToReport("Preenchi os dados sobre a Formação do Trem.");

        investigacaoSindicanciaPage.clicarExpandirCargas();
        cargasPage.preencherDados(cargas);
        addScreenshotToReport("Preenchi os dados sobre Cargas.");

        investigacaoSindicanciaPage.clicarExpandirMaquinistaCondutor();
        dadosDoMaquinistaCondutorPage.preencherDados(dadosDoMaquinistaCondutor);
        addScreenshotToReport("Preenchi os dados sobre Maquinista ou condutor.");

        investigacaoSindicanciaPage.clicarExpandirSupervisorResponsavel();
        dadosDoSupervisorResponsavelPage.preencherDados(dadosDoSupervisorResponsavel);
        addScreenshotToReport("Preenchi os dados sobre Supervisor Responsavel.");

        investigacaoSindicanciaPage.clicarExpandirManobradorAjudante();
        dadosDoManobradorAjudantePage.preencherDados(dadosDoManobradorAjudante);
        addScreenshotToReport("Preenchi os dados sobre Manobrador Ajudante.");
    }

    @E("clico no botão Salvar Rascunho da Investigação")
    public void clicoNoBotãoSalvarRascunhoDaInvestigação() {
        investigacaoSindicanciaPage.clicarBtnSalvarRascunho();
        addLogToReport("Cliquei no botão 'Salvar Rascunho'.");
    }

    @E("valido que os dados da Investigação estão conforme o preenchido")
    public void validoQueOsDadosDaInvestigaçãoEstãoConformeOPreenchido() {
        investigacaoSindicanciaPage.clicarExpandirSobreOAMV();
        sobreOAMVInvestigacaoPage.validarDados(sobreOAMV);
        addScreenshotToReport("Validei os dados sobre o AMV.");

        investigacaoSindicanciaPage.clicarExpandirEstacionamento();

        estacionamentoSindicanciaPage.validarDados(estacionamento);
        addScreenshotToReport("Validei os dados sobre o Estacionamento.");

        investigacaoSindicanciaPage.clicarExpandirFormacaoDoTrem();
        formacaoDoTremPage.validarDados(formacaoDoTrem);
        addScreenshotToReport("Validei os dados sobre a Formação do Trem.");

        investigacaoSindicanciaPage.clicarExpandirCargas();
        cargasPage.validarDados(cargas);
        addScreenshotToReport("Validei os dados sobre Cargas.");

        investigacaoSindicanciaPage.clicarExpandirMaquinistaCondutor();
        dadosDoMaquinistaCondutorPage.validarDados(dadosDoMaquinistaCondutor);
        addScreenshotToReport("Validei os dados sobre Maquinista ou condutor.");

        investigacaoSindicanciaPage.clicarExpandirSupervisorResponsavel();
        dadosDoSupervisorResponsavelPage.validarDados(dadosDoSupervisorResponsavel);
        addScreenshotToReport("Validei os dados sobre Supervisor Responsavel.");

        investigacaoSindicanciaPage.clicarExpandirManobradorAjudante();
        dadosDoManobradorAjudantePage.validarDados(dadosDoManobradorAjudante);
        addScreenshotToReport("Validei os dados sobre Manobrador Ajudante.");
    }

    @Dado("que eu edite todo o formulário de Investigação")
    public void queEuEditeTodoOFormulárioDeInvestigação() {
        sobreOAMVInvestigacaoPage.preencherDados(sobreOAMVEditado);
        addScreenshotToReport("Editei os dados sobre o AMV.");

        investigacaoSindicanciaPage.clicarExpandirEstacionamento();

        estacionamentoSindicanciaPage.preencherDados(estacionamentoEditado);
        addScreenshotToReport("Editei os dados sobre o Estacionamento.");

        investigacaoSindicanciaPage.clicarExpandirFormacaoDoTrem();
        formacaoDoTremPage.preencherDados(formacaoDoTremEditado);
        addScreenshotToReport("Editei os dados sobre a Formação do Trem.");

        investigacaoSindicanciaPage.clicarExpandirCargas();
        cargasPage.preencherDados(cargasEditado);
        addScreenshotToReport("Editei os dados sobre Cargas.");

        investigacaoSindicanciaPage.clicarExpandirMaquinistaCondutor();
        dadosDoMaquinistaCondutorPage.preencherDados(dadosDoMaquinistaCondutorEditado);
        addScreenshotToReport("Editei os dados sobre Maquinista ou condutor.");

        investigacaoSindicanciaPage.clicarExpandirSupervisorResponsavel();
        dadosDoSupervisorResponsavelPage.preencherDados(dadosDoSupervisorResponsavelEditado);
        addScreenshotToReport("Editei os dados sobre Supervisor Responsavel.");

        investigacaoSindicanciaPage.clicarExpandirManobradorAjudante();
        dadosDoManobradorAjudantePage.preencherDados(dadosDoManobradorAjudanteEditado);
        addScreenshotToReport("Editei os dados sobre Manobrador Ajudante.");

    }

    @E("valido que os dados da Investigação estão conforme o preenchido na edição")
    public void validoQueOsDadosDaInvestigaçãoEstãoConformeOPreenchidoNaEdição() {
        investigacaoSindicanciaPage.clicarExpandirSobreOAMV();
        sobreOAMVInvestigacaoPage.validarDados(sobreOAMVEditado);
        addScreenshotToReport("Validei os dados sobre o AMV.");

        investigacaoSindicanciaPage.clicarExpandirEstacionamento();

        estacionamentoSindicanciaPage.validarDados(estacionamentoEditado);
        addScreenshotToReport("Validei os dados sobre o Estacionamento.");

        investigacaoSindicanciaPage.clicarExpandirFormacaoDoTrem();
        formacaoDoTremPage.validarDados(formacaoDoTremEditado);
        addScreenshotToReport("Validei os dados sobre a Formação do Trem.");

        investigacaoSindicanciaPage.clicarExpandirCargas();
        cargasPage.validarDados(cargasEditado);
        addScreenshotToReport("Validei os dados sobre Cargas.");

        investigacaoSindicanciaPage.clicarExpandirMaquinistaCondutor();
        dadosDoMaquinistaCondutorPage.validarDados(dadosDoMaquinistaCondutorEditado);
        addScreenshotToReport("Validei os dados sobre Maquinista ou condutor.");

        investigacaoSindicanciaPage.clicarExpandirSupervisorResponsavel();
        dadosDoSupervisorResponsavelPage.validarDados(dadosDoSupervisorResponsavelEditado);
        addScreenshotToReport("Validei os dados sobre Supervisor Responsavel.");

        investigacaoSindicanciaPage.clicarExpandirManobradorAjudante();
        dadosDoManobradorAjudantePage.validarDados(dadosDoManobradorAjudanteEditado);
        addScreenshotToReport("Validei os dados sobre Manobrador Ajudante.");
    }

    @E("valido que os dados da Investigação estão conforme o preenchido na edição ReadOnly")
    public void validoQueOsDadosDaInvestigaçãoEstãoConformeOPreenchidoNaEdiçãoReadOnly() {
        sobreOAMVInvestigacaoPage.validarDados(sobreOAMVEditado);
        addScreenshotToReport("Validei os dados sobre o AMV.");

        investigacaoSindicanciaPage.clicarExpandirEstacionamento();

        estacionamentoSindicanciaPage.validarDados(estacionamentoEditado);
        addScreenshotToReport("Validei os dados sobre o Estacionamento.");

        investigacaoSindicanciaPage.clicarExpandirFormacaoDoTrem();
        formacaoDoTremPage.validarDados(formacaoDoTremEditado);
        addScreenshotToReport("Validei os dados sobre a Formação do Trem.");

        investigacaoSindicanciaPage.clicarExpandirCargas();
        cargasPage.validarDados(cargasEditado);
        addScreenshotToReport("Validei os dados sobre Cargas.");

        investigacaoSindicanciaPage.clicarExpandirMaquinistaCondutor();
        dadosDoMaquinistaCondutorPage.validarDados(dadosDoMaquinistaCondutorEditado);
        addScreenshotToReport("Validei os dados sobre Maquinista ou condutor.");

        investigacaoSindicanciaPage.clicarExpandirSupervisorResponsavel();
        dadosDoSupervisorResponsavelPage.validarDados(dadosDoSupervisorResponsavelEditado);
        addScreenshotToReport("Validei os dados sobre Supervisor Responsavel.");

        investigacaoSindicanciaPage.clicarExpandirManobradorAjudante();
        dadosDoManobradorAjudantePage.validarDados(dadosDoManobradorAjudanteEditado);
        addScreenshotToReport("Validei os dados sobre Manobrador Ajudante.");
    }
}
