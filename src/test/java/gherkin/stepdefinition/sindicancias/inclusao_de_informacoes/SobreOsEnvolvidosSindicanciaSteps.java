package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.sobre_os_envolvidos.VeiculoEnvolvido;
import br.com.api.model.sindicancia.sobre_os_envolvidos.VitimaEnvolvida;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.sobre_os_envolvidos.SobreOsEnvolvidosSindicanciaPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.sobre_os_envolvidos.TestemunhasSobreOsEnvolvidosSindicanciaPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.sobre_os_envolvidos.VeiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.sobre_os_envolvidos.VitimasSobreOsEnvolvidosSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.*;

public class SobreOsEnvolvidosSindicanciaSteps extends ReporterUtils {
    VitimasSobreOsEnvolvidosSindicanciaPage vitimasSobreOsEnvolvidosSindicanciaPage = new VitimasSobreOsEnvolvidosSindicanciaPage();
    TestemunhasSobreOsEnvolvidosSindicanciaPage testemunhasSobreOsEnvolvidosSindicanciaPage = new TestemunhasSobreOsEnvolvidosSindicanciaPage();
    VeiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage = new VeiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage();
    SobreOsEnvolvidosSindicanciaPage sobreOsEnvolvidosSindicanciaPage = new SobreOsEnvolvidosSindicanciaPage();
    VitimaEnvolvida vitimaEnvolvida = getVitimaEnvolvida();
    VitimaEnvolvida vitimaEnvolvidaEditada = getNewVitimaEnvolvida();
    VeiculoEnvolvido veiculoEnvolvido = getVeiculoEnvolvido();
    VeiculoEnvolvido veiculoEnvolvidoEditado = getNewVeiculoEnvolvido();

    @E("valido que estou na página Sobre os envolvidos")
    public void validoQueEstouNaPáginaSobreOsEnvolvidos() {
        vitimasSobreOsEnvolvidosSindicanciaPage.validaExibicaoDaPagina();
        testemunhasSobreOsEnvolvidosSindicanciaPage.validaExibicaoDaPagina();
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.validaExibicaoDaPagina();
        addScreenshotToReport("Validei que á página Sobre os envolvidos está sendo exibida.");
    }

    @Quando("clico no botão Adicionar vítima")
    public void clicoNoBotãoAdicionarVítima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.clicarBtnSimHouveVitimas();
        vitimasSobreOsEnvolvidosSindicanciaPage.clicarBtnAdicionarVitima();
        addLogToReport("Cliquei no botão para adicionar uma nova vítima.");
    }

    @Entao("deve exibir o modal de Adicionar vítima")
    public void deveExibirOModalDeAdicionarVítima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.validaModalDeAdicionarNovaVitima();
        addScreenshotToReport("Validei a exibição do modal de adicionar vítima.");
    }

    @Quando("preencher todos os campos do modal de Adicionar vítima")
    public void preencherTodosOsCamposDoModalDeAdicionarVítima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.preencherModalVitima(vitimaEnvolvida);
        addScreenshotToReport("Preenchi modal de Vítimas.");
    }

    @E("clico no botão Salvar do modal de Adicionar Vítima")
    public void clicoNoBotãoSalvarDoModalDeAdicionarVítima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.clicarBtnSalvarModal();
        addLogToReport("Cliquei no botão 'Salvar' do modal.");
    }

    @E("a vítima deve ser inserida na seção de Vítimas")
    public void aVítimaDeveSerInseridaNaSeçãoDeVítimas() {
        vitimasSobreOsEnvolvidosSindicanciaPage.validaVitimaAdicionada(vitimaEnvolvida, 1);
        addScreenshotToReport("Validei que a vítima está sendo exibida na listagem.");
    }

    @E("a vítima deve ser exibida na seção de Vítimas no modo readonly")
    public void aVítimaDeveSerExibidaNaSeçãoDeVítimasNoModoReadonly() {
        vitimasSobreOsEnvolvidosSindicanciaPage.validaVitimaAdicionadaNoModoReadonly(vitimaEnvolvida, 1);
        addScreenshotToReport("Validei que a vítima está sendo exibida na listagem.");
    }

    @Dado("que existe uma vítima")
    public void queExisteUmaVítima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.validaQueTemVitima();
        addScreenshotToReport("Validei que tem pelo menos uma vítima.");
    }

    @Quando("clicar no botão Editar da vítima {int}")
    public void clicarNoBotãoEditarDaVítima(int linha) {
        vitimasSobreOsEnvolvidosSindicanciaPage.clicarBotaoEditarVitima(linha);
        addLogToReport("Cliquei no botão de editar vítima.");
    }

    @Quando("edito todos os campos do modal de Adicionar vítima")
    public void editoTodosOsCamposDoModalDeAdicionarVítima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.preencherModalVitima(vitimaEnvolvidaEditada);
        addScreenshotToReport("Editei os dados da vítima.");
    }

    @E("a vítima editada deve ser exibida na seção de Vítimas")
    public void aVítimaEditadaDeveSerExibidaNaSeçãoDeVítimas() {
        vitimasSobreOsEnvolvidosSindicanciaPage.validaVitimaAdicionada(vitimaEnvolvidaEditada, 1);
        addScreenshotToReport("Validar dados da vítima editada");
    }

    @Quando("clicar no botão Excluir da vítima {int}")
    public void clicarNoBotãoExcluirDaVítima(int linha) {
        vitimasSobreOsEnvolvidosSindicanciaPage.clicarBotaoExcluirVitima(linha);
        addLogToReport("Cliquei no botão de excluir da vítima.");
    }

    @Quando("confirmo a exclusão da Vítima")
    public void confirmoAExclusãoDaVítima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.clicarBtnSimModalExclusao();
        addLogToReport("Cliquei no botão para confirmar a exclusão");
    }

    @E("a Vítima deve ser excluída")
    public void aVítimaDeveSerExcluída() {
        vitimasSobreOsEnvolvidosSindicanciaPage.validaQueAVitimaFoiExcluida();
        addScreenshotToReport("Validado que a vítima foi excluída.");
    }

    @Dado("que eu adiciono uma observação da seção da vítima")
    public void queEuAdicionoUmaObservacaoDaSecaoDaVítima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.preencherObservacao(vitimaEnvolvida);
        addScreenshotToReport("Preenchi a observação");
    }

    @Quando("clico no botão Salvar Informações da página Sobre os envolvidos")
    public void clicoNoBotãoSalvarInformaçõesDaPaginaSobreOsEnvolvidos() {
        sobreOsEnvolvidosSindicanciaPage.clicarBtnSalvarInformacoes();
        addLogToReport("Cliquei no botão salvar informações.");
    }

    @Entao("deve persistir o observação da seção da vítima")
    public void devePersistirOComentárioDaSeçãoDaVítima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.validaObservacao(vitimaEnvolvida);
        addScreenshotToReport("Validei a observação");
    }

    @Dado("que eu edite a observação da seção da vítima")
    public void queEuEditeAObservacaoDaSecaoDaVitima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.preencherObservacao(vitimaEnvolvidaEditada);
        addScreenshotToReport("Preenchi a observação");
    }

    @E("deve persistir o observação editado da seção da vítima")
    public void devePersistirObservacaoEditadoDaSecaoDaVitima() {
        vitimasSobreOsEnvolvidosSindicanciaPage.validaObservacao(vitimaEnvolvidaEditada);
        addScreenshotToReport("Validei a observação");
    }

    @Quando("clico no botão Adicionar testemunha")
    public void clicoNoBotãoAdicionarTestemunha() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.clicarBtnSimHouveTestemunhas();
        testemunhasSobreOsEnvolvidosSindicanciaPage.clicarBtnAdicionarTestemunha();
        addLogToReport("Cliquei no botão adicionar testemunha.");
    }

    @Entao("deve exibir o modal de Adicionar testemunha")
    public void deveExibirOModalDeAdicionarTestemunha() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.validaModalDeAdicionarNovaTestemunha();
        addScreenshotToReport("Validei a exibição do modal");
    }

    @Quando("preencher todos os campos do modal de Adicionar testemunha")
    public void preencherTodosOsCamposDoModalDeAdicionarTestemunha() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.preencherModalTestemunha(vitimaEnvolvida);
        addScreenshotToReport("Preenchi a informações do modal.");
    }

    @E("clico no botão Salvar do modal de Adicionar Testemunha")
    public void clicoNoBotãoSalvarDoModalDeAdicionarTestemunha() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.clicarBtnSalvarModal();
        addLogToReport("Cliquei no botão Salvar do modal.");
    }

    @E("a testemunha deve ser inserida na seção de Testemunhas")
    public void aTestemunhaDeveSerInseridaNaSeçãoDeTestemunhas() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.validaTestemunhaFoiAdicionada(vitimaEnvolvida, 1);
        addScreenshotToReport("Validei que a Testemunha foi adicionada.");
    }

    @E("a testemunha deve ser exibida na seção de Testemunhas no modo readonly")
    public void aTestemunhaDeveSerExibidaNaSeçãoDeTestemunhasNoModoReadonly() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.validaTestemunhaFoiAdicionadaNoModoReadOnly(vitimaEnvolvida, 1);
        addScreenshotToReport("Validei que a Testemunha é exibida no modo readonly.");
    }

    @Dado("que existe uma testemunha")
    public void queExisteUmaTestemunha() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.validaQueTemTestemunha();
        addScreenshotToReport("Validei que tem testemunha.");
    }

    @Quando("clicar no botão Editar da testemunha {int}")
    public void clicarNoBotãoEditarDaTestemunha(int linha) {
        testemunhasSobreOsEnvolvidosSindicanciaPage.clicarBotaoEditarTestemunha(linha);
        addLogToReport("Cliquei no botão 'Editar'");
    }

    @Quando("edito todos os campos do modal de Adicionar testemunha")
    public void editoTodosOsCamposDoModalDeAdicionarTestemunha() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.preencherModalTestemunha(vitimaEnvolvidaEditada);
        addScreenshotToReport("Editei todos os campos do modal.");
    }

    @E("a testemunha editada deve ser exibida na seção de Testemunhas")
    public void aTestemunhaEditadaDeveSerExibidaNaSeçãoDeTestemunhas() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.validaTestemunhaFoiAdicionada(vitimaEnvolvidaEditada, 1);
        addScreenshotToReport("Validei que a Testemunha foi editada.");
    }

    @Quando("clicar no botão Excluir da testemunha {int}")
    public void clicarNoBotãoExcluirDaTestemunha(int linha) {
        testemunhasSobreOsEnvolvidosSindicanciaPage.clicarBotaoExcluirTestemunha(linha);
        addLogToReport("Cliquei no botão de excluir.");
    }

    @Quando("confirmo a exclusão da Testemunha")
    public void confirmoAExclusãoDaTestemunha() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.clicarBtnSimModalExclusao();
        addLogToReport("Cliquei no botão sim, para excluir a testemunha.");
    }

    @E("a Testemunha deve ser excluída")
    public void aTestemunhaDeveSerExcluída() {
        testemunhasSobreOsEnvolvidosSindicanciaPage.validaQueATestemunhaFoiExcluida();
        addScreenshotToReport("Validado que a testemunha foi excluída.");
    }

    @Quando("clico no botão Adicionar veículo")
    public void clicoNoBotãoAdicionarVeículo() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.clicarBtnSimHouveVeiculos();
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.clicarBtnAdicionarVeiculo();
        addLogToReport("Cliquei no botão Adicionar Veículo.");
    }

    @Entao("deve exibir o modal de Adicionar veículo")
    public void deveExibirOModalDeAdicionarVeículo() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.validaModalDeAdicionarNovaVeiculo();
        addScreenshotToReport("Validei o modal de adicionar veículo");
    }

    @Quando("preencher todos os campos do modal de Adicionar veículo")
    public void preencherTodosOsCamposDoModalDeAdicionarVeículo() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.preencherModalVeiculo(veiculoEnvolvido, false);
        addScreenshotToReport("Preenchi todos os campo do modal.");
    }

    @E("clico no botão Salvar do modal de Adicionar Veículo")
    public void clicoNoBotãoSalvarDoModalDeAdicionarVeículo() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.clicarBtnSalvarModal();
        addLogToReport("Cliquei no botão salvar do modal.");
    }

    @E("o veículo deve ser inserida na seção de Veículos")
    public void aVeículoDeveSerInseridaNaSeçãoDeVeículos() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.validaVeiculoAdicionado(veiculoEnvolvido, 1);
        addScreenshotToReport("Validei a exibição do veículo adicionado.");
    }

    @Dado("que existe uma veículo")
    public void queExisteUmaVeículo() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.validaQueTemVeiculo();
        addScreenshotToReport("Validei que tem um veículo adicionado.");
    }

    @Quando("clicar no botão Editar da veículo {int}")
    public void clicarNoBotãoEditarDaVeículo(int linha) {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.clicarBtnEditarVeiculo(linha);
        addLogToReport("Cliquei no botão editar do veiculo da linha " + linha);
    }

    @Quando("edito todos os campos do modal de Adicionar veículo")
    public void editoTodosOsCamposDoModalDeAdicionarVeículo() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.preencherModalVeiculo(veiculoEnvolvidoEditado, true);
        addScreenshotToReport("Editei todos os campo do modal.");
    }

    @E("o veículo editado deve ser exibido na seção de Veículos")
    public void oVeículoEditadaDeveSerExibidaNaSeçãoDeVeículos() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.validaVeiculoAdicionado(veiculoEnvolvidoEditado, 1);
        addScreenshotToReport("Validei a exibição do veículo editado.");
    }

    @Quando("clicar no botão Excluir da veículo {int}")
    public void clicarNoBotãoExcluirDaVeículo(int linha) {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.clicarBtnExcluirVeiculo(linha);
        addLogToReport("Cliquei no botão de excluir veiculo da linha " + linha);
    }

    @Quando("confirmo a exclusão do Veículo")
    public void confirmoAExclusãoDoVeículo() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.clicarBtnSimModalExclusao();
        addLogToReport("Cliquei no botão sim, para confirmar a exclusão do veículo.");
    }

    @E("o Veículo deve ser excluído")
    public void oVeículoDeveSerExcluído() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.validaQueOVeiculoFoiExcluido();
        addLogToReport("Validado que o veículo foi excluído.");
    }

    @E("o veículo deve ser exibido na seção de Veículos no modo readonly")
    public void oVeículoDeveSerExibidoNaSeçãoDeVeículosNoModoReadonly() {
        veiculosEnvolvidosSobreOsEnvolvidosSindicanciaPage.validaVeiculoAdicionadoReadonly(veiculoEnvolvido, 1);
        addScreenshotToReport("Validei a exibição do veículo readonly.");
    }

}
