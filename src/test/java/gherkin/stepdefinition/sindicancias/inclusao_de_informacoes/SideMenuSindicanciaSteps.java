package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.SideMenuSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;


public class SideMenuSindicanciaSteps extends ReporterUtils {
    SideMenuSindicanciaPage sideMenuSindicanciaPage = new SideMenuSindicanciaPage();

    @E("clico no side-menu CUSTOS")
    public void clicoNoSideMenuCustos() {
        sideMenuSindicanciaPage.clicarMenuCustos();
        addLogToReport("Cliquei no menu custos.");
    }

    @E("clico no side-menu FLAMBAGEM")
    public void clicoNoSideMenuFLAMBAGEM() {
        sideMenuSindicanciaPage.clicarMenuFlambagem();
        addLogToReport("Cliquei no menu Flambagem.");
    }

    @E("clico no side-menu CONCLUSÃO")
    public void clicoNoSideMenuCONCLUSÃO() {
        sideMenuSindicanciaPage.clicarMenuConclusao();
        addLogToReport("Cliquei no menu Conclusão.");
    }

    @E("clico no side-menu TO")
    public void clicoNoSideMenuTO() {
        sideMenuSindicanciaPage.clicarMenuTO();
        addLogToReport("Cliquei no menu TO.");
    }

    @E("clico no side-menu Meio Ambiente")
    public void clicoNoSideMenuMeioAmbiente() {
        sideMenuSindicanciaPage.clicarMenuMeioAmbiente();
        addLogToReport("Cliquei no menu MeioAmbiente.");
    }

    @E("clico no side-menu SST")
    public void clicoNoSideMenuSST() {
        sideMenuSindicanciaPage.clicarMenuSST();
        addLogToReport("Cliquei no menu SST.");
    }

    @E("clico no side-menu Controle de Perdas")
    public void clicoNoSideMenuControleDePerdas() {
        sideMenuSindicanciaPage.clicarMenuControleDePerdas();
        addLogToReport("Cliquei no menu Controle de Perdas.");
    }

    @E("clico no side-menu REGISTRO FOTOGRÁFICO")
    public void clicoNoSideMenuREGISTROFOTOGRÁFICO() {
        sideMenuSindicanciaPage.clicarMenuRegistroFotografico();
        addLogToReport("Cliquei no menu de Registro fotográfico.");
    }

    @E("clico no side-menu DEPOIMENTO")
    public void clicoNoSideMenuDEPOIMENTO() {
        sideMenuSindicanciaPage.clicarMenuDepoimento();
        addLogToReport("Cliquei no menu de Depoimento.");
    }

    @E("clico no side-menu AMV")
    public void clicoNoSideMenuAMV() {
        sideMenuSindicanciaPage.clicarMenuAMV();
        addLogToReport("Cliquei no menu de AMV.");
    }

    @E("clico no side-menu TRILHO")
    public void clicoNoSideMenuTRILHO() {
        sideMenuSindicanciaPage.clicarMenuTrilho();
        addLogToReport("Cliquei no menu de Trilho.");

    }

    @E("clico no side-menu Investigação")
    public void clicoNoSideMenuInvestigacao() {
        sideMenuSindicanciaPage.clicarMenuInvestigacao();
        addLogToReport("Cliquei no menu de Investigação.");
    }

    @E("clico no side-menu LOCOMOTIVA")
    public void clicoNoSideMenuLOCOMOTIVA() {
        sideMenuSindicanciaPage.clicarMenuLocomotiva();
        addLogToReport("Cliquei no menu de Locomotiva.");
    }

    @E("clico no side-menu VAGÕES")
    public void clicoNoSideMenuVAGÕES() {
        sideMenuSindicanciaPage.clicarMenuVagoes();
        addLogToReport("Cliquei no menu de Vagões.");
    }

    @E("clico no side-menu ANÁLISE DE VIAGEM")
    public void clicoNoSideMenuANÁLISEDEVIAGEM() {
        sideMenuSindicanciaPage.clicarMenuAnaliseDeViagem();
        addLogToReport("Cliquei no menu de Análise da viagem.");
    }

    @E("clico no side-menu CRÍTICA DA VIAGEM")
    public void clicoNoSideMenuCRÍTICADAVIAGEM() {
        sideMenuSindicanciaPage.clicarMenuCriticaDaViagem();
        addLogToReport("Cliquei no menu de Crítica da viagem.");
    }

    @E("clico no side-menu Questionário")
    public void clicoNoSideMenuQuestionario() {
        sideMenuSindicanciaPage.clicarMenuQuestionario();
        addLogToReport("Cliquei no menu de Questionário.");
    }

    @E("clico no side-sub-menu Maquinista")
    public void clicoNoSideSubMenuMaquinista() {
        sideMenuSindicanciaPage.clicarSubMenuQuestionarioMaquinista();
        addLogToReport("Cliquei no sub - menu de Maquinista.");
    }

    @E("clico no side-sub-menu Manobrador")
    public void clicoNoSideSubMenuManobrador() {
        sideMenuSindicanciaPage.clicarSubMenuQuestionarioManobrador();
        addLogToReport("Cliquei no sub - menu de Manobrador.");
    }

    @Quando("clico no side-menu Registro do acidente")
    public void clicoNaTABRegistroDoAcidente() {
        sideMenuSindicanciaPage.clicarMenuRegistroAcidente();
        addLogToReport("Cliquei no side menu Registro do acidente");
    }

    @E("clico no side-menu HISTÓRICO")
    public void clicoNoSideMenuHISTÓRICO() {
        sideMenuSindicanciaPage.clicarSideMenuHistorico();
        addLogToReport("Cliquei no menu de Histórico.");
    }

    @Entao("clico no side-menu descrição")
    public void clicoNoSideMenuDescrição() {
        sideMenuSindicanciaPage.clicarSideMenuDescricao();
        addLogToReport("cliquei no sub - menu Descrição");
    }

    @Entao("clico no side-menu Sobre os envolvidos")
    public void clicoNoSideMenuSobreOsEnvolvidos() {
        sideMenuSindicanciaPage.clicarSideMenuSobreOsEnvolvidos();
        addLogToReport("cliquei no sub - menu Sobre os envolvidos");
    }

    @Entao("clico no side-menu sobre o local")
    public void clicoNoSideMenuSobreOLocal() {
        sideMenuSindicanciaPage.clicarSideMenuSobreOLocal();
        addLogToReport("cliquei no sub - menu Sobre o local");
    }

    @Entao("clico no side-menu análise de causa")
    public void sideMenuAnáliseDeCausa() {
        sideMenuSindicanciaPage.clicarSideMenuAnaliseCausa();
        addLogToReport("cliquei no sub - menu Análise de causa");
    }

    @E("clico no side-menu Plano de ação")
    public void clicoNoSideMenuPlanoDeAção() {
        sideMenuSindicanciaPage.clicarSideMenuPlanoDeAcao();
        addLogToReport("cliquei no sub - menu Plano de ação");
    }

    @E("clico no side-menu VIA")
    public void clicoNoSideMenuVIA() {
        sideMenuSindicanciaPage.clicarSideMenuVia();
        addLogToReport("cliquei no sub - menu Via");
    }

    @E("clico no side-menu MECÂNICA")
    public void clicoNoSideMenuMECÂNICA() {
        sideMenuSindicanciaPage.clicarSideMenuMecanica();
        addLogToReport("cliquei no sub - menu MECÂNICA");
    }
}
