package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.api.model.sindicancia.questionario.QuestionarioCheckListAcidente;
import br.com.api.model.sindicancia.questionario.QuestionarioManobrador;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.questionario_maquinista.InformarcoesQuestionariosSindicanciaPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.questionario_maquinista.QuestionarioCheckListAcidenteSindicanciaPage;
import br.com.pom.sro.sindicancias.inclusao_de_informacoes.questionario_maquinista.QuestionarioManobradorSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

import static br.com.api.GeradorDeMassa.*;

public class QuestionarioManobradorSindicanciaSteps extends ReporterUtils {

    InformarcoesQuestionariosSindicanciaPage informarcoesQuestionariosSindicanciaPage = new InformarcoesQuestionariosSindicanciaPage();
    QuestionarioManobradorSindicanciaPage questionarioManobradorSindicanciaPage = new QuestionarioManobradorSindicanciaPage();
    QuestionarioCheckListAcidenteSindicanciaPage questionarioCheckListAcidenteSindicanciaPage = new QuestionarioCheckListAcidenteSindicanciaPage();
    QuestionarioManobrador questionarioManobrador = getQuestionarioManobrador();
    QuestionarioManobrador questionarioManobradorEditado = getNewQuestionarioManobrador();
    QuestionarioCheckListAcidente questionarioCheckListAcidente = getQuestionarioCheckListAcidente();
    QuestionarioCheckListAcidente questionarioCheckListAcidenteEditado = getNewQuestionarioCheckListAcidente();


    @Entao("deve exibir a página de Informações do Manobrador")
    public void deveExibirAPáginaDeInformaçõesDoManobrador() {
        informarcoesQuestionariosSindicanciaPage.validaExibicaoDaPaginaQuestionarioManobrador();
        addScreenshotToReport("Validado exibição da página de Questionário.");
    }

    @E("ao preencher os dados do QUESTIONÁRIO MANOBRADOR")
    public void aoPreencherOsDadosDoQUESTIONÁRIOMANOBRADOR() {
        questionarioManobradorSindicanciaPage.preencherCampos(questionarioManobrador);
        addScreenshotToReport("Preenchi os campos do Questionário do Manobrador.");
    }

    @E("clico no botão Salvar Rascunho do Questionário do Manobrador")
    public void clicoNoBotãoSalvarRascunhoDoQuestionárioDoManobrador() {
        informarcoesQuestionariosSindicanciaPage.clicarBtnSalvarRascunho();
        addLogToReport("Cliquei no botão 'Salvar Rascunho'.");
    }

    @E("valido que os dados do Questionario do Manobrador estão conforme o preenchido")
    public void validoQueOsDadosDoQuestionarioDoManobradorEstãoConformeOPreenchido() {
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioManobrador();
        questionarioManobradorSindicanciaPage.validarDados(questionarioManobrador);
        addScreenshotToReport("Preenchi os campos do Questionário do Manobrador.");
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioCheckListAcidente();
        questionarioCheckListAcidenteSindicanciaPage.validarDados(questionarioCheckListAcidente);
        addScreenshotToReport("Validei os campos do Questionário do Check List.");
    }

    @E("ao preencher os dados do QUESTIONÁRIO CHECK-LIST ACIDENTE do manobrador")
    public void aoPreencherOsDadosDoQUESTIONÁRIOCHECKLISTACIDENTEDoManobrador() {
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioCheckListAcidente();
        questionarioCheckListAcidenteSindicanciaPage.preencherDados(questionarioCheckListAcidente);
        addScreenshotToReport("Preenchi os campos do Questionário do Check List.");
    }

    @Dado("que eu edite os dados do QUESTIONÁRIO MANOBRADOR")
    public void euEditeOsDadosDoQUESTIONÁRIOMANOBRADOR() {
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioManobrador();
        questionarioManobradorSindicanciaPage.preencherCampos(questionarioManobradorEditado);
        addScreenshotToReport("Preenchi os campos do Questionário do Manobrador.");
    }

    @E("edite os dados do QUESTIONÁRIO CHECK-LIST ACIDENTE do manobrador")
    public void editeOsDadosDoQUESTIONÁRIOCHECKLISTACIDENTEDoManobrador() {
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioCheckListAcidente();
        questionarioCheckListAcidenteSindicanciaPage.preencherDados(questionarioCheckListAcidenteEditado);
        addScreenshotToReport("Editei os campos do Questionário do Check List.");
    }

    @E("clico no botão Salvar do Questionário do Manobrador")
    public void clicoNoBotãoSalvarDoQuestionárioDoManobrador() {
        informarcoesQuestionariosSindicanciaPage.clicarBtnSalvar();
    }

    @E("valido que os dados do Questionario do Manobrador estão conforme o editado")
    public void validoQueOsDadosDoQuestionarioDoManobradorEstãoConformeOEditado() {
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioManobrador();
        questionarioManobradorSindicanciaPage.validarDados(questionarioManobradorEditado);
        addScreenshotToReport("Preenchi os campos do Questionário do Manobrador.");
        informarcoesQuestionariosSindicanciaPage.clicarExpandirQuestionarioCheckListAcidente();
        questionarioCheckListAcidenteSindicanciaPage.validarDados(questionarioCheckListAcidenteEditado);
        addScreenshotToReport("Validei os campos do Questionário do Check List.");
    }
}
