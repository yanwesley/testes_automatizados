package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.locomotiva.LocomotivaSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;

public class LocomotivaSindicanciaSteps extends ReporterUtils {

    LocomotivaSindicanciaPage locomotivaSindicanciaPage = new LocomotivaSindicanciaPage();

    @E("estou na página de LOCOMOTIVA")
    public void estouNaPáginaDeLOCOMOTIVA() {
        locomotivaSindicanciaPage.validaQueEstaNaPaginaDeLocomotiva();
        addScreenshotToReport("Valida que o usuário está na página de Locomotiva");
    }


    @E("o botão Salvar Informações da Locomotiva deve estar disabled")
    public void oBotãoSalvarInformaçõesDaLocomotivaDeveEstarDisabled() {
        locomotivaSindicanciaPage.validarBotaoSalvarDisabled();
    }

}
