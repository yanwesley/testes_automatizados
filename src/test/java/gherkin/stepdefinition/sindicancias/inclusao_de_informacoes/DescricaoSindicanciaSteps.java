package gherkin.stepdefinition.sindicancias.inclusao_de_informacoes;

import br.com.pom.sro.sindicancias.inclusao_de_informacoes.DescricaoSindicanciaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

import static br.com.api.GeradorDeMassa.getDescricao;

public class DescricaoSindicanciaSteps extends ReporterUtils {

    DescricaoSindicanciaPage descricaoSindicanciaPage = new DescricaoSindicanciaPage();
    String descricao = getDescricao();

    @E("valido que estou na página de descrição")
    public void validoQueEstouNaPáginaDeDescrição() {
        descricaoSindicanciaPage.validoQueEstouNaPaginaDescricao();
        addLogToReport("validei que estou na página de descrição");
    }

    @Entao("preencho o campo de Descrição")
    public void preenchoOCampoDeDescrição() {
        descricaoSindicanciaPage.preenchoOCampoDescricao(descricao);
        addScreenshotToReport("preenchi o campo descrição");
    }

    @E("clico no botão Salvar e Avançar da tela de Descrição")
    public void clicoNoBotãoSalvarEAvançarDaTelaDeDescrição() {
        descricaoSindicanciaPage.clicarbtnSalvarEAvancar();
        addLogToReport("cliquei em salvar e avançar");
    }


    @E("valido que a descrição persistiu")
    public void validoQueADescriçãoPersistiu() {
        descricaoSindicanciaPage.validoDescricao(descricao);
        addScreenshotToReport("Validei que as informações preenchidas persistiram");
    }

    @E("clico no botão Salvar da tela de Descrição")
    public void clicoNoBotãoSalvarDaTelaDeDescrição() {
        descricaoSindicanciaPage.clicarBtnSalvar();
        addLogToReport("Cliquei em salvar");
    }

    @E("valido que a descrição é exibida no modo readonly")
    public void validoQueADescriçãoÉExibidaNoModoReadonly() {
        descricaoSindicanciaPage.validoDescricaoReadonly(descricao);
        addScreenshotToReport("Validei que as informações preenchidas persistiram");
    }
}
