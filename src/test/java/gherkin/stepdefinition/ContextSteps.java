package gherkin.stepdefinition;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.incidente.Incidente;
import br.com.api.model.sindicancia.RelatorioFinal;
import br.com.api.model.sinistro.*;
import br.com.pom.sro.HeaderMenuPage;
import br.com.pom.sro.sinistro.SinistrosListagemPage;
import br.com.pom.sro.sinistro.adicionar_infomacoes.MaquinasRecursosAcionadosPage;
import br.com.pom.sro.sinistro.adicionar_infomacoes.ResponsaveisAdicionaisPage;
import br.com.pom.sro.sinistro.adicionar_infomacoes.TurmasRecursosAcionadosPage;
import br.com.pom.sro.sinistro.novo_sinistro.FormularioNovoSinistroPage;
import br.com.pom.sro.sinistro.novo_sinistro.liberacao.AbaLiberacaoPage;
import br.com.pom.sro.sinistro.novo_sinistro.sinistro.*;
import br.com.pom.sro.sinistro.novo_sinistro.tipo_entrada.EntradaMacroEOsPage;
import br.com.pom.sro.sinistro.novo_sinistro.tipo_entrada.EscolherTipoDeEntradaNovoSinistroPage;
import br.com.pom.sro.sinistro.novo_sinistro.veiculos.VeiculosNovoSinistroPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Quando;
import org.testng.Assert;

import static br.com.api.GeradorDeMassa.*;
import static br.com.pom.api.AcaoAPI.*;
import static br.com.pom.api.IncidenteAPI.criarIncidente;
import static br.com.pom.api.PlanoDeAcaoAPI.preencherAnaliseDeCausas;
import static br.com.pom.api.SindicanciaAPI.*;
import static br.com.pom.api.SinistroAPI.*;

public class ContextSteps extends ReporterUtils {
    HeaderMenuPage headerMenuPage = new HeaderMenuPage();
    SinistrosListagemPage sinistrosRegistrosPage = new SinistrosListagemPage();
    EscolherTipoDeEntradaNovoSinistroPage escolherTipoDeEntradaNovoSinistroPage = new EscolherTipoDeEntradaNovoSinistroPage();
    EntradaMacroEOsPage entradaMacroEOsPage = new EntradaMacroEOsPage();
    SobreOSinistroNovoSinistroPage sobreOSinistroNovoSinistroPage = new SobreOSinistroNovoSinistroPage();
    SobreOLocalNovoSinistroPage sobreOLocalNovoSinistroPage = new SobreOLocalNovoSinistroPage();
    SobreOTremNovoSinistroPage sobreOTremNovoSinistroPage = new SobreOTremNovoSinistroPage();
    SobreOCondutorOuMaquinistaNovoSinistroPage sobreOCondutorOuMaquinistaNovoSinistroPage = new SobreOCondutorOuMaquinistaNovoSinistroPage();
    AdicionarVitimasNovoSinistroPage adicionarVitimasNovoSinistroPage = new AdicionarVitimasNovoSinistroPage();
    AdicionarVeiculosTerceiroNovoSinistroPage adicionarVeiculosTerceiroSobreNovoSinistroPage = new AdicionarVeiculosTerceiroNovoSinistroPage();
    OutrasInformacoesNovoSinistroPage outrasInformacoesNovoSinistroPage = new OutrasInformacoesNovoSinistroPage();
    FormularioNovoSinistroPage formularioNovoSinistroPage = new FormularioNovoSinistroPage();
    VeiculosNovoSinistroPage veiculosNovoSinistroPage = new VeiculosNovoSinistroPage();
    AdicionarVeiculosTerceiroNovoSinistroPage adicionarVeiculosTerceiroNovoSinistroPage = new AdicionarVeiculosTerceiroNovoSinistroPage();
    AbaLiberacaoPage abaLiberacaoPage = new AbaLiberacaoPage();
    MaquinasRecursosAcionadosPage maquinasRecursosAcionadosPage = new MaquinasRecursosAcionadosPage();
    ResponsaveisAdicionaisPage responsaveisAdicionaisPage = new ResponsaveisAdicionaisPage();
    TurmasRecursosAcionadosPage turmasRecursosAcionadosPage = new TurmasRecursosAcionadosPage();

    Veiculo veiculo = getVeiculo();
    Sinistro sinistro = getSinistro();
    AbaLiberacao abaLiberacao = getAbaLiberacao();
    MaquinaAcionada maquinaAcionada = getMaquinaAcionada();
    TurmaAcionada turmaAcionada = getTurmaAcionada();
    ResponsavelAcionado responsavelAcionado = getResponsavelAcionado();
    RelatorioFinal relatorioFinal = getRelatorioFinal();
    PlanoDeAcao planoDeAcao = getPlanoDeAcao();
    Acao acao = getAcao();
    Incidente incidente = getIncidente();

    @E("cadastro um novo Sinistro manual com Veículo na linha {int}")
    public void cadastroUmNovoSinistroComVeículo(int linha) {

        headerMenuPage.clicarTABSinistros();
        addLogToReport("Cliquei na TAB Sinistros");
        sinistrosRegistrosPage.clicarBtnIncluirNovoSinistro();
        addLogToReport("Clicou no botão 'Incluir novo sinistros'.");
        escolherTipoDeEntradaNovoSinistroPage.clicarBtnDadosManuais();
        addLogToReport("Cliquei no botão:  'Dados Manuais'");

        //Sinistro
        sobreOSinistroNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o sinistro");
        sobreOLocalNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o trem");
        sobreOTremNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o trem");
        sobreOCondutorOuMaquinistaNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre O CONDUTOR OU MAQUINISTA");
        adicionarVitimasNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados ADICIONAR VÍTIMAS");
        adicionarVeiculosTerceiroSobreNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados ADICIONAR VEÍCULOS DE TERCEIROS");
        outrasInformacoesNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados OUTRAS INFORMAÇÕES");

        formularioNovoSinistroPage.clicarNoBotaoSalvarEAvancar();
        addLogToReport("Clicou no botão Salvar e Avançar");

        //Veículo
        veiculosNovoSinistroPage.clicarBtnAdicionarCampoVazio();
        addLogToReport("Cliquei no botão para adicionar campo vazio.");
        Assert.assertEquals(veiculosNovoSinistroPage.getCountLinesOfTheTable(), 1);
        veiculosNovoSinistroPage.validarQueFoiIncluidaUmaNovaLinha();
        addScreenshotToReport("Valido que foi incluído uma nova linha.");
        veiculosNovoSinistroPage.validarCamposVaziosDaLinha();
        addScreenshotToReport("Validou que os campos estão vazios.");
        veiculosNovoSinistroPage.preencherCampos(linha - 1, veiculo);
        addScreenshotToReport("Preenchido campos do veículo.");
        veiculosNovoSinistroPage.clicarBtnSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar Informações");
        adicionarVeiculosTerceiroNovoSinistroPage.validaMensagemSnackBar("Informações salvas com sucesso.");
        addScreenshotToReport("Validando mensagem snackBar.");
    }

    @E("cadastro um novo Sinistro manual com Veículo na linha {int} e datas de liberação")
    public void cadastroUmNovoSinistroComVeículoEDatasLiberacao(int linha) {

        headerMenuPage.clicarTABSinistros();
        addLogToReport("Cliquei na TAB Sinistros");
        sinistrosRegistrosPage.clicarBtnIncluirNovoSinistro();
        addLogToReport("Clicou no botão 'Incluir novo sinistros'.");
        escolherTipoDeEntradaNovoSinistroPage.clicarBtnDadosManuais();
        addLogToReport("Cliquei no botão:  'Dados Manuais'");

        //Sinistro
        sobreOSinistroNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o sinistro");
        sobreOLocalNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o trem");
        sobreOTremNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o trem");
        sobreOCondutorOuMaquinistaNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre O CONDUTOR OU MAQUINISTA");
        adicionarVitimasNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados ADICIONAR VÍTIMAS");
        adicionarVeiculosTerceiroSobreNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados ADICIONAR VEÍCULOS DE TERCEIROS");
        outrasInformacoesNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados OUTRAS INFORMAÇÕES");

        formularioNovoSinistroPage.clicarNoBotaoSalvarEAvancar();
        addLogToReport("Clicou no botão Salvar e Avançar");

        //Veículo
        veiculosNovoSinistroPage.clicarBtnAdicionarCampoVazio();
        addLogToReport("Cliquei no botão para adicionar campo vazio.");
        Assert.assertEquals(veiculosNovoSinistroPage.getCountLinesOfTheTable(), 1);
        veiculosNovoSinistroPage.validarQueFoiIncluidaUmaNovaLinha();
        addScreenshotToReport("Valido que foi incluído uma nova linha.");
        veiculosNovoSinistroPage.validarCamposVaziosDaLinha();
        addScreenshotToReport("Validou que os campos estão vazios.");
        veiculosNovoSinistroPage.preencherCampos(linha - 1, veiculo);
        addScreenshotToReport("Preenchido campos do veículo.");
        veiculosNovoSinistroPage.clicarBtnSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar Informações");
        adicionarVeiculosTerceiroNovoSinistroPage.validaMensagemSnackBar("Informações salvas com sucesso.");
        addScreenshotToReport("Validando mensagem snackBar.");

        //Liberacao
        formularioNovoSinistroPage.clicaNaTabLiberacao();

        abaLiberacaoPage.preencheSecaoPrevisaoDeLiberacao(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Previsão da Liberação");
        abaLiberacaoPage.preencheSecaoDeLiberacaoParcial(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Parcial");
        abaLiberacaoPage.preencheSecaoDeLiberacaoMecanica(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Mecânica");
        abaLiberacaoPage.preencheSecaoDeLiberacaoVia(abaLiberacao);
        addScreenshotToReport("Preencheu a seção de Liberação Via");

        abaLiberacaoPage.clicarBtnSalvarInformacoes();
        adicionarVeiculosTerceiroNovoSinistroPage.validaMensagemSnackBar("Informações salvas com sucesso.");

    }

    @Quando("cadastro um novo Sinistro manual sem Veículo e Liberação")
    public void cadastroUmNovoSinistroManualSemVeículo() {
        headerMenuPage.clicarTABSinistros();
        addLogToReport("Cliquei na TAB Sinistros");
        sinistrosRegistrosPage.clicarBtnIncluirNovoSinistro();
        addLogToReport("Clicou no botão 'Incluir novo sinistros'.");
        escolherTipoDeEntradaNovoSinistroPage.clicarBtnDadosManuais();
        addLogToReport("Cliquei no botão:  'Dados Manuais'");

        //Sinistro
        sobreOSinistroNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o sinistro");
        sobreOLocalNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o trem");
        sobreOTremNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o trem");
        sobreOCondutorOuMaquinistaNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre O CONDUTOR OU MAQUINISTA");
        adicionarVitimasNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados ADICIONAR VÍTIMAS");
        adicionarVeiculosTerceiroSobreNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados ADICIONAR VEÍCULOS DE TERCEIROS");
        outrasInformacoesNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados OUTRAS INFORMAÇÕES");
        formularioNovoSinistroPage.clicarNoBotaoSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar Informações");
        adicionarVeiculosTerceiroNovoSinistroPage.validaMensagemSnackBar("Sinistro salvo com sucesso.");
    }


    @Quando("cadastro um novo Sinistro via Macro sem Veículo e Liberação")
    public void cadastroUmNovoSinistroViaMacroSemVeículo() {
        headerMenuPage.clicarTABSinistros();
        addLogToReport("Cliquei na TAB Sinistros");
        sinistrosRegistrosPage.clicarBtnIncluirNovoSinistro();
        addLogToReport("Clicou no botão 'Incluir novo sinistros'.");


        escolherTipoDeEntradaNovoSinistroPage.clicarBtnNumeroDaMacro();
        addLogToReport("Cliquei no botão:  'Número da Macro'");

        entradaMacroEOsPage.selecionarMacro(1);
        addScreenshotToReport("Selecionou a linha " + 1 + "de macro e OS");

        entradaMacroEOsPage.clicarNoBtnSelecionar();
        addLogToReport("Clicou no botão Selecionar");

        //Sinistro
        sobreOSinistroNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o sinistro");
        sobreOLocalNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o trem");
        sobreOTremNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre o trem");
        sobreOCondutorOuMaquinistaNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados sobre O CONDUTOR OU MAQUINISTA");
        adicionarVitimasNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados ADICIONAR VÍTIMAS");
        adicionarVeiculosTerceiroSobreNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados ADICIONAR VEÍCULOS DE TERCEIROS");
        outrasInformacoesNovoSinistroPage.preencherCampos(sinistro);
        addScreenshotToReport("Preenchi os dados OUTRAS INFORMAÇÕES");
        formularioNovoSinistroPage.clicarNoBotaoSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar Informações");
        adicionarVeiculosTerceiroNovoSinistroPage.validaMensagemSnackBar("Sinistro salvo com sucesso.");
    }

    @E("preencho os dados do Sinistro com o CCO VIA")
    public void preenchoOsDadosDoSinistroComOCCOVIA() {
        headerMenuPage.clicarTABSinistros();
        addLogToReport("Cliquei na TAB Sinistros");
        sinistrosRegistrosPage.clicarBntEditarSinistroOrdenadoDecrescente();
        addLogToReport("no botão Editar do sinistro");
        //Responsável
        responsaveisAdicionaisPage.selecionarRadioButtonSim();
        responsaveisAdicionaisPage.clicarBtnAdicionarResponsaveis();
        addLogToReport("Clicou no botão Adicionar Responsável");
        responsaveisAdicionaisPage.preencherModalAcionamentoResponsavel(responsavelAcionado);
        addScreenshotToReport("Preencheu o modal de Acionamento de Responsável");
        responsaveisAdicionaisPage.clicarBtnSalvar();
        //Maquina
        maquinasRecursosAcionadosPage.selecionarRadioButtonMaquinasForamAcionadasSim();
        maquinasRecursosAcionadosPage.clicarBtnAdicionarMaquina();
        addLogToReport("Cliquei no botão 'Adicionar Máquina.'");
        maquinasRecursosAcionadosPage.preencherModalAcionamentoDeMaquina(maquinaAcionada);
        addScreenshotToReport("Preenchi a modal de adicionar máquina");
        maquinasRecursosAcionadosPage.clicarBtnSalvarGenerico();
        //Turma
        turmasRecursosAcionadosPage.selecionarRadioButtonTurmasForamAcionadasSim();
        turmasRecursosAcionadosPage.clicarBtnAdicionarTurma();
        addLogToReport("Cliquei no botão 'Adicionar Turma.'");
        turmasRecursosAcionadosPage.preencherModalAcionamentoDeTurmas(turmaAcionada);
        addScreenshotToReport("Preenchi a modal de Turmas acionadas");
        turmasRecursosAcionadosPage.clicarBtnSalvarGenerico();

        sinistrosRegistrosPage.clicarBtnSalvarEEncerrarGenerico();
        sinistrosRegistrosPage.clicarBtnConfirmar();

        adicionarVeiculosTerceiroNovoSinistroPage.validaMensagemSnackBar("O sinistro foi encerrado com sucesso.");
    }

    @Dado("que eu crie um novo sinistro sem veiculos via API")
    public void queEuCrieUmNovoSinistroSemVeiculosViaAPI() {
            criarSinistro(sinistro);
    }

    @Dado("que eu crie um novo sinistro com veiculos via API")
    public void queEuCrieUmNovoSinistroComVeiculosViaAPI() {
        criarSinistro(sinistro);
        adicionarVeiculoAoSinistro(sinistro, veiculo);
    }

    @E("adiciono um VAGÃO via API")
    public void adicionoVAGÃOViaAPI() {
        adicionarVagaoAoSinistro(sinistro, veiculo);
    }

    @E("adiciono uma LOCOMOTIVA via API")
    public void adicionoUmaLOCOMOTIVAViaAPI() {
        adicionarLocomotivaAoSinistro(sinistro, veiculo);
    }

    @E("concluo a áreas da sindicância via API")
    public void concluoAÁreasDaSindicânciaViaAPI() {
        concluirAreasSindicancia(sinistro, getConclusao());
    }

    @E("preencho o relatório final da CIPIA Técnica da sindicância via API")
    public void preenchoORelatórioFinalDaCIPIATécnicaDaSindicânciaViaAPI() {
        preencherRelatorioFinalSindicancia(sinistro, relatorioFinal);
    }

    @E("preencho a descrição da Segurança Patrimonial da sindicância via API")
    public void preenchoADescriçãoDaSegurançaPatrimonialDaSindicânciaViaAPI() {
        preencherDescricaoSegurancaPatrimonial(sinistro, getDescricao());
    }

    @Dado("que eu crie um novo plano de ação via API")
    public void queEuCrieUmNovoPlanoDeAçãoViaAPI() {
        criarSinistro(sinistro);
        concluirAreasSindicancia(sinistro, getConclusao());
        preencherRelatorioFinalSindicancia(sinistro, relatorioFinal);
        preencherAnaliseDeCausas(sinistro, relatorioFinal, planoDeAcao);
    }

    @Dado("que eu crie uma nova ação via API")
    public void queEuCrieUmaNovaAçãoViaAPI() {
        criarSinistro(sinistro);
        concluirAreasSindicancia(sinistro, getConclusao());
        preencherDescricaoSegurancaPatrimonial(sinistro, getDescricao());
        preencherRelatorioFinalSindicancia(sinistro, relatorioFinal);
        preencherAnaliseDeCausas(sinistro, relatorioFinal, planoDeAcao);
        preencherConclusaoCipiaTecnica(sinistro, getConclusao());
        preencherAcao(sinistro, planoDeAcao, acao, getResponsavelAcao());
    }

    @E("aceito a ação via API")
    public void aceitoAAçãoViaAPI() {
        aceitarAcao(acao);
    }

    @E("anulo a ação via API")
    public void anuloAAçãoViaAPI() {
        anularAcao(acao);
    }

    @Dado("que eu crie {int} nova\\(s) ação\\(es) via API")
    public void queEuCrieNovaSAçãoEsViaAPI(int quantidade) {
        for (int i = 0; i < quantidade; i++) {
            queEuCrieUmaNovaAçãoViaAPI();
        }
    }

    @E("adiciono um VEÍCULO DE VIA via API")
    public void adicionoUmVEÍCULODEVIAViaAPI() {
        adicionarVeiculoDeViaAoSinistro(sinistro, veiculo);
    }

    @Dado("que eu crie um novo incidente via API")
    public void queEuCrieUmNovoIncidenteViaAPI() {
        criarIncidente(incidente, veiculo);
    }

    @Dado("que eu navegue até o Administração")
    public void queEuNavegueAtéOAdministração() {
        resetDriver(urlAdministracao);
        addLogToReport("Naveguei até o administração.");
    }
}
