package gherkin.stepdefinition.incidentes;

import br.com.api.model.incidente.Incidente;
import br.com.pom.sro.incidentes.AreaResponsavelIncidentesPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.getIncidente;

public class AreaResponsavelIncidentesSteps extends ReporterUtils {

    AreaResponsavelIncidentesPage areaResponsavelIncidentesPage = new AreaResponsavelIncidentesPage();
    Incidente incidente = getIncidente();

    @E("preencho os dados da área responsavel do incidente")
    public void preenchoOsDadosDaÁreaResponsavelDoIncidente() {
        areaResponsavelIncidentesPage.preencherCampos(incidente);
        addLogToReport("Preenchi os dados da área responsável.");
    }

    @Então("salvo os dados da área responsavel do incidente")
    public void salvoOsDadosDaÁreaResponsavelDoIncidente() {
        areaResponsavelIncidentesPage.clicarBtnSalvarInformacoes();
        addLogToReport("Cliquei no botão 'Salvar informações'.");
    }

    @E("valido que os dados da área responsavel do incidente persistiram")
    public void validoQueOsDadosDaÁreaResponsavelDoIncidentePersistiram() {
        areaResponsavelIncidentesPage.validarDadosPreenchidos(incidente);
        addScreenshotToReport("Validei os dados preenchidos.");
    }
}
