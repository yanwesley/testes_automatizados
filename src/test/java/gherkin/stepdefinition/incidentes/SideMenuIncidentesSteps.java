package gherkin.stepdefinition.incidentes;

import br.com.pom.sro.incidentes.SideMenuIncidentePage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class SideMenuIncidentesSteps extends ReporterUtils {

    SideMenuIncidentePage sideMenuIncidentePage = new SideMenuIncidentePage();

    @E("clico no side-menu ANEXO de Incidentes")
    public void clicoNoSideMenuANEXO() {
        sideMenuIncidentePage.clicarMenuAnexos();
        addLogToReport("Clicou no side-menu ANEXO");
    }

    @Entao("clico no side-menu HISTÓRICO de Incidentes")
    public void clicoNoSideMenuHISTÓRICODeIncidentes() {
        sideMenuIncidentePage.clicarMenuHistorico();
        addLogToReport("Clicou no side-menu HISTÓRICO");
    }

    @E("clico no side-menu Área responsável de Incidentes")
    public void clicoNoSideMenuÁreaResponsávelDeIncidentes() {
        sideMenuIncidentePage.clicarMenuAreaResponsável();
        addLogToReport("Clicou no side-menu Área responsável");
    }

    @E("clico no side-menu análise de causa de incidentes")
    public void clicoNoSideMenuAnáliseDeCausaDeIncidentes() {
        sideMenuIncidentePage.clicarMenuAnaliseCausas();
        addLogToReport("cliquei no side-menu Análise de causa");
    }

    @Quando("clico no side-menu Plano de ação de incidentes")
    public void clicoNoSideMenuPlanoDeAçãoDeIncidentes() {
        sideMenuIncidentePage.clicarMenuPlanoAcao();
        addLogToReport("cliquei no side-menu Plano de ação");
    }
}
