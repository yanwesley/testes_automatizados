package gherkin.stepdefinition.incidentes;

import br.com.api.model.incidente.Incidente;
import br.com.api.model.sinistro.Veiculo;
import br.com.pom.sro.incidentes.DadosIniciaisIncidentesPage;
import br.com.pom.sro.incidentes.SideMenuIncidentePage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.*;

public class DadosIniciaisIncidenteSteps extends ReporterUtils {

    SideMenuIncidentePage sideMenuIncidentePage = new SideMenuIncidentePage();
    DadosIniciaisIncidentesPage dadosIniciaisIncidentesPage = new DadosIniciaisIncidentesPage();
    Incidente incidente = getIncidente();
    Incidente incidenteEditado = getNewIncidente();
    Veiculo veiculo = getVeiculo();
    Veiculo veiculoEditado = getNewVeiculo();
    String justificativa = getConclusao();
    boolean isEdicao;


    @E("clico no side-menu Dados Iniciais de Incidentes")
    public void clicoNoSideMenuDadosIniciais() {
        sideMenuIncidentePage.clicarMenuDadosIniciais();
        addLogToReport("Clicou no side-menu Dados Iniciais");
    }

    @E("estou na página de Dados Iniciais de Incidentes")
    public void estouNaPáginaDeDadosIniciais() {
        dadosIniciaisIncidentesPage.validaQueEstaNaPaginaDeDadosIniciais();
        addScreenshotToReport("Valida que o usuário está na página de Dados Iniciais");
    }

    @E("ao preencher os dados iniciais do incidente")
    public void aoPreencherOsDadosIniciaisDoIncidente() {
        isEdicao = false;
        dadosIniciaisIncidentesPage.preencherSobreOIncidente(incidente, isEdicao);
        addScreenshotToReport("Preenchi os dados sobre o Incidente");
        dadosIniciaisIncidentesPage.preencherSobreOLocal(incidente);
        addScreenshotToReport("Preenchi os dados sobre o Local do Incidente");
        dadosIniciaisIncidentesPage.preencherSobreOTrem(incidente);
        addScreenshotToReport("Preenchi os dados sobre o Trem do Incidente");
        dadosIniciaisIncidentesPage.preencherSobreAtivosEnvolvidos(incidente, veiculo, isEdicao);
        addScreenshotToReport("Preenchi os dados de Ativos Envolvidos");
    }

    @E("clicar no Salvar e Abrir Incidente")
    public void clicarNoSalvarEAbrirIncidente() {
        dadosIniciaisIncidentesPage.clicarBtnSalvarEAbrirIncidente();
        addLogToReport("Cliquei no botão 'Salvar e Abrir Incidente'");
    }

    @E("clico em salvar informações na investigação do incidente")
    public void clicoEmSalvarInformacoesNaInvestigacaoDoIncidente() {
        dadosIniciaisIncidentesPage.clicarBtnSalvarInvestigacao();
        addLogToReport("Cliquei no botão Salvar Investigação");
    }

    @Entao("confirmo a aprovação da investigação")
    public void confirmoAAprovaçãoDaInvestigação() {
        dadosIniciaisIncidentesPage.clicarBtnAprovarModal();
        addLogToReport("Cliquei no botão aprovar");
    }

    @Entao("valido que a investigação foi aprovada")
    public void validoQueAInvestigaçãoFoiAprovada() {
        dadosIniciaisIncidentesPage.validarQueInvestigacaoAprovada();
        addScreenshotToReport("validei que a investigação foi aprovada");
    }

    @E("clico em reprovar a investigação do incidente")
    public void clicoEmReprovarAInvestigaçãoDoIncidente() {
        dadosIniciaisIncidentesPage.clicarBtnReprovarInvestigação();
        addLogToReport("Cliquei no botão reprovar Investigação");
    }

    @Entao("confirmo a reprovação da investigação")
    public void confirmoAReprovaçãoDaInvestigação() {
        dadosIniciaisIncidentesPage.preencherModalReprovação(justificativa);
        addScreenshotToReport("preenchi a justificativa de reprovação");
        dadosIniciaisIncidentesPage.clicarEmConfirmarModalReprovacao();
        addLogToReport("Cliquei no botão confirmar do modal de reprovação");
    }

    @Entao("valido que a investigação foi reprovada")
    public void validoQueAInvestigaçãoFoiReprovada() {
        dadosIniciaisIncidentesPage.validarQueInvestigacaoReprovada();
        addScreenshotToReport("validei que a investigação foi reprovada");
    }

    @E("clico em Solicitar Anulação do incidente")
    public void clicoEmSolicitarAnulaçãoDoIncidente() {
        dadosIniciaisIncidentesPage.clicarBtnSolicitarAnulacao();
        addLogToReport("Cliquei no botão Solicitar Anulação");
    }

    @E("preencho os dados do modal de Solicitar Anulação do incidente")
    public void preenchoOsDadosDoModalDeSolicitarAnulaçãoDoIncidente() {
        dadosIniciaisIncidentesPage.preencherModalSolicitarAnulacao(justificativa);
        addScreenshotToReport("Preenchi os dados do modal de Solicitar Anulação.");
    }

    @Entao("confirmo o modal de Solicitar Anulação do incidente")
    public void confirmoOModalDeSolicitarAnulaçãoDoIncidente() {
        dadosIniciaisIncidentesPage.clicarBtnConfirmarModalSolicitarAnulacao();
        addLogToReport("Cliquei no botão confirmar.");
    }


    @E("clico em anular a investigação do incidente")
    public void clicoEmAnularAInvestigaçãoDoIncidente() {
        dadosIniciaisIncidentesPage.clicarBtnAnularInvestigacao();
        addLogToReport("Cliquei no botão anular");
    }

    @Entao("confirmo a anulação da investigação")
    public void confirmoAAnulaçãoDaInvestigação() {
        dadosIniciaisIncidentesPage.clicarBtnAnularModal();
        addLogToReport("Cliquei no botão confirmar anulação");
    }

    @Entao("valido que a investigação foi anulada")
    public void validoQueAInvestigaçãoFoiAnulada() {
        dadosIniciaisIncidentesPage.validarQueInvestigacaoFoiAnulada();
        addScreenshotToReport("validei que a investigação foi anulada");
    }

    @E("clico em reabrir incidente")
    public void clicoEmReabrirIncidente() {
        dadosIniciaisIncidentesPage.clicarBtnReabrirIncidente();
        addLogToReport("Cliquei no botão reabrir");
    }

    @Entao("preencho a justificativa de reabertura")
    public void preenchoAJustificativaDeReabertura() {
        dadosIniciaisIncidentesPage.preencherModalReabertura(justificativa);
        addScreenshotToReport("Preenchi a justificativa para reabertura");
        dadosIniciaisIncidentesPage.clicarBtnConfirmarReabertura();
        addLogToReport("Cliquei no botão confirmar");
    }

    @Entao("valido que a investigação foi reaberta")
    public void validoQueAInvestigaçãoFoiReaberta() {
    }

    @Entao("valida os dados preenchidos do Incidente")
    public void validaOsDadosPreenchidosDoIncidente() {
        isEdicao = false;
        dadosIniciaisIncidentesPage.validaSobreOIncidente(incidente, isEdicao);
        addScreenshotToReport("Valida os dados preenchidos sobre o Incidente");
        dadosIniciaisIncidentesPage.validaSobreOLocal(incidente);
        addScreenshotToReport("Valida os dados preenchidos sobre o Local do Incidente");
        dadosIniciaisIncidentesPage.validaSobreOTrem(incidente);
        addScreenshotToReport("Valida os dados preenchidos sobre o Trem do Incidente");
        dadosIniciaisIncidentesPage.validaSobreAtivosEnvolvidos(incidente, veiculo);
        addScreenshotToReport("Valida os dados preenchidos de Ativos Envolvidos");
    }

    @E("edito os dados do incidente")
    public void editoOsDadosDoIncidente() {
        isEdicao = true;
        dadosIniciaisIncidentesPage.preencherSobreOIncidente(incidenteEditado, isEdicao);
        addScreenshotToReport("Preenchi os dados sobre o Incidente");
        dadosIniciaisIncidentesPage.preencherSobreOLocal(incidenteEditado);
        addScreenshotToReport("Preenchi os dados sobre o Local do Incidente");
        dadosIniciaisIncidentesPage.preencherSobreOTrem(incidenteEditado);
        addScreenshotToReport("Preenchi os dados sobre o Trem do Incidente");
        dadosIniciaisIncidentesPage.preencherSobreAtivosEnvolvidos(incidenteEditado, veiculoEditado, isEdicao);
        addScreenshotToReport("Preenchi os dados de Ativos Envolvidos");
    }

    @Então("valido os dados editados do incidente")
    public void validoOsDadosEditadosDoIncidente() {
        isEdicao = true;
        dadosIniciaisIncidentesPage.validaSobreOIncidente(incidenteEditado, isEdicao);
        addScreenshotToReport("Valida os dados preenchidos sobre o Incidente");
        dadosIniciaisIncidentesPage.validaSobreOLocal(incidenteEditado);
        addScreenshotToReport("Valida os dados preenchidos sobre o Local do Incidente");
        dadosIniciaisIncidentesPage.validaSobreOTrem(incidenteEditado);
        addScreenshotToReport("Valida os dados preenchidos sobre o Trem do Incidente");
        dadosIniciaisIncidentesPage.validaSobreAtivosEnvolvidos(incidenteEditado, veiculoEditado);
        addScreenshotToReport("Valida os dados preenchidos de Ativos Envolvidos");
    }

    @Entao("valida os dados preenchidos do Incidente ReadOnly")
    public void validaOsDadosPreenchidosDoIncidenteReadOnly() {
        dadosIniciaisIncidentesPage.validaSobreOIncidenteReadOnly(incidente);
        addScreenshotToReport("Valida os dados preenchidos sobre o Incidente ReadOnly");
        dadosIniciaisIncidentesPage.validaSobreOLocalReadOnly(incidente);
        addScreenshotToReport("Valida os dados preenchidos sobre o Local do Incidente ReadOnly");
        dadosIniciaisIncidentesPage.validaSobreOTremReadOnly(incidente);
        addScreenshotToReport("Valida os dados preenchidos sobre o Trem do Incidente ReadOnly");
        dadosIniciaisIncidentesPage.validaSobreAtivosEnvolvidosReadOnly(incidente, veiculo);
        addScreenshotToReport("Valida os dados preenchidos de Ativos Envolvidos ReadOnly");
    }

    @Dado("clico nos botões de editar da tele de incidente")
    public void clicoNosBotõesDeEditarDaTeleDeIncidente() {
        dadosIniciaisIncidentesPage.habilitarEdicaoDaPaginaDeIncidente();
    }

    @E("clico em aprovar a investigação do incidente")
    public void clicoEmAprovarAInvestigaçãoDoIncidente() {
        dadosIniciaisIncidentesPage.clicarBtnAprovarInvestigacao();
        addLogToReport("Cliquei no botão 'Aprovar Investigação'");
    }
}
