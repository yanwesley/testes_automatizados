package gherkin.stepdefinition.incidentes;

import br.com.api.model.incidente.Incidente;
import br.com.api.model.sindicancia.AnaliseDeCausa;
import br.com.pom.sro.incidentes.AnaliseDeCausaIncidentePage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.*;

public class AnaliseDeCausaIncidenteSteps extends ReporterUtils {
    AnaliseDeCausaIncidentePage analiseDeCausaIncidentePage = new AnaliseDeCausaIncidentePage();
    AnaliseDeCausa analiseDeCausaIncidente = getAnaliseDeCausa();
    AnaliseDeCausa analiseDeCausaIncidenteEditada = getNewAnaliseDeCausa();
    Incidente incidente = getIncidente();

    @Entao("preencho a tela de Análise de causas de incidentes")
    public void preenchoATelaDeAnáliseDeCausasDeIncidentes() {
        analiseDeCausaIncidentePage.preencheFatosConstatados(analiseDeCausaIncidente);
        addScreenshotToReport("Preencheu Fatos constatados");
        analiseDeCausaIncidentePage.preencheFatoOcorrido(analiseDeCausaIncidente);
        addScreenshotToReport("Preencheu Fato ocorrido");
    }

    @E("clicar no botão Adicionar Análise de causas no incidente")
    public void clicarNoBotãoAdicionarAnáliseDeCausasNoIncidente() {
        analiseDeCausaIncidentePage.clicarBtnAddAnaliseDeCausa();
        addLogToReport("Clicou no botão Adicionar Análise de causas");
    }

    @E("adiciono análise de causas no Incidente")
    public void adicionoAnáliseDeCausasNoIncidente() {
        analiseDeCausaIncidentePage.preenchoAnaliseDeCausa(analiseDeCausaIncidente);
        addScreenshotToReport("Preenchi os 5 porquês");
    }

    @E("preencho a causa identificada no incidente")
    public void preenchoACausaIdentificadaNoIncidente() {
        analiseDeCausaIncidentePage.preenchoACausaIdentificada(analiseDeCausaIncidente);
    }

    @Quando("clico no botão salvar da análise de causa do incidente")
    public void clicoNoBotãoSalvarDaAnáliseDeCausaDoIncidente() {
        analiseDeCausaIncidentePage.cliqueiNoBotaoSalvar();
        addLogToReport("Cliquei sobre o botão salvar ");
    }

    @E("valido os porquês da análise de causa do incidente")
    public void validoOsPorquêsDaAnáliseDeCausaDoIncidente() {
        analiseDeCausaIncidentePage.validoOPreenchimentoDosPorques(analiseDeCausaIncidente);
        addScreenshotToReport("validei o preenchimento dos 5 porquês");
    }

    @Entao("clicar no botão Salvar e Criar Plano de Ação do incidente")
    public void clicarNoBotãoSalvarECriarPlanoDeAçãoDoIncidente() {
        analiseDeCausaIncidentePage.clicarBtnSalvarCriarPlanoAcao();
        addLogToReport("Clicou no botão Salvar e criar Plano");
    }

    @E("clicar na confirmação para Salvar e Criar Plano de Ação do incidente")
    public void clicarNaConfirmaçãoParaSalvarECriarPlanoDeAçãoDoIncidente() {
        analiseDeCausaIncidentePage.clicarBtnConfirmarSalvarCriarPlanoAcao();
        addLogToReport("Clicou na confirmação para Salvar e Criar Plano de Ação do incidente");
    }

    @E("valido que o plano avulso foi criado pelo incidente")
    public void validoQueOPlanoAvulsoFoiCriadoPeloIncidente() {
        analiseDeCausaIncidentePage.validoPlanoAvulso(incidente, analiseDeCausaIncidente);
        addScreenshotToReport("Valida Plano de ação criado");
    }

    @Quando("clico no botão editar uma análise de causa")
    public void clicoNoBotãoEditarUmaAnáliseDeCausa() {
        analiseDeCausaIncidentePage.clicarBtnEditarAnaliseDeCausa();
        addLogToReport("Clicou no botão Editar Análise de causa");
    }

    @Entao("edito as informações da análise de causa")
    public void editoAsInformaçõesDaAnáliseDeCausa() {
        analiseDeCausaIncidentePage.preenchoAnaliseDeCausa(analiseDeCausaIncidenteEditada);
        analiseDeCausaIncidentePage.preenchoACausaIdentificada(analiseDeCausaIncidenteEditada);
        addScreenshotToReport("Preencheu a Análise de causa");
    }

    @E("clico no botão salvar da pagina de Análise de causas")
    public void clicoNoBotãoSalvarDaPaginaDeAnáliseDeCausas() {
        analiseDeCausaIncidentePage.clicarBtnSalvarDaPagina();
        addLogToReport("Clicou no botão Salvar");
    }

    @Entao("valido os dados editados da Análise de causas")
    public void validoOsDadosEditadosDaAnáliseDeCausas() {
        analiseDeCausaIncidentePage.validoOPreenchimentoDosPorques(analiseDeCausaIncidenteEditada);
        addScreenshotToReport("Validou o preenchimento dos porques");
    }
}
