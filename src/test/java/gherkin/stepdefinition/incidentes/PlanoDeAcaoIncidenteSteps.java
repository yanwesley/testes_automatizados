package gherkin.stepdefinition.incidentes;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.api.model.incidente.Incidente;
import br.com.api.model.sindicancia.AnaliseDeCausa;
import br.com.pom.sro.incidentes.PlanoDeAcaoIncidentePage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.*;

public class PlanoDeAcaoIncidenteSteps extends ReporterUtils {
    PlanoDeAcaoIncidentePage planoDeAcaoIncidentePage = new PlanoDeAcaoIncidentePage();
    Acao acao = getAcao();
    AnaliseDeCausa analiseDeCausa = getAnaliseDeCausa();
    ResponsavelAcao responsavelAcao = getResponsavelAcao();
    Incidente incidente = getIncidente();


    @Entao("valido a ação incluída na análise de causa")
    public void validoAAçãoIncluídaNaAnáliseDeCausa() {
        planoDeAcaoIncidentePage.validaPlanoDeAcaoCriadoNaAnaliseDeCausa(analiseDeCausa, acao, responsavelAcao);
        addScreenshotToReport("Validou a ação incluída na análise de causa");
    }

    @Dado("clico na lupa para visualizar detalhe da ação do incidente")
    public void clicoNaLupaParaVisualizarDetalheDaAçãoDoIncidente() {
        planoDeAcaoIncidentePage.clicarNaLupaDaAcao();
        addLogToReport("Clicou na lupa para visualizar a ação");
    }

    @Entao("valido os dados da ação inserida do incidente no dialog")
    public void validoOsDadosDaAçãoInseridaDoIncidenteNoDialog() {
        planoDeAcaoIncidentePage.validaAAcaoInseridaNoIncidenteNoDialog(incidente, acao, responsavelAcao);
        addScreenshotToReport("Validou os dados da Ação no dialog de detalhe");
    }

    @Então("clico no botão SALVAR E ENCERRAR incidente")
    public void clicoNoBotãoSALVAREENCERRARIncidente() {
        planoDeAcaoIncidentePage.clicarBtnSalvarEEncerrar();
        addLogToReport("Cliquei no botão 'Salvar e Encerrar'.");
    }
}
