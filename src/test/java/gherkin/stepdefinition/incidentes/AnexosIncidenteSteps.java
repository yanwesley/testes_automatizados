package gherkin.stepdefinition.incidentes;

import br.com.pom.sro.incidentes.AnexosIncidentesPage;
import br.com.pom.sro.incidentes.SideMenuIncidentePage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;

import static br.com.api.GeradorDeMassa.getComentario4000Char;
import static br.com.api.GeradorDeMassa.getNewComentario4000Char;

public class AnexosIncidenteSteps extends ReporterUtils {

    SideMenuIncidentePage sideMenuIncidentePage = new SideMenuIncidentePage();
    AnexosIncidentesPage anexosIncidentesPage = new AnexosIncidentesPage();
    String comentario = getComentario4000Char();
    String comentarioEditado = getNewComentario4000Char();

    @E("estou na página de ANEXOS de Incidentes")
    public void estouNaPáginaDeANEXOS() {
        anexosIncidentesPage.validaQueEstaNaPaginaDeAnexos();
        addScreenshotToReport("Valida que o usuário está na página de ANEXOS");
    }

    @Dado("clico no botão Inserir anexos de Incidentes")
    public void clicoNoBotãoIncluirAnexos() {
        anexosIncidentesPage.clicarBtnIncluirAnexos();
        addLogToReport("Clicou no botão Incluir Anexos");
    }

    @E("insere um comentário na inclusão do anexo do incidente")
    public void insereUmComentárioNaInclusãoDoAnexo() {
        anexosIncidentesPage.insereComentario(comentario);
        addScreenshotToReport("Inseriu um comentário");
    }


    @E("valida que o\\(s) {int} arquivo\\(s) foram adicionados em incidentes")
    public void validaQueOSArquivoForamAdicionados(int qtdArquivosAdicionados) {
        anexosIncidentesPage.validaArquivoFoiInserido(qtdArquivosAdicionados);
        addScreenshotToReport("Valida que o(s)" + qtdArquivosAdicionados + " foram adicionados");
    }

    @Dado("clico no botão Editar do anexo de Incidentes")
    public void clicoNoBotãoEditarDoAnexo() {
        anexosIncidentesPage.clicarBtnEditarAnexo();
        addLogToReport("Clicou no botão Editar Anexo");
    }

    @E("edito o comentário do anexo do incidente")
    public void editoOComentárioDoAnexo() {
        anexosIncidentesPage.editaComentarioDoAnexo(comentarioEditado);
        addScreenshotToReport("Editou o comentário");
    }

    @E("valida que o comentário do anexo de Incidente foi editado")
    public void validaQueOComentárioDoAnexoFoiEditado() {
        anexosIncidentesPage.validaComentarioEditado(comentarioEditado);
        addScreenshotToReport("Valida o comentário editado");
    }

    @Dado("insere {int} arquivo na tela de anexos de incidentes com comentário")
    public void insereArquivoNaTelaDeAnexosDeIncidentesComComentário(int qtdArquivo) {
        anexosIncidentesPage.insereArquivosComComentario(qtdArquivo, comentario);
        addScreenshotToReport("Valida que o(s)" + qtdArquivo + " foram adicionados");
    }

    @Dado("excluo {int} arquivos dos Anexos de incidentes")
    public void excluoArquivosDosAnexosDeIncidentes(int qtdArquivosExcluidos) {
        anexosIncidentesPage.excluirArquivos(qtdArquivosExcluidos);
        addScreenshotToReport("Valida que o(s)" + qtdArquivosExcluidos + " foram excluídos");
    }
}
