package gherkin.stepdefinition.incidentes;

import br.com.api.model.incidente.Incidente;
import br.com.pom.sro.incidentes.HistoricoIncidentePage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;

import static br.com.api.GeradorDeMassa.getConclusao;
import static br.com.api.GeradorDeMassa.getIncidente;

public class HistoricoIncidentesSteps extends ReporterUtils {

    HistoricoIncidentePage historicoIncidentePage = new HistoricoIncidentePage();
    Incidente incidente = getIncidente();
    String justificativa = getConclusao();

    @E("valido que o histórico da solicitação de anulação está sendo exibido")
    public void validoQueOHistóricoDaSolicitaçãoDeAnulação() {
        historicoIncidentePage.validaHistoricoDeSolicitacaoDaAnulacao(justificativa);
        addScreenshotToReport("Validado histórico");
    }

    @E("clico no botão Aprovar Anulação")
    public void clicoNoBotãoAprovarAnulação() {
        historicoIncidentePage.clicarBrnAprovarAnulacao();
        addLogToReport("Cliquei no botão 'Aprovar Anulação'");
    }

    @E("preencho o modal de aprovar pedido de anulação")
    public void preenchoOModalDeAprovarPedidoDeAnulação() {
        historicoIncidentePage.preencherModalAprovarAnulacao(justificativa);
        addScreenshotToReport("Preenchi modal aprovar anulação.");
    }

    @E("clico em confirmar do modal de aprovar o pedido de anulação")
    public void clicoEmConfirmarDoModalDeAprovarOPedidoDeAnulação() {
        historicoIncidentePage.clicarBtnConfirmarModalAprovarAnulacao();
        addLogToReport("Cliquei no botão confirmar do modal de Aprovar Anulação.");
    }

    @E("valido que o histórico da solicitação de anulação aprovada está sendo exibido")
    public void validoQueOHistóricoDaSolicitaçãoDeAnulaçãoAprovadaEstáSendoExibido() {
        historicoIncidentePage.validarHistoricoPedidoAnulacaoAprovado(justificativa);
        addScreenshotToReport("Validado histórico pedido de anulação aprovado.");
    }

    @E("clico no botão Recusar Anulação")
    public void clicoNoBotãoRecusarAnulação() {
        historicoIncidentePage.clicarBtnRecusarAnulacao();
        addLogToReport("Cliquei no botão Recusar Anulação.");
    }

    @E("preencho o modal de recusar pedido de anulação")
    public void preenchoOModalDeRecusarPedidoDeAnulação() {
        historicoIncidentePage.preencherModalRecusarAnulacao(justificativa);
        addScreenshotToReport("Preenchi o modal de Reprovar pedido de anulação.");
    }

    @E("clico em confirmar do modal de recusar o pedido de anulação")
    public void clicoEmConfirmarDoModalDeRecusarOPedidoDeAnulação() {
        historicoIncidentePage.clicarBtnConfirmarModalReprovarAnulacao();
        addLogToReport("Cliquei no botão confirmar");
    }

    @E("valido que o histórico da solicitação de anulação recusado está sendo exibido")
    public void validoQueOHistóricoDaSolicitaçãoDeAnulaçãoRecusadoEstáSendoExibido() {
        historicoIncidentePage.validarHistoricoPedidoAnulacaoRecusado(justificativa);
        addScreenshotToReport("Validado histórico pedido de anulação aprovado.");
    }
}
