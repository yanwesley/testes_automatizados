package gherkin.stepdefinition.incidentes;

import br.com.api.model.incidente.Incidente;
import br.com.pom.sro.incidentes.IncidentesListagemPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getIncidente;

public class ListagemIncidentesStep extends ReporterUtils {

    IncidentesListagemPage incidenteListagemPage = new IncidentesListagemPage();
    Incidente incidente = getIncidente();

    @Entao("deve exibir a pagina de listagem de Incidentes")
    public void deveExibirAPaginaDeListagemDeIncidentes() {
        incidenteListagemPage.validaExibicaoDaPagina();
        addScreenshotToReport("Valida exibição da listagem");
    }

    @E("clicar no botão Editar da incidente ordenado por ordem decrescente")
    public void clicarNoBotãoEditarDaIncidenteOrdenadoPorOrdemDecrescente() {
        incidenteListagemPage.ordenarIdDecrescente();
        incidenteListagemPage.clicarBtnEditarListagem(1);
        addLogToReport("Cliquei no botão Editar do incidente");
    }

    @Entao("valido que o botão Novo Incidente deve ser exibido")
    public void validoQueOBotãoNovoIncidenteDeveSerExibido() {
        incidenteListagemPage.validaExibicaoDoBotaoNovoIncidente();
        addScreenshotToReport("Valida exibição do botão de novo incidente");
    }

    @Entao("deve exibir os campos do header do Incidente para filtrar a lista e exportar")
    public void deveExibirOsCamposDoHeaderDoIncidenteParaFiltrarAListaEExportar() {
        incidenteListagemPage.validaExibicaoDosCamposDoHeaderDaLista();
        addScreenshotToReport("Valida exibição das TABs e Botões de Incidentes");
    }

    @Entao("deve exibir os campos da tabela de de Incidentes")
    public void deveExibirOsCamposDaTabelaDeDeIncidentes() {
        incidenteListagemPage.validaExibicaoDosCamposDaTabelaDeIncidentes();
        addScreenshotToReport("Valida exibição dos campos da tabela de Incidentes");
    }

    @Quando("eu busco pelo texto {string} no filtro de incidente")
    public void euBuscoPeloTextoNoFiltroDeIncidente(String textoBusca) {
        incidenteListagemPage.filtrarPorTexto(textoBusca);
        addScreenshotToReport("Digitado texto para busca : " + textoBusca);
    }

    @Entao("deve ser exibido os registros que contenham o texto {string} na lista de incidente")
    public void deveSerExibidoOsRegistrosQueContenhamOTextoNaListaDeIncidente(String textoBusca) {
        incidenteListagemPage.validaResultadoDaBuscaPorTexto(textoBusca);
        addScreenshotToReport("Validando resultado da busca");
    }

    @Entao("deve exibir a mensagem informando {string} na lista de incidente")
    public void deveExibirAMensagemInformandoNaListaDeIncidente(String msgSemResultado) {
        incidenteListagemPage.validaExibicaoDaMensagemSemResultadoDaBusca(msgSemResultado);
        addScreenshotToReport("Validando exibição da mensagem de :  " + msgSemResultado);
    }

    @Quando("filtro a lista de Incidentes por data inicial {string}")
    public void filtroAListaDeIncidentesPorDataInicial(String dataInicial) {
        incidenteListagemPage.clicarBtnFiltrar();
        addScreenshotToReport("Cliquei no botão 'Filtrar'.");
        incidenteListagemPage.filtrarPorDataInicial(dataInicial);
        addScreenshotToReport("Preenchi a data inicial: " + dataInicial);
        incidenteListagemPage.clicarBtnAplicarFiltro();
        addLogToReport("Cliquei em 'Aplicar'.");
    }

    @Entao("deve exibir apenas os incidentes abertos a partir de {string}")
    public void deveExibirApenasOsIncidentesAbertosAPartirDe(String dataInicial) {
        incidenteListagemPage.validaResultadoDaBuscaPorDataInicial(dataInicial);
        addScreenshotToReport("Validando resultado.");
    }

    @Quando("filtro por incidentes com data final {string}")
    public void filtroPorIncidentesComDataFinal(String dataFinal) {
        incidenteListagemPage.clicarBtnFiltrar();
        addScreenshotToReport("Cliquei no botão 'Filtrar'.");
        incidenteListagemPage.limparCampoDataInicial();
        incidenteListagemPage.filtrarPorDataFinal(dataFinal);
        addScreenshotToReport("Preenchi a data final: " + dataFinal);
        incidenteListagemPage.clicarBtnAplicarFiltro();
        addLogToReport("Cliquei em 'Aplicar'.");
    }

    @Entao("deve exibir apenas os incidentes abertos até {string}")
    public void deveExibirApenasOsIncidentesAbertosAté(String dataFinal) {
        incidenteListagemPage.validaResultadoDaBuscaPorDataFinal(dataFinal);
        addScreenshotToReport("Validando resultado.");
    }

    @Quando("filtro por data inicial {string} e data final {string} na lista de incidentes")
    public void filtroPorDataInicialEDataFinalNaListaDeIncidentes(String dataInicial, String dataFinal) {
        incidenteListagemPage.clicarBtnFiltrar();
        addScreenshotToReport("Cliquei no botão 'Filtrar'.");
        incidenteListagemPage.filtrarPorDataInicial(dataInicial);
        incidenteListagemPage.filtrarPorDataFinal(dataFinal);
        addScreenshotToReport("Preenchi as datas: " + dataInicial + " e " + dataFinal);
        incidenteListagemPage.clicarBtnAplicarFiltro();
        addLogToReport("Cliquei em 'Aplicar'.");
    }

    @E("acesso a TAB Investigação")
    public void acessoNaTABInvestigação() {
        incidenteListagemPage.clicarTabInvestigacao();
        addLogToReport("Cliquei na TAB 'Investigação'.");
    }

    @E("acesso a TAB Incidentes criados")
    public void acessoATABIncidentesCriados() {
        incidenteListagemPage.clicarTabIncidentesCriados();
        addLogToReport("Clicou na TAB Incidentes Criados");
    }

    @E("acesso a TAB Encerrados")
    public void acessoATABEncerrados() {
        incidenteListagemPage.clicarTabEncerrados();
        addLogToReport("Clicou na TAB Encerrados");
    }

    @E("clicar no botão Incluir novo incidente")
    public void clicarNoBotãoIncluirNovoIncidente() {
        incidenteListagemPage.clicarBtnIncluirNovoIncidente();
        addLogToReport("Cliquei no botão Incluir novo incidente.");
    }

    @E("confirmo que a investigação está com status Encerrado")
    public void confirmoQueAInvestigaçãoEstáNaComStatusEncerrado() {
        incidenteListagemPage.clicarBtnVoltarParaListagem();
        addLogToReport("cliquei no Botão voltar");
        incidenteListagemPage.clicarTabEncerrados();
        addScreenshotToReport("Cliquei na Tab Encerrados");
        incidenteListagemPage.ordenarIdDecrescente();
        incidenteListagemPage.valideiStatusEncerrado(incidente, 1);
        addScreenshotToReport("Validei que o status da investigação está como encerrada");
    }

    @E("confirmo que a investigação está na com status Reprovado")
    public void confirmoQueAInvestigaçãoEstáNaComStatusReprovado() {
        incidenteListagemPage.clicarBtnVoltarParaListagem();
        addLogToReport("cliquei no Botão voltar");
        incidenteListagemPage.ordenarIdDecrescente();
        incidenteListagemPage.valideiStatusReprovado(incidente, 1);
        addScreenshotToReport("Validei que o status da investigação está como reprovado");
    }

    @E("limpo os filtros de data de Incidentes")
    public void limpoOsFiltrosDeDataDeIncidentes() {
        incidenteListagemPage.limparFiltrosDeData();
        addScreenshotToReport("Limpou filtros de data");
    }

    @E("confirmo que a investigação está na com status {string} na linha {int}")
    public void confirmoQueAInvestigaçãoEstáNaComStatus(String status, int linha) {
        incidenteListagemPage.validaStatusDoIncidente(incidente, status, linha);
        addScreenshotToReport("Validado status do incidente.");
    }

    @E("ordeno o incidente por ordem decrescente")
    public void ordenoOIncidentePorOrdemDecrescente() {
        incidenteListagemPage.ordenarIdDecrescente();
        addLogToReport("Ordenei por id.");
    }

    @E("confirmo que a investigação está na com status Anulado")
    public void confirmoQueAInvestigaçãoEstáNaComStatusAnulado() {
        incidenteListagemPage.clicarBtnVoltarParaListagem();
        addLogToReport("cliquei no Botão voltar");
        incidenteListagemPage.clicarTabEncerrados();
        addScreenshotToReport("Cliquei na Tab Encerrados");
        incidenteListagemPage.ordenarIdDecrescente();
        incidenteListagemPage.valideiStatusAnulado(incidente, 1);
        addScreenshotToReport("Validei que o status da investigação está como encerrada");
    }

    @Quando("clico na lupa o incidente na listagem")
    public void clicoNaLupaOIncidenteNaListagem() {
        incidenteListagemPage.clicarNaLupaListagem(1);
        addLogToReport("Cliquei na lupa da listagem");
    }

    @E("confirmo que a investigação está na com status Aberto")
    public void confirmoQueAInvestigaçãoEstáNaComStatusAberto() {
        incidenteListagemPage.clicarBtnVoltarParaListagem();
        addLogToReport("cliquei no Botão voltar");
        incidenteListagemPage.clicarTabInvestigacao();
        incidenteListagemPage.ordenarIdDecrescente();
        incidenteListagemPage.valideiStatusReaberto(incidente, 1);
        addScreenshotToReport("Validei que o status da investigação está como Aberto");
    }

    @Então("valido que a área responsavel é a que foi alterada")
    public void validoQueAÁreaResponsavelÉAQueFoiAlterada() {
        incidenteListagemPage.validarAreaResponsavelAlterada(incidente);
        addScreenshotToReport("Validei a área alterada");
    }

    @E("clicar no botão Visualizar do incidente ordenado por ordem decrescente")
    public void clicarNoBotãoVisualizarDoIncidenteOrdenadoPorOrdemDecrescente() {
        incidenteListagemPage.ordenarIdDecrescente();
        incidenteListagemPage.clicarNaLupaListagem(1);
        addLogToReport("Cliquei no botão Editar do incidente");

    }
}
