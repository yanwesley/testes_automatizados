package gherkin.stepdefinition.incidentes;

import br.com.api.model.incidente.Incidente;
import br.com.api.model.sindicancia.AnaliseDeCausa;
import br.com.api.model.sinistro.Veiculo;
import br.com.pom.sro.incidentes.AnaliseDeCausaIncidentePage;
import br.com.pom.sro.incidentes.AreaResponsavelIncidentesPage;
import br.com.pom.sro.incidentes.DadosIniciaisIncidentesPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

import static br.com.api.GeradorDeMassa.*;

public class FichaInvestigacaoSteps extends ReporterUtils {

    DadosIniciaisIncidentesPage dadosIniciaisIncidentesPage = new DadosIniciaisIncidentesPage();
    Incidente incidente = getIncidente();
    Veiculo veiculo = getVeiculo();
    AnaliseDeCausa analiseDeCausa = getAnaliseDeCausa();
    AreaResponsavelIncidentesPage areaResponsavelIncidentesPage = new AreaResponsavelIncidentesPage();
    AnaliseDeCausaIncidentePage analiseDeCausaIncidentePage = new AnaliseDeCausaIncidentePage();
    AnaliseDeCausa analiseDeCausaIncidente = getAnaliseDeCausa();
    boolean isEdicao;


    @Entao("valido os dados iniciais da ficha de investigação")
    public void validoOsDadosIniciaisDaFichaDeInvestigação() {
        dadosIniciaisIncidentesPage.validaSobreOIncidenteReadOnlyFicha(incidente);
        addScreenshotToReport("Valida os dados preenchidos sobre o Incidente ReadOnly");
        dadosIniciaisIncidentesPage.validaSobreOLocalReadOnlyFicha(incidente);
        addScreenshotToReport("Valida os dados preenchidos sobre o Local do Incidente ReadOnly");
        dadosIniciaisIncidentesPage.validaSobreOTremReadOnlyFicha(incidente);
        addScreenshotToReport("Valida os dados preenchidos sobre o Trem do Incidente ReadOnly");
        dadosIniciaisIncidentesPage.validaSobreAtivosEnvolvidosReadOnlyFicha(incidente, veiculo);
        addScreenshotToReport("Valida os dados preenchidos de Ativos Envolvidos ReadOnly");
//        fichaInvestigacaoPage.validarDadosIniciais(incidente);
//        addScreenshotToReport("validei os dados iniciais da ficha de investigação");
    }

    @E("valido as análises de causa da ficha de investigação")
    public void validoAsAnálisesDeCausaDaFichaDeInvestigação() {
        analiseDeCausaIncidentePage.validoOPreenchimentoDosPorques(analiseDeCausaIncidente);
//        fichaInvestigacaoPage.validarAnalisesDeCausa(analiseDeCausa);
        addScreenshotToReport("Validei as análises de causa da ficha de investigação");
    }

    @E("valido a área responsável da ficha de investigação")
    public void validoAÁreaResponsávelDaFichaDeInvestigação() {
        areaResponsavelIncidentesPage.validarDadosPreenchidos(incidente);
        addScreenshotToReport("Validei os dados preenchidos.");
//        fichaInvestigacaoPage.validarAreaResponsavel(incidente);
//        addScreenshotToReport("Validei os dados da área responsável");
    }

}
