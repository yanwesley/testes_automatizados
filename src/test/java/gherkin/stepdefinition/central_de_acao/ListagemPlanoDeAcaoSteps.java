package gherkin.stepdefinition.central_de_acao;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.api.model.incidente.Incidente;
import br.com.pom.sro.central_de_acao.IncluirUmaAcaoPage;
import br.com.pom.sro.central_de_acao.ListagemPlanoDeAcaoPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.*;

import static br.com.api.GeradorDeMassa.*;

public class ListagemPlanoDeAcaoSteps extends ReporterUtils {
    ListagemPlanoDeAcaoPage listagemPlanoDeAcaoPage = new ListagemPlanoDeAcaoPage();
    IncluirUmaAcaoPage incluirUmaAcaoPage = new IncluirUmaAcaoPage();
    PlanoDeAcao planoDeAcao = getPlanoDeAcao();
    ResponsavelAcao responsavelAcao = getResponsavelAcao();
    ResponsavelAcao responsavelAcaoEditado = getNewResponsavelAcao();
    Acao acao = getAcao();
    Incidente incidente = getIncidente();

    @Entao("valido que o plano de ação está sendo exibido na listagem")
    public void validoAListagemDoPlanoDeAção() {
        listagemPlanoDeAcaoPage.validoListagemPlanoDeAcao(planoDeAcao, 1);
        addScreenshotToReport("validei as informações do plano de ação");
    }

    @Entao("clicar no botão Editar da central de ações ordenado por ordem decrescente")
    public void clicarNoBotãoEditarDaCentralDeAçõesOrdenadoPorOrdemDecrescente() {
        listagemPlanoDeAcaoPage.ordenarIdDecrescente();
        listagemPlanoDeAcaoPage.clicarBtnEditarListagem(1);
        addLogToReport("Cliquei no botão Editar do plano de ação.");
    }

    @Entao("deve exibir a pagina de listagem de ações")
    public void deveExibirAPaginaDeListagemDeAções() {
        listagemPlanoDeAcaoPage.validoAExibicaoListagemAcoes();
        addScreenshotToReport("validei que estou na listagem de ações");
    }

    @Entao("deve ser exibido os registros que contenham o texto {string} na listagem de ações")
    public void deveSerExibidoOsRegistrosQueContenhamOTextoNaListagemDeAções(String textoBusca) {
        listagemPlanoDeAcaoPage.validaResultadoDaBuscaPorTextoListagemAcao(textoBusca);
        addScreenshotToReport("Validei a exibição dos resultados");
    }

    @Dado("que eu clique na tab Para Aceite")
    public void queEuCliqueNaTabParaAceite() {
        listagemPlanoDeAcaoPage.clicarNaTabParaAceite();
        addLogToReport("cliquei sobre a tab Para Aceite");
    }

    @E("escolho o gerente no filtro {string}")
    public void escolhoOGerenteNoFiltro(String gerente) {
        listagemPlanoDeAcaoPage.escolhoOGerenteNoFiltro(gerente);
        addScreenshotToReport("Validei a escolha do gerente no filtro");
    }

    @Entao("valido a listagem com o gerente {string}")
    public void validoListagemAcaoComFiltroGerente(String gerente) {
        listagemPlanoDeAcaoPage.validoListagemAcaoComFiltroGerente(gerente, 1);
        addScreenshotToReport("Validei a listagem com o filtro gerente");
    }

    @Dado("que eu clique na tab Em execução")
    public void queEuCliqueNaTabParaEmExecução() {
        listagemPlanoDeAcaoPage.clicarNaTabEmExecução();
        addLogToReport("cliquei sobre a tab Em execução");
    }

    @Dado("que eu clique na tab Encerradas")
    public void queEuCliqueNaTabEncerradas() {
        listagemPlanoDeAcaoPage.clicarNaTabEncerradas();
        addLogToReport("cliquei sobre a tab Encerradas.");
    }

    @E("escolho o responsável no filtro {string}")
    public void escolhoOResponsávelNoFiltro(String responsavel) {
        listagemPlanoDeAcaoPage.escolhoResponsavelNoFiltro(responsavel);
        addScreenshotToReport("Escolho o responsável no filtro");
    }

    @Entao("valido a listagem com o responsável {string}")
    public void validoAListagemComOResponsável(String responsavel) {
        listagemPlanoDeAcaoPage.validoListagemAcaoComFiltroResponsavel(responsavel, 1);
        listagemPlanoDeAcaoPage.limpoOFiltro();
        addScreenshotToReport("Validei a listagem com o responsável");
    }

    @Entao("ordeno a acao por ordem decrescente")
    public void ordenoAAcaoPorOrdemDecrescente() {
        listagemPlanoDeAcaoPage.ordenarAcaoIdDecrescente();
        addScreenshotToReport("Ordenei as ações por ordem Decrescente.");
    }

    @Entao("valido que a ação está sendo exibido na listagem")
    public void validoQueAAçãoEstáSendoExibidoNaListagem() {
        listagemPlanoDeAcaoPage.validaExibicaoDaAcaoNaListagemParaAceite(planoDeAcao, acao, responsavelAcao, 1);
        addScreenshotToReport("Validei a exibição da ação na listagem.");
    }

    @Entao("valido que a ação anulada está sendo exibido na listagem de encerradas")
    public void validoQueAAçãoAnuladaEstáSendoExibidoNaListagemDeEncerradas() {
        listagemPlanoDeAcaoPage.validaExibicaoDaAcaoNaListagemEncerradaSRO(planoDeAcao, acao, responsavelAcao, "Anulada", 1);
        addScreenshotToReport("Validei a exibição da ação na listagem Encerrada.");
    }

    @E("clicar no botão Editar ação da central de ações ordenado por ordem decrescente")
    public void clicarNoBotãoEditarAçãoDaCentralDeAçõesOrdenadoPorOrdemDecrescente() {
        listagemPlanoDeAcaoPage.ordenarAcaoIdDecrescente();
        addScreenshotToReport("Ordenei as ações por ordem Decrescente.");
        listagemPlanoDeAcaoPage.clicarBtnEditarAcaoListagem(1);
        addLogToReport("Cliquei no botão editar Ação.");
    }

    @E("clico no botão Editar Acao na listagem na linha {int}")
    public void clicoNoBotãoEditarAcaoNaListagemNaLinha(int linha) {
        listagemPlanoDeAcaoPage.clicarBtnEditarAcaoListagem(linha);
        addLogToReport("Cliquei no botão Editar na listagem");
    }

    @Então("valido que o status da {int} ação é {string}")
    public void validoQueOStatusDaAçãoÉ(int linha, String status) {
        listagemPlanoDeAcaoPage.validarStatusDaAcao(linha, status);
        addScreenshotToReport("Validei que o status da ação é " + status);
    }

    @Entao("deve exibir a ação da linha {int} com a origem da ação")
    public void deveExibirAAçãoDaLinhaComAOrigem(int linha) {
        listagemPlanoDeAcaoPage.validaOrigemAcao(linha, acao);
        addScreenshotToReport("Validei a origem da ação.");
    }

    @Entao("valido que o plano de ação está sendo exibido na listagem pública")
    public void validoQueOPlanoDeAçãoEstáSendoExibidoNaListagemPública() {
        listagemPlanoDeAcaoPage.validaExibicaoDaAcaoNaListagemPublicaSRO(planoDeAcao, acao, responsavelAcao, 1);
        addScreenshotToReport("Validei a exibição da ação na listagem pública.");
    }

    @E("clico na tab pública")
    public void clicoNaTabPública() {
        listagemPlanoDeAcaoPage.clicarNaTabPublica();
        addLogToReport("cliquei na aba pública");
    }

    @Entao("valido que o plano de ação está sendo exibido na listagem pública com perfil diferente de SRO")
    public void validoQueOPlanoDeAçãoEstáSendoExibidoNaListagemPúblicaComPerfilDiferenteDeSRO() {
        listagemPlanoDeAcaoPage.validaExibicaoDaAcaoNaListagemPublica(planoDeAcao, acao, responsavelAcao, 1);
        addScreenshotToReport("Validei a exibição da ação na listagem pública.");
    }

    @E("clico no botão incluir novo plano")
    public void clicoNoBotãoIncluirNovoPlano() {
        listagemPlanoDeAcaoPage.clicarBtnIncluirNovoPlano();
        addLogToReport("cliquei sobre o botão incluir novo plano");
    }

    @E("clicar no botão Editar ação da central de ações")
    public void clicarNoBotãoEditarAçãoDaCentralDeAções() {
        listagemPlanoDeAcaoPage.clicarBtnEditarAcaoNaListagem();
    }

    @Quando("que eu clique na tab pública")
    public void queEuCliqueNaTabPública() {
        listagemPlanoDeAcaoPage.clicarNaTabEmPublica();
        addScreenshotToReport("cliquei na aba pública");
    }

    @E("escolho a área no filtro {string}")
    public void escolhoAÁreaNoFiltro(String arg0) {
        listagemPlanoDeAcaoPage.filtroPelaArea();
        addScreenshotToReport("Validei a área no filtro");
    }

    @Entao("valido a listagem com a área {string}")
    public void validoAListagemComAÁrea(String area) {
        listagemPlanoDeAcaoPage.validoListagemAcaoComFiltroArea(area, 1);
        addScreenshotToReport("Validei a listagem com o filtro Area");
    }

    @Então("seleciono {int} açõe\\(es)")
    public void selecionoAçõeEs(int quantidade) {
        listagemPlanoDeAcaoPage.selecionarAcoes(quantidade);
        addScreenshotToReport("Selecionei as ações");
    }

    @E("clicar no botão Aceitar selecionadas")
    public void clicarNoBotãoAceitarSelecionadas() {
        listagemPlanoDeAcaoPage.clicarBtnAceitarSelecionadas();
        addLogToReport("Cliquei no botão Aceitar selecionadas.");
    }

    @Então("deve exibir o modal de aceitar ação em lote")
    public void deveExibirOModalDeAceitarAçãoEmLote() {
        listagemPlanoDeAcaoPage.validaModalAceitarAcaoEmLote();
        addScreenshotToReport("Validado modal de aceitar ação em lote.");
    }

    @E("ao clicar no botão Ok no modal de aceitar ação em lote")
    public void aoClicarNoBotãoOkNoModalDeAceitarAçãoEmLote() {
        listagemPlanoDeAcaoPage.clicarBtnOkModalAceitarAcao();
        addLogToReport("Cliquei no botão ok do modal.");
    }

    @Entao("valido que as {int} ação\\(es) está\\(ão) sendo exibida\\(s) na listagem de em execução")
    public void validoQueAsAçãoEsEstáÃoSendoExibidaSNaListagem(int quantidade) {
        listagemPlanoDeAcaoPage.validarAcoesListagemEmExecucao(quantidade, acao, responsavelAcao, planoDeAcao, "Aberta");
        addScreenshotToReport("Validei que as ações foram para execução.");
    }

    @E("clico no botão alterar responsavel em lote")
    public void clicoNoBotãoAlterarResponsavelEmLote() {
        listagemPlanoDeAcaoPage.clicarBtnAlterarResponsavelLote();
        addLogToReport("Cliquei no botão 'Alterar responsável'");
    }

    @E("edito as informações do responsável em lote")
    public void editoAsInformaçõesDoResponsávelEmLote() {
        incluirUmaAcaoPage.preencherDadosResponsavel(responsavelAcaoEditado);
        addScreenshotToReport("Preenchi os dados do responsável.");
        incluirUmaAcaoPage.clicarBtnConfirmarModalAdicionarResponsavel();
        addLogToReport("Cliquei no botão confirmar.");
    }

    @Entao("deve exibir o modal de confirmar alteração em lote")
    public void deveExibirOModalDeConfirmarAlteraçãoEmLote() {
        listagemPlanoDeAcaoPage.validarModalConfirmarAlteracoes();
        addScreenshotToReport("Validou modal de confirmar alteração");
    }

    @E("ao clicar no botão Ok no modal de confirmar alterações da ação em lote")
    public void aoClicarNoBotãoOkNoModalDeConfirmarAlteraçõesDaAçãoEmLote() {
        listagemPlanoDeAcaoPage.clicarBtnOkModalConfirmarAlteracao();
        addLogToReport("Cliquei no botão OK no modal de confirmar alteração.");
    }

    @Entao("valido que as {int} ação\\(es) está\\(ão) sendo exibida\\(s) na listagem em aberto")
    public void validoQueAsAçãoEsEstáÃoSendoExibidaSNaListagemEmAberto(int quantidade) {
        listagemPlanoDeAcaoPage.validarAcoesListagemEmExecucao(quantidade, acao, responsavelAcaoEditado, planoDeAcao, "Aguardando aceite");
        addScreenshotToReport("Validei que as ações tiveram o responsável editado.");

    }

    @Entao("valido que o plano de ação foi criado a partir do incidente")
    public void validoQueOPlanoDeAçãoFoiCriadoAPartirDoIncidente() {
        listagemPlanoDeAcaoPage.validarPlanoDeAcaoCriadoPeloIncidente(incidente, acao);
        addScreenshotToReport("Validou plano de ação criado pelo Incidente");
    }
}
