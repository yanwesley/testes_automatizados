package gherkin.stepdefinition.central_de_acao;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.pom.sro.central_de_acao.EditarAcaoPage;
import br.com.pom.sro.central_de_acao.IncluirUmaAcaoPage;
import br.com.pom.sro.central_de_acao.VisualizarAcaoPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.*;

public class VisualizarAcaoSteps extends ReporterUtils {

    VisualizarAcaoPage visualizarAcaoPage = new VisualizarAcaoPage();
    IncluirUmaAcaoPage incluirUmaAcaoPage = new IncluirUmaAcaoPage();
    EditarAcaoPage editarAcaoPage = new EditarAcaoPage();
    PlanoDeAcao planoDeAcao = getPlanoDeAcao();
    ResponsavelAcao responsavelAcao = getResponsavelAcao();
    ResponsavelAcao responsavelAcaoEditado = getResponsavelAcaoEditado();
    Acao acao = getAcao();
    Acao acaoEditada = getNewAcao();
    String justificativa = getConclusao();


    @Então("deve exibir a página de Visualizar Ação")
    public void deveExibirAPáginaDeVisualizarAção() {
        visualizarAcaoPage.validaExibicaoDaPagina();
        addScreenshotToReport("Validei a exibição da página de ");
    }

    @E("ao clicar no botão Aceitar Ação")
    public void aoClicarNoBotãoAceitarAção() {
        visualizarAcaoPage.clicaBtnAceitarAcao();
        addLogToReport("Cliquei no botão 'Aceitar Ação'");
    }

    @Então("deve exibir o modal de confirmação para o Aceite da Ação")
    public void deveExibirOModalDeConfirmaçãoParaOAceiteDaAção() {
        visualizarAcaoPage.validaModalDeAceitarAcao();
        addScreenshotToReport("Validei a exibição do Modal de Aceitar Ação.");
    }


    @E("ao clicar em Confirmar no modal de Aceite da Ação")
    public void aoClicarEmConfirmarNoModalDeAceiteDaAção() {
        visualizarAcaoPage.clicarBtnConfirmarModalAceitarAcao();
        addLogToReport("Cliquei no botão de confirmar aceite da ação.");
    }

    @Então("deve exibir a página de Visualizar Ação com os detalhes da ação")
    public void deveExibirAPáginaDeVisualizarAçãoComOsDetalhesDaAção() {
        visualizarAcaoPage.validaExibicaoDosDetalhesDaAcao(planoDeAcao, acao, responsavelAcao);
        addScreenshotToReport("Validei os detalhes da ação.");
    }

    @Entao("clico no botão Editar Acao")
    public void clicoNoBotãoEditarAcao() {
        visualizarAcaoPage.clicarBtnEditarAcao();
        addLogToReport("Cliquei no botão Editar Ação.");
    }

    @Então("deve exibir o modal de Editar ação")
    public void deveExibirOModalDeEditarAção() {
        editarAcaoPage.validaExibicaoDoModal();
        addScreenshotToReport("Validei a exibição do modal");
    }

    @Então("preencho a justificativa do modal de editar ação")
    public void preenchoAJustificativaDoModalDeEditarAção() {
        editarAcaoPage.preencherJustificativa(justificativa);
        addScreenshotToReport("Preenchi a justificativa.");
    }


    @E("seleciono a opção Editar Ação no modal")
    public void selecionoAOpçãoEditarAçãoNoModal() {
        editarAcaoPage.selecionarOpcaoEditarAcao();
        addScreenshotToReport("Selecionei a opção 'Editar Ação'.");
    }

    @E("edito as informações iniciais da ação")
    public void editoAsInformaçõesIniciaisDaAção() {
        editarAcaoPage.editarInformacoesIniciais(acaoEditada);
        addScreenshotToReport("Editei as informações iniciais da ação.");
    }

    @E("clico no botão confirmar do modal de editar acao")
    public void clicoNoBotãoConfirmarDoModalDeEditarAcao() {
        editarAcaoPage.clicarBtnConfirmarModal();
        addLogToReport("Cliquei no botão 'Confirmar'");
    }

    @E("valido que os dados da ação foram editado")
    public void validoQueOsDadosDaAçãoForamEditado() {
        visualizarAcaoPage.validaExibicaoDosDetalhesDaAcaoAposEdicao(acaoEditada);
        addScreenshotToReport("Validei que a ação foi editada.");
    }

    @E("seleciono a opção Anular Ação no modal")
    public void selecionoAOpçãoAnularAçãoNoModal() {
        editarAcaoPage.selecionarOpcaoAnularAcao();
        addScreenshotToReport("Selecionei a opção de  Anular Ação.");
    }

    @E("seleciono a opção Alterar o responsável por esta acao Ação no modal")
    public void selecionoAOpçãoAlterarOResponsávelPorEstaAcaoAçãoNoModal() {
        editarAcaoPage.selecionarOpcaoAlterarResponsavel();
        addScreenshotToReport("Selecionei a opção 'Alterar o responsável por esta ação'");
    }

    @E("edito as informações do responsável")
    public void editoAsInformaçõesDoResponsável() {
        editarAcaoPage.clicarBtnBuscarResponsavel();
        addLogToReport("Cliquei no botão 'Buscar Responsável'");
        incluirUmaAcaoPage.preencherDadosResponsavel(responsavelAcaoEditado);
        addScreenshotToReport("Preenchi os dados do novo responsável.");
        incluirUmaAcaoPage.clicarBtnConfirmarModalAdicionarResponsavel();
    }

    @E("o responsável da ação foi editado")
    public void oResponsávelDaAçãoFoiEditado() {
        visualizarAcaoPage.validarDadosDoResponsavel(responsavelAcaoEditado);
        addScreenshotToReport("Validei que os dados do responsável foram editados.");
    }

    @E("valido que a ação foi anulada")
    public void validoQueAAçãoFoiAnulada() {
        visualizarAcaoPage.validarDetalhesDaAcaoAnulada();
        addScreenshotToReport("Validei que os botãos da ação não estão visíveis.");
    }


    @E("ao clicar no botão Justificar Atraso")
    public void aoClicarNoBotãoJustificarAtraso() {
        visualizarAcaoPage.clicarBtnJustificarAtraso();
        addLogToReport("Cliquei no botão 'Justificar Atraso'.");
    }

    @Então("deve exibir o modal de Justificar Atraso")
    public void deveExibirOModalDeJustificarAtraso() {
        visualizarAcaoPage.validaExibicaoDoModalDeJustificarAtraso();
        addScreenshotToReport("Validado exibição do modal.");
    }

    @E("preencho os dados do modal de Justificar Atraso")
    public void preenchoOsDadosDoModalDeJustificarAtraso() {
        visualizarAcaoPage.preencherDadosDoModalJustificarAtraso(justificativa, acaoEditada);
        addScreenshotToReport("Preenchi os dados do modal.");
    }

    @E("clico no botão Confirmar do modal de Justificar Atraso")
    public void clicoNoBotãoConfirmarDoModalDeJustificarAtraso() {
        visualizarAcaoPage.clicarBtnConfirmarModalAcaoAtrasada();
        addLogToReport("Cliquei no botão confirmar.");
    }

    @E("confirmo a ação de Justificar Atraso")
    public void confirmoAAçãoDeJustificarAtraso() {
        visualizarAcaoPage.clicarBtnConfirmarAlteracao();
        addLogToReport("Cliquei no botão confirmar.");
    }

    @E("deve exibir a data preenchida no campo de Previsão de Conclusão")
    public void deveExibirADataPreenchidaNoCampoDePrevisãoDeConclusão() {
        visualizarAcaoPage.validaDataPrevisaoEditada(acaoEditada);
        addScreenshotToReport("Validei data da previsão editada.");
    }

    @E("ao preencher os dados de execução da ação")
    public void aoPreencherOsDadosDeExecuçãoDaAção() {
        visualizarAcaoPage.preencherDadosExecucao(acao);
        addScreenshotToReport("Preenchi os dados da execução.");
    }

    @E("clicar no botão Salvar e Finalizar Ação")
    public void clicarNoBotãoSalvarEFinalizarAção() {
        visualizarAcaoPage.clicarBtnSalvarEFinalizarAcao();
        addLogToReport("Cliquei no botão 'Salvar e Finalizar Ação'.");
    }

    @E("deve exibir os dados da execução preenchidos")
    public void deveExibirOsDadosDaExecuçãoPreenchidos() {
        visualizarAcaoPage.validaDadosExecucaoReadOnly(acao);
        addScreenshotToReport("Validei os dados da execução da Ação");
    }

    @E("edito se a ação é pública nas informações iniciais da ação")
    public void editoSeAAçãoÉPúblicaNasInformaçõesIniciaisDaAção() {
        editarAcaoPage.preencherAcaoPublica(acao);
        addScreenshotToReport("Preenchi se a ação é pública.");
    }

    @E("valido que os dados da ação pública foi editada")
    public void validoQueOsDadosDaAçãoPúblicaFoiEditada() {
        visualizarAcaoPage.validaAcaoPublica(acao);
        addScreenshotToReport("Validei a alteração se a ação é pública.");
    }

    @E("clico no botão Reprovar Evidência")
    public void clicoNoBotãoReprovarEvidência() {
        visualizarAcaoPage.clicarBtnReprovarEvidencia();
        addLogToReport("Cliquei no botão Reprovar Evidência.");
    }

    @Então("preencho a justificativa do modal de Reprovar Evidência")
    public void preenchoAJustificativaDoModalDeReprovarEvidência() {
        visualizarAcaoPage.preencherDadosDoModalReprovarEvidencia(justificativa);
        addScreenshotToReport("Preenchi os dados para Reprovar as evidências.");
    }

    @E("clico no botão confirmar do modal de Reprovar Evidência")
    public void clicoNoBotãoConfirmarDoModalDeReprovarEvidência() {
        visualizarAcaoPage.clicarBtnConfirmarModalReprovarEvidencia();
        addLogToReport("Cliquei no botão Confirmar");
    }

    @E("clico no botão Aprovar Evidência")
    public void clicoNoBotãoAprovarEvidência() {
        visualizarAcaoPage.clicarBtnAprovarEvidencia();
        addLogToReport("Cliquei no botão 'Aprovar Evidência'.");
    }

    @E("deve exibir o modal de Aprovar Evidência")
    public void deveExibirOModalDeAprovarEvidência() {
        visualizarAcaoPage.validarExibicaoDoModalDeAprovarEvidencia();
        addScreenshotToReport("Validei a exibição do modal de Aprovar Evidências.");
    }

    @E("ao selecionar a opção que não houve verificação da eficácia da ação")
    public void aoSelecionarAOpçãoQueNãoHouveVerificaçãoDaEficáciaDaAção() {
        acao.setHouveAVerificacaoDaEficaciaDaAcao(false);
        visualizarAcaoPage.selecionarOpcaoHouveVerificacaoEficacia(acao);
        addScreenshotToReport("Selecionei a opção 'Não', no Houve a verificação da eficácia da ação? ");
    }

    @Então("preencho os dados do modal de Aprovar Evidência")
    public void preenchoOsDadosDoModalDeAprovarEvidência() {
        visualizarAcaoPage.preencherDadosAprovarEvidencia(acao);
        addScreenshotToReport("Preenchi os dados do modal de aprovação");
    }

    @E("clico no botão confirmar do modal de Aprovar Evidência")
    public void clicoNoBotãoConfirmarDoModalDeAprovarEvidência() {
        visualizarAcaoPage.clicarBtnConfirmarModalAprovarEvidencia();
        addLogToReport("Cliquei no botão Confirmar do modal de Aprovar Evidência");
    }

    @E("valido que os botões Aprovar e Reprovar evidências não são exibidos")
    public void validoQueOsBotõesAprovarReprovarEvidênciasNãoSãoExibidos() {
        visualizarAcaoPage.validarQueOsBotoesAprovarReprovarEvidenciasNaoSaoExibidos();
        addScreenshotToReport("Validei que os botões Aprovar/Reprovar evidências não são exibidos.");
    }

    @E("ao selecionar a opção que houve verificação da eficácia da ação")
    public void aoSelecionarAOpçãoQueHouveVerificaçãoDaEficáciaDaAção() {
        acao.setHouveAVerificacaoDaEficaciaDaAcao(true);
        visualizarAcaoPage.selecionarOpcaoHouveVerificacaoEficacia(acao);
        addScreenshotToReport("Selecionei a opção 'Sim', no Houve a verificação da eficácia da ação? ");
    }

    @E("clico no botão Reabrir Ação")
    public void clicoNoBotãoReabrirAção() {
        visualizarAcaoPage.clicarBtnReabrirAcao();
        addLogToReport("Cliquei no botão 'Reabrir Ação'.");
    }

    @Então("preencho a justificativa do modal de Reabrir Ação")
    public void preenchoAJustificativaDoModalDeReabrirAção() {
        visualizarAcaoPage.preencherJustificativaReabrir(acaoEditada);
        addScreenshotToReport("Preenchi a justificativa para reabrir ação.");
    }

    @E("edito as informações iniciais da ação no modal de Reabrir Ação")
    public void editoAsInformaçõesIniciaisDaAçãoNoModalDeReabrirAção() {
        visualizarAcaoPage.editarInformacoesIniciais(acaoEditada);
        addScreenshotToReport("Preenchi os dados das informações iniciais");
    }

    @E("clico no botão confirmar do modal de Reabrir Ação")
    public void clicoNoBotãoConfirmarDoModalDeReabrirAção() {
        visualizarAcaoPage.clicarBtnConfirmarModalReabrirAcao();
        addLogToReport("Cliquei no botão confirmar.");
    }

    @Entao("valido o plano de ação detalhado")
    public void validoOPlanoDeAçãoDetalhado() {
        visualizarAcaoPage.validoPlanoDeAcaoDetalhado(planoDeAcao, acao, responsavelAcao);
        addScreenshotToReport("validei o preenchimento do plano de ação detalhado");
    }

    @E("clico no botão Desdobrar Ação")
    public void clicoNoBotãoDesdobrarAção() {
        visualizarAcaoPage.clicarBtnDesdobraAcao();
        addLogToReport("Cliquei no botão Desdobrar Ação");
    }

    @E("edito as informações iniciais da ação no modal de Desdobrar Ação")
    public void editoAsInformaçõesIniciaisDaAçãoNoModalDeDesdobrarAção() {
        visualizarAcaoPage.preencherDadosIniciasModalDesdobrarAcao(acao);
        addScreenshotToReport("Preenchi as informações iniciais da ação");
    }

    @E("clico no botão confirmar do modal de Desdobrar Ação")
    public void clicoNoBotãoConfirmarDoModalDeDesdobrarAção() {
        visualizarAcaoPage.clicarBtnConfirmaModalDesdobrarAcao();
        addLogToReport("Cliquei no modal confirmar do modal de Desdobrar ação.");
    }

    @E("clico no botão para buscar o responsável para a ação desdobrada")
    public void clicoNoBotãoParaBuscarOResponsavelParaAAçãoDesdobrada() {
        visualizarAcaoPage.clicarBtnAdicionarResponsavelModalDesdobrada();
        addLogToReport("Cliquei no botão para adicionar responsável");
    }

    @Então("deve exibir a página de Visualizar Ação com os detalhes da ação desdobrada")
    public void deveExibirAPáginaDeVisualizarAçãoComOsDetalhesDaAçãoDesdobrada() {
        visualizarAcaoPage.validaExibicaoDosDetalhesDaAcaoDesdobrada(planoDeAcao, acao, responsavelAcaoEditado);
        addScreenshotToReport("Validei os detalhes da ação.");
    }

    @E("seleciono o motivo para recusar a ação desdobrada")
    public void selecionoOMotivoParaRecusarAAçãoDesdobrada() {
        visualizarAcaoPage.selecionarMotivoRecusa(acao);
        addScreenshotToReport("selecionei o radio button informando a escolha da recusa");
    }

    @E("preencho a justificativa da recusa desdobrar ação")
    public void preenchoAJustificativaDaRecusaDesdobrarAção() {
        visualizarAcaoPage.preencherJustificativaRecusarDesdobrar(acao);
        addScreenshotToReport("preenchi a justificativa");
    }

    @Entao("confirmo a recusa da ação desdobrada")
    public void confirmoARecusaDaAçãoDesdobrada() {
        visualizarAcaoPage.clicarEnviarSolicitacao();
        addLogToReport("cliquei em enviar solicitação");
    }

    @E("ao clicar no botão Recusar Ação")
    public void aoClicarNoBotãoRecusarAção() {
        visualizarAcaoPage.clicarBtnRecusarAcao();
        addLogToReport("Cliquei no botão recusar ação");
    }

    @Então("seleciono a opção {string} no modal de recusar ação")
    public void selecionoAOpçãoNoModalDeRecusarAção(String opcao) {
        acao.setPorQueVoceEstaRecusandoAAcao(opcao);
        visualizarAcaoPage.selecionarOpcaoDeRecusaDaAcao(acao);
        addScreenshotToReport("Selecionei a opção " + opcao);
    }

    @E("ao clicar em Enviar Solicitação no modal de Recusar Ação")
    public void aoClicarEmEnviarSolicitaçãoNoModalDeRecusarAção() {
        visualizarAcaoPage.clicarBtnEnviarSolicitacaoRecusaAcao();
        addLogToReport("Cliquei no botão enviar solicitação");
    }

    @Então("clico na tab HISTÓRICO da ação")
    public void clicoNaTabHISTÓRICODaAção() {
        visualizarAcaoPage.clicarTABHistorico();
        addLogToReport("Cliquei na TAB historico");
    }

    @E("valido que o pedido de solicitação está registrado")
    public void validoQueOPedidoDeSolicitaçãoEstáRegistrado() {
        visualizarAcaoPage.validaRecusaNoHistorico(acao);
        addScreenshotToReport("Validei a recusa no histórico");
    }

    @E("preencho os dados do modal de recusar ação")
    public void preenchoOsDadosDoModalDeRecusarAção() {
        visualizarAcaoPage.preencherDadosRecusaAcao(acao);
        addScreenshotToReport("Preenchi as informações ");
    }

    @E("valido que o pedido de solicitação da ação desdobrada está registrado")
    public void validoQueOPedidoDeSolicitaçãoDaAçãoDesdobradaEstáRegistrado() {
        visualizarAcaoPage.validaRecusaAcaoDesdobradaNoHistorico(acao);
        addScreenshotToReport("Validei a recusa no histórico");
    }

    @Então("clico em Aceitar Alteração")
    public void clicoEmAceitarAlteração() {
        visualizarAcaoPage.clicarBtnAceitarAlteracao();
        addLogToReport("Cliquei no botão 'Aceitar alteração'.");
    }

    @Então("preencho a justificativa do modal de editar ação recusada")
    public void preenchoAJustificativaDoModalDeEditarAçãoRecusada() {
        visualizarAcaoPage.preencherJustificativaEditarAcaoRecusada(acaoEditada);
        addScreenshotToReport("Preenchi a justificativa");
    }

    @E("edito as informações iniciais da ação recusada")
    public void editoAsInformaçõesIniciaisDaAçãoRecusada() {
        visualizarAcaoPage.preencherDadosIniciasModalEditarAceitarAlteracaoAcao(acaoEditada);
        addScreenshotToReport("Ajustei os dados iniciais");
    }

    @E("clico na tab ACEITE da ação")
    public void clicoNaTabACEITEDaAção() {
        visualizarAcaoPage.clicarTABAceite();
        addLogToReport("Cliquei na tab aceite.");
    }

    @E("clico no botão Recusar Alteração")
    public void clicoNoBotãoRecusarAlteracao() {
        visualizarAcaoPage.clicarBtnRecusarAlteracao();
        addLogToReport("cliquei no botão recusar alteração");
    }

    @E("preencho a justificativa do modal de Reprovar alteração")
    public void preenchoAJustificativaDoModalDeReprovarAlteração() {
        visualizarAcaoPage.preencherJustificativaRecusarAlteracao(acao);
        addScreenshotToReport("preenchi a justificativa da recusa alteração");
    }

    @E("confirmo a Recusa da ação")
    public void confirmoARecusaDaAção() {
        visualizarAcaoPage.clicarBtnConfirmarRecusaAcao();
        addLogToReport("cliquei sobre o botão confirmar");
    }

    @E("valido o historico de alteração recusada")
    public void validoOHistoricoDeAlteraçãoRecusada() {
        visualizarAcaoPage.validarHistoricoAlteracaoRecusada(acao);
        addScreenshotToReport("validei o historico de alteração recusada");
    }

    @E("valido os detalhes da ação pública")
    public void validoOsDetalhesDaAçãoPública() {
        visualizarAcaoPage.validarDetalhesDaAcaoPublica(acao, responsavelAcao);
        addScreenshotToReport("validei os detalhes da ação pública");
    }

}
