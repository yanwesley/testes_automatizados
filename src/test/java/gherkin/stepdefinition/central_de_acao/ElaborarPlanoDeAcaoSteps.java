package gherkin.stepdefinition.central_de_acao;

import br.com.api.model.central_de_acoes.Acao;
import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.central_de_acoes.ResponsavelAcao;
import br.com.pom.sro.central_de_acao.ElaborarPlanoDeAcaoPage;
import br.com.pom.sro.central_de_acao.IncluirUmaAcaoPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.*;

import static br.com.api.GeradorDeMassa.*;

public class ElaborarPlanoDeAcaoSteps extends ReporterUtils {

    ElaborarPlanoDeAcaoPage elaborarPlanoDeAcaoPage = new ElaborarPlanoDeAcaoPage();
    IncluirUmaAcaoPage incluirUmaAcaoPage = new IncluirUmaAcaoPage();
    PlanoDeAcao planoDeAcao = getPlanoDeAcao();
    PlanoDeAcao planoDeAcaoEditado = getNewPlanoDeAcao();
    Acao acao = getAcao();
    Acao acaoEditada = getNewAcao();
    ResponsavelAcao responsavelAcao = getResponsavelAcao();
    ResponsavelAcao responsavelAcaoEditado = getResponsavelAcaoEditado();

    @Entao("deve exibir os detalhes do plano de ação na elaboração plano de ação")
    public void deveExibirOsDetalhesDoPlanoDeAçãoNaElaboraçãoPlanoDeAção() {
        elaborarPlanoDeAcaoPage.validarInformacoesDoPlano(planoDeAcao);
        addScreenshotToReport("Validei que o plano de ação está exibindo os dados cadastrados.");
    }

    @Dado("que eu esteja na tela de Elaborar Plano de Ação")
    public void queEuEstejaNaTelaDeElaborarDePlanoDeAção() {
        elaborarPlanoDeAcaoPage.validaExibicaoDaPagina();
        addScreenshotToReport("Validei exibição da página de Elaborar Plano de Ação.");
    }

    @Quando("eu clico no botão Criar uma ação")
    public void euClicoNoBotãoCriarUmaAção() {
        elaborarPlanoDeAcaoPage.clicarBtnAdicionarAcao();
        addLogToReport("Cliquei no botão para adicionar uma nova ação.");
    }

    @Entao("deve exibir as informações do plano de ação no modal de {string}")
    public void exibirAsInformaçõesDoPlanoDeAçãoNoModalDeIncluirAção(String title) {
        incluirUmaAcaoPage.validarDadosDoPlanoDeAcao(planoDeAcao, title);
        addScreenshotToReport("Validei os detalhes do plano de ação no modal de incluir nova ação.");
    }

    @Entao("ao preencher as informações iniciais")
    public void preenchoAsInformaçõesIniciais() {
        incluirUmaAcaoPage.preencherInformacoesIniciais(acao);
        addScreenshotToReport("Preenchi a informações iniciais");
    }

    @E("adicionar um responsável na ação")
    public void adicionoResponsávelNaAção() {
        incluirUmaAcaoPage.clicarBtnBuscarResponsavel();
        addLogToReport("Cliquei no botão 'Adicionar responsável'");
        incluirUmaAcaoPage.preencherDadosResponsavel(responsavelAcao);
        addScreenshotToReport("Preenchi os dados do responsável.");
        incluirUmaAcaoPage.clicarBtnConfirmarModalAdicionarResponsavel();
        addLogToReport("Cliquei no botão confirmar.");
    }

    @Então("clico no botão Salvar do Modal de Incluir Uma Ação")
    public void clicoNoBotãoSalvarDoModalDeIncluirUmaAção() {
        incluirUmaAcaoPage.clicarBtnSalvarIncluirAcao();
        addLogToReport("Cliquei no botão salvar.");
    }

    @E("a ação deve ser exibida na listagem de Elaboração de ações")
    public void aAçãoDeveSerExibidaNaListagemDeElaboraçãoDeAções() {
        elaborarPlanoDeAcaoPage.validarExibicaoDaAcaoInserida(acao, responsavelAcao, 1);
        addScreenshotToReport("Validei a exibição da ação.");
    }

    @E("que eu tenha {int} ação criada")
    public void queEuTenhaAçãoCriada(int quantidade) {
        elaborarPlanoDeAcaoPage.validarQueTenhaAcoes(quantidade);
        addScreenshotToReport("Validei que tem ação.");
    }

    @Quando("eu clico no botão editar da ação da linha {int}")
    public void euClicoNoBotãoEditarDaAçãoDaLinha(int linha) {
        elaborarPlanoDeAcaoPage.clicarBtnEditarAcao(linha);
        addLogToReport("Cliquei no botão editar ação da linha " + linha);
    }

    @E("ao editar as informações iniciais")
    public void aoEditarAsInformaçõesIniciais() {
        incluirUmaAcaoPage.preencherInformacoesIniciais(acaoEditada);
        addScreenshotToReport("Editei as informações iniciais.");
    }

    @E("remover o responsável da ação")
    public void removerOResponsavelDaAcao() {
        incluirUmaAcaoPage.clicarBtnRemoverResponsavel();
        addLogToReport("Cliquei para remover o responsável.");
        incluirUmaAcaoPage.validaModalDeRemoverResponsavel();
        addScreenshotToReport("Validei o modal de remover responsável.");
        incluirUmaAcaoPage.clicarBtnConfirmarModalRemoverResponsavel();
        addLogToReport("Cliquei no botão para confirmar a remoção do responsável.");
    }

    @E("a ação editada deve ser exibida na listagem de Elaboração de ações")
    public void aAçãoEditadaDeveSerExibidaNaListagemDeElaboraçãoDeAções() {
        elaborarPlanoDeAcaoPage.validarExibicaoDaAcaoInserida(acaoEditada, responsavelAcaoEditado, 1);
        addScreenshotToReport("Validei a exibição da ação editada.");
    }

    @Quando("eu clico no botão excluir da ação da linha {int}")
    public void euClicoNoBotãoExcluirDaAçãoDaLinha(int linha) {
        elaborarPlanoDeAcaoPage.clicarBtnExcluirAcao(linha);
        addLogToReport("Cliquei botão excluir ação.");
    }

    @Entao("deve exibir o modal para confirmar a exclusão")
    public void deveExibirOModalParaConfirmarAExclusão() {
        elaborarPlanoDeAcaoPage.validaModalDeExclusaoDaAcao();
        addScreenshotToReport("Validei a exibição do modal de excluir ação.");
    }

    @E("ao confirmar a exclusão")
    public void aoConfirmarAExclusão() {
        elaborarPlanoDeAcaoPage.clicarBtnExcluirModalExclusao();
        addLogToReport("Cliquei no botão 'Excluir' do modal de exclusão.");
    }

    @E("a ação não deve ser exibida na listagem de Elaboração de ações")
    public void aAçãoNãoDeveSerExibidaNaListagemDeElaboraçãoDeAções() {
        elaborarPlanoDeAcaoPage.validaQueAAcaoNaoEExibida();
        addScreenshotToReport("Validei que a ação não está mais sendo exibida.");
    }

    @E("adicionar um novo responsável na ação")
    public void adicionarUmNovoResponsávelNaAção() {
        incluirUmaAcaoPage.preencherDadosResponsavel(responsavelAcaoEditado);
        addScreenshotToReport("Preenchi os dados do responsável.");
        incluirUmaAcaoPage.clicarBtnConfirmarModalAdicionarResponsavel();
        addLogToReport("Cliquei no botão confirmar.");
    }

    @E("exibir os detalhes da ação com status {string} no plano de ação")
    public void exibirOsDetalhesDaAçãoComStatusNoPlanoDeAção(String status) {
        elaborarPlanoDeAcaoPage.validarDadosDaAcaoVisualizacao(acao, responsavelAcao, status, 1);
        addScreenshotToReport("Validei os dados da ação.");
    }

    @E("clico no botão de buscar responsável")
    public void clicoNoBotãoDeBuscarResponsável() {
        incluirUmaAcaoPage.clicarBtnBuscarResponsavel();
        addLogToReport("Cliquei no botão 'Adicionar responsável'");
    }

    @Entao("preencho o nome do plano")
    public void preenchoONomeDoPlano() {
        elaborarPlanoDeAcaoPage.preenchoNomePlano(planoDeAcao);
        addScreenshotToReport("Preenchi o nome do plano");
    }

    @E("adicionar um solicitante")
    public void adicionoUmSolicitante() {
        incluirUmaAcaoPage.clicarBtnBuscarResponsavel();
        addLogToReport("Cliquei no botão 'Adicionar responsável'");
        incluirUmaAcaoPage.preencherDadosResponsavel(responsavelAcao);
        addScreenshotToReport("Preenchi os dados do responsável.");
        incluirUmaAcaoPage.clicarBtnConfirmarModalAdicionarResponsavel();
        addLogToReport("Cliquei no botão confirmar.");
    }

    @Entao("valido que o plano avulso foi criado")
    public void validoQuePlanoAvulsoFoiCriado() {
        elaborarPlanoDeAcaoPage.validoPlanoAvulso(planoDeAcao, responsavelAcao);
        addScreenshotToReport("validei as informações do plano avulso");
    }

    @E("adicionar um novo responsável no plano de ação avulso")
    public void adicionarUmNovoResponsávelNaAçãoAvulsa() {
        incluirUmaAcaoPage.clicarBtnSolicitado();
        planoDeAcao.setSolicitanteMatricula(incluirUmaAcaoPage.preencherDadosResponsavel(responsavelAcao));
        planoDeAcao.setSolicitante(responsavelAcao.getResponsavel());
        addScreenshotToReport("Preenchi os dados do responsável.");
        incluirUmaAcaoPage.clicarBtnConfirmarModalAdicionarResponsavel();
        addLogToReport("Cliquei no botão confirmar.");
        incluirUmaAcaoPage.clicarBtnCriarPlanoAvulso();
        addLogToReport("Cliquei no botão confirmar.");
    }

    @Dado("que clico em editar plano")
    public void queClicoEmEditarPlano() {
        elaborarPlanoDeAcaoPage.clicarEditarPlano();
        addLogToReport("cliquei no botão editar plano");
    }

    @Entao("preencho o nome do plano editado")
    public void preenchoONomeDoPlanoEditado() {
        elaborarPlanoDeAcaoPage.editeiNomePlano(planoDeAcaoEditado);
        addScreenshotToReport("editei o nome do plano");
    }

    @E("adicionar um novo responsável no plano de ação avulso editado")
    public void adicionarUmNovoResponsávelNoPlanoDeAçãoAvulsoEditado() {
        incluirUmaAcaoPage.clicarBtnSolicitado();
        planoDeAcao.setSolicitanteMatricula(incluirUmaAcaoPage.preencherDadosResponsavel(responsavelAcaoEditado));
        planoDeAcao.setSolicitante(responsavelAcao.getResponsavel());
        addScreenshotToReport("Preenchi os dados do responsável.");
        incluirUmaAcaoPage.clicarBtnConfirmarModalAdicionarResponsavel();
        addLogToReport("Cliquei no botão confirmar.");
        incluirUmaAcaoPage.clicarBtnCriarPlanoAvulso();
        addLogToReport("Cliquei no botão confirmar.");
    }

    @Entao("valido que o plano avulso foi editado")
    public void validoQueOPlanoAvulsoFoiEditado() {
        elaborarPlanoDeAcaoPage.validoPlanoAvulso(planoDeAcaoEditado, responsavelAcaoEditado);
        addScreenshotToReport("validei as informações do plano avulso");
    }

    @Dado("que clico em incluir uma ação")
    public void queClicoEmIncluirUmaAção() {
        elaborarPlanoDeAcaoPage.clicarBtnIncluirAcao();
        addLogToReport("cliquei sobre o botão incluir ação");
    }

    @Entao("valido a ação do plano avulso")
    public void validarAcaoPlanoAvulso() {
        elaborarPlanoDeAcaoPage.validoPlanoAvulso(planoDeAcaoEditado, responsavelAcaoEditado);

        elaborarPlanoDeAcaoPage.validarAcaoPlanoAvulso(acao, responsavelAcao);
        addScreenshotToReport("validei a ação do plano avulso");
    }

    @E("clico na ação avulsa")
    public void clicoNaAçãoAvulsa() {
        elaborarPlanoDeAcaoPage.clicarAcaoAvulsa();
        addScreenshotToReport("Expandi a ação avulsa");
    }

    @Entao("excluo a ação avulsa")
    public void excluoAçãoAvulsa() {
        elaborarPlanoDeAcaoPage.excluirAcaoAvulsa();
        addLogToReport("cliquei no botão excluir ação");
    }

    @E("clico no boto confirmar exclusão")
    public void clicoNoBotoConfirmarExclusão() {
        elaborarPlanoDeAcaoPage.clicarBtnConfirmarExclusao();
        addLogToReport("cliquei no botão confirmar exclusão");
    }
}
