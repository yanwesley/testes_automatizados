package gherkin.stepdefinition.informacoes;

import br.com.api.model.informacoes.AtualizacaoDoSistema;
import br.com.pom.sro.informacoes.AtualizacaoInformacoesPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.getAtualizacaoDoSistema;

public class AtualizacoesInformacoesSteps extends ReporterUtils {

    AtualizacaoInformacoesPage atualizacaoInformacoesPage = new AtualizacaoInformacoesPage();
    AtualizacaoDoSistema atualizacaoDoSistema = getAtualizacaoDoSistema();

    @E("acesso a TAB Atualização no SRO")
    public void acessoATABAtualizaçãoNoSRO() {
        atualizacaoInformacoesPage.clicarTABAtualizacao();
        addLogToReport("Cliquei na TAB Atualização");
    }

    @Então("valido as informações da atualização do sistema")
    public void validoAsInformaçõesDaAtualizaçãoDoSistema() {
        atualizacaoInformacoesPage.validarDados(atualizacaoDoSistema, 1);
        addScreenshotToReport("Validei os dados");
    }
}
