package gherkin.stepdefinition.informacoes;

import br.com.api.model.informacoes.AtualizacaoDoSistema;
import br.com.api.model.informacoes.BlocoDeTexto;
import br.com.pom.sro.informacoes.DocumentacaoInformacoesPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.getAtualizacaoDoSistema;
import static br.com.api.GeradorDeMassa.getBlocoDeTexto;

public class DocumentacaoInformacoesSteps extends ReporterUtils {

    DocumentacaoInformacoesPage documentacaoInformacoesPage = new DocumentacaoInformacoesPage();
    AtualizacaoDoSistema atualizacaoDoSistema = getAtualizacaoDoSistema();
    BlocoDeTexto blocoDeTexto = getBlocoDeTexto();

    @E("acesso a TAB DOCUMENTAÇÃO no SRO")
    public void acessoATABDOCUMENTAÇÃONoSRO() {
        documentacaoInformacoesPage.clicarTABDocumentacao();
        addLogToReport("Cliquei na TAB Documentação");
    }

    @Então("valido as informações de Documentação oficial do projeto  no SRO")
    public void validoAsInformaçõesDeDocumentaçãoOficialDoProjetoNoSRO() {
        documentacaoInformacoesPage.validaInformacoesDeDocumentacaoNoSRO(atualizacaoDoSistema);
        addScreenshotToReport("Valido as informações de Documentação oficial do projeto  no SRO");
    }

    @Então("valido que o bloco de texto Documentação do projeto no SRO")
    public void validoQueOBlocoDeTextoDocumentaçãoDoProjetoNoSRO() {
        documentacaoInformacoesPage.validaDadosBlocoDeTexto(blocoDeTexto, 1);
        addScreenshotToReport("Validado dados do bloco do texto.");
    }
}
