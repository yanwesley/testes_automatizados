package gherkin.stepdefinition;

import br.com.api.model.central_de_acoes.PlanoDeAcao;
import br.com.api.model.incidente.Incidente;
import br.com.pom.sro.LoginPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.*;

import java.util.List;

import static br.com.api.GeradorDeMassa.getIncidente;
import static br.com.api.GeradorDeMassa.getPlanoDeAcao;

public class LoginSteps extends ReporterUtils {

    LoginPage loginPage = new LoginPage();
    PlanoDeAcao planoDeAcao = getPlanoDeAcao();
    Incidente incidente = getIncidente();

    @Dado("^que eu estou na página de login$")
    public void queEuEstouNaPaginaDeLogin() {
        loginPage.naoImplementado();
    }

    @Quando("preencho com usuário e senha")
    public void preenchoComUsuarioESenha(List<String> list) {
        loginPage.naoImplementado();
    }

    @E("clico em efetuar o login")
    public void clicoEmEfetuarOLogin() {
        loginPage.naoImplementado();
    }

    @Então("deve exibir o módulo do SRO")
    public void deveExibirOModuloDoSRO() {
        loginPage.naoImplementado();
    }

    @Então("deve exibir a mensagem de login inválido")
    public void deveExibirAMensagemDeLoginInvalido() {
        loginPage.naoImplementado();
    }

    @Dado("que eu esteja logado com usuário CCO VIA")
    public void queEuEstejaLogadoComUsuarioCCOVIA() {
        loginPage.selecionarUsuarioCCOVia();
        addScreenshotToReport("Estou logado com usuário com perfil CCO");
    }

    @Dado("que eu esteja logado com usuário CCO")
    public void queEuEstejaLogadoComUsuarioCCO() {
        loginPage.selecionarUsuarioCCO();
        addScreenshotToReport("Estou logado com usuário com perfil CCO");
    }

    @Dado("que eu esteja logado com usuário SRO")
    public void queEuEstejaLogadoComUsuarioSRO() {
        loginPage.selecionarUsuarioSRO();
        addScreenshotToReport("Estou logado com usuário com perfil SRO");
    }

    @Dado("que eu esteja logado com usuário CIPIA Via")
    public void queEuEstejaLogadoComUsuarioCipiaVia() {
        loginPage.selecionarUsuarioCipiaVia();
        addScreenshotToReport("Estou logado com usuário com perfil CIPIA Via");
    }

    @E("que eu esteja logado com usuário CIPIA Operação")
    public void queEuEstejaLogadoComUsuárioCIPIAOperação() {
        loginPage.selecionarUsuarioCipiaOperacao();
        addScreenshotToReport("Estou logado com usuário com perfil CIPIA Operação");
    }

    @E("que eu esteja logado com usuário TO")
    public void queEuEstejaLogadoComUsuárioTO() {
        loginPage.selecionarUsuarioTO();
        addScreenshotToReport("Estou logado com usuário com perfil TO");
    }

    @E("que eu esteja logado com usuário SST")
    public void queEuEstejaLogadoComUsuárioSST() {
        loginPage.selecionarUsuarioSST();
        addScreenshotToReport("Estou logado com usuário com perfil SST");
    }

    @E("que eu esteja logado com usuário Meio Ambiente")
    public void queEuEstejaLogadoComUsuárioMeioAmbiente() {
        loginPage.selecionarUsuarioMeioAmbiente();
        addScreenshotToReport("Estou logado com usuário com perfil Meio Ambiente");
    }

    @E("que eu esteja logado com usuário Controle de Perdas")
    public void queEuEstejaLogadoComUsuárioControleDePerdas() {
        loginPage.selecionarUsuarioControleDePerdas();
        addScreenshotToReport("Estou logado com usuário com perfil Controle de Perdas");
    }

    @E("que eu esteja logado com usuário Cipia Material Rodante")
    public void queEuEstejaLogadoComUsuárioCipiaMaterialRodante() {
        loginPage.selecionarUsuarioCipiaMaterialRodante();
        addScreenshotToReport("Estou logado com usuário com perfil Cipia Material Rodante");
    }

    @E("que eu esteja logado com usuário Segurança Patrimonial")
    public void queEuEstejaLogadoComUsuárioSegurançaPatrimonial() {
        loginPage.selecionarUsuarioCipiaSegurancaPatrimonial();
        addScreenshotToReport("Estou logado com usuário com perfil Segurança Patrimonial");
    }

    @Dado("que eu esteja logado com usuário CIPIA Técnica")
    public void queEuEstejaLogadoComUsuárioCIPIATecnica() {
        loginPage.selecionarUsuarioCIPIATecnica();
        addScreenshotToReport("Estou logado com usuário com perfil CIPIA Técnica");
    }

    @Quando("que eu esteja logado com usuário CIPIA Presidente")
    public void queEuEstejaLogadoComUsuárioCIPIAPresidente() {
        loginPage.selecionarUsuarioCIPIAPresidente();
        addScreenshotToReport("Estou logado com usuário com perfil CIPIA Presidente");
    }

    @Entao("que eu esteja logado com o Dono do Plano")
    public void queEuEstejaLogadoComODonoDoPlano() {
        loginPage.preencherUsuarioDonoDoPlano(planoDeAcao);
        addLogToReport("Estou logado com usuário " + planoDeAcao.getSolicitante() + " matricula " + planoDeAcao.getSolicitanteMatricula());
    }

    @Dado("que eu esteja logado com o Criador do incidente")
    public void queEuEstejaLogadoComOCriadorDoIncidente() {
        loginPage.preencherUsuarioCriadorDoIncidente(incidente);
        addLogToReport("Estou logado com usuário " + incidente.getNomeUsuarioCriacao() + " matricula " + incidente.getMatriculaUsuarioCriacao());
    }

    @E("que eu esteja logado com usuário Financeiro")
    public void queEuEstejaLogadoComUsuárioFinanceiro() {
        loginPage.selecionarUsuarioFinanceiro();
        addScreenshotToReport("Estou logado com usuário com perfil Financeiro");
    }
}
