package gherkin.stepdefinition.configuracoes;

import br.com.api.model.configuracoes.ConfiguracaoVia;
import br.com.pom.sro.configuracoes.ConfiguracoesViaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

import static br.com.api.GeradorDeMassa.getConfiguracaoVia;

public class ViaConfiguracoesSROSteps extends ReporterUtils {

    ConfiguracoesViaPage configuracoesViaPage = new ConfiguracoesViaPage();
    ConfiguracaoVia configuracaoVia = getConfiguracaoVia();
    ConfiguracaoVia configuracaoViaEditada = getConfiguracaoVia();

    @E("acesso a TAB Via das configurações")
    public void acessoATABViaDasConfigurações() {
        configuracoesViaPage.acessoATABViaDasConfiguracoes();
        addScreenshotToReport("Acessei a tab 'Via'");
    }

    @Então("clico em Adicionar Configuração de Via")
    public void clicoEmAdicionarConfiguraçãoDeVia() {
        configuracoesViaPage.clicarBtnAdicionarConfiguracao();
        addLogToReport("Cliquei no botão 'Adicionar Configuração'");
    }

    @E("preencho os dados da configuração da Via")
    public void preenchoOsDadosDaConfiguraçãoDaVia() {
        configuracoesViaPage.preencherModalAddConfiguracao(configuracaoVia);
        addScreenshotToReport("Preenchi os dados da configuração.");
    }

    @E("clico em salvar no modal de adicionar configuração da Via")
    public void clicoEmSalvarNoModalDeAdicionarConfiguraçãoDaVia() {
        configuracoesViaPage.clicarBtnSalvarModalAddConfiguracao();
        addLogToReport("Cliquei no botão 'Salvar' no modal de adicionar configuração.");
    }

    @E("valido os dados da configuração da via adicionada")
    public void validoOsDadosDaConfiguraçãoDaViaAdicionada() {
        configuracoesViaPage.validarDadosConfiguracao(configuracaoVia);
        addScreenshotToReport("Validei os dados da configuração adicionada.");
    }

    @Entao("clico no botão Editar configuração Via")
    public void clicoNoBotãoEditarConfiguraçãoVia() {
        configuracoesViaPage.clicarBtnEditarConfiguracao();
        addLogToReport("Cliquei no botão 'Editar configuração'");
    }

    @E("edito os dados da configuração da Via")
    public void editoOsDadosDaConfiguraçãoDaVia() {
        configuracoesViaPage.preencherModalAddConfiguracao(configuracaoViaEditada);
        addScreenshotToReport("Preenchi os dados da configuração.");
    }

    @E("valido os dados da configuração da via editada")
    public void validoOsDadosDaConfiguraçãoDaViaEditada() {
        configuracoesViaPage.validarDadosConfiguracao(configuracaoViaEditada);
        addScreenshotToReport("Validei os dados da configuração adicionada.");
    }

    @Quando("clico no botão Adicionar Faixa")
    public void clicoNoBotãoAdicionarFaixa() {
        configuracoesViaPage.clicarBtnAdicionarFaixa();
        addLogToReport("cliquei no botão 'Adicionar Faixa'");
    }

    @E("preencho os dados da faixa no modal")
    public void preenchoOsDadosDaFaixaNoModal() {
        configuracoesViaPage.preencherDadosFaixa(configuracaoVia);
        addScreenshotToReport("Preenchi os dados da Faixa");
    }

    @E("clico no botão Salvar no modal de Faixa")
    public void clicoNoBotãoSalvarNoModalDeFaixa() {
        configuracoesViaPage.clicarBtnSalvarModalFaixa();
        addScreenshotToReport("Preenchi os dados do modal de Faixas.");
    }

    @E("clico em Salvar Informações na tela de Configurações Via")
    public void clicoEmSalvarInformaçõesNaTelaDeConfiguraçõesVia() {
        configuracoesViaPage.clicarBtnSalvarInformacoes();
        addLogToReport("Cliquei no botão 'Salvar Informações'");
    }

    @Então("valido a faixa adicionada na linha {int}")
    public void validoAFaixaAdicionadaNaLinha(int linha) {
        configuracoesViaPage.validaFaixaAdicionada(configuracaoVia, linha);
        addScreenshotToReport("Validei a linha adicionada.");
    }

    @Quando("clico no botão Editar Faixa na linha {int}")
    public void clicoNoBotãoEditarFaixaNaLinha(int linha) {
        configuracoesViaPage.clicarBtnEditarFaixa(linha);
        addLogToReport("Cliquei no botão editar faixa");
    }

    @E("edito os dados da faixa no modal")
    public void editoOsDadosDaFaixaNoModal() {
        configuracoesViaPage.preencherDadosFaixa(configuracaoViaEditada);
        addScreenshotToReport("Editei os dados da Faixa");
    }

    @Então("valido a faixa editada na linha {int}")
    public void validoAFaixaEditadaNaLinha(int linha) {
        configuracoesViaPage.validaFaixaAdicionada(configuracaoViaEditada, linha);
        addScreenshotToReport("Validei a linha editada.");
    }

    @Quando("clico no botão Excluir Faixa na linha {int}")
    public void clicoNoBotãoExcluirFaixaNaLinha(int linha) {
        configuracoesViaPage.clicarBtnExcluirFaixa(linha);
        addLogToReport("Cliqeui no botão para excluir a faixa");
    }

    @E("confirmo a exclusão da faixa")
    public void confirmoAExclusãoDaFaixa() {
        configuracoesViaPage.validaModalExclusaoFaixa();
        addScreenshotToReport("Validei o modal de excluir faixa.");
        configuracoesViaPage.clicarBtnConfirmaExclusao();
        addLogToReport("Cliquei no botão para confirmar a exclusão.");
    }

    @Então("valido a faixa excluida na linha {int}")
    public void validoAFaixaExcluidaNaLinha(int linha) {
        configuracoesViaPage.validaFaixaExcluida(linha);
        addScreenshotToReport("Faixa foi excluída.");
    }
}
