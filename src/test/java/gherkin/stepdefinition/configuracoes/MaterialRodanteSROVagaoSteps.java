package gherkin.stepdefinition.configuracoes;

import br.com.api.model.configuracoes.ConfiguracaoMaterialRodanteVagao;
import br.com.pom.sro.configuracoes.MaterialRodanteSROVagaoPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

import static br.com.api.GeradorDeMassa.getConfiguracaoMaterialRodanteVagao;
import static br.com.api.GeradorDeMassa.getNewConfiguracaoMaterialRodanteVagao;

public class MaterialRodanteSROVagaoSteps extends ReporterUtils {

    ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagao = getConfiguracaoMaterialRodanteVagao();
    ConfiguracaoMaterialRodanteVagao configuracaoMaterialRodanteVagaoEditado = getNewConfiguracaoMaterialRodanteVagao();
    MaterialRodanteSROVagaoPage materialRodanteSROVagaoPage = new MaterialRodanteSROVagaoPage();
    boolean isEdicao;


    @E("clico no separator para expandir o formulário de configuração de vagão")
    public void clicoNoSeparatorParaExpandirOFormulárioDeConfiguraçãoDeVagão() {
        materialRodanteSROVagaoPage.clicarSeparatorConfiguracaoVagao();
        addLogToReport("Clicou no separator de configuração do Vagão");
    }

    @Entao("clico no botão Adicionar bloco de configuração de Vagão")
    public void clicoNoBotãoAdicionarBlocoDeConfiguraçãoDeVagão() {
        materialRodanteSROVagaoPage.clicarBtnAdicionarNovaConfiguracaoVagao();
        addLogToReport("Clicou no botão para adicionar uma nova configuração de Vagão");
    }

    @E("preenche a configuração do vagão da linha {int}")
    public void preencheAConfiguraçãoDoVagão(int linha) {
        isEdicao = false;
        //Configuração vagão
        materialRodanteSROVagaoPage.preencheConfiguracaoVagao(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Preencheu configuração do vagão");
        //Limites do rodeiro
        materialRodanteSROVagaoPage.preencheLimitesDoRodeiro(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Preencheu limites do rodeiro");
        //Informação sobre a manga de eixo
        materialRodanteSROVagaoPage.preencheInformacaoSobreMangaDeEixo(configuracaoMaterialRodanteVagao, linha, isEdicao);
        addScreenshotToReport("Preencheu informação sobre a manga de eixo");
        //Tipo de manutenção
        materialRodanteSROVagaoPage.preencheTipoDeManutencao(configuracaoMaterialRodanteVagao, linha, isEdicao);
        addScreenshotToReport("Preencheu tipo de manutenção");
        //Limite de cunhas de fricção
        materialRodanteSROVagaoPage.preencheLimiteCunhaDeFriccao(configuracaoMaterialRodanteVagao, linha, isEdicao);
        addScreenshotToReport("Preencheu limite de cunhas de fricção");
        //Limite de diferença de botões
        materialRodanteSROVagaoPage.preencheLimiteDeDiferencaDeBotoes(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Preencheu limite de diferença de botões");
        //Limites de prato de pião
        materialRodanteSROVagaoPage.preencheLimiteDePratoDePiao(configuracaoMaterialRodanteVagao, linha, isEdicao);
        addScreenshotToReport("Preencheu limites de prato de pião");
        //Limites de Folga de Prato Pião
        materialRodanteSROVagaoPage.preencheLimiteDeFolgaDePratoPiao(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Preencheu limites de Folga de Prato Pião");
        //Limites de amparo balanço
        materialRodanteSROVagaoPage.preencheLimitesAmparadoBalanco(configuracaoMaterialRodanteVagao, linha, isEdicao);
        addScreenshotToReport("Preencheu limites de amparo balanço");
        //Limites de altura de engate
        materialRodanteSROVagaoPage.preencheLimitesDeAlturaDeEngate(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Preencheu limites de altura de engate");
        //Limite de molas
        materialRodanteSROVagaoPage.preencheLimitesDeMolas(configuracaoMaterialRodanteVagao, linha, isEdicao);
        addScreenshotToReport("Preencheu limite de molas");
        //Mola cunha
        materialRodanteSROVagaoPage.preencheMolaCunha(configuracaoMaterialRodanteVagao,linha, isEdicao);
        addScreenshotToReport("Preencheu mola cunha");
        //Condição do vagão quanto a carga
        //Informação do rolamento
        //Cor e ano de substituição ou lubrificação do rolamento
    }

    @Entao("clico no botão Salvar Informações da configuração de Vagão")
    public void clicoNoBotãoSalvarInformaçõesDaConfiguraçãoDeVagão() {
        materialRodanteSROVagaoPage.clicarBtnSalvarInformacoes();
        addLogToReport("Clicou no botão Salvar informações");
    }

    @E("valida os dados preenchidos da configuração do vagão")
    public void validaOsDadosPreenchidosDaConfiguraçãoDoVagão() {
        //Configuração vagão
        materialRodanteSROVagaoPage.validaConfiguracaoVagao(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou configuração de vagão");
        //Limites do rodeiro
        materialRodanteSROVagaoPage.validaLimitesDoRodeiro(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou limites do rodeiro");
        //Informação sobre a manga de eixo
        materialRodanteSROVagaoPage.validaInformacaoSobreMangaDeEixo(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou informação sobre a manga de eixo");
        //Tipo de manutenção
        materialRodanteSROVagaoPage.validaTipoDeManutencao(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou tipo de manutenção");
        //Limite de cunhas de fricção
        materialRodanteSROVagaoPage.validaLimiteCunhaDeFriccao(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou limite de cunhas de fricção");
        //Limite de diferença de botões
        materialRodanteSROVagaoPage.validaLimiteDeDiferencaDeBotoes(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou limite de diferença de botões");
        //Limites de prato de pião
        materialRodanteSROVagaoPage.validaLimiteDePratoDePiao(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou limites de prato de pião");
        //Limites de Folga de Prato Pião
        materialRodanteSROVagaoPage.validaLimiteDeFolgaDePratoPiao(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou limites de Folga de Prato Pião");
        //Limites de amparo balanço
        materialRodanteSROVagaoPage.validaLimitesAmparadoBalanco(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou limites de amparo balanço");
        //Limites de altura de engate
        materialRodanteSROVagaoPage.validaLimitesDeAlturaDeEngate(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou imites de altura de engate");
        //Limite de molas
        materialRodanteSROVagaoPage.validaLimitesDeMolas(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou limite de molas");
        //Mola cunha
        materialRodanteSROVagaoPage.validaMolaCunha(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou mola cunha");
        //Condição do vagão quanto a carga
        //Informação do rolamento
        //Cor e ano de substituição ou lubrificação do rolamento
    }

    @Entao("valida o bloco de configuração de vagão")
    public void validaQueOStatusDoBlocoDeConfiguraçãoDeVagão() {
        materialRodanteSROVagaoPage.validaStatusBlocoDeConfiguracaoVagao(configuracaoMaterialRodanteVagao);
        addScreenshotToReport("Validou bloco de configuração do vagão");
    }

    @E("clica no botão editar do bloco de configuração do vagão")
    public void clicaNoBotãoEditaDoBlocoDeConfiguraçãoDoVagão() {
        materialRodanteSROVagaoPage.clicarBtnEditarDoBlocoDeConfigVagao();
        addLogToReport("Clicou no botão editar");
    }

    @Dado("edito os dados da configuração do vagão da linha {int}")
    public void editoOsDadosDaConfiguraçãoDoVagão(int linha) {
        isEdicao = true;
        //Configuração vagão
        materialRodanteSROVagaoPage.preencheConfiguracaoVagao(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Editou a configuração de vagão");
        //Limites do rodeiro
        materialRodanteSROVagaoPage.preencheLimitesDoRodeiro(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Editou limite do rodeiro");
        //Informação sobre a manga de eixo
        materialRodanteSROVagaoPage.preencheInformacaoSobreMangaDeEixo(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        addScreenshotToReport("Editou informação sobre a manga de eixo");
        //Tipo de manutenção
        materialRodanteSROVagaoPage.preencheTipoDeManutencao(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        addScreenshotToReport("Editou tipo de manutenção");
        //Limite de cunhas de fricção
        materialRodanteSROVagaoPage.preencheLimiteCunhaDeFriccao(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        addScreenshotToReport("Editou limite de cunhas de fricção");
        //Limite de diferença de botões
        materialRodanteSROVagaoPage.preencheLimiteDeDiferencaDeBotoes(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Editou limite de diferença de botões");
        //Limites de prato de pião
        materialRodanteSROVagaoPage.preencheLimiteDePratoDePiao(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        addScreenshotToReport("Editou limites de prato de pião");
        //Limites de Folga de Prato Pião
        materialRodanteSROVagaoPage.preencheLimiteDeFolgaDePratoPiao(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Editou limites de Folga de Prato Pião");
        //Limites de amparo balanço
        materialRodanteSROVagaoPage.preencheLimitesAmparadoBalanco(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        addScreenshotToReport("Editou limites de amparo balanço");
        //Limites de altura de engate
        materialRodanteSROVagaoPage.preencheLimitesDeAlturaDeEngate(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Editou limites de altura de engate");
        //Limite de molas
        materialRodanteSROVagaoPage.preencheLimitesDeMolas(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        addScreenshotToReport("Editou limite de molas");
        //Mola cunha
        materialRodanteSROVagaoPage.preencheMolaCunha(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        addScreenshotToReport("Editou mola cunha");
    }

    @E("valida o bloco de configuração de vagão editado")
    public void validaOBlocoDeConfiguraçãoDeVagãoEditado() {
        materialRodanteSROVagaoPage.validaStatusBlocoDeConfiguracaoVagao(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou o bloco de configuração de vagão editado");
    }

    @Entao("valida os dados preenchidos da configuração do vagão editado")
    public void validaOsDadosPreenchidosDaConfiguraçãoDoVagãoEditado() {
        //Configuração vagão
        materialRodanteSROVagaoPage.validaConfiguracaoVagao(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição da configuração vagão");
        //Limites do rodeiro
        materialRodanteSROVagaoPage.validaLimitesDoRodeiro(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição dos limites do rodeiro");
        //Informação sobre a manga de eixo
        materialRodanteSROVagaoPage.validaInformacaoSobreMangaDeEixo(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição da informação sobre a manga de eixo");
        //Tipo de manutenção
        materialRodanteSROVagaoPage.validaTipoDeManutencao(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição do tipo de manutenção");
        //Limite de cunhas de fricção
        materialRodanteSROVagaoPage.validaLimiteCunhaDeFriccao(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição do limite de cunhas de fricção");
        //Limite de diferença de botões
        materialRodanteSROVagaoPage.validaLimiteDeDiferencaDeBotoes(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição do limite de diferença de botões");
        //Limites de prato de pião
        materialRodanteSROVagaoPage.validaLimiteDePratoDePiao(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição dos limites de prato de pião");
        //Limites de Folga de Prato Pião
        materialRodanteSROVagaoPage.validaLimiteDeFolgaDePratoPiao(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição dos limites de Folga de Prato Pião");
        //Limites de amparo balanço
        materialRodanteSROVagaoPage.validaLimitesAmparadoBalanco(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição dos limites de amparo balanço");
        //Limites de altura de engate
        materialRodanteSROVagaoPage.validaLimitesDeAlturaDeEngate(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição dos limites de altura de engate");
        //Limite de molas
        materialRodanteSROVagaoPage.validaLimitesDeMolas(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição do limite de molas");
        //Mola cunha
        materialRodanteSROVagaoPage.validaMolaCunha(configuracaoMaterialRodanteVagaoEditado);
        addScreenshotToReport("Validou edição da mola cunha");
    }

    @E("valida a mensagem de preenchimento do Nome da manga duplicado na configuração de vagão na linha {int}")
    public void validaAMensagemDePreenchimentoDoNomeDaMangaDuplicadoNaConfiguraçãoDeVagãoNaLinha(int linha) {
        isEdicao = false;
        materialRodanteSROVagaoPage.preencheInformacaoSobreMangaDeEixo(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        materialRodanteSROVagaoPage.validaMsgNomeDuplicadoInformacaoSobreMangaDeEixo();
        addScreenshotToReport("Validou mensagem de campo preenchido duplicado");
    }

    @E("valida a mensagem de preenchimento do nome do tipo de manutenção duplicado na configuração de vagão na linha {int}")
    public void validaAMensagemDePreenchimentoDoNomeDoTipoDeManutençãoDuplicadoNaConfiguraçãoDeVagãoNaLinha(int linha) {
        isEdicao = false;
        materialRodanteSROVagaoPage.preencheTipoDeManutencao(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        materialRodanteSROVagaoPage.validaMsgTipoDuplicadoTipoDeManutencao();
        addScreenshotToReport("Validou mensagem de campo preenchido duplicado");
    }

    @E("valida a mensagem de preenchimento do nome do limite da cunha de fricção duplicado na configuração de vagão na linha {int}")
    public void validaAMensagemDePreenchimentoDoNomeDoLimiteDaCunhaDeFricçãoDuplicadoNaConfiguraçãoDeVagãoNaLinha(int linha) {
        isEdicao = false;
        materialRodanteSROVagaoPage.preencheLimiteCunhaDeFriccao(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        materialRodanteSROVagaoPage.validaMsgNomeDuplicadoLimiteCunhaFriccao();
        addScreenshotToReport("Validou mensagem de campo preenchido duplicado");
    }

    @E("valida a mensagem de preenchimento do nome de limite do prato pião duplicado na configuração de vagão na linha {int}")
    public void validaAMensagemDePreenchimentoDoNomeDeLimiteDoPratoPiãoDuplicadoNaConfiguraçãoDeVagãoNaLinha(int linha) {
        isEdicao = false;
        materialRodanteSROVagaoPage.preencheLimiteDePratoDePiao(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        materialRodanteSROVagaoPage.validaMsgNomeDuplicadoLimiteDePratoDePiao();
        addScreenshotToReport("Validou mensagem de campo preenchido duplicado");
    }

    @E("valida a mensagem de preenchimento do nome do limite de amparo balanço duplicado na configuração de vagão na linha {int}")
    public void validaAMensagemDePreenchimentoDoNomeDoLimiteDeAmparoBalançoDuplicadoNaConfiguraçãoDeVagãoNaLinha(int linha) {
        isEdicao = false;
        materialRodanteSROVagaoPage.preencheLimitesAmparadoBalanco(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        materialRodanteSROVagaoPage.validaMsgNomeDuplicadoLimitesAmparadoBalanco();
        addScreenshotToReport("Validou mensagem de campo preenchido duplicado");
    }

    @E("valida a mensagem de preenchimento do nome dos limites das molas duplicado na configuração de vagão na linha {int}")
    public void validaAMensagemDePreenchimentoDoNomeDosLimitesDasMolasDuplicadoNaConfiguraçãoDeVagãoNaLinha(int linha) {
        isEdicao = false;
        materialRodanteSROVagaoPage.preencheLimitesDeMolas(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        materialRodanteSROVagaoPage.validaMsgNomeDuplicadoLimitesDeMolas();
        addScreenshotToReport("Validou mensagem de campo preenchido duplicado");
    }

    @E("valida a mensagem de preenchimento do nome da mola cunha duplicado na configuração de vagão na linha {int}")
    public void validaAMensagemDePreenchimentoDoNomeDaMolaCunhaDuplicadoNaConfiguraçãoDeVagãoNaLinha(int linha) {
        isEdicao = false;
        materialRodanteSROVagaoPage.preencheMolaCunha(configuracaoMaterialRodanteVagaoEditado, linha, isEdicao);
        materialRodanteSROVagaoPage.validaMsgNomeDuplicadoMolaCunha();
        addScreenshotToReport("Validou mensagem de campo preenchido duplicado");
    }
}
