package gherkin.stepdefinition.configuracoes;

import br.com.api.model.configuracoes.ConfiguracoesDashboard;
import br.com.pom.sro.configuracoes.DashboardSROPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;

import static br.com.api.GeradorDeMassa.getConfiguracoesDashboard;

public class DashboardSROSteps extends ReporterUtils {

    DashboardSROPage configuracoesSROPage = new DashboardSROPage();
    ConfiguracoesDashboard configuracoesDashboard = getConfiguracoesDashboard();

    @E("acesso a TAB Dashboard das configurações")
    public void acessoATABDashboardDasConfigurações() {
        configuracoesSROPage.clicarTabDashboard();
        addLogToReport("Clicou na TAB Dashboard");
    }

    @E("clico no botão para Salvar as configurações do dashboard")
    public void clicoNoBotãoParaSalvarAsConfiguraçõesDoDashboard() {
        configuracoesSROPage.clicarBtnSalvarConfiguraçõesDashboard();
        addLogToReport("Clicou no botão Salvar as configurações");
    }

    @Então("preencho a tabela de Tolerância para acidentes")
    public void preenchoATabelaDeTolerânciaParaAcidentes() {
        configuracoesSROPage.preencheToleranciaAcidenteCentral(configuracoesDashboard);
        configuracoesSROPage.preencheToleranciaAcidenteNorte(configuracoesDashboard);
        configuracoesSROPage.preencheToleranciaAcidenteSul(configuracoesDashboard);
        addScreenshotToReport("Preencheu a tabela de Tolerância de acidentes");
    }

    @E("preencho a tabela de Tolerância para gravidades")
    public void preenchoATabelaDeTolerânciaParaGravidades() {
        configuracoesSROPage.preencheToleranciaGravidadeCentral(configuracoesDashboard);
        configuracoesSROPage.preencheToleranciaGravidadeNorte(configuracoesDashboard);
        configuracoesSROPage.preencheToleranciaGravidadeSul(configuracoesDashboard);
        addScreenshotToReport("Preencheu a tabela de Tolerância de gravidades");

    }

    @E("valido as informações inseridas nas configurações do dashboard")
    public void validoAsInformaçõesInseridasNasConfiguraçõesDoDashboard() {
        configuracoesSROPage.validaToleranciaAcidenteNorte(configuracoesDashboard);
        configuracoesSROPage.validaToleranciaAcidenteSul(configuracoesDashboard);
        configuracoesSROPage.validaToleranciaAcidenteCentral(configuracoesDashboard);
        configuracoesSROPage.validaToleranciaGravidadeNorte(configuracoesDashboard);
        configuracoesSROPage.validaToleranciaGravidadeSul(configuracoesDashboard);
        configuracoesSROPage.validaToleranciaGravidadeCentral(configuracoesDashboard);
        addScreenshotToReport("Validou as informações inseridas nas configurações do dashboard");
    }
}
