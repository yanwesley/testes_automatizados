package gherkin.stepdefinition.configuracoes;

import br.com.pom.sro.configuracoes.ParametrizacaoSROPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;

public class ParametrizacaoSROSteps extends ReporterUtils {

    ParametrizacaoSROPage parametrizacaoSROPage = new ParametrizacaoSROPage();

    @E("acesso a TAB Parametrização")
    public void acessoATABParametrização() {
        parametrizacaoSROPage.clicarTabParametrizacao();
        addLogToReport("acesso a TAB Parametrização");
    }
}
