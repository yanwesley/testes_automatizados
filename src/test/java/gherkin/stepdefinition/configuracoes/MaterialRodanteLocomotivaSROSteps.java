package gherkin.stepdefinition.configuracoes;

import br.com.api.model.configuracoes.ConfiguracaoMaterialRodanteLocomotiva;
import br.com.pom.sro.configuracoes.MaterialRodanteBlocoLocomotivaSROPage;
import br.com.pom.sro.configuracoes.MaterialRodanteConfiguracaoLocomotivaPage;
import br.com.utils.ReporterUtils;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

import static br.com.api.GeradorDeMassa.getConfiguracaoMaterialRodanteLocomotiva;
import static br.com.api.GeradorDeMassa.getNewConfiguracaoMaterialRodanteLocomotiva;

public class MaterialRodanteLocomotivaSROSteps extends ReporterUtils {

    MaterialRodanteBlocoLocomotivaSROPage materialRodanteBlocoLocomotivaSROPage = new MaterialRodanteBlocoLocomotivaSROPage();
    MaterialRodanteConfiguracaoLocomotivaPage materialRodanteConfiguracaoLocomotivaPage = new MaterialRodanteConfiguracaoLocomotivaPage();
    ConfiguracaoMaterialRodanteLocomotiva configuracaoMaterialRodanteLocomotiva = getConfiguracaoMaterialRodanteLocomotiva();
    ConfiguracaoMaterialRodanteLocomotiva configuracaoMaterialRodanteLocomotivaEditada = getNewConfiguracaoMaterialRodanteLocomotiva();

    @E("acesso a TAB Material Rodante das configurações")
    public void eAcessoATABMaterialRodanteDasConfigurações() {
        materialRodanteBlocoLocomotivaSROPage.clicarTabMaterialRodante();
        addLogToReport("Clicou na TAB Material Rodante");
    }

    @Entao("clico no botão Adicionar bloco de configuração de Material Rodante")
    public void clicoNoBotãoAdicionarBlocoDeConfiguraçãoDeMaterialRodante() {
        materialRodanteBlocoLocomotivaSROPage.clicarBtnAddConfigMaterialRodante();
        addLogToReport("Clicou no botão Adicionar bloco de configuração");
    }

    @E("Insiro o nome do bloco da configuração de Material Rodante")
    public void insiroONomeDoBlocoDaConfiguraçãoDeMaterialRodante() {
        //TODO: trocar a string pelo objeto
        materialRodanteBlocoLocomotivaSROPage.adicionarBlocoDeConfiguracaoMaterialRodante(configuracaoMaterialRodanteLocomotiva);
        addScreenshotToReport("Inseriu o nome do bloco de configuração");
    }

    @E("Clica em confirmar no dialog para adicionar uma configuração de Material Rodante")
    public void clicaEmConfirmarNoDialogParaAdicionarUmaConfiguraçãoDeMaterialRodante() {
        materialRodanteBlocoLocomotivaSROPage.clicarBtnConfirmarBlocoDeConfiguracao();
        addLogToReport("Clicou no botão confirmar do dialog para adicionar configuração de Material Rodante");
    }

    @E("valida que um novo bloco que configuração de Material Rodante foi adicionado")
    public void validaQueUmNovoBlocoQueConfiguraçãoDeMaterialRodanteFoiAdicionado() {
        materialRodanteBlocoLocomotivaSROPage.validaBlocoDeConfiguracaoInserido(configuracaoMaterialRodanteLocomotiva);
        addScreenshotToReport("Validou que um novo bloco que configuração de Material Rodante foi adicionado");
    }

    @E("valida que o status do bloco de configuração é {string}")
    public void validaQueOStatusDoBlocoDeConfiguraçãoDaLinhaÉ(String status) {
        materialRodanteBlocoLocomotivaSROPage.validaStatusBloco(status);
        addScreenshotToReport("Validou o status do bloco de configuração");
    }

    @E("valida que um novo bloco que configuração de Material Rodante foi editado")
    public void validaQueUmNovoBlocoQueConfiguraçãoDeMaterialRodanteFoiEditado() {
        materialRodanteBlocoLocomotivaSROPage.validaBlocoDeConfiguracaoInserido(configuracaoMaterialRodanteLocomotivaEditada);
        addScreenshotToReport("Editou o nome do bloco de configuração");
    }

    @Entao("clico no botão Editar bloco")
    public void clicoNoBotãoEditarBloco() {
        materialRodanteBlocoLocomotivaSROPage.clicarBtnEditarBloco();
        addLogToReport("Clicou no botão Editar bloco");
    }

    @E("edito os dados do bloco de configuração")
    public void editoOsDadosDoBlocoDeConfiguração() {
        materialRodanteBlocoLocomotivaSROPage.editaDadosBlocoDeConfiguracao(configuracaoMaterialRodanteLocomotivaEditada);
        addScreenshotToReport("Editou os dados do bloco de configuração");
    }

    @E("deixa o bloco de configuração de material rodante ativo")
    public void deixaOBlocoDeConfiguraçãoDeMaterialRodanteAtivo() {
        materialRodanteBlocoLocomotivaSROPage.deixaBlocoDeConfiguracaoAtivo();
        addScreenshotToReport("Alterou o status do bloco de configuração");
    }

    @E("clico no separator para expandir o formulário de configuração de material rodante")
    public void clicoNoSeparatorParaExpandirOFormulárioDeConfiguraçãoDeMaterialRodante() {
        materialRodanteBlocoLocomotivaSROPage.clicarSeparatorConfiguracaoMaterialRodante();
        addLogToReport("Clica no separator para expandir o form");
    }

    @E("clico no botão adicionar configuração na configuração de locomotiva")
    public void clicoNoBotãoAdicionarConfiguraçãoNaConfiguraçãoDeLocomotiva() {
        materialRodanteBlocoLocomotivaSROPage.clicarBtnAdicionarConfiguracaoLocomotiva();
        addLogToReport("Cliquei sobre o botão adicionar configuração");
    }

    @E("preencho o modal adicionar bloco sem copiar parâmetro")
    public void preenchoOModalAdicionarBloco() {
        materialRodanteBlocoLocomotivaSROPage.preencherModalAdicionarBloco();
        addScreenshotToReport("Preenchi o modal sem copiar os parâmetros de configuração");
    }

    @Entao("preencho a tela de criar configuração locomotiva")
    public void preenchoATelaDeCriarConfiguraçãoLocomotiva() {
        materialRodanteConfiguracaoLocomotivaPage.preencherTelaCriarConfiguracaoLocomotiva(configuracaoMaterialRodanteLocomotiva, true);
        addScreenshotToReport("Preenchi a tela de criar configuração da locomotiva");
    }

    @E("clico em salvar a configuração da locomotiva")
    public void clicoEmSalvarAConfiguraçãoDaLocomotiva() {
        materialRodanteConfiguracaoLocomotivaPage.clicarBtnSalvarConfiguracao();
        addLogToReport("Cliquei sobre o botão salvar das configurações locomotiva");
    }

    @E("clico no botão editar configuração na configuração de locomotiva")
    public void clicoNoBotãoEditarConfiguraçãoNaConfiguraçãoDeLocomotiva() {
        materialRodanteConfiguracaoLocomotivaPage.clicarBtnEditarConfiguracao();
        addLogToReport("Cliquei sobre o botão editar da configuração");
    }

    @Entao("edito a tela de criar configuração locomotiva inicial")
    public void editoATelaDeCriarConfiguraçãoLocomotivaInicial() {
        materialRodanteConfiguracaoLocomotivaPage.preencherTelaCriarConfiguracaoLocomotiva(configuracaoMaterialRodanteLocomotivaEditada, false);
        addScreenshotToReport("Editei a tela de criar configuração da locomotiva");
    }

    @E("valido que as informações de configuração de locomotiva inicial persistiram")
    public void validoQueAsInformaçõesDeConfiguraçãoDeLocomotivaInicialPersistiram() {
        materialRodanteConfiguracaoLocomotivaPage.validarQueAsInformacoesLocomotivaPersistiram(configuracaoMaterialRodanteLocomotiva, false);
        addScreenshotToReport("Validei que as informações persistiram na tela de configurações");
    }

    @Entao("preencho a tela de criar configuração locomotiva inicial")
    public void preenchoATelaDeCriarConfiguraçãoLocomotivaInicial() {
        materialRodanteConfiguracaoLocomotivaPage.preencherTelaCriarConfiguracaoLocomotiva(configuracaoMaterialRodanteLocomotiva, false);
        addScreenshotToReport("Preenchi a tela de criar configuração da locomotiva");
    }

    @E("valido que as informações de configuração de locomotiva persistiram")
    public void validoQueAsInformaçõesDeConfiguraçãoDeLocomotivaPersistiram() {
        materialRodanteConfiguracaoLocomotivaPage.validarQueAsInformacoesLocomotivaPersistiram(configuracaoMaterialRodanteLocomotiva, true);
        addScreenshotToReport("Validei que as informações persistiram na tela de configurações");
    }

    @E("valido que as informações de configuração de locomotiva editadas persistiram")
    public void validoQueAsInformaçõesDeConfiguraçãoDeLocomotivaEditadasPersistiram() {
        materialRodanteConfiguracaoLocomotivaPage.validarQueAsInformacoesLocomotivaPersistiram(configuracaoMaterialRodanteLocomotivaEditada, false);
        addScreenshotToReport("Validei que as informações persistiram na tela de configurações");
    }

    @E("clico no botão editar configuração adicional na configuração de locomotiva na linha {int}")
    public void clicoNoBotãoEditarConfiguraçãoAdicionalNaConfiguraçãoDeLocomotiva(int linha) {
        materialRodanteConfiguracaoLocomotivaPage.clicarBtnEditarConfiguracaoAdicional(linha);
        addLogToReport("Cliquei sobre o botão editar da configuração");
    }

    @E("clico no botão excluir configuração adicional de locomotiva na linha {int}")
    public void clicoNoBotãoExcluirConfiguraçãoAdicionalDeLocomotiva(int linha) {
        materialRodanteConfiguracaoLocomotivaPage.clicarBtnExcluirConfiguracao(linha);
        addLogToReport("Cliquei sobre o botão excluir configuração adicional");
    }

    @E("confirmo a exclusão da configuração adicional locomotiva")
    public void confirmoAExclusãoDaConfiguraçãoAdicionalLocomotiva() {
        materialRodanteConfiguracaoLocomotivaPage.confirmarExclusaoConfiguracao();
        addScreenshotToReport("Cliquei no modal de confirmação de exclusão de configuração");
    }

    @E("preencho o modal adicionando configuração copiada")
    public void preenchoOModalAdicionandoConfiguraçãoCopiada() {
        materialRodanteBlocoLocomotivaSROPage.adicionarConfiguracaoCopiada();
        addScreenshotToReport("Selecionei uma configuração a ser copiada");
    }

    @Entao("preencho o modelo e configuração da tela de configuração")
    public void preenchoModeloEConfiguracaoLocomotiva() {
        materialRodanteConfiguracaoLocomotivaPage.preenchoModeloEConfiguracaoLocomotiva(configuracaoMaterialRodanteLocomotiva);
        addScreenshotToReport("Preenchi o modelo e a configuração da locomotiva");
    }

    @Entao("valido que os valores dos parâmetros foram copiados da configuração locomotiva")
    public void validoQueOsValoresDosParâmetrosForamCopiadosDaConfiguraçãoLocomotiva() {
        materialRodanteConfiguracaoLocomotivaPage.validarValoresConfiguracaoCopiada(configuracaoMaterialRodanteLocomotiva);
        addScreenshotToReport("Valido que as configurações foram copiadas corretamente");
    }
}
