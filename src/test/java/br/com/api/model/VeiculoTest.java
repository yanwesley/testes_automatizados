package br.com.api.model;

import br.com.api.model.sinistro.Veiculo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VeiculoTest {
    Veiculo veiculo;

    @Before()
    public void setup() {
        veiculo = new Veiculo();
    }

    @After()
    public void tear() {
        veiculo = null;
    }

    @Test
    public void getQtdRodeiros() {
        assert veiculo.getQtdRodeiros() >= 0;
    }

    @Test
    public void getIcone() {
        veiculo.setTipoValue("Vagão");
        assert veiculo.getIcone().equals("Ícone - Vagão - Alerta");

        veiculo.setTipoValue("Locomotiva");
        assert veiculo.getIcone().equals("Ícone - Locomotiva - Alerta");

        veiculo.setTipoValue("");
        assert veiculo.getIcone().equals("");
    }

    @Test
    public void getPosicao() {
        assert veiculo.getPosicao() >= 100;
    }

    @Test
    public void getIndexTipo() {
        assert veiculo.getIndexTipo() >= 0 && veiculo.getIndexTipo() <= 2;
    }

    @Test
    public void getSituacaoIndex() {
        assert veiculo.getSituacaoIndex() >= 0 && veiculo.getSituacaoIndex() <= 6;
    }

    @Test
    public void getNumero() {
        assert !veiculo.getNumero().isEmpty();
    }

    @Test
    public void getTipoValue() {
        assert veiculo.getTipoValue() == null;
        veiculo.setTipoValue("TESTE");
        assert veiculo.getTipoValue().equals("TESTE");
    }

    @Test
    public void setTipoValue() {
        veiculo.setTipoValue("TESTE");
        assert veiculo.getTipoValue().equals("TESTE");
    }

    @Test
    public void getModelo() {
        assert veiculo.getModelo().length() <= 5;
    }

    @Test
    public void getMercadoria() {
        assert veiculo.getMercadoria().split(" ").length == 1;
    }

    @Test
    public void getSituacaoValue() {
        assert veiculo.getSituacaoValue() == null;
        veiculo.setSituacaoValue("TESTE");
        assert veiculo.getSituacaoValue().equals("TESTE");
    }

    @Test
    public void setSituacaoValue() {
        veiculo.setSituacaoValue("TESTE1");
        assert veiculo.getSituacaoValue().equals("TESTE1");
    }
}