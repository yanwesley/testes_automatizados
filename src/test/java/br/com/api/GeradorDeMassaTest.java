package br.com.api;


import br.com.api.model.sinistro.VeiculoDeTerceiro;
import com.github.javafaker.Faker;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Locale;

public class GeradorDeMassaTest {

    @Test
    public void getNewVeiculoDeTerceiros() {
        VeiculoDeTerceiro veiculoDeTerceiro = GeradorDeMassa.getNewVeiculoDeTerceiro();
        Assert.assertNotNull(veiculoDeTerceiro.getMarca());
        Assert.assertNotNull(veiculoDeTerceiro.getModelo());
        Assert.assertNotNull(veiculoDeTerceiro.getPlaca());
    }

    @Test
    public void testaChar() {

        Faker faker = new Faker(new Locale("pt-BR"));
//        System.out.println(faker.lorem().characters(200, true));
//        System.out.println(faker.lorem().words(200));
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzáéíóúçâêîôû0123456789~`!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?";
        String pwd = RandomStringUtils.random(2000, characters);
        System.out.println(pwd);
        assert pwd.length() == 2000;
    }

}
