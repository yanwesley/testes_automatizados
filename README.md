_README in English [here](README.en-us.md)_  
# Testes Automatizados de Front-End

## PRÉ-REQUISITOS

Descreva os requisitos de software e hardware que é necessário para executar este projeto de automação

*   Java 1.8 SDK
*   Maven 3.5.*
*   Node.js 8.*

## EMPRESA DESENVOLVEDORA

GFT

## AUTORES

* **Deovan Zanol**
* **Felipe Nowak**
* **Yan Wesley**