#  Mapeamento de elementos 

```
//Atualizar o selector do elemento

var allElement = document.querySelector("#sobre-o-amv > div").getElementsByTagName("*");

for (var i = 0; i < allElement.length; i++) {

    if (!!allElement[i].id) {

        var variableName = allElement[i].tagName.replace('MAT-', '').toLowerCase() + ' ' + allElement[i].id;

        var variableCamelCase = variableName.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
            return index == 0 ? word.toLowerCase() : word.toUpperCase();
        }).replace(/\s+/g, '');

        console.log('private By', variableCamelCase.replace(new RegExp('-', 'g'), ''), '= By.id("' + allElement[i].id + '");');

    } else if (!!allElement[i].getAttribute("name")) {

        var variableName = allElement[i].tagName.replace(new RegExp('SRO-TOPIC-SEPARATOR', 'g'), "separator") + ' ' + allElement[i].getAttribute("name");
        variableName = variableName.toLowerCase();

        var variableCamelCase = variableName.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
            return index == 0 ? word.toLowerCase() : word.toUpperCase();
        }).replace(/\s+/g, '');

        console.log('private By', variableCamelCase.replace(new RegExp('-', 'g'), ''), '= By.name("' + allElement[i].getAttribute("name") + '");');
    }
}


```

 Deve retornar uma lista de elementos mapeados por id, declarado em java 

**Example:** 
 ```
private By buttonCancelardadosvia = By.id("cancelarDadosVia");
```